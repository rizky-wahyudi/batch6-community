package main;

import java.time.LocalDate;

import model.Menetapkan;
import model.Mengingat;
import model.Menimbang;
import model.Peraturan;

public class Main {
	public static void main(String[] args) {
		
		Peraturan pUud = new Peraturan("PENGESAHAN PERSETUJUAN KEMITRAAN EKONOMI KOMPREHENSIF INDONESIA–AUSTRALIA"
				+ " (INDONESIA–AUSTRALIA COMPREHENSIVE ECONOMIC PARTNERSHIP AGREEMENT)"
				, "undang-undang", "2020", 67, 674 , 1, LocalDate.of(2020, 2, 28), LocalDate.of(2020, 2, 28));
		
		Menimbang MbUud = new Menimbang("bahwa penyebaran Corona Virus Disease 2019 (COVID-19)\r\n"
				+ "yang dinyatakan oleh Organisasi Kesehatan Dunia (World\r\n"
				+ "Health Organization) sebagai pandemi pada sebagian besar\r\n"
				+ "negara-negara di seluruh dunia, termasuk di Indonesia,\r\n"
				+ "menunjukkan peningkatan dari waktu ke waktu dan telah\r\n"
				+ "menimbulkan korban jiwa, serta kerugian material yang\r\n"
				+ "semakin besar, sehingga berimplikasi pada aspek sosial,\r\n"
				+ "ekonomi, dan kesejahteraan masyarakat", "bahwa implikasi pandemi Corona Virus Disease 2019 (COVID19) telah berdampak antara lain terhadap perlambatan\r\n"
						+ "pertumbuhan ekonomi nasional, penurunan penerimaan\r\n"
						+ "negara, dan peningkatan belanja negara dan pembiayaan,\r\n"
						+ "sehingga diperlukan berbagai upaya Pemerintah untuk\r\n"
						+ "melakukan penyelamatan kesehatan dan perekonomian");
		
		Mengingat MgUud = new Mengingat("Pasal 5 ayat (1), Pasal 20, dan Pasal 22 ayat (2) Undang-Undang\r\n"
				+ "Dasar Negara Republik Indonesia Tahun 1945;\r\n"
				+ "");
		
		Menetapkan MnUud = new Menetapkan("Beberapa ketentuan dalam Undang-Undang Nomor 24\r\n"
				+ "Tahun 2003 tentang Mahkamah Konstitusi (Lembaran Negara\r\n"
				+ "Republik Indonesia Tahun 2003 Nomor 98, Tambahan\r\n"
				+ "Lembaran Negara Republik Indonesia Nomor 4316), yang telah\r\n"
				+ "beberapa kali diubah dengan Undang-Undang", "Nomor 8 Tahun 2011 tentang Perubahan atas\r\n"
						+ "Undang-Undang Nomor 24 Tahun 2003 tentang\r\n"
						+ "Mahkamah Konstitusi (Lembaran Negara Republik\r\n"
						+ "Indonesia Tahun 2011 Nomor 70, Tambahan Lembaran\r\n"
						+ "Negara Republik Indonesia Nomor 5226)", "Dihapus.", "Nomor 4 Tahun 2014 tentang Penetapan Peraturan\r\n"
								+ "Pemerintah Pengganti Undang-Undang Nomor 1\r\n"
								+ "Tahun 2013 tentang Perubahan Kedua atas UndangUndang Nomor 24 Tahun 2003 tentang Mahkamah\r\n"
								+ "Konstitusi Menjadi Undang-Undang (Lembaran Negara\r\n"
								+ "Republik Indonesia Tahun 2014 Nomor 5, Tambahan\r\n"
								+ "Lembaran Negara Republik Indonesia Nomor 5456);", "Dihapus.", "Kepaniteraan sebagaimana dimaksud dalam Pasal 7\r\n"
										+ "merupakan jabatan fungsional yang menjalankan\r\n"
										+ "tugas teknis administratif peradilan Mahkamah\r\n"
										+ "Konstitusi dengan usia pensiun 62 (enam puluh\r\n"
										+ "dua) tahun bagi panitera, panitera muda, dan\r\n"
										+ "panitera pengganti.", "a. koordinasi pelaksanaan teknis peradilan di\r\n"
												+ "Mahkamah Konstitusi;", "Tugas teknis administratif peradilan sebagaimana\r\n"
														+ "dimaksud pada ayat (1) meliputi:", "Hakim konstitusi harus memenuhi syarat sebagai\r\n"
																+ "berikut:", "pelaksanaan tugas lain yang diberikan oleh\r\n"
																		+ "Ketua Mahkamah Konstitusi sesuai dengan\r\n"
																		+ "bidang tugasnya.");
		
		pUud.printPeraturan();
		MbUud.printMenimbang();
		MgUud.printMengingat();
		MnUud.printMenetapkan();
		
	}

}
