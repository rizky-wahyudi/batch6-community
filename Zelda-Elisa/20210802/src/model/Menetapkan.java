package model;

import java.util.ArrayList;

public class Menetapkan {
	ArrayList<Menetapkan> menetapkan;
	private String pasal1,pasal2,subPasal1A,subPasal1B,subPasal2A,subPasal2B,
	mesPasal1A,mesPasal1B,mesPasal2A,mesPasal2B;
	
	public Menetapkan(String pasal1,String pasal2,String subPasal1A,String subPasal1B,String subPasal2A,
			String subPasal2B,String mesPasal1A,String mesPasal1B,String mesPasal2A,String mesPasal2B) {
		this.pasal1 =pasal1;
		this.pasal2=pasal2;
		this.subPasal1A=subPasal1A;
		this.subPasal1B=subPasal1B;
		this.subPasal2A=subPasal2A;
		this.subPasal2B=subPasal2B;
		this.mesPasal1A=mesPasal1A;
		this.mesPasal1B=mesPasal1B;
		this.mesPasal2A=mesPasal2A;
		this.mesPasal2B=mesPasal2B;
	}

	public String getPasal1() {
		return pasal1;
	}

	public String getPasal2() {
		return pasal2;
	}

	public String getSubPasal1A() {
		return subPasal1A;
	}

	public String getSubPasal1B() {
		return subPasal1B;
	}

	public String getSubPasal2A() {
		return subPasal2A;
	}

	public String getSubPasal2B() {
		return subPasal2B;
	}

	public String getMesPasal1A() {
		return mesPasal1A;
	}

	public String getMesPasal1B() {
		return mesPasal1B;
	}

	public String getMesPasal2A() {
		return mesPasal2A;
	}

	public String getMesPasal2B() {
		return mesPasal2B;
	}
	
	public void printMenetapkan() {
		System.out.println("");
		System.out.println("Pasal 1");
		System.out.println(this.pasal1);
		System.out.println("");
		System.out.println("Sub pasal 1 A");
		System.out.println(this.subPasal1A);
		System.out.println("");
		System.out.println("Message 1 A");
		System.out.println(this.mesPasal1A);
		System.out.println("");
		System.out.println("Sub pasal 1 B");
		System.out.println(this.subPasal1B);
		System.out.println("");
		System.out.println("Message 1 B");
		System.out.println(this.mesPasal2B);
		System.out.println("");
		System.out.println("Pasal 2");
		System.out.println(this.pasal2);
		System.out.println("");
		System.out.println("Sub pasal 2 A");
		System.out.println(this.subPasal2A);
		System.out.println("");
		System.out.println("Message 2 A");
		System.out.println(this.mesPasal2A);
		System.out.println("");
		System.out.println("Sub pasal 2 B");
		System.out.println(this.subPasal1B);
		System.out.println("Message 2 B");
		System.out.println(this.mesPasal2B);
		

	}
	
}
