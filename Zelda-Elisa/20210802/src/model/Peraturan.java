package model;

import java.time.LocalDate;
import java.util.ArrayList;

import model.Peraturan;

public class Peraturan {
	ArrayList<Peraturan> peraturan;
	private LocalDate tanggalDiundangkan, tanggalDitetapkan;
	private int noLn, noTln, noPeraturan;
	private String tentang, jenis, tahunPeraturan;
	
	public Peraturan() {
		this.peraturan = new ArrayList<Peraturan>();
		
	}
	
	public Peraturan(String tentang,String jenis, String tahunPeraturan, int noLn,
			int noTln, int noPeraturan, LocalDate tanggalDiundangkan, LocalDate tanggalDitetapkan) {
		this.tentang = tentang;
		this.jenis =jenis;
		this.noLn = noLn;
		this.noPeraturan =noPeraturan;
		this.tahunPeraturan = tahunPeraturan;
		this.tanggalDitetapkan = tanggalDitetapkan;
		this.noTln = noTln;
		this.tanggalDiundangkan = tanggalDiundangkan;
		
	}


	public LocalDate getTanggalDiundangkan() {
		return tanggalDiundangkan;
	}

	public LocalDate getTanggalDitetapkan() {
		return tanggalDitetapkan;
	}

	public int getNoLn() {
		return noLn;
	}

	public int getNoTln() {
		return noTln;
	}

	public int getNoPeraturan() {
		return noPeraturan;
	}

	public String getTentang() {
		return tentang;
	}

	public String getJenis() {
		return jenis;
	}

	public String getTahunPeraturan() {
		return tahunPeraturan;
	}
	
	public void printPeraturan() {
		System.out.println("Jenis Peraturan      : " +this.jenis);
		System.out.println("No Peraturan       	 : " +this.noPeraturan);
		System.out.println("Tahun Peraturan 	 : " +this.tahunPeraturan);
		System.out.println("Tentang      		 : " +this.tentang);
		System.out.println("Tanggal Ditetapkan   : " +this.tanggalDitetapkan);
		System.out.println("No LN        		 : " +this.noLn);
		System.out.println("No TLN        		 : " +this.noTln);
		System.out.println("Tanggal Diundangkan  : " +this.tanggalDiundangkan);
	}

	
	
}