package model;

import java.util.ArrayList;

public class Menimbang {
	ArrayList<Menimbang> menimbang;
	private String poinA, poinB;
	
	public Menimbang(String poinA, String poinB) {
		this.poinA = poinA;
		this.poinB = poinB;
	}

	public String getPoinA() {
		return poinA;
	}

	public String getPoinB() {
		return poinB;
	}

	public void printMenimbang() {
		System.out.println("");
		System.out.println("Poin A");
		System.out.println(this.poinA);
		System.out.println("");
		System.out.println("Poin B");
		System.out.println(this.poinB);
	}
}
