import React from "react";
import Button from "@material-ui/core/Button";
import MobileStepper from "@material-ui/core/MobileStepper";
import Paper from "@material-ui/core/Paper";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import Typography from "@material-ui/core/Typography";
import { useTheme } from "@material-ui/core/styles";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";

const MyCollection = [
{
	label: "First Picture",
	imgPath:"https://media.matamata.com/thumbs/2021/06/17/74684-jerome-polin-edit-foto-keluarga-bareng-waseda-boys-instagramjeromepolin/745x489-img-74684-jerome-polin-edit-foto-keluarga-bareng-waseda-boys-instagramjeromepolin.jpg",
},
{
	label: "Second Picture",
	imgPath:"https://cdn.tmpo.co/data/2020/07/15/id_952934/952934_720.jpg",
},
{
	label: "Third Picture",
	imgPath:"https://image.akurat.co/images/uploads/images/akurat_20200310110338_op45eW.jpg",
},
];

const App = () => {
const CollectionSize = MyCollection.length;
const theme = useTheme();
const [index, setActiveStep] = React.useState(0);

const goToNextPicture = () => {
	setActiveStep((prevActiveStep) => prevActiveStep + 1);
};

const goToPrevPicture = () => {
	setActiveStep((prevActiveStep) => prevActiveStep - 1);
};

return (
	<div className="slider">
	<h2>Handson Image Slider</h2>
	<div className="image">
		<Paper square elevation={0}>
		<Typography>{MyCollection[index].label}</Typography>
		</Paper>
		<img src={MyCollection[index].imgPath} alt={MyCollection[index].label}/>
		<MobileStepper variant="text" position="static" index={index} steps={CollectionSize}
		nextButton={   
			<Button size="small" onClick={goToNextPicture} disabled={index === CollectionSize - 1}>
			Next {theme.direction !== "rtl" ? (
         <KeyboardArrowRight />
			) : (
			<KeyboardArrowLeft />
			)}
			</Button>
		}
      prevButton={   
			<Button size="small" onClick={goToPrevPicture} disabled={index === CollectionSize + 1}>
			Prev {theme.direction !== "rtl" ? (
         <KeyboardArrowRight />
			) : (
			<KeyboardArrowLeft />
			)}
			</Button>
		}
		/>
	</div>
	</div>
);
};

export default App;
