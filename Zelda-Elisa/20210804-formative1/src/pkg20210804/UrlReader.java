/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg20210804;

/**
 *
 * @author User
 */
import java.net.*;
import java.io.*;

public class UrlReader {
    public static void main(String[] args) throws Exception {

        URL google = new URL("https://google.com ");
        BufferedReader in = new BufferedReader(
        new InputStreamReader(google.openStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null)
            System.out.println(inputLine);
        in.close();
    }
}
