import java.util.Scanner;
public class VendingMachine {
    public static void main(String[] args) {
        Money mon = new Money();
        Food food = new Food();

        System.out.println("SELAMAT DATANG DI VENDING MACHINE");
        System.out.println("----------------------------------");
        System.out.println("a1 a2 a3 a4 a5 a6 a7 a8 a9 a10");
        System.out.println("b1 b2 b3 b4 b5 b6 b7 b8 b9 b10");
        System.out.println("c1 c2 c3 c4 c5 c6 c7 c8 c9 c10");
        System.out.println("d1 d2 d3 d4 d5 d6 d7 d8 d9 d10");
        System.out.println("e1 e2 e3 e4 e5 e6 e7 e8 e9 e10");
        System.out.println("f1 f2 f3 f4 f5 f6 f7 f8 f9 f10");
        System.out.println("----------------------------------");
        System.out.println("Dengan Rincian =");
        System.out.println("----------------------------------");
        System.out.println("1. a1 - a10 Cheetos Rp.10.000,- ");
        System.out.println("2. b1 - b10 Silver Queen Rp.15.000,- ");
        System.out.println("3. c1 - c10 Chupacups Rp.5.000,- ");
        System.out.println("4. d1 - d10 Mineral Water Rp.5.000,- ");
        System.out.println("5. e1 - e10 Coca-cola Rp.10.000,- ");
        System.out.println("6. f1 - f10 Fresh Tea Rp.10.000,- ");
        System.out.println("----------------------------------");
        System.out.print("Harap masukkan nominal uang anda = ");

        Scanner input_money = new Scanner(System.in);
        mon.pecahan=input_money.nextInt();

            System.out.print("Pilih Menu baris (a/b/c/d/e/f)= ");
            Scanner input_menu = new Scanner(System.in);
            String baris=input_menu.nextLine();

            if(baris.equals("a")){
                System.out.println("food name = "+food.a);
                System.out.println("price = "+food.priceA);
                if(mon.pecahan<food.priceA){
                    System.out.print("uang anda kurang "+(food.priceA-mon.pecahan)+" mohon tambah = ");
                    Scanner input_m = new Scanner(System.in);
                    mon.mTambah=input_m.nextInt();
                    System.out.println("saldo anda =  Rp."+(mon.mTambah+mon.pecahan));
                }
                System.out.print("Ingin melanjutkan? (ya/tidak) = ");
                Scanner cancel = new Scanner(System.in);
                String c = cancel.nextLine();

                if(c.equals("ya")){
                    System.out.println("silahkan ambil pesanan anda");
                    System.out.println("kembalian anda = Rp."+(mon.pecahan+mon.mTambah-food.priceA));
                }
                else if(c.equals("tidak")){
                    System.out.println("Cancel berhasil ambil kembali uang anda");
                    System.out.println("Rp."+mon.pecahan);
                    System.exit(0);
                }
                else{
                    System.exit(0);
        
                }
            }
            else if(baris.equals("b")){
                System.out.println("food name = "+food.b);
                System.out.println("price = "+food.priceB);
                if(mon.pecahan<food.priceB){
                    System.out.print("uang anda kurang "+(food.priceB-mon.pecahan)+" mohon tambah = ");
                    Scanner input_m = new Scanner(System.in);
                    mon.mTambah=input_m.nextInt();
                    System.out.println("saldo anda =  Rp."+(mon.mTambah+mon.pecahan));
                }
                System.out.print("Ingin melanjutkan? (ya/tidak) = ");
                Scanner cancel = new Scanner(System.in);
                String c = cancel.nextLine();

                if(c.equals("ya")){
                    System.out.println("silahkan ambil pesanan anda");
                    System.out.println("kembalian anda = Rp."+(mon.pecahan+mon.mTambah-food.priceB));
                }
                else if(c.equals("tidak")){
                    System.out.println("Cancel berhasil ambil kembali uang anda");
                    System.out.println("Rp."+mon.pecahan);
                    System.exit(0);
                }
                else{
                    System.exit(0);
        
                }
                
            }
            else if(baris.equals("c")){
                System.out.println("food name = "+food.c);
                System.out.println("price = "+food.priceC);
                if(mon.pecahan<food.priceC){
                    System.out.print("uang anda kurang "+(food.priceC-mon.pecahan)+" mohon tambah = ");
                    Scanner input_m = new Scanner(System.in);
                    mon.mTambah=input_m.nextInt();
                    System.out.println("saldo anda =  Rp."+(mon.mTambah+mon.pecahan));
                }
                System.out.print("Ingin melanjutkan? (ya/tidak) = ");
                Scanner cancel = new Scanner(System.in);
                String c = cancel.nextLine();

                if(c.equals("ya")){
                    System.out.println("silahkan ambil pesanan anda");
                    System.out.println("kembalian anda = Rp."+(mon.pecahan+mon.mTambah-food.priceC));
                }
                else if(c.equals("tidak")){
                    System.out.println("Cancel berhasil ambil kembali uang anda");
                    System.out.println("Rp."+mon.pecahan);
                    System.exit(0);
                }
                else{
                    System.exit(0);
        
                }
            }
            else if(baris.equals("d")){
                System.out.println("food name = "+food.d);
                System.out.println("price = "+food.priceD);
                if(mon.pecahan<food.priceD){
                    System.out.print("uang anda kurang "+(food.priceD-mon.pecahan)+" mohon tambah = ");
                    Scanner input_m = new Scanner(System.in);
                    mon.mTambah=input_m.nextInt();
                    System.out.println("saldo anda =  Rp."+(mon.mTambah+mon.pecahan));
                }
                System.out.print("Ingin melanjutkan? (ya/tidak) = ");
                Scanner cancel = new Scanner(System.in);
                String c = cancel.nextLine();

                if(c.equals("ya")){
                    System.out.println("silahkan ambil pesanan anda");
                    System.out.println("kembalian anda = Rp."+(mon.pecahan+mon.mTambah-food.priceD));
                }
                else if(c.equals("tidak")){
                    System.out.println("Cancel berhasil ambil kembali uang anda");
                    System.out.println("Rp."+mon.pecahan);
                    System.exit(0);
                }
                else{
                    System.exit(0);
        
                }
            }
            else if(baris.equals("e")){
                System.out.println("food name = "+food.e);
                System.out.println("price = "+food.priceE);
                if(mon.pecahan<food.priceE){
                    System.out.print("uang anda kurang "+(food.priceE-mon.pecahan)+" mohon tambah = ");
                    Scanner input_m = new Scanner(System.in);
                    mon.mTambah=input_m.nextInt();
                    System.out.println("saldo anda =  Rp."+(mon.mTambah+mon.pecahan));
                }
                System.out.print("Ingin melanjutkan? (ya/tidak) = ");
                Scanner cancel = new Scanner(System.in);
                String c = cancel.nextLine();

                if(c.equals("ya")){
                    System.out.println("silahkan ambil pesanan anda");
                    System.out.println("kembalian anda = Rp."+(mon.pecahan+mon.mTambah-food.priceE));
                }
                else if(c.equals("tidak")){
                    System.out.println("Cancel berhasil ambil kembali uang anda");
                    System.out.println("Rp."+mon.pecahan);
                    System.exit(0);
                }
                else{
                    System.exit(0);
        
                }
            }
            else if(baris.equals("f")){
                System.out.println("food name = "+food.f);
                System.out.println("price = "+food.priceF);
                if(mon.pecahan<food.priceF){
                    System.out.print("uang anda kurang "+(food.priceF-mon.pecahan)+" mohon tambah = ");
                    Scanner input_m = new Scanner(System.in);
                    mon.mTambah=input_m.nextInt();
                    System.out.println("saldo anda =  Rp."+(mon.mTambah+mon.pecahan));
                }
                System.out.print("Ingin melanjutkan? (ya/tidak) = ");
                Scanner cancel = new Scanner(System.in);
                String c = cancel.nextLine();

                if(c.equals("ya")){
                    System.out.println("silahkan ambil pesanan anda");
                    System.out.println("kembalian anda = Rp."+(mon.pecahan+mon.mTambah-food.priceF));
                }
                else if(c.equals("tidak")){
                    System.out.println("Cancel berhasil ambil kembali uang anda");
                    System.out.println("Rp."+mon.pecahan);
                    System.exit(0);
                }
                else{
                    System.exit(0);
        
                }
            }

    }
}
