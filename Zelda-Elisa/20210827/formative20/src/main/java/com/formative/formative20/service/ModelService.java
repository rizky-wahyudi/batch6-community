package com.formative.formative20.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formative.formative20.model.ModelPerson;
import com.formative.formative20.repository.ModelRepository;

@Service
public class ModelService {
	@Autowired
	private ModelRepository repo;

	public ModelPerson saveData(ModelPerson model) {
		return repo.save(model);
	}

	public ModelPerson findDataById(int id) {
		return repo.findById(id);
	}

	public List<ModelPerson> findAllData() {
		return repo.findAll();
	}

	public String saveAllData(List<ModelPerson> model) {
		repo.saveAll(model);
		return "Success";
	}

}
