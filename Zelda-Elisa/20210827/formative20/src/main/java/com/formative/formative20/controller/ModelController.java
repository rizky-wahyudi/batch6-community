package com.formative.formative20.controller;

import java.awt.PageAttributes.MediaType;
import java.util.ArrayList;
import java.util.List;
import org.springframework.ui.Model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.formative.formative20.model.ModelPerson;
import com.formative.formative20.service.ModelService;

@RestController
public class ModelController {
		@Autowired
		private ModelService service;

		@GetMapping("/")
		public ModelAndView index() {
			ModelAndView mv = new ModelAndView("index.html");
			List<ModelPerson> data = new ArrayList<>();
			
			return mv;
		}

		@PostMapping(path="/addAll", consumes = MediaType.APPLICATION_JSON_VALUE)
		public void tambah(@RequestBody List<ModelPerson> data,Model model) {
			service.saveAllData(data);
			
		}
		@PostMapping("/showAll")
		public ModelAndView showAll(Model model) {
			ModelAndView mv=new ModelAndView("show.html");
			List<ModelPerson>data=service.findAllData();
			model.addAttribute("data", data);
			mv.addObject(model);
			return mv;
		}



}
