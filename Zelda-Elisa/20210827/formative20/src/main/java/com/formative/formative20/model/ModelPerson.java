package com.formative.formative20.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

public class ModelPerson {
	@Entity
	@Table(name = "dataperson")
	public class DataPerson {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private int id;

		private String name;
		private String email;
		private String body;

		public DataPerson() {

		}

		public DataPerson(int id, String name, String email, String body) {
			this.id = id;
			this.name = name;
			this.email = email;
			this.body = body;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}
}
}
