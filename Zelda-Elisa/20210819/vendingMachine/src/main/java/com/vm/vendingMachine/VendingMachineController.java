package com.vm.vendingMachine;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

@RestController
public class VendingMachineController {
    @Autowired
    private VendingMachineRepository repo;
  
    // Home Page
    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
            + "<h1>WELCOME FROM VENDING MACHINE</h1>"
            + "</body></html>";
    }
  
    // Get All Notes
    @GetMapping("/vendingMachine")
    public List<VendingMachine> getAllNotes() {
        return repo.findAll();
    }
  
    // Get the company details by
    // ID
    @GetMapping("/vendingMachine/{id}")
    public VendingMachine getVendingMachineById(@PathVariable(value = "id") int id) {
        return repo.findById(id-1);
    }

    @GetMapping("vendingMachine/getItem/{koordinate}")
	public String getItem(@PathVariable String koordinate) {
		VendingMachine vendingMachine = repo.findByKoordinate(koordinate);
			updateItem( vendingMachine.getId());
			return "<html><body>" + "<h1>" + vendingMachine.getName() + ",from "
					+ vendingMachine.getKoordinate() + ",price " + 
					vendingMachine.getPrice() + ",Success</h1>"+ "</body></html>";
			}
    
    @PutMapping("vendingMachine/update/{id}")
	public ResponseEntity<Object> updateItem(@PathVariable int id) {
		Optional<VendingMachine> vendingMachineRepo = Optional.ofNullable(repo.findById(id));

		if (!vendingMachineRepo.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		vendingMachineRepo.get().setEntity(vendingMachineRepo.get().getEntity() - 1);
		VendingMachine vendingmachine = vendingMachineRepo.get();
		repo.save(vendingmachine);
		return ResponseEntity.noContent().build();
	}
  
    @PostMapping("/vendingMachine")
    @ResponseStatus(HttpStatus.CREATED)
    public VendingMachine addVendingMachine(@RequestBody VendingMachine vendingMachine) {
        return repo.save(vendingMachine);
    }
  
    @DeleteMapping("/delete/{id}")
    public void deleteVendingMachine( @PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }
  
    @PutMapping("/VendingMachine/{id}")
    public ResponseEntity<Object> updateVendingMachine(@RequestBody VendingMachine vendingMachine, @PathVariable int id) {
  
        Optional<VendingMachine> vendingMachineRepo = Optional.ofNullable(repo.findById(id));
  
        if (!vendingMachineRepo.isPresent())
            return ResponseEntity.notFound().build();
  
        vendingMachine.setId(id);
        repo.save(vendingMachine);
        return ResponseEntity.noContent().build();
    }
}
