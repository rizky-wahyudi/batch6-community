package com.vm.vendingMachine;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendingMachineRepository extends CrudRepository<VendingMachine, Integer> {
    VendingMachine findById(int id);
//    VendingMachine findByKoordinate(String koordinate);
    List<VendingMachine> findAll();
    void deleteById(int id);
    

	VendingMachine save(VendingMachine vendingMachine);
	VendingMachine findByKoordinate(String koordinate);

}