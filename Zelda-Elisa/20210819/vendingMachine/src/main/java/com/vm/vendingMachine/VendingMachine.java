package com.vm.vendingMachine;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class VendingMachine {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
	
    String koordinate;
    String name;
    int price;
    int entity;

	public VendingMachine() {
    }
  
    // Parameterized constructor
    public VendingMachine(String koordinate, String name, int price) {
        this.koordinate = koordinate;
        this.name = name;
        this.price = price;
    }

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getKoordinate() {
		return koordinate;
	}

	public void setKoordinate(String koordinate) {
		this.koordinate = koordinate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getEntity() {
		return entity;
	}

	public void setEntity(int entity) {
		this.entity = entity;
		}



}