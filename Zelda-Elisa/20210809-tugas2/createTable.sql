create table perjalanan.bagasi(
id int not null auto_increment primary key,
berat int null);

create table perjalanan.dokumen(
id int not null auto_increment primary key,
imageIdCard varchar(225)null,
imagePaspor varchar(225)null,
imageVaccineCard varchar(225)null);

create table perjalanan.transport(
id int not null auto_increment primary key,
jenisKendaraan varchar(50)null);

create table perjalanan.lokasi(
id int not null auto_increment primary key,
namaLokasi varchar(225)null,
waktu datetime null,
street varchar(225)null,
longitude float(10,6) not null,
latitude float(10,6) not null,
city varchar(225) null,
distric varchar(225) null,
province varchar(225) null,
country varchar(225) null);

create table perjalanan.pelanggan(
id int not null auto_increment primary key,
nama varchar(225)null,
gender char(1)null,
birthDate date null,
idDokumen int null,
idBagasi int null,
foreign key (idDokumen) references dokumen(id),
foreign key (idBagasi) references bagasi(id));

create table perjalanan.tiket(
id int not null auto_increment primary key,
price int null,
idPelanggan int null,
idTransport int null,
idLokasiBerangkat int null,
idLokasiTujuan int null,
foreign key(idPelanggan) references pelanggan(id),
foreign key(idTransport) references transport(id),
foreign key(idLokasiBerangkat) references lokasi(id),
foreign key(idLokasiTujuan) references lokasi(id));
