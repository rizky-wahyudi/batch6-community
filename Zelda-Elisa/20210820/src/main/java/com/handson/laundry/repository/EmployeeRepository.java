package com.handson.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.handson.laundry.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
	Employee save(Employee employee);
	List<Employee> findAll();
	Employee findById(int id);
}
