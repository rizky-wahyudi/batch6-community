package com.handson.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.handson.laundry.model.Adress;
import com.handson.laundry.model.Customer;
import com.handson.laundry.model.Employee;
import com.handson.laundry.model.Order;
import com.handson.laundry.model.OrderItems;
import com.handson.laundry.model.Service;
import com.handson.laundry.repository.AdressRepository;
import com.handson.laundry.repository.CustomerRepository;
import com.handson.laundry.repository.EmployeeRepository;
import com.handson.laundry.repository.OrderItemsRepository;
import com.handson.laundry.repository.OrderRepository;
import com.handson.laundry.repository.ServiceRepository;

@RestController
public class Controller {
	@Autowired
	private AdressRepository repoadr;
	@Autowired
	private CustomerRepository repocus;
	@Autowired
	private EmployeeRepository repoemp;
	@Autowired
	private OrderRepository repoor;
	@Autowired
	private OrderItemsRepository repooi;
	@Autowired
	private ServiceRepository reposer;
	
	@GetMapping("/")
	public String welcome() {
		return "<html><body>"
				+ "<h1>WELCOME TO LAUNDRY</h1>"
	            + "</body></html>";
	}
	
	@GetMapping("/showAllAdress")
	public List<Adress> getAllAddress(){
		return repoadr.findAll();
	}
	
	@PostMapping("/addAdress")
	public Adress setAdress(@RequestBody Adress adress) {
		return repoadr.save(adress);
	}
	
	@GetMapping("/showAllCustomer")
	public List<Customer> getAllCustomer(){
		return repocus.findAll();
	}
	
	@PostMapping("/addCustomer/adress:{id}")
	public Customer setCustomer(@RequestBody Customer customer,@PathVariable(value="id")int id) {
		customer.setAdress(repoadr.findById(id));
		return repocus.save(customer);
	}
	
	@GetMapping("/showAllEmployee")
	public List<Employee> getAllEmployee(){
		return repoemp.findAll();
	}
	
	@PostMapping("/addEmployee/adress:{id}")
	public Employee setEmployee(@RequestBody Employee employee,@PathVariable(value="id") int id) {
		employee.setAdress(repoadr.findById(id));
		return repoemp.save(employee);
	}
	
	@GetMapping("/showOrder/{id}")
	public Order getOrder(@PathVariable int id) {
		return repoor.findById(id);
	}

	@GetMapping("/showAllOrder")
	public List<Order> getAllOrder() {
		return repoor.findAll();
	}
	
	@PostMapping("/addOrder")
	public Order setOrder(@RequestBody Order order) {
		return repoor.save(order);
	}

	@PostMapping(value = "/addOrder2/employee:{id}", consumes = "application/json")
	public String addCountry(@RequestBody Order order,@PathVariable(value="id")int id) {
		for (OrderItems oi : order.getOrderItem()) {
			repooi.save(oi);
		}
		order.setStatus(String.valueOf(id));
		repoor.save(order);
		return "Success";
	}
	
	@GetMapping("/showAllOrderItems")
	public List<OrderItems> getAllOrderItems(){
		return repooi.findAll();
	}
	
	@PostMapping("/addOrderItems/service:{id}")
	public OrderItems setOrderItems(@RequestBody OrderItems orderItems,@PathVariable(value="id")int id) {
		orderItems.setService(reposer.findById(id));
		return repooi.save(orderItems);
	}
	
	@GetMapping("/showAllService")
	public List<Service> getAllService() {
		return reposer.findAll();
	}

	@PostMapping("/addService")
	public Service setService(@RequestBody Service service) {
		return reposer.save(service);
	}

	
}
