package com.handson.laundry.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String name;
    @JsonFormat(pattern = "yyyy-MM-dd")
    Date birthDate;
    String phone;
    String email;
    
    @OneToOne(targetEntity = Adress.class)
	private Adress adress;
    
    @OneToMany(targetEntity=Order.class)
	private List<Order>order;
    
    public Employee() {
		
	}
	
    public Employee(String name, Date birthDate,String phone, String email,List<Order>order) {
		this.name = name;
		this.birthDate = birthDate;
		this.phone = phone;
		this.email = email;
		this.order=order;
	}


    
    
    
    public Adress getAdress() {
		return adress;
	}
	public void setAdress(Adress adress) {
		this.adress = adress;
	}
	public List<Order> getOrder() {
		return order;
	}
	public void setOrder(List<Order> order) {
		this.order = order;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthDate = birthdate;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}