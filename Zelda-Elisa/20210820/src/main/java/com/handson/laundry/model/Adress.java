package com.handson.laundry.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Adress {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String street;
    String district;
    String city;
    String province;
    String country;
    Float langitude;
    Float latitude;
    
    public Adress() {

	}

	public Adress(String street, String district, String city, String country, float langitude,float latitude) {
		this.street = street;
		this.district = district;
		this.city = city;
		this.country = country;
		this.langitude = langitude;
		this.latitude = latitude;
	}

    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Float getLangitude() {
		return langitude;
	}
	public void setLangitude(Float langitude) {
		this.langitude = langitude;
	}
	public Float getLatitude() {
		return latitude;
	}
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
}