package com.handson.laundry.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    int id;
	
	@OneToMany(targetEntity = OrderItems.class)
	private List<OrderItems> orderitem;

	@JsonFormat(pattern = "yyyy-MM-dd")
    Date orderDate;
    String status;
    String paymentStatus;
    
    @JsonFormat(pattern = "yyyy-MM-dd")
    Date lastUpdateDate;
    String updateBy;
    
    public Order() {

	}

	public Order(List<OrderItems> orderitem, Date orderDate, String paymentStatus, Date lastUpdateDate) {
		this.orderitem = orderitem;
		this.orderDate = orderDate;
		this.paymentStatus = paymentStatus;
		this.lastUpdateDate = lastUpdateDate;
	}
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<OrderItems> getOrderItem() {
		return orderitem;
	}
	public void setOrderItem(List<OrderItems> orderitem) {
		this.orderitem = orderitem;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
    
    
}