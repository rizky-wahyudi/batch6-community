package com.handson.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.handson.laundry.model.OrderItems;

@Repository
public interface OrderItemsRepository extends CrudRepository<OrderItems, Integer> {
	OrderItems save(OrderItems orderItems);
	List<OrderItems> findAll();
	OrderItems findById(int id);
}
