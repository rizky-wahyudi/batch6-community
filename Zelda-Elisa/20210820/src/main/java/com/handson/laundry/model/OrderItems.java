package com.handson.laundry.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class OrderItems {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String itemName;
    int weight;
    double amount;
    
    @OneToOne(targetEntity = Service.class)
	private Service service;
    
    public OrderItems() {
		
	}
	
    public OrderItems(String itemName, int weight, int amount) {
		this.itemName = itemName;
		this.weight = weight;
		this.amount = amount;
    }


	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
