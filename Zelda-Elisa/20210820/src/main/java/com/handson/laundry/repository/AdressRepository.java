package com.handson.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.handson.laundry.model.Adress;

@Repository
public interface AdressRepository extends CrudRepository<Adress, Integer> {
	Adress save(Adress adress);
	List<Adress> findAll();
	Adress findById(int id);
}

