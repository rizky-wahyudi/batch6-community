import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Cart cart = new Cart();

        Product i1 = new Product(1, "Spicy huha ", 30000, 0, "08/07/2022" , 0, 1,"makanan");
        Product i2 = new Product(2, "Say Cheese ", 20000, 0, "10/05/2023", 0, 1, "makanan");
        Product i3 = new Product(3, "mineral water ", 5000, 0, "08/07/2022" , 0, 1, "minuman");
        Product i4 = new Product(4, "matemaTeaka ", 20000, 0, "07/05/2023", 0, 1,"minuman");
        Product i5 = new Product(5, "Shampoo ", 15000, 0, "08/09/2022" , 0, 1, "sabun");
        Product i6 = new Product(6, "Soap ", 10000, 0, "10/10/2023", 0, 1,"sabun");

        System.out.println("Selamat datang di Menantea Market");
        System.out.println("1. spicy huha (frozen karaage)");
        System.out.println("2. say cheese (frozen sausage)");
        System.out.println("3. mineral water");
        System.out.println("4. matemateaka (tea)");
        System.out.println("5. sham foo (shampo)");
        System.out.println("6. so upp (soap)");

        boolean running = true;
        while (running){

        System.out.print("ketik 0 untuk menyelesaikan belanja");    
        System.out.print("Silahkan pilih produk yang anda inginkan (1-6)= ");
        int addcart = scanner.nextInt();

        if(addcart==1){
            cart.addToCart(i1);
            cart.showCart();
        }
        else if(addcart==2){
            cart.addToCart(i2);
            cart.showCart();
        }
        else if(addcart==3){
            cart.addToCart(i3);
            cart.showCart();
        }
        else if(addcart==4){
            cart.addToCart(i4);
            cart.showCart();
        }
        else if(addcart==5){
            cart.addToCart(i5);
            cart.showCart();
        }
        else if(addcart==6){
            cart.addToCart(i6);
            cart.showCart();
        }
        else if(addcart==0){
            System.out.println("YOUR CART: ");
            cart.showCart();
            running = false;

            System.out.println("Total yang harus dibayarkan = ");
            cart.printInvoice();
            System.out.println("\t\t\t" + "Status   :  Unpaid");
            System.out.println("=============================");
            System.out.print("bayar sekarang? (ya/tidak) = ");

            Scanner scan = new Scanner(System.in);
            String answer = scan.nextLine();
            
            if (answer.equals("ya")){
                System.out.print("masukkan uang sejumlah " + cart.getPayableAmount()+" = ");
                Scanner sc = new Scanner(System.in);
                int paid = sc.nextInt();
                
                if(paid>=cart.getPayableAmount()){
                    cart.printInvoiceOrder();
                    System.out.println("\t\t\t" + "Cash     : " + paid);
                    System.out.println("\t\t\t" + "Change   : " + (paid - cart.getPayableAmount()));
                    System.out.println("");
                    System.out.print("mau pesan lagi? (ya/tidak) =");
                    Scanner scann= new Scanner(System.in);
                    String answ = scann.nextLine();
                    if(answ.equals("ya")){
                        cart.removeFromCart(i1);
                        cart.removeFromCart(i2);
                        cart.removeFromCart(i3);
                        cart.removeFromCart(i4);
                        cart.removeFromCart(i5);
                        cart.removeFromCart(i6);
                        running = true;
                    }
                    else if(answ.equals("tidak")){
                        System.exit(0);
                    }
                    else{
                        System.out.println("pilihan tidak tersedia");
                        System.exit(0);
                    }
                    
                }
                else if(paid<=cart.getPayableAmount()){
                    System.out.println("uang anda kurang, pesanan dibatalkan, uang kembali = "+paid);
                    System.out.print("mau pesan lagi? (ya/tidak) =");
                    Scanner s = new Scanner(System.in);
                    String ans = s.nextLine();
                    if(ans.equals("ya")){
                        cart.removeFromCart(i1);
                        cart.removeFromCart(i2);
                        cart.removeFromCart(i3);
                        cart.removeFromCart(i4);
                        cart.removeFromCart(i5);
                        cart.removeFromCart(i6);
                        running = true;
                    }
                    else if(ans.equals("tidak")){
                        running = false;
                        System.exit(0);
                    }
                    else{
                        System.out.println("pilihan tidak tersedia");
                        System.exit(0);
                    }
                }
            
            } else if(answer.equals("tidak")){
                running = true;
            }
            else{
                System.out.println("pilihan tidak tersedia");
                System.exit(0);
            }
        }
        else{
            System.out.println("pilihan tidak tersedia");
        }

    }

}

}
