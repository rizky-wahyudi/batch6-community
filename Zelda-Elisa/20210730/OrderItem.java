public class OrderItem {
    private int orderId;
    private double amount;
    
    public OrderItem(int orderId, double amount) {
       this.orderId = orderId;
       this.amount = amount;
    }
    public int getOrderId() {
        return orderId;
    }
    public double getAmount() {
        return amount;
    }
}
