
public class Product {
    private int articleNumber;
    private String description;
    private double purchasePrice;
    private double sellPrice = purchasePrice+purchasePrice*10/100;
    private String expiredDate;
    private double tax;
    private String jenis;
    private double quantity;

    public Product(int articleNumber, String description, int purchasePrice,
     int sellPrice, String expiredDate, int tax, double quantity, String jenis) {
        this.articleNumber = articleNumber;
        this.description = description;
        this.purchasePrice = purchasePrice;
        this.sellPrice= sellPrice;
        this.expiredDate= expiredDate;
        this.tax = tax;
        this.quantity = quantity;
        this.jenis = jenis;
    }

	public String toString() {
		String s = this.description + ": ";
		s = s + this.quantity + "\n";
		return s;
	}

    public int getArticleNumber() {
        return articleNumber;
    }
    public double getPurchasePrice() {
        return purchasePrice;
    }
    public double getSellPrice() {
        return sellPrice;
    }
    public String getJenis() {
        return jenis;
    }
    public double getTax() {
        if (getJenis().equalsIgnoreCase("makanan")) {
            return 0.05;
        } else if (getJenis().equalsIgnoreCase("minuman")) {
            return 0.01;
        } else if (getJenis().equalsIgnoreCase("sabun")) {
            return 0.7;
        } else {
            return 0;
        }
    }
    public double getQuantity() {
        return quantity;
    }
    public String getDescription() {
        return description;
    }
    public String getExpiredDate() {
        return expiredDate;
    }

}
