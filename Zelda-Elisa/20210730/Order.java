import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Order {
	ArrayList<Product> product;
    private String orderDate;
    private int cashier;
    private int orderNumber;
    private int branchId;
    private String status;
    private String paymentStatus;

    public Order(String orderDate, int cashier, int orderNumber, int branchId, int totalPrice, String status,
			String paymentStatus) {
       this.orderDate = orderDate;
       this.orderNumber = orderNumber;
       this.branchId = branchId;
       this.cashier = cashier;
       this.status = status;
       this.paymentStatus = paymentStatus;
    }

	LocalDateTime myDateObj = LocalDateTime.now();
    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("E, MMM dd HH:mm:ss yyyy");
    String formattedDate = myDateObj.format(myFormatObj);

	Order() {
		this.product = new ArrayList<Product>();
		this.orderDate = (formattedDate);
		this.orderNumber = 01;
		this.branchId = 01;
		this.cashier = 01;
		this.status = "ordered";
		this.paymentStatus = "paid";
	}
    
	public String getOrderDate() {
		this.orderDate = (formattedDate);
		return orderDate;
	}
	public int getCashier() {
		this.orderNumber = 1;
		return cashier;
	}
	public int getOrderNumber() {
		this.branchId = 1;
		return orderNumber;
	}
	public int getBranchId() {
		this.branchId = 01;
		return branchId;
	}
	public String getStatus() {
		this.paymentStatus = "ordered";
		return status;
	}
	public String getPaymentStatus() {
		this.paymentStatus = "paid";
		return paymentStatus;
	}


}