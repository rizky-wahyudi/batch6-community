import java.util.ArrayList;
import java.util.ListIterator;
class Cart {
	ArrayList<Product> product;
	double totalAmount;
	double payableAmount;
	double discount;
	double tax;

	Order order = new Order();

	Cart() {
		this.product = new ArrayList<Product>();
		this.totalAmount = 0;
		this.payableAmount = 0;
		this.discount = 0;
		this.tax = 0;
	}

	public void addToCart(Product product) {
		this.product.add(product);
	}

	public void showCart() {
		ListIterator<Product> iterator = product.listIterator();
		while(iterator.hasNext()) {
			Product item1 = iterator.next();
			System.out.println(item1);
		}
	}

	public void removeFromCart(Product i) {
		ListIterator<Product> iterator1 = product.listIterator();
		while(iterator1.hasNext()) {
			Product item2 = iterator1.next();
			if (item2.getDescription().equals(i.getDescription())) {
				this.product.remove(i);
				break;
			}
		}
	}

	public double getTotalAmount() {
		ListIterator<Product> iterator2 = product.listIterator();
		this.totalAmount = 0;
		while(iterator2.hasNext()) {
			Product item3 = iterator2.next();
			this.totalAmount = this.totalAmount + (item3.getPurchasePrice() * item3.getQuantity());
		}
		return this.totalAmount;
	}

	public double getPayableAmount() {
		this.payableAmount = 0;
		this.tax = this.totalAmount * (0.1);
		this.payableAmount = this.totalAmount + this.tax;
		return this.payableAmount;
	}

	public void printInvoice() {
		ListIterator<Product> iterator3 = product.listIterator();
		while(iterator3.hasNext()) {
			Product item4 = iterator3.next();
			System.out.print(item4.getDescription() + "\t");
			System.out.print(item4.getQuantity() + "\t");
			System.out.print(item4.getPurchasePrice() + "\t");
			System.out.println(item4.getSellPrice() * item4.getQuantity());
		}
		System.out.println("\t\t\t" + "Total    : " + this.getTotalAmount());
		System.out.println("\t\t\t" + "Discount : " + this.discount);
		this.getPayableAmount();
		System.out.println("\t\t\t" + "Tax      : " + this.tax);
		System.out.println("\t\t\t" + "Total    : " + this.getPayableAmount());
		
		
	}

	public void printInvoiceOrder() {
		ListIterator<Product> iterator3 = product.listIterator();
		while(iterator3.hasNext()) {
			Product item4 = iterator3.next();
			System.out.print(item4.getDescription() + "\t");
			System.out.print(item4.getQuantity() + "\t");
			System.out.print(item4.getPurchasePrice() + "\t");
			System.out.println(item4.getSellPrice() * item4.getQuantity());
		}
		System.out.println("\t\t\t" + "Date     : " + order.getOrderDate());
		System.out.println("\t\t\t" + "BranchId : " + order.getBranchId());
		System.out.println("\t\t\t" + "Order no.: " + order.getOrderNumber());
		System.out.println("\t\t\t" + "Cashier  : " + order.getCashier());
		System.out.println("\t\t\t" + "Status   : " + order.getStatus());
		System.out.println("\t\t\t" + "PaymentS : " + order.getPaymentStatus());
		System.out.println("\t\t\t" + "Total    : " + this.getTotalAmount());
		System.out.println("\t\t\t" + "Discount : " + this.discount);
		this.getPayableAmount();
		System.out.println("\t\t\t" + "Tax      : " + this.tax);
		System.out.println("\t\t\t" + "Total    : " + this.getPayableAmount());
}
}