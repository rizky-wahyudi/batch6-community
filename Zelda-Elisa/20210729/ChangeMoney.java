import java.util.*;
import java.text.DecimalFormat;

public class ChangeMoney {
    public static void main(String[] args) {
     
		DecimalFormat f = new DecimalFormat("##.##");
 
		Scanner sc = new Scanner(System.in);
        Valuta val = new Valuta();
 
		System.out.println("hi, Welcome to Money Changer!");
 
		System.out.println("pilih mata uang yang akan ditukar (1/2/3)");

		System.out.println("1.Rupiah");
        System.out.println("2.Dollar");
        System.out.println("3.Euro");
		val.code = sc.nextInt();
		
		System.out.print("Masukkan uang anda = ");
		val.amount = sc.nextFloat();
 
		if (val.code == 1) {
            System.out.println("1.Dollar");
            System.out.println("2.Euro");
            System.out.println("ingin menukar dengan mata uang apa(1/2)");
            val.change = sc.nextFloat();

            if(val.change == 1){
                val.dollar = val.amount / val.rpUsd;
                System.out.println("Rp." + val.amount + " Bernilai : $" + f.format(val.dollar));
                System.out.println("Silahkan ambil uang senilai $"+ f.format(val.dollar));
            }
            else if(val.change == 2){
                val.euro = val.amount / val.rpEuro;
                System.out.println("Rp." + val.amount + " Bernilai : E" + f.format(val.euro));
                System.out.println("Silahkan ambil uang senilai E"+ f.format(val.euro));
            }
            else {
                System.out.println("Invalid input");
            }
    
        } else if (val.code == 2) {
            System.out.println("1.Rupiahh");
            System.out.println("2.Euro");
            System.out.println("ingin menukar dengan mata uang apa(1/2)");
            val.change = sc.nextFloat();

            if(val.change == 1){
                val.rupiah = val.amount * val.rpUsd;
                System.out.println("$" + val.amount + " Bernilai : Rp." + f.format(val.rupiah));
                System.out.println("Silahkan ambil uang senilai Rp"+ f.format(val.rupiah));
            }
            else if(val.change == 2){
                val.euro = val.amount * val.usdEuro;
                System.out.println("$" + val.amount + " Bernilai : E" + f.format(val.euro));
                System.out.println("Silahkan ambil uang senilai E"+ f.format(val.euro));
            }
            else {
                System.out.println("Invalid input");
            }
    
        } else if (val.code == 3) {
            System.out.println("1.Rupiahh");
            System.out.println("2.Dollar");
            System.out.println("ingin menukar dengan mata uang apa(1/2)");
            val.change = sc.nextFloat();

            if(val.change == 1){
                val.rupiah = val.amount * val.rpEuro;
                System.out.println("E" + val.amount + " Bernilai : Rp" + f.format(val.rupiah));
                System.out.println("Silahkan ambil uang senilai Rp"+ f.format(val.rupiah));
            }
            else if(val.change == 2){
                val.dollar = val.amount * val.euroUsd;
                System.out.println("E" + val.amount + " Bernilai : $" + f.format(val.dollar));
                System.out.println("Silahkan ambil uang senilai $"+ f.format(val.dollar));
            }
            else {
                System.out.println("Invalid input");
            }
    
        }  else {
			System.out.println("Invalid input");
		}
		
}
}
