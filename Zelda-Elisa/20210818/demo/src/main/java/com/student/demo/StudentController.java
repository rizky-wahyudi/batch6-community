package com.student.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

@RestController
public class StudentController {
    @Autowired
    private StudentRepository repo;
  
    // Home Page
    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
            + "<h1>WELCOME</h1>"
            + "</body></html>";
    }
  
    // Get All Notes
    @GetMapping("/student")
    public List<Student> getAllNotes() {
        return repo.findAll();
    }
  
    // Get the company details by
    // ID
    @GetMapping("/student/{id}")
    public Student getCompanyById(@PathVariable(value = "id") int id) {
        return repo.findById(id);
    }
  
    @PostMapping("/student")
    @ResponseStatus(HttpStatus.CREATED)
    public Student addStudent(@RequestBody Student student) {
        return repo.save(student);
    }
  
    @DeleteMapping("/delete/{id}")
    public void deleteStudent( @PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }
  
    @PutMapping("/Student/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Student student, @PathVariable int id) {
  
        Optional<Student> studentRepo = Optional.ofNullable(repo.findById(id));
  
        if (!studentRepo.isPresent())
            return ResponseEntity.notFound().build();
  
        student.setId(id);
        repo.save(student);
        return ResponseEntity.noContent().build();
    }
}