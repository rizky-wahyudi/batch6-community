import React from "react"


class Table extends React.Component {
  constructor(props) {
     super(props)
     this.state = {
        students: [
           { id: 1, name: 'Jerome', age: 23, email: 'jeromeepolin@email.com' },
           { id: 2, name: 'Yusuke', age: 22, email: 'yusuke@email.com' },
           { id: 3, name: 'Tomohiro', age: 22, email: 'tomohiro@email.com' },
           { id: 4, name: 'Otsuka Ryoma', age: 22, email: 'otsukarym@email.com' }
        ]
     }
  }

  renderTableHeader() {
     let header = Object.keys(this.state.students[0])
     return header.map((key, index) => {
        return <th key={index}>{key.toUpperCase()}</th>
     })
  }

  renderTableData() {
     return this.state.students.map((student, index) => {
        const { id, name, age, email } = student //destructuring
        return (
           <tr key={id}>
              <td>{id}</td>
              <td>{name}</td>
              <td>{age}</td>
              <td>{email}</td>
           </tr>
        )
     })
  }

  render() {
     return (
        <div>
           <h1 id='title'>Handson React Table</h1>
           <table id='students'>
              <tbody>
                 <tr>{this.renderTableHeader()}</tr>
                 {this.renderTableData()}
              </tbody>
           </table>
        </div>
     )
  }
}

export default Table