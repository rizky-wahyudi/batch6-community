/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task1;

/**
 *
 * @author User
 */
public class Calculator {

  public static int add(int numOne, int numTwo) {
    return numOne+numTwo;
  }

  public static int subtract(int numOne, int numTwo) {
    return numOne-numTwo;
  }

  public static int multiply(int numOne, int numTwo) {
    return numOne*numTwo;
  }

  public static int divide(int numOne, int numTwo) {
    return numOne/numTwo;
  }

  public static void main(String[] args) {
    int numOne = Integer.parseInt(args[0]);
    int numTwo = Integer.parseInt(args[1]);
    add(numOne, numTwo);
    subtract(numOne, numTwo);
    multiply(numOne, numTwo);
    divide(numOne, numTwo);
  }
}