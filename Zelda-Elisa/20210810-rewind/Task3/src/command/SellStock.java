/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author User
 */
public class SellStock implements TicketOrder {
   private Stock ticStock;

   public SellStock(Stock ticStock){
      this.ticStock = ticStock;
   }

   public void execute() {
      ticStock.sell();
   }
}
