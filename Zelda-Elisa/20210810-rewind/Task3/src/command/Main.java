/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author User
 */
public class Main {
   public static void main(String[] args) {
      Stock ticStock = new Stock();

      BuyStock buyStockOrder = new BuyStock(ticStock);
      SellStock sellStockOrder = new SellStock(ticStock);

      Broker broker = new Broker();
      broker.takeOrder(buyStockOrder);
      broker.takeOrder(sellStockOrder);

      broker.placeOrders();
   }
}
