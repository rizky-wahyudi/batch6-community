/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author User
 */
public class Stock {
   private String name1 = "EXO CONCERT";
   private String name2 = "FIERSA BESARI CONCERT";
   private int quantity1 = 100;
   private int quantity2 = 100;

   public void buy(){
      System.out.println("Stock [ Name: "+name1+", Quantity: " + quantity1 +" ] bought");
   }
   public void sell(){
      System.out.println("Stock [ Name: "+name2+", Quantity: " + quantity2 +" ] sold");
   }
}
