/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author User
 */
import java.util.ArrayList;
import java.util.List;

   public class Broker {
   private List<TicketOrder> orderList = new ArrayList<TicketOrder>(); 

   public void takeOrder(TicketOrder order){
      orderList.add(order);		
   }

   public void placeOrders(){
   
      for (TicketOrder order : orderList) {
         order.execute();
      }
      orderList.clear();
   }
}
