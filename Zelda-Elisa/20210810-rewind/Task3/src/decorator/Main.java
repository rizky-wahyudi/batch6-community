/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author User
 */
public class Main {

	public static void main(String[] args) {
		Wear SpringWear = new SpringWear(new BasicWear());
		SpringWear.assemble();
		System.out.println("\n*****");
		
		Wear WinterWear = new WinterWear(new BasicWear());
		WinterWear.assemble();
	}

}
