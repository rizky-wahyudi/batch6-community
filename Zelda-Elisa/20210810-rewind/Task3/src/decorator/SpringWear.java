/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author User
 */public class SpringWear extends WearDecorator {

	public SpringWear(Wear w) {
		super(w);
	}
	
	@Override
	public void assemble(){
		super.assemble();
		System.out.print(" new arrival Spring wear.");
	}
}
