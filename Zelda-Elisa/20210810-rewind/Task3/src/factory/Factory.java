/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

/**
 *
 * @author User
 */
public class Factory {
    public Family getFamily(String famAct){
      if(famAct == null){
         return null;
      }		
      if(famAct.equalsIgnoreCase("Dad")){
         return new Dad();
         
      } else if(famAct.equalsIgnoreCase("Mom")){
         return new Mom();
         
      } else if(famAct.equalsIgnoreCase("Child")){
         return new Child();
      }
      
      return null;
   }
}
