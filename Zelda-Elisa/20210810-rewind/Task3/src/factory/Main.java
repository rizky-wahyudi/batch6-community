/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

/**
 *
 * @author User
 */
public class Main {

   public static void main(String[] args) {
      Factory factory = new Factory();

      Family fam1 = factory.getFamily("Dad");
      fam1.activity();

      Family fam2 = factory.getFamily("Mom");
      fam2.activity();

      Family fam3 = factory.getFamily ("Child");
      fam3.activity();
   }
}
