/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

/**
 *
 * @author User
 */
public class Member implements Observer {

    protected String name;
    public Member(String name) {
        super();
        this.name = name;
    }

    @Override
    public void notification(String handle, String message) {
        System.out.printf("'%s' received notification from Handle: '%s', Message: '%s'\n", name, handle, message);
    }

}
