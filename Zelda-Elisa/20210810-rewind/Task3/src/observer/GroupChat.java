/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

/**
 *
 * @author User
 */
import java.util.ArrayList;

import java.util.List;

public class GroupChat implements Subject {

    protected List<Observer> observers = new ArrayList<Observer>();
    protected String name;
    protected String handle;

    public GroupChat(String name, String handle) {
        super();
        this.name = name;
        this.handle = "#" + handle;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getHandle() {
        return handle;
    }
    public void message(String message) {
        System.out.printf("\nName: %s, Message: %s\n", name, message);
        notifyMember(message);
    }
    @Override
    public synchronized void addMember(Observer observer) {
        observers.add(observer);
    }
    @Override
    public synchronized void removeMember(Observer observer) {

        observers.remove(observer);

    }

    @Override
    public void notifyMember(String message) {
        observers.forEach(observer -> observer.notification(handle, message));
    }

}
