/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

/**
 *
 * @author User
 */

public interface Subject {

    public void addMember(Observer observer);
    public void removeMember(Observer observer);
    public void notifyMember(String message);

}
