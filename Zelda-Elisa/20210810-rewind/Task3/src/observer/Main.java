/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

/**
 *
 * @author User
 */
public class Main {

    public static void main(String args[]) {
        GroupChat wasedaBoys = new GroupChat("Jerome Polin", "wasedaBoys");

        Member yusuke = new Member ("Yusuke");
        Member  otsuka = new Member ("Otsuka");
        Member  tomohiro = new Member ("Tomohiro");
        wasedaBoys.addMember(yusuke);
        wasedaBoys.addMember(otsuka);
        wasedaBoys.addMember(tomohiro);

        wasedaBoys.message("Halo guys, hariini bikin video yuk!");
        wasedaBoys.removeMember(tomohiro);
        wasedaBoys.message("gausah ajak tomo");
    }
}
