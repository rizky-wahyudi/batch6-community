create database Stock_Management;

use Stock_Management;

create table productImg(
id int not null auto_increment,
url varchar(225) null,
description varchar(225) null,
primary key(id));

create table product(
id int not null auto_increment,
artNumber int null,
productImgId int null,
sellPrice int null,
expiredDate date null,
primary key(id),
foreign key (productImgId) references product(id));

create table adress(
id int not null auto_increment,
street varchar(225) null,
longitude float(10,6) not null,
latitude float(10,6) not null,
city varchar(225) null,
distric varchar(225) null,
province varchar(225) null,
country varchar(225) null,
primary key (id));

create table distributor(
id int not null auto_increment,
name varchar(225) null,
adressId int,
adressType varchar(225) null,
primary key(id),
foreign key(adressId) references adress(id));

create table productDistributor(
id int not null auto_increment,
distributorId int,
buyPrice int,
stock int,
primary key(id),
foreign key(distributorId) references distributor(id));

create table productStock(
id int not null auto_increment,
productId int,
stock int,
productDistributorId int,
purchase date,
delivery date,
primary key(id),
foreign key (productId) references product(id),
foreign key (productDistributorId) references productDistributor(id));

insert into productImg values('1','https://cdn1.productnation.co/stg/sites/5/5cb5498b465f2.jpeg','kimchi'),
('2','https://s1.bukalapak.com/img/63570979912/s-360-360/155cdb7a89accdb7031999302e4c2bde0_nongshim_tako_chips_snack_.png','taco chips'),
('3','https://blogunik.com/wp-content/uploads/2019/03/Choco-Pie-e1553495229191-1280x720.jpg','choco pie'),
('4','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYtHZ4kU6zetdFVgmUXdsh78reFfHfzm2Hgg&usqp=CAU','instant tteok'),
('5','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFmtcSjSzVlmf5XtAG9tuWGfzgLD5Cud9CWQ&usqp=CAU','shin ramyun'),
('6','https://images-na.ssl-images-amazon.com/images/I/91v-8RK1EaL._SL1500_.jpg','samyang'),
('7','https://media.karousell.com/media/photos/products/2020/11/8/odengkorean_foodkofuinstant_fo_1604818391_073c8fc0_progressive.jpg','odeng'),
('8','https://cf.shopee.co.id/file/318cf738a546afda1e33c46ba875566f','milkis'),
('9','https://id-test-11.slatic.net/p/8cd9c7c7e93ea79df8925379fe5338bc.jpg_720x720q80.jpg_.webp','red gingseng'),
('10','https://asset.kompas.com/crops/O32r2jy2KSWSOKg5A9dT-gzbsoE=/0x66:1080x786/750x500/data/photo/2020/08/23/5f425302c63c3.jpg','soju');

insert into product values
('1','1','1','20000','2023-09-02'),
('2','2','2','30000','2022-03-01'),
('3','3','3','15000','2024-05-06'),
('4','4','4','45000','2023-10-06'),
('5','5','5','10000','2022-11-10'),
('6','6','6','20000','2023-10-08'),
('7','7','7','50000','2024-03-06'),
('8','8','8','15000','2023-06-11'),
('9','9','9','80000','2023-10-06'),
('10','10','10','100000','2022-12-07');

insert into adress values
('1','Cheonggyecheon-ro',-37.570097,127.001245,'Myeong-dong','Gangbukgu','Seoul','Korea Selatan'),
('2','1190 Phahonyothin Rd',-13.818129,100.563050,'Chom Phon','Chatuchak','Bangkok','Thailand'),
('3','Jl.Wijaya II',-6.254128,106.800563,'Jakarta Selatan','Kebayoran Baru','DKI Jakarta','Indonesia');

insert into distributor values
('1','Namdaemun Market','1','center'),
('2','HyperMarket Tesco Lotus','2','branch'),
('3','Wijaya K-Market','3','branch');

insert into productDistributor values
('1','1','19000','10'),
('2','1','25000','0'),
('3','1','12000','10'),
('4','1','40000','5'),
('5','2','9000','10'),
('6','2','15000','5'),
('7','2','45000','0'),
('8','3','13000','5'),
('9','3','78000','10'),
('10','3','99000','0');

insert into productstock values
('1','1','10','1','2021-04-06','2021-07-07'),
('2','2','0','1','2021-03-10','2021-04-11'),
('3','3','10','1','2021-07-06','2021-07-10'),
('4','4','5','1','2021-06-06','2021-07-17'),
('5','5','10','2','2021-05-06','2021-06-10'),
('6','6','5','2','2021-02-09','2021-04-26'),
('7','7','0','2','2021-06-08','2021-07-16'),
('8','8','5','3','2021-07-26','2021-07-30'),
('9','9','10','3','2021-03-06','2021-05-10'),
('10','10','0','3','2021-07-18','2021-07-29');

select p.id,p.artNumber,p.productimgId,p.sellPrice,
p.expiredDate,ps.stock,ps.productDistributorId,ps.purchase,ps.delivery,
d.id,d.name,d.adressId,d.adresstype
from productStock ps
inner join product p on ps.productId=p.id
inner join productImg pi on p.productImgId=pi.id
inner join productDistributor pd on ps.productDistributorId=pd.id
inner join distributor d on pd.distributorId=d.id
inner join adress a on d.adressId=a.id
where ps.stock=0;

select*
from productStock ps
left join product p on ps.productId=p.id
left join productImg pi on p.productImgId=pi.id
left join productDistributor pd on ps.productDistributorId=pd.id
left join distributor d on pd.distributorId=d.id
left join adress a on d.adressId=a.id
where ps.stock=0;
