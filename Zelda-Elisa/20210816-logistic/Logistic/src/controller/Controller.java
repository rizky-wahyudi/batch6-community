package controller;
import java.util.ArrayList;
import java.util.List;

import model.Adress;
import model.DistanceMapping;
import model.Product;
import model.Transport;
import model.Warehouse;

public class Controller {
	private List <Adress> adress = new ArrayList<>();
    Adress a1 = new Adress(1, "Jl kosambi", "Duri", "DKI Jakarta", "Indonesia", (float) -6.177771, (float) 106.802762);
 	Adress a2 = new Adress(2, "Jl Daan mogot", "Cengkareng", "DKI Jakarta", "Indonesia", (float) -6.154380, (float) 106.710534);
 	
 	private List <DistanceMapping> dm = new ArrayList<>();
 	DistanceMapping dm1 = new DistanceMapping(1, 10, 1, 2);
 	
 	private List <Product> product = new ArrayList<>();
 	Product p1 = new Product(1, "baju", a1.getId(), 10, 20, 10, 30);
 	
 	private List <Transport> transport = new ArrayList<>();
 	Transport t1 = new Transport(1, "", 2, 80, "motorcycle", 50);
 	Transport t2 = new Transport(2, "", 5, 140, "car", 2);
 	Transport t3 = new Transport(3, "", 2, 120, "truck", 10);
 	
 	private List <Warehouse> wh = new ArrayList<>();
 	Warehouse w1 = new Warehouse(1, 1, 1);
 	Warehouse w2 = new Warehouse(2, 2, 2);
 	
 	public Controller() {
 		adress.add(a1);
 		adress.add(a2);
 		dm.add(dm1);
 		product.add(p1);
 		transport.add(t1);
 		transport.add(t2);
 		transport.add(t3);
 		wh.add(w1);
 		wh.add(w2);
 	}
 	 
 	public List<Adress> getAdress(){
 		return adress;
 	}
 	
 	public List<DistanceMapping> getDistanceMapping(){
 		return dm;
 	}
 	
 	public List<Product> getProduct(){
 		return product;
 	}
 	
 	public List<Transport> getTransport(){
 		return transport;
 	}
 	
 	public List<Warehouse> getWarehouse(){
 		return wh;
 	}
 	
 	public void showProduct() {
 		System.out.println ("Name of Product   : " +p1.getName());
 		System.out.println ("Id Deliver Adress : " +p1.getDeliverAdress());
 		System.out.println ("Width             : " +p1.getWidth());
 		System.out.println ("Height            : " +p1.getHeight());
 		System.out.println ("Depth             : " +p1.getDepth());
 		System.out.println ("Weigth            : " +p1.getWeight());
 	}
 	
 	public void showFrom() {
 		System.out.println ("Street            : " +a1.getStreet());
 		System.out.println ("Distric           : " +a1.getDistric());
 		System.out.println ("Province          : " +a1.getProvince());
 		System.out.println ("Country           : " +a1.getCountry());
 		System.out.println ("Langitude         : " +a1.getLangitude());
 		System.out.println ("Latitude          : " +a1.getLatitude());
 	}
 	
 	public void showTo() {
 		System.out.println ("Street            : " +a2.getStreet());
 		System.out.println ("Distric           : " +a2.getDistric());
 		System.out.println ("Province          : " +a2.getProvince());
 		System.out.println ("Country           : " +a2.getCountry());
 		System.out.println ("Langitude         : " +a2.getLangitude());
 		System.out.println ("Latitude          : " +a2.getLatitude());
 	}
 	
 	public void showDistance() {
 		System.out.println ("From WH Id         : " +dm1.getFromWarehouseId());
 		System.out.println ("To WH Id           : " +dm1.getToWarehouseId());
 		System.out.println ("Total Distance     : " +dm1.getDistance()+ " Km");
 	}
 	
 	public void showTransport() {
 		System.out.println ("Size               : " +t2.getSize());
 		System.out.println ("Speed              : " +t2.getSpeed()+ " Km/hour");
 		System.out.println ("Type               : " +t2.getType());
 		System.out.println ("Load               : " +t2.getLoad()+ " Ton");
 	}
 	
}
