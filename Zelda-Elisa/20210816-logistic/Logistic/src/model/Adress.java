package model;

public class Adress {
	private int id;
	private String street,distric,province,country;
	private float langitude,latitude;
	
	public Adress(int id, String street, String distric, String province,
			String country, float langitude,float latitude) {
		this.id=id;
		this.street=street;
		this.distric=distric;
		this.province=province;
		this.country=country;
		this.langitude=langitude;
		this.latitude=latitude;
	}

	public int getId() {
		return id;
	}

	public String getStreet() {
		return street;
	}

	public String getDistric() {
		return distric;
	}

	public String getProvince() {
		return province;
	}

	public String getCountry() {
		return country;
	}

	public float getLangitude() {
		return langitude;
	}

	public float getLatitude() {
		return latitude;
	}
}
