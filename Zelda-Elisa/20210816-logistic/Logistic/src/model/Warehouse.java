package model;

public class Warehouse {
	private int id,adressId,distanceMapperId;
	
	public Warehouse(int id, int adressId, int distanceMapperId) {
		this.id=id;
		this.adressId=adressId;
		this.distanceMapperId=distanceMapperId;
	}

	public int getId() {
		return id;
	}

	public int getAdressId() {
		return adressId;
	}

	public int getDistanceMapperId() {
		return distanceMapperId;
	}
}
