package model;

public class DistanceMapping {
	private int id,distance,fromWarehouseId, toWarehouseId;
	
	public DistanceMapping(int id, int distance, int fromWarehouseId,int toWarehouseId) {
		this.id=id;
		this.distance=distance;
		this.fromWarehouseId=fromWarehouseId;
		this.toWarehouseId=toWarehouseId;
	}

	public int getId() {
		return id;
	}

	public int getDistance() {
		return distance;
	}

	public int getFromWarehouseId() {
		return fromWarehouseId;
	}

	public int getToWarehouseId() {
		return toWarehouseId;
	}

}

