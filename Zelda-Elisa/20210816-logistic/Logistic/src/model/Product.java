package model;

public class Product {
	private int id,deliverAdress;
	private String name;
	private double width,height,depth,weight;
	
	public Product(int id, String name, int deliverAdress,
			double width, double height, double depth,double weight) {
		this.id=id;
		this.name=name;
		this.deliverAdress=deliverAdress;
		this.width=width;
		this.height=height;
		this.depth=depth;
		this.weight=weight;
	}


	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getDeliverAdress() {
		return deliverAdress;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public double getDepth() {
		return depth;
	}

	public double getWeight() {
		return weight;
	}
	
}
