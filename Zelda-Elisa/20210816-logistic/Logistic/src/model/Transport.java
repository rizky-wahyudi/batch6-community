package model;

public class Transport {
	private int id,size,speed,load;
	private String driver,type;
	
	public Transport(int id, String driver, int size, int speed,
			String type, int load) {
		this.id=id;
		this.driver=driver;
		this.size=size;
		this.speed=speed;
		this.type=type;
		this.load=load;
	}

	public int getId() {
		return id;
	}

	public int getSize() {
		return size;
	}

	public int getSpeed() {
		return speed;
	}

	public int getLoad() {
		return load;
	}

	public String getDriver() {
		return driver;
	}

	public String getType() {
		return type;
	}
	
}
