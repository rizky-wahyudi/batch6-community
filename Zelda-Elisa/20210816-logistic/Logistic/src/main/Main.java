package main;

import controller.Controller;

public class Main {
	public static void main(String[] args) {
		System.out.println("===========================");
		System.out.println("LOGISTIC");
		System.out.println("===========================");
		System.out.println("DETAIL PRODUCT");
		System.out.println("---------------------------");
		Controller c = new Controller();
		c.showProduct();
		System.out.println("===========================");
		System.out.println("FROM WAREHOUSE ADDRESS");
		System.out.println("---------------------------");
		c.showFrom();
		System.out.println("===========================");
		System.out.println("TO WAREHOUSE ADDRESS");
		System.out.println("---------------------------");
		c.showTo();
		System.out.println("===========================");
		System.out.println("SHOW DISTANCE");
		System.out.println("---------------------------");
		c.showDistance();
		System.out.println("===========================");
		System.out.println("DETAIL TRANSPORT");
		System.out.println("---------------------------");
		c.showTransport();
	}

}
