create table service(
id int auto_increment not null primary key,
imageurl varchar(225) null,
status varchar(7) null);

create table brand(
id int auto_increment not null primary key,
name varchar(225) null,
logoUrl varchar(225) null);

create table category(
id int auto_increment not null primary key,
type varchar(225) null);

create table currentLocation(
id int auto_increment not null primary key,
street varchar(225) null,
district varchar(225) null,
city varchar(225) null,
province varchar(225) null,
country varchar(225) null,
longitude float(10,6) not null,
latitude float(10,6) not null);

create table address(
id int auto_increment not null primary key,
street varchar(225) null,
district varchar(225) null,
city varchar(225) null,
province varchar(225) null,
country varchar(225) null,
longitude float(10,6) not null,
latitude float(10,6) not null);

create table service(
id int not null auto_increment primary key,
insurance varchar(3) null,
weekdayPrice int null,
weekendPrice int null,
holidayPrice int null,
carCategoryId int,
carGearType varchar(8) null,
foreign key (carCategoryId) references category(id));

create table images(
id int auto_increment not null primary key,
imageurl varchar(225) null,
status varchar(225) null);

create table car(
id int auto_increment not null primary key,
imagesId int null,
brandId int null,
categoryId int null,
engineSize varchar(10) null,
fuelType varchar(10) null,
currentLocationId int null,
gearType varchar(10) null,
foreign key (imagesId) references images(id),
foreign key (brandId) references brand(id),
foreign key (categoryId) references category(id),
foreign key (currentLocationId) references currentLocation(id));

create table booking(
id int not null auto_increment primary key,
customerId int null,
carId int null,
serviceId int null,
pickupLocation varchar(225) null,
dropOffLocation varchar(225) null,
pickupDate date null,
dropOffDate date null,
totalPrice int null,
foreign key (customerId) references customer(id),
foreign key (carId) references car(id),
foreign key (serviceId) references service(id));