import java.util.Scanner;
class Main {
    public static void main(String[] args){
        AirPlane airplane = new AirPlane();
        Business business = new Business();
        Economy economy = new Economy();
        FirstClass firstClass = new FirstClass();

        System.out.println("PILIH PENERBANGAN");
        System.out.println("-------------");
        System.out.println("1. EUROPE - ASIA");
        System.out.println("2. ASIA - EUROPE");
        System.out.println("3. EUROPE - US");
        System.out.println("-------------");
        System.out.print("Pilihan anda (pilih dalam angka 1,2,atau 3) = ");

        Scanner input_penerbangan = new Scanner(System.in);
        int penerbangan=input_penerbangan.nextInt();

        System.out.println("PILIH MASKAPAI");
        System.out.println("-------------");
        System.out.println("1. AIRBUS 380");
        System.out.println("2. BOEING 747");
        System.out.println("3. BOEING 787");
        System.out.println("-------------");
        System.out.print("Pilihan anda (pilih dalam angka 1,2,atau 3) = ");

        Scanner input_maskapai = new Scanner(System.in);
        int maskapai=input_maskapai.nextInt();

        if(penerbangan==1 && maskapai==1){
            float totalBusiness;
            totalBusiness = airplane.airbus*25/100*airplane.priceEUAS*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.airbus*65/100*airplane.priceEUAS*100/100;
            float totalFirst;
            totalFirst = airplane.airbus*10/100*airplane.priceEUAS*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI AIRBUS 380 DESTINASI EUROPE - ASIA");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.airbus*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.airbus*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.airbus*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else if(penerbangan==1 && maskapai==2){
            float totalBusiness;
            totalBusiness = airplane.boeing747*25/100*airplane.priceEUAS*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.boeing747*65/100*airplane.priceEUAS*100/100;
            float totalFirst;
            totalFirst = airplane.boeing747*10/100*airplane.priceEUAS*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI AIRBUS 380 DESTINASI ASIA - EUROPE");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.boeing747*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.boeing747*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.boeing747*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else if(penerbangan==1 && maskapai==3){
            float totalBusiness;
            totalBusiness = airplane.boeing787*25/100*airplane.priceEUAS*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.boeing787*65/100*airplane.priceEUAS*100/100;
            float totalFirst;
            totalFirst = airplane.boeing787*10/100*airplane.priceEUAS*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI AIRBUS 380 DESTINASI EUROPE - US");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.boeing787*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.boeing787*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.boeing787*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else if(penerbangan==2 && maskapai==1){
            float totalBusiness;
            totalBusiness = airplane.airbus*25/100*airplane.priceASEU*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.airbus*65/100*airplane.priceASEU*100/100;
            float totalFirst;
            totalFirst = airplane.airbus*10/100*airplane.priceASEU*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI BOEING 747 DESTINASI EUROPE - ASIA");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.airbus*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.airbus*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.airbus*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else if(penerbangan==2 && maskapai==2){
            float totalBusiness;
            totalBusiness = airplane.boeing747*25/100*airplane.priceASEU*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.boeing747*65/100*airplane.priceASEU*100/100;
            float totalFirst;
            totalFirst = airplane.boeing747*10/100*airplane.priceASEU*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI BOEING 747 DESTINASI ASIA - EUROPE");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.boeing747*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.boeing747*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.boeing747*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else if(penerbangan==2 && maskapai==3){
            float totalBusiness;
            totalBusiness = airplane.boeing747*25/100*airplane.priceEUUS*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.boeing747*65/100*airplane.priceEUUS*100/100;
            float totalFirst;
            totalFirst = airplane.boeing747*10/100*airplane.priceEUUS*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI BOEING 747 DESTINASI EUROPE - US");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.boeing747*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.boeing747*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.boeing747*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else if(penerbangan==3 && maskapai==1){
            float totalBusiness;
            totalBusiness = airplane.boeing787*25/100*airplane.priceEUAS*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.boeing787*65/100*airplane.priceEUAS*100/100;
            float totalFirst;
            totalFirst = airplane.boeing787*10/100*airplane.priceEUAS*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI BOEING 787 DESTINASI EUROPE - ASIA");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.boeing787*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.boeing787*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.boeing787*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else if(penerbangan==3 && maskapai==2){
            float totalBusiness;
            totalBusiness = airplane.boeing787*25/100*airplane.priceASEU*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.boeing787*65/100*airplane.priceASEU*100/100;
            float totalFirst;
            totalFirst = airplane.boeing787*10/100*airplane.priceASEU*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI BOEING 787 DESTINASI ASIA - EUROPE");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.boeing787*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.boeing787*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.boeing787*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else if(penerbangan==3 && maskapai==3){
            float totalBusiness;
            totalBusiness = airplane.boeing787*25/100*airplane.priceEUUS*225/100;
            float totalEkonomi;
            totalEkonomi = airplane.boeing787*65/100*airplane.priceEUUS*100/100;
            float totalFirst;
            totalFirst = airplane.boeing787*10/100*airplane.priceEUUS*300/100;
            float total;
            total = totalBusiness+totalEkonomi+totalFirst;
            System.out.println("RINCIAN PENERBANGAN MASKAPAI BOEING 787 DESTINASI EUROPE - US");
            System.out.println("=======================================");
            System.out.println("total penumpang business = "+airplane.boeing787*25/100);
            System.out.println("pendapatan kelas business = $"+totalBusiness);
            System.out.println("total laguage = "+airplane.laguage*200/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang ekonomi = "+airplane.boeing787*65/100);
            System.out.println("pendapatan kelas ekonomi = $"+totalEkonomi);
            System.out.println("total laguage = "+airplane.laguage*100/100+"Kg");
            System.out.println("=======================================");
            System.out.println("total penumpang firstclass = "+airplane.boeing787*10/100);
            System.out.println("pendapatan kelasfirsclass = $"+totalFirst);
            System.out.println("total laguage = "+airplane.laguage*250/100+"Kg");
            System.out.println("=======================================");
            System.out.println("TOTAL KEUNTUNGAN = $"+total);
        }
        else{
            System.out.println("tidak ada maskapai yang sesuai");
        }
    }
}
