package id.co.nexsoft.model;

public class DistanceMapper {
    private int id;
    private String fromWarehouseld;
    private String toWarehouseld;
    private double distance;

    public DistanceMapper(int id, String fromWarehouseld, String toWarehouseld, double distance) {
        this.id = id;
        this.fromWarehouseld = fromWarehouseld;
        this.toWarehouseld = toWarehouseld;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFromWarehouseld() {
        return fromWarehouseld;
    }

    public void setFromWarehouseld(String fromWarehouseld) {
        this.fromWarehouseld = fromWarehouseld;
    }

    public String getToWarehouseld() {
        return toWarehouseld;
    }

    public void setToWarehouseld(String toWarehouseld) {
        this.toWarehouseld = toWarehouseld;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
