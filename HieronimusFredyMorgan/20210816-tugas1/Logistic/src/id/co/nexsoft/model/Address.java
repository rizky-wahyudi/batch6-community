package id.co.nexsoft.model;

public class Address {
    private int id;
    private String street;
    private String district;
    private String province;
    private String county;
    private String langitude;
    private String latitude;

    public Address(int id, String street, String district, String province, String county, String langitude, String latitude) {
        this.id = id;
        this.street = street;
        this.district = district;
        this.province = province;
        this.county = county;
        this.langitude = langitude;
        this.latitude = latitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getLangitude() {
        return langitude;
    }

    public void setLangitude(String langitude) {
        this.langitude = langitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
