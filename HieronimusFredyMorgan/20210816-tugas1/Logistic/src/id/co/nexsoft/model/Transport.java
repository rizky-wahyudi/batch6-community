package id.co.nexsoft.model;

public class Transport {
    private int id;
    private String driver;
    private double size;
    private double speed;
    private String type;
    private double load;

    public Transport(int id, String driver, double size, double speed, String type) {
        this.id = id;
        this.driver = driver;
        this.size = size;
        this.speed = speed;
        this.type = type;
    }

    public Transport() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        if (type.equalsIgnoreCase("Truck")) {
            load = 10;
        } else if (type.equalsIgnoreCase("Car")) {
            load = 5;
        } else if (type.equalsIgnoreCase("Motor")) {
            load = 0.05;
        }
    }

}
