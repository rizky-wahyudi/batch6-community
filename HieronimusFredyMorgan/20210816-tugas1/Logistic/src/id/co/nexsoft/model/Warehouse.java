package id.co.nexsoft.model;

public class Warehouse {
    private int id;
    private Address address;
    private DistanceMapper distanceMapper;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public DistanceMapper getDistanceMapper() {
        return distanceMapper;
    }

    public void setDistanceMapper(DistanceMapper distanceMapper) {
        this.distanceMapper = distanceMapper;
    }
}
