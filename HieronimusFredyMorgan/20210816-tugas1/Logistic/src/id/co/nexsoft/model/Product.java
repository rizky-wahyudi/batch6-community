package id.co.nexsoft.model;

public class Product {
    private int id;
    private String name;
    private String deliverAddress;
    private double width;
    private double height;
    private double depth;
    private double weight;

    public Product(int id, String name, String deliverAddress, double width, double height, double depth, double weight) {
        this.id = id;
        this.name = name;
        this.deliverAddress = deliverAddress;
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeliverAddress() {
        return deliverAddress;
    }

    public void setDeliverAddress(String deliverAddress) {
        this.deliverAddress = deliverAddress;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
