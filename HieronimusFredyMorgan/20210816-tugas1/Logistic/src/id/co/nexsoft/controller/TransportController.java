package id.co.nexsoft.controller;

import id.co.nexsoft.model.Transport;

import java.util.ArrayList;

public class TransportController extends Transport{

    private ArrayList<Transport> transport = new ArrayList<>();
    Transport t1;

    public ArrayList<Transport> inputTransport() {
        t1 = new Transport(1, "Joko", 50, 80, "Motor");
        transport.add(t1);
        t1 = new Transport(2, "Susilo", 600, 90, "Car");
        transport.add(t1);
        t1 = new Transport(3, "Bambang", 4000, 60, "Truck");
        transport.add(t1);
        return transport;
    }
}
