package id.co.nexsoft.controller;

import id.co.nexsoft.model.DistanceMapper;

public class DistanceMapperController {
    DistanceMapper distanceMapper;

    public DistanceMapper addDistanceMapper() {
        distanceMapper = new DistanceMapper(1, "Jakarta", "Semarang", 340);
        return distanceMapper;
    }

    public DistanceMapper addDistanceMapper2() {
        distanceMapper = new DistanceMapper(2, "Semarang", "Yogyakarta", 120);
        return distanceMapper;
    }

    public DistanceMapper addDistanceMapper3() {
        distanceMapper = new DistanceMapper(2, "Yogyakarta", "Surabaya", 415.6);
        return distanceMapper;
    }

}
