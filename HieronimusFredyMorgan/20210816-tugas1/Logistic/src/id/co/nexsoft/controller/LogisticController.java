package id.co.nexsoft.controller;

import id.co.nexsoft.model.Product;
import id.co.nexsoft.model.Transport;
import id.co.nexsoft.model.Warehouse;

import java.util.ArrayList;

public class LogisticController {

    WarehouseController warehouseController = new WarehouseController();
    ProductController productController = new ProductController();
    TransportController transportController = new TransportController();

    public WarehouseController getWarehouseController() {
        return warehouseController;
    }

    public ProductController getProductController() {
        return productController;
    }

    public TransportController getTransportController() {
        return transportController;
    }

    public ArrayList<Product> checkDeliverProduct(ArrayList<Product> products, Warehouse warehouse) {
        ArrayList<Integer> temp = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getDeliverAddress().equalsIgnoreCase(warehouse.getDistanceMapper().getFromWarehouseld())) {
                products.remove(i);
            }
        }
        return products;
    }

    public double countProduct(ArrayList<Product> products) {
        double temp = 0;
        for (int i = 0; i < products.size(); i++) {
            temp += products.get(i).getWeight();
        }
        return temp;
    }

    public Transport chooseTransport(ArrayList<Product> product, Warehouse warehouses, ArrayList<Transport> transports) {
        for (int i = 0; i < transports.size(); i++) {
            if (countProduct(product) <= 50 && warehouses.getDistanceMapper().getDistance() < 20) {
                return  new Transport(1, "Joko", 50, 80, "Motor");
            } else if (countProduct(product) > 50 && warehouses.getDistanceMapper().getDistance() >= 50) {
                return new Transport(2, "Susilo", 600, 90, "Car");
            } else if (countProduct(product) > 100 && warehouses.getDistanceMapper().getDistance() >= 100) {
                return new Transport(3, "Bambang", 4000, 60, "Truck");
            }
        }
        return null;
    }

    public void print(ArrayList<Product> product, Warehouse warehouse, Transport transport) {
        System.out.println("Warehouse :" + warehouse.getAddress().getProvince());
        System.out.println("Product :");
        ;
        for (int i = 0; i < product.size(); i++) {
            System.out.println("\t " + product.get(i).getName());
            System.out.println("\t Deliver Product : " + product.get(i).getDeliverAddress());
        }
        System.out.println("To Warehouse : " + warehouse.getDistanceMapper().getToWarehouseld());
        System.out.println("Vehicle : " + transport.getType());
        System.out.println("Driver : " + transport.getDriver());
        System.out.println("Size : " + transport.getSize());
        System.out.println("Speed : " + transport.getSpeed());
        System.out.println("=================================");
    }
}
