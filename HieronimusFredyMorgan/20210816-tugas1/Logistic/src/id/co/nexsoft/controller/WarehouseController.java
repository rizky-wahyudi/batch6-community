package id.co.nexsoft.controller;

import id.co.nexsoft.model.Address;
import id.co.nexsoft.model.DistanceMapper;
import id.co.nexsoft.model.Warehouse;

import java.util.ArrayList;

public class WarehouseController extends Warehouse{
    private Warehouse warehouses;
    private AddressController idAddress = new AddressController();
    private DistanceMapperController idDistanceMapper = new DistanceMapperController();


    public ArrayList<Warehouse> inputWarehouse() {
        ArrayList<Warehouse> warehouseArrayList = new ArrayList<>();

        warehouses = new Warehouse();
        warehouses.setAddress(idAddress.inputAddress1());
        warehouses.setDistanceMapper(idDistanceMapper.addDistanceMapper());
        warehouses.setId(1);
        warehouseArrayList.add(warehouses);
        warehouses = new Warehouse();
        warehouses.setAddress(idAddress.inputAddress2());
        warehouses.setDistanceMapper(idDistanceMapper.addDistanceMapper2());
        warehouses.setId(2);
        warehouseArrayList.add(warehouses);
        warehouses = new Warehouse();
        warehouses.setAddress(idAddress.inputAddress3());
        warehouses.setDistanceMapper(idDistanceMapper.addDistanceMapper3());
        warehouses.setId(3);
        warehouseArrayList.add(warehouses);

        return warehouseArrayList;
    }
}
