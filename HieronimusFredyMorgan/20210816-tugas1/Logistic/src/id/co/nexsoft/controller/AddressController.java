package id.co.nexsoft.controller;

import id.co.nexsoft.model.Address;

public class AddressController {
    Address address;

    public Address inputAddress1() {
        address = new Address(1, "St. Bouveld", "Beoveld", "Jakarta",
                "Indonesia", "108", "21");
        return address;
    }


    public Address inputAddress2() {
        address = new Address(2,"St. Refold","Refold","Semarang",
                "Indonesia","178","46");
        return address;
    }

    public Address inputAddress3() {
        address = new Address(3, "St. William", "Wisburg", "Surabaya",
                "Indonesia", "628", "45");
        return address;
    }

}
