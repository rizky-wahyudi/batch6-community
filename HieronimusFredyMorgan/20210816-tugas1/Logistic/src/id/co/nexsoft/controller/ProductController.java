package id.co.nexsoft.controller;

import id.co.nexsoft.model.Product;

import java.util.ArrayList;

public class ProductController {
    private ArrayList<Product> products = new ArrayList<>();
    private Product p1;

    public ArrayList<Product> inputProduct() {
        p1 = new Product(1, "Sepatu", "Jakarta", 20, 15, 10, 50);
        products.add(p1);
        p1 = new Product(2, "Kaos", "Surabaya", 30, 15, 1, 100);
        products.add(p1);
        p1 = new Product(3, "Bantal", "Semarang", 30, 60, 10, 60);
        products.add(p1);
        p1 = new Product(4, "Laptop", "Surabaya", 30, 30, 10, 50);
        products.add(p1);
        return products;
    }


}
