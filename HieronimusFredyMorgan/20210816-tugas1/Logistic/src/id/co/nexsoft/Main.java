package id.co.nexsoft;

import id.co.nexsoft.controller.LogisticController;
import id.co.nexsoft.controller.ProductController;
import id.co.nexsoft.controller.TransportController;
import id.co.nexsoft.controller.WarehouseController;
import id.co.nexsoft.model.Product;
import id.co.nexsoft.model.Transport;
import id.co.nexsoft.model.Warehouse;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        LogisticController logisticController = new LogisticController();

        ArrayList<Warehouse> warehouses;
        ArrayList<Product> products;
        ArrayList<Transport> transports;
        warehouses = logisticController.getWarehouseController().inputWarehouse();
        products = logisticController.getProductController().inputProduct();
        transports = logisticController.getTransportController().inputTransport();

        Transport transport = logisticController.chooseTransport(products,
                warehouses.get(0), transports);
        logisticController.print(products, warehouses.get(0), transport);
        for (int i = 0; i < warehouses.size(); i++) {
            transport = logisticController.chooseTransport(products,
                    warehouses.get(i), transports);
            products = logisticController.checkDeliverProduct(products, warehouses.get(i));
            logisticController.print(products, warehouses.get(i), transport);
        }
    }
}
