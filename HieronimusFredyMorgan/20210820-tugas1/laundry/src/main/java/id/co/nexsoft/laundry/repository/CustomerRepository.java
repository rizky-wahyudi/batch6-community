package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    Customer findById(int id);

    List<Customer> findAll();

    void deleteById(int id);

}