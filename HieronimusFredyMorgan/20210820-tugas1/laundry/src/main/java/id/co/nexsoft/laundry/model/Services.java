package id.co.nexsoft.laundry.model;

import javax.persistence.*;

@Entity
public class Services {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
    private String nameService;
    private String priceService;
    private String unitService;
    private String typeService;

    public Services(String nameService, String priceService, String unitService, String typeService) {
        this.nameService = nameService;
        this.priceService = priceService;
        this.unitService = unitService;
        this.typeService = typeService;
    }

    public Services() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameService() {
        return nameService;
    }

    public void setNameService(String nameService) {
        this.nameService = nameService;
    }

    public String getPriceService() {
        return priceService;
    }

    public void setPriceService(String priceService) {
        this.priceService = priceService;
    }

    public String getUnitService() {
        return unitService;
    }

    public void setUnitService(String unitService) {
        this.unitService = unitService;
    }

    public String getTypeService() {
        return typeService;
    }

    public void setTypeService(String typeService) {
        this.typeService = typeService;
    }

    @Override
    public String toString() {
        return "Services{" +
                "id=" + id +
                ", nameService='" + nameService + '\'' +
                ", priceService='" + priceService + '\'' +
                ", unitService='" + unitService + '\'' +
                ", typeService='" + typeService + '\'' +
                '}';
    }
}
