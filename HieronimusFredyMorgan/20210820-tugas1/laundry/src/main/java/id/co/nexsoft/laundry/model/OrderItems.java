package id.co.nexsoft.laundry.model;

import javax.persistence.*;

@Entity
public class OrderItems {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
    private String itemName;
    private double weight;
    private double amount;
    @OneToOne(targetEntity = Services.class)
    private Services servicesId;

    public OrderItems(String itemName, double weight, double amount) {
        this.itemName = itemName;
        this.weight = weight;
        this.amount = amount;
    }

    public OrderItems() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Services getServiceId() {
        return servicesId;
    }

    public void setServiceId(Services servicesId) {
        this.servicesId = servicesId;
    }

    @Override
    public String toString() {
        return "OrderItems{" +
                "id=" + id +
                ", itemName='" + itemName + '\'' +
                ", weight=" + weight +
                ", amount=" + amount +
                ", serviceId=" + servicesId +
                '}';
    }
}
