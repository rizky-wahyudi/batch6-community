package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.Services;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends CrudRepository<Services, Integer> {
    Services findById(int id);

    List<Services> findAll();

    void deleteById(int id);

    Services save(Services employee);
}