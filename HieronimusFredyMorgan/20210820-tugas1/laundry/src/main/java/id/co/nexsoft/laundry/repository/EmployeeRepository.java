package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    Employee findById(int id);

    List<Employee> findAll();

    void deleteById(int id);

    Employee save(Employee employee);
}