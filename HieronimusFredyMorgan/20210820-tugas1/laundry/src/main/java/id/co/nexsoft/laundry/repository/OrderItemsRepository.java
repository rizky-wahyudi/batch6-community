package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.OrderItems;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemsRepository extends CrudRepository<OrderItems, Integer> {
    OrderItems findById(int id);

    List<OrderItems> findAll();

    void deleteById(int id);

    OrderItems save(OrderItems orderItems);
}