package id.co.nexsoft.laundry.controller;

import id.co.nexsoft.laundry.model.*;
import id.co.nexsoft.laundry.repository.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LaundryController {
    AddressRepository addressRepository;
    CustomerRepository customerRepository;
    EmployeeRepository employeeRepository;
    OrderRepository orderRepository;
    OrderItemsRepository orderItemsRepository;
    ServiceRepository serviceRepository;


    public LaundryController(AddressRepository addressRepository, CustomerRepository customerRepository,
                             EmployeeRepository employeeRepository, OrderRepository orderRepository,
                             OrderItemsRepository orderItemsRepository, ServiceRepository serviceRepository) {
        this.addressRepository = addressRepository;
        this.customerRepository = customerRepository;
        this.employeeRepository = employeeRepository;
        this.orderRepository = orderRepository;
        this.orderItemsRepository = orderItemsRepository;
        this.serviceRepository = serviceRepository;
    }

    @PostMapping(value = "/address", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String addAddress(@RequestBody Address address) {
        addressRepository.save(address);
        return "Add Address " + address.toString();
    }

    @PostMapping("/addCustomer/add/address:{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public String addCustomer(@RequestBody Customer customer, @PathVariable(value = "id") int id) {
        customer.setAddressId(addressRepository.findById(id));
        customerRepository.save(customer);
        return "Add " + customer.toString();
    }

    @PostMapping("/addEmployee/add/address:{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public String addEmployees(@RequestBody Employee employee, @PathVariable(value = "id") int id) {
        employee.setAddressId(addressRepository.findById(id));
        employeeRepository.save(employee);
        return "Add " + employee.toString();
    }

    @PostMapping("/addService/add")
    @ResponseStatus(HttpStatus.CREATED)
    public String addService(@RequestBody Services services) {

        serviceRepository.save(services);
        return "Add " + services.toString();
    }

    @PostMapping("/addOrders/add/service:{idService}/employees:{idEmployees}")
    @ResponseStatus(HttpStatus.CREATED)
    public String addOrders(@RequestBody Orders orders,
                            @PathVariable(value = "idService") int idService,
                            @PathVariable(value = "idEmployees") int idEmployees) {
        System.out.println(idService+""+idEmployees);
        for (OrderItems orderItems : orders.getOrderItems()) {
            orderItems.setServiceId(serviceRepository.findById(idService));
            orderItemsRepository.save(orderItems);
        }
        orders.setUpdateBy(employeeRepository.findById(idEmployees));
        orderRepository.save(orders);
        return "Add " + orders.toString();
    }

    @GetMapping("/orders")
    public List<Orders> getOrdersAll() {
        return orderRepository.findAll();
    }


}
