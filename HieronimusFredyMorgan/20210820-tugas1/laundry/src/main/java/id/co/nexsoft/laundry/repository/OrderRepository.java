package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.Orders;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Orders, Integer> {
    Orders findById(int id);

    List<Orders> findAll();

    void deleteById(int id);

    Orders save(Orders orders);
}