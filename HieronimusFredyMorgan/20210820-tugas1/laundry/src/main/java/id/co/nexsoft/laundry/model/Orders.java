package id.co.nexsoft.laundry.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
    @OneToMany(targetEntity = OrderItems.class)
    private List<OrderItems> orderItems;
    private Date orderDate;
    private String statusOrders;
    private boolean paymentStatus;
    private Date lastUpdateDate;
    @ManyToOne(targetEntity = Employee.class)
    private Employee updateBy;

    public Orders(List<OrderItems> orderItems, Date orderDate, String statusOrders, boolean paymentStatus, Date lastUpdateDate, Employee updateBy) {
        this.orderItems = orderItems;
        this.orderDate = orderDate;
        this.statusOrders = statusOrders;
        this.paymentStatus = paymentStatus;
        this.lastUpdateDate = lastUpdateDate;
        this.updateBy = updateBy;
    }

    public Orders() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<OrderItems> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItems> orderItems) {
        this.orderItems = orderItems;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatusOrders() {
        return statusOrders;
    }

    public void setStatusOrders(String statusOrders) {
        this.statusOrders = statusOrders;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Employee getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Employee updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderItems=" + orderItems +
                ", orderDate=" + orderDate +
                ", Status='" + statusOrders + '\'' +
                ", paymentStatus=" + paymentStatus +
                ", lastUpdateDate=" + lastUpdateDate +
                ", updateBy='" + updateBy + '\'' +
                '}';
    }
}
