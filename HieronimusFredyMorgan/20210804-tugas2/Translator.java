package handson8;

import java.util.Scanner;
import controller.ControllerTranslator;

public class Translator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean check = true;
        while (check) {
            ControllerTranslator controller = new ControllerTranslator();
            System.out.println("====================");
            System.out.println("Translator");
            System.out.println("====================");
            System.out.println("Pilihan:\n1. Suhu\n2. Jarak\n3. Keluar");
            System.out.println("--------------------");
            System.out.printf("%-10s %-1s ", "Pilih", ": ");
            int pilih = scanner.nextInt();
            System.out.println("--------------------");
            switch (pilih) {
                case 1:
                    System.out.println("Pilihan:\n1. Celcius\n2. Fahrenhiet");
                    System.out.println("--------------------");
                    System.out.printf("%-10s %-1s ", "Pilih", ": ");
                    int pilihsuhu = scanner.nextInt();
                    System.out.println("--------------------");
                    System.out.printf("%-10s %-1s ", "Suhu", ": ");
                    int suhu = scanner.nextInt();
                    System.out.println("--------------------");
                    controller.countSuhu(pilihsuhu, suhu);
                    break;
                case 2:
                    System.out.println("Pilihan:\n1. Kilometer\n2. Mile");
                    System.out.println("--------------------");
                    System.out.printf("%-10s %-1s ", "Pilih", ": ");
                    int pilihjarak = scanner.nextInt();
                    System.out.println("--------------------");
                    System.out.printf("%-10s %-1s ", "Jarak", ": ");
                    int jarak = scanner.nextInt();
                    System.out.println("--------------------");
                    controller.countJarak(pilihjarak, jarak);
                    break;
                case 3:
                    System.out.println("Terima kasih!");
                    check = false;
                    break;
                default:
                    System.out.println("Salah Input!");
                    check = false;
                    break;
            }
        }
    }
}
