package controller;

public class ControllerTranslator {
    private double celcius;
    private double fahrenheit;
    private double kilometer;
    private double miles;

    public double getCelcius() {
        return celcius;
    }

    public void setCelcius(double celcius) {
        this.celcius = celcius;
    }

    public double getFahrenheit() {
        return fahrenheit;
    }

    public void setFahrenheit(double fahrenheit) {
        this.fahrenheit = fahrenheit;
    }

    public double getKilometer() {
        return kilometer;
    }

    public void setKilometer(double kilometer) {
        this.kilometer = kilometer;
    }

    public double getMiles() {
        return miles;
    }

    public void setMiles(double miles) {
        this.miles = miles;
    }

    public void countSuhu(int pilihsuhu, int suhu) {
        double count;
        if (pilihsuhu == 1) {
            count = (suhu * 9 / 5) + 32;
            setCelcius(suhu);
            setFahrenheit(count);
        } else if (pilihsuhu == 2) {
            count = (suhu - 32) * 5 / 9;
            setCelcius(count);
            setFahrenheit(suhu);
        }
        printSuhu(pilihsuhu);
    }

    public void countJarak(int pilihjarak, int jarak) {
        double count;
        if (pilihjarak == 1) {
            count = jarak / 1.609;
            setKilometer(jarak);
            setMiles(count);
        } else if (pilihjarak == 2) {
            count = jarak * 1.609;
            setKilometer(count);
            setMiles(jarak);
        }
        printJarak(pilihjarak);
    }

    public void printSuhu(int pilihsuhu) {
        if (pilihsuhu == 1) {
            System.out.printf("%-10s %-1s %-5s\n", "Celcius", ":", getCelcius());
            System.out.printf("%-10s %-1s %-5s\n", "Fahrenheit", ":", getFahrenheit());
        } else if (pilihsuhu == 2) {
            System.out.printf("%-10s %-1s %-5s\n", "Fahrenheit", ":", getFahrenheit());
            System.out.printf("%-10s %-1s %-5s\n", "Celcius", ":", getCelcius());
        }
    }

    public void printJarak(int pilihJarak) {
        if (pilihJarak == 1) {
            System.out.printf("%-10s %-1s %-5s\n", "Celcius", ":", getKilometer());
            System.out.printf("%-10s %-1s %-5s\n", "Fahrenheit", ":", getMiles());
        } else if (pilihJarak == 2) {
            System.out.printf("%-10s %-1s %-5s\n", "Fahrenheit", ":", getMiles());
            System.out.printf("%-10s %-1s %-5s\n", "Celcius", ":", getKilometer());
        }
    }

}
