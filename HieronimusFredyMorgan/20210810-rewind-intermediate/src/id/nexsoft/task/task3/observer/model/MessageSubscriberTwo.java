package id.nexsoft.task.task3.observer.model;

public class MessageSubscriberTwo implements Observer {
    @Override
    public void update(Message m) {
        System.out.println("MessageSubscriberTwo : " + m.getMessageContent());
    }
}
