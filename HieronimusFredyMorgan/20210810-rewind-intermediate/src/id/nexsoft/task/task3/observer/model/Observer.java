package id.nexsoft.task.task3.observer.model;

public interface Observer {
    public void update(Message m);
}
