package id.nexsoft.task.task3.observer.model;

public class MessageSubscriberThree implements Observer {
    @Override
    public void update(Message m) {
        System.out.println("MessageSubscriberThree : " + m.getMessageContent());
    }
}
