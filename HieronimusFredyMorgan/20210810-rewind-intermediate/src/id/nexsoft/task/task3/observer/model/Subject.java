package id.nexsoft.task.task3.observer.model;

public interface Subject {
    public void attach(Observer o);

    public void detach(Observer o);

    public void notifyUpdate(Message m);
}
