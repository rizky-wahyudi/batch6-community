package id.nexsoft.task.task3.decorator.main;

import id.nexsoft.task.task3.decorator.model.Circle;
import id.nexsoft.task.task3.decorator.model.ColorShapeDecorator;
import id.nexsoft.task.task3.decorator.model.Rectangle;
import id.nexsoft.task.task3.decorator.model.Shape;

public class Main {
    public static void main(String[] args) {
        Shape circle = new Circle();
        Shape colorCircle
                = new ColorShapeDecorator(new Circle());

        Shape colorRectangle
                = new ColorShapeDecorator(new Rectangle());
        circle.draw();
        System.out.println("\nCircle of color border");
        colorCircle.draw();

        System.out.println("\nRectangle of color border");
        colorRectangle.draw();
    }
}
