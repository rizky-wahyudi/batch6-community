package id.nexsoft.task.task3.decorator.model;

public interface Shape {
    void draw();
}
