package id.nexsoft.task.task3.command.model;

public class DoorLockCommand implements Command {

    private Door door;

    public DoorLockCommand(Door door) {
        this.door = door;
    }

    @Override
    public void execute() {
        door.lockDoor();
    }
}