package id.nexsoft.task.task3.command.model;

public interface Command {
    void execute();
}