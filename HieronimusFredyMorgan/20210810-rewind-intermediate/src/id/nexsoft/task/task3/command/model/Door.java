package id.nexsoft.task.task3.command.model;

public class Door {

    public void unlockDoor() {
        System.out.println("Unlock the Door");
    }

    public void lockDoor() {
        System.out.println("Lock the Door");
    }
}