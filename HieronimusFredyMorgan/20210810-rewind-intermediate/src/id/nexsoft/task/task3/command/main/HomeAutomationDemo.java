package id.nexsoft.task.task3.command.main;

import id.nexsoft.task.task3.command.model.*;

public class HomeAutomationDemo {

    public static void main(String[] args) {

        Controller controller = new Controller();
        Door door = new Door();

        Command unlockDoor = new DoorUnlockCommand(door);
        controller.setCommand(unlockDoor);
        controller.executeCommand();

        Command lockDOor = new DoorLockCommand(door);
        controller.setCommand(lockDOor);
        controller.executeCommand();
    }
}