package id.nexsoft.task.task3.command.model;

public class DoorUnlockCommand implements Command {

    private Door door;

    public DoorUnlockCommand(Door door) {
        this.door = door;
    }

    @Override
    public void execute() {
        door.unlockDoor();
    }
}