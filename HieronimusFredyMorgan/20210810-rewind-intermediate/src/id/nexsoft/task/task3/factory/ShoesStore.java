package id.nexsoft.task.task3.factory;

class ShoesStore extends ShoesFactory {
    @Override
    Shoes createShoes(String type) {
        if (type.equalsIgnoreCase("sneaker")) {
            return new ShoesPantofel("Sneaker");
        } else if (type.equalsIgnoreCase("pantofel")) {
            return new ShoesSneaker("Pantofel");
        } else return null;
    }
}