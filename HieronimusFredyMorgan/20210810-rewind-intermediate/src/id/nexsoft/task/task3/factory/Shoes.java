package id.nexsoft.task.task3.factory;

abstract class Shoes {
    private String name;

    Shoes(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    void attachLegs() {
        System.out.println("Attaching Legs");
    }

    void attachShoesTop() {
        System.out.println("Attaching shoe top");
    }
}