package id.nexsoft.task.task3.factory;

class Main {
    public static void main(String[] args) {
        ShoesStore tableStore = new ShoesStore();
        Shoes strangeShoes = tableStore.orderTable("formar shoes");
        Shoes officeShoes = tableStore.orderTable("sneaker");
        Shoes kitchenShoes = tableStore.orderTable("pantofel");
    }
}