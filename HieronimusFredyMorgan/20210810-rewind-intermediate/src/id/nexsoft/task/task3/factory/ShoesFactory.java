package id.nexsoft.task.task3.factory;

abstract class ShoesFactory {

    abstract Shoes createShoes(String type);

    Shoes orderTable(String type) {
        Shoes shoes = createShoes(type);
        if (shoes == null) {
            System.out.println("Sorry, we are not able to create this kind of shoes\n");
            return null;
        }
        System.out.println("Making " + shoes.getName());
        shoes.attachLegs();
        shoes.attachShoesTop();
        System.out.println("Created " + shoes.getName() + "\n");
        return shoes;
    }
}