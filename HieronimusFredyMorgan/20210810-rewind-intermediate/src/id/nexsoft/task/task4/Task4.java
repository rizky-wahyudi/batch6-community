package id.nexsoft.task.task4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Task4 {
    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            String link = "https://jsonplaceholder.typicode.com/todos/" + i;
            try {
                URL url = new URL(link);
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(url.openStream())
                );
                String inputLine;
                while ((inputLine = bufferedReader.readLine()) != null)
                    System.out.println(inputLine);
                bufferedReader.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
