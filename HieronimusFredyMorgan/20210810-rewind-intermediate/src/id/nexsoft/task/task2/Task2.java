package id.nexsoft.task.task2;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Task2 {
    public static void main(String[] args) {
        ExecutorService exec = Executors.newFixedThreadPool(2);
        File folder = new File("E:\\Bootcamp\\Bootcamp\\Code\\20210810-rewind-intermediate\\text");
        File[] files = folder.listFiles();
        for (File file : files) {
            exec.execute(new Worker(file));
        }
        exec.shutdown();
    }
}