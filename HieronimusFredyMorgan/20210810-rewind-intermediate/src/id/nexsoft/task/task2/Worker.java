package id.nexsoft.task.task2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Worker implements Runnable {
    private File file;

    public Worker(File file) {
        this.file = file;
    }

    @Override
    public void run() {
        Scanner scan = null;
        for (int i = 0; i < 1000; i++) {
            try {
                scan = new Scanner(file);
                System.out.println(scan.next() + " " + scan.next());
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
