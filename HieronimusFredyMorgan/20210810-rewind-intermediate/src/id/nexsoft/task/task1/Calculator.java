package id.nexsoft.task.task1;

public class Calculator {

    public int add(int numOne, int numTwo) {
        int result = numOne + numTwo;
        return result;
    }

    public int subtract(int numOne, int numTwo) {
        int result = numOne - numTwo;
        return result;
    }

    public int multiply(int numOne, int numTwo) {
        int result = numOne * numTwo;
        return result;
    }

    public int divide(int numOne, int numTwo) {
        int result = numOne / numTwo;
        return result;
    }

}
