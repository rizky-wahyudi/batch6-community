package id.nexsoft.task.task1;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    Calculator calculator = new Calculator();

    @Test
    @DisplayName("Add True")
    public void testAddTrue() {
        int result = calculator.add(2, 2);
        assertTrue(result > 3);
    }

    @Test
    @DisplayName("Subtract True")
    public void testSubtractTrue() {
        int result = calculator.subtract(5, 2);
        assertTrue(result > 0);
    }

    @Test
    @DisplayName("Multiply False")
    public void testAddFalse() {
        int result = calculator.multiply(2, 2);
        assertFalse(!(result > 1));
    }

    @Test
    @DisplayName("Add False 0")
    public void testAddFalseZero() {
        int result = calculator.divide(2, 2);
        assertFalse(result == 0);
    }

    @Test
    @DisplayName("Add Throws String")
    public void testAddThrowsString() {
        assertThrows(NumberFormatException.class, () -> {
            calculator.add(3, Integer.parseInt("Tiga"));
        });
    }

    @Test
    @DisplayName("Add Throws Integer")
    public void testMultiplyThrowsInteger() {
        assertThrows(NumberFormatException.class, () -> {
            calculator.multiply(3, Integer.parseInt("3"));
        });
    }

    @Test
    @DisplayName("Add Equals")
    public void testAddEquals() {
        int result = calculator.add(2, 2);
        Assert.assertEquals(4, result);
    }

    @Test
    @DisplayName("Subtract Equals")
    public void testSubtractEquals() {
        int result = calculator.subtract(5, 2);
        Assert.assertEquals(4, result);
    }


}