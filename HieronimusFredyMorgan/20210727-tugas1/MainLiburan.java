import Liburan.Liburan;
import LiburanStaticFactory.LiburanStaticFactory;

import java.util.Scanner;

public class MainLiburan {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Destinasi Anda: ");
        String tujuan = scanner.nextLine();
        System.out.print("Berapa Orang : ");
        int orang = scanner.nextInt();
        System.out.print("Berapa Hari : ");
        int hari = scanner.nextInt();

        int penginapan = 0;
        int perjalanan = 0;
        if (tujuan.equalsIgnoreCase("Bandung")) {
            penginapan = 100000;
            perjalanan = 500000;
        } else if (tujuan.equalsIgnoreCase("Tokyo")) {
            penginapan = 4000000;
            perjalanan = 14000000;
        } else if (tujuan.equalsIgnoreCase("New York")) {
            penginapan = 6000000;
            perjalanan = 25000000;
        }
        System.out.println("=====================================");
        System.out.println();
        System.out.println("Tujuan\t\t\t:" + tujuan.toUpperCase());
        System.out.println("=====================================");
        System.out.println("Tempat Wisata:");
        Liburan liburan = LiburanStaticFactory.newInstance(tujuan, orang, hari, penginapan, perjalanan);
        System.out.println("=====================================");
        System.out.println("Biaya Penginapan\t: Rp " + liburan.getHargaPenginapan());
        System.out.println("Biaya Perjalanan\t: Rp " + liburan.getBiayaPerjalanan());
        System.out.println("Dana Yang dibutuhkan\t: Rp " + liburan.getDana());
    }
}
