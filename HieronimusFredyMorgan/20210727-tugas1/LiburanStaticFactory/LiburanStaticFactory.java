package LiburanStaticFactory;

import Liburan.*;

public class LiburanStaticFactory {
    public static Liburan newInstance(String tujuan, int jumlahOrang, int jumlahHari, long hargaPenginapan, long biayaPerjalanan) {
        if (tujuan.equalsIgnoreCase("Bandung")) {
            return new Bandung(jumlahOrang, jumlahHari, hargaPenginapan, biayaPerjalanan);
        } else if (tujuan.equalsIgnoreCase("Tokyo")) {
            return new Tokyo(jumlahOrang, jumlahHari, hargaPenginapan, biayaPerjalanan);
        } else if (tujuan.equalsIgnoreCase("New York")) {
            return new NewYork(jumlahOrang, jumlahHari, hargaPenginapan, biayaPerjalanan);
        }
        return null;
    }
}
