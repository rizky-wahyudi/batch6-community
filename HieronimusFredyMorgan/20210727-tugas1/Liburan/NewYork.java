package Liburan;

public class NewYork extends Liburan {
    public NewYork(int jumlahOrang, int jumlahHari, long hargaPenginapan, long biayaPerjalanan) {
        super(jumlahOrang, jumlahHari, hargaPenginapan, biayaPerjalanan);
        destinasiWisata();
    }

    public void destinasiWisata() {
        System.out.println("Central Park");
        System.out.println("The Metropolitan Museum of Art");
        System.out.println("Empire State Building");
    }
}
