package Liburan;

public class Bandung extends Liburan {
    public Bandung(int jumlahOrang, int jumlahHari, long hargaPenginapan, long biayaPerjalanan) {
        super(jumlahOrang, jumlahHari, hargaPenginapan, biayaPerjalanan);
        destinasiWisata();
    }

    public void destinasiWisata() {
        System.out.println("Tangkuban Perahu");
        System.out.println("Jalan Braga");
        System.out.println("Gedung Sate");
    }
}
