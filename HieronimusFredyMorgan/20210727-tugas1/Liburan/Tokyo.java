package Liburan;

public class Tokyo extends Liburan {
    public Tokyo(int jumlahOrang, int jumlahHari, long hargaPenginapan, long biayaPerjalanan) {
        super(jumlahOrang, jumlahHari, hargaPenginapan, biayaPerjalanan);
        destinasiWisata();
    }

    public void destinasiWisata() {
        System.out.println("Tokyo Skytree");
        System.out.println("Meiji Jingu");
        System.out.println("Shinjuku Gyoen National Garden");
    }
}
