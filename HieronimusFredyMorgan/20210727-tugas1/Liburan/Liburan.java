package Liburan;

public class Liburan {
    private int jumlahOrang;
    private int jumlahHari;
    private long hargaPenginapan;
    private long biayaPerjalanan;
    private double dana;

    public Liburan() {
    }

    public Liburan(int jumlahOrang, int jumlahHari, long hargaPenginapan, long biayaPerjalanan) {
        this.jumlahOrang = jumlahOrang;
        this.jumlahHari = jumlahHari;
        this.hargaPenginapan = hargaPenginapan;
        this.biayaPerjalanan = biayaPerjalanan;
    }

    public int getJumlahOrang() {
        return jumlahOrang;
    }

    public void setJumlahOrang(int jumlahOrang) {
        this.jumlahOrang = jumlahOrang;
    }

    public int getJumlahHari() {
        return jumlahHari;
    }

    public void setJumlahHari(int jumlahHari) {
        this.jumlahHari = jumlahHari;
    }

    public long getHargaPenginapan() {
        return hargaPenginapan;
    }

    public void setHargaPenginapan(long hargaPenginapan) {
        this.hargaPenginapan = hargaPenginapan;
    }

    public long getBiayaPerjalanan() {
        return biayaPerjalanan;
    }

    public void setBiayaPerjalanan(long biayaPerjalanan) {
        this.biayaPerjalanan = biayaPerjalanan;
    }

    public long getDana() {
        return jumlahOrang * jumlahHari * hargaPenginapan + biayaPerjalanan;
    }

}
