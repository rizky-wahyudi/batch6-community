package id.co.nexsoft.student.controller;

import id.co.nexsoft.student.entity.Student;
import id.co.nexsoft.student.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class StudentController {
    @Autowired
    StudentRepository repo;

    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
                + "<h1>WELCOME</h1>"
                + "</body></html>";
    }

    @GetMapping("/student")
    public List<Student> getAllNotes() {
        return repo.findAll();
    }

    @GetMapping("/student/{id}")
    public Student getstudentById(@PathVariable(value = "id") int id) {
        return repo.findById(id);
    }

    @PostMapping(value = "/student", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    public Student addstudent(@RequestBody Student student) {
        return repo.save(student);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteStudent(@PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }

    @PutMapping("/student/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Student student, @PathVariable int id) {

        Optional<Student> studentRepo = Optional.ofNullable(repo.findById(id));

        if (!studentRepo.isPresent())
            return ResponseEntity.notFound().build();

        student.setId(id);
        repo.save(student);
        return ResponseEntity.noContent().build();
    }
}
