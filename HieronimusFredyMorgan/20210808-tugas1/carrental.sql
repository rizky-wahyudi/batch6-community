CREATE DATABASE CARRENTAL;

CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `street` varchar(50) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `province` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
CREATE TABLE `brand` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `logoUrl` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `imageUrl` varchar(45) DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `car` (
  `id` int NOT NULL AUTO_INCREMENT,
  `imageId` int DEFAULT NULL,
  `brandId` int DEFAULT NULL,
  `categoryId` int DEFAULT NULL,
  `engineSize` varchar(45) DEFAULT NULL,
  `fuelType` varchar(45) DEFAULT NULL,
  `addressId` int DEFAULT NULL,
  `gearType` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `imageId_idx` (`imageId`),
  KEY `brandId_idx` (`brandId`),
  KEY `categoryId_idx` (`categoryId`),
  CONSTRAINT `brandId` FOREIGN KEY (`brandId`) REFERENCES `brand` (`id`),
  CONSTRAINT `categoryId` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`),
  CONSTRAINT `imageId` FOREIGN KEY (`imageId`) REFERENCES `image` (`id`)
);

CREATE TABLE `customer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `addressId` int DEFAULT NULL,
  `idNumber` int DEFAULT NULL,
  `imageId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `image_id_idx` (`imageId`),
  CONSTRAINT `image_id` FOREIGN KEY (`imageId`) REFERENCES `image` (`id`)
);

CREATE TABLE `service` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `insurance` VARCHAR(3) DEFAULT NULL,
    `weekdayPrice` INT DEFAULT NULL,
    `weekendPrice` INT DEFAULT NULL,
    `holidayPrice` INT DEFAULT NULL,
    `carCategoriId` INT DEFAULT NULL,
    `carGearType` VARCHAR(45) DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `carCategoriId` FOREIGN KEY (`id`)
        REFERENCES `category` (`id`)
);


CREATE TABLE `booking` (
  `id` int NOT NULL AUTO_INCREMENT,
  `customerId` int DEFAULT NULL,
  `carId` int DEFAULT NULL,
  `serviceId` int DEFAULT NULL,
  `pickUpLocation` varchar(45) DEFAULT NULL,
  `dropOffLocation` varchar(45) DEFAULT NULL,
  `pickupDate` datetime DEFAULT NULL,
  `dropOffDate` datetime DEFAULT NULL,
  `totalPrice` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id_idx` (`customerId`),
  KEY `car_id_idx` (`carId`),
  KEY `service_id_idx` (`serviceId`),
  CONSTRAINT `car_id` FOREIGN KEY (`carId`) REFERENCES `car` (`id`),
  CONSTRAINT `customer_id` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`),
  CONSTRAINT `service_id` FOREIGN KEY (`serviceId`) REFERENCES `service` (`id`)
);





