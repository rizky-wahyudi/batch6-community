package vending;

import controller.Controller;

public class Vending extends Controller {
    private int moneyMachine = 50000;
    private Items[][] items = new Items[6][10];

    public void loadProduct() {
        Product isi = new Product();
        items = isi.products(items);
    }

    public int getMoneyMachine() {
        return moneyMachine;
    }

    public void setMoneyMachine(int moneyMachine) {
        this.moneyMachine = moneyMachine;
    }

    public Items[][] getProducts() {
        return items;
    }

    public void setProducts(Items[][] items) {
        this.items = items;
    }
}
