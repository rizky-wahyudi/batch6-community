package vending;

public class Items extends Vending {
    private String name;
    private int jumlah;
    private int harga;

    public Items(String name, int jumlah, int harga) {
        this.name = name;
        this.jumlah = jumlah;
        this.harga = harga;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    @Override
    public String toString() {
        System.out.println("Nama      :" + name);
        System.out.println("Harga     :" + harga);
        System.out.println("Tersedia  :" + jumlah);
        return "";
    }
}
