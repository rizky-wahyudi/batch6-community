package vending;

public class Product {
    String[] row1 = {"Pringles", "Pringles", "Chitato", "Chitato", "Kusuka", "Kusuka",
            "Taro", "Taro", "Happy Tos", "Happy Tos"};

    String[] row2 = {"Silverqueen", "Silverqueen", "Silverqueen", "Diary Milk", "Diary Milk", "Diary Milk",
            "Chocholatos", "Chocholatos", "Chocholatos", "Chocholatos"};

    String[] row3 = {"Milkita", "Milkita", "Milkita", "Milkita", "Milkita", "M&M",
            "M&M", "M&M", "M&M", "M&M"};

    String[] row4 = {"Aqua", "Aqua", "Aqua", "Aqua", "Vit", "Vit",
            "Vit", "LeMinerale", "LeMinerale", "LeMinerale"};

    String[] row5 = {"Coca Cola", "Coca Cola", "Coca Cola", "Coca Cola", "Sprite", "Sprite",
            "Sprite", "Bintang", "Bintang", "Bintang"};

    String[] row6 = {"Teh Pucuk", "Teh Pucuk", "Teh Pucuk", "Sosro", "Sosro", "Sosro",
            "Sosro", "Sosro", "Teh Kotak", "Teh Kotak"};

    public Items[][] products (Items[][] items){
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 10; j++) {
                if (i==0)
                    items[i][j] = new Items(row1[j],10,3000);
                if (i==1)
                    items[i][j] = new Items(row2[j],10,13000);
                if (i==2)
                    items[i][j] = new Items(row3[j],10,1000);
                if (i==3)
                    items[i][j] = new Items(row4[j],10,4000);
                if (i==4)
                    items[i][j] = new Items(row5[j],10,10000);
                if (i==5)
                    items[i][j] = new Items(row6[j],10,5000);
            }
        }
        return items;
    }
}
