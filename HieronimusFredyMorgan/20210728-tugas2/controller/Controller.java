package controller;

import vending.Items;
import vending.Vending;

public class Controller {
    String[] abjad = {"A", "B", "C", "D", "E", "F"};

    public String[] getAbjad() {
        return abjad;
    }

    public void setAbjad(String[] abjad) {
        this.abjad = abjad;
    }

    public double checkMoney(Items items, int money) {
        return items.getHarga() - money;
    }

    public boolean checkEntity(int row, int column) {
        return row < 6 || row > 0 || column < 10 || column > 0;
    }

    public Vending editValue(Vending vending, Items items, int row, int column, double check) {
        vending.getProducts()[row][column].setJumlah(items.getJumlah() - 1);
        int moneyMachine = vending.getMoneyMachine();
        vending.setMoneyMachine((int) (moneyMachine - check));
        return vending;
    }

    public int getKode(String a) {
        if (a.equalsIgnoreCase("A")) {
            return 0;
        } else if (a.equalsIgnoreCase("B")) {
            return 1;
        } else if (a.equalsIgnoreCase("C")) {
            return 2;
        } else if (a.equalsIgnoreCase("D")) {
            return 3;
        } else if (a.equalsIgnoreCase("E")) {
            return 4;
        } else if (a.equalsIgnoreCase("F")) {
            return 5;
        } else {
            System.out.println("Inputan salah");
            return -1;
        }
    }
}
