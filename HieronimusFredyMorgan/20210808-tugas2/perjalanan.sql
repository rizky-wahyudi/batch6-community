-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: perjalanan
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bagasi`
--

DROP TABLE IF EXISTS `bagasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bagasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `jumlah_muatan` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bagasi`
--

LOCK TABLES `bagasi` WRITE;
/*!40000 ALTER TABLE `bagasi` DISABLE KEYS */;
INSERT INTO `bagasi` VALUES (1,10),(2,15),(3,20),(4,30),(5,40);
/*!40000 ALTER TABLE `bagasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dokumen`
--

DROP TABLE IF EXISTS `dokumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dokumen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_dokumen` varchar(20) DEFAULT NULL,
  `nomor_dokumen` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dokumen`
--

LOCK TABLES `dokumen` WRITE;
/*!40000 ALTER TABLE `dokumen` DISABLE KEYS */;
INSERT INTO `dokumen` VALUES (1,'Passport','341345376421'),(2,'KTP','3325138974560001'),(3,'KTP','4356662350005423');
/*!40000 ALTER TABLE `dokumen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelanggan`
--

DROP TABLE IF EXISTS `pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pelanggan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelanggan`
--

LOCK TABLES `pelanggan` WRITE;
/*!40000 ALTER TABLE `pelanggan` DISABLE KEYS */;
INSERT INTO `pelanggan` VALUES (1,'Steven King','081999222333','1987-05-11'),(2,'Neena Kochhar','082555444990','1999-10-10'),(3,'Lex Den Haan','082555444990','1998-03-15');
/*!40000 ALTER TABLE `pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket`
--

DROP TABLE IF EXISTS `tiket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tiket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `penumpangId` int DEFAULT NULL,
  `transportasiId` int DEFAULT NULL,
  `dokumenId` int DEFAULT NULL,
  `bagasiId` int DEFAULT NULL,
  `asal` varchar(20) DEFAULT NULL,
  `tujuan` varchar(20) DEFAULT NULL,
  `waktu_berangkat` datetime DEFAULT NULL,
  `waktu_sampai` datetime DEFAULT NULL,
  `nomor_tiket` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `penumpangId` (`penumpangId`),
  KEY `transportasiId` (`transportasiId`),
  KEY `dokumenId` (`dokumenId`),
  KEY `bagasiId` (`bagasiId`),
  CONSTRAINT `tiket_ibfk_1` FOREIGN KEY (`penumpangId`) REFERENCES `pelanggan` (`id`),
  CONSTRAINT `tiket_ibfk_2` FOREIGN KEY (`transportasiId`) REFERENCES `transportasi` (`id`),
  CONSTRAINT `tiket_ibfk_3` FOREIGN KEY (`dokumenId`) REFERENCES `dokumen` (`id`),
  CONSTRAINT `tiket_ibfk_4` FOREIGN KEY (`bagasiId`) REFERENCES `bagasi` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket`
--

LOCK TABLES `tiket` WRITE;
/*!40000 ALTER TABLE `tiket` DISABLE KEYS */;
INSERT INTO `tiket` VALUES (1,1,1,1,2,'Jakarta','Surabaya','2021-08-09 10:00:00','2021-08-09 12:20:00','CL201'),(2,2,2,2,1,'Purwokerto','Yogyakarta','2021-08-09 15:00:00','2021-08-09 19:00:00','EF232'),(3,3,4,3,3,'Surabaya','Yogyakarta','2021-08-09 08:30:00','2021-08-09 10:15:00','BA426');
/*!40000 ALTER TABLE `tiket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transportasi`
--

DROP TABLE IF EXISTS `transportasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transportasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `kapasitas` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transportasi`
--

LOCK TABLES `transportasi` WRITE;
/*!40000 ALTER TABLE `transportasi` DISABLE KEYS */;
INSERT INTO `transportasi` VALUES (1,'Citilink',200),(2,'Bus Efisiensi',50),(3,'Bus Mulyo',50),(4,'Batik Air',150),(5,'Kereta Joglosemarkerto',200);
/*!40000 ALTER TABLE `transportasi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-10 22:20:05
