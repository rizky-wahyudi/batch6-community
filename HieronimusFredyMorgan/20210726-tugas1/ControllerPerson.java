public class ControllerPerson {
    private int a;
    private int b;
    private int c;

    public ControllerPerson(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public ControllerPerson() {
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public void proccesController() {
        if (a > b) {
            System.out.println("a > b, complete me");
        } else if (b > c) {
            System.out.println("b > c, complete me");
        } else if (c > 0) {
            System.out.println("c > 0, complete me");
        } else {
            System.out.println("False");
        }
    }
}
