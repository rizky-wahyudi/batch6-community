package AirplaneFactory;

import Airplane.Airplane;
import Airplane.Business;
import Airplane.Economy;
import Airplane.FirstClass;

public class StaticFactory {
    public static Airplane airplaneFactory(String type) {
        if (type.equalsIgnoreCase("Economy")) {
            return new Economy();
        } else if (type.equalsIgnoreCase("Business")) {
            return new Business();
        } else if (type.equalsIgnoreCase("First Class")) {
            return new FirstClass();
        }
        return null;
    }
}
