import AirplaneFactory.StaticFactory;
import Airplane.Airplane;
import Airplane.Business;
import Airplane.Economy;
import Airplane.FirstClass;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Pilih Class Airplane (Economy/Business/First Class): ");
        String pilih = scanner.nextLine();
        Airplane airplane = StaticFactory.airplaneFactory(pilih);

        if (airplane instanceof Economy) {
            Economy economy = (Economy) airplane;
            System.out.println("Kelas Economy:");
            System.out.println("-------------------------------------------------");
            System.out.println("Seat Airplane airbus 380: " + economy.totalSeat(airplane.airbus380()));
            System.out.println("Seat Airplane boeing 747: " + economy.totalSeat(airplane.boeing747()));
            System.out.println("Seat Airplane boeing 787: " + economy.totalSeat(airplane.boeing787()));
            System.out.println("-------------------------------------------------");
            System.out.println("Total Price Euro-Asia   : $" + economy.totalPrice(airplane.europeAsia()));
            System.out.println("Total Price Asia-Euro   : $" + economy.totalPrice(airplane.asiaEurope()));
            System.out.println("Total Price Euro-US     : $" + economy.totalPrice(airplane.europeUS()));
            System.out.println("-------------------------------------------------");
            System.out.println("Total Luggage           : " + economy.totalLuggage(airplane.getLuggage()));
        } else if (airplane instanceof Business) {
            Business business = (Business) airplane;
            System.out.println("Kelas Economy:");
            System.out.println("-------------------------------------------------");
            System.out.println("Seat Airplane airbus 380: " + business.totalSeat(airplane.airbus380()));
            System.out.println("Seat Airplane boeing 747: " + business.totalSeat(airplane.boeing747()));
            System.out.println("Seat Airplane boeing 787: " + business.totalSeat(airplane.boeing787()));
            System.out.println("-------------------------------------------------");
            System.out.println("Total Price Euro-Asia   : $" + business.totalPrice(airplane.europeAsia()));
            System.out.println("Total Price Asia-Euro   : $" + business.totalPrice(airplane.asiaEurope()));
            System.out.println("Total Price Euro-US     : $" + business.totalPrice(airplane.europeUS()));
            System.out.println("-------------------------------------------------");
            System.out.println("Total Luggage           : " + business.totalLuggage(airplane.getLuggage()));
        } else if (airplane instanceof FirstClass) {
            FirstClass firstClass = (FirstClass) airplane;
            System.out.println("Kelas Economy:");
            System.out.println("-------------------------------------------------");
            System.out.println("Seat Airplane airbus 380: " + firstClass.totalSeat(airplane.airbus380()));
            System.out.println("Seat Airplane boeing 747: " + firstClass.totalSeat(airplane.boeing747()));
            System.out.println("Seat Airplane boeing 787: " + firstClass.totalSeat(airplane.boeing787()));
            System.out.println("-------------------------------------------------");
            System.out.println("Total Price Euro-Asia   : $" + firstClass.totalPrice(airplane.europeAsia()));
            System.out.println("Total Price Asia-Euro   : $" + firstClass.totalPrice(airplane.asiaEurope()));
            System.out.println("Total Price Euro-US     : $" + firstClass.totalPrice(airplane.europeUS()));
            System.out.println("-------------------------------------------------");
            System.out.println("Total Luggage           : " + firstClass.totalLuggage(airplane.getLuggage()));
        } else {
            System.out.println("Salah Input !");
        }

    }
}
