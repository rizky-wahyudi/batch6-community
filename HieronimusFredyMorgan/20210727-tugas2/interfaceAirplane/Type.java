package interfaceAirplane;

public interface Type {
    public int airbus380();
    public int boeing747();
    public int boeing787();
}
