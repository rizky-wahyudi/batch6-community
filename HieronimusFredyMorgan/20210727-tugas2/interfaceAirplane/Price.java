package interfaceAirplane;

public interface Price {
    public int europeAsia();
    public int asiaEurope();
    public int europeUS();
}
