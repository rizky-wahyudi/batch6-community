package Airplane;

public class Economy extends Airplane {
    public int totalSeat(double seat) {
        return (int) (seat * 0.65);
    }

    public double totalPrice(double price) {
        return price;
    }

    public double totalLuggage(double luggage) {
        return luggage;
    }
}
