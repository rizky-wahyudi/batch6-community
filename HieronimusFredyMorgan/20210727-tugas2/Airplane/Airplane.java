package Airplane;

import interfaceAirplane.Price;
import interfaceAirplane.Type;

public class Airplane implements Price, Type {

    public double getLuggage() {
        return 20;
    }

    @Override
    public int europeAsia() {
        return 500;
    }

    @Override
    public int asiaEurope() {
        return 600;
    }

    @Override
    public int europeUS() {
        return 650;
    }

    @Override
    public int airbus380() {
        return 750;
    }

    @Override
    public int boeing747() {
        return 529;
    }

    @Override
    public int boeing787() {
        return 248;
    }
}
