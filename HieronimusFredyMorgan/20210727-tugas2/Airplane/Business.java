package Airplane;

public class Business extends Airplane {
    public int totalSeat(double seat) {
        return (int) (seat * 0.25);
    }

    public double totalPrice(double price) {
        return price * 2.25;
    }

    public double totalLuggage(double luggage) {
        return luggage * 2;
    }
}
