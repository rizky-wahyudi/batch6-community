package Airplane;

public class FirstClass extends Airplane {
    public int totalSeat(double seat) {
        return (int) (seat * 0.1);
    }

    public double totalPrice(double price) {
        return price * 3;
    }

    public double totalLuggage(double luggage) {
        return luggage * 2.5;
    }
}
