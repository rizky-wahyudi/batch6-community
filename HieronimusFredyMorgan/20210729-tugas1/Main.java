package money;

import money.change.Change;
import money.change.Money;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Change change = new Change();
        System.out.print("Pilih Mata Uang (1.USD/2.EUR) : ");

        Money money = change.choose(scanner.nextInt());

        System.out.print("Masukan Nilai Konversi IDR to " + money.getValuta() + " : ");
        money.setNominal(scanner.nextInt());
        System.out.println(money.getNominal() + " " + money.getValuta() + " : " + change.countRates(money) + " IDR");
        System.out.printf(money.getNominal() + " IDR : %.10f USD\n", change.countBasicRates(money));
    }
}
