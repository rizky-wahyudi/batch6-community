package money.change;

import money.valuta.Valuta;

import java.util.Date;

public class Change extends Valuta {

    public double countRates(Money money) {
        return money.getRates() * money.getNominal();
    }

    public double countBasicRates(Money money) {
        return money.getBasic() * money.getNominal();
    }

    public Date exchangeDate() {
        Date date = new Date();
        date.getTime();
        return date;
    }

    public Money choose(int choose) {
        Money money = new Money();
        switch (choose) {
            case 1:
                money.setValuta(USD());
                money.setRates(ratesUSDIDR());
                money.setBasic(ratesIDRUSD());
                information(money);
                break;
            case 2:
                money.setValuta(EUR());
                money.setRates(ratesEURIDR());
                money.setBasic(ratesIDREUR());
                information(money);
                break;
            default:
                System.out.println("Salah Input");
                break;
        }
        return money;
    }

    public void information(Money money) {
        System.out.println("1 " + money.getValuta() + " : " + money.getRates());
        System.out.printf("1 IDR : %.10f \n",  money.getBasic());
    }

    @Override
    public String EUR() {
        return super.EUR();
    }

    @Override
    public String USD() {
        return super.USD();
    }

    @Override
    public String IDR() {
        return super.IDR();
    }

    @Override
    public double ratesUSDIDR() {
        return super.ratesUSDIDR();
    }

    @Override
    public double ratesEURIDR() {
        return super.ratesEURIDR();
    }

    @Override
    public double ratesIDREUR() {
        return super.ratesIDREUR();
    }

    @Override
    public double ratesIDRUSD() {
        return super.ratesIDRUSD();
    }
}
