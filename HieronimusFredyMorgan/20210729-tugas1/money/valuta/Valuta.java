package money.valuta;

public class Valuta implements code, rates{

    @Override
    public String EUR() {
        return "EUR";
    }

    @Override
    public String USD() {
        return "USD";
    }

    @Override
    public String IDR() {
        return "IDR";
    }

    @Override
    public double ratesUSDIDR() {
        return 14490;
    }

    @Override
    public double ratesEURIDR() {
        return 17000;
    }

    @Override
    public double ratesIDREUR() {
        return 0.000058d;
    }

    @Override
    public double ratesIDRUSD() {
        return 0.000069d;
    }
}
