package money.valuta;

public interface rates {
    public double ratesUSDIDR();
    public double ratesEURIDR();
    public double ratesIDREUR();
    public double ratesIDRUSD();
}
