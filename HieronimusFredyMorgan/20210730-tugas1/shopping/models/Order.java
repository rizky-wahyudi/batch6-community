package shopping.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.stream.IntStream;

public class Order {

    private Date orderDate;
    private String cashierName;
    private double orderNumber;
    private String branchId;
    private ArrayList<orderItems> orderItem;
    private String statusOrder;
    private String paymentStatus;

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    public double getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(double orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public ArrayList<orderItems> getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(ArrayList<orderItems> orderItem) {
        this.orderItem = orderItem;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(String statusOrder) {
        this.statusOrder = statusOrder;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public double totalAllTax() {
        double a = getOrderItem().stream().mapToDouble(orderItems -> orderItems.getProduct().tax() * orderItems.productPrice()).sum();
        return a;
    }

    public double totalAllPrice() {
        double a = getOrderItem().stream().mapToDouble(orderItems::productPrice).sum();
        return a;
    }

    public double payments() {
        return (totalAllTax() + totalAllPrice());
    }

    public void print() {
        System.out.println("DETAIL SHOPPING CART");
        System.out.println("Order Date          : " + getOrderDate());
        System.out.println("Order Number        : " + getOrderNumber());
        System.out.println("Cashier             : " + getCashierName());
        System.out.println("Branch Id           : " + getBranchId());
        System.out.println("Jumlah Order Item   : " + getOrderItem().size());
        System.out.println("Order Item          : ");
        System.out.println("=====================================");
        IntStream.range(0, getOrderItem().size()).forEach(i -> getOrderItem().get(i).cetak());
        System.out.println("=====================================");

        System.out.println("Price               : Rp " + totalAllPrice());
        System.out.println("Total Tax           : Rp " + totalAllTax());
        System.out.println("Total Price         : Rp " + payments());
        System.out.println("Status              : " + getStatusOrder());
        System.out.println("Payment Status      : " + getPaymentStatus());
        System.out.println("=====================================");

    }

}