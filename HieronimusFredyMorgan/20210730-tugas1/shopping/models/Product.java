package shopping.models;

public class Product {

    private String articleNumber;
    private String description;
    private double purchasePrice;
    private String image;
    private String expiredDate;
    private String type;

    public String getArticleNumber() {
        return articleNumber;
    }

    public void setArticleNumber(String articleNumber) {
        this.articleNumber = articleNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double sellPrice() {
        return getPurchasePrice() + (getPurchasePrice() * 0.1);
    }

    public double tax() {
        if (getType().equalsIgnoreCase("makanan")) {
            return 0.05;
        } else if (getType().equalsIgnoreCase("sabun")) {
            return 0.07;
        } else if (getType().equalsIgnoreCase("minuman")) {
            return 0.1;
        }
        return 0;
    }
}