package shopping.models;

public class orderItems {

    private Product product;
    private String orderId;
    private int amount;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double productPrice() {
        return (getProduct().sellPrice() * getAmount());

    }

    public void cetak() {
        System.out.println("Order Id           : " + getOrderId());
        System.out.println("Article Number     : " + getProduct().getArticleNumber());
        System.out.println("Description        : " + getProduct().getDescription());
        System.out.println("Amount             : " + getAmount());
        System.out.println("Image              : " + getProduct().getImage());
        System.out.println("Expired Date       : " + getProduct().getExpiredDate());
        System.out.println("Price              : Rp " + productPrice());
    }

}