package shopping.main;

import shopping.models.Order;
import shopping.models.Product;
import shopping.models.orderItems;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class MainActivity {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        Date date = new Date();

        Product makanan = new Product();
        makanan.setArticleNumber("3321441");
        makanan.setDescription("Nastar");
        makanan.setType("makanan");
        makanan.setPurchasePrice(20000);
        makanan.setImage("nastar.png");
        makanan.setExpiredDate("03/01/2024");

        Product sabun = new Product();
        sabun.setArticleNumber("2345431");
        sabun.setDescription("Lifebouy");
        sabun.setType("sabun");
        sabun.setPurchasePrice(5000);
        sabun.setImage("Lifebouy.png");
        sabun.setExpiredDate("02/01/2030");

        Product minuman = new Product();
        minuman.setArticleNumber("24431123");
        minuman.setDescription("Pocari Sweet");
        minuman.setType("minuman");
        minuman.setPurchasePrice(6000);
        minuman.setImage("pocari.png");
        minuman.setExpiredDate("19/12/2025");

        ArrayList<Order> listOrder = new ArrayList<>();
        String[] Cashier = {"Overste", "Verinika", "Antonio", "Bruce"};

        boolean process = true;
        if (process) {
            do {
                Order order = new Order();

                System.out.println("Shopping Cart");
                int cashierNumber = random.nextInt(4);
                order.setCashierName(Cashier[cashierNumber]);
                System.out.println("Cashier : " + order.getCashierName());
                System.out.println("==================================");

                System.out.println("Product: ");
                System.out.println("1. " + makanan.getDescription() + ""
                        + "\n2. " + sabun.getDescription() + ""
                        + "\n3. " + minuman.getDescription());
                System.out.println("==================================");

                ArrayList<orderItems> orderItems = new ArrayList<>();

                do {
                    System.out.print("Pilih Product : ");
                    int pilihProduct = scanner.nextInt();
                    while (pilihProduct > 3) {
                        System.out.print("Ulangi Pilih Product : ");
                        pilihProduct = scanner.nextInt();
                    }
                    System.out.print("Jumlah Product : ");
                    int jumlahProduct = scanner.nextInt();

                    orderItems orderItem = new orderItems();
                    switch (pilihProduct) {
                        case 1 -> orderItem.setProduct(makanan);
                        case 2 -> orderItem.setProduct(sabun);
                        case 3 -> orderItem.setProduct(minuman);
                    }
                    orderItem.setAmount(jumlahProduct);
                    orderItem.setOrderId("ORDER " + date.getTime());
                    orderItems.add(orderItem);

                    System.out.println("\nBeli Item Lagi? (Y/N)");
                    String cek = scanner.next();
                    if (cek.equalsIgnoreCase("N")) {
                        break;
                    }
                } while (true);
                System.out.println("==================================");
                order.setOrderItem(orderItems);
                order.setOrderDate(date);
                order.setOrderNumber(Math.abs(random.nextInt()));
                order.setBranchId(String.valueOf(Math.abs(random.nextInt())));
                order.setStatusOrder("Ordered");
                order.setPaymentStatus("Unpaid");

                order.print();

                System.out.print("Lanjut Transaksi? (Y/N) : ");
                String lanjut = scanner.next();
                System.out.println("");
                if (lanjut.equalsIgnoreCase("Y")) {
                    while (order.getPaymentStatus().equalsIgnoreCase("unpaid")) {
                        System.out.print("Masukkan nominal : Rp ");
                        double nominal = scanner.nextDouble();
                        if (nominal >= order.payments()) {
                            System.out.println("");
                            System.out.println("Pembayaran berhasil dilakukan!");
                            order.setStatusOrder("Delivered");
                            order.setPaymentStatus("Paid");
                            order.print();
                            System.out.println("Change Payment      : " + (nominal - order.payments()));
                            System.out.println("TERIMA KASIH");
                            System.out.println("");
                            process = false;
                            break;
                        } else if (nominal < order.payments()) {
                            System.out.println("");
                            System.out.println("Uang tidak mencukupi! Silakan input kembali");
                        }
                    }
                } else {
                    System.out.println("Transaksi Batal!");
                    process = false;
                    break;
                }
            } while (process);
        }
    }
}