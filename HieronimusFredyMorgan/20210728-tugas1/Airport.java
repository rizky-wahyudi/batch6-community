import java.util.Date;

public class Airport {
    private String namaPesawat;
    private Date takeoff;

    public String getNamaPesawat() {
        return namaPesawat;
    }

    public void setNamaPesawat(String namaPesawat) {
        this.namaPesawat = namaPesawat;
    }

    public Date getTakeoff() {
        return takeoff;
    }

    public void setTakeoff(Date takeoff) {
        this.takeoff = takeoff;
    }

    public Penumpang checkSecurity(Penumpang penumpang) {
        if (penumpang.getTicket() != null && penumpang.getPassport() != null) {
            System.out.println("Data lengkap!");
            System.out.println("Tiket Keberangkatan :" + penumpang.getTicket());
            System.out.println("Passport Penumpang  :" + penumpang.getPassport());
            System.out.println("Luggage Penumpang   :" + penumpang.getLuggage());
            return penumpang;
        } else {
            System.out.println("Data tidak lengkap!");
            System.out.print("Kekurangan: ");

            if (penumpang.getTicket() == null) {
                System.out.println("Tiket Tidak Ada !");
            }
            if (penumpang.getPassport() == null) {
                System.out.println("Passport Tidak Ada !");
            }
            return null;
        }
    }

    public Penumpang checkImmigration(Penumpang penumpang) {
        if (penumpang.getTicket().compareTo(getTakeoff()) == 0 &&
                penumpang.getExpiredPassport().compareTo(getTakeoff()) == 1) {
            System.out.println("Data lengkap!");
            return penumpang;
        } else {
            if (penumpang.getTicket().compareTo(getTakeoff()) == 1 ||
                    penumpang.getTicket().compareTo(getTakeoff()) == -1) {
                System.out.println("Tiket tidak berlaku");
            }
            if (penumpang.getExpiredPassport().compareTo(getTakeoff()) == -1) {
                System.out.println("Passport tidak berlaku");
            }
        }
        return null;
    }

    public Penumpang checkWaitingRoom(Penumpang penumpang) {
        if (penumpang.getTicket().compareTo(getTakeoff()) == 0 &&
                penumpang.getExpiredPassport().compareTo(getTakeoff()) == 1) {
            int luggage = penumpang.getLuggage();
            if (luggage > 20) {
                System.out.println("Data lengkap, Luggage kena cash!");
                penumpang.setCashLuggage((luggage - 20) * 50);
                penumpang.setLuggage(luggage);
            } else {
                System.out.println("Data lengkap, Luggage tidak kena cash!");
            }

            return penumpang;
        }
        return null;
    }

}
