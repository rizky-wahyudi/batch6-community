import java.util.Date;

public class Penumpang {
    private String nama;
    private Date ticket;
    private String passport;
    private Date expiredPassport;
    private int cashLuggage = 0;
    private int luggage = 0;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Date getTicket() {
        return ticket;
    }

    public void setTicket(Date ticket) {
        this.ticket = ticket;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public int getLuggage() {
        return luggage;
    }

    public void setLuggage(int luggage) {
        this.luggage = luggage;
    }

    public int getCashLuggage() {
        return cashLuggage;
    }

    public void setCashLuggage(int cashLuggage) {
        this.cashLuggage = cashLuggage;
    }

    public Date getExpiredPassport() {
        return expiredPassport;
    }

    public void setExpiredPassport(Date expiredPassport) {
        this.expiredPassport = expiredPassport;
    }
}

