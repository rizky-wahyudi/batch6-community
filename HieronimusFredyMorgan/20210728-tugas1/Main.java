import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Date takeOff = new Date(2021, 07, 28, 10, 0, 0);
        Date ticketPenumpang = new Date(2021, 07, 28, 10, 0, 0);
        Date expiredPassport = new Date(2021, 10, 9);

        Penumpang penumpang = new Penumpang();
        penumpang.setNama("Morgan");
        penumpang.setTicket(ticketPenumpang);
        penumpang.setLuggage(20);
        penumpang.setExpiredPassport(expiredPassport);

        penumpang.setPassport("Morgan");

        Airport airport = new Airport();
        airport.setNamaPesawat("Airbus 340");
        airport.setTakeoff(takeOff);

        System.out.println("===============================\n");
        System.out.println("Data Diri: ");
        System.out.println("Nama Penumpang      :" + penumpang.getNama());
        System.out.println("Tiket Keberangkatan :" + penumpang.getTicket());
        System.out.println("Passport Penumpang  :" + penumpang.getPassport());
        System.out.println("Expired Passport    :" + penumpang.getExpiredPassport());
        System.out.println("Luggage Penumpang   :" + penumpang.getLuggage());
        System.out.println("\n===============================\n");
        System.out.println("Pesawat: ");
        System.out.println("Nama Pesawat        :" + airport.getNamaPesawat());
        System.out.println("Keberangkatan       :" + airport.getTakeoff());
        System.out.println("\n===============================\n");
        System.out.println("Check Security:");
        penumpang = airport.checkSecurity(penumpang);
        System.out.println("\n===============================\n");

        System.out.println("Check Immigration:");
        if (penumpang != null) {
            penumpang = airport.checkImmigration(penumpang);
            System.out.println("\n===============================\n");
        } else {
            System.exit(0);
        }

        if (penumpang != null) {
            System.out.println("Waiting Room");
            penumpang = airport.checkWaitingRoom(penumpang);
            System.out.println("Biaya Tambahan Luggage: $" + penumpang.getCashLuggage());
            System.out.println("\n===============================\n");
        } else {
            System.exit(0);
        }

        System.out.println("Selamat Berlibur");


    }
}
