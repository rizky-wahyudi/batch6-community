package main;

import controller.Controller;

import java.util.Scanner;

public class CalculatorElectric {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Controller controller = new Controller();
        System.out.println("Watts / Volts / Amps / Ohms calculator");
        System.out.println("======================================");
        int count = 0;
        while (count < 2) {
            System.out.println("Input:");
            System.out.println("1. Resistance");
            System.out.println("2. Current");
            System.out.println("3. Voltage");
            System.out.println("4. Power");
            System.out.print("Pilih: ");
            int pilih = scanner.nextInt();
            switch (pilih) {
                case 1:
                    if (controller.getResistance() == 0) {
                        System.out.print("Masukkan nilai Resistance : ");
                        controller.setResistance(scanner.nextInt());
                        count++;
                    } else {
                        System.out.println("nilai Resistance Sudah terisi ! ");
                    }
                    break;

                case 2:
                    if (controller.getCurrent() == 0) {
                        System.out.print("Masukkan nilai Current : ");
                        controller.setCurrent(scanner.nextInt());
                        count++;
                    } else {
                        System.out.println("nilai Current Sudah terisi ! ");
                    }
                    break;

                case 3:
                    if (controller.getVoltage() == 0) {
                        System.out.print("Masukkan nilai Voltage : ");
                        controller.setVoltage(scanner.nextInt());
                        count++;
                    } else {
                        System.out.println("nilai Voltage Sudah terisi ! ");
                    }
                    break;
                case 4:
                    if (controller.getPower() == 0) {
                        System.out.print("Masukkan nilai Power : ");
                        controller.setPower(scanner.nextInt());
                        count++;
                    } else {
                        System.out.println("Nilai Power Sudah terisi ! ");
                    }
                    break;

                default:
                    System.out.println("Salah Input, Ulangi!");
                    break;
            }
        }
        controller.count();
    }
}
