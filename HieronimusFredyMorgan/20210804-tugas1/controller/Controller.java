package controller;

public class Controller {
    private double resistance;
    private double current;
    private double voltage;
    private double power;

    public double getResistance() {
        return resistance;
    }

    public void setResistance(double resistance) {
        this.resistance = resistance;
    }

    public double getCurrent() {
        return current;
    }

    public void setCurrent(double current) {
        this.current = current;
    }

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public void count() {
        if (getCurrent() != 0) {
            if (getPower() != 0) {
                setVoltage(getPower() / getCurrent());
                setResistance(getPower() / Math.pow(getCurrent(), 2));
            } else if (getResistance() != 0) {
                setVoltage(getCurrent() * getResistance());
                setPower(Math.pow(getCurrent(), 2) * getResistance());
            } else if (getVoltage() != 0) {
                setPower(getVoltage() * getCurrent());
                setResistance(getVoltage() / getCurrent());
            }
        } else if (getResistance() != 0) {
            if (getPower() != 0) {
                setVoltage(Math.sqrt(getPower() * getResistance()));
                setCurrent(Math.sqrt(getPower() / getResistance()));
            } else if (getCurrent() != 0) {
                setVoltage(getCurrent() * getResistance());
                setPower(Math.pow(getCurrent(), 2) * getResistance());
            } else if (getVoltage() != 0) {
                setPower(Math.pow(getVoltage(), 2) / getResistance());
                setCurrent(getVoltage() / getResistance());
            }
        } else if (getVoltage() != 0) {
            if (getPower() != 0) {
                setResistance(Math.pow(getVoltage(), 2) / getPower());
                setCurrent(getPower() / getVoltage());
            } else if (getCurrent() != 0) {
                setResistance(getVoltage() / getCurrent());
                setPower(getVoltage() * getCurrent());
            } else if (getResistance() != 0) {
                setPower(Math.pow(getVoltage(), 2) / getResistance());
                setCurrent(getVoltage() / getResistance());
            }
        }
        print();
    }

    public void print() {
        System.out.println("========================================");
        System.out.println("Hasil Perhitungan");
        System.out.printf("%-15s %-1s %-10s\n", "1. Resistance", ": ", getResistance());
        System.out.printf("%-15s %-1s %-10s\n", "2. Current", ": ", getCurrent());
        System.out.printf("%-15s %-1s %-10s\n", "3. Voltage", ": ", getVoltage());
        System.out.printf("%-15s %-1s %-10s\n", "4. Power", ": ", getPower());
    }

}
