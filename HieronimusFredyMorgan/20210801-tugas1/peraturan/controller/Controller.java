package peraturan.controller;

import peraturan.model.Pasal;
import peraturan.model.Peraturan;

import java.util.ArrayList;

public class Controller extends Peraturan {


    public ArrayList<Peraturan> isi() {
        ArrayList<Peraturan> listPeraturan = new ArrayList<>();

        Peraturan peraturan = new Peraturan();
        peraturan.setNama("UU NO. 1 TAHUN 2020");
        peraturan.setJenisPeraturan("Undang-Undang");
        peraturan.setNomorPeraturan("1");
        peraturan.setTahunPeraturan("2020");
        peraturan.setTentang("PENGESAHAN PERSETUJUAN KEMITRAAN EKONOMI KOMPREHENSIF INDONESIA–AUSTRALIA (INDONESIA–AUSTRALIA COMPREHENSIVE ECONOMIC PARTNERSHIP AGREEMENT)");
        peraturan.setTanggalDitetapkan("2020-02-28");
        peraturan.setNomorLN(67);
        peraturan.setNomorTLN(6476);
        peraturan.setTanggalDiundangkan("2020-02-28");

        peraturan.getDetailPeraturan().setMenimbang("Menimbang 1 :Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. " +
                "Pellentesque scelerisque eu purus eu pulvinar. ");
        peraturan.getDetailPeraturan().setMenimbang("Menimbang 2 :Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. " +
                "Pellentesque scelerisque eu purus eu pulvinar. ");
        peraturan.getDetailPeraturan().setMengingat("Mengingat 1: Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. " +
                "Pellentesque scelerisque eu purus eu pulvinar.");

        peraturan.getDetailPeraturan().setMenetapkan(setPasal1());

        Peraturan peraturan1 = new Peraturan();
        peraturan1.setNama("UU NO. 2 TAHUN 2020");
        peraturan1.setJenisPeraturan("Undang-Undang");
        peraturan1.setNomorPeraturan("2");
        peraturan1.setTahunPeraturan("2020");
        peraturan1.setTentang("PENETAPAN PERATURAN PEMERINTAH PENGGANTI UNDANG-UNDANG NOMOR 1 TAHUN 2020 TENTANG KEBIJAKAN KEUANGAN NEGARA DAN STABILITAS SISTEM KEUANGAN UNTUK PENANGANAN PANDEMI CORONA VIRUS DISEASE 2019 (COVID-19) DAN/ATAU DALAM RANGKA MENGHADAPI ANCAMAN YANG MEMBAHAYAKAN PEREKONOMIAN NASIONAL DAN/ATAU STABILITAS SISTEM KEUANGAN MENJADI UNDANG-UNDANG");
        peraturan1.setTanggalDitetapkan("2020-05-16");
        peraturan1.setNomorLN(134);
        peraturan1.setNomorTLN(6516);
        peraturan1.setTanggalDiundangkan("2020-05-18");

        peraturan1.getDetailPeraturan().setMenimbang("Menimbang 1 :Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. " +
                "Pellentesque scelerisque eu purus eu pulvinar. ");
        peraturan1.getDetailPeraturan().setMenimbang("Menimbang 2 :Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. " +
                "Pellentesque scelerisque eu purus eu pulvinar. ");
        peraturan1.getDetailPeraturan().setMengingat("Mengingat 1: Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. " +
                "Pellentesque scelerisque eu purus eu pulvinar.");

        peraturan1.getDetailPeraturan().setMenetapkan(setPasal1());
        peraturan1.getDetailPeraturan().setMenetapkan(setPasal2());

        listPeraturan.add(peraturan);
        listPeraturan.add(peraturan1);

        return listPeraturan;
    }

    public Pasal setPasal1() {
        Pasal pasal = new Pasal();
        pasal.setIsi("Pasal 1 : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque eu purus eu pulvinar. Fusce leo libero, interdum a felis vitae, imperdiet suscipit lectus. ");
        pasal.setSubPasal("Ayat 1 :Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque eu purus eu pulvinar. Fusce leo libero, interdum a felis vitae, imperdiet suscipit lectus. ");
        pasal.setSubPasal("Ayat 2 :Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque eu purus eu pulvinar. Fusce leo libero, interdum a felis vitae, imperdiet suscipit lectus. ");
        pasal.setSubPasal("Ayat 3 :Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque eu purus eu pulvinar. Fusce leo libero, interdum a felis vitae, imperdiet suscipit lectus. ");
        return pasal;
    }

    public Pasal setPasal2() {
        Pasal pasal = new Pasal();
        pasal.setIsi("Pasal 2 : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque eu purus eu pulvinar. Fusce leo libero, interdum a felis vitae, imperdiet suscipit lectus. ");
        pasal.setSubPasal("Ayat 1 :Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque eu purus eu pulvinar. Fusce leo libero, interdum a felis vitae, imperdiet suscipit lectus. ");
        pasal.setSubPasal("Ayat 2 :Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque eu purus eu pulvinar. Fusce leo libero, interdum a felis vitae, imperdiet suscipit lectus. ");
        pasal.setSubPasal("Ayat 3 :Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque eu purus eu pulvinar. Fusce leo libero, interdum a felis vitae, imperdiet suscipit lectus. ");
        return pasal;
    }

    public void printJudul(ArrayList<Peraturan> peraturan) {
        System.out.println("");
        for (int i = 0; i < peraturan.size(); i++) {
            System.out.println("Peraturan " + (i + 1));
            System.out.println("Peraturan : " + peraturan.get(i).getNama());
            System.out.println("Tentang   : " + peraturan.get(i).getTentang());
            System.out.println("-----------------------------------------------");
        }
    }

    public void cetakDetail(Peraturan peraturan) {
        Peraturan peraturan1 = peraturan;
        System.out.println("jenis Peraturan   : " + peraturan1.getJenisPeraturan());
        System.out.println("Nomor Peraturan   : " + peraturan1.getNomorPeraturan());
        System.out.println("Tahun Peraturan   : " + peraturan1.getTahunPeraturan());
        System.out.println("Tentang Peraturan : " + peraturan1.getTentang());
        System.out.println("Tanggal ditetapkan: " + peraturan1.getTanggalDitetapkan());
        System.out.println("Nomor LN          : " + peraturan1.getNomorLN());
        System.out.println("Nomor TLN         : " + peraturan1.getNomorTLN());
        System.out.println("Tanggal ditetapkan: " + peraturan1.getTanggalDitetapkan());
        System.out.println("");
        System.out.println("Menimbang:");
        for (int i = 0; i < peraturan1.getDetailPeraturan().getMenimbang().size(); i++) {
            System.out.println(peraturan1.getDetailPeraturan().getMenimbang().get(i));
        }
        System.out.println("\n");
        System.out.println("Mengingat:");

        System.out.println(peraturan1.getDetailPeraturan().getMengingat());
        System.out.println("Menetapkan:");
        for (int i = 0; i < peraturan1.getDetailPeraturan().getMenetapkan().size(); i++) {
            System.out.println(peraturan1.getDetailPeraturan().getMenetapkan().get(i).getIsi());
            System.out.println("\n");

            for (int j = 0; j < peraturan1.getDetailPeraturan().getMenetapkan().get(i).getSubPasal().size(); j++) {
                System.out.println(peraturan1.getDetailPeraturan().getMenetapkan().get(i).getSubPasal().get(j).getIsi());
            }
            System.out.println("\n");
        }
    }
}
