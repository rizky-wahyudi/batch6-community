package peraturan.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class DetailPeraturan {
    private ArrayList<String> menimbang = new ArrayList<>();
    private String mengingat;
    private ArrayList<Pasal> menetapkan = new ArrayList<>();

    public ArrayList<String> getMenimbang() {
        return menimbang;
    }

    public void setMenimbang(String menimbang) {
        this.menimbang.add(menimbang);
    }

    public String getMengingat() {
        return mengingat;
    }

    public void setMengingat(String mengingat) {
        this.mengingat = mengingat;
    }

    public ArrayList<Pasal> getMenetapkan() {
        return menetapkan;
    }

    public void setMenetapkan(ArrayList<Pasal> menetapkan) {
        this.menetapkan = menetapkan;
    }

    public void setMenetapkan(Pasal isi) {
        this.menetapkan.add(isi);
    }
}
