package peraturan.model;

public class SubPasal {
    private String isi;

    public SubPasal(String isi) {
        this.isi = isi;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
