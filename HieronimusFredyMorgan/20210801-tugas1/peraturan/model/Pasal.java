package peraturan.model;

import java.util.ArrayList;
import java.util.HashMap;

public class Pasal {
    private String isi;
    private ArrayList<SubPasal> subPasal = new ArrayList<>();


    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public ArrayList<SubPasal> getSubPasal() {
        return subPasal;
    }

    public void setSubPasal(String subPasal) {
        this.subPasal.add(new SubPasal(subPasal));
    }
}
