package peraturan.model;

public class Peraturan {
    private String nama;
    private String jenisPeraturan;
    private String nomorPeraturan;
    private String tahunPeraturan;
    private String tentang;
    private String tanggalDitetapkan;
    private int nomorLN;
    private int nomorTLN;
    private String tanggalDiundangkan;
    private DetailPeraturan detailPeraturan = new DetailPeraturan();

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisPeraturan() {
        return jenisPeraturan;
    }

    public void setJenisPeraturan(String jenisPeraturan) {
        this.jenisPeraturan = jenisPeraturan;
    }

    public String getNomorPeraturan() {
        return nomorPeraturan;
    }

    public void setNomorPeraturan(String nomorPeraturan) {
        this.nomorPeraturan = nomorPeraturan;
    }

    public String getTahunPeraturan() {
        return tahunPeraturan;
    }

    public void setTahunPeraturan(String tahunPeraturan) {
        this.tahunPeraturan = tahunPeraturan;
    }

    public String getTentang() {
        return tentang;
    }

    public void setTentang(String tentang) {
        this.tentang = tentang;
    }

    public String getTanggalDitetapkan() {
        return tanggalDitetapkan;
    }

    public void setTanggalDitetapkan(String tanggalDitetapkan) {
        this.tanggalDitetapkan = tanggalDitetapkan;
    }

    public int getNomorLN() {
        return nomorLN;
    }

    public void setNomorLN(int nomorLN) {
        this.nomorLN = nomorLN;
    }

    public int getNomorTLN() {
        return nomorTLN;
    }

    public void setNomorTLN(int nomorTLN) {
        this.nomorTLN = nomorTLN;
    }

    public String getTanggalDiundangkan() {
        return tanggalDiundangkan;
    }

    public void setTanggalDiundangkan(String tanggalDiundangkan) {
        this.tanggalDiundangkan = tanggalDiundangkan;
    }

    public DetailPeraturan getDetailPeraturan() {
        return detailPeraturan;
    }

    public void setDetailPeraturan(DetailPeraturan detailPeraturan) {
        this.detailPeraturan = detailPeraturan;
    }
}
