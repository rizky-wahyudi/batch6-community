package peraturan.main;

import peraturan.controller.Controller;
import peraturan.model.Peraturan;

import java.util.ArrayList;
import java.util.Scanner;

public class MainActivity {
    public static void main(String[] args) {
        Controller controller = new Controller();
        ArrayList<Peraturan> peraturan;
        peraturan = controller.isi();
        controller.printJudul(peraturan);
        Scanner scanner = new Scanner(System.in);
        System.out.print("Lihat Detail No (1/2): ");
        int no = scanner.nextInt();
        System.out.println("===============================");
        switch (no) {
            case 1 -> controller.cetakDetail(peraturan.get(0));
            case 2 -> controller.cetakDetail(peraturan.get(1));
            default -> System.out.println("Salah input!");
        }
    }
}
