package id.co.nexsoft.vending.controller;

import id.co.nexsoft.vending.entity.Vending;
import id.co.nexsoft.vending.repository.VendingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class VendingController {
    @Autowired
    VendingRepository vendingRepository;

    @GetMapping("/vending")
    public List<Vending> getAllNotes() {
        return vendingRepository.findAll();
    }

    @GetMapping("/vending/{id}")
    public Vending getVendingById(@PathVariable(value = "id") int id) {
        return vendingRepository.findById(id);
    }

    @PostMapping(value = "/vending", consumes = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    public Vending addVending(@RequestBody Vending Vending) {
        return vendingRepository.save(Vending);
    }

    @DeleteMapping("/delete/vending/{id}")
    public void deleteVending(@PathVariable(value = "id") int id) {
        vendingRepository.deleteById(id);
    }

    @PutMapping("/vending/{id}")
    public ResponseEntity<Object> updateVending(@RequestBody Vending Vending, @PathVariable int id) {

        Optional<Vending> VendingRepo = Optional.ofNullable(vendingRepository.findById(id));

        if (!VendingRepo.isPresent())
            return ResponseEntity.notFound().build();

        Vending.setId(id);
        vendingRepository.save(Vending);
        return ResponseEntity.noContent().build();
    }
}
