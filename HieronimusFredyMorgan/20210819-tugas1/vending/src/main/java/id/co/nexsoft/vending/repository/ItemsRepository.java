package id.co.nexsoft.vending.repository;

import id.co.nexsoft.vending.entity.ItemsProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemsRepository extends CrudRepository<ItemsProduct, Integer> {
    ItemsProduct findById(int id);

    List<ItemsProduct> findAll();

    void deleteById(int id);
}