package id.co.nexsoft.vending.entity;

import javax.persistence.*;

@Entity
public class Vending {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int moneyMachine = 50000;

    public Vending() {
    }

    public Vending(int id, int moneyMachine) {
        this.id = id;
        this.moneyMachine = moneyMachine;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
