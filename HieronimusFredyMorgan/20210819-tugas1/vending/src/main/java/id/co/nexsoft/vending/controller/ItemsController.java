package id.co.nexsoft.vending.controller;

import id.co.nexsoft.vending.entity.ItemsProduct;
import id.co.nexsoft.vending.repository.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static id.co.nexsoft.vending.entity.Product.*;

@RestController
public class ItemsController {
    @Autowired
    ItemsRepository itemsRepository;

    @GetMapping("/items")
    public List<ItemsProduct> getAllNotes() {
        return itemsRepository.findAll();
    }

    @GetMapping("/items/{id}")
    public ItemsProduct getItemsById(@PathVariable(value = "id") int id) {
        return itemsRepository.findById(id);
    }

    @PostMapping(value = "/items")
    @ResponseStatus(HttpStatus.CREATED)
    public void addItems() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 10; j++) {
                if (i == 0)
                    itemsRepository.save(new ItemsProduct(row1[j], 10, 3000, i + "" + j));
                if (i == 1)
                    itemsRepository.save(new ItemsProduct(row2[j], 10, 13000, i + "" + j));
                if (i == 2)
                    itemsRepository.save(new ItemsProduct(row3[j], 10, 1000, i + "" + j));
                if (i == 3)
                    itemsRepository.save(new ItemsProduct(row4[j], 10, 4000, i + "" + j));
                if (i == 4)
                    itemsRepository.save(new ItemsProduct(row5[j], 10, 10000, i + "" + j));
                if (i == 5)
                    itemsRepository.save(new ItemsProduct(row6[j], 10, 5000, i + "" + j));
            }
        }
    }

    @DeleteMapping("/delete/items/{id}")
    public void deleteItems(@PathVariable(value = "id") int id) {
        itemsRepository.deleteById(id);
    }

    @PutMapping("/items/{id}")
    public String updateItems(@PathVariable int id) {
        ItemsProduct itemsProduct = getItemsById(id);
        Optional<ItemsProduct> ItemsRepo = Optional.ofNullable(itemsRepository.findById(id));
        if (!ItemsRepo.isPresent()) {
            return "Data tidak tersedia!";
        }
        if (ItemsRepo.get().getJumlah() > 0) {
            itemsProduct.setId(id);
            itemsProduct.setJumlah(ItemsRepo.get().getJumlah() - 1);
            itemsRepository.save(itemsProduct);
            return "Berhasil : " + itemsProduct.toString();
        }
        return "Data habis : " + itemsProduct.toString();
    }

    @PutMapping("/items/restock/{id}")
    public String updateStockItems(@PathVariable int id) {
        ItemsProduct itemsProduct = getItemsById(id);
        Optional<ItemsProduct> ItemsRepo = Optional.ofNullable(itemsRepository.findById(id));
        if (!ItemsRepo.isPresent()) {
            return "Data tidak tersedia!";
        }
        itemsProduct.setJumlah(10);
        itemsRepository.save(itemsProduct);
        return "Berhasil Restock: " + itemsProduct.toString();

    }
}
