package id.co.nexsoft.vending.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ItemsProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private int jumlah;
    private int harga;
    private String rowItems;

    public ItemsProduct(String name, int jumlah, int harga, String rowItems) {
        this.name = name;
        this.jumlah = jumlah;
        this.harga = harga;
        this.rowItems = rowItems;
    }

    public ItemsProduct() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getRowItems() {
        return rowItems;
    }

    public void setRowItems(String rowItems) {
        this.rowItems = rowItems;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", jumlah=" + jumlah +
                ", harga=" + harga +
                '}';
    }
}