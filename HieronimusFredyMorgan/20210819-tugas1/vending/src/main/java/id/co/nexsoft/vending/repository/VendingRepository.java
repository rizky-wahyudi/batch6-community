package id.co.nexsoft.vending.repository;

import id.co.nexsoft.vending.entity.Vending;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VendingRepository extends CrudRepository<Vending, Integer> {
    Vending findById(int id);

    List<Vending> findAll();

    void deleteById(int id);
}