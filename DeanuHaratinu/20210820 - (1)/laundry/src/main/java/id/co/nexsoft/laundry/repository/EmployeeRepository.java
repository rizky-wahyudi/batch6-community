package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.Address;
import id.co.nexsoft.laundry.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

    Employee findById(int id);
}
