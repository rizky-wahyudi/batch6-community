package id.co.nexsoft.laundry.controller;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import id.co.nexsoft.laundry.model.*;
import id.co.nexsoft.laundry.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LaundryController {

    @Autowired
    private AddressRepository addressRepo;
    @Autowired
    private CustomerRepository customerRepo;
    @Autowired
    private EmployeeRepository employeeRepo;
    @Autowired
    private ServiceRepository serviceRepo;
    @Autowired
    private OrderItemRepository orderItemRepo;
    @Autowired
    private OrderRepository orderRepo;

    // Home page
    @GetMapping("/")
    public String welcomePage() {
        return "<html><body>"
                + "<h1>Home page</h1>"
                + "</body></html>";
    }

    // Save a list of address
    @PostMapping("/address")
    @ResponseStatus(HttpStatus.CREATED)
    public Iterable<Address> addAddress(@RequestBody List<Address> address) {
        return addressRepo.saveAll(address);
    }

    // Get all address in list
    @GetMapping("/address")
    @ResponseBody
    public Iterable<Address> getAllAddress() {
        return addressRepo.findAll();
    }

    // Post customer detail with addressId as foreign key
    @PostMapping("/customer/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Customer addCustomer(@RequestBody Customer customer, @PathVariable(value = "id") int id) {
        Address address = addressRepo.findById(id);
        customer.setAddress(address);
        return customerRepo.save(customer);
    }

    // Get all customer in list
    @GetMapping("/customer")
    @ResponseBody
    public Iterable<Customer> getAllCustomer() {
        return customerRepo.findAll();
    }

    // Post employee detail with addressId as foreign key
    @PostMapping("/employee/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee addEmployee(@RequestBody Employee employee, @PathVariable(value = "id") int id) {
        Address address = addressRepo.findById(id);
        employee.setAddress(address);
        return employeeRepo.save(employee);
    }

    // Get all employee in list
    @GetMapping("/employee")
    @ResponseBody
    public Iterable<Employee> getAllEmployee() {
        return employeeRepo.findAll();
    }

    // Post service detail
    @PostMapping("/service")
    @ResponseStatus(HttpStatus.CREATED)
    public Service addService(@RequestBody Service service) {
        return serviceRepo.save(service);
    }

    // Get all service in list
    @GetMapping("/service")
    @ResponseBody
    public Iterable<Service> getAllService() {
        return serviceRepo.findAll();
    }


    @PostMapping(value = "laundryorder/employee:{employeeId}/service:{serviceId}", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public LaundryOrder addLaundryOrder(@RequestBody LaundryOrder laundryOrder,
                                                  @PathVariable(value = "employeeId") int ei,
                                                  @PathVariable(value = "serviceId") int si) {

        List<OrderItem> orderItemList = laundryOrder.getOrderItems();

        for (OrderItem orderItem : orderItemList) {
            orderItem.setService(serviceRepo.findById(si));
            orderItemRepo.save(orderItem);
        }
        laundryOrder.setUpdateBy(employeeRepo.findById(ei));

        return orderRepo.save(laundryOrder);
    }

    @GetMapping("/laundryorder")
    @ResponseBody
    public Iterable<LaundryOrder> getAllOrder() {
        return orderRepo.findAll();
    }
}
