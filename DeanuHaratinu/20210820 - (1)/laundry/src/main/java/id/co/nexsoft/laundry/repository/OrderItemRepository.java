package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.OrderItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemRepository extends CrudRepository<OrderItem, Integer> {

}
