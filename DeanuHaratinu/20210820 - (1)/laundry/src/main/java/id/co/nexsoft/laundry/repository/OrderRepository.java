package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.LaundryOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<LaundryOrder, Integer> {

}
