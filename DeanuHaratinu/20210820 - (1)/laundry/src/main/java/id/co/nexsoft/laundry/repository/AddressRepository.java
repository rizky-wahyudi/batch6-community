package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

    Address findById(int id);

}
