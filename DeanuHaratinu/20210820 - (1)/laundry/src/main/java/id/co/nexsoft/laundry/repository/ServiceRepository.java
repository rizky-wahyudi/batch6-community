package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.model.Address;
import id.co.nexsoft.laundry.model.Service;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends CrudRepository<Service, Integer> {

    Service findById(int id);
}
