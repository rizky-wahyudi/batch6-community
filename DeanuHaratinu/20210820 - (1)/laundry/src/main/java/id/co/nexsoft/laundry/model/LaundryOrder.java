package id.co.nexsoft.laundry.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class LaundryOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @OneToMany(targetEntity = OrderItem.class)
    private List<OrderItem> orderItems;
    private LocalDate orderDate;
    private String status;
    private String paymentStatus;
    private LocalDate lastUpdateDate;
    @ManyToOne(targetEntity = Employee.class)
    private Employee updateBy;

    public LaundryOrder() {
    }

    public LaundryOrder(int id, List<OrderItem> orderItems, LocalDate orderDate,
                        String status, String paymentStatus, LocalDate lastUpdateDate, Employee updateBy) {
        this.id = id;
        this.orderItems = orderItems;
        this.orderDate = orderDate;
        this.status = status;
        this.paymentStatus = paymentStatus;
        this.lastUpdateDate = lastUpdateDate;
        this.updateBy = updateBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public LocalDate getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDate lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Employee getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Employee updateBy) {
        this.updateBy = updateBy;
    }
}
