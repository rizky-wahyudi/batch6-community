package airplane.model;

interface Economy {
    double ecoprice = 1;
    double ecoluggage = 1;
    double ecoseats = 0.65 ;

    double getEcoSeats();
    double getEcoPrice(int price);
    double getEcoLuggage();
}
