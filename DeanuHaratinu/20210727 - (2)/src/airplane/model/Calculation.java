package airplane.model;

public class Calculation {

    public static double profit(String type, int rute) {
        double totalprofit = 0;

        if (type.equals("A380")) {
            A380 a380 = new A380();
            double ecoprice = a380.getEcoPrice(rute);
            double busiprice = a380.getBusiPrice(rute);
            double fcprice = a380.getFcPrice(rute);
            totalprofit = ecoprice * a380.getEcoSeats();
            totalprofit += busiprice * a380.getBusiSeats();
            totalprofit += fcprice * a380.getEcoSeats();
        } else if (type.equals("B747")) {
            B747 b747 = new B747();
            double ecoprice = b747.getEcoPrice(rute);
            double busiprice = b747.getBusiPrice(rute);
            double fcprice = b747.getFcPrice(rute);
            totalprofit = ecoprice * b747.getEcoSeats();
            totalprofit += busiprice * b747.getBusiSeats();
            totalprofit += fcprice * b747.getEcoSeats();
        } else if (type.equals("B787")) {
            B787 b787 = new B787();
            double ecoprice = b787.getEcoPrice(rute);
            double busiprice = b787.getBusiPrice(rute);
            double fcprice = b787.getFcPrice(rute);
            totalprofit = ecoprice * b787.getEcoSeats();
            totalprofit += busiprice * b787.getBusiSeats();
            totalprofit += fcprice * b787.getEcoSeats();
        }
        return totalprofit;
    }

    public static double luggage(String type, int rute) {
        double totallug = 0;
        if (type.equals("A380")) {
            A380 a380 = new A380();
            double ecolug = a380.getEcoLuggage();
            double busilug = a380.getBusiLuggage();
            double fclug = a380.getFcLuggage();
            totallug = ecolug * a380.getEcoSeats();
            totallug += busilug * a380.getBusiSeats();
            totallug += fclug * a380.getFcSeats();
        } else if (type.equals("B747")) {
            B747 b747 = new B747();
            double ecolug = b747.getEcoLuggage();
            double busilug = b747.getBusiLuggage();
            double fclug = b747.getFcLuggage();
            totallug = ecolug * b747.getEcoSeats();
            totallug += busilug * b747.getBusiSeats();
            totallug += fclug * b747.getFcSeats();

        } else if (type.equals("B787")) {
            B787 b787 = new B787();
            double ecolug = b787.getEcoLuggage();
            double busilug = b787.getBusiLuggage();
            double fclug = b787.getFcLuggage();
            totallug = ecolug * b787.getEcoSeats();
            totallug += busilug * b787.getBusiSeats();
            totallug += fclug * b787.getFcSeats();
        }
        return totallug;
    }
}
