package airplane.model;

interface Business {
    double busiprice = 2.25;
    double busiluggage = 2;
    double busiseats = 0.25 ;

    double getBusiSeats();
    double getBusiPrice(int price);
    double getBusiLuggage();
}
