package airplane.model;

public class A380 implements Economy, Business, FirstClass {
    public int seats = 750;
    int luggage = 20;

    @Override
    public double getEcoSeats() {
        return ecoseats * seats;
    }

    @Override
    public double getEcoPrice(int price) {
        return ecoprice * price;
    }

    @Override
    public double getEcoLuggage() {
        return ecoluggage * luggage;
    }

    @Override
    public double getBusiSeats() {
        return busiseats * seats;
    }

    @Override
    public double getBusiPrice(int price) {
        return busiprice * price;
    }

    @Override
    public double getBusiLuggage() {
        return busiluggage * luggage;
    }

    @Override
    public double getFcSeats() {
        return fcseats * seats;
    }

    @Override
    public double getFcPrice(int price) {
        return fcprice * price;
    }

    @Override
    public double getFcLuggage() {
        return fcluggage * luggage;
    }

}
