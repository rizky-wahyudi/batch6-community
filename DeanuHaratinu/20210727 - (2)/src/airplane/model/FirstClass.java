package airplane.model;

interface FirstClass {
    double fcprice = 3;
    double fcluggage = 2.5;
    double fcseats = 0.1 ;

    double getFcSeats();
    double getFcPrice(int price);
    double getFcLuggage();
}