package airplane.controller;

import airplane.model.*;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int eurasia = 500;
        int asiaeur = 600;
        int eurus = 650;

        System.out.println("Masukkan jenis pesawat: ");
        String planetype = sc.nextLine().toUpperCase();
     //   String planetype = "A380";

        if (planetype.equals("A380")) {
            System.out.print("Total profit rute Europe - Asia: $ ");
            System.out.println(Calculation.profit("A380", eurasia));
            System.out.print("Total luggage rute Europe - Asia: ");
            System.out.println(Calculation.luggage("A380", eurasia) + " kg");
            System.out.println("=========================");

            System.out.println("Total profit rute Asia - Europe: $ ");
            System.out.println(Calculation.profit("A380", asiaeur));
            System.out.print("Total luggage rute Asia - Europe: ");
            System.out.println(Calculation.luggage("A380", asiaeur) + " kg");
            System.out.println("=========================");

            System.out.print("Total profit rute Europe - US: $ ");
            System.out.println(Calculation.profit("A380", eurus));
            System.out.print("Total luggage rute Europe - US: ");
            System.out.println(Calculation.luggage("A380", eurus) + " kg");
        } else if (planetype.equals("B747")) {
            System.out.print("Total profit rute Europe - Asia: $ ");
            System.out.println(Calculation.profit("B747", eurasia));
            System.out.print("Total luggage rute Europe - Asia: ");
            System.out.println(Calculation.luggage("B747", eurasia) + " kg");
            System.out.println("=========================");

            System.out.print("Total profit rute Asia - Europe: $ ");
            System.out.println(Calculation.profit("B747", asiaeur));
            System.out.print("Total luggage rute Asia - Europe: ");
            System.out.println(Calculation.luggage("B747", asiaeur) + " kg");
            System.out.println("=========================");

            System.out.print("Total profit rute Europe - US: $ ");
            System.out.println(Calculation.profit("B747", eurus));
            System.out.print("Total luggage rute Europe - US: ");
            System.out.println(Calculation.luggage("B747", eurus) + " kg");
        } else if (planetype.equals("B787")) {
            System.out.print("Total profit rute Europe - Asia: $ ");
            System.out.println(Calculation.profit("B787", eurasia));
            System.out.print("Total luggage rute Europe - Asia: ");
            System.out.println(Calculation.luggage("B787", eurasia) + " kg");
            System.out.println("=========================");

            System.out.print("Total profit rute Asia - Europe: $ ");
            System.out.println(Calculation.profit("B787", asiaeur));
            System.out.print("Total luggage rute Asia - Europe: ");
            System.out.println(Calculation.luggage("B787", asiaeur) + " kg");
            System.out.println("=========================");

            System.out.print("Total profit rute Europe - US: $ ");
            System.out.println(Calculation.profit("B787", eurus));
            System.out.print("Total luggage rute Europe - US: ");
            System.out.println(Calculation.luggage("B787", eurus) + " kg");
        } else {
            System.out.println("Pilihan tidak ada");
        }
    }
}
