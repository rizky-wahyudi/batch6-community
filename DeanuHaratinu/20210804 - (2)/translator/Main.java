package translator;

import java.util.Scanner;

public class Main {

    public static void main (String[] args) {
        int option = 0;
        String satuan;
        double besaran;
        boolean choosing = true;

        Scanner sc = new Scanner(System.in);

        System.out.println("====================== Konversi ======================");
        System.out.println("1. Konversi Kilometer - Mile");
        System.out.println("2. Konversi Celcius   - Fahrenheit");
        System.out.println("====================== Konversi ======================");

        while (!(option > 0 && option < 3)) {
            System.out.print("Silakan pilih konversi yang ingin dilakukan (1-2): ");
            option = sc.nextInt();
        }

        // Memilih konversi Kilometer - Mile
        if (option == 1) {
            while (choosing) {
                System.out.print("Masukan satuan yang diketahui (km / m): ");
                satuan = sc.next();
                System.out.print("Masukan besaran yang diketahui: ");
                besaran = sc.nextDouble();
                if (satuan.equalsIgnoreCase("m") || satuan.equalsIgnoreCase(("km"))) {
                    double result = convertKilometerMile(satuan, besaran);
                    System.out.print("Untuk " + besaran + " " + satuan + " = ");
                    System.out.printf("%.3f \n", result, satuan);
                    choosing = false;
                } else {
                    choosing = true;
                    System.out.println("Pilihan salah");
                }
            }
        } else if (option == 2) {
            while (choosing) {
                System.out.print("Masukan satuan yang diketahui (F / C): ");
                satuan = sc.next();
                System.out.print("Masukan besaran yang diketahui: ");
                besaran = sc.nextDouble();
                if (satuan.equalsIgnoreCase("f") || satuan.equalsIgnoreCase(("c"))) {
                    double result = convertCelciusFahrenhet(satuan, besaran);
                    System.out.print("Untuk " + besaran + " " + satuan + " = ");
                    System.out.printf("%.3f \n", result);
                    choosing = false;
                } else {
                    choosing = true;
                    System.out.println("Pilihan salah");
                }
            }
        } else {
            System.out.println("Pilihan salah");
        }
        System.out.println("==== end of program ===");
    }

    static double convertKilometerMile (String satuan, double besaran) {
        double result = 0;
        if (satuan.equalsIgnoreCase("km")) {
            result = DistanceConvert.kilometerToMile(besaran);
        } else if (satuan.equalsIgnoreCase("m")){
            result = DistanceConvert.mileToKilometer(besaran);
        }
        return result;
    }

    static double convertCelciusFahrenhet (String satuan, double besaran) {
        double result = 0;
        if (satuan.equalsIgnoreCase("f")) {
            result = TemperatureConvert.fahrenheitToCelcius(besaran);
        } else if (satuan.equalsIgnoreCase("c")){
            result = TemperatureConvert.celciusToFahrenheit(besaran);
        }
        return result;
    }

}
