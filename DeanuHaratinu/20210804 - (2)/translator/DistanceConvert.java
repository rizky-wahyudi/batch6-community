package translator;

public class DistanceConvert {
    static double kilometer;
    static double mile;

    static double kilometerToMile (double kilometer) {
        mile = kilometer / 1.609;
        return mile;
    }

    static double mileToKilometer (double mile) {
        kilometer = mile * 1.609;
        return kilometer;
    }

}
