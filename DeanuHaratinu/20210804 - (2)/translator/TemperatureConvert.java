package translator;

public class TemperatureConvert {
    static double celcius;
    static double fahrenheit;

    static double celciusToFahrenheit (double c) {
        fahrenheit = (c * (9/5)) + 32;
        return fahrenheit;
    }

    static double fahrenheitToCelcius (double f) {
        celcius = (f - 32) * (5/9);
        return celcius;
    }

}
