package id.co.nexsoft.vendingmachine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class VendingProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    String vendingColumn;
    int vendingRow;
    String name;
    int stock;
    double price;

    public VendingProduct() {
    }

    public VendingProduct(String vendingColumn, int vendingRow, String name, int stock, double price) {
        this.vendingColumn = vendingColumn;
        this.vendingRow = vendingRow;
        this.name = name;
        this.stock = stock;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVendingColumn() {
        return vendingColumn;
    }

    public void setVendingColumn(String vendingColumn) {
        this.vendingColumn = vendingColumn;
    }

    public int getVendingRow() {
        return vendingRow;
    }

    public void setVendingRow(int vendingRow) {
        this.vendingRow = vendingRow;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
