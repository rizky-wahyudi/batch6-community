package id.co.nexsoft.vendingmachine.repository;

import id.co.nexsoft.vendingmachine.model.VendingProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<VendingProduct, Integer> {

//    VendingProduct findById(int id);
    VendingProduct findByVendingColumnAndVendingRow(String col, int row);
    List<VendingProduct> findAll();
    List<VendingProduct> findAllByPriceIsGreaterThanEqual(double price);
    List<VendingProduct> findAllByName(String name);
}
