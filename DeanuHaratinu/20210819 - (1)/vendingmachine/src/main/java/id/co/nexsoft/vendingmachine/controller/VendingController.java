package id.co.nexsoft.vendingmachine.controller;

import id.co.nexsoft.vendingmachine.model.VendingProduct;
import id.co.nexsoft.vendingmachine.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class VendingController {

    @Autowired
    private ProductRepository repo;

    // Home Page
    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
                + "<h1>Home page</h1>"
                + "</body></html>";
    }

    // Get All product
    @GetMapping("/product")
    public List<VendingProduct> getAllProduct() {
        return repo.findAll();
    }

    //greaterthanequals
    @GetMapping("/product/{price}")
    public List<VendingProduct> getAllByprice(@RequestParam(value = "price") double price){
        return repo.findAllByPriceIsGreaterThanEqual(price);
    }

    // Get row by col and id
    @GetMapping("/product/row:{row}/col:{column}")
    @ResponseBody
    public VendingProduct getProduct(@PathVariable(value = "column") String column,
                                     @PathVariable(value = "row") int row) {
        return repo.findByVendingColumnAndVendingRow(column, row);
    }

    // Post product
    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public VendingProduct addProduct(@RequestBody VendingProduct vendingProduct) {
        return repo.save(vendingProduct);
    }

    @PutMapping("/buy/row:{row}/col:{column}")
    public ResponseEntity<VendingProduct> buyProduct(@PathVariable(value = "column") String column,
                                             @PathVariable(value = "row") int row) {

        Optional<VendingProduct> productRepo = Optional.ofNullable(
                repo.findByVendingColumnAndVendingRow(column, row));

        if (!productRepo.isPresent())
            return ResponseEntity.notFound().build();

        // subtract the stock
        VendingProduct vendingProd = repo.findByVendingColumnAndVendingRow(column, row);
        if (vendingProd.getStock() >= 1) {
            vendingProd.setStock(vendingProd.getStock() - 1);
        } else {
            return ResponseEntity.notFound().build();
        }

        repo.save(vendingProd);

        return new  ResponseEntity<VendingProduct>(repo.findByVendingColumnAndVendingRow(column, row),HttpStatus.OK);
    }
}
