package id.co.nexsoft.controller;

import id.co.nexsoft.model.product.Product;
import id.co.nexsoft.service.LogisticService;
import id.co.nexsoft.service.ProductService;
import id.co.nexsoft.model.transport.Transport;
import id.co.nexsoft.model.warehouse.Address;
import id.co.nexsoft.model.warehouse.DistanceMapper;
import id.co.nexsoft.model.warehouse.Warehouse;
import id.co.nexsoft.service.TransportService;
import id.co.nexsoft.service.WarehouseService;

import java.util.ArrayList;

public class Main {

    static ArrayList<Warehouse> warehouseList = new ArrayList<>();
    static ArrayList<Address> addressList = new ArrayList<>();
    static ArrayList<DistanceMapper> distanceList = new ArrayList<>();
    static ArrayList<Product> productsList = new ArrayList<>();
    static ArrayList<Transport> transportsList = new ArrayList<>();

    public static void main(String[] args) {
        int from = 3;
        int to = 2;

        System.out.println("start of program\n");

        // Setting the warehouse and addresses
        startWarehouse();
        WarehouseService warehouseService = new WarehouseService();
        warehouseService.setWarehouse(warehouseList);
//        warehouseService.showWarehouse();

        warehouseService.setDistanceList(distanceList);

        // Setting the product
        startProduct();
        ProductService productService = new ProductService();
        productService.setProducts(productsList);
        productService.setAddressList(addressList);
//        productService.showProducts();

        // Starting the transport
        startTransport();
        TransportService ts = new TransportService();
        ts.setTransport(transportsList);
//        ts.showTransport();

        // Show the logistic
        LogisticService ls = new LogisticService();
        ls.setLogisticService(warehouseList);
        ls.showLogistic(from, to);
        System.out.println("distance        : " + warehouseService.checkDistance(from, to));
        ts.showTransport();

        System.out.println("end of program");
    }

    public static void startWarehouse() {
        // Set warehouse and address
        Warehouse warehouse = new Warehouse();

        Address address = new Address();
        address.setId(1);
        address.setStreet("Jl. Keruwing");
        address.setDistrict("Kab. Barito Kuala");
        address.setProvince("Kalimantan Selatan");
        address.setCountry("Indonesia");
        address.setLongitude(-6.255315);
        address.setLatitude(106.615067);

        warehouse.setId(1);
        warehouse.setAddress(address);
        warehouseList.add(warehouse);

        address = new Address();
        address.setId(2);
        address.setStreet("Jl. Soekarno-Hatta");
        address.setDistrict("Kab. Sidoarjo");
        address.setProvince("Jawa Timur");
        address.setCountry("Indonesia");
        address.setLongitude(-6.255315);
        address.setLatitude(106.615067);
        addressList.add(address);

        warehouse = new Warehouse();
        warehouse.setId(2);
        warehouse.setAddress(address);
        warehouseList.add(warehouse);

        address = new Address();
        address.setId(3);
        address.setStreet("Jl. Prof. Dr. Satrio");
        address.setDistrict("Kec. Setiabudi");
        address.setProvince("DKI Jakarta");
        address.setCountry("Indonesia");
        address.setLongitude(-6.224945);
        address.setLatitude(106.828406);
        addressList.add(address);

        warehouse = new Warehouse();
        warehouse.setId(3);
        warehouse.setAddress(address);
        warehouseList.add(warehouse);

        // Set distanceMapper
        DistanceMapper distance = new DistanceMapper(1, 1, 2, 2000);
        distanceList.add(distance);
        distance = new DistanceMapper(2, 2, 1, 2000);
        distanceList.add(distance);
        distance = new DistanceMapper(2, 2, 3, 780);
        distanceList.add(distance);
        distance = new DistanceMapper(2, 3, 2, 780);
        distanceList.add(distance);
        distance = new DistanceMapper(2, 1, 3, 2780);
        distanceList.add(distance);
        distance = new DistanceMapper(2, 3, 1, 2780);
        distanceList.add(distance);
    }

    public static void startTransport() {
        double weight = 0;

        Transport transport = new Transport();
        transport.setId(1);
        transport.setDriver("Andarukma Pratama");
        transport.setSize(1); // productId = 1
        transport.setSpeed(3);
        weight = TransportService.checkWeight(transport.getSize(),productsList);
        transport.setLoad(weight);
        transport.setTransportType(TransportService.checkTransportType(weight));
        transportsList.add(transport);
    }

    public static void startProduct() {
        Product product = new Product();
        product.setId(1);
        product.setName("Furniture");
        product.setDeliverAddress(3);
        product.setWidth(100);
        product.setHeight(200);
        product.setDepth(150);
        product.setWeight(1000);
        product.setAmount(10);
        productsList.add(product);
    }
}
