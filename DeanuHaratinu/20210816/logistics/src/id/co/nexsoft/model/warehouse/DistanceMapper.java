package id.co.nexsoft.model.warehouse;

public class DistanceMapper {

    private int id;
    private int fromWarehouseId;
    private int toWarehouseId;
    private double distance;

    public DistanceMapper(int id, int fromWarehouseId, int toWarehouseId, double distance) {
        this.id = id;
        this.fromWarehouseId = fromWarehouseId;
        this.toWarehouseId = toWarehouseId;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public int getFromWarehouseId() {
        return fromWarehouseId;
    }

    public int getToWarehouseId() {
        return toWarehouseId;
    }

    public double getDistance() {
        return distance;
    }

}
