package id.co.nexsoft.service;

import id.co.nexsoft.model.product.Product;
import id.co.nexsoft.model.transport.Transport;
import id.co.nexsoft.model.warehouse.Address;

import java.util.ArrayList;

public class TransportService {

    static private ArrayList<Transport> transport = new ArrayList<>();

    public static ArrayList<Transport> getTransport() {
        return transport;
    }

    public void setTransport(ArrayList<Transport> transport) {
        this.transport = transport;
    }

    public static double checkWeight(int size, ArrayList<Product> productList) {
        double weight = 0;
        for (Product product : productList) {
            if (size == product.getId()) {
                weight = product.getWeight() * product.getAmount();
            }
        }
        return weight;
    }

    public static String checkTransportType(double weight) {
        if (weight > 0 && weight <= 50) {
            return "Motor Cycle";
        } else if (weight <= 2000) {
            return "Car";
        } else if (weight <= 10_000) {
            return "Truck";
        } else {
            return "No available transport type";
        }
    }

    public void showTransport() {
        for (Transport transport : getTransport()) {
//            System.out.println("id              : " + transport.getId());
            System.out.println("driver          : " + transport.getDriver());
//            System.out.println("size            : " + transport.getSize());
            System.out.println("speed           : " + transport.getSpeed() + " days");
            System.out.println("transport type  : " + transport.getTransportType());
            System.out.println("load            : " + transport.getLoad() + " kg");
            System.out.println(" ");
        }
    }
}
