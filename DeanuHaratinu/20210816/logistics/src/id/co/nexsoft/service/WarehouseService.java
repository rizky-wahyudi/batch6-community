package id.co.nexsoft.service;

import id.co.nexsoft.model.warehouse.DistanceMapper;
import id.co.nexsoft.model.warehouse.Warehouse;
import java.util.ArrayList;

public class WarehouseService {

    private ArrayList<Warehouse> warehouse;
    private ArrayList<DistanceMapper> distanceList;

    public ArrayList<DistanceMapper> getDistanceList() {
        return distanceList;
    }

    public void setDistanceList(ArrayList<DistanceMapper> distanceList) {
        this.distanceList = distanceList;
    }

    public ArrayList<Warehouse> getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(ArrayList<Warehouse> warehouse) {
        this.warehouse = warehouse;
    }

    public void showWarehouse() {
        for (Warehouse warehouses : getWarehouse()) {
            System.out.println("id          : " + warehouses.getId());
            System.out.println("street      : " + warehouses.getAddress().getStreet());
            System.out.println("district    : " + warehouses.getAddress().getDistrict());
            System.out.println("province    : " + warehouses.getAddress().getProvince());
            System.out.println("country     : " + warehouses.getAddress().getCountry());
            System.out.println(" ");
        }
    }

    public double checkDistance(int from, int to) {
        double distance = 0;

        for (int i = 0; i < getDistanceList().size(); i++) {
            if (from == getDistanceList().get(i).getFromWarehouseId()) {
                if (to == getDistanceList().get(i).getToWarehouseId()) {
                    distance = getDistanceList().get(i).getDistance();
                }
            }
        }

        return distance;
    }

}
