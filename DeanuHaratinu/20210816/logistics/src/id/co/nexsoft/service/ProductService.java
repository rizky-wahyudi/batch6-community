package id.co.nexsoft.service;

import id.co.nexsoft.model.product.Product;
import id.co.nexsoft.model.warehouse.Address;
import java.util.ArrayList;

public class ProductService {

    private ArrayList<Product> products = new ArrayList<>();
    static private ArrayList<Address> addressList = new ArrayList<>();


    public static ArrayList<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(ArrayList<Address> addressList) {
        this.addressList = addressList;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public void showProducts() {
        for (Product product : getProducts()) {
            System.out.println("id          : " + product.getId());
            System.out.println("name        : " + product.getName());
            checkAddress(product.getId());
            System.out.println("width       : " + product.getWidth());
            System.out.println("height      : " + product.getHeight());
            System.out.println("depth       : " + product.getDepth());
            System.out.println("weight      : " + product.getWeight());
            System.out.println("amount      : " + product.getAmount());
            System.out.println(" ");
        }
    }

    public static void checkAddress(int to) {
        for (Address address : getAddressList()) {
            if (to == address.getId()) {
                System.out.println("delivery address: " + address.getStreet());
                System.out.println("delivery district: " + address.getDistrict());
                System.out.println("delivery province: " + address.getProvince());
                System.out.println("delivery country: " + address.getCountry());
            }
        }
    }
}
