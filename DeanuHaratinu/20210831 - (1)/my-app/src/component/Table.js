function Table() {
  const data = [{
    id : 1,
    name : "Deanu",
    age : 18,
    email: "tangerang@gmail.com"
  },
  {
    id : 2,
    name : "Arif",
    age : 18,
    email: "tangerang@gmail.com"
  },
  {
    id : 3,
    name : "Ezra",
    age : 18,
    email: "tangerang@gmail.com"
  },
  {
    id : 4,
    name : "Zelda",
    age : 18,
    email: "tangerang@gmail.com"
  },
  {
    id : 5,
    name : "Izul Ganteng",
    age : 18,
    email: "tangerang@gmail.com"
  },
  {
    id : 6,
    name : "Izul Keren",
    age : 18,
    email: "tangerang@gmail.com"
  }
]

  return (
    <div>
    <h1 id='title'>React Dynamic Table</h1>
    <table id='students'>
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Age</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
        {
          data.map((item, index) => {
            return (
              <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.age}</td>
                <td>{item.email}</td>
              </tr>
            )
          })
        }
      </tbody>
    </table>
    </div>
    )
}

export default Table;