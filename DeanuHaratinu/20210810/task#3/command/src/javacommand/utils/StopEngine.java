package javacommand.utils;

import javacommand.utils.Command;
import javacommand.utils.Engine;

public class StopEngine implements Command {

    // Concrete command

    private Engine engine;

    public StopEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void execute() {
        engine.turnOff();
    }
}
