package javacommand.utils;

public class Engine {

    // Receiver

    public void turnOn() {
        System.out.println("The engine is started");
    }

    public void turnOff() {
        System.out.println("The engine is stopped");
    }
}
