package javacommand.utils;

public interface Command {

    void execute();
}
