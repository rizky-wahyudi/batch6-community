package javacommand.utils;

import javacommand.utils.Command;
import javacommand.utils.Engine;

public class StartEngine implements Command {

    // Concrete command

    private Engine engine;

    public StartEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void execute() {
        engine.turnOn();
    }
}
