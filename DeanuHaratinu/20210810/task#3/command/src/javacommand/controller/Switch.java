package javacommand.controller;

import javacommand.utils.Command;

import java.util.ArrayList;
import java.util.List;

public class Switch {

    // Invoker

    private List<Command> history = new ArrayList<Command>();
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void executeCommand(Command command) {
        this.history.add(command);
        command.execute();
    }
}
