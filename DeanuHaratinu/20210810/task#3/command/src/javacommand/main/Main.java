package javacommand.main;

import javacommand.utils.Command;
import javacommand.utils.Engine;
import javacommand.utils.StartEngine;
import javacommand.utils.StopEngine;
import javacommand.controller.Switch;

public class Main {

    public static void main(String[] args) {
        String str = "on";

        Engine engine = new Engine();
        Command start = new StartEngine(engine);
        Command stop = new StopEngine(engine);

        Switch switcher = new Switch();

        try {
            if (str.equalsIgnoreCase("ON")) {
                switcher.executeCommand(start);
            } else if (str.equalsIgnoreCase("OFF")) {
                switcher.executeCommand(stop);
            } else {
                System.out.println("Invalid input");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
