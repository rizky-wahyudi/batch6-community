package javafactory.model;

public class Truck implements Vehicle {

    @Override
    public void getVehicle() {
        System.out.println("You are making a truck from the vehicle factory");
    }
}
