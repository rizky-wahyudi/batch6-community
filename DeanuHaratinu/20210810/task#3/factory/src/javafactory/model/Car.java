package javafactory.model;

public class Car implements Vehicle {

    @Override
    public void getVehicle() {
        System.out.println("You are making a car from the vehicle factory");
    }
}
