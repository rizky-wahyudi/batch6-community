package javafactory.model;

public class Motorcycle implements Vehicle{

    @Override
    public void getVehicle() {
        System.out.println("You are making a motorcycle from the vehicle factory");
    }
}
