package javafactory.utils;

import javafactory.model.Car;
import javafactory.model.Motorcycle;
import javafactory.model.Truck;
import javafactory.model.Vehicle;

public class VehicleFactory {

    public static Vehicle getVehicle(String vehicleType){
        if (vehicleType.equalsIgnoreCase("car")) {
            return new Car();
        }
        else if (vehicleType.equalsIgnoreCase("motorcycle"))  {
            return new Motorcycle();
        }
        else if (vehicleType.equalsIgnoreCase("truck")) {
            return new Truck();
        }
        return null;
    }

}
