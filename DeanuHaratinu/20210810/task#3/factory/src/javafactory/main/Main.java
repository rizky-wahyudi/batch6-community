package javafactory.main;

import javafactory.model.Vehicle;
import javafactory.utils.VehicleFactory;

public class Main {

    public static void main(String[] args) {
        Vehicle car = VehicleFactory.getVehicle("Car");
        System.out.println("---------");
        car.getVehicle();

        Vehicle motorcycle = VehicleFactory.getVehicle("Motorcycle");
        System.out.println("---------");
        motorcycle.getVehicle();

        Vehicle truck = VehicleFactory.getVehicle("Truck");
        System.out.println("---------");
        truck.getVehicle();
    }
}
