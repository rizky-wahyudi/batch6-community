package javadecorator.utils;

import javadecorator.model.Vehicle;

public class Decorator implements Vehicle {

    public Vehicle vehicle;

    public Decorator(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public void getVehicle() {
        vehicle.getVehicle();
    }
}
