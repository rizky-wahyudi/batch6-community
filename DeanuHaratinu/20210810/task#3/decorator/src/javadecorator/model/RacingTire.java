package javadecorator.model;

import javadecorator.utils.Decorator;

public class RacingTire extends Decorator {

    public RacingTire(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public void getVehicle() {
        vehicle.getVehicle();
        decorateRacingTire();
    }

    public void decorateRacingTire(){
        System.out.print(" + modifikasi tire");
    }
}
