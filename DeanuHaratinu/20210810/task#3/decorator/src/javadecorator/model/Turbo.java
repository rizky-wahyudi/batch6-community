package javadecorator.model;

import javadecorator.utils.Decorator;

public class Turbo extends Decorator {

    public Turbo(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public void getVehicle() {
        vehicle.getVehicle();
        decorateTurbo();
    }

    public void decorateTurbo(){
        System.out.print(" + modifikasi turbo");
    }
}
