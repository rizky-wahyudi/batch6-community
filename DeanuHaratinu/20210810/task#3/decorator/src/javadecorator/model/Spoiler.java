package javadecorator.model;

import javadecorator.utils.Decorator;

public class Spoiler extends Decorator {


    public Spoiler (Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public void getVehicle() {
        vehicle.getVehicle();
        decorateSpoiler();
    }

    public void decorateSpoiler(){
        System.out.print(" + modifikasi spoiler");
    }
}
