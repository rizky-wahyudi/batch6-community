package javadecorator.main;

import javadecorator.model.*;

public class Main {

    public static void main(String[] args) {
        Vehicle car = new Car();
        car.getVehicle(); // Mobil saja

        System.out.println("\n");
        Vehicle carModif1 = new Turbo(new Car());
        carModif1.getVehicle();

        System.out.println("\n");
        Vehicle carModif2 = new Turbo(new RacingTire(new Car()));
        carModif2.getVehicle(); // turbo + racing tire

        System.out.println("\n");
        Vehicle carModif3 = new Turbo(new Spoiler(new RacingTire(new Car())));
        carModif3.getVehicle(); // turbo + spoiler + racing tire


    }
}
