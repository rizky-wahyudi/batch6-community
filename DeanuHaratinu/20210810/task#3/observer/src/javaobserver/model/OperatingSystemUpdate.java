package javaobserver.model;

public class OperatingSystemUpdate implements Observer {

    private Observable observable;

    public OperatingSystemUpdate(Observable observable) {
        this.observable = observable;
    }

    @Override
    public void update(String update) {
        System.out.println("===============================");
        System.out.println("There is new update for your OS");
        System.out.println(update);
        System.out.println("===============================");
    }
}
