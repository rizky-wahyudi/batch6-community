package javaobserver.model;

public interface Observer {

    void update(String update);
}
