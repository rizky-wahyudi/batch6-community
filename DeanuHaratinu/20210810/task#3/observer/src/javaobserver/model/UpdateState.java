package javaobserver.model;

import java.util.ArrayList;

public class UpdateState implements Observable {

    private ArrayList<Observer> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObserver(String update) {
        for(Observer observer: observers) {
            observer.update(update);
        }
    }

    public void sendUpdate(String update) {
        notifyObserver(update);
    }
}
