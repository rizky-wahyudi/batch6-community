package javaobserver.main;

import javaobserver.model.OperatingSystemUpdate;
import javaobserver.model.UpdateState;

public class Main {

    public static void main(String[] args) {
        UpdateState upState = new UpdateState();

        OperatingSystemUpdate os1 = new OperatingSystemUpdate(upState);
        OperatingSystemUpdate os2 = new OperatingSystemUpdate(upState);
        OperatingSystemUpdate os3 = new OperatingSystemUpdate(upState);
        OperatingSystemUpdate os4 = new OperatingSystemUpdate(upState);
        OperatingSystemUpdate os5 = new OperatingSystemUpdate(upState);

        upState.addObserver(os1);
        upState.addObserver(os2);
        upState.addObserver(os3);
        upState.addObserver(os4);
        upState.addObserver(os5);


        upState.sendUpdate("Update 14.2 - summer release");
    }
}
