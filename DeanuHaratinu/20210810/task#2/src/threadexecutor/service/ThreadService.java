package threadexecutor.service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ThreadService {

    public static void threadForRead (String threadName, String taskName) {
        String dir = "D:\\Community\\batch6-community\\DeanuHaratinu\\20210810\\task#2\\testfiles\\";
        String[] listfile;

        // List semua file di folder testfiles
        Path path = Paths.get(dir);
        if (Files.exists(path)) {
            File file = new File(dir);
            listfile = file.list();

            try {
                for (String filename : listfile) {
                    File txtfile = new File(dir + filename);
                    BufferedReader br = new BufferedReader(new FileReader(txtfile));
                    String str;
                    while ((str = br.readLine()) != null) {
                        System.out.println("Task-" + taskName + "     | " + threadName + " isi: " + str);
                    }
                }
            } catch (NullPointerException | IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Folder tidak ada di path");
        }
    }

}
