package threadexecutor.main;

import threadexecutor.service.ThreadService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    private final static int POOL_SIZE = 2;
    private final static int NUMBER_OF_TASKS = 1000;

    public static void main(String[] args) {
        ExecutorService executor1 = Executors.newFixedThreadPool(POOL_SIZE);
        ExecutorService executor2 = Executors.newFixedThreadPool(POOL_SIZE);

        for (int i = 0; i < NUMBER_OF_TASKS; i++) {
            int taskNumber = i;
            executor1.submit(() -> {
                String taskName = "exe#1-" + taskNumber;
                String threadName = Thread.currentThread().getName();
                ThreadService.threadForRead(threadName, taskName);
            });

            executor2.submit(() -> {
                String taskName = "exe#2-" + taskNumber;
                String threadName = Thread.currentThread().getName();
                ThreadService.threadForRead(threadName, taskName);
            });

        }

        executor1.shutdown();
        executor2.shutdown();
        System.out.println("End of program");
    }

}
