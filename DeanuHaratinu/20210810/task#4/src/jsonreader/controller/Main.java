package jsonreader.controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String dir = "D:\\Community\\batch6-community\\DeanuHaratinu\\20210810\\task#4\\jsonfiles\\";

        for (int i = 1; i <= 5; i++){
            String jsonUrl = "https://jsonplaceholder.typicode.com/todos/" + i;
            String txtname = dir + "file-" + i + ".json";

            try {
                URL url = new URL(jsonUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();

                if (conn.getResponseCode() == 200) {
                    BufferedWriter writetxt = new BufferedWriter(new FileWriter(txtname));
                    Scanner scan = new Scanner(url.openStream());

                    while (scan.hasNext()) {
                        String temp = scan.nextLine();
                        System.out.println(temp);
                        writetxt.write(temp);
                        writetxt.newLine();
                    }
                    System.out.println("File-" + i + ".json telah dibuat.\n");
                    writetxt.close();
                } else {
                    System.out.println("Connection unauthorized");
                }
                conn.disconnect();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
