package testunit.formative.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    @Test
    @DisplayName("-Add: AssertTrue & AssertEquals")
    public void testAddSuccess() {
        assertEquals(3, Calculator.add(1, 2));
        assertTrue(Calculator.add(10, 100) == 110);
    }

    @Test
    @DisplayName("-Add: AssertFalse & AssertThrown")
    public void testAddFailure() {
        assertFalse(Calculator.add(10, 100) == 120);
        assertFalse(Calculator.add(5, 1) == 7);
        assertThrows(NumberFormatException.class, () -> {
            Calculator.add(Integer.parseInt("b"), 1);
            });
    }

    @Test
    @DisplayName("-Subt: AssertTrue & AssertEquals")
    public void testSubtractSuccess() {
        assertEquals(5, Calculator.subtract(10, 5));
        assertTrue(Calculator.subtract(100, 100) == 0);
    }

    @Test
    @DisplayName("-Subt: AssertFalse & AssertThrown")
    public void testSubtractFailure() {
        assertFalse(Calculator.subtract(10, 100) == 90);
        assertFalse(Calculator.subtract(100, 5) == 80);
        assertThrows(NumberFormatException.class, () -> {
            Calculator.subtract(10, Integer.parseInt("b"));
        });
    }

    @Test
    @DisplayName("-Multi: AssertTrue & AssertEquals")
    public void testMultiplySuccess() {
        assertEquals(0, Calculator.multiply(10, 0));
        assertTrue(Calculator.multiply(-1, -1) == 1);
    }

    @Test
    @DisplayName("-Multi: AssertFalse & AssertThrown")
    public void testMultiplyFailure() {
        assertFalse(Calculator.multiply(-10, -2) == -20);
        assertFalse(Calculator.multiply(10, 2) == -20);
        assertThrows(NumberFormatException.class, () -> {
            Calculator.multiply(20, Integer.parseInt("AB"));
        });
    }

    @Test
    @DisplayName("-Divide: AssertTrue & AssertEquals")
    public void testDivideSuccess() {
        assertEquals(0, Calculator.divide(0, 5));
        assertTrue(Calculator.divide(-5, 2) == -2.5);
    }

    @Test
    @DisplayName("-Divide: AssertFalse & AssertThrown")
    public void testDivideFailure() {
        assertFalse(Calculator.divide(-10, -2) == -5);
        assertFalse(Calculator.divide(-13, 2) == 7.5);
        assertThrows(ArithmeticException.class, () -> {
            Calculator.divide(20, 0);
        });
    }
}