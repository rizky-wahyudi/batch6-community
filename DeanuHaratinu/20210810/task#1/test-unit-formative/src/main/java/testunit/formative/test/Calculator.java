package testunit.formative.test;


public class Calculator {

    public static int add(int numOne, int numTwo) {
        return numOne + numTwo;
    }

    public static int subtract(int numOne, int numTwo) {
        return (numOne - numTwo);
    }

    public static int multiply(int numOne, int numTwo) {
        return (numOne * numTwo);
    }

    public static double divide(int numOne, int numTwo) {
        if (numTwo == 0) {
            throw  new ArithmeticException("Divide by zero");
        } else {
            return (double) numOne / numTwo;
        }
    }

    public static void main(String[] args) {
        int numOne = Integer.parseInt(args[0]);
        int numTwo = Integer.parseInt(args[1]);
        add(numOne, numTwo);
        subtract(numOne, numTwo);
        multiply(numOne, numTwo);
        divide(numOne, numTwo);

        System.out.println("Hello World!");
    }
}
