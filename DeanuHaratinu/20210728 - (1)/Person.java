public class Person {
    private boolean ticket = false;
    private boolean pasport = false;
    private boolean pasValid= false;
    private int luggage = 0;
    private String lugDetails;
    private String handLug;
    private boolean personAct = false;

    public void setPersonAct(boolean personAct) {
        this.personAct = personAct;
    }

    public boolean getPersonAct() {
        return personAct;
    }

    public void setTicket(boolean ticket) {
        this.ticket = ticket;
    }

    public boolean getTicket() {
        return ticket;
    }

    public void setPasport(boolean pasport) {
        this.pasport = pasport;
    }

    public boolean getPasport() {
        return pasport;
    }

    public void setPasValid(boolean pasValid) {
        this.pasValid = pasValid;
    }

    public boolean getPasValid() {
        return pasValid;
    }

    public void setLuggage(int luggage) {
        this.luggage = luggage;
    }

    public int getLuggage() {
        return luggage;
    }

    public void setLugDetails(String lugDetails) {
        this.lugDetails = lugDetails;
    }

    public String getLugDetails() {
        return lugDetails;
    }

    public void setHandLug(String handLug) {
        this.handLug = handLug;
    }

    public String getHandLug() {
        return handLug;
    }
}
