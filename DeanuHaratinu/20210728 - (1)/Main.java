import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        String[] terlarang = {"Pisau", "Gunting"};

        Person person = new Person();

        person.setTicket(true);
        person.setPasport(true);
        person.setPasValid(false);
        person.setHandLug("Gunting");
        person.setLuggage(10);
        person.setLugDetails(null);
        person.setPersonAct(true);

        // Xray - 1
        System.out.println("Process Xray - 1");
        if (Arrays.asList(terlarang).contains(person.getLugDetails())) {
            System.out.println("Ada barang terlarang");
            System.out.println("Anda tidak boleh masuk ke Counter Airline");
            person.setPersonAct(false);
        } else {
            System.out.println("Silakan lanjut ke Counter Airline");
            System.out.println("-------------------");
        }

        // Counter Airline
        if (person.getPersonAct()) {
            System.out.println("Process Counter Airline");
            if (person.getTicket() && person.getPasport() && person.getLuggage() > 0) {
                int lugPrice = person.getLuggage() * 10;
                System.out.println("Biaya atas Luggage Anda : $" + lugPrice);
                System.out.println("Tiket dan Pasport Anda sesuai");
                System.out.println("Silakan lanjut ke Imigrasi");
                System.out.println("-------------------");

            } else if (person.getTicket() && person.getPasport()) {
                System.out.println("Tiket dan Pasport Anda sesuai");
                System.out.println("Silakan lanjut ke Imigrasi");
                System.out.println("-------------------");
            }
        } else {
            person.setPersonAct(false);
        }

        // Imigrasi
        if (person.getPersonAct()) {
            System.out.println("Process Imigrasi");
            if (person.getTicket() && person.getPasport() && person.getPasValid()) {
                System.out.println("Tiket dan Pasport Anda telah sesuai");
                System.out.println("Silakan lanjut ke Waiting Room");
                System.out.println("-------------------");
            } else if (person.getTicket() && person.getPasport() && !person.getPasValid()) {
                System.out.println("Pasport Anda tidak valid");
                System.out.println("Anda tidak diperkenankan ke Waiting Room");
                System.out.println("-------------------");
                person.setPersonAct(false);
            }
        } else {
            person.setPersonAct(false);
        }

        // Waiting Room

        if (person.getPersonAct()) {
            System.out.println("Process Waiting Room");
            if (person.getTicket() && person.getPasport()) {
                if (Arrays.asList(terlarang).contains(person.getHandLug())) {
                    System.out.println("Ada barang terlarang di Hand Luggage Anda");
                    System.out.println("Anda tidak diperkenankan masuk pesawat");
                    System.out.println("-------------------");
                    person.setPersonAct(false);
                }
            } else {
                System.out.println("Tiket, pasport, dan hand luggage anda telah sesuai");
                System.out.println("Silakan masuk ke pesawat");
                System.out.println("-------------------");
            }
        } else {
            person.setPersonAct(false);
        }
    }
}