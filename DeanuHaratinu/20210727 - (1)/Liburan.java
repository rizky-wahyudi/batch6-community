import java.util.Scanner;

public class Liburan {
	int day;
	int orang;
    String tujuan;
	
    public static void main(String[] args) {
		int a, b;
		String c;
		
        Scanner scanner = new Scanner(System.in);
	
        System.out.print("Tujuan :");
        c = scanner.nextLine();
        System.out.print("Hari :");
        b = scanner.nextInt();
        System.out.print("Orang :");
        a = scanner.nextInt();

		if (c.equalsIgnoreCase("Tokyo")) {
            Tokyo tokyo = new Tokyo();
			tokyo.result(b, a);
        } else if (c.equalsIgnoreCase("New York")) {
            NewYork ny = new NewYork();
			ny.result(b, a);
        } else if (c.equalsIgnoreCase("Bandung")) {
            Bandung bdg = new Bandung();
			bdg.result(b, a);
        } else {
            System.out.println("Tujuan Tidak Ada");
        }
    }
}