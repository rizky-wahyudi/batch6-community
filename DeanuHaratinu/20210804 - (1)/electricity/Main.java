package electricity;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main (String[] args) {
        String[] satuan = {"Ampere", "Resistance", "Voltage", "Power"};
        Scanner sc = new Scanner(System.in);
        int satuan1 = 0;
        int satuan2 = 0;
        double besaran1 = 0;
        double besaran2 = 0;

        System.out.println("====================== Konversi ======================");
        int no = 1;
        for (String i : satuan) {
            System.out.println(no + ". " + i);
            no++;
        }
        System.out.println("====================== Konversi ======================");

        while (!(satuan1 > 0 && satuan1 < 5) && !(satuan2 > 0 && satuan1 < 5)) {
            System.out.print("Masukan dua satuan listrik yang ingin digunakan (1-4): ");
            satuan1 = sc.nextInt();
            System.out.print("dan: ");
            satuan2 = sc.nextInt();
        }

        System.out.println("======================================================");
        while (!(besaran1 > 0) && !(besaran2 > 0)) {
            System.out.println("Masukan besaran listrik: ");
            System.out.print("Untuk satuan " + satuan[satuan1-1] + ": ");
            besaran1 = sc.nextDouble();
            System.out.print("Untuk satuan " + satuan[satuan2-1] + ": ");
            besaran2 = sc.nextDouble();
        }
        System.out.println("======================================================");

        ArrayList<Double> result;
        if (satuan1 == 1 && satuan2 == 2) {
            // I-R -> P-V
            result = Konversi.calculatePandV(besaran1, besaran2);
            display(result, satuan);
        } else if (satuan1 == 1 && satuan2 == 3) {
            // I-V -> R-P
            result = Konversi.calculateRandP(besaran2, besaran1);
            display(result, satuan);
        } else if (satuan1 == 1 && satuan2 == 4) {
            // I-P -> R-V
            result = Konversi.calculateRandV(besaran1, besaran2);
            display(result, satuan);
        } else if (satuan1 == 2 && satuan2 == 3) {
            // R-V -> I-P
            result = Konversi.calculateAandP(besaran1, besaran2);
            display(result, satuan);
        } else if (satuan1 == 2 && satuan2 == 4) {
            // R-P -> I-V
            result = Konversi.calculateAandV(besaran1, besaran2);
            display(result, satuan);
        } else if (satuan1 == 3 && satuan2 == 4) {
            // V-P -> R-I
            result = Konversi.calculateRandA(besaran1, besaran2);
            display(result, satuan);
        } else {
            System.out.println("Pilihan tidak tepat");
        }

        System.out.println("================= end of program ==================");
    }

    static void display (ArrayList<Double> result, String[] satuan) {
        for (int i = 0; i < result.size(); i++) {
            System.out.println(satuan[i] + " = " + result.get(i));
        }
    }



}
