package electricity;

import java.util.ArrayList;

public class Konversi {

    public static ArrayList<Double> calculatePandV (double i, double r) {
        double p = (Math.pow(i, 2) * r);
        double v = i * r;

        ArrayList<Double> resultList = new ArrayList<>();
        resultList.add(i);
        resultList.add(r);
        resultList.add(p);
        resultList.add(v);

        return resultList;
    }

    public static ArrayList<Double> calculateRandP (double v, double i) {
        double r = (v / i);
        double p = v * i;

        ArrayList<Double> resultList = new ArrayList<>();
        resultList.add(i);
        resultList.add(r);
        resultList.add(p);
        resultList.add(v);

        return resultList;
    }

    public static ArrayList<Double> calculateRandA (double v, double p) {
        double r = (Math.pow(v, 2) / p);
        double i = (p / v);

        ArrayList<Double> resultList = new ArrayList<>();
        resultList.add(i);
        resultList.add(r);
        resultList.add(p);
        resultList.add(v);

        return resultList;
    }

    public static ArrayList<Double> calculateAandP (double r, double v) {
        double i = (v / r);
        double p = (Math.pow(v, 2) * r);

        ArrayList<Double> resultList = new ArrayList<>();
        resultList.add(i);
        resultList.add(r);
        resultList.add(p);
        resultList.add(v);

        return resultList;
    }

    public static ArrayList<Double> calculateRandV (double i, double p) {
        double r = (p / Math.pow(i, 2));
        double v = (p / i);

        ArrayList<Double> resultList = new ArrayList<>();
        resultList.add(i);
        resultList.add(r);
        resultList.add(p);
        resultList.add(v);

        return resultList;
    }

    public static ArrayList<Double> calculateAandV (double r, double p) {
        double i = Math.sqrt(p / r);
        double v = Math.sqrt(p * r);

        ArrayList<Double> resultList = new ArrayList<>();
        resultList.add(i);
        resultList.add(r);
        resultList.add(p);
        resultList.add(v);

        return resultList;
    }


}
