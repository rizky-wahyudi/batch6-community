package shoppingcart.controller;

import shoppingcart.model.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Date;



public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean orderState = true, choosingItem = true, paymentState = false;
        int orderNumb = 1;
        String cashier = "Sulis";

        ArrayList<Product> content = Main.setProduct();
        //      System.out.println(Main.setProduct().get(0).getDesc());

        while (orderState == true) {
            Order order = new Order();
            order.setCashier(cashier);

            System.out.println("------------------------");
            System.out.println("Mini market Sinar Dunia");
            System.out.println("Daftar Barang: ");
            System.out.println("------------------------");

            for (int i = 0; i < content.size(); i++) {
                System.out.print((i + 1) + ". " + content.get(i).getDesc() + "\n");
            }

            ArrayList<Integer> productOptions = new ArrayList<>();
            ArrayList<Integer> amounts = new ArrayList<>();

            while (choosingItem == true) {
                System.out.println("------------------------");
                System.out.print("Silakan pilih barang (1-9): ");
                int option = sc.nextInt();
                System.out.print("Silakan masukan jumlah barang: ");
                int amount = sc.nextInt();

                if (option > 0 && option < 10) {
                    productOptions.add(option);
                    amounts.add(amount);
                } else if (option <= 0) {
                    System.out.println("Pilihan tidak ada");
                } else if (option > 9) {
                    System.out.println("Pilihan tidak ada");
                }

                System.out.print("Selesai memilih? (y/n): ");
                String choosingstate = sc.next();

                if (choosingstate.equalsIgnoreCase("y")) {
                    choosingItem = false;
                } else if (choosingstate.equalsIgnoreCase("n")) {
                    choosingItem = true;
                } else {
                    // do nothing, belum diisi
                }
            }

            System.out.println("================");
            System.out.println("Daftar pesanan: ");
            System.out.println("================");

            OrderItem[] listOrder = new OrderItem[productOptions.size()];
            order.setBranchId("MM SD - 001");
            order.setPaymentStatus("Unpaid");

            inputToListOrder(productOptions, amounts, content, listOrder);

            order.setOrderItem(listOrder);
            order.setOrderNum(orderNumb);
            Date date = new Date();
            order.setOrderDate(date);

            order.finalOrder();

            while (paymentState == false) {
                System.out.print("Silakan lakukan pembayaran: ");
                double pay = sc.nextDouble();

                if (pay == order.priceIncludeTax()) {
                    order.setPaymentStatus("Paid");
                    System.out.println("Pembayaran: " + order.getPaymentStatus());
                    paymentState = true;
                } else if (pay >= 0 && pay < order.priceIncludeTax()) {
                    System.out.println("Uang tidak cukup ");
                    paymentState = false;
                } else if (pay > order.priceIncludeTax()) {
                    order.setPaymentStatus("Paid");
                    System.out.println("Pembayaran: " + order.getPaymentStatus());
                    System.out.println("Uang Anda berlebih sebesar: Rp" + (pay - order.priceIncludeTax()));
                    paymentState = true;
                }
            }

            System.out.print("Ingin belanja kembali? (y/n): ");
            String reorder = sc.next();
            if (reorder.equalsIgnoreCase("y")) {
                orderNumb++;
                orderState = true;
                choosingItem = true;
                paymentState = false;
            } else if (reorder.equalsIgnoreCase("n")) {
                orderState = false;
            }
        }
    }

    static ArrayList<Product> setProduct () {
        ArrayList<Product> products = new ArrayList<Product>();

        Product mnm1 = new Product();
        mnm1.setArticle("98782108714");
        mnm1.setDesc("Coca Cola");
        mnm1.setType("minuman");
        mnm1.setExp("01-12-2021");
        mnm1.setPurchase(7000);
        mnm1.setImg("image/cocacola.png");
        products.add(mnm1);

        Product mnm2 = new Product();
        mnm2.setArticle("98782179822");
        mnm2.setDesc("Fanta");
        mnm2.setType("minuman");
        mnm2.setExp("10-10-2021");
        mnm2.setPurchase(6500);
        mnm2.setImg("image/fanta.png");
        products.add(mnm2);

        Product mnm3 = new Product();
        mnm3.setArticle("98782145798");
        mnm3.setDesc("Sprite");
        mnm3.setType("minuman");
        mnm3.setExp("20-12-2021");
        mnm3.setPurchase(7000);
        mnm3.setImg("image/sprite.png");
        products.add(mnm3);

        Product mkn1 = new Product();
        mkn1.setArticle("9821012348");
        mkn1.setDesc("Indomie Goreng");
        mkn1.setType("makanan");
        mkn1.setExp("01-01-2022");
        mkn1.setPurchase(3000);
        mkn1.setImg("image/indomiegoreng.png");
        products.add(mkn1);

        Product mkn2 = new Product();
        mkn2.setArticle("9821187862");
        mkn2.setDesc("Pop Mie");
        mkn2.setType("makanan");
        mkn2.setExp("10-05-2022");
        mkn2.setPurchase(7000);
        mkn2.setImg("image/popmie.png");
        products.add(mkn2);

        Product mkn3 = new Product();
        mkn3.setArticle("9821148795");
        mkn3.setDesc("Mie Sedap Goreng");
        mkn3.setType("makanan");
        mkn3.setExp("15-02-2022");
        mkn3.setPurchase(2500);
        mkn3.setImg("image/miesedapgoreng.png");
        products.add(mkn3);

        Product sbn1 = new Product();
        sbn1.setArticle("98987412014");
        sbn1.setDesc("Life Buoy");
        sbn1.setType("sabun");
        sbn1.setExp("10-12-2022");
        sbn1.setPurchase(5000);
        sbn1.setImg("image/lifebuoy.png");
        products.add(sbn1);

        Product sbn2 = new Product();
        sbn2.setArticle("98987457890");
        sbn2.setDesc("Shinzui");
        sbn2.setType("sabun");
        sbn2.setExp("10-12-2022");
        sbn2.setPurchase(6000);
        sbn2.setImg("image/shinzui.png");
        products.add(sbn2);

        Product sbn3 = new Product();
        sbn3.setArticle("98997321402");
        sbn3.setDesc("Biore");
        sbn3.setType("sabun");
        sbn3.setExp("02-05-2022");
        sbn3.setPurchase(10000);
        sbn3.setImg("image/biore.png");
        products.add(sbn3);

        return products;
    }

    static void inputToListOrder (ArrayList<Integer> productOptions, ArrayList<Integer> amounts,
                                  ArrayList<Product> content, OrderItem[] listOrder) {
        for (int i = 0; i < productOptions.size(); i++) {
            OrderItem orderItem = new OrderItem();
            if (productOptions.get(i) == 1) {
                orderItem.setProd(content.get(0));
            } else if (productOptions.get(i) == 2) {
                orderItem.setProd(content.get(1));
            } else if (productOptions.get(i) == 3) {
                orderItem.setProd(content.get(2));
            } else if (productOptions.get(i) == 4) {
                orderItem.setProd(content.get(3));
            } else if (productOptions.get(i) == 5) {
                orderItem.setProd(content.get(4));
            } else if (productOptions.get(i) == 6) {
                orderItem.setProd(content.get(5));
            } else if (productOptions.get(i) == 7) {
                orderItem.setProd(content.get(6));
            } else if (productOptions.get(i) == 8) {
                orderItem.setProd(content.get(7));
            } else if (productOptions.get(i) == 9) {
                orderItem.setProd(content.get(8));
            } else {
                // do nothing
            }
            orderItem.setAmount(amounts.get(i));
            listOrder[i] = orderItem;
        }
    }
}


