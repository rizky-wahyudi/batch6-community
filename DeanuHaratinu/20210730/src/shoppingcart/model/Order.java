package shoppingcart.model;

import java.util.Date;

public class Order {

    Date orderDate;
    String branchId, paymentStatus, cashier;
    int orderNumber;
    OrderItem[] orderItem;

    public void finalOrder() {
        System.out.println("Mini market Sinar Dunia");
        System.out.println("ID Cabang: " + getBranchId());
        System.out.println("Kasir : " + getCashier());
        System.out.println("-----------------------");
        System.out.println("Tanggal: " + getOrderDate());
        System.out.println("No. Order: " + getOrderNum());
        System.out.println("Jumlah item   : " + getOrderItem().length);
        System.out.println("-----------------------");
        System.out.println("Rincian item");

        for (int i = 0; i < getOrderItem().length; i++) {
            getOrderItem()[i].finalOrder();
        }

        System.out.println("-----------------------");
        System.out.println("Total harga: Rp " + priceExcludeTax());
        System.out.println("Harga beserta pajak: Rp " + priceIncludeTax());
        System.out.println("-----------------------");
        System.out.println("Pembayaran: " + getPaymentStatus());
    }

    public OrderItem[] getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem[] orderItem) {
        this.orderItem = orderItem;
    }

    public double priceIncludeTax() {
        double priceincludetax = 0;

        for (int i = 0; i < getOrderItem().length; i++) {
            priceincludetax += (getOrderItem()[i].getProd().tax() * getOrderItem()[i].totalPrice());
        }
        return priceincludetax + priceExcludeTax();
    }

    public double priceExcludeTax() {
        double priceexcludetax = 0;

        for (int i = 0; i < getOrderItem().length; i++) {
            priceexcludetax += getOrderItem()[i].totalPrice();
        }
        return priceexcludetax;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getCashier() {
        return cashier;
    }

    public void setOrderNum(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getOrderNum() {
        return orderNumber;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchId() {
        return branchId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}