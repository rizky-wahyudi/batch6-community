package shoppingcart.model;

public class OrderItem {

    Product product;
    String orderId;
    int amount;
    double totalPrice;

    public void finalOrder() {
        System.out.println("------------------");
        System.out.println("Kode barang: " + getProd().getArticle());
        System.out.println("Nama barang: " + getProd().getDesc());
        System.out.println("Jumlah: " + getAmount());
        System.out.println("Harga: Rp" + totalPrice());
    }

    public double totalPrice() {
        totalPrice = getProd().sellPrice() * getAmount();
        return totalPrice;
    }

    public void setProd(Product product) {
        this.product = product;
    }

    public Product getProd() {
        return product;
    }

    // masih dibingungkan
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    // masih dibingungkan
    public String getOrderId() {
        return orderId;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }
}
