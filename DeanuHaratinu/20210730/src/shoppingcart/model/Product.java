package shoppingcart.model;

public class Product {

    String type, articleNumber, description, image, expiredDate;
    double purchasePrice, sellPrice;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public double tax() {
        if (getType().equals("makanan")) {
            return 0.05;
        } else if (getType().equals("sabun")) {
            return 0.07;
        } else if (getType().equals("minuman")) {
            return 0.1;
        } else {
            return 0;
        }
    }

    public void setArticle(String articleNumber) {
        this.articleNumber = articleNumber;
    }

    public String getArticle() {
        return articleNumber;
    }

    public void setDesc(String description) {
        this.description = description;
    }

    public String getDesc() {
        return description;
    }

    public void setPurchase(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public double getPurchase() {
        return purchasePrice;
    }

    public double sellPrice() {
        sellPrice = getPurchase() * (110 / 100);
        return sellPrice;
    }

    public void setImg(String image) {
        this.image = image;
    }

    public String getImg() {
        return image;
    }

    public void setExp(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getExp() {
        return expiredDate;
    }
}