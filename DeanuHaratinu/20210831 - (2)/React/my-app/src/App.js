import './App.css';
import Slider from './component/Slider';
import './component/style.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";

function App() {
  return (
    <div className="margin">
        <Slider/>
    </div>
  );
}

export default App;
