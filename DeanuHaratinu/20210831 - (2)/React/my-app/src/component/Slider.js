import { Carousel } from 'react-responsive-carousel';

function Slider() {
  return (
    <Carousel autoPlay>
      <div>
        <img alt="" src="images/image1.jpg" />
      </div>
      <div>
        <img alt="" src="images/image2.jpg" />
      </div>
      <div>
        <img alt="" src="images/image3.jpg" />
      </div>
    </Carousel>
    )
}

export default Slider;