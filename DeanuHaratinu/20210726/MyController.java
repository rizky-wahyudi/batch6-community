// Deanu Haratinu Tu'u

public class MyController {
	public static void main(String[] args) {
		Number a = new Number();
		Number b = new Number();
		Number c = new Number();
		
		try {
			a.setNumber(Integer.parseInt(args[0]));
			b.setNumber(Integer.parseInt(args[1]));
			c.setNumber(Integer.parseInt(args[2]));
		}
		catch (NumberFormatException err) {
			System.out.println("NaN: " + err);
		}
		
		if (a.getNumber() > b.getNumber()) {
          		System.out.println("a > b");
       		}
		
		if (b.getNumber() > c.getNumber()) {
        		System.out.println("b > c");
      		}
		
		if (c.getNumber() > 0) {
            		System.out.println("c > 0");
        	} 
	}
}