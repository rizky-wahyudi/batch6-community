package moneychanger;

public class UsDollar implements Valuta {

    @Override
    public double getAtoB(double nominal) {
        return nominal * usdtoidr ;
    }

    @Override
    public double getAtoC(double nominal) {
        return nominal * usdtoeur;
    }
}
