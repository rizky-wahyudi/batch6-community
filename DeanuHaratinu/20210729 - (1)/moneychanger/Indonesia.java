package moneychanger;

public class Indonesia implements Valuta {

    @Override
    public double getAtoB(double nominal) {
        return nominal * idrtousd;
    }

    @Override
    public double getAtoC(double nominal) {
        return nominal * idrtoeur;
    }

}
