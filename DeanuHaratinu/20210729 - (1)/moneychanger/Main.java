package moneychanger;

import java.util.Scanner;

public class Main {

    public static void main (String[] args) {
        double hasilKonversi = 0;
        boolean vendingState = true;
        String option;

        Scanner sc = new Scanner(System.in);
        String[] valuta = {"IDR", "USD", "EUR"};

        while (vendingState) {
            System.out.println("========================");
            for (String element : valuta) {
                System.out.print(element + " | ");
            }
            System.out.print("\nSilakan pilih valuta tujuan: ");
            String tujuan = sc.next().toUpperCase();
            System.out.println("========================");

            for (String element : valuta) {
                System.out.print(element + " | ");
            }
            System.out.print("\nSilakan pilih valuta Anda: ");
            String dari = sc.next().toUpperCase();
            System.out.println("========================");

            System.out.println("Anda akan menukar uang dari: " + dari);
            System.out.println("Menuju                     : " + tujuan);
            System.out.println("Masukkan nominal uang Anda: ");
            double nominal = sc.nextDouble();

            if (dari.equals(valuta[0])) {
                Indonesia idr = new Indonesia();
                if (tujuan.equals(valuta[1])) {
                    hasilKonversi = idr.getAtoB(nominal);
                } else if (tujuan.equals(valuta[2])) {
                    hasilKonversi = idr.getAtoC(nominal);
                }
            } else if (dari.equals(valuta[1])) {
                UsDollar usd = new UsDollar();
                if (tujuan.equals(valuta[0])) {
                    hasilKonversi = usd.getAtoB(nominal);
                } else if (tujuan.equals(valuta[2])) {
                    hasilKonversi = usd.getAtoC(nominal);
                }
            } else if (dari.equals(valuta[2])) {
                Europe eur = new Europe();
                if (tujuan.equals(valuta[0])) {
                    hasilKonversi = eur.getAtoB(nominal);
                } else if (tujuan.equals(valuta[1])) {
                    hasilKonversi = eur.getAtoC(nominal);
                }
            } else {
                System.out.println("Pilihan tidak ada");
            }

            System.out.println("========================");
            System.out.printf("Hasil konversi anda: %s %.3f ", tujuan, hasilKonversi);

            System.out.print("\nIngin menggunakan kembali? (Y/N) : ");
            option = sc.next();
            if (option.equalsIgnoreCase("Y")) {
                vendingState = true;
            } else if (option.equalsIgnoreCase("N")) {
                vendingState = false;
            }
        }
    }

}
