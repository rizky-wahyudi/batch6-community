package moneychanger;

public interface Valuta {
    double usdtoidr = 14500;
    double eurtoidr = 17000;
    double idrtousd = 1 / usdtoidr;
    double idrtoeur = 1 / eurtoidr;
    double usdtoeur = 0.84;
    double eurtousd = 1 / usdtoeur;

    double getAtoB(double nominal);
    double getAtoC(double nominal);

}
