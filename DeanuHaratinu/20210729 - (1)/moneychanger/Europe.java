package moneychanger;

public class Europe implements Valuta {

    @Override
    public double getAtoB(double nominal) {
        return nominal * eurtoidr;
    }

    @Override
    public double getAtoC(double nominal) {
        return nominal * eurtousd;
    }
}
