package VendingMachine;

public interface Vending {
    int get1();
    int get2();
    int get3();
    int get4();
    int get5();
    int get6();
    int get7();
    int get8();
    int get9();
    int get10();
}
