package VendingMachine;

public class RowC implements Vending {

    @Override
    public int get1() {
        String a1 = "CandiesA1";
        int a1Price = 4;
        System.out.println(a1);
        return a1Price;
    }

    @Override
    public int get2() {
        String a2 = "CandiesA2";
        int a2Price = 3;
        System.out.println(a2);
        return a2Price;
    }

    @Override
    public int get3() {
        String a3 = "CandiesA2";
        int a3Price = 3;
        System.out.println(a3);
        return a3Price;
    }

    @Override
    public int get4() {
        String a4 = "CandiesA4";
        int a4Price = 1;
        System.out.println(a4);
        return a4Price;
    }

    @Override
    public int get5() {
        String a5 = "CandiesA5";
        int a5Price = 2;
        System.out.println(a5);
        return a5Price;
    }

    @Override
    public int get6() {
        String a6 = "CandiesA6";
        int a6Price = 5;
        System.out.println(a6);
        return a6Price;
    }

    @Override
    public int get7() {
        String a7 = "CandiesA7";
        int a7Price = 3;
        System.out.println(a7);
        return a7Price;
    }

    @Override
    public int get8() {
        String a8 = "CandiesA8";
        int a8Price = 6;
        System.out.println(a8);
        return a8Price;
    }

    @Override
    public int get9() {
        String a9 = "CandiesA9";
        int a9Price = 4;
        System.out.println(a9);
        return a9Price;
    }

    @Override
    public int get10() {
        String a10 = "CandiesA10";
        int a10Price = 2;
        System.out.println(a10);
        return a10Price;
    }
}
