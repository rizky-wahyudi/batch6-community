package VendingMachine;

import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        int price = 0;
        int money;
        boolean vendingState = true;
        String option;

        Scanner sc = new Scanner(System.in);

        while (vendingState) {
            System.out.print("Silakan pilih (baris 1 - 6): ");
            int baris = sc.nextInt();
            System.out.print("Silakan pilih (kolom 1 - 10): ");
            int kolom = sc.nextInt();
            System.out.println("========================");

            if (baris >0 && baris < 7) {
                Vending detailsRow = FactoryVending.getDisRow(baris);

                if (kolom == 1) {
                    price = detailsRow.get1();
                } else if (kolom == 2) {
                    price = detailsRow.get2();
                } else if (kolom == 3) {
                    price = detailsRow.get3();
                } else if (kolom == 4) {
                    price = detailsRow.get4();
                } else if (kolom == 5) {
                    price = detailsRow.get5();
                } else if (kolom == 6) {
                    price = detailsRow.get6();
                } else if (kolom == 7) {
                    price = detailsRow.get7();
                } else if (kolom == 8) {
                    price = detailsRow.get8();
                } else if (kolom == 9) {
                    price = detailsRow.get9();
                } else if (kolom == 10) {
                    price = detailsRow.get10();
                } else {
                    System.out.println("Pilihan tidak ada");
                    vendingState = false;
                }
            } else if (baris < 0 || baris > 7) {
                vendingState = false;
                System.out.println("Pilihan tidak ada");
                break;
            }

            System.out.println("Harga : $" + price);

            System.out.print("Silakan masukan uang Anda : ");
            money = sc.nextInt();
            System.out.println("========================");

            if (money >= price) {
                System.out.println("Anda memasukkan uang: $" + money);
                System.out.println("Uang kembalian Anda sebesar: $" + (money - price));
                System.out.println("Silakan ambil barang Anda");

            } else if (money < price) {
                System.out.println("Uang Anda kurang");
                System.out.println("Silakan ambil kembali uang Anda");
            }

            System.out.println("========================");
            System.out.print("Ingin memilih kembali? (Y/N) : ");
            option = sc.next();

            if (option.equalsIgnoreCase("Y")) {
                vendingState = true;
            } else if (option.equalsIgnoreCase("N")) {
                vendingState = false;
            }
        }
    }
}
