package VendingMachine;

public class FactoryVending {
    public static Vending getDisRow(int baris){
        if (baris == 1) {
            return new RowA();
        }
        else if (baris == 2)  {
            return new RowB();
        }
        else if (baris == 3)  {
            return new RowC();
        }
        else if (baris == 4)  {
            return new RowD();
        }
        else if (baris == 5)  {
            return new RowE();
        }
        else if (baris == 6)  {
            return new RowF();
        }
        return null;
    }
}

