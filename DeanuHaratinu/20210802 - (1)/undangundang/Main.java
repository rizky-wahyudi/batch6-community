package undangundang;

import undangundang.model.*;
import java.util.ArrayList;

public class Main {

    public static void main (String[] args) {

        ArrayList<Peraturan> peraturan = getPeraturan();
        System.out.println("Isi Undang-undang   : ");
        System.out.println("Jenis Peraturan     : " + peraturan.get(0).getJenis());
        System.out.println("Nomor Peraturan     : " + peraturan.get(0).getNoPeraturan());
        System.out.println("Tahun Peraturan     : " + peraturan.get(0).getTahunPeraturan());
        System.out.println("Tentang             : " + peraturan.get(0).getTentang());
        System.out.println("Tgl Ditetapkan      : " + peraturan.get(0).getTanggalTetapkan());
        System.out.println("Nomor LN            : " + peraturan.get(0).getNoLN());
        System.out.println("Nomor TLN           : " + peraturan.get(0).getNoTLN());
        System.out.println("Tgl Diundangkan     : " + peraturan.get(0).getTanggalUndang());

        System.out.println("\nMenimbang           : ");
        String[] alphabet = {"a", "b", "c", "d", "e", "f"};
        Menimbang menimbang = peraturan.get(0).getDetailPeraturan().getMenimbang();
        for (int i = 0; i < menimbang.getPoints().size(); i++) {
            System.out.println(alphabet[i] + ". " + menimbang.getPoints().get(i));
        }

        System.out.println("\nMengingat          : ");
        Mengingat mengingat = peraturan.get(0).getDetailPeraturan().getMengingat();
        for (int i = 0; i < mengingat.getSubMengingat().size(); i++) {
            System.out.println((i+1) + ". " + mengingat.getSubMengingat().get(i));
        }

        // Menetapkan
        Menetapkan menetapkan = peraturan.get(0).getDetailPeraturan().getMenetapkan();
        System.out.println("\n" + menetapkan.getMenetapkan());
        for (int i = 0; i < menetapkan.getPasal().size(); i++) {
            System.out.println(menetapkan.getPasal().get(i).getPasal());
            if (menetapkan.getPasal().get(i).getSubPasal() != null) {
                for (int j = 0; j < menetapkan.getPasal().get(i).getSubPasal().size(); j++) {
                    System.out.println((j+1) + ". " + menetapkan.getPasal().get(i).getSubPasal().get(j));
                }
            } else if ((menetapkan.getPasal().get(i).getSubPasal() == null)) {
                continue;
            }
        }
    }

    static ArrayList<Peraturan> getPeraturan () {

        ArrayList<Peraturan> listPeraturan = new ArrayList<>();
        DetailPeraturan detail = new DetailPeraturan();

        Peraturan peraturan = new Peraturan();
        peraturan.setJenis("Undang-Undang");
        peraturan.setNoPeraturan("14");
        peraturan.setTahunPeraturan("2012");
        peraturan.setTentang("Pertanggungjawaban Atas Pelaksanaan Anggaran Dan Belanja " +
                "Negara Tahun Anggaran 2011");
        peraturan.setTanggalTetapkan("2012-09-22");
        peraturan.setNoLN("178");
        peraturan.setNoTLN("5341");
        peraturan.setTanggalUndang("2012-09-22");

        ArrayList<String> subPasalMenimbang = new ArrayList<>();
        String sub1 = "bahwa Anggaran Pendapatan dan Belanja Negara lorem ipsum dolor sit amet";
        String sub2 = "bahwa sesuai dengan ketentuan Pasal 30 ayat (1) lorem ipsum dolor sit amet";
        String sub3 = "bahwa sesuai ketentuan Pasal 3 ayat (2), Pasal 30, lorem ipsum dolor sit amet";
        String sub4 = "bahwa pembahasan Undang-Undang tentang Pertanggungjawaban atas lorem ipsum dolor sit amet";
        String sub5 = "bahwa berdasarkan pertimbangan sebagaimana dimaksud pada huruf a lorem ipsum dolor sit amet";
        subPasalMenimbang.add(sub1);
        subPasalMenimbang.add(sub2);
        subPasalMenimbang.add(sub3);
        subPasalMenimbang.add(sub4);
        subPasalMenimbang.add(sub5);

        Menimbang menimbang = new Menimbang();
        menimbang.setPoints(subPasalMenimbang);

        ArrayList<String> subPasalMengingat = new ArrayList<>();
        sub1 = "Pasal 5 ayat (1), Pasal 20 ayat (1) lorem ipsum dolor sit amet";
        sub2 = "Undang-Undang Nomor 17 lorem ipsum";
        sub3 = "Undang-Undang Nomor 1 Tahun 2003 lorem ipsum";
        sub4 = "Undang-Undang Nomor 15 Tahun 2004 lorem ipsum";
        sub5 = "Undang-Undang Nomor 15 Tahun 2006 lorem ipsum";
        subPasalMengingat.add(sub1);
        subPasalMengingat.add(sub2);
        subPasalMengingat.add(sub3);
        subPasalMengingat.add(sub4);
        subPasalMengingat.add(sub5);

        Mengingat mengingat = new Mengingat();
        mengingat.setSubMengingat(subPasalMengingat);
        detail.setMengingat(mengingat);

        detail.setMenimbang(menimbang);

        String tetapan = "Menetapkan        : Undang-Undang Tentang PertanggungJawaban lorem ipsum";

        ArrayList<Pasal> daftarPasal = new ArrayList<>();
        Pasal pasal1 = new Pasal();
        pasal1.setPasal("Pasal 1: Pertanggungjawaban atas pelaksanaan APBN Tahun Anggaran lorem ipsum");
        daftarPasal.add(pasal1);

        ArrayList<String> subPasal2 = new ArrayList<>();
        sub1 = "Laporan Realisasi APBN";
        sub2 = "Neraca Pemerintah Pusat";
        sub3 = "Laporan Arus Kas Tahun Anggaran 2011";
        subPasal2.add(sub1);
        subPasal2.add(sub2);
        subPasal2.add(sub3);

        Pasal pasal2 = new Pasal();
        pasal2.setPasal("Pasal 2: Laporang keuangan Pemerintah Pusat lorem ipsum");
        pasal2.setSubPasal(subPasal2);
        daftarPasal.add(pasal2);

        Menetapkan menetapkan = new Menetapkan();
        menetapkan.setMenetapkan(tetapan);
        menetapkan.setPasal(daftarPasal);
        detail.setMenetapkan(menetapkan);

        peraturan.setDetailPeraturan(detail);
        listPeraturan.add(peraturan);

        return listPeraturan;
    }

}
