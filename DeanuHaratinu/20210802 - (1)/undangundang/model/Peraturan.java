package undangundang.model;


public class Peraturan {

    private String tanggalUndang;
    private String noLN;
    private String noTLN;
    private String tanggalTetapkan;
    private String tentang;
    private String jenis;
    private String noPeraturan;
    private String tahunPeraturan;
    private DetailPeraturan detailPeraturan;

    public String getTanggalUndang() {
        return tanggalUndang;
    }

    public void setTanggalUndang(String tanggalUndang) {
        this.tanggalUndang = tanggalUndang;
    }

    public String getNoLN() {
        return noLN;
    }

    public void setNoLN(String noLN) {
        this.noLN = noLN;
    }

    public String getNoTLN() {
        return noTLN;
    }

    public void setNoTLN(String noTLN) {
        this.noTLN = noTLN;
    }

    public String getTanggalTetapkan() {
        return tanggalTetapkan;
    }

    public void setTanggalTetapkan(String tanggalTetapkan) {
        this.tanggalTetapkan = tanggalTetapkan;
    }

    public String getTentang() {
        return tentang;
    }

    public void setTentang(String tentang) {
        this.tentang = tentang;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getNoPeraturan() {
        return noPeraturan;
    }

    public void setNoPeraturan(String noPeraturan) {
        this.noPeraturan = noPeraturan;
    }

    public String getTahunPeraturan() {
        return tahunPeraturan;
    }

    public void setTahunPeraturan(String tahunPeraturan) {
        this.tahunPeraturan = tahunPeraturan;
    }

    public void setDetailPeraturan(DetailPeraturan detailPeraturan) {
        this.detailPeraturan = detailPeraturan;
    }

    public DetailPeraturan getDetailPeraturan() {
        return detailPeraturan;
    }
}
