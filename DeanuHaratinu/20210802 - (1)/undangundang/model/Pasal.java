package undangundang.model;

import java.util.ArrayList;

public class Pasal {

    private ArrayList<String> subPasal;
    private String pasal;

    public ArrayList<String> getSubPasal() {
        return subPasal;
    }

    public void setSubPasal(ArrayList<String> subPasal) {
        this.subPasal = subPasal;
    }

    public String getPasal() {
        return pasal;
    }

    public void setPasal(String pasal) {
        this.pasal = pasal;
    }
}
