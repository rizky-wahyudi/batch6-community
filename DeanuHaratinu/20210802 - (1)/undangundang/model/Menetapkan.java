package undangundang.model;

import java.util.ArrayList;

public class Menetapkan {

    private String menetapkan;
    private ArrayList<Pasal> pasal;

    public String getMenetapkan() {
        return menetapkan;
    }

    public void setMenetapkan(String menetapkan) {
        this.menetapkan = menetapkan;
    }

    public ArrayList<Pasal> getPasal() {
        return pasal;
    }

    public void setPasal(ArrayList<Pasal> pasal) {
        this.pasal = pasal;
    }
}
