package undangundang.model;

public class DetailPeraturan {

    private Menimbang menimbang;
    private Menetapkan menetapkan;
    private Mengingat mengingat;

    public Menimbang getMenimbang() {
        return menimbang;
    }

    public void setMenimbang(Menimbang menimbang) {
        this.menimbang = menimbang;
    }

    public Menetapkan getMenetapkan() {
        return menetapkan;
    }

    public void setMenetapkan(Menetapkan menetapkan) {
        this.menetapkan = menetapkan;
    }

    public Mengingat getMengingat() {
        return mengingat;
    }

    public void setMengingat(Mengingat mengingat) {
        this.mengingat = mengingat;
    }
}
