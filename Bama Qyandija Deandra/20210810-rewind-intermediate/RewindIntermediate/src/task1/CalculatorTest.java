package task1;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class CalculatorTest {

    @Test
    @DisplayName("test true")
    void tesi1(){
        assertTrue(true,String.valueOf(Calculator.add(2,1) == 3));
        String[] b = {"2","4"};
        Calculator.main(b);
        assertTrue(true);

    }

    @Test
    @DisplayName("test false")
    void test2(){
        assertFalse(Calculator.divide(4,3));
        assertFalse(Calculator.divide(4,4));
    }

    @Test
    @DisplayName("Equals")
    void test3(){
        assertEquals(6,Calculator.multiply(3,2));
    }

    @Test
    @DisplayName("assert throw")
    void test4(){
        String[] a = {"3","b"};
        assertThrows(NumberFormatException.class, ()-> Calculator.main(a));
    }

}