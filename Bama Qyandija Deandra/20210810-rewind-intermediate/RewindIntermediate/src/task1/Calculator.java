package task1;

public class Calculator {
    public static double add(int numOne, int numTwo) {
        int result = numOne + numTwo;
        System.out.println(numOne + " + " + numTwo + " = " + result);
        return result;
    }
    public static void subtract(int numOne, int numTwo) {
        int result = numOne - numTwo;
        System.out.println(numOne + " - " + numTwo + " = " + result);
    }
    public static int multiply(int numOne, int numTwo) {
        int result = numOne * numTwo;
        System.out.println(numOne + " * " + numTwo + " = " + result);
        return result;
    }
    public static boolean divide(int numOne, int numTwo) {
        boolean a = true;
        int result = numOne / numTwo;
        System.out.println(numOne + " / " + numTwo + " = " + result);
        if (numOne / numTwo != 0){
            a = false;
        }
        return a;
    }
    public static void main(String[] args) {
        int numOne = Integer.parseInt(args[0]);
        int numTwo = Integer.parseInt(args[1]);
        add(numOne, numTwo);
        subtract(numOne, numTwo);
        multiply(numOne, numTwo);
        divide(numOne, numTwo);
    }


}
