package task4;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Reader {
    public static void main(String[] args) {
        Path currentPath = Paths.get("");
        String path = currentPath.toAbsolutePath().toString();
        try {
            System.out.println("Path kita adalah : " + path);
            Path direktori = Paths.get(path + "/Reader/");
            Files.createDirectories(direktori);
            System.out.println("Direktori sudah terbuat");
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 1; i <=5; i++){
            String link = "https://jsonplaceholder.typicode.com/todos/" + i;
            FileOutputStream fout = null;
            File file = null;
            try {
                file = new File(path+"/Reader/#"+ i + ".json");
                fout = new FileOutputStream(file);
                URL json = new URL(link);
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(json.openStream())
                );
                String inputLine;
                while ((inputLine = bufferedReader.readLine()) != null){
                    byte[] b = inputLine.getBytes();
                    fout.write(b);
                    System.out.println(inputLine);
                }
                fout.close();
                bufferedReader.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
