package task2;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Thread2 implements Runnable {

    @Override
    public void run() {
        Path currentPath = Paths.get("");
        String path = currentPath.toAbsolutePath().toString();
        try {
            int i = (int) (Math.random() * 4) + 1;
            String fileName = "text" + i + ".txt";
            FileInputStream fin = new FileInputStream(path + "/rewind/" + fileName);
            byte[] bit = fin.readAllBytes();
            String str = new String(bit);
            System.out.println(fileName);
            System.out.println(str);
            System.out.println();
            fin.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
