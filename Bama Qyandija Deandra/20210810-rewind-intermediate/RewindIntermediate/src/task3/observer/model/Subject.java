package task3.observer.model;


public interface Subject {
    public void attach(Observer observer);
    public void notifyUpdate(Message m);
}
