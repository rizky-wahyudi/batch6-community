package task3.observer.model;

public class Message {

    private String message;

    public Message(String m){
        this.message = m;
    }

    public String getMessage() {
        return message;
    }
}
