package task3.observer.controller;

import task3.observer.model.Message;
import task3.observer.model.Observer;

public class MessageSubs1 implements Observer {

    @Override
    public void update(Message m) {

        System.out.println("Message sub 1 : : " + m.getMessage());

    }
}
