package task3.observer.controller;

import task3.observer.model.Message;
import task3.observer.model.Observer;
import task3.observer.model.Subject;

import java.util.ArrayList;
import java.util.List;


public class MessagePublish implements Subject {

    private List<Observer> observers = new ArrayList<>();

    @Override
    public void attach(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyUpdate(Message m) {
        for(Observer o: observers) {
            o.update(m);
        }
    }
}
