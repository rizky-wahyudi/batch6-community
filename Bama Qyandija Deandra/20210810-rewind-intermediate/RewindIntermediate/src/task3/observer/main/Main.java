package task3.observer.main;

import task3.observer.controller.MessagePublish;
import task3.observer.controller.MessageSubs1;
import task3.observer.controller.MessageSubs2;
import task3.observer.controller.MessageSubs3;
import task3.observer.model.Message;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        MessageSubs1 messageSubs1 = new MessageSubs1();
        MessageSubs2 messageSubs2 = new MessageSubs2();
        MessageSubs3 messageSubs3 = new MessageSubs3();

        MessagePublish p = new MessagePublish();

        p.attach(messageSubs1);
        p.attach(messageSubs2);
        p.attach(messageSubs3);

        p.notifyUpdate(new Message("Hallo guys nama saya Bams"));

        Thread.sleep(1_500);
        p.notifyUpdate(new Message("Saat ini saya lagi makan malam"));
    }
}
