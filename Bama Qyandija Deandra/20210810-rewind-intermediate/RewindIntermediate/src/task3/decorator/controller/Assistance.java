package task3.decorator.controller;

import task3.decorator.model.Employee;
import task3.decorator.model.EmployeeDecorator;

import java.util.Arrays;

public class Assistance extends EmployeeDecorator {

    //constructor agar bisa new new di main
    public Assistance(Employee employee) {
        super(employee);
    }

    //function yang berisi data untuk di masukan ke override jobdes
    public String[] tanggungJawab(){
        return new String[] {"koordinasi ", "pelaporan"};
    }

    //override jobdes
    @Override
    public String jobDes() {
        String b = "";
        for (String a: tanggungJawab()){
            b += a;
        }
        return super.jobDes() + " " + b;
    }
}
