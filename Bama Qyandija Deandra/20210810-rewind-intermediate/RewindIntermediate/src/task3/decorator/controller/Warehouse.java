package task3.decorator.controller;

import task3.decorator.model.Employee;
import task3.decorator.model.EmployeeDecorator;


public class Warehouse extends EmployeeDecorator {

    //menambahkan funsi agar saat main bisa new new
    public Warehouse(Employee employee) {
        super(employee);
    }

    // menambahkan method yang akan mengoverride
    public String[] tanggungJawab(){
        return new String[]{"Membantu bongkar ", "cek Stok barang"};
    }

    //override jobdes dari employee
    @Override
    public String jobDes() {
        String b = "";
        for (String a: tanggungJawab()){
            b += a;
        }
        return super.jobDes() + " " + b;
    }
}
