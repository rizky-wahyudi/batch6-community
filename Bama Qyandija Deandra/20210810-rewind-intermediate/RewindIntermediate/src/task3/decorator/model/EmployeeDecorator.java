package task3.decorator.model;

import task3.decorator.model.Employee;

public class EmployeeDecorator implements Employee {

    private Employee employee;

    public EmployeeDecorator(Employee employee){
        this.employee = employee;
    }

    @Override
    public String jobDes() {
        return employee.jobDes();
    }
}
