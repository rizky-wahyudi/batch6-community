package task3.decorator.main;

import task3.decorator.controller.Assistance;
import task3.decorator.controller.Cashier;
import task3.decorator.controller.Warehouse;
import task3.decorator.model.Employee;

public class Main {

    public static void main(String[] args) {
        // inisialisai data decorator
        Employee employee = new Assistance(new Warehouse(new Cashier()));

        System.out.println(employee.jobDes());
    }
}
