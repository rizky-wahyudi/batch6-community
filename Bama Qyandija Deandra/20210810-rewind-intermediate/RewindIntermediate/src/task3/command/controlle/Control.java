package task3.command.controlle;

import task3.command.model.Order;

import java.util.ArrayList;
import java.util.List;

public class Control {

    private List<Order> orders = new ArrayList<>();

    public void take(Order order){
        orders.add(order);
    }
    public void place(){
        for (Order order : orders){
            order.execute();
        }
        orders.clear();
    }
}
