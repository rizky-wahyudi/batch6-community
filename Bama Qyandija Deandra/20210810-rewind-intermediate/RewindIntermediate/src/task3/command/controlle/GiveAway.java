package task3.command.controlle;

import task3.command.model.Order;

public class GiveAway implements Order {

    Stock s;
    public GiveAway(Stock s){
        this.s = s;
    }

    @Override
    public void execute() {
        s.giveAway();
    }
}
