package task3.command.controlle;

import task3.command.model.Order;

public class BuyStock implements Order {
    Stock s;

    public BuyStock(Stock s) {
        this.s = s;
    }

    @Override
    public void execute() {
        s.buy();
    }
}
