package task3.command.main;

import task3.command.controlle.BuyStock;
import task3.command.controlle.Control;
import task3.command.controlle.GiveAway;
import task3.command.controlle.Stock;

public class Main {

    public static void main(String[] args) {
        Stock stock = new Stock();

        BuyStock buyStock = new BuyStock(stock);
        GiveAway giveAway = new GiveAway(stock);

        Control control =  new Control();
        control.take(buyStock);
        control.take(giveAway);

        control.place();
    }
}
