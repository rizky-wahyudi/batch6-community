package task3.command.model;

public interface Order {
    void execute();
}
