package task3.factory.main;

import task3.factory.controller.Handphone;
import task3.factory.controller.HandphoneFactory;

public class Main {

    public static void main(String[] args) {
        HandphoneFactory handphoneFactory = new HandphoneFactory();

        // inisialisasi object dari Xiaomi
        Handphone handphone1 = handphoneFactory.getType("Xiaomi");
        // memanggil method brand nya
        handphone1.brand();

        // inisialisasi object dari Realme
        Handphone handphone2 = handphoneFactory.getType("Realme");
        // memanggil method brand nya
        handphone2.brand();

        // inisialisasi object dari Samsung
        Handphone handphone3 = handphoneFactory.getType("Samsung");
        // memanggil method brand nya
        handphone3.brand();
    }
}
