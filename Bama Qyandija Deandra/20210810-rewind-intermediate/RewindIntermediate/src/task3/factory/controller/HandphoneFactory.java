package task3.factory.controller;

import task3.factory.model.Realme;
import task3.factory.model.Samsung;
import task3.factory.model.Xiaomi;

public class HandphoneFactory {

    // membuat class factory untuk melihat apa yang akan di kirm oleh pengguna di main
    public Handphone getType(String type) {

        // terdapat beberapa if di mana mengikuti input saat di main
        // dan mengembalilkan class karena typenya berupa class
        if (type == null) {
            return null;
        } else if (type.equalsIgnoreCase("Xiaomi")) {
            return new Xiaomi();
        } else if (type.equalsIgnoreCase("Realme")) {
            return new Realme();
        } else if (type.equalsIgnoreCase("Samsung")) {
            return new Samsung();
        }
        return null;
    }
}
