package task3.factory.model;

import task3.factory.controller.Handphone;

public class Realme implements Handphone {

    //membuat class untuk implementasi handphone brand nya

    //override method brand
    @Override
    public void brand() {
        System.out.println("REALME :: Ini merek hp mahal guys");
    }
}
