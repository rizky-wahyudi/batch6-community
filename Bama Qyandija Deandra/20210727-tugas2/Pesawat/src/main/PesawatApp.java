package main;

import pesawat.*;

import java.util.Scanner;

public class PesawatApp {

    public static void main(String[] args) {
        //inisialisasi Scanner
        Scanner input = new Scanner(System.in);
        String kelas;
        int pesawat;

        //set nama pesawatnya
        AirPlane a1 = new AirPlane("airbus 380", 750);
        AirPlane a2 = new AirPlane("Boeing 747", 529);
        AirPlane a3 = new AirPlane("Boeing 787", 248);

        //set laguage
        a1.setLanguage(20);
        a2.setLanguage(20);
        a3.setLanguage(20);


        //bertanya kelas
        System.out.println("============ KELAS PESAWAT ============");
        System.out.println("Ekonomy");
        System.out.println("Bisnis");
        System.out.println("First Class");
        System.out.print("silakan memilih kelas : ");
        kelas = input.nextLine();

        //bertanya nama pesawat
        System.out.println("============ Nama Pesawat =============");
        System.out.println("1. airbus 380");
        System.out.println("2. Boeing 747");
        System.out.println("3. Boeing 787");
        System.out.print("silakan memilih pesawat : ");
        pesawat = input.nextInt();

        //bertannya tujuan
        System.out.println("========= tujuan =========");
        System.out.println("1. europe-asia");
        System.out.println("2. asia-europe");
        System.out.println("3. europe-us");
        System.out.print("silakan memilih ");
        int tujuan = input.nextInt();

        //bertanya berapa orang
        System.out.println("======= jumlah tiket =======");
        System.out.println("silakan memasukan julmlah tiket : ");
        int jumlah = input.nextInt();

        //output semua

        System.out.println("sedang proses");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("\n========= your ticket =========");

        //set kelas
        Ekonomi e1 = new Ekonomi("europe-asia", 500, jumlah);
        Ekonomi e2 = new Ekonomi("asia-europe", 600, jumlah);
        Ekonomi e3 = new Ekonomi("europe-us", 650, jumlah);
        Bisnis b1 = new Bisnis("europe-asia", 500, jumlah);
        Bisnis b2 = new Bisnis("asia-europe", 600, jumlah);
        Bisnis b3 = new Bisnis("europe-us", 650, jumlah);
        First f1 = new First("europe-asia", 500, jumlah);
        First f2 = new First("asia-europe", 600, jumlah);
        First f3 = new First("europe-us", 650, jumlah);


        //masukan ke dalam factory class + outputnya
        AirPlane plane = PlaneFactory.newInstance(kelas);
        if (plane instanceof Ekonomi) {
            if (tujuan == 1) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e1.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e1.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e1.getHarga());
                }
            } else if (tujuan == 2) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e2.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e2.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e2.getHarga());
                }
            } else if (tujuan == 3) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e3.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e3.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + e3.getHarga());
                }
            }
        } else if (plane instanceof Bisnis) {
            if (tujuan == 1) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b1.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b1.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b1.getHarga());
                }
            } else if (tujuan == 2) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b2.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b2.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b2.getHarga());
                }
            } else if (tujuan == 3) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b3.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b3.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + b3.getHarga());
                }
            }
        } else if (plane instanceof First) {
            if (tujuan == 1) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f1.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f1.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f1.getHarga());
                }
            } else if (tujuan == 2) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f2.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f2.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f2.getHarga());
                }
            } else if (tujuan == 3) {
                if (pesawat == 1) {
                    System.out.println("anda memilih pesawat : " + a1.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f3.getHarga());
                } else if (pesawat == 2) {
                    System.out.println("anda memilih pesawat : " + a2.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f3.getHarga());
                } else if (pesawat == 3) {
                    System.out.println("anda memilih pesawat : " + a3.getNamaPesawat());
                    System.out.println("kelas : " + kelas);
                    System.out.println("total : " + f3.getHarga());
                }
            }
        } else {
            System.out.println("anda tidak memilih yang sesuai");
        }
    }
}
