package pesawat;

public class PlaneFactory {
    public static AirPlane newInstance(String type){
        if (type.equalsIgnoreCase("ekonomi")){
            return new Ekonomi();
        }else if(type.equalsIgnoreCase("bisnis")){
            return new Bisnis();
        }else if (type.equalsIgnoreCase("first")||type.equalsIgnoreCase("First class")){
            return new First();
        }else {
            System.out.println("pilih la yang sesuai");
        }
        return null;
    }
}
