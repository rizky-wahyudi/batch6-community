package pesawat;

public class AirPlane {
    private String namaPesawat;
    private int jumlahKursi;
    private double language = 20;
    private String tujuan;

    public AirPlane(){

    }
    public AirPlane(String namaPesawat,int jumlahKursi){
        this.namaPesawat = namaPesawat;
        this.jumlahKursi = jumlahKursi;
    }

    public String getNamaPesawat() {
        return namaPesawat;
    }


    public int getJumlahKursi() {
        return jumlahKursi;
    }


    public double getLanguage() {
        return language;
    }

    public void setLanguage(double language) {
        this.language = language;
    }

    public String getTujuan() {
        return tujuan;
    }
}
