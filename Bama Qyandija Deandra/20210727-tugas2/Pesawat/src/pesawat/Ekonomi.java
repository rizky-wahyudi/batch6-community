package pesawat;

public class Ekonomi extends AirPlane{
    private double harga;
    private double jumlah;
    private String tujuan;
    public Ekonomi(){

    }
    public Ekonomi(String tujuan, double harga, int jumlah){
        this.tujuan = tujuan;
        this.harga = harga;
        this.jumlah = jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    public double getHarga() {
        return harga*jumlah;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }
}
