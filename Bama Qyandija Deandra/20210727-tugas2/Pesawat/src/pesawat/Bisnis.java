package pesawat;

public class Bisnis extends AirPlane {
    private double harga;
    private double jumlah;
    private String tujuan;

    public Bisnis(){

    }

    public Bisnis(String tujuan, double harga, int jumlah){
        this.tujuan = tujuan;
        this.harga = harga;
        this.jumlah = jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    public double getHarga() {
        return harga * 2.25 * jumlah;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }
}
