package id.co.nexsoft.vendingmesin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendingmesinApplication {

    public static void main(String[] args) {
        SpringApplication.run(VendingmesinApplication.class, args);
    }

}
