package id.co.nexsoft.vendingmesin.repository;

import id.co.nexsoft.vendingmesin.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
    Product findById(int id);

    List<Product> findByBlokAndNomor(String blok, int nomor);

    List<Product> findAll();

    void deleteByBlokAndNomor(String blok, int nomor);
}
