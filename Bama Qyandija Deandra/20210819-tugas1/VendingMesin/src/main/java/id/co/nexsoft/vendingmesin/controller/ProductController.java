package id.co.nexsoft.vendingmesin.controller;

import id.co.nexsoft.vendingmesin.entity.Product;
import id.co.nexsoft.vendingmesin.entity.Request;
import id.co.nexsoft.vendingmesin.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository repository;

    @GetMapping("/")
    public String welcome() {
        return "SELAMAT DATANG KE VENDING MART";
    }

    @GetMapping("/product")
    public List<Product> list() {
        return repository.findAll();
    }

    @GetMapping("/product/{id}")
    public Product getProduct(@PathVariable(value = "id") int id) {
        return repository.findById(id);
    }

    @GetMapping("/product/blokandnomor")
    public ResponseEntity<List<Product>> getData(@RequestParam String blok, @RequestParam int nomor) {
        return new ResponseEntity<List<Product>>(repository.findByBlokAndNomor(blok, nomor), HttpStatus.OK);
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Iterable<Product> addProduct(@RequestBody List<Product> product) {
        return repository.saveAll(product);
    }

    @PostMapping("/newproduct")
    @ResponseStatus(HttpStatus.CREATED)
    public Product addOne(@RequestBody Product product) {
        return repository.save(product);
    }

    @GetMapping("/deleted")
    private void deletedProduk(@RequestParam String blok, @RequestParam int nomor) {
        repository.deleteByBlokAndNomor(blok, nomor);
    }

    @PostMapping("/product/update")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<List<Product>> updateProduct(@RequestBody Request request) {

        int requestnomor = request.getNomor();
        String blok = request.getBlok();

        List<Product> pilih = repository.findByBlokAndNomor(blok, requestnomor);
        for (var a : pilih) {
            if (request.getStatus().equals("paid") && (a.getEntity() > 0)) {
                int updateEntity = a.getEntity() - 1;
                a.setEntity(updateEntity);
                repository.save(a);
            }
        }
        return new ResponseEntity<List<Product>>(repository.findByBlokAndNomor(blok, requestnomor), HttpStatus.OK);
    }
}
