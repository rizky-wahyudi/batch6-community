package com.example.demo.controller;


import com.example.demo.entity.Student;
import com.example.demo.repo.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;


@RestController
public class StudentController {

    @Autowired
    private StudentRepository repo;

    // Home Page
    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
                + "<h1>This is Home page</h1>"
                + "</body></html>";
    }

    // Get All Notes
    @GetMapping("/student")
    public List<Student> getAllNotes() {
        return repo.findAll();
    }

    // Get the student details by
    // ID
    @GetMapping("/student/{id}")
    public Student getStudentById(@PathVariable(value = "id") int id) {
        return repo.findById(id);
    }

    @PostMapping("/student")
    @ResponseStatus(HttpStatus.CREATED)
    public Student addStudent(@RequestBody Student student) {
        return repo.save(student);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteStudent( @PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }

    @PutMapping("/student/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Student student, @PathVariable int id) {

        Optional<Student> studentRepo = Optional.ofNullable(repo.findById(id));

        if (!studentRepo.isPresent())
            return ResponseEntity.notFound().build();

        student.setId(id);
        repo.save(student);
        return ResponseEntity.noContent().build();
    }
}