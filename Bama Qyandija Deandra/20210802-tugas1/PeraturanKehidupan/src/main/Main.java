package main;

import data.Detail;
import data.Peraturan;

import java.util.LinkedList;

public class Main {

    public static void peraturan() {
        Peraturan p1 = new Peraturan();
        p1.setJenis("UNDANG - UNDANG");
        p1.setTahunPeraturan(2020);
        p1.setNoPeraturan(2);
        p1.setTentang("PENETAPAN PERATURAN PEMERINTAH PENGGANTI UNDANG-UNDANG NOMOR 1 TAHUN" +
                "\n2020 TENTANG KEBIJAKAN KEUANGAN NEGARA DAN STABILITAS SISTEM KEUANGAN UNTUK " +
                "\nPENANGANAN PANDEMI CORONA VIRUS DISEASE 2019 (COVID-19) DAN/ATAU DALAM " +
                "\nRANGKA MENGHADAPI ANCAMAN YANG MEMBAHAYAKAN PEREKONOMIAN NASIONAL " +
                "\nDAN/ATAU STABILITAS SISTEM KEUANGAN MENJADI UNDANG-UNDANG");
        p1.setTanggalDiundangkan("2020-05-18");
        p1.setNomorLN("134");
        p1.setNomorTLN("6516");
        p1.setTanggalDitetapkan("2020-05-16");

        p1.printperaturan();

    }

    public static void detail() {
        Detail detail1 = new Detail();
        detail1.setMenetapkan("UNDANG-UNDANG TENTANG PENETAPAN PERATURAN\n" +
                "PEMERINTAH PENGGANTI UNDANG-UNDANG NOMOR 1\n" +
                "TAHUN 2020 TENTANG KEBIJAKAN KEUANGAN NEGARA\n" +
                "DAN STABILITAS SISTEM KEUANGAN UNTUK PENANGANAN\n" +
                "PANDEMI CORONA VIRUS DISEASE 2019 (COVID-19)\n" +
                "DAN/ATAU DALAM RANGKA MENGHADAPI ANCAMAN YANG\n" +
                "MEMBAHAYAKAN PEREKONOMIAN NASIONAL DAN/ATAU\n" +
                "STABILITAS SISTEM KEUANGAN MENJADI UNDANGUNDANG.");
        detail1.setMenimbang("a. bahwa penyebaran Corona Virus Disease 2019 (COVID-19)\n" +
                "yang dinyatakan oleh Organisasi Kesehatan Dunia (World\n" +
                "Health Organization) sebagai pandemi pada sebagian besar\n" +
                "negara-negara di seluruh dunia, termasuk di Indonesia,\n" +
                "menunjukkan peningkatan dari waktu ke waktu dan telah\n" +
                "menimbulkan korban jiwa, serta kerugian material yang\n" +
                "semakin besar, sehingga berimplikasi pada aspek sosial,\n" +
                "ekonomi, dan kesejahteraan masyarakat;\n" +
                "b. bahwa implikasi pandemi Corona Virus Disease 2019 (COVID19) telah berdampak antara lain terhadap perlambatan\n" +
                "pertumbuhan ekonomi nasional, penurunan penerimaan\n" +
                "negara, dan peningkatan belanja negara dan pembiayaan,\n" +
                "sehingga diperlukan berbagai upaya Pemerintah untuk\n" +
                "melakukan penyelamatan kesehatan dan perekonomian ");
        detail1.setMengingat("Pasal 5 ayat (1), Pasal 20, dan Pasal 22 ayat (2) Undang-Undang\n" +
                "Dasar Negara Republik Indonesia Tahun 1945;\n");


        System.out.println("\nMENETAPKAN : ");
        System.out.println(detail1.getMenetapkan());
        System.out.println("\nMENIMBANG : ");
        System.out.println(detail1.getMenimbang());
        System.out.println("\nMENGINGAT : ");
        System.out.println(detail1.getMengingat());



    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println();
        peraturan();
        Thread.sleep(3000);
        detail();
    }
}
