package data;

public class Peraturan {
    private String jenis;
    private int noPeraturan;
    private int tahunPeraturan;
    private String nomorTLN;
    private String nomorLN;
    private String tentang;
    private String tanggalDiundangkan;
    private String tanggalDitetapkan;


    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public int getNoPeraturan() {
        return noPeraturan;
    }

    public void setNoPeraturan(int noPeraturan) {
        this.noPeraturan = noPeraturan;
    }

    public int getTahunPeraturan() {
        return tahunPeraturan;
    }

    public void setTahunPeraturan(int tahunPeraturan) {
        this.tahunPeraturan = tahunPeraturan;
    }

    public String getNomorTLN() {
        return nomorTLN;
    }

    public void setNomorTLN(String nomorTLN) {
        this.nomorTLN = nomorTLN;
    }

    public String getNomorLN() {
        return nomorLN;
    }

    public void setNomorLN(String nomorLN) {
        this.nomorLN = nomorLN;
    }

    public String getTentang() {
        return tentang;
    }

    public void setTentang(String tentang) {
        this.tentang = tentang;
    }

    public String getTanggalDiundangkan() {
        return tanggalDiundangkan;
    }

    public void setTanggalDiundangkan(String tanggalDiundangkan) {
        this.tanggalDiundangkan = tanggalDiundangkan;
    }

    public String getTanggalDitetapkan() {
        return tanggalDitetapkan;
    }

    public void setTanggalDitetapkan(String tanggalDitetapkan) {
        this.tanggalDitetapkan = tanggalDitetapkan;
    }

    public void printperaturan() {
        System.out.println("Jenis Peraturan      : " + this.jenis);
        System.out.println("No Peraturan       	 : " + this.noPeraturan);
        System.out.println("Tahun Peraturan 	 : " + this.tahunPeraturan);
        System.out.println("Tentang      		 : " + this.tentang);
        System.out.println("Tanggal Ditetapkan   : " + this.tanggalDitetapkan);
        System.out.println("No LN        		 : " + this.nomorLN);
        System.out.println("No TLN        		 : " + this.nomorTLN);
        System.out.println("Tanggal Diundangkan  : " + this.tanggalDiundangkan);
    }
}
