package data;

public class Detail {
    private String menetapkan;
    private String mengingat;
    private String menimbang;

    public String getMenetapkan() {
        return menetapkan;
    }

    public void setMenetapkan(String menetapkan) {
        this.menetapkan = menetapkan;
    }

    public String getMengingat() {
        return mengingat;
    }

    public void setMengingat(String mengingat) {
        this.mengingat = mengingat;
    }

    public String getMenimbang() {
        return menimbang;
    }

    public void setMenimbang(String menimbang) {
        this.menimbang = menimbang;
    }
}
