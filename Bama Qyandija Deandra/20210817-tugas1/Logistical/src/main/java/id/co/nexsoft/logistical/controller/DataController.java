package id.co.nexsoft.logistical.controller;

import id.co.nexsoft.logistical.model.*;

import java.util.ArrayList;

public class DataController {

    //dataproduk
    public ArrayList<Product> getDataProduk() {
        ArrayList<Product> dataProduk = new ArrayList<>();
        dataProduk.add(new Product(1, "a", "a", 1, 1, 1, 1));
        dataProduk.add(new Product(2, "b", "b", 1, 1, 1, 2));
        dataProduk.add(new Product(3, "c", "c", 1, 1, 1, 3));
        return dataProduk;
    }

    //data address
    public ArrayList<Address> getDataAddress() {
        ArrayList<Address> dataAddress = new ArrayList<>();
        dataAddress.add(new Address(1, "ruko newton Barat", "kelapa dua", "Tangerang", "jawa barat", "indonesia", 22.12, -2.332));
        dataAddress.add(new Address(2, "komplek elgreen", "Cicalengka", "Bandung", "jawa barat", "indonesia", 22.12, -2.332));
        dataAddress.add(new Address(3, "jl. Malabar", "purworejo", "Surabaya", "jawa barat", "indonesia", 22.12, -2.332));
        return dataAddress;
    }

    //data distance
    public ArrayList<DistanceMapper> getDataDistance() {
        ArrayList<DistanceMapper> dataDistance = new ArrayList<>();
        dataDistance.add(new DistanceMapper(1, 1, 2, 15));
        dataDistance.add(new DistanceMapper(2, 2, 3, 20));
        dataDistance.add(new DistanceMapper(3, 3, 1, 25));
        dataDistance.add(new DistanceMapper(4, 3, 2, 20));
        dataDistance.add(new DistanceMapper(5, 1, 3, 25));
        dataDistance.add(new DistanceMapper(6, 2, 1, 15));
        return dataDistance;
    }

    //data transport
    public ArrayList<Transport> getDatatransport() {
        ArrayList<Transport> dataTransport = new ArrayList<>();
        dataTransport.add(new Transport(1, "TATA", 4, 150, "Truck", 10000));
        dataTransport.add(new Transport(2, "TITI", 2, 150, "Pick up", 2000));
        dataTransport.add(new Transport(3, "TUTU", 1, 80, "MotorCycle", 50));
        return dataTransport;
    }

    //data WareHouse
    public ArrayList<Warehouse> getDataWareHouse() {
        ArrayList<Warehouse> dataWarehouse = new ArrayList<>();
        dataWarehouse.add(new Warehouse(1, 1));
        dataWarehouse.add(new Warehouse(2, 2));
        dataWarehouse.add(new Warehouse(3, 3));
        return dataWarehouse;
    }
}
