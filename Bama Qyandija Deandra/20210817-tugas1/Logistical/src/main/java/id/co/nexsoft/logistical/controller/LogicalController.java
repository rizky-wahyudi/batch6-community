package id.co.nexsoft.logistical.controller;

import java.util.Scanner;

public class LogicalController {
    public static DataController data = new DataController();
    public static Scanner scanner = new Scanner(System.in);
    static String nama;
    static boolean salah;
    static int from, to, weight, id = 0, idtransport = 0;

    public void run() {
        addLogistical();
    }

    public static void addLogistical() {
        System.out.println("==== SILAKAN INPUT NAMA BARANG ====");
        System.out.print("Nama Produk : ");
        nama = scanner.nextLine();
        System.out.println("");

        //untuk awal
        for (var a : data.getDataAddress()) {
            System.out.println("No: " + a.getId() + "." + " Jalan : " + a.getStreet() + "kota : " + a.getDistrict());
        }
        System.out.print("dari : ");
        from = scanner.nextInt();
        System.out.println("");

        //untuk tujuan
        for (var a : data.getDataAddress()) {
            System.out.println("No: " + a.getId() + "." + " Jalan : " + a.getStreet() + "kota : " + a.getDistrict());
        }
        System.out.print("tujuan : ");
        to = scanner.nextInt();
        System.out.println("");

        //input berat
        System.out.print("berat : ");
        weight = scanner.nextInt();

        System.out.println("");
        logicalShow();
    }

    public static int idDistance() {
        System.out.println("====== RESI =====");
        salah = false;
        for (var a : data.getDataDistance()) {
            if (from == a.getFromwarehouseid() && to == a.getTowarehouseid()) {
                id = a.getId();
                salah = true;
            }
        }
        if (!salah) {
            System.out.println("Tidak ada tujuan ");
        }
        return id;
    }

    public static void showAddress() {
        System.out.println("===== TUJUAN =====");
        boolean benar = false;
        for (var a : data.getDataDistance()) {
            if (id == a.getId()) {
                for (var b : data.getDataWareHouse()) {
                    if (a.getTowarehouseid() == b.getAddressId()) {
                        for (var c : data.getDataAddress()) {
                            if (b.getAddressId() == c.getId()) {
                                System.out.println("jalan : " + c.getStreet());
                                System.out.println("Kecamatan : " + c.getDistrict());
                                System.out.println("kota : " + c.getCity());
                                System.out.println("Jarak : " + a.getKm());
                                benar = true;
                            }
                        }
                    }
                }
            }
        }
        if (!benar) {
            System.out.println("Tujuan tidak ada ");
        }
    }

    public static void showKendaraan() {
        System.out.println("===== Transport =====");
        if (weight < 50) {
            idtransport += 3;
        } else if (weight < 2000) {
            idtransport += 2;
        } else {
            idtransport += 1;
        }
        if (salah) {
            for (var a : data.getDatatransport()) {
                if (idtransport == a.getId()) {
                    System.out.println("Id Transport : " + a.getId());
                    System.out.println("Transport : " + a.getType());
                    System.out.println("Driver : " + a.getDriver());
                    System.out.println("Speed : " + a.getSpeed());
                }
            }
        } else {
            System.out.println("Harap Megisi dengan benar ");
        }

    }

    public static void logicalShow() {
        System.out.println("id Pengiriman : " + idDistance());
        System.out.println("Nama Produk : " + nama);
        System.out.println("");
        showAddress();
        System.out.println("");
        showKendaraan();
    }
}
