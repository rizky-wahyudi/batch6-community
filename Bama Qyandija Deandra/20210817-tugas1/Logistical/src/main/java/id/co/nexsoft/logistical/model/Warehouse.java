package id.co.nexsoft.logistical.model;

public class Warehouse {

    private int id;
    private int addressId;

    public Warehouse(int id, int addressId) {
        this.id = id;
        this.addressId = addressId;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public Address getAddressId() {
//        return addressId;
//    }
//
//    public void setAddressId(Address addressId) {
//        this.addressId = addressId;
//    }
//
//    public DistanceMapper getDistanceMapper() {
//        return distanceMapper;
//    }
//
//    public void setDistanceMapper(DistanceMapper distanceMapper) {
//        this.distanceMapper = distanceMapper;
//    }
}
