package id.co.nexsoft.logistical.main;

import id.co.nexsoft.logistical.controller.DataController;
import id.co.nexsoft.logistical.controller.LogicalController;


public class Main {

    public static void main(String[] args) {
        LogicalController logicalController = new LogicalController();
        logicalController.run();
    }
}
