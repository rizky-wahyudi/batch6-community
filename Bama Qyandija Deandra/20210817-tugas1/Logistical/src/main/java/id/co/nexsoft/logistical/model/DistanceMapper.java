package id.co.nexsoft.logistical.model;

public class DistanceMapper {
    private int id;
    private int fromwarehouseid;
    private int towarehouseid;
    private int km;

    public DistanceMapper() {

    }

    public DistanceMapper(int id, int fromwarehouseid, int towarehouseid, int km) {
        this.id = id;
        this.fromwarehouseid = fromwarehouseid;
        this.towarehouseid = towarehouseid;
        this.km = km;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromwarehouseid() {
        return fromwarehouseid;
    }

    public void setFromwarehouseid(int fromwarehouseid) {
        this.fromwarehouseid = fromwarehouseid;
    }

    public int getTowarehouseid() {
        return towarehouseid;
    }

    public void setTowarehouseid(int towarehouseid) {
        this.towarehouseid = towarehouseid;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }
}
