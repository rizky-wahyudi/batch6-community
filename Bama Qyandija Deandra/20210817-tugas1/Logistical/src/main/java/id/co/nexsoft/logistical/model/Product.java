package id.co.nexsoft.logistical.model;

public class Product {

    private int id;
    private String name;
    private String deliveryAddress;
    private double width;
    private double height;
    private double depth;
    private int weight;

    public Product() {

    }

    public Product(int id, String name, String deliveryAddress, double width, double height, double depth, int weight) {
        this.id = id;
        this.name = name;
        this.deliveryAddress = deliveryAddress;
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAdress) {
        this.deliveryAddress = deliveryAdress;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
