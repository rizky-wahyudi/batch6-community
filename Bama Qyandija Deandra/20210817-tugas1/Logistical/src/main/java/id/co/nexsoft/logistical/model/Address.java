package id.co.nexsoft.logistical.model;

public class Address {
    private int id;
    private String street;
    private String district;
    private String city;
    private String province;
    private String country;
    private double langitude;
    private double latitude;

    public Address() {
    }

    public Address(int id, String street, String district, String city, String province, String country, double langitude, double latitude) {
        this.id = id;
        this.street = street;
        this.district = district;
        this.city = city;
        this.province = province;
        this.country = country;
        this.langitude = langitude;
        this.latitude = latitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setLangitude(double langitude) {
        this.langitude = langitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLangitude() {
        return langitude;
    }

    public void setLangitude(float langitude) {
        this.langitude = langitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
}
