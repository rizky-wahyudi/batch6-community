package com.translator.model;

public class Temperature {
    private double temperatur;

    public double getTemperatur() {
        return temperatur;
    }

    public void setTemperatur(double temperatur) {
        this.temperatur = temperatur;
    }

    public void conver(int conver) {
        if (conver == 1) {
            System.out.println("dari : " + getTemperatur() + " Fahrenheit" + " menjadi : " + ((getTemperatur() - 32) * 5/9) + " Celcius");
        } else if (conver == 2) {
            System.out.println("dari : " + getTemperatur() + " Celcius" + " menjadi : " + (getTemperatur() * 9/5) + 32 + " Fahrenheit");
        }
    }
}
