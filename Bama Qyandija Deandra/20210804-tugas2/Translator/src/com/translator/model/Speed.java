package com.translator.model;

public class Speed {
    private double speed;

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void converter(int conver) {
        if (conver == 1) {
            System.out.println("dari : " + getSpeed() + " KM" + " menjadi : " + getSpeed() * 0.62137 + " MILES");
        } else if (conver == 2) {
            System.out.println("dari : " + getSpeed() + " MILES" + " menjadi : " + getSpeed() * 1.609 + " KM");
        }
    }
}
