import './Table.css';
// li

function Table(){
    const data = [{
        nama: "Bama",
        age: 18,
        city: "Tangerang"
    },{
        nama: "Qyan",
        age: 18,
        city: "Bandung"
    },{
        nama: "Dija",
        age: 18,
        city: "Tasik"
    }]
    return (
        // <h1>React Dynamic Table</h1>
        <table className="table-border">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>City</th>
                </tr>
            </thead>
            <tbody>
                {
                    data.map((item) => {
                        return (
                            <tr>
                                <td>{item.nama}</td>
                                <td>{item.age}</td>
                                <td>{item.city}</td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </table>
    )
}

export default Table;