import logo from './logo.svg';
import './App.css';
import Header from './component/Header';
import Table from './component/Table';

function App() {
  return (
    <div className="App">
      <header>
        <Header />
        <Table/>
      </header>
    </div>
  );
}

export default App;
