package parent;

import model.Barang;

import java.util.ArrayList;

public class VendingMachine {
    private double bayar = 0;

    public VendingMachine(){

    }
    public VendingMachine(ArrayList<Barang> data){
        for (Barang barang : data){
            System.out.print(barang.getBlock() + " ");
            System.out.print(barang.getNo() + " ");
            System.out.print(barang.getNama() + " ");
            System.out.print(barang.getHarga() + " ");
            System.out.println();
        }
    }

    public double getBayar() {
        return bayar;
    }

    public void setBayar(double bayar) {
        this.bayar = this.bayar + bayar;
    }
}
