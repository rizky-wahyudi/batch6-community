package model;

import parent.VendingMachine;

public class Barang extends VendingMachine {
    private String block;
    private String nama;
    private int no;
    private double harga;

    public Barang (){

    }
    public Barang(String block,int no,String nama, double harga){
        this.block = block;
        this.no = no;
        this.harga = harga;
        this.nama = nama;
    }

    public String getBlock() {
        return block;
    }

    public String getNama() {
        return nama;
    }

    public int getNo() {
        return no;
    }

    public double getHarga() {
        return harga;
    }
}
