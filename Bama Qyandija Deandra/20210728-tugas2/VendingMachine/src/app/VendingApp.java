package app;

import model.Barang;
import parent.VendingMachine;

import java.util.ArrayList;
import java.util.Scanner;

public class VendingApp {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Barang> dataBarang = new ArrayList<Barang>();
        for (int a = 1; a <= 5; a++) {
            dataBarang.add(new Barang("A", a, "Lays " + a, 8000));
        }
        for (int b = 1; b <= 5; b++) {
            dataBarang.add(new Barang("B", b, "Coklat " + b, 5000));
        }
        for (int c = 1; c <= 5; c++) {
            dataBarang.add(new Barang("C", c, "Chitato " + c, 12000));
        }
        for (int d = 1; d <= 5; d++) {
            dataBarang.add(new Barang("D", d, "Freshtea" + d, 5000));
        }
        for (int e = 1; e <= 5; e++) {
            dataBarang.add(new Barang("E", e, "Pocari sweet" + e, 6000));
        }
        for (int f = 1; f <= 5; f++) {
            dataBarang.add(new Barang("F", f, "Adem sari" + f, 8000));
        }

        hasil(dataBarang,input);

    }
    public static void hasil(ArrayList<Barang> dataBarang, Scanner input){
        VendingMachine vm = new VendingMachine(dataBarang);
        System.out.println("\n======== silakan memilih ========");
        System.out.print("blok : ");
        String blok = input.nextLine();
        System.out.print("nomor : ");
        int nomor = input.nextInt();
        boolean sukses = false;
        //pilih index
        for (var data : dataBarang) {
            if (blok.equalsIgnoreCase(data.getBlock()) && nomor == data.getNo()) {
                System.out.println("===========================================");
                System.out.println("yang anda pilih adalah : " + data.getNama());
                System.out.println("harganya : " + data.getHarga());
                System.out.println("1. lanjut");
                System.out.println("2. cancel");
                System.out.print("pilih dengan angka : ");
                int lanjut = input.nextInt();
                if (lanjut == 1){
                    System.out.println("===========================================");
                    System.out.print("silakan masukan uang : ");
                    double bayar = input.nextDouble();
                    vm.setBayar(bayar);
                    if (bayar < data.getHarga()) {
                        while (vm.getBayar() < data.getHarga()){
                            System.out.println("kurang : " + (data.getHarga() - vm.getBayar()));
                            System.out.println("uang anda : "+ vm.getBayar());
                            System.out.println("lanjut? ya / tidak ");
                            String lanj = input.next();
                            if (lanj.equalsIgnoreCase("ya")){
                                System.out.print("silakan memasukan lagi uang : ");
                                double bayarLagi = input.nextDouble();
                                vm.setBayar( bayarLagi);
                                if (vm.getBayar() >= data.getHarga()){
                                    System.out.println("makanan anda : " + data.getNama());
                                    System.out.println("kembaliannya adalah : " + (vm.getBayar() - data.getHarga()));
                                }
                            }else {
                                System.out.println("anda telah keluar ");
                                System.out.println("uang anda kami kembalikan : " + vm.getBayar());
                                break;
                            }
                        }
                    } else if (bayar >= data.getHarga()) {
                        System.out.println("=================================");
                        System.out.println("makanan anda : " + data.getNama());
                        System.out.println("kembaliannya adalah : " + (vm.getBayar() - data.getHarga()));
                    }
                }else {
                    System.out.println("anda telah cancel");
                }
                sukses = true;
            }
        }
        if (sukses == false){
            System.out.println("barang tidak ada ");
        }
    }
}
