public class Tiket extends Airport{
    private String nama;
    private String jadwal;
    private String tujuan;

    public Tiket(){

    }

    public Tiket(String nama, String jadwal, String tujuan) {
        this.nama = nama;
        this.jadwal = jadwal;
        this.tujuan = tujuan;
    }

    public String getNama() {
        return nama;
    }

    public String getJadwal() {
        return jadwal;
    }

    public String getTujuan() {
        return tujuan;
    }
}
