public class Pasport {
    private String nama;
    private String nik;
    private String asal;

    public Pasport(String nama, String nik, String asal) {
        this.nama = nama;
        this.nik = nik;
        this.asal = asal;
    }

    public String getNama() {
        return nama;
    }

    public String getNik() {
        return nik;
    }

    public String getAsal() {
        return asal;
    }
}
