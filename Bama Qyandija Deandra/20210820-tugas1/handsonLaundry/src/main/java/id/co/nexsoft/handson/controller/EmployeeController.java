package id.co.nexsoft.handson.controller;

import id.co.nexsoft.handson.entity.Address;
import id.co.nexsoft.handson.entity.Employee;
import id.co.nexsoft.handson.repository.AddressRepository;
import id.co.nexsoft.handson.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    AddressRepository addressRepository;

    //post employee with addres for univers
    @PostMapping("/employee")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee addEmployee(@RequestBody Employee employee){
        Address address = employee.getAddress();
        addressRepository.save(address);
        return employeeRepository.save(employee);
    }

    //get employee data
    @GetMapping("/employee")
    public List<Employee> getEmployee(){
       return employeeRepository.findAll();
    }

}
