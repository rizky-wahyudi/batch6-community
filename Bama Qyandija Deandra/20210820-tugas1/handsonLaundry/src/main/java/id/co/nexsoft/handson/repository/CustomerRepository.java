package id.co.nexsoft.handson.repository;

import id.co.nexsoft.handson.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    Customer findById(int id);

    List<Customer> findAll();

}
