package id.co.nexsoft.handson.repository;

import id.co.nexsoft.handson.entity.Service;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends CrudRepository<Service, Integer> {
    Service findById(int id);

    List<Service> findAll();
}
