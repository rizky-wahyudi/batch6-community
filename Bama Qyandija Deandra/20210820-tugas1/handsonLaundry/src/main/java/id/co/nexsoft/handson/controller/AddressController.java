package id.co.nexsoft.handson.controller;

import id.co.nexsoft.handson.entity.Address;
import id.co.nexsoft.handson.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AddressController {

    @Autowired
    AddressRepository addressRepository;

    @GetMapping("/address")
    public List<Address> showAll(){
        return addressRepository.findAll();
    }

    @PostMapping("/address")
    public Address inputAddress(@RequestBody Address address){
        return addressRepository.save(address);
    }
}
