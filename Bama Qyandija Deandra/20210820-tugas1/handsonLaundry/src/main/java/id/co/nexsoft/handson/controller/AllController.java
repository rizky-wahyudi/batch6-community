package id.co.nexsoft.handson.controller;


import id.co.nexsoft.handson.entity.Orders;
import id.co.nexsoft.handson.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AllController {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ServiceRepository serviceRepository;




    @GetMapping("/")
    public String welcome(){
        return "SELAMAT DATANG ";
    }

    @GetMapping("/alllaundry")
    public List<Orders> getAll(){
        employeeRepository.findAll();
        customerRepository.findAll();

        return orderRepository.findAll();
    }
}
