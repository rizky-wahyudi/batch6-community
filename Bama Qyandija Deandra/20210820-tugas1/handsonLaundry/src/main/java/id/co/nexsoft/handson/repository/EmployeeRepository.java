package id.co.nexsoft.handson.repository;

import id.co.nexsoft.handson.entity.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    Employee findById(int id);

    List<Employee> findAll();

}
