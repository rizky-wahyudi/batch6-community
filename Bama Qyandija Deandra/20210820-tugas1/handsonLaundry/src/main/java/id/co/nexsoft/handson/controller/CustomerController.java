package id.co.nexsoft.handson.controller;

import id.co.nexsoft.handson.entity.Address;
import id.co.nexsoft.handson.entity.Customer;
import id.co.nexsoft.handson.repository.AddressRepository;
import id.co.nexsoft.handson.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    AddressRepository addressRepository;

    @GetMapping("/customers")
    public List<Customer> getAll(){
        return customerRepository.findAll();
    }

    @PostMapping("/customers")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Customer> addCust(@RequestBody Customer customer){
        Address address = customer.getAddress();
        addressRepository.save(address);
        customerRepository.save(customer);
        if (customerRepository!= null){
            return customerRepository.findAll();
        }else {
            return null;
        }

    }
}
