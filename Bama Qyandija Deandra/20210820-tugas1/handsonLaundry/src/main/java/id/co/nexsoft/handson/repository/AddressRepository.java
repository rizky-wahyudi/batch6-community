package id.co.nexsoft.handson.repository;

import id.co.nexsoft.handson.entity.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {
    Address findById(int id);

    List<Address> findAll();

}
