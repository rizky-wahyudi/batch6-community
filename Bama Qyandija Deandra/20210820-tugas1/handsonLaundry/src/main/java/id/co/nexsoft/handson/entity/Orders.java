package id.co.nexsoft.handson.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToMany(targetEntity = OrdersItem.class)
    private List<OrdersItem> ordersItem;

    @JsonFormat(locale = "YYYY-MM-DD")
    private LocalDate date;


    private String status;
    private String paymentStatus;

    @JsonFormat(locale = "YYYY-MM-DD")
    private LocalDate lastUpdateDate;
    private String updateBy;

    public Orders(){

    }

    public Orders(List<OrdersItem> ordersItem, LocalDate date, String status, String paymentStatus, LocalDate lastUpdateDate, String updateBy) {
        this.ordersItem = ordersItem;
        this.date = date;
        this.status = status;
        this.paymentStatus = paymentStatus;
        this.lastUpdateDate = lastUpdateDate;
        this.updateBy = updateBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<OrdersItem> getOrdersItem() {
        return ordersItem;
    }

    public void setOrdersItem(List<OrdersItem> ordersItem) {
        this.ordersItem = ordersItem;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public LocalDate getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDate lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
}
