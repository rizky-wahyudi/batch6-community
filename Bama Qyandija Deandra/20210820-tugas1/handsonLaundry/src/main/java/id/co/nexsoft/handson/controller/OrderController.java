package id.co.nexsoft.handson.controller;

import id.co.nexsoft.handson.entity.Orders;
import id.co.nexsoft.handson.entity.OrdersItem;
import id.co.nexsoft.handson.entity.Service;
import id.co.nexsoft.handson.repository.OrderItemRepository;
import id.co.nexsoft.handson.repository.OrderRepository;
import id.co.nexsoft.handson.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @GetMapping("/order")
    public List<Orders> showAll(){
        return orderRepository.findAll();
    }

    @PostMapping("/order")
    @ResponseStatus(HttpStatus.CREATED)
    public String order(@RequestBody Orders orderEntity){
        if (orderEntity.getOrdersItem().size() > 0) {
            for (OrdersItem ordersItem : orderEntity.getOrdersItem()) {
                Service serviceEntity = ordersItem.getService();
                serviceRepository.save(serviceEntity);
                orderItemRepository.save(ordersItem);
            }
            orderRepository.save(orderEntity);
        }else {
            return "Null Point Exception";
        }
        return "Work";

    }

}
