package id.co.nexsoft.handson.controller;

import id.co.nexsoft.handson.entity.Service;
import id.co.nexsoft.handson.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServiceController {

    @Autowired
    private ServiceRepository serviceRepository;

    @PostMapping("/service")
    @ResponseStatus(HttpStatus.CREATED)
    public String addService(@RequestBody Service service){
//        boolean add = false;
        String work = "YEAYY";
        if (service != null){
            serviceRepository.save(service);
//            add = true;
            return work;
        }else {
            return "anda Gagal";
        }
    }

    @GetMapping("/service")
    public List<Service> showAll(){
        return serviceRepository.findAll();
    }
}
