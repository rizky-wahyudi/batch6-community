package id.co.nexsoft.handson.repository;

import id.co.nexsoft.handson.entity.Orders;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Orders, Integer> {
    Orders findById(int id);

    List<Orders> findAll();
}
