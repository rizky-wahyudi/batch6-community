package id.co.nexsoft.handson.repository;

import id.co.nexsoft.handson.entity.OrdersItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends CrudRepository<OrdersItem, Integer> {
    OrdersItem findById(int id);

    List<OrdersItem> findAll();

}
