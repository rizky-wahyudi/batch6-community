package id.co.nexsoft.handson.entity;

import javax.persistence.*;

@Entity
public class OrdersItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String itemName;
    private int weight;
    private double amount;


    @OneToOne(targetEntity = Service.class)
    private Service service;

    public OrdersItem(){

    }

    public OrdersItem(String itemName, int weight, double amount, Service service) {
        this.itemName = itemName;
        this.weight = weight;
        this.amount = amount;
        this.service = service;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
