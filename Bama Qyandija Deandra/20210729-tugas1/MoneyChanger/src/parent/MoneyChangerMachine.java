package parent;

import model.Curs;

import java.util.ArrayList;

public class MoneyChangerMachine {
    private double value;

    public MoneyChangerMachine() {

    }

    public MoneyChangerMachine(ArrayList<Curs> data) {
        for (var curs : data){
            System.out.println(curs.getNo() + " " + curs.getNama());
        }
    }


    public double getValue() {
        return value;
    }


}
