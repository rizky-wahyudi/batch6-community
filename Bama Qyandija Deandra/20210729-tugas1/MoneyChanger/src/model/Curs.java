package model;

import parent.MoneyChangerMachine;

public class Curs extends MoneyChangerMachine {
    private String nama;
    private double value;
    private int no;

    public Curs(){

    }
    public Curs(int no,String nama, double value){
        this.no = no;
        this.nama = nama;
        this.value = value;
    }

    public String getNama() {
        return nama;
    }

    public double getValue() {
        return value;
    }

    public int getNo() {
        return no;
    }
}
