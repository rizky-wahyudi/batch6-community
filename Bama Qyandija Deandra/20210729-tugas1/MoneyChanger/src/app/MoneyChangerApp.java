package app;

import model.Curs;
import parent.MoneyChangerMachine;

import java.util.ArrayList;
import java.util.Scanner;

public class MoneyChangerApp {
    public static void main(String[] args) {
        ArrayList<Curs> dataMoney= new ArrayList();
        Scanner input = new Scanner(System.in);
            run(dataMoney);
            proces(input,dataMoney);
    }

    public static void run(ArrayList<Curs> dataMoney) {
        dataMoney.add(new Curs(1,"IDR - EUR", 17000));
        dataMoney.add(new Curs(2,"IDR - USD", 15000));
        dataMoney.add(new Curs(3,"USD - IDR",15000));
        dataMoney.add(new Curs(4,"EUR - IDR",17000));
        MoneyChangerMachine moneyChangerMachine = new MoneyChangerMachine(dataMoney);
    }
    public static void proces(Scanner input, ArrayList<Curs> dataMoney){
        System.out.println("\n======== silakan memilih ========");
        System.out.print("Curs : ");
        int pilih = input.nextInt();
        System.out.print("masukan nominal : ");
        double uang = input.nextInt();

        System.out.println("proses..");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean suskes = false;
        for (var data : dataMoney){
            if (pilih == data.getNo() && data.getNo() == 1){
                System.out.println("anda memilih : " + data.getNama());
                System.out.println("uang anda : " + ( uang / data.getValue()));
            }else if ( pilih == data.getNo() && data.getNo() == 2){
                System.out.println("anda memilih : " + data.getNama());
                System.out.println("uang anda : " + ( uang / data.getValue()));
            }else if (pilih == data.getNo() && data.getNo() == 3){
                System.out.println("anda memilih : " + data.getNama());
                System.out.println("uang anda : " + ( uang * data.getValue()));
            }else if (pilih == data.getNo() && data.getNo() == 4){
                System.out.println("anda memilih : " + data.getNama());
                System.out.println("uang anda : " + ( uang * data.getValue()));
            }
            suskes = true;
        }
        if (!suskes){
            System.out.println("Money changer tidak memiliki curs tersebut :)");
        }

    }
}
