import java.util.Arrays;

public class Tokyo extends Liburan{
        private String [] wisata;
        private int harga;

        public String[] getWisata() {
                return wisata;
        }

        public void setWisata(String[] wisata) {
                this.wisata = wisata;
        }

        public int getHarga() {
                return harga;
        }

        public void setHarga(int harga) {
                this.harga = harga;
        }

}
