public class Liburan {
    private String destinasi;
    private int jumlah;
    private int hari;
    private String[] wisata;

    public String[] getWisata() {
        return wisata;
    }

    public void setWisata(String[] wisata) {
        this.wisata = wisata;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public int getHari() {
        return hari;
    }

    public void setHari(int hari) {
        this.hari = hari;
    }

    public String getDestinasi() {
        return destinasi;
    }

    public void setDestinasi(String destinasi) {
        this.destinasi = destinasi;
    }
}
