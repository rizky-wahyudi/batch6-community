public class LiburanFactory {
    public static Liburan newInstance(String type){
        if (type.equals("Bandung".toLowerCase())){
            return new Bandung();
        }else if (type.equals("New York".toLowerCase())){
            return new NewYork();
        }else if (type.equals("Tokyo".toLowerCase())){
            return new Tokyo();
        }else {
            System.out.println("destinasi tidak ada");
        }
        return null;
    }
}
