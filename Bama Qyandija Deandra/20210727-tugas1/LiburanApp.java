import java.util.Locale;
import java.util.Scanner;

public class LiburanApp {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String destinasi;
        String[] tokyo = {"Tokyo Sky three", "meiji jinggu"};
        String[] newyork = {"sentral Park", "empire"};
        String[] bandung = {"Tangkuban perahu", "jalan braga"};

        //init data object
        Tokyo tokyo1 = new Tokyo();
        tokyo1.setWisata(tokyo);
        tokyo1.setHarga(1000);
        NewYork newYork1 = new NewYork();
        newYork1.setWisata(newyork);
        newYork1.setHarga(2000);
        Bandung bandung1 = new Bandung();
        bandung1.setWisata(bandung);
        bandung1.setHarga(200);

        //pilih
        System.out.println("Bandung");
        System.out.println("Tokyo");
        System.out.println("New york");
        System.out.print("Silakan memilih destinasi : ");
        destinasi = input.nextLine();

        Liburan libur = LiburanFactory.newInstance(destinasi);
        libur.setDestinasi(destinasi);

        System.out.println("destinasi : " + libur.getDestinasi());

        System.out.print("Berapa orang ? : " );
        int orang = input.nextInt();
        libur.setJumlah(orang);

        System.out.print("Berapa hari? : ");
        int hari = input.nextInt();
        libur.setHari(hari);

        //output destinasi
        System.out.println("Anda akan pergi ke : " + libur.getDestinasi());

        if (libur instanceof Bandung){
            System.out.println("anda akan pergi ke : " );
            for (var data:bandung
                 ) {
                System.out.println(data + ", ");
            }
            System.out.println("Biaya yang perlu di keluar kan adalah : " + bandung1.getHarga()*10*orang);
        }else if (libur instanceof NewYork){
            System.out.println("anda akan pergi ke : " );
            for (var data:newyork
            ) {
                System.out.println(data + ", ");
            }
        }else if (libur instanceof Tokyo){
            System.out.println("anda akan pergi ke : " );
            for (var data:newyork
            ) {
                System.out.println(data + ", ");
            }
            System.out.println("Biaya yang perlu di keluar kan adalah : " + tokyo1.getHarga()*10*orang + "Dollar");
        }else {
            System.out.println("Anda tidak pergi ke destinasi manapun ");
        }

    }
}
