public class Bandung extends Liburan{
    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    @Override
    public String[] getWisata() {
        return wisata;
    }

    @Override
    public void setWisata(String[] wisata) {
        this.wisata = wisata;
    }

    private int harga;
    private String[] wisata;

}
