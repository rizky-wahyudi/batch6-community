package model;

public class ProdukItem extends OrderItem{
    int id;
    private String deskripsi;
    private double sellPrice;
    private double tax;

    public int getId() {
        return id;
    }

    public ProdukItem(){

    }

    public ProdukItem(int id, String articleNumber, String deskripsi, double purchasPrice, double sellPrice, double tax) {
        this.deskripsi = deskripsi;
        this.sellPrice = sellPrice;
        this.tax = tax;
        this.id = id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }


    public double getSellPrice() {
        return sellPrice;
    }

    public double getTax() {
        return tax;
    }

}
