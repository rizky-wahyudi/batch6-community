package model;

import java.util.ArrayList;

public class OrderItem extends Order{
    private String orderId;
    private double ammount;

    public OrderItem(){

    }
    public OrderItem(ArrayList<ProdukItem> data){
        for (var produk : data){
            System.out.println(produk.getId() + " " + produk.getDeskripsi() + " " + produk.getSellPrice());
        }
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }

    public double getAmmount() {
        return ammount;
    }

    public String getOrderId() {
        return orderId;
    }

}
