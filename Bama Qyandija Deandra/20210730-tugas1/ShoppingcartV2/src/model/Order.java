package model;

public class Order {
    private double total;
    private String status;
    private double bayar;
    private double kembalian;


    public double getBayar() {
        return bayar;
    }

    public void setBayar(double bayar) {
        this.bayar = this.bayar + bayar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = this.total + total;
    }

    public void setKembalian(double kembalian) {
        this.kembalian = kembalian;
    }

    public double getKembalian() {
        return kembalian;
    }
}
