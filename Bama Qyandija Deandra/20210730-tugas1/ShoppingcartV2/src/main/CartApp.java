package main;

import model.Employee;
import model.Order;
import model.OrderItem;
import model.ProdukItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class CartApp {
    static Order order = new Order();
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("===== Selamat Datang Di BAMART=====");
        setOrder(allProduct());
    }

    public static ArrayList<ProdukItem> allProduct() {
        ArrayList<ProdukItem> dataProduk = new ArrayList<>();
        dataProduk.add(new ProdukItem(1, "MKN1", "Chitato", 8000, 9000, 5));
        dataProduk.add(new ProdukItem(2, "SBN1", "Rinso", 10000, 11000, 7));
        dataProduk.add(new ProdukItem(3, "MNM", "Coca Cola", 5500, 6500, 10));
        order.setStatus("unpaid");

        return dataProduk;
    }

    public static void setOrder(ArrayList<ProdukItem> data) {
        ArrayList keranjang = new ArrayList();
        OrderItem orderItem;
        double tax = 0;
        boolean add = true;
        boolean ada = false;

        do {
            System.out.println("\n");
            orderItem = new OrderItem(data);
            System.out.print("Mau belanja apa? : ");
            int i = input.nextInt();

            for (var dataitem : data) {
                if (i == dataitem.getId()) {
                    System.out.println();
                    System.out.println("Anda memilih : " + dataitem.getDeskripsi());
                    System.out.print("mau beli berapa? ");
                    int buy = input.nextInt();
                    keranjang.add(dataitem.getId() + "," + buy);
                    System.out.println("ada lagi ? : ");
                    System.out.println("1. ya");
                    System.out.println("2. tidak");
                    System.out.print("silakan pilih : ");
                    int lagi = input.nextInt();
                    add = lagi == 1;
                    ada = true;
                }
            }
            if (!ada) {
                System.out.println("Barang tidak ada :( ");
            }
        } while (add);
        System.out.println("status : " + order.getStatus());
        for (var belanja : keranjang) {
//            System.out.println("belanja anda " + belanja);
            String[] allItem = belanja.toString().split(",");
            for (var item : data) {
                if (Integer.parseInt(allItem[0]) == item.getId()) {
                    order.setTotal(item.getSellPrice() * Integer.parseInt(allItem[1]) + (item.getSellPrice() * Integer.parseInt(allItem[1]) * item.getTax() / 100));
                    tax = item.getTax() + tax;
                }
            }
        }
        orderItem.setOrderId("bm2321");
        bayar(orderItem, tax, keranjang, data);
    }

    public static void bayar(OrderItem orderItem, double tax, ArrayList listItem, ArrayList<ProdukItem> produkItems) {
        Employee employee = new Employee();

        employee.setId("bm1");
        employee.setBranchId("INDMRT");
        employee.setNama("Abdul dudul");

        System.out.println("Total : " + order.getTotal());
        System.out.print("bayar : ");
        double bayar = input.nextDouble();
        order.setBayar(bayar);
        order.setStatus("paid");

        if (bayar < order.getTotal()) {
            while (order.getBayar() < order.getTotal()) {
                System.out.println("\n");
                System.out.println("kurang : " + (order.getTotal() - order.getBayar()));
                System.out.println("uang anda : " + order.getBayar());
                System.out.print("silakan masukan uang : ");
                double bayarLagi = input.nextDouble();
                order.setBayar(bayarLagi);

                if (order.getBayar() >= order.getTotal()) {
                    order.setKembalian(order.getBayar() - order.getTotal());
                }
            }
        } else {
            order.setKembalian(order.getBayar() - order.getTotal());
        }
        struk(employee, order.getBayar(), orderItem.getOrderId(), tax, listItem, produkItems);
    }

    public static void struk(Employee employee, double bayar, String id, double tax, ArrayList listItem, ArrayList<ProdukItem> produkItems) {
        Date date = new Date();

        System.out.println("\n======= pembayaran ======");
        System.out.println("Bayar : " + bayar);
        System.out.println("kembalian : " + order.getKembalian());
        System.out.println("status : " + order.getStatus());

        System.out.println("\n======== Struk ========");
        System.out.println("anda membeli : ");
        for (var produk : listItem) {
            String[] allItem = produk.toString().split(",");
            for (var item : produkItems) {
                if (Integer.parseInt(allItem[0]) == item.getId()) {
                    item.setAmmount(item.getSellPrice() * Integer.parseInt(allItem[1]) + (item.getSellPrice() * Integer.parseInt(allItem[1]) * item.getTax() / 100));
                    System.out.println(item.getDeskripsi() + " " + allItem[1] + "x" + " " + item.getAmmount());
                }
            }
        }
        System.out.println("order date : " + date);
        System.out.println("nama Kasir : " + employee.getNama());
        System.out.println("order number : " + id);
        System.out.println("branch id : " + employee.getBranchId());
        System.out.println("total nya : " + order.getTotal());
        System.out.println("tax : " + tax);
    }
}
