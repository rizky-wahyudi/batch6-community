package undangundang.main;

import java.util.ArrayList;
import java.util.Scanner;

import undangundang.model.DetailUndang;
import undangundang.model.Pasal;
import undangundang.model.Peraturan;

public class Main {
	static ArrayList<Peraturan> uu = new ArrayList<>();
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		start();
		Peraturan peraturan = new Peraturan();
		boolean loop = true;
		while (loop) {
			System.out.println("1.Nomor Peraturan");
			System.out.println("2.Tahun Peraturan");
			System.out.println("3.Tentang");
			System.out.print("Pilih :");
			int pilih = sc.nextInt();

			switch (pilih) {
			case 1:
				System.out.print("Masukkan nomor Peraturan: ");
				int no = sc.nextInt();
				peraturan.cariBaseByNo(no, uu);
				choose(peraturan.cariBaseByNo(no, uu));
				break;
			case 2:
				System.out.print("Masukkan tahun Peraturan: ");
				int tahun = sc.nextInt();
				peraturan.cariBaseByTahun(tahun, uu);
				choose(peraturan.cariBaseByTahun(tahun, uu));
				break;
			case 3:
				System.out.println("Masukkan tentang Peraturan: ");
				String tentang = sc.next();
				peraturan.cariBaseByTentang(tentang, uu);
				choose(peraturan.cariBaseByTentang(tentang, uu));
				break;
			default:
				loop = false;
				break;
			}
		}

	}

	public static void start() {
		Peraturan undang = new Peraturan();
		undang.setJenis("UU");
		undang.setLn(67);
		undang.setTln(6476);
		undang.setTanggalDitetapkan("2020-02-08");
		undang.setTanggalDiundangkan("2020-02-28");
		undang.setNomorPeraturan(1);
		undang.setTentang(
				"PENGESAHAN PERSETUJUAN KEMITRAAN EKONOMI KOMPREHENSIF INDONESIA–AUSTRALIA (INDONESIA–AUSTRALIA COMPREHENSIVE ECONOMIC PARTNERSHIP AGREEMENT)");
		undang.setTahunPeraturan(2020);
		DetailUndang d1 = new DetailUndang();
		ArrayList<String> menimbang = new ArrayList<>();
		ArrayList<String> mengingat = new ArrayList<>();
		String menetapkan;
		ArrayList<Pasal> pasal = new ArrayList<>();
		ArrayList<String> isiPasal = new ArrayList<>();
		menimbang.add("bahwa kegiatan perdagangan merupakan salah satu \r\n"
				+ "sektor utama penggerak perekonomian nasional yang \r\n"
				+ "dapat dilakukan melalui kerja sama perdagangan \r\n" + "internasional untuk mendukung program \r\n"
				+ "pembangunan nasional di bidang ekonomi dalam \r\n"
				+ "rangka memajukan kesejahteraan umum sebagaimana \r\n"
				+ "tercantum dalam Pembukaan Undang-Undang Dasar \r\n" + "Negara Republik Indonesia Tahun 1945;\r\n"
				+ "");
		menimbang.add("bahwa untuk meningkatkan kerja sama ekonomi \r\n"
				+ "secara komprehensif antara Indonesia dan Australia, \r\n"
				+ "Pemerintah Republik Indonesia dan Pemerintah \r\n"
				+ "Australia telah menandatangani Persetujuan \r\n"
				+ "Kemitraan Ekonomi Komprehensif Indonesia–Australia \r\n"
				+ "(Indonesia–Australia Comprehensive Economic \r\n"
				+ "Partnership Agreement) pada tanggal 4 Maret 2019 di \r\n" + "Jakarta, Indonesia;\r\n" + "");
		mengingat
				.add("Pasal 5 ayat (1), Pasal 11, dan Pasal 20 UndangUndang Dasar Negara Republik Indonesia Tahun \r\n"
						+ "1945;");
		mengingat.add("Undang-Undang Nomor 24 Tahun 2000 tentang \r\n"
				+ "Perjanjian Internasional (Lembaran Negara Republik \r\n"
				+ "Indonesia Tahun 2000 Nomor 185, Tambahan \r\n" + "Lembaran Negara Republik Indonesia Nomor 4012);");
		menetapkan = "UNDANG-UNDANG TENTANG PENGESAHAN \r\n" + "PERSETUJUAN KEMITRAAN EKONOMI KOMPREHENSIF \r\n"
				+ "INDONESIA–AUSTRALIA (INDONESIA–AUSTRALIA \r\n" + "COMPREHENSIVE ECONOMIC PARTNERSHIP \r\n"
				+ "AGREEMENT)";
		isiPasal.add(" Mengesahkan Persetujuan Kemitraan Ekonomi \r\n"
				+ "Komprehensif Indonesia–Australia (Indonesia–Australia \r\n"
				+ "Comprehensive Economic Partnership Agreement) yang \r\n"
				+ "telah ditandatangani pada tanggal 4 Maret 2019 di \r\n" + "Jakarta, Indonesia.\r\n" + "");
		isiPasal.add(" Mengesahkan Persetujuan Kemitraan Ekonomi \r\n"
				+ "Komprehensif Indonesia–Australia (Indonesia–Australia \r\n"
				+ "Comprehensive Economic Partnership Agreement) yang \r\n"
				+ "telah ditandatangani pada tanggal 4 Maret 2019 di \r\n" + "Jakarta, Indonesia.\r\n" + "");
		Pasal pa = new Pasal(1, isiPasal);
		pasal.add(pa);

		isiPasal = new ArrayList<>();
		isiPasal.add("Undang-Undang ini mulai berlaku pada tanggal \r\n" + "diundangkan.\r\n" + "");

		pa = new Pasal(2, isiPasal);
		pasal.add(pa);
		d1.setMenimbang(menimbang);
		d1.setMengingat(mengingat);
		d1.setMenetapkan(menetapkan);
		d1.setPasal(pasal);
		undang.setDetailUndang(d1);
		uu.add(undang);

		undang = new Peraturan();
		undang.setJenis("UU");
		undang.setLn(245);
		undang.setTln(6573);
		undang.setTanggalDitetapkan("2020-11-02");
		undang.setTanggalDiundangkan("2020-11-02");
		undang.setNomorPeraturan(11);
		undang.setTentang("CIPTA KERJA");
		undang.setTahunPeraturan(2020);
		d1 = new DetailUndang();
		menimbang = new ArrayList<>();
		mengingat = new ArrayList<>();
		pasal = new ArrayList<>();
		isiPasal = new ArrayList<>();
		menimbang.add("bahwa untuk mewujudkan tujuan pembentukan \r\n"
				+ "Pemerintah Negara Indonesia dan mewujudkan \r\n"
				+ "masyarakat Indonesia yang sejahtera, adil, dan makmur \r\n"
				+ "berdasarkan Pancasila dan Undang-Undang Dasar \r\n"
				+ "Negara Republik Indonesia Tahun 1945, Negara perlu \r\n"
				+ "melakukan berbagai upaya untuk memenuhi hak warga \r\n"
				+ "negara atas pekerjaan dan penghidupan yang layak bagi \r\n" + "kemanusiaan melalui cipta kerja;");
		menimbang.add("bahwa untuk mewujudkan tujuan pembentukan \r\n"
				+ "Pemerintah Negara Indonesia dan mewujudkan \r\n"
				+ "masyarakat Indonesia yang sejahtera, adil, dan makmur \r\n"
				+ "berdasarkan Pancasila dan Undang-Undang Dasar \r\n"
				+ "Negara Republik Indonesia Tahun 1945, Negara perlu \r\n"
				+ "melakukan berbagai upaya untuk memenuhi hak warga \r\n"
				+ "negara atas pekerjaan dan penghidupan yang layak bagi \r\n" + "kemanusiaan melalui cipta kerja; ");
		menimbang.add("bahwa untuk mewujudkan tujuan pembentukan \r\n"
				+ "Pemerintah Negara Indonesia dan mewujudkan \r\n"
				+ "masyarakat Indonesia yang sejahtera, adil, dan makmur \r\n"
				+ "berdasarkan Pancasila dan Undang-Undang Dasar \r\n"
				+ "Negara Republik Indonesia Tahun 1945, Negara perlu \r\n"
				+ "melakukan berbagai upaya untuk memenuhi hak warga \r\n"
				+ "negara atas pekerjaan dan penghidupan yang layak bagi \r\n" + "kemanusiaan melalui cipta kerja; ");
		menimbang.add("bahwa pengaturan yang berkaitan dengan kemudahan,\r\n"
				+ "perlindungan, dan pemberdayaan koperasi dan usaha \r\n"
				+ "mikro, kecil, dan menengah, peningkatan ekosistem \r\n"
				+ "investasi, dan percepatan proyek strategis nasional, \r\n"
				+ "termasuk peningkatan perlindungan dan kesejahteraan \r\n"
				+ "pekerja yang tersebar di berbagai Undang-Undang \r\n"
				+ "sektor saat ini belum dapat memenuhi kebutuhan \r\n"
				+ "hukum untuk percepatan cipta kerja sehingga perlu \r\n" + "dilakukan perubahan;");
		mengingat.add("Pasal 4, Pasal 5 ayat (1), Pasal 18, Pasal 18A, Pasal 18B, \r\n"
				+ "Pasal 20, Pasal 22D ayat (2), Pasal 27 ayat (2), Pasal \r\n"
				+ "28D ayat (1) dan ayat (2), dan Pasal 33 Undang-Undang \r\n"
				+ "Dasar Negara Republik Indonesia Tahun 1945;");
		mengingat.add("Ketetapan Majelis Permusyawaratan Rakyat Republik \r\n"
				+ "Indonesia Nomor XVI/MPR/1998 tentang Politik \r\n" + "Ekonomi Dalam Rangka Demokrasi Ekonomi;");
		mengingat.add("Ketetapan Majelis Permusyawaratan Rakyat Republik \r\n"
				+ "Indonesia Nomor IX/MPR/2001 tentang Pembaruan \r\n" + "Agraria dan Pengelolaan Sumber Daya Alam;");
		menetapkan = "Ketetapan Majelis Permusyawaratan Rakyat Republik \r\n"
				+ "Indonesia Nomor IX/MPR/2001 tentang Pembaruan \r\n" + "Agraria dan Pengelolaan Sumber Daya Alam;";
		isiPasal.add("Cipta Kerja adalah upaya penciptaan kerja melalui \r\n"
				+ "usaha kemudahan, perlindungan, dan pemberdayaan \r\n"
				+ "koperasi dan usaha mikro, kecil, dan menengah, \r\n"
				+ "peningkatan ekosistem investasi dan kemudahan \r\n"
				+ "berusaha, dan investasi Pemerintah Pusat dan \r\n" + "percepatan proyek strategis nasional.");
		pa = new Pasal(1, isiPasal);
		pasal.add(pa);

		isiPasal = new ArrayList<>();
		isiPasal.add("Cipta Kerja adalah upaya penciptaan kerja melalui \r\n"
				+ "usaha kemudahan, perlindungan, dan pemberdayaan \r\n"
				+ "koperasi dan usaha mikro, kecil, dan menengah, \r\n"
				+ "peningkatan ekosistem investasi dan kemudahan \r\n"
				+ "berusaha, dan investasi Pemerintah Pusat dan \r\n" + "percepatan proyek strategis nasional.");
		pa = new Pasal(2, isiPasal);
		pasal.add(pa);

		isiPasal = new ArrayList<>();
		isiPasal.add("Usaha Mikro, Kecil, dan Menengah yang selanjutnya \r\n"
				+ "disingkat UMK-M adalah usaha mikro, usaha kecil, dan \r\n"
				+ "usaha menengah sebagaimana dimaksud dalam \r\n"
				+ "Undang-Undang tentang Usaha Mikro, Kecil, dan \r\n" + "Menengah.\r\n" + "");

		pa = new Pasal(3, isiPasal);
		pasal.add(pa);
		d1.setMenimbang(menimbang);
		d1.setMengingat(mengingat);
		d1.setMenetapkan(menetapkan);
		d1.setPasal(pasal);
		undang.setDetailUndang(d1);
		uu.add(undang);

	}

	public static void choose(DetailUndang detail) {
		boolean loop = true;
		while (loop) {
			System.out.println("1. Menimbang");
			System.out.println("2. Mengingatkan");
			System.out.println("3. Pasal");
			System.out.println("Tampilkan : ");
			int choice = sc.nextInt();
			if (choice == 1) {
				detail.getDetail(detail.getMenimbang());
			} else if (choice == 2) {
				detail.getDetail(detail.getMengingat());
			} else if (choice == 3) {
				detail.getPasalDetail(detail.getPasal());
			} else {
				loop = false;
			}
		}
	}
}
