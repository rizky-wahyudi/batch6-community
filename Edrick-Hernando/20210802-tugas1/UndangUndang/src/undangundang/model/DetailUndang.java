package undangundang.model;

import java.util.ArrayList;

public class DetailUndang {
	private ArrayList<String> menimbang;
	private ArrayList<String> mengingat;
	private String menetapkan;
	private ArrayList<Pasal> pasal;

	public ArrayList<String> getMenimbang() {
		return menimbang;
	}

	public void setMenimbang(ArrayList<String> menimbang) {
		this.menimbang = menimbang;
	}

	public ArrayList<String> getMengingat() {
		return mengingat;
	}

	public void setMengingat(ArrayList<String> mengingat) {
		this.mengingat = mengingat;
	}

	public String getMenetapkan() {
		return menetapkan;
	}

	public void setMenetapkan(String menetapkan) {
		this.menetapkan = menetapkan;
	}

	public ArrayList<Pasal> getPasal() {
		return pasal;
	}

	public void setPasal(ArrayList<Pasal> pasal) {
		this.pasal = pasal;
	}

	
	public void getDetail(ArrayList<String>data) {
		for(int i=0;i<data.size();i++) {
			System.out.println((i+1)+" "+data.get(i));
		}
	}
	public void getPasalDetail(ArrayList<Pasal>data) {
		for(int i=0;i<data.size();i++) {
			data.get(i).showPasal();
		}
	}
	
	public void showDetail() {
		getDetail(menimbang);
		getDetail(mengingat);
		getPasalDetail(pasal);
	}
}
