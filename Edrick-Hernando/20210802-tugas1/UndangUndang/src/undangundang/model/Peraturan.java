package undangundang.model;

import java.util.ArrayList;

public class Peraturan {
	private String jenis;
	private String tanggalDiundangkan;
	private String tanggalDitetapkan;
	private int ln;
	private int tln;
	private int nomorPeraturan;
	private int tahunPeraturan;
	private DetailUndang detailUndang;
	private String tentang;

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public String getTanggalDiundangkan() {
		return tanggalDiundangkan;
	}

	public void setTanggalDiundangkan(String tanggalDiundangkan) {
		this.tanggalDiundangkan = tanggalDiundangkan;
	}

	public String getTanggalDitetapkan() {
		return tanggalDitetapkan;
	}

	public void setTanggalDitetapkan(String tanggalDitetapkan) {
		this.tanggalDitetapkan = tanggalDitetapkan;
	}

	public int getLn() {
		return ln;
	}

	public void setLn(int ln) {
		this.ln = ln;
	}

	public int getTln() {
		return tln;
	}

	public void setTln(int tln) {
		this.tln = tln;
	}

	public int getNomorPeraturan() {
		return nomorPeraturan;
	}

	public void setNomorPeraturan(int nomorPeraturan) {
		this.nomorPeraturan = nomorPeraturan;
	}

	public int getTahunPeraturan() {
		return tahunPeraturan;
	}

	public void setTahunPeraturan(int tahunPeraturan) {
		this.tahunPeraturan = tahunPeraturan;
	}

	public DetailUndang getDetailUndang() {
		return detailUndang;
	}

	public void setDetailUndang(DetailUndang detailUndang) {
		this.detailUndang = detailUndang;
	}

	public String getTentang() {
		return tentang;
	}

	public void setTentang(String tentang) {
		this.tentang = tentang;
	}

	public DetailUndang cariBaseByNo(int no, ArrayList<Peraturan> uu) {
		for (int i = 0; i < uu.size(); i++) {
			if (no == uu.get(i).getNomorPeraturan()) {
				uu.get(i).show();
				return uu.get(i).getDetailUndang();

			}
		}
		return null;
	}

	public DetailUndang cariBaseByTahun(int tahun, ArrayList<Peraturan> uu) {
		for (int i = 0; i < uu.size(); i++) {
			if (tahun == uu.get(i).getTahunPeraturan()) {
				uu.get(i).show();
				return uu.get(i).getDetailUndang();
			}
		}
		return null;
	}

	public DetailUndang cariBaseByTentang(String input, ArrayList<Peraturan> uu) {
		for (int i = 0; i < uu.size(); i++) {
			if (uu.get(i).getTentang().contains(input)) {
				uu.get(i).show();
				return uu.get(i).getDetailUndang();
			}
		}
		return null;
	}

	public void show() {
		System.out.println("Jenis              : " + getJenis());
		System.out.println("No Peraturan       : " + getNomorPeraturan());
		System.out.println("Tentang Peraturan  : " + getTentang());
		System.out.println("No LN              : " + getLn());
		System.out.println("No TLN             : " + getTln());
		System.out.println("Tanggal Ditetapkan : " + getTanggalDitetapkan());
		System.out.println("Tanggal Diudangkan : " + getTanggalDiundangkan());
		System.out.println("");
	}

}
