package undangundang.model;

import java.util.ArrayList;

public class Pasal {
	private int noPasal;
	private ArrayList<String> isi;
	
	public Pasal(int noPasal, ArrayList<String> isi) {
		this.noPasal = noPasal;
		this.isi = isi;
	}

	public int getNoPasal() {
		return noPasal;
	}

	public void setNoPasal(int noPasal) {
		this.noPasal = noPasal;
	}

	public ArrayList<String> getIsi() {
		return isi;
	}

	public void setIsi(ArrayList<String> isi) {
		this.isi = isi;
	}
	
	public void showPasal() {
		System.out.println("Pasal "+getNoPasal()+" :");
		for(int i=0;i<isi.size();i++) {
			System.out.println((i+1)+" "+isi.get(i));
		}
	}

}
