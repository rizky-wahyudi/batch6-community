package handsonday8.main;

import java.util.Scanner;
import java.util.HashMap;

public class Main {
	static double resistance = 0, current = 0, voltage = 0, power = 0;
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		while (true) {
			System.out.println("1.Resistance 3.Voltage");
			System.out.println("2.Current    4.Power");
			System.out.println();
			System.out.println("5.Hitung     6.Tampil");
			System.out.println("7.Reset");
			System.out.print("Pilih : ");
			int pilih = sc.nextInt();
			System.out.println();
			if (pilih == 1) {
				setNilai(pilih);
			} else if (pilih == 2) {
				setNilai(pilih);
			} else if (pilih == 3) {
				setNilai(pilih);
			} else if (pilih == 4) {
				setNilai(pilih);
			} else if (pilih == 5) {
				hitung(resistance, current, voltage, power);
			} else if (pilih == 6) {
				tampil();
			} else if (pilih == 7) {
				reset();
			} else {
				System.out.println("Keluar");
				System.out.println("Program Berakhir!!!");
				break;
			}
		}

	}

	public static void setNilai(int i) {
		switch (i) {
		case 1:
			System.out.print("Masukkan nilai Resistance : ");
			resistance = sc.nextDouble();
			break;
		case 2:
			System.out.print("Masukkan nilai Current : ");
			current = sc.nextDouble();
			break;
		case 3:
			System.out.print("Masukkan nilai Voltage : ");
			voltage = sc.nextDouble();
			break;
		case 4:
			System.out.print("Masukkan nilai Power : ");
			power = sc.nextDouble();
			break;
		default:
			break;

		}
	}

	public static void hitung(double r, double i, double v, double p) {
		double[] value = { r, i, v, p };
		HashMap<String, Double> value2 = new HashMap<String, Double>();
		int k = 0;
		int isi = 0;
		for (int j = 0; j < value.length; j++) {
			if (value[j] == 0) {
				k++;
			} else {
				isi++;
				if (j == 0) {
					value2.put("r", value[j]);
				} else if (j == 1) {
					value2.put("i", value[j]);
				} else if (j == 2) {
					value2.put("v", value[j]);
				} else if (j == 3) {
					value2.put("3", value[j]);
				}
			}
		}
		if (k >= 3 || isi == 4) {
			System.out.println("Inputan parameter harus 2 atau kurang dari 4 : ");
			System.out.println();
		} else {
			double r2 = 0, i2 = 0, v2 = 0, p2 = 0;
			if (value2.containsKey("r")) {// r ada
				r2 = value2.get("r");
				if (value2.containsKey("i") && value2.containsKey("v") == false && value2.containsKey("p") == false) {
					// i ada dan v tida dan p tidak

					i2 = value2.get("i");
					v2 = r2 * i2;
					p2 = Math.pow(i2, 2) * r2;

				} else if (value2.containsKey("v") && value2.containsKey("i") == false
						&& value2.containsKey("p") == false) {// v ada dan i tidak dan p tidak

					v2 = value2.get("v");
					i2 = v2 / r;
					p2 = Math.pow(v2, 2) / r2;

				} else if (value2.containsKey("p") && value2.containsKey("i") == false
						&& value2.containsKey("v") == false) {// p ada dan i tidak dan v tidak

					p2 = value2.get("p");
					i2 = Math.sqrt(p2 / r2);
					v2 = Math.sqrt(p2 * r2);
				}
			} else if (value2.containsKey("i")) {// i ada
				i2 = value2.get("i");
				if (value2.containsKey("r") && value2.containsKey("v") == false && value2.containsKey("p") == false) {
					// r ada dan v tidak dan p tidak

					r2 = value2.get("r");
					v2 = i2 * r2;
					p2 = Math.pow(i2, 2) * r2;

				} else if (value2.containsKey("v") && value2.containsKey("r") == false
						&& value2.containsKey("p") == false) {// v ada dan r tidak dan p tidak

					v2 = value2.get("v");
					r2 = v2 / i2;
					p2 = v2 * i2;

				} else if (value2.containsKey("p") && value2.containsKey("r") == false
						&& value2.containsKey("v") == false) {// p ada dan r tidak dan v tidak

					p2 = value2.get("p");
					r2 = p2 / Math.pow(i2, 2);
					v2 = p2 / i2;

				}
			} else if (value2.containsKey("v")) {// v ada
				v2 = value2.get("v");
				if (value2.containsKey("r") && value2.containsKey("i") == false && value2.containsKey("p") == false) {
					// r ada dan i tidak dan p tidak
					r2 = value2.get("r");
					i2 = v2 / r2;
					p2 = Math.pow(v2, 2) / r2;

				} else if (value2.containsKey("i") && value2.containsKey("r") == false
						&& value2.containsKey("p") == false) {// i ada dan r tidak dan p tidak

					i2 = value2.get("i");
					r2 = v2 / i2;
					p2 = v2 * i2;

				} else if (value2.containsKey("p") && value2.containsKey("r") == false
						&& value2.containsKey("i") == false) {// p ada dan r tidak dan i tidak
					p2 = value2.get("P");
					r2 = Math.pow(v2, 2) / p2;
					i2 = p2 / v2;
				}
			} else if (value2.containsKey("p")) {// p ada
				p2 = value2.get("p");
				if (value2.containsKey("r") && value2.containsKey("i") == false && value2.containsKey("v") == false) {
					// r ada dan i tidak dan v tidak
					r2 = value2.get("r");
					i2 = Math.sqrt(p2 / r2);
					v2 = Math.sqrt(p2 * r2);

				} else if (value2.containsKey("i") && value2.containsKey("r") == false
						&& value2.containsKey("v") == false) {// i ada dan r tidak dan v tidak
					i2 = value2.get("i");
					r2 = p2 / Math.pow(i2, 2);
					v2 = p2 / i2;

				} else if (value2.containsKey("v") && value2.containsKey("r") == false
						&& value2.containsKey("i") == false) {// v ada dan r tidak dan i tidak
					v2 = value2.get("v");
					r2 = Math.pow(v2, 2) / p2;
					i2 = p2 / v2;
				}
			}

			resistance = r2;
			current = i2;
			voltage = v2;
			power = p2;
			tampil();

		}
	}

	public static void reset() {
		resistance = 0;
		current = 0;
		voltage = 0;
		power = 0;
		System.out.println("Semua Nilai berhasil di riset");
		System.out.println();
	}

	public static void tampil() {
		System.out.println("Resistance : " + resistance);
		System.out.println("Current : " + current);
		System.out.println("Voltage : " + voltage);
		System.out.println("Power : " + power);
		System.out.println();
	}
}