package airport.bean;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import ariport.model.Airplane;
import ariport.model.Laguage;
import ariport.model.Passenger;
import ariport.model.Passport;
import ariport.model.Ticket;

public class Main {

	public static void main(String[] args) {
		Airplane plane = new Airplane("Garuda", "Malaysia", "30/07/2021 12:00");
		Airplane plane2 = new Airplane("Air Asia", "Singapura", "28/07/2021 12:00");
		ArrayList<Airplane> planes = new ArrayList<>();
		planes.add(plane);
		planes.add(plane2);

		Passenger p1 = new Passenger("William Wallean");
		Passenger p2 = new Passenger("Robertus Bintoro");

		Ticket t1 = new Ticket(p1.getName(), "30/07/2021 12:00", "Garuda", "Malaysia");
		Ticket t2 = new Ticket(p2.getName(), "28/07/2021 12:00", "Air Asia", "Singapura");

		Passport pass1 = new Passport(p1.getName(), "12/08/2023");
		Passport pass2 = new Passport(p2.getName(), "28/07/2020");

		Laguage l1;
		l1 = new Laguage();
		l1.setWeight(20);
		ArrayList<String> isi = new ArrayList<>();
		isi.add("baju");
		//isi.add("Gunting");
		isi.add("sepatu");
		l1.setContain(isi);

		p1.setTicket(t1);
		p1.setPassport(pass1);
		p1.setLaguage(l1);

		l1 = new Laguage();
		l1.setWeight(30);
		ArrayList<String> isi2 = new ArrayList<>();
		isi.add("Handuk");
		isi.add("baju");
		isi.add("Celana");
		l1.setContain(isi2);

		p2.setTicket(t2);
		p2.setPassport(pass2);
		p2.setLaguage(l1);
		
		tampil(p1);
		securityOne(p1,planes);
		
		tampil(p2);
		securityOne(p2,planes);
		

	}

	public static void tampil(Passenger p) {
		System.out.println("Penumpang:");
		System.out.println("Nama : "+p.getName());
		System.out.println("Passport: ");
		System.out.println("Nama Passport :"+p.getPassport().getName());
		System.out.println("Expired Date :"+p.getPassport().getExpiredDate());
		System.out.println("Ticket:");
		System.out.println("Pesawat : "+p.getTicket().getAirplane());
		System.out.println("Tanggal :"+p.getTicket().getDate());
		System.out.println("Tujuan : "+p.getTicket().getDestination());
		System.out.println();
	}
	public static boolean securityOne(Passenger passenger, ArrayList<Airplane> planes) {
		System.out.println("Securtiy one:");
		System.out.println("X ray Laguage");

		ArrayList<String> contain = passenger.getLaguage().getContain();
		for (int i = 0; i < contain.size(); i++) {
			System.out.println((i + 1) + ". " + contain.get(i));
			if (contain.get(i).equalsIgnoreCase("pisau") || contain.get(i).equalsIgnoreCase("Gunting")
					|| contain.get(i).equalsIgnoreCase("Senjata")) {
				System.out.println("Bagasi bermasalah");
				return false;
			}
		}
		System.out.println("Bagasi Aman");
		if (checkName(passenger) && checkLaguage(passenger)) {
			System.out.println("Imigration:");
			if (checkTicket(passenger) && checkPassport(passenger)) {
				System.out.println("X ray 2:");
				if (checkHandLaguage(passenger) && checkPassport(passenger) && checkPlaneTicket(passenger, planes)) {
					return true;
				}
			}
		}
		System.out.println("Penerbangan penumpang "+passenger.getName()+" dibatalkan karena bermasalah");
		return false;

	}

	public static boolean checkTicket(Passenger passenger) {
		SimpleDateFormat sdFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm");
		Date current = new Date();
		System.out.println("------------------------------------------------------");
		System.out.println("Pengecekan Ticket");
		if (passenger.getTicket().getDate().compareTo(sdFormat.format(current)) < 0) {
			System.out.println("Tanggal pada Ticket :" + passenger.getTicket().getDate());
			System.out.println("Ticket sudah hangus");
			return false;
		} else {
			System.out.println("Tanggal pada Ticket :" + passenger.getTicket().getDate());
			System.out.println("Ticket masih berlaku");
			return true;
		}

	}

	public static boolean checkPassport(Passenger passenger) {
		SimpleDateFormat sdFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm");
		Date current = new Date();
		System.out.println("------------------------------------------------------");
		System.out.println("Pengecekan Passport");
		if (passenger.getPassport().getExpiredDate().compareTo(sdFormat.format(current)) > 0) {
			System.out.println("Masa berlaku passport :" + passenger.getPassport().getExpiredDate());
			System.out.println("Passport sudah tidak berlaku");
			return false;
		} else {
			System.out.println("Masa berlaku passport :" + passenger.getPassport().getExpiredDate());
			System.out.println("Passport masih berlaku");
			return true;
		}
	}

	public static boolean checkLaguage(Passenger passenger) {
		System.out.println("--------------------------------------------------------");
		System.out.println("Pengecekan Bagasi ");
		if (passenger.getLaguage().getWeight() <= 20) {
			System.out.println("Berat bagasi masih berkisar 20kg");
			return true;
		} else {
			System.out.println("Berat Bagasi melampau kapasitas");
			return false;

		}
	}

	public static boolean checkName(Passenger passenger) {
		System.out.println("--------------------------------");
		System.out.println("Pengecekan Nama penumpang");
		if (passenger.getName().equalsIgnoreCase(passenger.getTicket().getName())
				&& passenger.getName().equalsIgnoreCase(passenger.getPassport().getName())) {
			System.out.println("Nama sesuai dengan ticket dan passport");
			return true;
		} else {
			System.out.println("Nama tidak sesuai dengan ticket atau passport");
			return false;
		}
	}

	public static boolean checkHandLaguage(Passenger passenger) {
		System.out.println("-----------------------------------------------");
		System.out.println("Pengecekan barang bawaan penumpang");
		if (passenger.getHandBag() == null) {
			System.out.println("Tidak ada bagasi kabin");
			return true;
		} else {
			ArrayList<String> isi = passenger.getHandBag().getContain();
			for (int i = 0; i < isi.size(); i++) {
				System.out.println((i + 1) + ". " + isi.get(i));
				if (isi.get(i).equalsIgnoreCase("pisau") || isi.get(i).equalsIgnoreCase("Gunting")
						|| isi.get(i).equalsIgnoreCase("Senjata")) {
					System.out.println("Bagasi Kabin bermasalah");
					return false;
				}
			}
			if (passenger.getHandBag().getWeight() > 15) {
				System.out.println("Bagasi Kabin melebih kapasitas");
				return false;
			} else {
				System.out.println("Bagasi kabin aman");
				return true;
			}
		}
	}

	public static boolean checkPlaneTicket(Passenger passenger, ArrayList<Airplane> plane) {
		for (int i = 0; i < plane.size(); i++) {
			if (passenger.getTicket().getDate().equalsIgnoreCase(plane.get(i).getDate())
					&& passenger.getTicket().getAirplane().equalsIgnoreCase(plane.get(i).getPlaneName())
					&& passenger.getTicket().getDestination().equalsIgnoreCase(plane.get(i).getDestination())) {
				System.out.println("Penerbangan ada");
				System.out.println("Selamat berlibur");
				return true;
			}
		}
		System.out.println("Penerbangan tidak ada");
		System.out.println();
		return false;
	}

}
