package ariport.model;

import java.util.ArrayList;

public class Laguage {

	private int weight;
	private ArrayList<String> contain;

	public Laguage() {
		
	}
	
	public Laguage(int weight, ArrayList<String> contain) {
		this.weight = weight;
		this.contain = contain;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public ArrayList<String> getContain() {
		return contain;
	}

	public void setContain(ArrayList<String> contain) {
		this.contain = contain;
	}

}
