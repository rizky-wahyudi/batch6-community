package ariport.model;

public class Airplane {

	private String planeName;
	private String destination;
	private String date;

	public Airplane(String planeName, String destination, String date) {
		this.planeName = planeName;
		this.destination = destination;
		this.date = date;
	}

	public String getPlaneName() {
		return planeName;
	}

	public void setPlaneName(String planeName) {
		this.planeName = planeName;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
