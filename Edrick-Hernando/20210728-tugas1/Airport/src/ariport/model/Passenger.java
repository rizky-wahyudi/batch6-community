package ariport.model;

public class Passenger {
	private Passport passport;
	private Ticket ticket;
	private Laguage laguage;
	private String name;
	private HandBag handBag;
	
	public Passenger( String name) {
		this.name = name;
	}
	public Passport getPassport() {
		return passport;
	}
	public void setPassport(Passport passport) {
		this.passport = passport;
	}
	public Ticket getTicket() {
		return ticket;
	}
	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	public Laguage getLaguage() {
		return laguage;
	}
	public void setLaguage(Laguage laguage) {
		this.laguage = laguage;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public HandBag getHandBag() {
		return handBag;
	}
	public void setHandBag(HandBag handBag) {
		this.handBag = handBag;
	}
	
	
	
	
}
