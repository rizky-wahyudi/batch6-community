package ariport.model;

public class Ticket {

	private String name;
	private String date;
	private String airplane;
	private String destination;

	public Ticket(String name, String date, String airplane, String destination) {
		this.name = name;
		this.date = date;
		this.airplane = airplane;
		this.destination = destination;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAirplane() {
		return airplane;
	}

	public void setAirplane(String airplane) {
		this.airplane = airplane;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
