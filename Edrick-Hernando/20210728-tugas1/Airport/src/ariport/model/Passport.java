package ariport.model;

public class Passport {

	private String name;
	private String expiredDate;
	
	public Passport(String name, String expiredDate) {
		this.name = name;
		this.expiredDate = expiredDate;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

}
