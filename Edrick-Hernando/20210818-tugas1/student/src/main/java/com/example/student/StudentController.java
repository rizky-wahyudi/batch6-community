package com.example.student;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

	@Autowired
	private StudentRepository repo;

	// Home Page
	@GetMapping("/")
	public String welcome() {
		return "<html><body>" + "<h1>WELCOME</h1>" + "</body></html>";
	}

	// Get All Notes
	@GetMapping("/Student")
	public List<Student> getAllNotes() {
		return repo.findAll();
	}

	@PostMapping(value = "/student", consumes = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	public Student addStudent(@RequestBody Student student) {
		return repo.save(student);
	}
}
