package shoppingcart.controller;

public class Product {

	private String articleNumber;
	private String name;
	private String description;
	private double purchasePrice;
	private double sellPrice;
	private String image;
	private String expiredDate;
	private double tax;

	public String getArticleNumber() {
		return articleNumber;
	}

	public void setArticleNumber(String articleNumber) {
		this.articleNumber = articleNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice() {
		this.sellPrice = (getPurchasePrice()+(getPurchasePrice()*0.1));
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public void showProduct() {
		System.out.println("Product : " + getName());
		System.out.println("Article Number : " + getArticleNumber());
		System.out.println("Deskripsi : " + getDescription());
		System.out.println("Harga : " + getSellPrice());
	}

	public boolean checkEpxiredDateProduct(String current) {
		int compare=getExpiredDate().compareTo(current);
		if (compare< 0) {
			System.out.println(getName() + " sudah expired");
			System.out.println("Expired Date : " + getExpiredDate());
			return false;
		}
		return true;

	}

}
