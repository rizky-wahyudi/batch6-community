package shoppingcart.controller;

import java.util.ArrayList;

public class Order {
	private String orderDate;
	private String cashier;
	private String orderNumber;
	private String branchId;
	private double totalPrice;
	private ArrayList<OrderItem> orderItem;
	private int tax;
	private boolean paymentStatus;
	private String status;

	public Order() {
		this.paymentStatus = false;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getCashier() {
		return cashier;
	}

	public void setCashier(String cashier) {
		this.cashier = cashier;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public boolean isPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<OrderItem> getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(ArrayList<OrderItem> orderItem) {
		this.orderItem = orderItem;
	}

	public void showOrderItems() {
		if (getOrderItem() != null) {
			int hargaFinal = 0;
			int pajakFinal = 0;
			for (int i = 0; i < getOrderItem().size(); i++) {
				System.out.println((i + 1) + ". " + getOrderItem().get(i).getProduct().getName());
				System.out.println("Jumlah : " + getOrderItem().get(i).getAmount() + " Harga Satuan : "
						+ getOrderItem().get(i).getProduct().getSellPrice() + " Harga Total: "
						+ getOrderItem().get(i).getTotalPrice());
				int pajak = (int) (getOrderItem().get(i).getProduct().getTax() * getOrderItem().get(i).getTotalPrice());
				System.out.println("Pajak produk : " + pajak);
				System.out.println("Harga Final : " + (int) (pajak + getOrderItem().get(i).getTotalPrice()));
				hargaFinal = hargaFinal + (int) (pajak + getOrderItem().get(i).getTotalPrice());
				pajakFinal = pajakFinal + pajak;
				System.out.println();
			}
			setTotalPrice(hargaFinal);
			setTax(pajakFinal);
			System.out.println("Pajak Total Keseluruhan : " + getTax());
			System.out.println("Harga Total Keseluruhan : " + getTotalPrice());
		} else {
			System.out.println("Keranjang Kosong!!!");
			System.out.println();
		}
	}

	public void pay() {
		if (isPaymentStatus() == false) {
			setPaymentStatus(true);
			System.out.println("Pembayaran berhasil dilakukan");
			System.out.println("Terima Kasih!!!");
			System.out.println();
		}
	}

	public void cekOrderItems(String articleNumber) {
		if (getOrderItem() != null) {
			for (int i = 0; i < getOrderItem().size(); i++) {
				if (getOrderItem().get(i).getProduct().getArticleNumber() == articleNumber) {
					getOrderItem().remove(i);
					break;
				}
			}
		}
	}

}
