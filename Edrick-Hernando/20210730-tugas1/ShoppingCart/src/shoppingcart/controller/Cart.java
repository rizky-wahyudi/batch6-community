package shoppingcart.controller;

import java.util.ArrayList;

public class Cart {

	private ArrayList<Order> order;

	public ArrayList<Order> getOrder() {
		return order;
	}

	public void setOrder(ArrayList<Order> order) {
		this.order = order;
	}

	public void tampilHistory() {
		for (int i = 0; i < getOrder().size(); i++) {
			if (getOrder().get(i).isPaymentStatus() == true) {
				System.out.println((i + 1) + ". Order Number :" + getOrder().get(i).getOrderNumber() + "| Order Date : "
						+ getOrder().get(i).getOrderDate() + "| Price : " + getOrder().get(i).getTotalPrice());
			}
		}
	}
	public void tampilDetailHistory(int index) {
		System.out.println("Order Number : "+getOrder().get(index).getOrderNumber()+ "| Order Date : "+getOrder().get(index).getOrderDate());
		System.out.println("Cashier : "+getOrder().get(index).getCashier());
		System.out.println("Item : ");
		getOrder().get(index).showOrderItems();
		System.out.println();
	}

}
