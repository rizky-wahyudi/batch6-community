package shoppingcart.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

import shoppingcart.controller.*;

public class Main {
	static Scanner sc = new Scanner(System.in);
	static ArrayList<Product> products = new ArrayList<>();
	static SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
	static ArrayList<OrderItem> orderItems = new ArrayList<>();
	static ArrayList<Order> orders = new ArrayList<>();
	static Order order = new Order();
	static Cart keranjang = new Cart();

	public static void main(String[] args) {
		start();
		go();

	}

	public static void start() {

		Product p1 = new Product();
		p1.setArticleNumber("A0001");
		p1.setName("Indomie");
		p1.setDescription("Mie Instan yang dapat dimasak dengan cepat dalam 3 menit saja");
		p1.setPurchasePrice(2000);
		p1.setSellPrice();
		p1.setImage("imageIndomie");
		p1.setExpiredDate("2022-08-20");
		p1.setTax(0.05);

		products.add(p1);

		p1 = new Product();
		p1.setArticleNumber("B0001");
		p1.setName("Lifeboy Cair");
		p1.setDescription("Sabun cair yang dapat membersihkan tubuh dari kuman");
		p1.setPurchasePrice(20000);
		p1.setSellPrice();
		p1.setImage("imageSabun");
		p1.setExpiredDate("2024-08-17");
		p1.setTax(0.07);

		products.add(p1);

		p1 = new Product();
		p1.setArticleNumber("C0001");
		p1.setName("Susu Milo");
		p1.setDescription("Susu dengan rasa cokelat yang menyehatkan tubuh");
		p1.setPurchasePrice(8000);
		p1.setSellPrice();
		p1.setImage("imageSusu");
		p1.setExpiredDate("2022-04-05");
		p1.setTax(0.1);

		products.add(p1);
	}

	public static void go() {

		while (true) {
			System.out.println("Shopping Cart");
			System.out.println("1. Cek Keranjang");
			System.out.println("2. History Pesananan");
			System.out.println("3. Belanja");
			System.out.println("4. Keluar");
			System.out.print("Pilih: ");
			int choice = sc.nextInt();

			if (choice == 1) {
				showOrderItemList();
				bayarKeranjang();
			} else if (choice == 2) {
				historyPesanan();
			} else if (choice == 3) {
				showProductList();
				choose();
			} else {
				System.out.println("Terima Kasih!!!");
				break;
			}
		}
	}

	public static void showProductList() {
		for (int i = 0; i < products.size(); i++) {
			System.out.println((i + 1) + " Product :");
			products.get(i).showProduct();
			System.out.println();
		}

	}

	public static void choose() {
		Date date = new Date();
		System.out.print("Pilih nomor produk : ");
		int choose = sc.nextInt();
		if (choose <= products.size()) {
			choose = choose - 1;
			boolean check = products.get(choose).checkEpxiredDateProduct(sd.format(date));
			if (check == true) {
				System.out.println("Produk : " + products.get(choose).getName());
				int jumlah = 1;
				while (true) {
					try {
						System.out.print("Masukkan Jumlah : ");
						jumlah = sc.nextInt();
						if (jumlah < 1) {
							System.out.println("Jumlah minimal 1");
							continue;
						}
						break;
					} catch (InputMismatchException e) {
						System.out.println("Masukkan harus angka");
						break;
					}
				}
				OrderItem oi = new OrderItem();
				order.cekOrderItems(products.get(choose).getArticleNumber());
				oi.setAmount(orderItems.size() + 1);
				oi.setProduct(products.get(choose));
				oi.setAmount(jumlah);
				oi.setTotalPrice(jumlah * products.get(choose).getSellPrice());
				orderItems.add(oi);
				order.setOrderItem(orderItems);
				order.setStatus("ordered");
				System.out.println("Produk " + products.get(choose).getName() + " berhasil ditambahkan ke keranjang");
				System.out.println();
			} else {
				System.out.println("Silahkan pilih produk lain Terima Kasih!");
				System.out.println();
			}
		} else {
			System.out.println("Batal Belanja !!!");
		}

	}

	public static void showOrderItemList() {
		order.showOrderItems();

	}

	public static void bayarKeranjang() {
		if (order.getOrderItem() != null) {
			while (true) {
				System.out.println("1. Yes");
				System.out.println("2. No");
				System.out.println("Lakukan Pembayaran : ");

				try {
					int choice = sc.nextInt();
					if (choice == 1) {
						order.pay();
						Date now = new Date();
						order.setOrderDate(sd.format(now));
						order.setCashier("Kasa-123");
						if (keranjang.getOrder() == null) {
							order.setOrderNumber("Order-" + String.valueOf(1));
						} else {
							order.setOrderNumber("Order-" + String.valueOf(keranjang.getOrder().size() + 1));
						}
						order.setStatus("Delievered");
						orders.add(order);
						keranjang.setOrder(orders);
						order = new Order();
						break;
					} else if (choice == 2) {
						break;
					} else {
						System.out.println("Harap masukan angka 1 atau 2");
					}
				} catch (InputMismatchException e) {
					e.printStackTrace();
					System.out.println("Harap masukkan angka");
					break;
				}
			}
		}
	}

	public static void historyPesanan() {
		if (keranjang.getOrder() != null) {
			keranjang.tampilHistory();
			System.out.print("Masukkan History yang ingin dilihat : ");
			int choice = sc.nextInt();
			System.out.println();
			if (choice <= keranjang.getOrder().size() && choice > 0) {
				keranjang.tampilDetailHistory(choice - 1);
			} else {
				System.out.println("Batal Cek History");
			}
		} else {
			System.out.println("Tidak ada history belanja");
		}
	}
}
