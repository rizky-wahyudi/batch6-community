CREATE TABLE handsonqueryday12.address(
id INT NOT NULL AUTO_INCREMENT,
street VARCHAR(50),
district VARCHAR(50),
city VARCHAR(50),
province VARCHAR(50),
country VARCHAR(50),
PRIMARY KEY(id));

CREATE TABLE handsonqueryday12.customer(
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(50),
phone VARCHAR(18),
email VARCHAR(50),
idNumber VARCHAR(16),
idImages TEXT,
PRIMARY KEY(id));



CREATE TABLE handsonqueryday12.images(
id INT NOT NULL AUTO_INCREMENT,
imageUrl TEXT,
status VARCHAR(7),
PRIMARY KEY(id));

CREATE TABLE handsonqueryday12.brand(
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(50),
logoUrl TExT,
PRIMARY KEY(id));

CREATE TABLE handsonqueryday12.category(
id INT NOT NULL AUTO_INCREMENT,
type VARCHAR(10),
PRIMARY KEY (id));

CREATE TABLE handsonqueryday12.car(
id INT NOT NULL AUTO_INCREMENT,
imagesId INT NOT NULL,
brandId INT NOT NULL,
categoryId INT NOT NULL,
engineSize INT,
fuelType VARCHAR(10),
currentLocation INT NOT NULL,
gearType VARCHAR(10),
PRIMARY KEY(id),
FOREIGN KEY(imagesId) REFERENCES images(id),
FOREIGN KEY(brandId) REFERENCES brand(id),
FOREIGN KEY(categoryId) REFERENCES category(id),
FOREIGN KEY(currentLocation) REFERENCES address(id)
);
CREATE TABLE handsonqueryday12.service(
id INT NOT NULL AUTO_INCREMENT,
insurance VARCHAR(1),
weekdayPrice INT,
weekendPrice INT,
holidayPrice INT,
carCategoryId INT NOT NULL,
carGearTyep VARCHAR(8),
PRIMARY KEY (id),
FOREIGN KEY (carCategoryId) REFERENCES category(id));

CREATE TABLE handsonqueryday12.booking(
id INT NOT NULL AUTO_INCREMENT,
customerId INT NOT NULL,
carId INT NOT NULL,
serviceId INT NOT NULL,
pickupLocation VARCHAR(50),
dropOffLocation VARCHAR(50),
pickupDate DATE,
dropOffDate DATE,
totalPrice INT,
PRIMARY KEY(id),
FOREIGN KEY(customerId) REFERENCES customer(id),
FOREIGN KEY(carId) REFERENCES car(id),
FOREIGN KEY(serviceId) REFERENCES service(id));

