package Liburan.KotaFactory;

import java.util.ArrayList;

public class Liburan {
	private int jumlahOrang;
	private int jumlahHari;
	private int biaya;
	private ArrayList<Integer> destinasi;
	private int biayaDestinasi;
	
	public int getBiayaDestinasi() {
		return biayaDestinasi;
	}

	public void setBiayaDestinasi(int biayaDestinasi) {
		this.biayaDestinasi = biayaDestinasi;
	}

	public ArrayList<Integer> getDestinasi() {
		return destinasi;
	}

	public void setDestinasi(ArrayList<Integer> destinasi) {
		this.destinasi = destinasi;
	}

	public int getJumlahOrang() {
		return jumlahOrang;
	}

	public void setJumlahOrang(int jumlahOrang) {
		this.jumlahOrang = jumlahOrang;
	}

	public int getJumlahHari() {
		return jumlahHari;
	}

	public void setJumlahHari(int jumlahHari) {
		this.jumlahHari = jumlahHari;
	}

	public int getBiaya() {
		return biaya;
	}

	public void setBiaya(int biaya) {
		this.biaya = biaya;
	}

	public void tampilTujuan() {
		System.out.println();
	}
	
	public int getTicketPrice() {
		return 0;
	}

	public int Destination(int choice) {
		return 0;
	}
	
	
}
