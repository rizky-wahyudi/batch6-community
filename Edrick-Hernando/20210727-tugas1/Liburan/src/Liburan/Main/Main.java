package Liburan.Main;

import java.util.ArrayList;
import java.util.Scanner;

import Liburan.KotaFactory.*;
import Liburan.KotaFactory.LiburanFactory;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Jumlah Orang yang liburan : ");
		int jumlah = sc.nextInt();
		System.out.print("Lama hari liburan : ");
		int hari = sc.nextInt();

		Liburan destinasi = new Liburan();
		System.out.println("Tujuan Liburan(Tokyo ($500),New York ($700), Bandung($50)) :");
		String tujuan = sc.next();
		if (tujuan.equalsIgnoreCase("Tokyo")) {
			destinasi = LiburanFactory.newInstance("Tokyo");
		} else if (tujuan.equalsIgnoreCase("New York")) {
			destinasi = LiburanFactory.newInstance("NewYork");
		} else if (tujuan.equalsIgnoreCase("Bandung")) {
			destinasi = LiburanFactory.newInstance("Bandung");
		} else {
			System.out.println("Tujuan tidak diketahui");

		}
		destinasi.setJumlahOrang(jumlah);
		destinasi.setJumlahHari(hari);

		System.out.print("Jumlah Destinasi Wisata Liburan :");
		int jumlah2 = sc.nextInt();
		
		ArrayList<Integer>opsi=new ArrayList<>();
		
		if (destinasi instanceof Tokyo) {
			destinasi.setBiaya(500);
			Tokyo tokyo;
			for (int i = 0; i < jumlah2; i++) {
				((Tokyo) destinasi).tampilTujuan();
				System.out.print(" Pilih Destinasi "+i+1+" : " );
				int op=sc.nextInt();
				opsi.add(op);
				destinasi.setDestinasi(opsi);
				tokyo=new Tokyo();
				destinasi.setBiaya(destinasi.getBiaya()+tokyo.Destination(op));
			}
			
		} else if (destinasi instanceof NewYork) {
			destinasi.setBiaya(700);
			NewYork nY;
			for (int i = 0; i < jumlah2; i++) {
				((NewYork) destinasi).tampilTujuan();
				System.out.print(" Pilih Destinasi "+i+1+" : " );
				int op=sc.nextInt();
				opsi.add(op);
				destinasi.setDestinasi(opsi);
				nY=new NewYork();
				destinasi.setBiaya(destinasi.getBiaya()+nY.Destination(op));
			}
			
		} else if (destinasi instanceof Bandung) {
			destinasi.setBiaya(50);
			Bandung bD;
			for (int i = 0; i < jumlah2; i++) {
				((Bandung) destinasi).tampilTujuan();
				System.out.print(" Pilih Destinasi "+i+1+" : " );
				int op=sc.nextInt();
				opsi.add(op);
				destinasi.setDestinasi(opsi);
				bD=new Bandung();
				destinasi.setBiaya(destinasi.getBiaya()+bD.Destination(op));
			}
			
		}
		destinasi.setBiaya(destinasi.getBiaya()*jumlah*hari);
		
		System.out.println("Biaya yang dikeluarkan: $" +destinasi.getBiaya());
		
		
	}

}
