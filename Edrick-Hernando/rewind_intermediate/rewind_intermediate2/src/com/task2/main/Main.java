package com.task2.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		for (int i = 0; i < 1000; i++) {
			
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					for (int index = 0; index < 4; index++) {
						String path = "E:\\Nexsoft\\BootCamp\\batch6-community\\Edrick-Hernando\\rewind_intermediate\\test"
								+ (index + 1) + ".txt";
						File file = new File(path);
						Scanner reader;
						try {
							reader = new Scanner(file);
							while (reader.hasNextLine()) {
								System.out.println(reader.nextLine());
								System.out.println();
							}
							reader.close();
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}
			});	
		}
		executorService.shutdown();
	}

}
