package id.co.calculator.test;


import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import id.co.calculator.model.Calculator;

class CalculatorTest {

	@Test
	@DisplayName("Test add Method")
	void testAdd() {
		int res=Calculator.add(1, 2);
		assertTrue(3==res);
		assertFalse(4==res);
		assertFalse(3!=res);
		assertThrows(NumberFormatException.class,()->{Calculator.add(0,Integer.parseInt("one"));});
		assertEquals(3, res);
	}

	@Test
	@DisplayName("Test substract Method")
	void testSubstract() {
		int res=Calculator.subtract(1, 2);
		assertTrue(-1==res);
		assertFalse(-1!=res);
		assertFalse(res>1);
		assertThrows(NumberFormatException.class,()->{Calculator.subtract(0,Integer.parseInt("two"));});
		assertEquals(-1,res);
		
	}

	@Test
	@DisplayName("Test multiply Method")
	void testMultiply() {
		int res=Calculator.multiply(2, 1);
		assertTrue(2==res);
		assertFalse(2!=res);
		assertFalse(res<2);
		assertThrows(NumberFormatException.class,()->{Calculator.multiply(0,Integer.parseInt("trhee"));});
		assertEquals(2,res);
	}

	@Test
	@DisplayName("Test divide Method")
	void testDivide() {
		int res=Calculator.divide(2, 1);
		assertTrue(2==res);
		assertFalse(2!=res);
		assertFalse(res>2);
		assertThrows(ArithmeticException.class,()->{Calculator.divide(1, 0);});
		assertEquals(2,res);
	}

}
