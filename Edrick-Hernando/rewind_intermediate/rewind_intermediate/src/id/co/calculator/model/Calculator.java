package id.co.calculator.model;

public class Calculator {

	public static int add(int numOne, int numTwo) {
		int result = numOne + numTwo;
		return result;
	}

	public static int subtract(int numOne, int numTwo) {
		int result = numOne - numTwo;
		return result;
	}

	public static int multiply(int numOne, int numTwo) {
		int result = numOne * numTwo;
		return result;
	}

	public static int divide(int numOne, int numTwo) {
		int result = numOne / numTwo;
		return result;
	}

	public static void main(String[] args) {
		int numOne = Integer.parseInt(args[0]);
		int numTwo = Integer.parseInt(args[1]);
		add(numOne, numTwo);
		subtract(numOne, numTwo);
		multiply(numOne, numTwo);
		divide(numOne, numTwo);

	}

}
