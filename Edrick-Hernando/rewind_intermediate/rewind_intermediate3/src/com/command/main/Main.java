package com.command.main;

import com.command.model.Command;
import com.command.model.Controller;
import com.command.model.Credit;
import com.command.model.Payment;
import com.command.model.Tax;

public class Main {

	public static void main(String[] args) {
		Controller controller = new Controller();
        Payment payment = new Payment();

        Command tax = new Tax(payment);
        Command credit = new Credit(payment);

        controller.setCommand(tax);
        controller.executeCommand();

        controller.setCommand(credit);
        controller.executeCommand();
	}

}
