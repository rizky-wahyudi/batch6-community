package com.command.model;

public interface Command {
	void execute();
}
