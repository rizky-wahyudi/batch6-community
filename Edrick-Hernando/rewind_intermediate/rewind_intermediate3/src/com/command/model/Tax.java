package com.command.model;

public class Tax implements Command {

	private Payment pay;

	public Payment getPay() {
		return pay;
	}

	public void setPay(Payment pay) {
		this.pay = pay;
	}

	public Tax(Payment pay) {
		this.pay = pay;
	}

	@Override
	public void execute() {
		System.out.println("Pay the Tax");

	}

}
