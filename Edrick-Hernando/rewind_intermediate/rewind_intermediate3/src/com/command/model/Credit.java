package com.command.model;

public class Credit implements Command {

	private Payment pay;

	public Credit(Payment pay) {
		this.pay = pay;
	}

	public Payment getPay() {
		return pay;
	}

	public void setPay(Payment pay) {
		this.pay = pay;
	}

	@Override
	public void execute() {
		System.out.println("Pay the Credit Card");

	}

}
