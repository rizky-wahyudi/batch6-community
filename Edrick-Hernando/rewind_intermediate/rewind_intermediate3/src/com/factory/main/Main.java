package com.factory.main;

import com.factory.model.Employee;
import com.factory.model.EmployeeStaticFactory;

public class Main {

	public static void main(String[] args) {
		EmployeeStaticFactory p1=new EmployeeStaticFactory();
		Employee emp1=p1.newInstance("Employee");
		emp1.show();
		Employee emp2=p1.newInstance("Treasure");
		emp2.show();
		Employee emp3=p1.newInstance("Secretary");
		emp3.show();
	}
	

}
