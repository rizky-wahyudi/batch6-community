package com.factory.model;

public class Employee {
	private int nip;
	private String name;
	private double salary;

	public int getNip() {
		return nip;
	}

	public void setNip(int nip) {
		this.nip = nip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public void show() {
		System.out.println("Saya Karyawan");
	}
	
}
