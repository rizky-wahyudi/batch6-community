package com.factory.model;

public class EmployeeStaticFactory {
	public static Employee newInstance(String type) {
		if (type.equals("Employee")) {
			return new Employee();
		} else if (type.equals("Treasure")) {
			return new Treasure();
		} else if (type.equals("Secretary")) {
			return new Secretary();
		}
		return null;
	}
}
