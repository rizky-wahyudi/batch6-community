package com.decorator.model;

public class OffRoadBike extends BikeDecorator {

	public OffRoadBike(Bike bike) {
		super(bike);

	}
	
	public void assemble() {
		super.assemble();
		System.out.println("Modify road for off road and bike suspension");
	}

	
}
