package com.decorator.model;

public class BikeDecorator implements Bike {

	protected Bike bike;
	
	public BikeDecorator (Bike bike) {
		this.bike=bike;
	}
	
	@Override
	public void assemble() {
		this.bike.assemble();
		
	}

}
