package com.decorator.model;

public class RacingBike extends BikeDecorator {

	
	public RacingBike(Bike bike) {
		super(bike);
	}
	
	@Override
	public void assemble() {
		super.assemble();
		System.out.println("Modify speed and add several feauture for racing");
	}
}
