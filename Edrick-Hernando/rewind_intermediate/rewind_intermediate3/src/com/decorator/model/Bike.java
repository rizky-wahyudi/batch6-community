package com.decorator.model;

public interface Bike {

	public void assemble();
}
