package com.decorator.main;

import com.decorator.model.*;

public class Main {

	public static void main(String[] args) {
		Bike RacingBike = new RacingBike(new BikeBase());
		RacingBike.assemble();
		System.out.println();
		
		Bike OffRoadBike=new OffRoadBike(new BikeBase());
		OffRoadBike.assemble();
		System.out.println();
	}
}

