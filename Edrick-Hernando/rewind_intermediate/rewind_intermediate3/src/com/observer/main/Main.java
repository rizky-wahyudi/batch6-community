package com.observer.main;

import com.observer.model.Library;
import com.observer.model.Visitor;

public class Main {

	public static void main(String[] args) {

		Library lib = new Library();
		Visitor v1 = new Visitor(lib);
		Visitor v2 = new Visitor(lib);
		
		lib.addObserver(v1);
		lib.addObserver(v2);
		
		lib.releaseNewBook("Theory Darwin");
		lib.releaseNewBook("Java for Dummy");
		lib.releaseNewBook("Jungle Book");
	}

}
