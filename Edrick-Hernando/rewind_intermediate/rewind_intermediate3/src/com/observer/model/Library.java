package com.observer.model;

import java.util.ArrayList;

public class Library implements Observable {

	 private ArrayList<Observer> observers = new ArrayList<>();

	    @Override
	    public void addObserver(Observer observer) {
	        observers.add(observer);
	    } 

	    @Override
	    public void removeObserver(Observer observer) {
	        observers.remove(observer);
	    }

	    public void releaseNewBook(String book) {
	        System.out.println("Release new Book : " + book);
	        notifyObserver();
	    }

	    @Override
	    public void notifyObserver() {
	        for(Observer observer: observers) {
	            observer.update();
	        }
	    }

}
