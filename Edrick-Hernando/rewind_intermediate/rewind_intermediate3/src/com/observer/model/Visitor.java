package com.observer.model;

public class Visitor implements Observer {

	private Observable observable;

	public Visitor(Observable observable) {
		this.observable = observable;
	}

	public Observable getObservable() {
		return observable;
	}

	public void setObservable(Observable observable) {
		this.observable = observable;
	}

	@Override
	public void update() {
		System.out.println("New Book!!");
	}

}
