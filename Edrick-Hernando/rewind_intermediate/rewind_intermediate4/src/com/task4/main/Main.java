package com.task4.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Main {

	public static void main(String[] args) {
		URL link;
		try {
			for (int i = 0; i < 5; i++) {
				link = new URL("https://jsonplaceholder.typicode.com/todos/" + (i + 1));
				URLConnection uc = link.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
				String inputLine;
				String path = "E:\\Nexsoft\\BootCamp\\batch6-community\\Edrick-Hernando\\rewind_intermediate\\"
						+ (i + 1) + ".json";
				File file = new File(path);
				FileWriter isi;
				boolean res;
				res = file.createNewFile();
				if (res) {
					isi = new FileWriter(path);
					while ((inputLine = in.readLine()) != null) {
						System.out.println(inputLine);
						isi.write(in.readLine());
					}
					isi.close();
				}
				in.close();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} 

	}

}
