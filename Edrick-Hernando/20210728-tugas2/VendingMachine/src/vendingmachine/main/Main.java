package vendingmachine.main;

import java.util.Scanner;

import vendingmachine.controller.*;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		Isi[][] isi = new Isi[6][10];
		Makanan makan;
		Minuman minum;
		makan = new Makanan("Kitkat", 5000, "00");
		isi[0][0] = makan;
		makan = new Makanan("KitKat Matcha", 5000, "01");
		isi[0][1] = makan;
		makan = new Makanan("KitKat Strawbery", 5000, "02");
		isi[0][2] = makan;
		makan = new Makanan("KitKat White Choco", 10000, "03");
		isi[0][3] = makan;
		makan = new Makanan("KitKat Jumbo", 10000, "04");
		isi[0][4] = makan;
		makan = new Makanan("Kitkat", 5000, "05");
		isi[0][5] = makan;
		makan = new Makanan("KitKat Matcha", 5000, "06");
		isi[0][6] = makan;
		makan = new Makanan("KitKat Strawbery", 5000, "07");
		isi[0][7] = makan;
		makan = new Makanan("KitKat White Choco", 10000, "08");
		isi[0][8] = makan;
		makan = new Makanan("KitKat Jumbo", 10000, "09");
		isi[0][9] = makan;
		makan = new Makanan("Chitato Cheese Flavor", 10000, "10");
		isi[1][0] = makan;
		makan = new Makanan("Chitato Cheese Flavor", 10000, "11");
		isi[1][1] = makan;
		makan = new Makanan("Chitato Cheese Flavor", 10000, "12");
		isi[1][2] = makan;
		makan = new Makanan("Chitato Beef Flavor", 10000, "13");
		isi[1][3] = makan;
		makan = new Makanan("Chitato Beef Flavor", 10000, "14");
		isi[1][4] = makan;
		makan = new Makanan("Chitato Beef Flavor", 10000, "15");
		isi[1][5] = makan;
		makan = new Makanan("Pringles Barbecue Flavor", 20000, "16");
		isi[1][6] = makan;
		makan = new Makanan("Pringles Original Flavor", 20000, "17");
		isi[1][7] = makan;
		makan = new Makanan("Pringles Cheese Flavor", 20000, "18");
		isi[1][8] = makan;
		makan = new Makanan("Pringles Cheese Flavor", 20000, "19");
		isi[1][9] = makan;
		makan = new Makanan("Good Times Choco Chips", 10000, "20");
		isi[2][0] = makan;
		makan = new Makanan("Good Times Choco Chips", 10000, "21");
		isi[2][1] = makan;
		makan = new Makanan("Good Times Choco Chips", 10000, "22");
		isi[2][2] = makan;
		makan = new Makanan("Good Times Choco Chips", 10000, "23");
		isi[2][3] = makan;
		makan = new Makanan("Good Times Choco Chips", 10000, "24");
		isi[2][4] = makan;
		makan = new Makanan("Oreo", 10000, "25");
		isi[2][5] = makan;
		makan = new Makanan("Oreo", 10000, "26");
		isi[2][6] = makan;
		makan = new Makanan("Oreo", 20000, "27");
		isi[2][7] = makan;
		makan = new Makanan("Oreo", 20000, "28");
		isi[2][8] = makan;
		makan = new Makanan("Oreo", 20000, "29");
		isi[2][9] = makan;

		minum = new Minuman("Aqua", 5000, "30");
		isi[3][0] = minum;
		minum = new Minuman("Aqua", 5000, "31");
		isi[3][1] = minum;
		minum = new Minuman("Aqua", 5000, "32");
		isi[3][2] = minum;
		minum = new Minuman("Aqua", 10000, "33");
		isi[3][3] = minum;
		minum = new Minuman("Aqua", 10000, "34");
		isi[3][4] = minum;
		minum = new Minuman("Vit", 5000, "35");
		isi[3][5] = minum;
		minum = new Minuman("Vit", 5000, "36");
		isi[3][6] = minum;
		minum = new Minuman("Vit", 5000, "37");
		isi[3][7] = minum;
		minum = new Minuman("Nestle Mineral", 10000, "38");
		isi[3][8] = minum;
		minum = new Minuman("Nestle Mineral", 10000, "39");
		isi[3][9] = minum;
		minum = new Minuman("Milo", 10000, "40");
		isi[4][0] = minum;
		minum = new Minuman("Milo", 10000, "41");
		isi[4][1] = minum;
		minum = new Minuman("Milo", 10000, "42");
		isi[4][2] = minum;
		minum = new Minuman("Milo", 10000, "43");
		isi[4][3] = minum;
		minum = new Minuman("Milo", 10000, "44");
		isi[4][4] = minum;
		minum = new Minuman("Milo", 10000, "45");
		isi[4][5] = minum;
		minum = new Minuman("Bear Brand", 20000, "46");
		isi[4][6] = minum;
		minum = new Minuman("Bear Brand", 20000, "47");
		isi[4][7] = minum;
		minum = new Minuman("Bear Brand", 20000, "48");
		isi[4][8] = minum;
		minum = new Minuman("Bear Brand", 20000, "49");
		isi[4][9] = minum;
		minum = new Minuman("Tebs", 10000, "50");
		isi[5][0] = minum;
		minum = new Minuman("Tebs", 10000, "51");
		isi[5][1] = minum;
		minum = new Minuman("Tebs", 10000, "52");
		isi[5][2] = minum;
		minum = new Minuman("Tebs", 10000, "53");
		isi[5][3] = minum;
		minum = new Minuman("Tebs", 10000, "54");
		isi[5][4] = minum;
		minum = new Minuman("Fruit Tea", 10000, "55");
		isi[5][5] = minum;
		minum = new Minuman("Fruit Tea", 10000, "56");
		isi[5][6] = minum;
		minum = new Minuman("Fruit Tea", 20000, "57");
		isi[5][7] = minum;
		minum = new Minuman("Fruit Tea", 20000, "58");
		isi[5][8] = minum;
		minum = new Minuman("Fruit Tea", 20000, "59");
		isi[5][9] = minum;

		VendingMachine vm = new VendingMachine();
		vm.setIsi(isi);
		boolean kondisi = true;
		
		do {
			boolean pembelian = false;
			System.out.println("Masukkan Nominal Uang anda :");
			System.out.println("1. 20000");
			System.out.println("2. 10000");
			System.out.println("3. 5000");
			System.out.print("Pilih : ");
			int uang = sc.nextInt();
			System.out.println("----------------------");
			if (uang == 1) {
				System.out.println("Jumlah Uang : 20000");
				uang = 20000;
			} else if (uang == 2) {
				System.out.println("Jumlah Uang : 10000");
				uang = 10000;
			} else if (uang == 3) {
				System.out.println("Jumlah Uang : 5000");
				uang = 5000;
			} else {
				System.out.println("Inputan salah");
				System.out.println("Pembelian gagal");
				continue;
			}
			tampil(vm);
			while (pembelian == false) {
				System.out.print("Pilih kode produk :");
				String pilih = sc.next();
				pembelian = vm.choice(pilih, uang);
			}
			System.out.print("Apakah ingin belanja lagi?(1.Y/2.N): ");
			int pilih2 = sc.nextInt();
			if (pilih2 == 2) {
				kondisi = false;
				System.out.println("Terima Kasih sudah berbelanja");
			}

		} while (kondisi);

	}

	public static void tampil(VendingMachine vending) {
		System.out.println("Vending Machine");
		System.out.println(
				"----------------------------------------------------------------------------------------------------------------");
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 10; j++) {
				if (vending.getStatus()[i][j]) {
					System.out.print("|" + vending.getIsi()[i][j].getNama() + " kode: "
							+ vending.getIsi()[i][j].getKode() + " price: " + vending.getIsi()[i][j].getHarga());
				} else {
					System.out.print("| Barang Habis!!!!! ");
				}
			}
			System.out.println();
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");
		}

	}

}
