package vendingmachine.controller;

public class Isi {
	private String nama;
	private int harga;
	private String kode;
	
	public Isi(String nama, int harga, String kode) {
		this.nama = nama;
		this.harga = harga;
		this.kode = kode;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}
}
