package vendingmachine.controller;

public class VendingMachine {

	private Isi[][] isi;
	private int[][] jumlahUang;
	private boolean[][] status;

	public VendingMachine() {
		this.jumlahUang = new int[3][2];
		jumlahUang[0][0] = 5000;
		jumlahUang[1][0] = 10000;
		jumlahUang[2][0] = 20000;

		jumlahUang[0][1] = 10;
		jumlahUang[1][1] = 10;
		jumlahUang[2][1] = 10;
		this.status = new boolean[6][10];

		setStatusAwal(status);
	}

	public boolean[][] getStatus() {
		return status;
	}

	public void setStatus(boolean[][] status) {
		this.status = status;
	}

	public void setStatusAwal(boolean[][] status) {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 10; j++) {
				status[i][j] = true;
			}
		}
	}

	public Isi[][] getIsi() {
		return isi;
	}

	public void setIsi(Isi[][] isi) {
		this.isi = isi;
	}

	public boolean choice(String masuk, int uang) {

		int nilai = Integer.parseInt(masuk);
		if (nilai < 1 || nilai > 60) {
			System.out.println("Inputan kode invalid");
			return false;
		}
		char first = masuk.charAt(0);
		char second = masuk.charAt(1);

		int i = Character.getNumericValue(first);
		int j = Character.getNumericValue(second);

		if (uang >= getIsi()[i][j].getHarga()) {
			if (getStatus()[i][j]) {
				if (checkKembalian(uang, getIsi()[i][j].getHarga(), jumlahUang)) {
					status[i][j] = false;
					System.out.println(getIsi()[i][j].getNama() + " berhasil dibeli");
					System.out.println();
					setStatus(status);
					return true;
				} else {
					return false;
				}
			} else {

				System.out.println("Barang tidak ada");
				return false;
			}

		} else {
			System.out.println("Uang tidak mencukupi");
			return false;

		}

	}

	public boolean checkKembalian(int uang, int harga, int[][] jumlahUang) {

		int kembalian = uang - harga;
		int sisaUang = 0;
		if (kembalian == 0) {
			return true;
		} else {
			sisaUang = hitungKembalian(kembalian, jumlahUang);
			if (sisaUang != 0) {
				return true;
			}
		}
		System.out.println();
		return false;

	}

	public int hitungKembalian(int kembalian, int[][] jumlahUang) {
		int n = 0;
		int x = 0;
		int y = 0;
		int z = 0;
		while (n < kembalian) {
			if (kembalian >= jumlahUang[0][0] && kembalian <= jumlahUang[1][0] && jumlahUang[0][1] > 0) {
				n = n + jumlahUang[0][0];
				jumlahUang[0][1] = jumlahUang[0][1] - 1;
				x++;
			} else if (kembalian >= jumlahUang[1][0] && kembalian <=jumlahUang[2][0] && jumlahUang[1][1] > 0) {
				n = n + jumlahUang[1][0];
				jumlahUang[1][1] = jumlahUang[1][1] - 1;
				y++;
			} else if (kembalian >= jumlahUang[2][0] && jumlahUang[2][1] > 0) {
				n = n + jumlahUang[2][0];
				jumlahUang[2][1] = jumlahUang[2][1] - 1;
				z++;
			} else {
				System.out.println("Tidak ada Kembalian");
				System.out.println("Produk gagal dibeli!!!");
				System.out.println();
				return 0;
			}
		}

		System.out.println("kembalian sebesar : " + n);
		System.out.println("Dalama pecaahan : " + jumlahUang[0][0] + " x " + x);
		System.out.println("Dalama pecaahan : " + jumlahUang[1][0] + " x " + y);
		System.out.println("Dalama pecaahan : " + jumlahUang[2][0] + " x " + z);
		return 1;
	}

}
