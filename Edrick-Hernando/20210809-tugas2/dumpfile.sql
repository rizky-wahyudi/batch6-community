-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: handsonquery12_2
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `street` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `province` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'Gambir','Jakarta','DKI Jakarta'),(2,'Serpong','Tangerang','Banten'),(3,'Paingan','Sleman','DI Yogyakarta'),(4,'Sudirman','Bogor','Jawa Barat');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bagasi`
--

DROP TABLE IF EXISTS `bagasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bagasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idPelanggan` int NOT NULL,
  `idTiket` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idPelanggan` (`idPelanggan`),
  KEY `idTiket` (`idTiket`),
  CONSTRAINT `bagasi_ibfk_1` FOREIGN KEY (`idPelanggan`) REFERENCES `pelanggan` (`id`),
  CONSTRAINT `bagasi_ibfk_2` FOREIGN KEY (`idTiket`) REFERENCES `tiket` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bagasi`
--

LOCK TABLES `bagasi` WRITE;
/*!40000 ALTER TABLE `bagasi` DISABLE KEYS */;
INSERT INTO `bagasi` VALUES (1,1,1),(2,1,2);
/*!40000 ALTER TABLE `bagasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dokumen`
--

DROP TABLE IF EXISTS `dokumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dokumen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idTransportasi` int NOT NULL,
  `platNomor` varchar(8) DEFAULT NULL,
  `validDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idTransportasi` (`idTransportasi`),
  KEY `platNomor` (`platNomor`),
  CONSTRAINT `dokumen_ibfk_1` FOREIGN KEY (`idTransportasi`) REFERENCES `transportasi` (`id`),
  CONSTRAINT `dokumen_ibfk_2` FOREIGN KEY (`platNomor`) REFERENCES `transportasi` (`platNomor`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dokumen`
--

LOCK TABLES `dokumen` WRITE;
/*!40000 ALTER TABLE `dokumen` DISABLE KEYS */;
INSERT INTO `dokumen` VALUES (1,1,'B 888 E','2022-08-09'),(2,2,'AB 87 BE','2022-09-09');
/*!40000 ALTER TABLE `dokumen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `isibagasi`
--

DROP TABLE IF EXISTS `isibagasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `isibagasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idBagasi` int NOT NULL,
  `namaBarang` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idBagasi` (`idBagasi`),
  CONSTRAINT `isibagasi_ibfk_1` FOREIGN KEY (`idBagasi`) REFERENCES `bagasi` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `isibagasi`
--

LOCK TABLES `isibagasi` WRITE;
/*!40000 ALTER TABLE `isibagasi` DISABLE KEYS */;
INSERT INTO `isibagasi` VALUES (1,1,'Koper'),(2,1,'Tas Selempang'),(3,2,'Koper'),(4,2,'Laptop');
/*!40000 ALTER TABLE `isibagasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelanggan`
--

DROP TABLE IF EXISTS `pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pelanggan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelanggan`
--

LOCK TABLES `pelanggan` WRITE;
/*!40000 ALTER TABLE `pelanggan` DISABLE KEYS */;
INSERT INTO `pelanggan` VALUES (1,'Edrick','Hernando','1998-12-02','M'),(2,'Robertus','Bintoro','1997-02-05','M');
/*!40000 ALTER TABLE `pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket`
--

DROP TABLE IF EXISTS `tiket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tiket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idPelanggan` int NOT NULL,
  `departureDate` date NOT NULL,
  `arrivalDate` date NOT NULL,
  `arrivalTime` time DEFAULT NULL,
  `departureTime` time DEFAULT NULL,
  `idTransportasi` int NOT NULL,
  `from` int NOT NULL,
  `to` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idPelanggan_idx` (`idPelanggan`),
  KEY `idTransportasi_idx` (`idTransportasi`),
  KEY `from_idx` (`from`),
  KEY `to_idx` (`to`),
  CONSTRAINT `from` FOREIGN KEY (`from`) REFERENCES `address` (`id`),
  CONSTRAINT `idPelanggan` FOREIGN KEY (`idPelanggan`) REFERENCES `pelanggan` (`id`),
  CONSTRAINT `idTransportasi` FOREIGN KEY (`idTransportasi`) REFERENCES `transportasi` (`id`),
  CONSTRAINT `to` FOREIGN KEY (`to`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket`
--

LOCK TABLES `tiket` WRITE;
/*!40000 ALTER TABLE `tiket` DISABLE KEYS */;
INSERT INTO `tiket` VALUES (1,1,'2022-08-09','2022-08-10','10:00:00','20:00:00',1,1,3),(2,1,'2022-08-13','2022-08-14','12:00:00','22:00:00',1,3,1);
/*!40000 ALTER TABLE `tiket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transportasi`
--

DROP TABLE IF EXISTS `transportasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transportasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `platNomor` varchar(8) NOT NULL,
  `capacity` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PlatNomor_UNIQUE` (`platNomor`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transportasi`
--

LOCK TABLES `transportasi` WRITE;
/*!40000 ALTER TABLE `transportasi` DISABLE KEYS */;
INSERT INTO `transportasi` VALUES (1,'B 888 E',8),(2,'AB 87 BE',6);
/*!40000 ALTER TABLE `transportasi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-09 18:33:07
