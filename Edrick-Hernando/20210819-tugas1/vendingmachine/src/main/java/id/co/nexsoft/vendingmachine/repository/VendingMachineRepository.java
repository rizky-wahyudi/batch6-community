package id.co.nexsoft.vendingmachine.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.vendingmachine.model.VendingMachine;

public interface VendingMachineRepository extends CrudRepository<VendingMachine, Integer> {
	VendingMachine findById(int id);

	List<VendingMachine> findAll();

	void deleteById(int id);

	VendingMachine save(VendingMachine vendingMachine);

	VendingMachine findByKodeAndPosition(String kode,int position);
}
