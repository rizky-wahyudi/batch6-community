package id.co.nexsoft.vendingmachine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class VendingMachine {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String kode;
	private int position;

	private int jumlah;
	private String nama;
	private int price;

	public VendingMachine() {
	}

	public VendingMachine(String kode, int position, int jumlah, String nama, int price) {
		this.kode = kode;
		this.position = position;
		this.jumlah = jumlah;
		this.nama = nama;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getJumlah() {
		return jumlah;
	}

	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
