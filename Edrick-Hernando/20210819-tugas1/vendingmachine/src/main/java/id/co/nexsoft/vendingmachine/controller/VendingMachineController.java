package id.co.nexsoft.vendingmachine.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.vendingmachine.model.VendingMachine;
import id.co.nexsoft.vendingmachine.repository.VendingMachineRepository;

@RestController
@RequestMapping("/vendingMachine")
public class VendingMachineController {

	@Autowired
	private VendingMachineRepository repo;

	// Home Page
	@GetMapping("/")
	public String welcome() {
		
		
		return "<html><body>" + "<h1>WELCOME TO VENDING MACHINE</h1>" + "</body></html>";
	}

	// Get All Notes
	@GetMapping("/showall")
	public List<VendingMachine> getAllItems() {
		return repo.findAll();
	}

	@PostMapping("/add")
	@ResponseStatus(HttpStatus.CREATED)
	public VendingMachine addItem(@RequestBody VendingMachine vendingmachine) {
		return repo.save(vendingmachine);
	}

	@GetMapping("/getItem/{kode}")
	public String getItem(@PathVariable String kode) {
		String baris = kode.substring(0, 1);
		int kolom = Integer.parseInt(kode.substring(1));
		String isi;
		VendingMachine vending = repo.findByKodeAndPosition(baris, kolom);
		if (vending.getJumlah() > 0) {
			updateItem( vending.getId());
			isi = "<html><body>" + "<h1>" + vending.getNama() + " dari rak " + vending.getKode() + ""
					+ vending.getPosition() + " seharga " + vending.getPrice() + " berhasil dibeli</h1>"
					+ "</body></html>";
		} else {
			isi = "<html><body>" + "<h1> Produk dirak " + vending.getKode() + "" + vending.getPosition()
					+ " sudah habis</h1>" + "</body></html>";

		}
		return isi;
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateItem(@PathVariable int id) {
		Optional<VendingMachine> vendingmachineRepo = Optional.ofNullable(repo.findById(id));

		if (!vendingmachineRepo.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		vendingmachineRepo.get().setJumlah(vendingmachineRepo.get().getJumlah() - 1);
		VendingMachine vendingmachine = vendingmachineRepo.get();
		repo.save(vendingmachine);
		return ResponseEntity.noContent().build();
	}

	@PutMapping("/restock/{id}")
	public String restockItem(@PathVariable int id) {
		Optional<VendingMachine> vendingmachineRepo = Optional.ofNullable(repo.findById(id));

		if (!vendingmachineRepo.isPresent()) {
			return "<html><body><h1>Produk Tidak ada</h1></body></html>";
		}
		vendingmachineRepo.get().setJumlah(10);
		VendingMachine vendingmachine = vendingmachineRepo.get();
		repo.save(vendingmachine);
		return "<html><body><h1>Produk " + vendingmachine.getNama() + " pada rak " + vendingmachine.getKode() + ""
				+ vendingmachine.getPosition() + " berhasil di Restock menjadi 10 buah</h1></body></html>";
	}

	@DeleteMapping("/deleteItem")
	public void deleteVendingMachine(@PathVariable(value = "id") int id) {
		repo.deleteById(id);
	}
	
	

}
