package id.co.nexsoft.controller;

import id.co.nexsoft.service.DistanceMapperService;

public class DistanceMapperController {

	public void setFirstLocation(int id, int awal, int akhir, int jarak) {
		DistanceMapperService dm = new DistanceMapperService();
		dm.setItemFirstLocation(id, awal, akhir, jarak);
	}

	public void setNextLocation(int awal, int idBarang) {
		DistanceMapperService dm = new DistanceMapperService();
		dm.setNextLocation(awal, idBarang);
	}

	public int getCurrentLocation(int idBarang) {
		DistanceMapperService dm = new DistanceMapperService();
		return dm.getCurrentLocation(idBarang);
	}
}
