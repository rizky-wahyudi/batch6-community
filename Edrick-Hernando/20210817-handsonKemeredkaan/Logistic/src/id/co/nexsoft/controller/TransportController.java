package id.co.nexsoft.controller;

import id.co.nexsoft.service.TransportService;

public class TransportController {

	public void setTransport(int id, int idKendaraan, int idCurrent) {
		TransportService ts = new TransportService();
		ts.setTransport(id, idKendaraan, idCurrent);
	}
	
	public int getTranpsortId(double weight,int idCurrent) {
		TransportService ts=new TransportService();
		return ts.getIdKendaraanByWeight(weight, idCurrent);
	}
	
	public int getCurrentTransport(int idBarang) {
		TransportService ts=new TransportService();
		return ts.getTranpsort(idBarang);
	}
	
	public void setNextTransport(int idBarang) {
		TransportService ts=new TransportService();
		ts.setNextTransport(idBarang);
	}
}
