package id.co.nexsoft.controller;

import id.co.nexsoft.model.Product;
import id.co.nexsoft.service.ProductService;

public class ProductController {

	public int addItem(Product product) {
		ProductService ps = new ProductService();
		int id=ps.addItem(product.getName(), product.getDeliverAddress(), product.getWidth(), product.getDepth(),
				product.getHeight(), product.getWeight());
		return id;
	}
	
	public int getItem(String name) {
		ProductService ps=new ProductService();
		return ps.getItem(name);
	}
}
