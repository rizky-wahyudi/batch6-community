package id.co.nexsoft.main;

import java.util.Scanner;

import id.co.nexsoft.controller.AddressController;
import id.co.nexsoft.controller.DistanceMapperController;
import id.co.nexsoft.controller.ProductController;
import id.co.nexsoft.controller.TransportController;
import id.co.nexsoft.model.Product;

public class Main {
	static Scanner sc = new Scanner(System.in);
	static int idBarang = 0;

	public static void main(String[] args) {
		while (true) {
			System.out.println("1.Kirim Barang");
			System.out.println("2.Cek Posisi");
			int pilih = sc.nextInt();
			if (pilih == 1) {
				inputBarang();
			} else if (pilih == 2) {

				lanjutPosisi();
			} else {
				System.out.println("Program Berakhir!!!");
				break;
			}
		}

	}

	public static void inputBarang() {
		AddressController ac = new AddressController();
		ac.showDistrict();
		System.out.print("Pilih Distrik saat ini :");
		int idDistrik = sc.nextInt();
		System.out.println("Memasukkan barang yang ingin dikirim");
		System.out.print("Jenis : ");
		String name = sc.next();
		ac.showDistrict();
		System.out.print("Distrik Tujuan : ");
		String district = sc.next();
		System.out.print("Width : ");
		double width = sc.nextDouble();
		System.out.print("Depth : ");
		double depth = sc.nextDouble();
		System.out.print("Height : ");
		double height = sc.nextDouble();
		System.out.print("Weight : ");
		double weight = sc.nextDouble();

		ProductController pc = new ProductController();
		Product product = new Product(name, district, width, depth, height, weight);
		int idTujuan = pc.addItem(product);
		idBarang = pc.getItem(name);
		int jarak = cekJarak(idDistrik, idTujuan);
		System.out.println("Total Jarak : "+jarak);
		setBarangFirst(idBarang, idDistrik, idTujuan, jarak);
		TransportController tc = new TransportController();
		tc.setTransport(idBarang, tc.getTranpsortId(weight, idDistrik), idDistrik);
	}

	public static void setBarangFirst(int idBarang, int awal, int akhir, int jarak) {
		DistanceMapperController dm = new DistanceMapperController();
		dm.setFirstLocation(idBarang, awal, akhir, jarak);
	}

	public static int cekJarak(int awal, int akhir) {
		int jarak = 0;
		switch (awal) {
		case 1:
			switch (akhir) {
			case 2:
				jarak = 380;
				break;
			case 3:
				jarak = 1689;
				break;
			case 4:
				jarak = 1710;
				break;
			default:
				jarak = 0;
				break;
			}
			break;
		case 2:
			switch (akhir) {
			case 1:
				jarak = 300;
				break;
			case 3:
				jarak = 1309;
				break;
			case 4:
				jarak = 1330;
				break;
			default:
				jarak = 0;
				break;
			}
			break;
		case 3:
			switch (akhir) {
			case 1:
				jarak = 1689;
				break;
			case 2:
				jarak = 1309;
				break;
			case 4:
				jarak = 21;
				break;
			default:
				jarak = 0;
				break;
			}
			break;
		case 4:
			switch (akhir) {
			case 1:
				jarak = 1710;
				break;
			case 2:
				jarak = 330;
				break;
			case 3:
				jarak = 21;
				break;
			default:
				jarak = 0;
				break;
			}
			break;
		default:
			jarak = 0;
			break;
		}
		return jarak;
	}

	public static int cekCurrent(int idBarang) {
		System.out.println("Posisi");
		DistanceMapperController dc = new DistanceMapperController();
		return dc.getCurrentLocation(idBarang);
	}

	public static void lanjutPosisi() {
		int total=0;
		while (true) {
			int id = cekCurrent(idBarang);
			System.out.println("1.Lanjut");
			System.out.println("2.Stop");
			System.out.print("Pilih :");
			int pilih = sc.nextInt();
			if (pilih == 1) {
				TransportController tc=new TransportController();
				total=total+tc.getCurrentTransport(idBarang);
				tc.setNextTransport(idBarang);
				DistanceMapperController dm = new DistanceMapperController();
				dm.setNextLocation(id, idBarang);
				
				
			} else {
				System.out.println("Durasi pengiriman : "+total);
				System.out.println();
				break;
			}
		}
	}
}
