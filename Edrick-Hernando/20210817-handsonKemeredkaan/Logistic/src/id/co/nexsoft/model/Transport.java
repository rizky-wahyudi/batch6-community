package id.co.nexsoft.model;

public class Transport {

	private int id;
	private int driverId;
	private double size;
	private double speed;
	private String type;
	private double load;

	public Transport() {

	}

	public Transport(int id, int driverId, double size, double speed, String type, double load) {
		this.id = id;
		this.driverId = driverId;
		this.size = size;
		this.speed = speed;
		this.type = type;
		this.load = load;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getLoad() {
		return load;
	}

	public void setLoad(double load) {
		this.load = load;
	}

}
