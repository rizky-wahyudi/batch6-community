package id.co.nexsoft.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import id.co.nexsoft.controller.DataBaseConnection;

public class AddressService {

	public void showDistrict() {
		DataBaseConnection db = new DataBaseConnection();
		String query = "SELECT id, district FROM address";
		Statement st;
		try {
			st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				System.out.println(rs.getInt("id") + ". " + rs.getString("district"));
			}
			rs.close();
			st.close();
			db.getConnection().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
