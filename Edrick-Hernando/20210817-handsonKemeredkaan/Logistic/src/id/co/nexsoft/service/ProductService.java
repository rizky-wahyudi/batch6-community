package id.co.nexsoft.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import id.co.nexsoft.controller.DataBaseConnection;

public class ProductService {

	public int addItem(String name, String address, double width, double depth, double height, double weight) {

		DataBaseConnection db = new DataBaseConnection();
		String query0 = "SELECT id FROM address WHERE district='" + address + "'";
		Statement st;
		int id = 0;
		try {
			db.getConnection().setAutoCommit(false);
			st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query0);

			while (rs.next()) {
				id = rs.getInt(1);
			}
			rs.close();
			st.close();

			String query1 = "INSERT INTO product(name,deliverAddressId,width,depth,height,weight) VALUES('" + name
					+ "'," + id + "," + width + "," + depth + "," + height + "," + weight + ")";

			st = db.getConnection().createStatement();
			st.executeUpdate(query1);
			db.getConnection().commit();
			System.out.println("Product yang ingin dikirim berhasil ditambahkan ke database!!!");
			st.close();
			db.getConnection().close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;

	}

	public int getItem(String name) {

		String query = "SELECT id FROM product where name='"+name+"'";
		DataBaseConnection db = new DataBaseConnection();
		Statement st;
		int id = 0;
		try {
			st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				id = rs.getInt("id");
			}
			rs.close();
			st.close();
			db.getConnection().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return id;
	}
}
