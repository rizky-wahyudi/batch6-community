package id.co.nexsoft.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import id.co.nexsoft.controller.DataBaseConnection;

public class WarehouseService {

	public int getWarehouseId(int id) {
		String query = "SELECT id FROM warehouse WHERE addressId=" + id + "";
		DataBaseConnection db = new DataBaseConnection();
		int warehouseId = 0;
		try {
			Statement st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				warehouseId = rs.getInt(id);
			}
			rs.close();
			st.close();
			db.getConnection().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return warehouseId;
	}

}
