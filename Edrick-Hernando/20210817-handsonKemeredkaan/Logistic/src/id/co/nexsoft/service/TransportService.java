package id.co.nexsoft.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import id.co.nexsoft.controller.DataBaseConnection;

public class TransportService {

	public void setTransport(int idBarang, int idKendaraan, int sekarang) {
		DataBaseConnection db = new DataBaseConnection();
		String query = "INSERT INTO travel(productId,transportId,currentPositionId) values(" + idBarang + ","
				+ idKendaraan + "," + sekarang + ")";
		try {
			db.getConnection().setAutoCommit(false);
			Statement st = db.getConnection().createStatement();
			st.executeUpdate(query);
			db.getConnection().commit();
			st.close();
			db.getConnection().close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getIdKendaraanByWeight(double weight, int current) {
		DataBaseConnection db = new DataBaseConnection();
		String query = "SELECT id FROM transport where size>=" + weight + "";
		int idKendaraan = 0;
		try {
			Statement st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				idKendaraan = rs.getInt("id");
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return idKendaraan;

	}

	public int getTranpsort(int idBarang) {
		DataBaseConnection db = new DataBaseConnection();
		String query = "SELECT * from travel where productId=" + idBarang + "";
		int total = 0;
		int transportId = 0;
		try {
			db.getConnection().setAutoCommit(false);
			Statement st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				System.out.println("Posisi Saat ini ada pada kendaraan " + rs.getInt("transportId") + " di posisi "
						+ rs.getInt("currentPositionId"));
				transportId = rs.getInt("transportId");
			}
			rs.close();
			st.close();
			st = db.getConnection().createStatement();
			query = "SELECT speed FROM transport WHERE id=" + transportId + "";
			rs = st.executeQuery(query);
			while (rs.next()) {
				total = rs.getInt("speed");
			}
			db.getConnection().close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return total;
	}

	public void setNextTransport(int idBarang) {
		DataBaseConnection db = new DataBaseConnection();
		int id = 0;
		int current = 0;
		String query1 = "SELECT id, currentPositionId FROM travel WHERE productId=" + idBarang + "";

		try {

			Statement st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query1);
			while (rs.next()) {
				id = rs.getInt("id");
				current = rs.getInt("currentPositionId");
			}
			rs.close();
			st.close();
			String query2 = "SELECT toWarehouseId FROM distancemapper WHERE productId=" + idBarang + "";
			st = db.getConnection().createStatement();
			rs = st.executeQuery(query2);
			int cek = 0;
			while (rs.next()) {
				cek = rs.getInt("toWarehouseId");
			}
			if (cek != current) {
				db.getConnection().setAutoCommit(false);
				current++;
				String query3 = "UPDATE travel set currentPositionId=" + current + " WHERE id=" + id + "";
				st = db.getConnection().createStatement();
				st.executeUpdate(query3);
				db.getConnection().commit();
				st.close();
			}
			db.getConnection().close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
