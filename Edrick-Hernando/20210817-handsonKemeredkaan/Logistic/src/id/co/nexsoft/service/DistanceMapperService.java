package id.co.nexsoft.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import id.co.nexsoft.controller.DataBaseConnection;

public class DistanceMapperService {

	public void setItemFirstLocation(int idBarang, int idAwal, int idTujuan, int jarak) {
		DataBaseConnection db = new DataBaseConnection();
		String query = "INSERT INTO distancemapper (productId,fromWarehouseId,toWarehouseId,distance,currentWarehouseId) values("
				+ idBarang + "," + idAwal + "," + idTujuan + "," + jarak + "," + idAwal + ")";

		try {
			db.getConnection().setAutoCommit(false);
			Statement st = db.getConnection().createStatement();
			st.executeUpdate(query);
			db.getConnection().commit();
			st.close();
			db.getConnection().close();
			System.out.println("Barang berhasil dimasukkan kedalam Warehouse");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setNextLocation(int current, int idBarang) {
		DataBaseConnection db = new DataBaseConnection();
		String query1 = "SELECT currentWarehouseId,toWarehouseId from distancemapper WHERE productId=" + idBarang + "";

		try {
			db.getConnection().setAutoCommit(false);
			Statement st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query1);
			int currentId = 0;
			int currentWareId = 0;
			while (rs.next()) {
				currentId = rs.getInt("currentWarehouseId");
				currentWareId = rs.getInt("toWarehouseId");
			}
			rs.close();
			st.close();
			if (currentId != currentWareId) {
				currentId++;
				String query2 = "UPDATE distancemapper set currentWarehouseId=" + currentId + " WHERE productId="
						+ idBarang + "";
				st = db.getConnection().createStatement();
				st.executeUpdate(query2);
				db.getConnection().commit();
				st.close();
			} else {
				System.out.println("Barang sudah sampai tujuan");
			}
			db.getConnection().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int getCurrentLocation(int idBarang) {
		DataBaseConnection db = new DataBaseConnection();
		String query = "SELECT * FROM distancemapper WHERE productId=" + idBarang + "";
		int id = 0;
		try {
			Statement st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				System.out.println("Posisi Barang " + rs.getInt("productId") + " ada di warehouse "
						+ rs.getInt("currentWareHouseId"));
				System.out.println();
				id = rs.getInt("id");
			}
			rs.close();
			st.close();
			db.getConnection().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
}
