
public class Airplane {

	protected String plane;
	protected int seats;
	protected int laguage;
	protected int price;
	protected int route;
	private Airplane [] planeClass;

	public void setPrice(int price) {
		this.price = price;
	}

	Airplane(String plane) {
		this.plane=plane;
		
	}

	Airplane( int seats, int laguage) {
		
		this.seats = seats;
		this.laguage = laguage;
	}

	public String getPlane() {
		return plane;
	}

	public void setPlane(String plane) {
		this.plane = plane;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public int getLaguage() {
		return laguage;
	}

	public void setLaguage(int laguage) {
		this.laguage = laguage;
	}

	public int getPrice(int route) {
		return price;
	}

	public int getRoute() {
		return route;
	}

	public void setRoute(int route) {
		this.route = route;
	}
	
	public Airplane[] getPlaneClass() {
		return planeClass;
	}
	public void setPlaneClass(Airplane [] planeClass) {
		this.planeClass=planeClass;
	}
	
}
