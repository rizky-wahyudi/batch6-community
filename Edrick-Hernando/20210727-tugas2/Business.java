

public class Business extends Airplane {

	Business( int seats, int laguage) {
		super( seats*25/100, laguage);
		
	}

	@Override
	public int getPrice(int route) {
		if (route == 1) {
			double res=500*2.25;
			return (int)res;
		} else if (route == 2) {
			double res=600*2.25;
			return (int)res;
		} else {
			double res=650*2.25;
			return (int)res;
		}
	}
	
	@Override
	public int getLaguage() {
		return laguage*2;
	}
}
