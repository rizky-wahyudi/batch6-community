

public class Economy extends Airplane {

	Economy( int seats, int laguage) {
		super( seats * 65 / 100, laguage);

	}

	@Override
	public int getPrice(int route) {
		if (route == 1) {
			return 500;
		} else if (route == 2) {
			return 600;
		} else {
			return 650;
		}
	}
	
	@Override
	public int getLaguage() {
		return laguage;
	}
}
