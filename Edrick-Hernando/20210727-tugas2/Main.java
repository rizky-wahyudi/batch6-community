

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		ArrayList<Airplane> airplanes = new ArrayList<>();

		Airplane ab = new Airplane("Airbus 380");
		Airplane ab1 = new Economy(750, 20);
		Airplane ab2 = new Business(750, 20);
		Airplane ab3 = new FirstClass(750, 20);
		Airplane[] ap = new Airplane[3];
		ap[0] = ab1;
		ap[1] = ab2;
		ap[2] = ab3;
		ab.setPlaneClass(ap);
		ab.setRoute(1);
		airplanes.add(ab);

		ab = new Airplane("Boeing 747");
		ab1 = new Economy(529, 20);
		ab2 = new Business(529, 20);
		ab3 = new FirstClass(529, 20);
		ap = new Airplane[3];
		ap[0] = ab1;
		ap[1] = ab2;
		ap[2] = ab3;
		ab.setPlaneClass(ap);
		ab.setRoute(2);
		airplanes.add(ab);

		ab = new Airplane("Boeing 784");
		ab1 = new Economy(248, 20);
		ab2 = new Business(248, 20);
		ab3 = new FirstClass(248, 20);
		ap = new Airplane[3];
		ap[0] = ab1;
		ap[1] = ab2;
		ap[2] = ab3;
		ab.setPlaneClass(ap);
		ab.setRoute(3);
		airplanes.add(ab);

		for (int i = 0; i < airplanes.size(); i++) {
			int price = 0;
			System.out.println(" Pesawat : " + airplanes.get(i).getPlane());
			switch (airplanes.get(i).getRoute()) {
			case 1:
				System.out.println("Europe-Asia");
				break;
			case 2:
				System.out.println("Asia-Europe");
				break;
			case 3:
				System.out.println("Europe-US");
				break;
			}
			System.out.println(" Jumlah Kursi Economy : " + airplanes.get(i).getPlaneClass()[0].getSeats()
					+ " Harga Tiket : $" + airplanes.get(i).getPlaneClass()[0].getPrice(airplanes.get(i).getRoute())
					+ " Total : $" + +airplanes.get(i).getPlaneClass()[0].getSeats()
							* airplanes.get(i).getPlaneClass()[0].getPrice(airplanes.get(i).getRoute()));
			price = price + (airplanes.get(i).getPlaneClass()[0].getSeats()
					* airplanes.get(i).getPlaneClass()[0].getPrice(airplanes.get(i).getRoute()));

			System.out.println(" Jumlah Kursi Business : " + airplanes.get(i).getPlaneClass()[1].getSeats()
					+ " Harga Tiket :$" + airplanes.get(i).getPlaneClass()[1].getPrice(airplanes.get(i).getRoute())
					+ " Total : $" + +airplanes.get(i).getPlaneClass()[1].getSeats()
							* airplanes.get(i).getPlaneClass()[1].getPrice(airplanes.get(i).getRoute()));
			price = price + (airplanes.get(i).getPlaneClass()[0].getSeats()
					* airplanes.get(i).getPlaneClass()[0].getPrice(airplanes.get(i).getRoute()));

			System.out.println(" Jumlah Kursi FirstClass : " + airplanes.get(i).getPlaneClass()[2].getSeats()
					+ "Harga Tiket : $" + airplanes.get(i).getPlaneClass()[2].getPrice(airplanes.get(i).getRoute())
					+ " Total : $ " + +airplanes.get(i).getPlaneClass()[2].getSeats()
							* airplanes.get(i).getPlaneClass()[2].getPrice(airplanes.get(i).getRoute()));
			price = price + (airplanes.get(i).getPlaneClass()[0].getSeats()
					* airplanes.get(i).getPlaneClass()[0].getPrice(airplanes.get(i).getRoute()));

			System.out.println("Total : $ " + price);
			System.out.println(
					"----------------------------------------------------------------------------------------------------------");
		}
	}

}
