

public class FirstClass extends Airplane {

	FirstClass( int seats, int laguage) {
		super(seats*10/100, laguage);
	
	}

	@Override
	public int getPrice(int route) {
		if (route == 1) {
			double res=500*3;
			return (int)res;
		} else if (route == 2) {
			double res=600*3;
			return (int)res;
		} else {
			double res=650*3;
			return (int)res;
		}
	}
	@Override
	public int getLaguage() {
		return laguage*3;
	}
}
