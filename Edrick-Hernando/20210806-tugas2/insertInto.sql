INSERT INTO handsonquery.address(street,langitute,latitude,city,district,province,country)
values ("JL Paingan", -7.754882,110.422897,"Depok","Sleman","DI Yogyakarta","Indonesia");
INSERT INTO handsonquery.address(street,langitute,latitude,city,district,province,country)
values ("JL Affandi", -7.774302,110.389393,"Depok","Sleman","DI Yogyakarta","Indonesia");

INSERT INTO handsonquery.distributor(nama, addressId,addressType)
values("Wahyu",1,"Domisili");
INSERT INTO handsonquery.distributor(nama, addressId,addressType)
values("Roni",2,"Domisili");

INSERT INTO handsonquery.productDistributor(distributorId,buyPrice,stock)
values(1,3000,10);
INSERT INTO handsonquery.productDistributor(distributorId,buyPrice,stock)
values(2,3000,5);
INSERT INTO handsonquery.productDistributor(distributorId,buyPrice,stock)
values(1,6000,10);
INSERT INTO handsonquery.productDistributor(distributorId,buyPrice,stock)
values(2,6000,10);
INSERT INTO handsonquery.productDistributor(distributorId,buyPrice,stock)
values(1,3000,5);
INSERT INTO handsonquery.productDistributor(distributorId,buyPrice,stock)
values(1,8000,5);
INSERT INTO handsonquery.productDistributor(distributorId,buyPrice,stock)
values(2,19000,10);
INSERT INTO handsonquery.productDistributor(distributorId,buyPrice,stock)
values(2,9000,2);

INSERT INTO handsonquery.productImages(url,description)
values("https://images.tokopedia.net/img/cache/700/attachment/2020/8/15/-1/-1_b813c029-217d-4849-8ed3-053b63c3cb79.jpg","Mie Instant siap saji");
INSERT INTO handsonquery.productImages(url,description)
values("https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//85/MTA-10133355/milo_milo_sachet_22_gram_milo_activ_go_sachet_22_gram__full00.jpg","Susu bubuk Milo cepat sedu");
INSERT INTO handsonquery.productImages(url,description)
values("https://cf.shopee.co.id/file/03c4d872f48b78248c799e4037532d63","Susu UHT berkualitas dalam kaleng");
INSERT INTO handsonquery.productImages(url,description)
values("https://cf.shopee.co.id/file/06f603da0862a76a91c6265667c78036","Frozen food nugget rasa ayam");
INSERT INTO handsonquery.productImages(url,description)
values("https://www.unileverprofessional.com/uploads/products/276/0c0b5c419ea26af8_1587115600.png","Pasta pembersih gigi");

INSERT INTO handsonquery.product(artnumhber,productImageId,sellPrice,expiredDate)
values("Indomie",1,4000,'2022-08-08');
INSERT INTO handsonquery.product(artnumhber,productImageId,sellPrice,expiredDate)
values("Milo",2,7000,'2022-09-08');
INSERT INTO handsonquery.product(artnumhber,productImageId,sellPrice,expiredDate)
values("Bear Brand",3,10000,'2022-01-12');
INSERT INTO handsonquery.product(artnumhber,productImageId,sellPrice,expiredDate)
values("Fiesta Chicken Nugget",4,22000,'2022-08-08');
INSERT INTO handsonquery.product(artnumhber,productImageId,sellPrice,expiredDate)
values("Pepsodent",5,1200,'2022-03-08');

INSERT INTO productStock(productId,stock,productDistributorId,purchaseDate,deliveryDate)
values(1,10,1,'2020-08-19','2020-08-20');
INSERT INTO productStock(productId,stock,productDistributorId,purchaseDate,deliveryDate)
values(1,5,2,'2020-08-17','2020-08-21');
INSERT INTO productStock(productId,stock,productDistributorId,purchaseDate,deliveryDate)
values(2,10,3,'2020-08-17','2020-08-20');
INSERT INTO productStock(productId,stock,productDistributorId,purchaseDate,deliveryDate)
values(2,10,4,'2020-08-17','2020-08-20');
INSERT INTO productStock(productId,stock,productDistributorId,purchaseDate,deliveryDate)
values(3,5,5,'2020-08-17','2020-08-20');
INSERT INTO productStock(productId,stock,productDistributorId,purchaseDate,deliveryDate)
values(4,0,6,'2020-08-17','2020-08-20');
INSERT INTO productStock(productId,stock,productDistributorId,purchaseDate,deliveryDate)
values(4,0,7,'2020-08-17','2020-08-20');
INSERT INTO productStock(productId,stock,productDistributorId,purchaseDate,deliveryDate)
values(5,0,8,'2020-08-17','2020-08-20');



