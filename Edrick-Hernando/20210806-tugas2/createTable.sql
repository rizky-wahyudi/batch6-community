CREATE TABLE handsonquery.productimages(
id INT NOT NULL AUTO_INCREMENT,
url BLOB,
description TEXT,
PRIMARY KEY (id));

CREATE TABLE handsonquery.address(
id INT NOT NULL AUTO_INCREMENT,
street VARCHAR(50),
langitute decimal(10,8),
latitude decimal(11,8),
city VARCHAR(50),
district VARCHAR(50),
province VARCHAR(50),
country VARCHAR(50),
PRIMARY KEY (id));

CREATE TABLE handsonquery.distributor(
id INT NOT NULL AUTO_INCREMENT,
nama VARCHAR(255),
addressId INT NOT NULL,
addressType VARCHAR(50),
PRIMARY KEY(id),
FOREIGN KEY(addressId) REFERENCES address(id));

CREATE TABLE handsonquery.productdistributor(
id INT NOT NULL AUTO_INCREMENT,
distributorId INT NOT NULL,
buyPrice DECIMAL,
stock INT,
PRIMARY KEY(id),
FOREIGN KEY(distributorId) REFERENCES distributor(id));

CREATE TABLE handsonquery.product(
id INT NOT NULL AUTO_INCREMENT,
artnumhber VARCHAR(50),
productImageId INT NOT NULL,
sellPrice decimal(10,2),
expiredDate DATE,
PRIMARY KEY(id),
FOREIGN KEY(productImageId) REFERENCES productimages(id));

CREATE TABLE handsonquery.productstock(
id INT NOT NULL AUTO_INCREMENT,
productId INT NOT NULL,
stock INT,
productDistributorId INT NOT NULL,
purchaseDate DATE,
deliveryDate DATE,
PRIMARY KEY(id),
FOREIGN KEY(productDistributorId) REFERENCES productdistributor(id),
FOREIGN KEY(productId) REFERENCES product(id));