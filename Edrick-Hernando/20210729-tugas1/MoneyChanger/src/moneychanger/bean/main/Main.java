package moneychanger.bean.main;

import java.util.ArrayList;
import java.util.Scanner;

import moneychanger.bean.model.Change;
import moneychanger.bean.model.MoneyChanger;
import moneychanger.bean.model.Valuta;

public class Main {
	static Scanner sc = new Scanner(System.in);
	static MoneyChanger mc = new MoneyChanger();

	public static void main(String[] args) {
		start();
		pilih();
	}

	public static Valuta chooseValuta() {
		mc.showValuta(mc);
		System.out.print("Pilih Valuta saat ini (NUMBER): ");
		int choose = sc.nextInt();
		String code = mc.getValuta().get(choose - 1).getCode();
		System.out.println();
		System.out.println("Nilai TUKAR " + code + " ke Mata Uang lain: ");
		for (int i = 0; i < mc.getValuta().get(choose - 1).getCodeRates().size(); i++) {
			System.out.println((i + 1) + ". " + mc.getValuta().get(choose - 1).getCodeRates().get(i) + " Nilai Tukar "
					+ mc.getValuta().get(choose - 1).getRates().get(i));
		}
		Valuta val = mc.getValuta().get(choose - 1);
		return val;

	}

	public static String chooseSecondValuta(Valuta val) {
		System.out.println("Pilih Mata uang tujuan : ");
		String code2 = sc.next();
		for (int i = 0; i < val.getCodeRates().size(); i++) {
			if (code2.equalsIgnoreCase(val.getCodeRates().get(i))) {
				System.out.println(val.getCode() + " ke " + code2 + " sebesar " + val.getRates().get(i));
				return code2;
			}
		}
		return "";
	}

	public static void tukar(Valuta val, String code2) {
		System.out.print("Masukkan Jumlah Uang yang ingin di tukar dalam :" + val.getCode() + " ");
		int jumlah = sc.nextInt();

		Change change = new Change();

		System.out.println("Nilai tukar ke " + code2 + " sebesar : " + change.exchangeMoney(jumlah, val, code2));
	}

	public static void pilih() {
		int choose = 0;
		while (true) {
			System.out.println("1. Tambah Valuta");
			System.out.println("2. Tukar Mata Uang");
			System.out.println("3. Keluar");
			System.out.print("Pilih (NUMBER): ");
			choose = sc.nextInt();
			if (choose == 1) {
				inputNewValuta();
			} else if (choose == 2) {
				
				Valuta val = chooseValuta();
				String code2 = chooseSecondValuta(val);
				tukar(val, code2);

			} else if (choose == 3) {
				System.out.println("Keluar");
				break;
			} else {
				System.out.println("Pilihan salah");
				System.out.println();
			}
		}
	}

	public static void inputNewValuta() {
		System.out.println("Input kode Valuta : ");
		String code = sc.next();
		ArrayList<Double> listRate = new ArrayList<>();
		ArrayList<String> listCode = new ArrayList<>();
		for (int i = 0; i < mc.getValuta().size(); i++) {
			ArrayList<String> listCodeRate = mc.getValuta().get(i).getCodeRates();
			listCodeRate.add(code);
			System.out.println("input nilai tukar terhadap " + mc.getValuta().get(i).getCode() + " : ");
			double n = sc.nextDouble();
			mc.getValuta().get(i).getRates().add(n);
			listCode.add(mc.getValuta().get(i).getCode());
			listRate.add(n);
		}
		Valuta val = new Valuta();
		Valuta val2 = val.addNewValuta(code, listCode, listRate);
		mc.getValuta().add(val2);
		System.out.println("Valuta berhasil ditambahkan");
	}

	public static void start() {

		ArrayList<Valuta> valuta = new ArrayList<>();
		Valuta valut;
		ArrayList<String> code = new ArrayList<>();
		ArrayList<Double> rates = new ArrayList<>();
		code.add("EUR");
		rates.add(0.8435169);
		code.add("IDR");
		rates.add(14.484671);
		valut = new Valuta("USD", code, rates);
		valuta.add(valut);

		code = new ArrayList<>();
		rates = new ArrayList<>();
		code.add("EUR");
		rates.add(0.0000582442);
		code.add("USD");
		rates.add(0.0000690533);
		valut = new Valuta("IDR", code, rates);
		valuta.add(valut);

		code = new ArrayList<>();
		rates = new ArrayList<>();
		code.add("USD");
		rates.add(1.1854577);
		code.add("IDR");
		rates.add(17.168641);
		valut = new Valuta("EUR", code, rates);
		valuta.add(valut);

		mc.setValuta(valuta);
	}

}
