package moneychanger.bean.model;

public class Change {

	public int amount;
	public String exchangeDate;
	public Valuta valuta;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getExchangeDate() {
		return exchangeDate;
	}

	public void setExchangeDate(String exchangeDate) {
		this.exchangeDate = exchangeDate;
	}

	public Valuta getValuta() {
		return valuta;
	}

	public void setValuta(Valuta valuta) {
		this.valuta = valuta;
	}

	public double exchangeMoney(int amount,Valuta valuta,String code2) {		

		double rate=0;
		for(int i=0;i<valuta.getCodeRates().size();i++) {
			if(code2.equalsIgnoreCase(valuta.getCodeRates().get(i))) {
				rate=valuta.getRates().get(i);
				break;
			}
		}
		return amount*rate;	
	}

}
