package moneychanger.bean.model;

import java.util.ArrayList;

public class Valuta {

	private String code;
	private ArrayList<String> codeRates;
	private ArrayList<Double> rates;

	public Valuta() {

	}

	public Valuta(String code, ArrayList<String> codeRates, ArrayList<Double> rates) {
		this.code = code;
		this.codeRates = codeRates;
		this.rates = rates;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ArrayList<String> getCodeRates() {
		return codeRates;
	}

	public void setCodeRates(ArrayList<String> codeRates) {
		this.codeRates = codeRates;
	}

	public ArrayList<Double> getRates() {
		return rates;
	}

	public void setRates(ArrayList<Double> rates) {
		this.rates = rates;
	}
	
	public Valuta addNewValuta(String code, ArrayList<String> codeRates, ArrayList<Double> rate) {
		ArrayList<Double> reverseRates=new ArrayList<>();
		for(int i=0;i<codeRates.size();i++) {
			reverseRates.add(1/rate.get(i));
		}
		Valuta val=new Valuta(code,codeRates,reverseRates);
		return val;
	}

}
