package moneychanger.bean.model;

import java.util.ArrayList;

public class MoneyChanger {
	private ArrayList<Valuta> valuta;

	public ArrayList<Valuta> getValuta() {
		return valuta;
	}

	public void setValuta(ArrayList<Valuta> valuta) {
		this.valuta = valuta;
	}

	public void showValuta(MoneyChanger mc) {
		System.out.println("Valuta :");
		for (int i = 0; i < valuta.size(); i++) {
			System.out.println((i + 1) + ". " + mc.getValuta().get(i).getCode());
		}
		
	}


}
