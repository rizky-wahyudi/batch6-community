package id.co.nexsoft.laundry.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.laundry.model.OrderItems;

@Repository
public interface OrderItemsRepository extends CrudRepository<OrderItems, Integer> {

	OrderItems save(OrderItems orderItems);

	List<OrderItems> findAll();

	OrderItems findById(int id);
	
}
