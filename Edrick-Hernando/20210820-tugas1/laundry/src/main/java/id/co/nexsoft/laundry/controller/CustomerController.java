package id.co.nexsoft.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.laundry.model.Customer;
import id.co.nexsoft.laundry.repository.AddressRepository;
import id.co.nexsoft.laundry.repository.CustomerRepository;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	
	@Autowired
	private CustomerRepository repo;
	@Autowired
	private AddressRepository addressRepo;
	
	@GetMapping("/")
	public String jalan() {
		return "";
	}
	
	@GetMapping("/showAllCustomer")
	public List<Customer> getAllCustomer(){
		return repo.findAll();
	}
	
	@PostMapping("/addCustomer/address:{id}")
	public Customer setCustomer(@RequestBody Customer customer,@PathVariable(value="id")int id) {
		customer.setAddress(addressRepo.findById(id));
		return repo.save(customer);
	}
}
