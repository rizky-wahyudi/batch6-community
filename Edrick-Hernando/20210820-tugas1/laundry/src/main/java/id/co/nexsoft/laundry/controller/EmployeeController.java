package id.co.nexsoft.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.laundry.model.Employee;
import id.co.nexsoft.laundry.repository.AddressRepository;
import id.co.nexsoft.laundry.repository.EmployeeRepository;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	
	@Autowired
	private EmployeeRepository repo;
	
	@Autowired
	private AddressRepository addressRepo;
	
	@GetMapping("/")
	public String jalan() {
		return "";
	}
	
	@GetMapping("/showAllEmployee")
	public List<Employee> getAllEmployee(){
		return repo.findAll();
	}
	
	@PostMapping("/addEmployee/address:{id}")
	public Employee setEmployee(@RequestBody Employee employee,@PathVariable(value="id") int id) {
		employee.setAddress(addressRepo.findById(id));
		return repo.save(employee);
	}
}
