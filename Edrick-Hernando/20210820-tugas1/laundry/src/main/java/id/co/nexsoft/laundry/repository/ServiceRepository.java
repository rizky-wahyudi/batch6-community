package id.co.nexsoft.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.laundry.model.Service;

@Repository
public interface ServiceRepository extends CrudRepository<Service, Integer> {

	Service save(Service service);

	List<Service> findAll();

	Service findById(int id);
}
