package id.co.nexsoft.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.laundry.model.Service;
import id.co.nexsoft.laundry.repository.ServiceRepository;

@RestController
@RequestMapping("/service")
public class ServiceController {
	@Autowired
	private ServiceRepository repo;

	@GetMapping("/")
	public String jalan() {
		return "";
	}

	@GetMapping("/showAllService")
	public List<Service> getAllService() {
		return repo.findAll();
	}

	@PostMapping("/addService")
	public Service setService(@RequestBody Service service) {
		return repo.save(service);
	}
}
