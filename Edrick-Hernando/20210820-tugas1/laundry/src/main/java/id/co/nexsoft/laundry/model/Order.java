package id.co.nexsoft.laundry.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Pesan")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@OneToMany(targetEntity = OrderItems.class)
	private List<OrderItems> orderItem;

	@JsonFormat(pattern = "yyyy-MM-dd")

	private LocalDate orderDate;

	private String status;

	@Column(name = "paymentStatus")
	private boolean paymentStatus;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate lastUpdateDate;

	public Order() {

	}

	public Order(List<OrderItems> orderItem, LocalDate orderDate, boolean paymentStatus, LocalDate lastUpdateDate) {
		this.orderItem = orderItem;
		this.orderDate = orderDate;
		this.paymentStatus = paymentStatus;
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<OrderItems> getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(List<OrderItems> orderItem) {
		this.orderItem = orderItem;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public LocalDate getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDate lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

}
