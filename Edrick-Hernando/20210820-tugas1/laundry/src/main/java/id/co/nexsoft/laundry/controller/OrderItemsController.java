package id.co.nexsoft.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.laundry.model.OrderItems;
import id.co.nexsoft.laundry.repository.OrderItemsRepository;
import id.co.nexsoft.laundry.repository.ServiceRepository;

@RestController
@RequestMapping("/orderitems")
public class OrderItemsController {

	@Autowired
	private OrderItemsRepository repo;
	@Autowired
	private ServiceRepository serviceRepo;
	
	@GetMapping("/")
	public String jalan() {
		return "";
	}
	
	@GetMapping("/showAllOrderItems")
	public List<OrderItems> getAllOrderItems(){
		return repo.findAll();
	}
	
	@PostMapping("/addOrderItems/service:{id}")
	public OrderItems setOrderItems(@RequestBody OrderItems orderItems,@PathVariable(value="id")int id) {
		orderItems.setService(serviceRepo.findById(id));
		return repo.save(orderItems);
	}
	
}
