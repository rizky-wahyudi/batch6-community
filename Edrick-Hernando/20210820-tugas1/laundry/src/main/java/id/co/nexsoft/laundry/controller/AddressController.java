package id.co.nexsoft.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.laundry.model.Address;
import id.co.nexsoft.laundry.repository.AddressRepository;

@RestController
@RequestMapping("/address")
public class AddressController {

	@Autowired
	private AddressRepository repo;
	
	@GetMapping("/")
	public String jalan() {
		return "";
	}
	
	@GetMapping("/showAllAddress")
	public List<Address> getAllAddress(){
		return repo.findAll();
	}
	
	@PostMapping("/addAddress")
	public Address setAddress(@RequestBody Address address) {
		return repo.save(address);
	}
	
}
