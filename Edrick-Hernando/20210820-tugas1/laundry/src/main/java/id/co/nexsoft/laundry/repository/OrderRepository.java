package id.co.nexsoft.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.laundry.model.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer> {

	Order save(Order order);

	List<Order> findAll();

	Order findById(int id);
}
