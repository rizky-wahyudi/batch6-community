package id.co.nexsoft.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.laundry.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {

	Customer save(Customer customer);

	List<Customer> findAll();

	Customer findById(int id);

}
