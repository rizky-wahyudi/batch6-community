package id.co.nexsoft.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.laundry.model.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

	Address save(Address address);

	List<Address> findAll();
	
	Address findById(int id);
}
