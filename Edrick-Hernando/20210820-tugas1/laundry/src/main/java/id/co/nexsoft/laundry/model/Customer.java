package id.co.nexsoft.laundry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Entity
public class Customer {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Pattern(regexp="^[a-zA-Z]*$")
	private String firstName;
	
	@Pattern(regexp="^[a-zA-Z]*$")
	private String lastName;
	
	
	@OneToOne(targetEntity=Address.class)
	private Address address;
	
	@Pattern(regexp="^[0-9]*$")
	private String phone;
	
	@Email
	private String email;
	
	private String avatar;

	public Customer() {
		
	}
	
	public Customer( @Pattern(regexp = "^[a-zA-Z]*$") String firstName,
			@Pattern(regexp = "^[a-zA-Z]*$") String lastName, 
			@Pattern(regexp = "^[0-9]*$") String phone, @Email String email, String avatar) {
		this.firstName = firstName;
		this.lastName = lastName;

		this.phone = phone;
		this.email = email;
		this.avatar = avatar;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	
	
	
}
