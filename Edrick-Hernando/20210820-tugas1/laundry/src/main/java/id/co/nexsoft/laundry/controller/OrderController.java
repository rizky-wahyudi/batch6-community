package id.co.nexsoft.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.laundry.model.Order;
import id.co.nexsoft.laundry.model.OrderItems;
import id.co.nexsoft.laundry.repository.OrderItemsRepository;
import id.co.nexsoft.laundry.repository.OrderRepository;

@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderRepository repo;

	@Autowired
	private OrderItemsRepository orderItemsRepo;
	
	@GetMapping("/showOrder/{id}")
	public Order getOrder(@PathVariable int id) {
		return repo.findById(id);
	}
	
	@GetMapping("/")
	public String jalan() {
		return "";
	}

	@GetMapping("/showAllOrder")
	public List<Order> getAllOrder() {
		return repo.findAll();
	}
	
	

	@PostMapping("/addOrder")
	public Order setOrder(@RequestBody Order order) {
		return repo.save(order);
	}

	@PostMapping(value = "/addOrderBrute/employee:{id}", consumes = "application/json")
	public String addCountry(@RequestBody Order order,@PathVariable(value="id")int id) {

		for (OrderItems oi : order.getOrderItem()) {

			orderItemsRepo.save(oi);
		}
		order.setStatus(String.valueOf(id));
		repo.save(order);

		return "Data Berhasil Disimpan";
	}
	
	

}
