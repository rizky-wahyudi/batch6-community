package id.co.nexsoft.laundry.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Pattern(regexp = "^[a-zA-Z]*$")
	private String name;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "birthDate")
	private LocalDate birthDate;

	@OneToOne(targetEntity = Address.class)
	private Address address;

	@Pattern(regexp = "^[0-9]*$")
	private String phone;

	@Email
	private String email;
	
	@OneToMany(targetEntity=Order.class)
	private List<Order>order;

	public Employee() {
		
	}
	public Employee(@Pattern(regexp = "^[a-zA-Z]*$") String name, LocalDate birthDate,
			@Pattern(regexp = "^[0-9]*$") String phone, @Email String email,List<Order>order) {
		this.name = name;
		this.birthDate = birthDate;
		
		this.phone = phone;
		this.email = email;
		this.order=order;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Order> getOrder() {
		return order;
	}

	public void setOrder(List<Order> order) {
		this.order = order;
	}
	
	

}
