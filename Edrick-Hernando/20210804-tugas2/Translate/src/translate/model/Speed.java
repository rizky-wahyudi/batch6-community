package translate.model;

public class Speed implements Convert {

	private double kecepatan;

	public double getKecepatan() {
		return kecepatan;
	}

	public void setKecepatan(double kecepatan) {
		this.kecepatan = kecepatan;
	}

	@Override
	public void Konversi(int i) {
		if(i==1) {
			double newSpeed=getKecepatan()*0.62137;
			System.out.println(getKecepatan()+" KM = "+newSpeed+" Miles");
		}else {
			double newSpeed=getKecepatan()*1.609;
			System.out.println(getKecepatan()+ " Miles = "+newSpeed+" KM");
		}
		
	}

}
