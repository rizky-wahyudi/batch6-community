package translate.model;

public class Suhu implements Convert{

	private double temperature;

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	@Override
	public void Konversi(int i) {
		if(i==1) {
			
			double newTemp=(getTemperature()*(9/5))+32;
			System.out.println(getTemperature()+" C = "+newTemp+" F");
		}else {
			
			double newTemp=(getTemperature()-32)*5/9;
			System.out.println(getTemperature()+" F = "+newTemp+" C");
		}
		
	}

	
	
}
