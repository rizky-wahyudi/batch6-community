package translate.main;

import java.util.Scanner;

import translate.model.Speed;
import translate.model.Suhu;

public class Main {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		while (true) {
			System.out.println("1. Kecepatan");
			System.out.println("2. Suhu");
			System.out.print("Pilih : ");
			int choose = sc.nextInt();
			System.out.println();
			if (choose == 1 || choose == 2) {
				chooseParam(choose);

			} else {
				System.out.println("Program Berakhir!!!");
				break;
			}
		}
	}

	public static void chooseParam(int choose) {
		int pilih = 0;
		boolean loop = true;
		if (choose == 1) {
			Speed speed = new Speed();
			double cepat = 0;

			while (loop) {
				System.out.println("1.KM");
				System.out.println("2.Miles");
				System.out.print("pilih masukkan awal:");
				pilih = sc.nextInt();
				System.out.println();
				switch (pilih) {
				case 1:
					System.out.println("Masukkan Kecepatan Dalam KM :");
					cepat = sc.nextDouble();
					speed.setKecepatan(cepat);
					speed.Konversi(pilih);
					loop = false;
					break;
				case 2:
					System.out.println("Masukkan Kecepatan Dalam Miles :");
					cepat = sc.nextDouble();
					speed.setKecepatan(cepat);
					speed.Konversi(pilih);
					loop = false;
					break;
				default:
					System.out.println("Pilihan Tidak ada");
					break;
				}
			}

		} else if (choose == 2) {
			double suhu = 0;
			Suhu temp = new Suhu();
			while (loop) {
				System.out.println("1.Celcius");
				System.out.println("2.Fahrenheit");
				System.out.print("pilih masukkan awal:");
				pilih = sc.nextInt();
				System.out.println();
				switch (pilih) {
				case 1:
					System.out.println("Masukkan Suhu Dalam Celcius :");
					suhu = sc.nextDouble();

					temp.setTemperature(suhu);
					temp.Konversi(pilih);
					loop = false;
					break;
				case 2:
					System.out.println("Masukkan Suhu Dalam Fahrenheit :");
					suhu = sc.nextDouble();

					temp.setTemperature(suhu);
					temp.Konversi(pilih);
					loop = false;
					break;
				default:
					System.out.println("Pilihan Tidak ada");
					break;
				}
			}

		}
	}
}
