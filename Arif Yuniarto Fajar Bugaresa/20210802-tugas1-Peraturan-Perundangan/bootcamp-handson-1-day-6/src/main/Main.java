package main;

import model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Perpres1:Start

        // menetapkan
        ArrayList<SubPasal> listMenetapkanSubPasal = new ArrayList<SubPasal>();

        SubPasal pasal1subPasal1 = new SubPasal(
                1,
                "Pegawai Negeri Sipil yang bekerja pada Pemerintah\n" +
                        "Daerah dibebankan pada Anggaran Pendapatan dan\n" +
                        "Belanja Daerah"
        );

        SubPasal pasal1subPasal2 = new SubPasal(
             2,
                "Pegawai Negeri Sipil yang bekerja pada Pemerintah\n" +
                        "Pusat dibebankan pada Anggaran Pendapatan dan\n" +
                        "Belanja Negara"
        );

        listMenetapkanSubPasal.add(pasal1subPasal1);
        listMenetapkanSubPasal.add(pasal1subPasal2);

        Pasal menetapkanPasal = new Pasal(
                1,
                "Pemberian Tunjangan Analis Ketahanan Pangan bagi:",
                listMenetapkanSubPasal
        );

        ArrayList<Pasal> listMenetapkanPasal = new ArrayList<Pasal>();

        listMenetapkanPasal.add(menetapkanPasal);

        Menetapkan perpres1Menetapkan = new Menetapkan(
                "PERATURAN PRESIDEN TENTANG TUNJANGAN JABATAN\n" +
                        "FUNGSIONAL ANALIS KETAHANAN PANGAN.",
                listMenetapkanPasal
        );

        // Menimbang
        ArrayList<SubMenimbang> listMenimbangPerpres1 = new ArrayList<SubMenimbang>();

        SubMenimbang subMenimbang1Perpres1 = new SubMenimbang(
                1,
                "bahwa untuk meningkatkan mutu, prestasi,\n" +
                        "pengabdian, dan produktivitas kinerja Pegawai Negeri\n" +
                        "Sipil yang diangkat dan ditugaskan secara penuh\n" +
                        "dalam Jabatan Fungsional Analis Ketahanan Pangan,\n" +
                        "perlu diberikan Tunjangan Jabatan Fungsional Analis\n" +
                        "Ketahanan Pangan yang sesuai dengan beban kerja\n" +
                        "dan tanggung jawab pekerjaan;"
        );

        SubMenimbang subMenimbang2Perpres1 = new SubMenimbang(
                2,
                "bahwa berdasarkan pertimbangan sebagaimana\n" +
                        "dimaksud, perlu menetapkan Peraturan\n" +
                        "Presiden tentang Tunjangan Jabatan Fungsional\n" +
                        "Analis Ketahanan Pangan;"
        );

        listMenimbangPerpres1.add(subMenimbang1Perpres1);
        listMenimbangPerpres1.add(subMenimbang2Perpres1);

        Menimbang menimbangPerpres1 = new Menimbang(1, listMenimbangPerpres1);

        String mengingatPerpres1 = "Undang-Undang Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara";

        DetailPeraturan detailPerpres1 = new DetailPeraturan(
                1,
                menimbangPerpres1,
                mengingatPerpres1,
                perpres1Menetapkan
        );

        Peraturan perpres1 = new Peraturan(
                "PERPRES NO. 21 TAHUN 2021",
                "Peraturan Presiden",
                LocalDate.of(2021, 4,6),
                LocalDate.of(2021,4,1),
                21,
                2021,
                90,
                90,
                "TUNJANGAN JABATAN FUNGSIONAL ANALIS KETAHANAN PANGAN"
        );

        perpres1.setDetailPeraturan(detailPerpres1);
        // Perpres1:End


        //Main
        Scanner scanner = new Scanner(System.in);

        System.out.println("Peraturan Presiden");
        System.out.println("1." + perpres1.getNamaPeraturan());
        System.out.println("\nmau lihat detailnya?");
        System.out.print("Masukan [y/n]: ");
        String masukan = scanner.nextLine();

        if (masukan.equals("y")) {
            tampilkanPeraturan(perpres1);
        } else {
            System.out.println("Oke, Silahkan kembali nanti ya");
        }
    }

    public static void tampilkanPeraturan(Peraturan peraturan) {
        System.out.println("Oke, berikut ini detailnya: \n");
        System.out.println("Nama Peraturan: " + peraturan.getNamaPeraturan());
        System.out.println("Jenis Peraturan: " + peraturan.getJenisPeraturan());
        System.out.println("Tahun Peraturan: " + peraturan.getTahunPeraturan());
        System.out.println("Tentang: " + peraturan.getTentang());
        System.out.println("Tgk ditetapkan: " + peraturan.getTanggalDiterapkan());
        System.out.println("Nomor LN: " + peraturan.getNomorLn());
        System.out.println("Nomor TLN: "+ peraturan.getNomorTln());
        System.out.println("Tanggal Diundangkan: " + peraturan.getTanggalDiundangkan());

        System.out.println("\nMenimbang: ");
        for (SubMenimbang sm: peraturan.getDetailPeraturan().getMenimbang().getSubMenimbang()) {
            System.out.print(sm.getId() + ": ");
            System.out.println(sm.getPoint());
        }

        System.out.println("\nMengingat: ");
        System.out.println(peraturan.getDetailPeraturan().getMengingat());

        System.out.println("\nMEMUTUSKAN: ");
        System.out.println("Menetapkan: ");
        System.out.println(peraturan.getDetailPeraturan().getMenetapkan().getIsiPenetapan());
        System.out.println("");

        for (Pasal lp: peraturan.getDetailPeraturan().getMenetapkan().getListPasal()) {
            System.out.print("Pasal " + lp.getId() + ": \n");
            System.out.println(lp.getIsiPasal());
            for (SubPasal sp: lp.getListSubPasal()) {
                System.out.print(sp.getNomorSubPasal() + ": ");
                System.out.println(sp.getIsiSubPasal());
            }
        }
    }
}
