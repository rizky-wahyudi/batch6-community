package model;

public class SubPasal {
    int id;
    String isiSubPasal;

    public SubPasal(int nomorSubPasal, String isiSubPasal) {
        this.id = nomorSubPasal;
        this.isiSubPasal = isiSubPasal;
    }

    public int getNomorSubPasal() {
        return id;
    }

    public String getIsiSubPasal() {
        return isiSubPasal;
    }
}
