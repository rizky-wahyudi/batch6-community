package model;

import java.util.ArrayList;

public class Pasal {
    int id;
    String isiPasal;
    ArrayList<SubPasal> listSubPasal;

    public Pasal(int nomorPasal, String isiPasal, ArrayList<SubPasal> listSubPasal) {
        this.id = nomorPasal;
        this.listSubPasal = listSubPasal;
        this.isiPasal = isiPasal;
    }

    public int getId() {
        return id;
    }

    public String getIsiPasal() {
        return isiPasal;
    }

    public ArrayList<SubPasal> getListSubPasal() {
        return listSubPasal;
    }
}
