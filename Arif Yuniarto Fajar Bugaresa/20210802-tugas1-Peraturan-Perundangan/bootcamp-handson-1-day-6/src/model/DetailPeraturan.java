package model;

import java.util.ArrayList;

public class DetailPeraturan {
    private int id;
    private Menimbang menimbang;
    private String mengingat;
    private Menetapkan menetapkan;

    public DetailPeraturan(int id, Menimbang menimbang, String mengingat, Menetapkan menetapkan) {
        this.id = id;
        this.menimbang = menimbang;
        this.mengingat = mengingat;
        this.menetapkan = menetapkan;
    }

    public int getId() {
        return id;
    }

    public Menimbang getMenimbang() {
        return menimbang;
    }

    public String getMengingat() {
        return mengingat;
    }

    public Menetapkan getMenetapkan() {
        return menetapkan;
    }
}
