package model;

public class SubMenimbang {
    private int id;
    private String point;

    public SubMenimbang(int id, String point) {
        this.id = id;
        this.point = point;
    }

    public int getId() {
        return id;
    }

    public String getPoint() {
        return point;
    }
}
