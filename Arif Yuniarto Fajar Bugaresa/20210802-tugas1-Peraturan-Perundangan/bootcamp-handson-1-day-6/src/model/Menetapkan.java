package model;

import java.util.ArrayList;

public class Menetapkan {
    private int id;
    private String isiPenetapan;
    private ArrayList<Pasal> listPasal;

    public Menetapkan(String isiPenetapan, ArrayList<Pasal> listPasal) {
        this.isiPenetapan = isiPenetapan;
        this.listPasal = listPasal;
    }

    public String getIsiPenetapan() {
        return isiPenetapan;
    }

    public ArrayList<Pasal> getListPasal() {
        return listPasal;
    }
}
