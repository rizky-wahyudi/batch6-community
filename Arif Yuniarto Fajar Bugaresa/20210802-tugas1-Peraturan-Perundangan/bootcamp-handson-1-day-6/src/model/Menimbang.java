package model;

import java.util.ArrayList;

public class Menimbang {
    private int id;
    private ArrayList<SubMenimbang> subMenimbang;

    public Menimbang(int id, ArrayList<SubMenimbang> subMenimbang) {
        this.id = id;
        this.subMenimbang = subMenimbang;
    }

    public int getId() {
        return id;
    }

    public ArrayList<SubMenimbang> getSubMenimbang() {
        return subMenimbang;
    }
}
