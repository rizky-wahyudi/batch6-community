package model;

import java.time.LocalDate;

public class Peraturan {
    private String namaPeraturan;
    private String jenisPeraturan;
    LocalDate tanggalDiundangkan;
    LocalDate tanggalDiterapkan;
    private int nomorPeraturan;
    private int tahunPeraturan;
    int nomorTln;
    int nomorLn;
    String tentang;

    DetailPeraturan detailPeraturan;

    public Peraturan(
            String namaPeraturan,
            String jenisPeraturan,
            LocalDate tanggalDiundangkan,
            LocalDate tanggalDiterapkan,
            int nomorPeraturan,
            int tahunPeraturan,
            int nomorTln,
            int nomorLn,
            String tentang) {
        this.namaPeraturan = namaPeraturan;
        this.jenisPeraturan = jenisPeraturan;
        this.tanggalDiundangkan = tanggalDiundangkan;
        this.tanggalDiterapkan = tanggalDiterapkan;
        this.nomorPeraturan = nomorPeraturan;
        this.tahunPeraturan = tahunPeraturan;
        this.nomorTln = nomorTln;
        this.nomorLn = nomorLn;
        this.tentang = tentang;
    }

    public DetailPeraturan getDetailPeraturan() {
        return detailPeraturan;
    }

    public void setDetailPeraturan(DetailPeraturan detailPeraturan) {
        this.detailPeraturan = detailPeraturan;
    }

    public String getNamaPeraturan() {
        return namaPeraturan;
    }

    public String getJenisPeraturan() {
        return jenisPeraturan;
    }

    public LocalDate getTanggalDiundangkan() {
        return tanggalDiundangkan;
    }

    public LocalDate getTanggalDiterapkan() {
        return tanggalDiterapkan;
    }

    public int getNomorPeraturan() {
        return nomorPeraturan;
    }

    public int getTahunPeraturan() {
        return tahunPeraturan;
    }

    public int getNomorTln() {
        return nomorTln;
    }

    public int getNomorLn() {
        return nomorLn;
    }

    public String getTentang() {
        return tentang;
    }
}
