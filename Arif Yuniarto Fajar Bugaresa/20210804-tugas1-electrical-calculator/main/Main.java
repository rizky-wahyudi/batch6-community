package main;

import model.Calculator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception{
        Calculator calculator = new Calculator();
        Scanner input = new Scanner(System.in);

        System.out.println("Electrical Calculator");
        System.out.print("Masukkan Resistance(R): ");
        double resistance = input.nextDouble();

        System.out.print("Masukkan Current(I): ");
        double current = input.nextDouble();

        System.out.print("Masukkan Voltage(V): ");
        double voltage = input.nextDouble();

        System.out.print("Masukkan Power(P): ");
        double power = input.nextDouble();

        cekValidasi(resistance, current, voltage, power);
        setHasil(calculator, resistance, current, voltage, power);
        getHasil(calculator);

    }
    public static void cekValidasi(double resistance, double current, double voltage, double power) throws Exception{

        double[] inputanUser = {resistance, current, voltage, power};

        int jumlahSlotKosong = 0;
        for (int i = 0; i < 4; i++) {
            if (inputanUser[i] == 0 ) {
                jumlahSlotKosong++;
            } else {
                continue;
            }
        }

        if (jumlahSlotKosong != 2) {
            System.err.println("ERROR: inputan tidak valid");
            System.exit(0);
        } else {
            System.out.println("Calculating...");
            Thread.sleep(1500);
        }

    }

    public static void getHasil(Calculator calculator) {
        System.out.println("\nHasil Perhitungan");
        System.out.println("Resistance (R): " + calculator.getResistance() + " ohms");
        System.out.println("Current (I): " + calculator.getCurrent() + " amps");
        System.out.println("Voltage (V): " + calculator.getVoltage() + " volts");
        System.out.println("Power (P): " + calculator.getPower() + " watts");
    }

    public static void setHasil(Calculator calculator, double r, double i, double v, double p) {

        if ( r == 0 && i == 0) {
            calculator.setResistance(getNilaiResistanceVP(v,p));
            calculator.setCurrent(getNilaiCurrentPV(p,v));
            calculator.setVoltage(v);
            calculator.setPower(p);
        } else if ( r == 0 && v == 0) {
            calculator.setResistance(getNilaiResistancePI(p,i));
            calculator.setCurrent(i);
            calculator.setVoltage(getNilaiVoltagePI(p,i));
            calculator.setPower(p);
        } else if ( r == 0 && p == 0) {
            calculator.setResistance(getNilaiResistanceVI(v,i));
            calculator.setCurrent(i);
            calculator.setVoltage(v);
            calculator.setPower(getNilaiPowerVI(v,i));
        } else if ( i == 0 && v == 0) {
            calculator.setResistance(r);
            calculator.setPower(p);
            calculator.setCurrent(getNilaiCurrentPR(p,r));
            calculator.setVoltage(getNilaiVoltagePR(p,r));
        } else if (i == 0 && p == 0) {
            calculator.setResistance(r);
            calculator.setCurrent(getNilaiCurrentVR(v,r));
            calculator.setVoltage(v);
            calculator.setPower(getNilaiPowerVR(v,r));
        } else if ( v == 0 && p == 0) {
            calculator.setResistance(r);
            calculator.setCurrent(i);
            calculator.setVoltage(getNilaiVoltageIR(i,r));
            calculator.setPower(getNilaiPowerIR(i,r));
        } else {
            System.err.println("ERROR: inputan tidak sesuai");
            System.exit(0);
        }
    }

    public static double getNilaiResistanceVI(double v, double i) {
        return v / i ;
    }

    public static double getNilaiResistanceVP(double v, double p) {
        return v * v / p;
    }

    public static double getNilaiResistancePI(double p, double i) {
        return p / i * i;
    }

    public static double getNilaiCurrentVR(double v, double r) {
        return v / r;
    }

    public static double getNilaiCurrentPV(double p, double v) {
        return p / v;
    }

    public static double getNilaiCurrentPR(double p, double r) {
        return Math.sqrt((p/r));
    }

    public static double getNilaiVoltageIR(double i, double r) {
        return i * r;
    }

    public static double getNilaiVoltagePI(double p, double i) {
        return p / i;
    }

    public static double getNilaiVoltagePR(double p, double r) {
        return Math.sqrt((p*r));
    }

    public static double getNilaiPowerVI(double v, double i) {
        return v * i;
    }

    public static double getNilaiPowerVR(double v, double r) {
        return v * v / r;
    }

    public static double getNilaiPowerIR(double i, double r) {
        return i * i * r;
    }
}
