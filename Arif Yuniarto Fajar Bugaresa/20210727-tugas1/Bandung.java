public class Bandung extends Liburan{
    private String destinasi;
    private int hargaTiket;
    private int hargaTransportasi;

    public Bandung(String destinasi, int hargaTiket, int hargaTransportasi) {
        this.destinasi = destinasi;
        this.hargaTiket = hargaTiket;
        this.hargaTransportasi = hargaTransportasi;
    }

    public int getHargaTiket() {
        return hargaTiket;
    }

    public void setHargaTiket(int hargaTiket) {
        this.hargaTiket = hargaTiket;
    }

    public int getHargaTransportasi() {
        return hargaTransportasi;
    }

    public void setHargaTransportasi(int hargaTransportasi) {
        this.hargaTransportasi = hargaTransportasi;
    }

    public String getDestinasi() {
        return destinasi;
    }

    public void setDestinasi(String destinasi) {
        this.destinasi = destinasi;
    }

    public int getHargaLiburan() {
        return hargaTiket + hargaTransportasi;
    }
}
