public class NewYorkStaticFactory {
    public  static NewYork newInstance(String type) {
        if (type.equals("cp")) {
            return new NewYork(
                    "Central Park",
                    1000,
                    2000
            );
        } else if (type.equals("tmmoa")) {
            return new NewYork(
                    "The Metropolitan Museum of Art",
                    2000,
                    3000
            );
        } else if (type.equals("esb")) {
            return new NewYork(
                    "Empire State Building",
                    3000,
                    4000
            );
        }
        return null;
    }
}
