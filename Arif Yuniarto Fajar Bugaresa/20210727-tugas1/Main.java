import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Tujuan = ");
        String a = scanner.nextLine();

        System.out.print("Jumlah Orang = ");
        int b = scanner.nextInt();

        System.out.print("Jumlah hari = ");
        int c = scanner.nextInt();

        System.out.println("Biaya: " + hitungBiayaDestinasi(a, b, c));

    }

    static int hitungBiayaDestinasi(String a, int b, int c) {
        int hasil = 0;

        if (a.equals("tp")) {
            Bandung tujuan = BandungStaticFactory.newInstance("tp");
            hasil = tujuan.getHargaLiburan()*b*c;
        } else if ( a.equals("jb")) {
            Bandung tujuan = BandungStaticFactory.newInstance("jb");
            hasil = tujuan.getHargaLiburan()*b*c;
        } else if ( a.equals("gs")) {
            Bandung tujuan = BandungStaticFactory.newInstance("gs");
            hasil = tujuan.getHargaLiburan()*b*c;
        } else if ( a.equals("cp")) {
            NewYork tujuan = NewYorkStaticFactory.newInstance("cp");
            hasil = tujuan.getHargaLiburan()*b*c;
        } else if ( a.equals("tmmoa")) {
            NewYork tujuan = NewYorkStaticFactory.newInstance("tmmoa");
            hasil = tujuan.getHargaLiburan()*b*c;
        } else if ( a.equals("esb")) {
            NewYork tujuan = NewYorkStaticFactory.newInstance("esb");
            hasil = tujuan.getHargaLiburan()*b*c;
        } else if ( a.equals("ts")) {
            Tokyo tujuan = TokyoStaticFactory.newInstance("ts");
            hasil = tujuan.getHargaLiburan()*b*c;
        } else if ( a.equals("mj")) {
            Tokyo tujuan = TokyoStaticFactory.newInstance("mj");
            hasil = tujuan.getHargaLiburan() *b*c;
        } else if ( a.equals("sgng")) {
            Tokyo tujuan = TokyoStaticFactory.newInstance("sgng");
            hasil = tujuan.getHargaLiburan()*b*c;
        }

        return hasil;
    }
}
