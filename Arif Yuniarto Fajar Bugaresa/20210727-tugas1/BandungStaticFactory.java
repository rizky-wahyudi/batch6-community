public class BandungStaticFactory {

    public  static Bandung newInstance(String type) {
        if (type.equals("tp")) {
            return new Bandung(
                    "Tangkuban Perahu",
                    1000,
                    2000
            );
        } else if (type.equals("jb")) {
            return new Bandung(
                    "Jalan Braga",
                    2000,
                    3000
            );
        } else if (type.equals("gs")) {
            return new Bandung(
                    "Gedung Sate",
                    3000,
                    4000
            );
        }
        return null;
    }
}
