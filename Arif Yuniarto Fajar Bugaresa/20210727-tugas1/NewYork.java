public class NewYork {
    private String destinasi;
    private int hargaTiket;
    private int hargaTransportasi;

    public NewYork(String destinasi, int hargaTiket, int hargaTransportasi) {
        this.destinasi = destinasi;
        this.hargaTiket = hargaTiket;
        this.hargaTransportasi = hargaTransportasi;
    }

    public String getDestinasi() {
        return destinasi;
    }

    public int getHargaTiket() {
        return hargaTiket;
    }

    public int getHargaTransportasi() {
        return hargaTransportasi;
    }

    public int getHargaLiburan() {
        return  hargaTiket + hargaTransportasi;
    }
}
