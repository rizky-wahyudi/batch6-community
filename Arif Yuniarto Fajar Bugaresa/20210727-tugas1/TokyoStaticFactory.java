public class TokyoStaticFactory {
    public  static Tokyo newInstance(String type) {
        if (type.equals("ts")) {
            return new Tokyo(
                    "Tokyo Skytree",
                    1000,
                    2000
            );
        } else if (type.equals("mj")) {
            return new Tokyo(
                    "Meiji Jingu",
                    2000,
                    3000
            );
        } else if (type.equals("sgng")) {
            return new Tokyo(
                    "Shinjoku Gyoen National Garden",
                    3000,
                    4000
            );
        }
        return null;
    }
}
