package controller;

import entity.Address;
import entity.DistanceMapper;
import entity.Transport;
import entity.Warehouse;
import repository.WarehouseRepository;

import java.util.HashMap;
import java.util.Map;

public class WarehouseController {
    WarehouseRepository warehouseRepository = new WarehouseRepository();

    public void createWarehouse(
            int id,
            String name,
            Address address,
            DistanceMapper distanceMapper
    ) {
        warehouseRepository.add(id, name, address, distanceMapper);
    }

    public void showRoute(Transport transport) {
        HashMap<Integer, Warehouse> listWarehouse = warehouseRepository.getAll();
        int speedVehicle = transport.getSpeed();

        System.out.println("Rute Pengiriman Pekalongan-Jakarta");
        System.out.println("==================================");

        for ( Map.Entry<Integer, Warehouse> warehouseEntry : listWarehouse.entrySet()) {
                System.out.print(warehouseEntry.getKey() + ". ");
                System.out.println(warehouseEntry.getValue().getName());
                int jarak = warehouseEntry.getValue().getDistanceMapper().getDistance();
                double lamaPerjalan = jarak * 60 / speedVehicle;

                if (warehouseEntry.getKey() !=  listWarehouse.size()) {
                    System.out.println("Jarak ke warehouse selanjutnya: " + jarak + " km");
                    System.out.println("Kecepatan berkendara: " + speedVehicle + " km/jam");
                    System.out.println("Estimasi waktu: " + lamaPerjalan + " menit");
                } else {
                    System.out.println("Distribusi ke costumer");
                }
        }
    }
}
