package controller;

import entity.Transport;
import repository.TransportRepository;

public class TransportController {
    TransportRepository transportRepository = new TransportRepository();

    public void createTransport(int id, String driverName, int sizeMax, int speed, String type, int load ) {
        transportRepository.add(id, driverName, sizeMax, speed, type, load);
    }

    public Transport getTrasportation(int id) {
        return transportRepository.getById(id);
    }


}
