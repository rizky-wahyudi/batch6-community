package controller;

import entity.DistanceMapper;
import repository.DistanceMapperRepository;

public class DistanceMapperController {
    DistanceMapperRepository distanceMapperRepository = new DistanceMapperRepository();

    public void createDistanaceMapper(
            int id,
            int fromWarehouseId,
            int toWarehouseId,
            int distance
    ) {
        distanceMapperRepository.add(id,fromWarehouseId,toWarehouseId,distance);
    }

    public DistanceMapper getDistancemapperById(int id) {
        return distanceMapperRepository.getById(id);
    }

}
