package controller;

import entity.Address;
import repository.AddressRepository;

public class AddressController {
    private AddressRepository addressRepository = new AddressRepository();

    public void createAddress(
            int id,
            String street,
            String district,
            String province,
            String country,
            String longitude,
            String latitide
    ) {
        addressRepository.add(id, street, district, province, country,longitude, latitide);
    }

    public Address getAddressById(int id) {
        return addressRepository.getById(id);
    }

}
