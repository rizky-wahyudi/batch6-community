package controller;

import entity.Product;
import repository.ProductRepository;

import java.util.HashMap;
import java.util.Map;

public class ProductController {
    ProductRepository productRepository = new ProductRepository();

    public void createProduct(
            int id,
            String name,
            String deliverAddress,
            int width,
            int height,
            int depth,
            int weight
    ) {
        productRepository.add(id, name, deliverAddress, width, height, depth, weight);
    }

    public int getTotalProduct() {
        HashMap<Integer, Product> products = productRepository.getAll();
        return products.size();
    }

    public int getTotalWeigh() {
        HashMap<Integer, Product> listProduct = productRepository.getAll();
        int totalWeight = 0;
        for ( Map.Entry<Integer, Product> productEntry : listProduct.entrySet()) {
            totalWeight += productEntry.getValue().getWeight();
        }
        return totalWeight;
    }

    public int getTotalSizeProduct(){
        HashMap<Integer, Product> listProduct = productRepository.getAll();
        int totalSizeProduct = 0;

        for ( Map.Entry<Integer, Product> productEntry : listProduct.entrySet()) {
            int height = productEntry.getValue().getHeight();
            int width = productEntry.getValue().getWidth();
            int depth = productEntry.getValue().getDepth();
            int size = height * width * depth;
            totalSizeProduct = totalSizeProduct + size;
        }
        return totalSizeProduct;
    }

    public void showAllProduct() {
        HashMap<Integer, Product> listProduct = productRepository.getAll();

        System.out.println("List Produk: ");
        for ( Map.Entry<Integer, Product> productEntry : listProduct.entrySet()) {
            System.out.print(productEntry.getKey() + ". ");
            System.out.print(productEntry.getValue().getName());
            System.out.print(" [ width: " + productEntry.getValue().getWidth() + " cm, ");
            System.out.print("heigh: " + productEntry.getValue().getHeight() + " cm, ");
            System.out.print("depth: " + productEntry.getValue().getDepth() + " cm, ");
            System.out.println("weight: " + productEntry.getValue().getWeight() + " kg ]");
        }

        System.out.println("\nTotal Produk: " + getTotalProduct());
        System.out.println("Total Size: " + getTotalSizeProduct() + " cm kubik");
        System.out.println("Total Weight: " + getTotalWeigh() + " kg\n");
    }

}
