package entity;

public class Warehouse {
    int id;
    String name;
    Address address;
    DistanceMapper distanceMapper;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public DistanceMapper getDistanceMapper() {
        return distanceMapper;
    }

    public void setDistanceMapper(DistanceMapper distanceMapper) {
        this.distanceMapper = distanceMapper;
    }
}
