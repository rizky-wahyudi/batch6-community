import controller.*;
import entity.Address;
import entity.DistanceMapper;
import entity.Transport;

public class LogisticApp {
    public static void main(String[] args) {
        TransportController transportController = new TransportController();
        AddressController addressController = new AddressController();
        DistanceMapperController distanceMapperController = new DistanceMapperController();
        WarehouseController warehouseController = new WarehouseController();
        ProductController productController = new ProductController();

        LogisticApp.setupWarehouse(addressController, distanceMapperController, warehouseController);
        LogisticApp.setupTransportation(transportController);
        LogisticApp.setupProduct(productController);

        LogisticApp.showProduct(productController);
        LogisticApp.pickTransport(transportController,productController);
        LogisticApp.showTransport(transportController,productController);
        LogisticApp.showRoute(warehouseController, pickTransport(transportController,productController));
    }

    public static void setupWarehouse(
            AddressController addressController,
            DistanceMapperController distanceMapperController,
            WarehouseController warehouseController
    ) {

        LogisticApp.setupAddress(addressController);
        LogisticApp.setupDistanceMapper(distanceMapperController);

        Address alamatGudangPekalongan = addressController.getAddressById(1);
        DistanceMapper jarakPekalonganSemarang = distanceMapperController.getDistancemapperById(1);
        warehouseController.createWarehouse(
                1,
                "Gudang Pusat Pekalongan",
                alamatGudangPekalongan,
                jarakPekalonganSemarang
        );

        Address alamatGudangSemarang = addressController.getAddressById(2);
        DistanceMapper jarakSemarangJakarta = distanceMapperController.getDistancemapperById(2);
        warehouseController.createWarehouse(
                2,
                "Gudang Transit Semarang",
                alamatGudangSemarang,
                jarakSemarangJakarta
        );

        Address alamatGudangJakarta = addressController.getAddressById(3);
        DistanceMapper jarakKeKonsumen = distanceMapperController.getDistancemapperById(3);
        warehouseController.createWarehouse(
                3,
                "Gudang Distribusi Jakarta",
                alamatGudangJakarta,
                jarakKeKonsumen
        );
    }

    public static void setupAddress(AddressController addressController) {
        addressController.createAddress(
                1,
                "Jalan merdeka",
                "Pekalongan",
                "Jawa Tengah",
                "Indonesia",
                "87978968932",
                "9897687698"
        );

        addressController.createAddress(
                2,
                "Jalan Depok",
                "Semarang Kota",
                "Jawa Tengah",
                "Indonesia",
                "1010928",
                "09897897"
        );

        addressController.createAddress(
                3,
                "Jalan Patimura",
                "Jakarta Pusat",
                "DKI Jakarta",
                "Indonesia",
                "98097896",
                "0980989"
        );
    }

    public static void setupDistanceMapper(DistanceMapperController distanceMapperController) {
        distanceMapperController.createDistanaceMapper(
                1,
                1,
                2,
                40
        );

        distanceMapperController.createDistanaceMapper(
                2,
                2,
                3,
                50
        );

        distanceMapperController.createDistanaceMapper(
                3,
                3,
                3,
                0
        );
    }

    public static void setupTransportation(TransportController transportController) {
        transportController.createTransport(
                1,
                "Joko",
                100000,
                60,
                "truck",
                2000
        );

        transportController.createTransport(
                2,
                "Ari",
                10000,
                60,
                "car",
                10000
        );

        transportController.createTransport(
                3,
                "Budi",
                1000,
                60,
                "motor",
                50
        );
    }

    public static void setupProduct(ProductController productController) {
        productController.createProduct(
                1,
                "Kursi",
                "Jakarta Utara",
                5,
                5,
                10,
                10
        );

        productController.createProduct(
                2,
                "Meja",
                "Jakarta Selatan",
                10,
                10,
                10,
                20
        );

        productController.createProduct(
                3,
                "Lemari",
                "Jakarta Timur",
                15,
                15,
                15,
                40
        );

        productController.createProduct(
                4,
                "Kasur",
                "Jakarta Barat",
                20,
                20,
                20,
                10
        );
    }

    public static void showProduct(ProductController productController) {
        productController.showAllProduct();
    }

    public static void showRoute(WarehouseController warehouseController, Transport transport) {
        warehouseController.showRoute(transport);
    }

    public static void showTransport(TransportController tc, ProductController pc) {
        Transport transportation = LogisticApp.pickTransport(tc, pc);

        if (transportation != null) {
            System.out.println("Transportation ");
            System.out.println("Driver: " + transportation.getDriverName());
            System.out.println("Type: " + transportation.getType());
            System.out.println("Max Size: " + transportation.getSizeMax() + " cm kubik");
            System.out.println("Max Load: " + transportation.getLoad() + " kg");
            System.out.println("Speed: " + transportation.getSpeed() + " km/jam\n");
        } else {
            System.out.println("Tidak ada transportasi yang sesuai");
        }

    }

    public static Transport pickTransport(
            TransportController transportController,
            ProductController productController
    ) {
        int totalProduct = productController.getTotalProduct();
        int totalWeightProduct = productController.getTotalWeigh();
        int sizeProduct = productController.getTotalSizeProduct();

        Transport motor = transportController.getTrasportation(3);
        Transport car = transportController.getTrasportation(2);
        Transport truck = transportController.getTrasportation(1);

        Transport transportation = null;

        if (
                (( totalWeightProduct <= motor.getLoad() ) &&
                (totalProduct <= 60 ) &&
                (sizeProduct <= motor.getSizeMax()) )
        ) {
            transportation = motor;
        } else if (
                ((totalWeightProduct <= car.getLoad()) &&
                (sizeProduct <= car.getSizeMax()))
        ) {
            transportation = car;
        } else if (
                (( totalWeightProduct <= truck.getLoad() ) &&
                ( sizeProduct < truck.getSizeMax() ))
        ) {
            transportation = truck;

        } else  transportation = null;

        return transportation;
    }
}
