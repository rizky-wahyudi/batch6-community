package repository;

import entity.Product;

import java.util.HashMap;

public class ProductRepository {
    HashMap<Integer, Product> listProduct = new HashMap<Integer, Product>();

    public void add(
            int id,
            String name,
            String deliverAddress,
            int width,
            int height,
            int depth,
            int weight
    ) {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setDepth(depth);
        product.setHeight(height);
        product.setWidth(width);
        product.setDeliverAddress(deliverAddress);
        product.setWeight(weight);
        listProduct.put(id, product);
    }

    public HashMap<Integer, Product> getAll() {
        return listProduct;
    }

}
