package repository;

import entity.Transport;

import java.util.HashMap;
import java.util.Map;

public class TransportRepository {
    HashMap<Integer, Transport> listTransport = new HashMap<Integer, Transport>();

    public void add( int id, String dn, int sm, int s, String t, int l) {
        Transport transport = new Transport();
        transport.setId(id);
        transport.setDriverName(dn);
        transport.setLoad(l);
        transport.setSpeed(s);
        transport.setType(t);
        transport.setSizeMax(sm);
        listTransport.put(id, transport);
    }

    public HashMap<Integer, Transport> getAll() {
        return listTransport;
    }

    public Transport getById(int id) {
        Transport transport = null;

        for ( Map.Entry<Integer, Transport> transportEntry : listTransport.entrySet()) {
            if (transportEntry.getKey().equals(id)) {
                transport = transportEntry.getValue();
            }
        }
        return transport;
    }
}
