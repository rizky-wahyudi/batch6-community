package repository;

import entity.Address;

import java.util.HashMap;
import java.util.Map;

public class AddressRepository {
    HashMap<Integer, Address> listAddress = new HashMap<Integer, Address>();

    public void add(
        int id,
        String street,
        String district,
        String province,
        String country,
        String longitude,
        String latitide
    ) {
        Address address = new Address();
        address.setId(id);
        address.setCountry(country);
        address.setDistrict(district);
        address.setStreet(street);
        address.setProvince(province);
        address.setLongitude(longitude);
        address.setLatitide(latitide);
        listAddress.put(id, address);
    }

    public Address getById(int id) {
        Address address = null;

        for ( Map.Entry<Integer, Address> addressEntry : listAddress.entrySet()) {
            if (addressEntry.getKey().equals(id)) {
                address = addressEntry.getValue();
            }
        }
        return address;
    }
}
