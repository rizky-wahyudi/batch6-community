package repository;

import entity.DistanceMapper;

import java.util.HashMap;
import java.util.Map;

public class DistanceMapperRepository {
    HashMap<Integer, DistanceMapper> listDistanceMapper = new HashMap<Integer, DistanceMapper>();

    public void add(
        int id,
        int fromWarehouseId,
        int toWarehouseId,
        int distance
    ) {
        DistanceMapper distanceMapper = new DistanceMapper();
        distanceMapper.setDistance(distance);
        distanceMapper.setId(id);
        distanceMapper.setFromWarehouseId(fromWarehouseId);
        distanceMapper.setFromWarehouseId(toWarehouseId);
        listDistanceMapper.put(id, distanceMapper);
    }

    public DistanceMapper getById(int id) {
        DistanceMapper distanceMapper = null;

        for ( Map.Entry<Integer, DistanceMapper> distanceMapperEntry : listDistanceMapper.entrySet() ) {
            if (distanceMapperEntry.getKey().equals(id)) {
                distanceMapper = distanceMapperEntry.getValue();
            }
        }
        return distanceMapper;
    }
}
