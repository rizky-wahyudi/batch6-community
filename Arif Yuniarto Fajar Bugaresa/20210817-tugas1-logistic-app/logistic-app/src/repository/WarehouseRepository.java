package repository;

import entity.Address;
import entity.DistanceMapper;
import entity.Warehouse;

import java.util.HashMap;

public class WarehouseRepository {
    private HashMap<Integer, Warehouse> listWarehouse = new HashMap<Integer, Warehouse>();

    public void add(int id, String name, Address address, DistanceMapper distanceMapper){
        Warehouse warehouse = new Warehouse();
        warehouse.setId(id);
        warehouse.setName(name);
        warehouse.setAddress(address);
        warehouse.setDistanceMapper(distanceMapper);
        listWarehouse.put(id, warehouse);
    }

    public HashMap<Integer, Warehouse> getAll() {
        return listWarehouse;
    }



}
