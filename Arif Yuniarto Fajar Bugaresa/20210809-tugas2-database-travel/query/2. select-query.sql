select * from bagasi;
select * from transportasi;
select * from alamat;
select * from perjalanan;
select * from penumpang;
select * from ticket;
select * from dokumen;

select 
	ti.id						as "Ticket Id",
	pen.nama					as "Nama Penumpang",
    tr.nama_kendaraan			as "Transportasi",
    ba.berat_bawaan				as "Max Bawaan di Bagasi",
	per.keberangkatan			as "Keberangkatan",
    per.tujuan					as "Transit",
    do.nama						as "Syarat",
    per.waktu_keberangkatan		as "Jadwal"
from perjalanan as per
	join ticket as ti
	on per.ticket_id = ti.id
		join penumpang as pen
        on pen.id = per.penumpang_id
			join alamat as al
            on al.penumpang_id = pen.id
				join dokumen as do
				on do.id = per.dokumen_id
					join transportasi as tr
                    on tr.id = per.transportasi_id
						join bagasi as ba
                        on ba.id = tr.bagasi_id;