create database travel;

use travel;

-- create table

create table bagasi (
	id int not null auto_increment,
    berat_bawaan int,
    primary key(id)
);

select * from bagasi;

create table alamat(
	id int not null auto_increment,
    penumpang_id int,
    jalan varchar(100),
    kota varchar(100),
    primary key (id)
);

create table penumpang (
	id int not null auto_increment,
	nama varchar(100),
	no_hp varchar(20),
	primary key(id)
);

create table transportasi (
	id int not null auto_increment,
    bagasi_id int,
    nama_kendaraan varchar(10),
    primary key (id)
);

create table perjalanan (
	id int not null auto_increment,
    transportasi_id int,
    penumpang_id int,
    ticket_id int,
    dokumen_id int,
    keberangkatan varchar(50),
    tujuan varchar(50),
    waktu_keberangkatan date,
    primary key (id)
);

create table dokumen(
	id int not null auto_increment,
    nama varchar(200),
    primary key(id)
);

create table ticket(
	id int not null auto_increment,
    harga int,
    primary key(id)
);


-- relasi tabel

alter table alamat
	add constraint fk_alamat_penumpang
    foreign key (penumpang_id) references penumpang(id);
    
alter table transportasi
	add constraint fk_transportasi_bagasi
    foreign key (bagasi_id) references bagasi(id);
    
alter table perjalanan
	add constraint fk_perjalanan_transportasi
    foreign key (transportasi_id) references transportasi(id)
;

alter table perjalanan
	add constraint fk_perjalanan_penumpang
    foreign key (penumpang_id) references penumpang(id)
;

alter table perjalanan
	add constraint fk_perjalanan_ticket
    foreign key (ticket_id) references ticket(id)
;

alter table perjalanan
	add constraint fk_perjalanan_dokumen
    foreign key (dokumen_id) references dokumen(id)
;



