package main;

import model.Besaran;
import model.Translator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Translator translatorJarak = new Translator();
        Besaran km = new Besaran();
        km.setNama("Kilometer");

        Besaran m = new Besaran();
        m.setNama("Meter");

        translatorJarak.setBesaran1(km);
        translatorJarak.setBesaran2(m);

        Translator translatorTemperatur = new Translator();
        Besaran c = new Besaran();
        c.setNama("Celcius");

        Besaran f = new Besaran();
        f.setNama("Farenheit");

        translatorTemperatur.setBesaran1(c);
        translatorTemperatur.setBesaran2(f);


        System.out.println("Menu Translator");
        System.out.println("1. Jarak");
        System.out.println("2. Temperatur");
        displayMessageInput("1 atau 2");
        Scanner masukan = new Scanner(System.in);
        int inputMenu = masukan.nextInt();

        if (inputMenu == 1) {
            System.out.println("Oke, Mau konversi apa?");
            System.out.println("\n1. Kilometer ke Meter");
            System.out.println("2. Meter ke Kilometer");
            displayMessageInput("1 atau 2");
            int inputMenuJarak = masukan.nextInt();

            if (inputMenuJarak == 1) {
                displayMessageInput("kilometer");
                int inputBesaranKilometer = masukan.nextInt();

                translatorJarak.getBesaran1().setValue(inputBesaranKilometer);
                setMeter(translatorJarak);
                displayTranslator(translatorJarak);

            } else if (inputMenuJarak == 2) {
                displayMessageInput("meter");
                int inputBesaranMeter = masukan.nextInt();

                translatorJarak.getBesaran2().setValue(inputBesaranMeter);
                setKilometer(translatorJarak);
                displayTranslator(translatorJarak);

            } else {
                getErrorInputMessage();
            }
        } else if (inputMenu == 2) {
            System.out.println("Oke, Mau konversi apa?");
            System.out.println("\n1. Celcius ke Farenheit");
            System.out.println("2. Farenheit ke Celcius");

            displayMessageInput("1 atau 2");
            int inputMenuTemperatur = masukan.nextInt();

            if(inputMenuTemperatur == 1) {
                displayMessageInput("celcius");
                int inputBesaranCelcius = masukan.nextInt();

                translatorTemperatur.getBesaran1().setValue(inputBesaranCelcius);
                setFarenheit(translatorTemperatur);
                displayTranslator(translatorTemperatur);

            } else if (inputMenuTemperatur == 2) {
                displayMessageInput("farenheit");
                int inputBesaranFarenheit = masukan.nextInt();

                translatorTemperatur.getBesaran2().setValue(inputBesaranFarenheit);
                setCelcius(translatorTemperatur);
                displayTranslator(translatorTemperatur);

            } else {
                getErrorInputMessage();
            }

        } else {
            getErrorInputMessage();
        }
    }

    public static void setFarenheit(Translator translator) {
        double celcius = translator.getBesaran1().getValue();
        double farenheit = ((9.0/5.0) * celcius) + 32;
        translator.getBesaran2().setValue(farenheit);
    }

    public static void setCelcius(Translator translator) {
        double farenheit = translator.getBesaran2().getValue();
        double celcius = (5.0 / 9.0) * (farenheit - 32);
        translator.getBesaran1().setValue(celcius);
    }

    public static void setMeter(Translator translator) {
        double kilometer = translator.getBesaran1().getValue();
        double meter = kilometer * 1000;
        translator.getBesaran2().setValue(meter);
    }

    public static void setKilometer(Translator translator) {
        double meter = translator.getBesaran2().getValue();
        double kilometer = meter / 1000;
        translator.getBesaran1().setValue(kilometer);
    }

    public static void displayTranslator(Translator translator) {
        System.out.println("\nHasil Konversi");
        System.out.println(
                translator.getBesaran1().getValue() +
                " " +
                translator.getBesaran1().getNama() +
                " = " +
                translator.getBesaran2().getValue() +
                " " +
                translator.getBesaran2().getNama()
        );
    }

    public static void displayMessageInput(String masukan) {
        System.out.println("oke, sekarang berapa besaran yang mau dikonversi?");
        System.out.print("Masukan [" + masukan + "]: " );
    }

    public static void getErrorInputMessage() {
        System.err.println("ERROR: inputan salah");
    }


}
