package model;

public class Translator {
   Besaran besaran1;
   Besaran besaran2;

    public Besaran getBesaran1() {
        return besaran1;
    }

    public void setBesaran1(Besaran besaran1) {
        this.besaran1 = besaran1;
    }

    public Besaran getBesaran2() {
        return besaran2;
    }

    public void setBesaran2(Besaran besaran2) {
        this.besaran2 = besaran2;
    }
}
