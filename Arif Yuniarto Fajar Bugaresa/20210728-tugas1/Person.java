public class Person {
    private String name;
    private Passport passport;
    private Ticket ticket;
    private  Laguage laguage;

    public Person(String name, Passport passport, Ticket ticket, Laguage laguage) {
        this.name = name;
        this.passport = passport;
        this.ticket = ticket;
        this.laguage = laguage;
    }

    public String getName() {
        return name;
    }

    public Passport getPassport() {
        return passport;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public Laguage getLaguage() {
        return laguage;
    }
}
