import java.time.LocalDate;
import java.util.Scanner;

public class Airport {
    private String name;
    private String tujuan;
    private LocalDate tanggalKeberangkatan;
    private int laguage = 20 ;

    public Airport(String name, String tujuan, LocalDate tanggalKeberangkatan) {
        this.name = name;
        this.tujuan = tujuan;
        this.tanggalKeberangkatan = tanggalKeberangkatan;
    }

    public String getName() {
        return name;
    }

    public String getTujuan() {
        return tujuan;
    }

    public LocalDate getTanggalKeberangkatan() {
        return tanggalKeberangkatan;
    }

    public int getLaguage() {
        return laguage;
    }

    public void getInfo() {
        System.out.println("Selamat datang di " + getName());
        System.out.println("Kota Tujuan \t\t\t: " + getTujuan());
        System.out.println("Tanggal Penerbangan \t: " + getTanggalKeberangkatan());
        System.out.println("Berat Maksimal Bawaan \t: " + getLaguage());
    }

    public void checkPerson(Person person) throws Exception{
        security1(person);
        security2(person);
        security3(person);
    }

    public void security1(Person person) throws Exception{
        System.out.println("\nPengecekan Security 1");
        System.out.println("Checking...");
        Thread.sleep(2000);

        checkPassport(person);
        checkTicket(person);
        checkWeightLaguage(person);

        System.out.println("Lolos pengeceken X ray\n");
    }

    public void security2(Person person) throws Exception{
        System.out.println("Pengecekan Security 2");
        System.out.println("Checking...");
        Thread.sleep(2000);

        checkPassport(person);
        checkTicket(person);

        System.out.println("Lolos pengeceken X ray 2\n");
    }

    public void security3(Person person) throws Exception{
        System.out.println("Pengecekan Security 3");
        System.out.println("Checking...");
        Thread.sleep(2000);

        checkPassport(person);
        checkTicket(person);
        checkTotalWeight(person);

        System.out.println("\nDone! Have a nice day");
    }

    public void checkPassport(Person person) {
        if (person.getPassport().isActive() == true) {
            System.out.println("Passport Masih Active");
        } else {
            System.out.println("Passport mati");
            System.out.println("Silahkan urus passport terlebih dahulu");
            System.exit(0);
        }
    }

    public void checkTicket(Person person) {
        if ( (person.getTicket().getTujuan().equals(tujuan))
                && (person.getTicket().getTanggalKeberangkatan().equals(tanggalKeberangkatan))) {
            System.out.println("Tanggal atau Tujuan keberangkatan sesuai");
        } else {
            System.out.println("Tanggal atau Tujuan tidak sesuai");
            System.exit(0);
        }
    }

    public void checkWeightLaguage(Person person) {
        if (person.getLaguage().getLaguage() <= laguage ) {
            System.out.println("Berat koper dapat ditoleransi");
        } else  {
            System.out.println("Terlalu berat, silahkan kurangi berat koper");
            System.exit(0);
        }
    }

    public void checkTotalWeight(Person person) {
        int totalWeight = getTotalWeight(person);

        if ( totalWeight <= laguage) {
            System.out.println("Berat bawaan masih dapat ditoleransi");
        } else {
            System.out.println("Melebihi kapasitas, silahkan kurangi berat barang bawaan");
            System.exit(0);
        }
    }

    public int getTotalWeight(Person person) {
        return person.getLaguage().getWeightLaguage() + person.getLaguage().getWeightLaguage();
    }
}
