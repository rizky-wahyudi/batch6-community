public class Laguage {
    private int laguage;
    private int weightLaguage;

    public Laguage(int laguage, int weightLaguage) {
        this.laguage = laguage;
        this.weightLaguage = weightLaguage;
    }

    public int getLaguage() {
        return laguage;
    }

    public int getWeightLaguage() {
        return weightLaguage;
    }
}
