import java.time.LocalDate;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception{
        Passport passport1 = new Passport(2345678, false);
        Laguage laguage1 = new Laguage(10, 5);
        Ticket ticket1 = new Ticket(LocalDate.of(2021, 07, 28),"Semarang");

        Person person1 = new Person("Arif",passport1,ticket1,laguage1);

        Airport airport = new Airport(
                "Bandara Jogjakarta",
                "Semarang",
                LocalDate.now()
        );

        airport.getInfo();
        airport.checkPerson(person1);
    }
}
