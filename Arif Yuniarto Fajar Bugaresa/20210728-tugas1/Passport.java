public class Passport {
    private int number;
    private boolean active;

    public Passport(int number, boolean active) {
        this.number = number;
        this.active = active;
    }

    public int getNumber() {
        return number;
    }

    public boolean isActive() {
        return active;
    }
}
