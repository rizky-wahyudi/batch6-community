import java.time.LocalDate;

public class Ticket {
    private LocalDate tanggalKeberangkatan;
    private String tujuan;

    public Ticket(LocalDate tanggalKeberangkatan, String tujuan) {
        this.tanggalKeberangkatan = tanggalKeberangkatan;
        this.tujuan = tujuan;
    }

    public LocalDate getTanggalKeberangkatan() {
        return tanggalKeberangkatan;
    }

    public String getTujuan() {
        return tujuan;
    }
}
