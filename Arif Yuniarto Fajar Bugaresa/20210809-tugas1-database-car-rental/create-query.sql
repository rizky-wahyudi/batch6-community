create database car_rental;

use car_rental;

-- service
create table service( 
	id int not null auto_increment,
    insurance enum("yes","no"),
    weekday_price int,
    weekend_price int,
    holiday_price int,
    car_category_id int,
    car_gear_type enum("matic", "manual","electric"),
    primary key (id)
);

-- costumer
create table costumer(
	id int not null auto_increment,
    name varchar(100),
    phone varchar(100),
    email varchar(100),
    id_number varchar(100) unique,
    primary key (id)
);

create table address (
	id int not null auto_increment,
    costumer_id int,
    street varchar(100),
    district varchar(100),
    city varchar(100),
    province varchar(100),
    country varchar(100),
    longitude varchar(200),
    latitude varchar(200),
    primary key (id)
);

create table image_costumer (
	id int not null auto_increment,
    image_url longtext,
    primary key(id)
);

-- booking
create table booking(
	id int not null auto_increment,
    costumer_id int,
    car_id int,
    service_id int,
    pickup_location varchar(100),
    dropoff_location varchar(100),
    pickup_date date,
    dropoff_date date,
    total_price int,
    primary key(id)
);

-- car
create table car(
	id int not null auto_increment,
    brand_id int,
    category_id int,
    engine_size enum("1000cc", "1500cc", "2000cc"),
    fuel_type enum("pertalite", "pertamax", "solar"),
    gear_type enum("matic", "manual", "electric"),
    primary key(id)
);

create table car_images (
	id int not null auto_increment,
    car_id int,
    image_url longtext,
    status enum("active", "pending"),
    primary key (id)
);

create table brand (
	id int not null auto_increment,
    name varchar(1000),
    logo_url longtext,
    primary key (id)
);

create table category (
	id int not null auto_increment,
    type enum("hatckback", "sedan", "suv"),
    primary key (id)
);

create table current_location (
	id int not null auto_increment,
    car_id int,
    street varchar(100),
    district varchar(100),
    city varchar(100),
    province varchar(100),
    country varchar(100),
    longitude varchar(200),
    latitude varchar(200),
    primary key(id)
);


-- Relasi

-- costumer - address
alter table address
	add constraint fk_address_costumer
    foreign key (costumer_id) references costumer(id);
    
-- images - car
alter table car_images
	add constraint fk_car_images_car
    foreign key (car_id) references car(id);

-- car - brand
alter table car 
	add constraint fk_car_brand
    foreign key (brand_id) references brand(id);
    
-- car - category
alter table car 
	add constraint fk_car_category
    foreign key (category_id) references category(id);
    
-- current_location - car
alter table current_location
	add constraint fk_current_location_car
    foreign key ( car_id ) references car(id);
    
-- service - category
alter table service
	add constraint fk_service_category
    foreign key (car_category_id) references category(id);
    
-- booking - costumer
alter table booking 
	add constraint fk_booking_costumer_id
    foreign key (costumer_id) references costumer(id);
    
alter table booking 
	add constraint fk_booking_car
    foreign key (car_id) references car(id);
    
alter table booking
	add constraint fk_booking_service
    foreign key ( service_id ) references service(id);
    




    











    
    
    