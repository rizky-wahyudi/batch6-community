CREATE DATABASE lessons;

use lessons;

-- Create table
CREATE TABLE teacher (
	id INT NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(15),
    lastName VARCHAR(15),
    gender ENUM('L','P'),
    birthDate DATE,
    avatar VARCHAR(50),
    PRIMARY KEY(id)
);

CREATE TABLE teacherLessonSchedule (
	id INT NOT NULL AUTO_INCREMENT,
    teacherId INT,
    lessonId INT,
    teachingDate DATE,
    teachingTime TIME,
    PRIMARY KEY(id)
);

CREATE TABLE lessons (
	id INT NOT NULL AUTO_INCREMENT,
    subject VARCHAR(20),
    class ENUM('7','8','9'),
    PRIMARY KEY(id)
);

CREATE TABLE exam (
	id INT NOT NULL AUTO_INCREMENT,
    examDate DATE,
    lessonId INT,
    examTime TIME,
    PRIMARY KEY(id)
);

CREATE TABLE studentExamScore (
	id INT NOT NULL AUTO_INCREMENT,
    studentId INT,
    examId INT,
    score INT,
    PRIMARY KEY(id)
);

CREATE TABLE student (
	id INT NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(15),
    lastName VARCHAR(15),
    class ENUM('7','8','9'),
    gender ENUM('L','P'),
    avatar VARCHAR(50),
    PRIMARY KEY(id)
);

CREATE TABLE studentLessonSchedule (
	id INT NOT NULL AUTO_INCREMENT,
    studentId INT,
    lessonId INT,
    lessonDate DATE,
    lessonTime TIME,
    PRIMARY KEY(id)
);

-- Relasi
ALTER TABLE teacherLessonSchedule
	ADD CONSTRAINT fk_teacherLessonShcedule_teacher
		FOREIGN KEY (teacherId) REFERENCES teacher(id);

ALTER TABLE teacherLessonSchedule
	ADD CONSTRAINT fk_teacherLessonSchedule_lessons
		FOREIGN KEY (lessonId) REFERENCES lessons(id);
        
ALTER TABLE studentLessonSchedule
	ADD CONSTRAINT fk_studentLessonSchedule_student
		FOREIGN KEY (studentId) REFERENCES student(id);
        
ALTER TABLE studentLessonSchedule
	ADD CONSTRAINT fk_studentLessonSchedule_lessons
		FOREIGN KEY (lessonId) REFERENCES lessons(id);
    
ALTER TABLE exam
	ADD CONSTRAINT fk_exam_lessons
		FOREIGN KEY (lessonId) REFERENCES lessons(id);
        
ALTER TABLE studentExamScore
	ADD CONSTRAINT fk_studentExamScore_exam
		FOREIGN KEY (examId) REFERENCES exam(id);
        
ALTER TABLE studentExamScore
	ADD CONSTRAINT fk_studentExamScore_student
		FOREIGN KEY (studentId) REFERENCES student(id);