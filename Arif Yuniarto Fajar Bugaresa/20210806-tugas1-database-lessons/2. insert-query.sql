-- Teacher
INSERT INTO teacher(firstName, lastName, gender, birthDate, avatar)
	VALUES("Arif","Yuniarto","L", "1998-06-08", "src//img//teacher//arif.jpg"),
		("Fajar","Bugaresa","L", "1998-06-09", "src//img//teacher//fajar.jpg")
;

-- Lesson
INSERT INTO lessons(subject, class)
	VALUES("IPA","7"),
		("IPA","8"),
        ("IPA","9"),
        ("IPS","7"),
        ("IPS","8"),
        ("IPS","9")
;

INSERT INTO teacherLessonSchedule (teacherId, lessonId, teachingDate, teachingTime)
	VALUES("1", "1", "2021-08-09","07:00:00"),
		("1", "2", "2021-08-09","09:00:00"),
        ("1", "3", "2021-08-09","11:00:00"),
        ("2", "4", "2021-08-09","07:00:00"),
		("2", "5", "2021-08-09","09:00:00"),
        ("2", "6", "2021-08-09","11:00:00")
;

-- Student
INSERT INTO student (firstname, lastName, class, gender, avatar)
	VALUES ("Aldo","Martino", "7", "L", "src//img//student//aldo.jpg"),
		("Aulia","Majdina", "8", "P", "src//img//student//dina.jpg"),
        ("Wahyu","Hidayat", "9", "L", "src//img//student//wahyu.jpg")
;       

INSERT INTO studentLessonSchedule(studentId, lessonId, lessonDate, lessonTime)
	VALUES ( "1", "1", "2021-08-09", "07:00:00"),
    ( "1", "2", "2021-08-09", "07:00:00"),
    ( "2", "3", "2021-08-09", "09:00:00"),
    ( "2", "4", "2021-08-09", "09:00:00"),
    ( "3", "5", "2021-08-09", "11:00:00"),
    ( "3", "6", "2021-08-09", "11:00:00")
;

-- Exam
INSERT INTO exam(examDate, lessonId, examTime) 
	VALUES("2021-08-06","1","07:00:00"),
    ("2021-08-07","4","07:00:00")
;

INSERT INTO studentExamScore(studentId,examId,score)
	VALUES("1","1","90"),
		("1","2","50")
;