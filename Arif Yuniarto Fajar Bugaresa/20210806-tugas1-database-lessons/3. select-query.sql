-- Select Teacher
SELECT * FROM teacher as t
	JOIN teacherLessonSchedule as tls
    ON tls.teacherId = t.id
		JOIN  lessons as l
        ON l.id = tls.lessonId;
        
SELECT 
	tls.teachingDate 		as "Hari",
    tls.teachingTime 		as "Jam",
    l.subject 				as "Pelajaran",
    l.class 				as "Kelas",
    t.firstName 			as "Teacher"
FROM teacher as t
	JOIN teacherLessonSchedule as tls
    ON tls.teacherId = t.id
		JOIN  lessons as l
        ON l.id = tls.lessonId;

-- Select Student
SELECT * FROM student as s
		JOIN studentLessonSchedule as sls
		ON sls.studentId = s.id
			JOIN lessons as l
				on l.id = sls.lessonId;
                
-- Select Exam
SELECT * FROM student as s
		JOIN studentLessonSchedule as sls
		ON sls.studentId = s.id
			JOIN lessons as l
				on l.id = sls.lessonId
					JOIN exam as e
                    ON e.lessonId = l.id
						JOIN studentExamScore as ses
                        ON ses.examId = e.id;

SELECT 
	s.firstname 		as "Nama", 
	l.class 			as "Kelas", 
    l.subject			as "Pelajaran",
    e.examDate			as "Tanggal Ujian", 
    ses.score			as "Nilai" 
    FROM student as s
		JOIN studentLessonSchedule as sls
		ON sls.studentId = s.id
			JOIN lessons as l
				on l.id = sls.lessonId
					JOIN exam as e
                    ON e.lessonId = l.id
						JOIN studentExamScore as ses
                        ON ses.examId = e.id;