package controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FileController {

    public void makeFile(String fileName) {
        File file = new File(fileName);
        PrintWriter pw = null;

        try {
            pw = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        pw.println(fileName);

        pw.close();
    }

}
