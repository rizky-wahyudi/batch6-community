import jdk.swing.interop.SwingInterOpUtils;

public class WingController implements CommandController{

    private Wings wings;

    public static final String MOVE_UP = "MOVE_UP";
    public static final String MOVE_DOWN = "MOVE_UP";
    public static final String KEEP_FLAT = "KEP_FLAT";

    public WingController() {
        this.wings = new Wings();
    }

    @Override
    public void execute(String command) {
        switch (command) {
            case MOVE_UP:
                wings.moveUp();
                break;
            case MOVE_DOWN:
                wings.moveDown();
                break;
            case KEEP_FLAT:
                wings.keepFlat();
                break;
            default:
                System.out.println("No Command");
        }
    }

}
