public class Wings {

    public void moveUp() {
        System.out.println("Flight moving up");
    }

    public void moveDown() {
        System.out.println("Flight moving down");
    }

    public void keepFlat() {
        System.out.println("Flying flat");
    }
}
