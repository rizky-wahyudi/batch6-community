public class TommatoSauce extends ToppingDecorator{
    TommatoSauce(Pizza newPizza) {
        super(newPizza);

        System.out.println("Adding  Sauce");
    }

    @Override
    public String getDescription() {
        return tempPizza.getDescription() + ", TommatoSauce";
    }

    @Override
    public double getCost() {
        return tempPizza.getCost() + .35;
    }
}
