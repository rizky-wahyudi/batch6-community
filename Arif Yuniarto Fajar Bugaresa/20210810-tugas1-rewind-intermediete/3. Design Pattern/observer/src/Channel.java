import java.util.ArrayList;
import java.util.List;

public class Channel {
    private String title;
    private List<Subscriber> listSubscriber = new ArrayList<>();

    public void subscribe(Subscriber sub) {
        listSubscriber.add(sub);
    }

    public void unSubscribe(Subscriber sub) {
        listSubscriber.remove(sub);
    }

    public void notifySubscribers() {
        for (Subscriber s : listSubscriber) {
            s.update();
        }
    }

    public void upload(String title) {
        this.title = title;
        notifySubscribers();
    }
}
