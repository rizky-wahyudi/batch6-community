public class Subscriber {
    private String name;
    private Channel channel = new Channel();

    public Subscriber(String name) {
        this.name = name;
    }

    public void update() {
        System.out.println("Upload new Video");
    }

    public void subscribeChannel(Channel channel) {
        this.channel = channel;
    }

}
