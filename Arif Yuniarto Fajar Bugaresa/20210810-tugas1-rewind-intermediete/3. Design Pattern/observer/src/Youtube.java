public class Youtube {
    public static void main(String[] args) {

        Channel gadgetin = new Channel();

        Subscriber subscriber1 = new Subscriber("JOko");
        Subscriber subscriber2 = new Subscriber("andi");
        Subscriber subscriber3 = new Subscriber("dedi");
        Subscriber subscriber4 = new Subscriber("irfan");
        Subscriber subscriber5 = new Subscriber("leo");

        gadgetin.subscribe(subscriber1);
        gadgetin.subscribe(subscriber2);
        gadgetin.subscribe(subscriber3);
        gadgetin.subscribe(subscriber4);
        gadgetin.subscribe(subscriber5);

        subscriber1.subscribeChannel(gadgetin);
        subscriber2.subscribeChannel(gadgetin);
        subscriber3.subscribeChannel(gadgetin);
        subscriber4.subscribeChannel(gadgetin);
        subscriber5.subscribeChannel(gadgetin);

        gadgetin.upload("HAPE BADAK 6000mah");

    }
}
