package id.co.nexsoft;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    /**
     * TES METHOD 1 ADD
     */

    @Test
    @DisplayName("TES 1 ADD: Success Tambah 2 Angka")
    public void addTestSuccess1() {
        int result = Calculator.add(2,3);
        assertTrue(result == 5);
    }

    @Test
    @DisplayName("TES 2 ADD: Success Tambah 2 Angka")
    public void addTestSuccess2() {
        int result = Calculator.add(2,8);
        assertEquals(10, result);
    }

    @Test
    @DisplayName("TES 3 ADD: Failed assertFalse")
    public void addTestFailed1() {
        int result = Calculator.add(2,3);
        assertFalse(result == 2);
    }

    @Test
    @DisplayName("TES 4 ADD: Failed assertFalse")
    public void addTestFailed2() {
        int result = Calculator.add(3,3);
        assertFalse(result == 8);
    }

    @Test
    @DisplayName("TES 5 ADD: Failed Parameter String")
    public void addTestFailed3() {
        assertThrows(NumberFormatException.class,
                ()->{
                    int a = Integer.parseInt("S");
                    int b = Integer.parseInt("5");
                    Calculator.add(a,b);
                });
    }

    /**
     * TES METHOD 2 SUBSTRACT
     */

    @Test
    @DisplayName("TES 1 SUBSTRACT: Success Pengurangan 2 Angka")
    public void substractTestSuccess1() {
        int result = Calculator.subtract(5,3);
        assertTrue(result == 2);
    }

    @Test
    @DisplayName("TES 2 SUBSTRACT: Success Pengurangan 2 Angka")
    public void substractTestSuccess2() {
        int result = Calculator.subtract(10,8);
        assertEquals(2, result);
    }

    @Test
    @DisplayName("TES 3 SUBSTRACT: Failed assertFalse")
    public void substractTestFailed1() {
        int result = Calculator.subtract(10,3);
        assertFalse(result == 2);
    }

    @Test
    @DisplayName("TES 4 SUBSTRACT: Failed assertFalse")
    public void substractTestFailed2() {
        int result = Calculator.subtract(10,3);
        assertFalse(result == 30);
    }

    @Test
    @DisplayName("TES 5 SUBSTRACT: Failed Parameter String")
    public void substractTestFailed3() {
        assertThrows(NumberFormatException.class,
                ()->{
                    int a = Integer.parseInt("S");
                    int b = Integer.parseInt("5");
                    Calculator.subtract(a,b);
                });
    }


    /**
     * TES METHOD 3 MULTIPLY
     */

    @Test
    @DisplayName("TES 1 multiply: Success Perkalian 2 Angka")
    public void multiplyTestSuccess1() {
        int result = Calculator.multiply(2,3);
        assertTrue(result == 6);
    }

    @Test
    @DisplayName("TES 2 multiply: Success Perkalian 2 Angka")
    public void multiplyTestSuccess2() {
        int result = Calculator.multiply(2,8);
        assertEquals(16, result);
    }

    @Test
    @DisplayName("TES 3 multiply: Failed assertFalse")
    public void multiplyTestFailed1() {
        int result = Calculator.multiply(2,3);
        assertFalse(result == 2);
    }

    @Test
    @DisplayName("TES 4 multiply: Failed assertFalse")
    public void multiplyTestFailed2() {
        int result = Calculator.multiply(2,3);
        assertFalse(result == 5);
    }

    @Test
    @DisplayName("TES 5 multiply: Failed Parameter String")
    public void multiplyTestFailed3() {
        assertThrows(NumberFormatException.class,
                ()->{
                    int a = Integer.parseInt("S");
                    int b = Integer.parseInt("5");
                    Calculator.multiply(a,b);
                });
    }

    /**
     * TES METHOD 4 DIVIDE
     */

    @Test
    @DisplayName("TES 1 divide: Success Pembagian 2 Angka")
    public void divideTestSuccess1() {
        int result = Calculator.multiply(2,3);
        assertTrue(result == 6);
    }

    @Test
    @DisplayName("TES 2 divide: Success Pembagian 2 Angka")
    public void divideTestSuccess2() {
        int result = Calculator.multiply(2,8);
        assertEquals(16, result);
    }

    @Test
    @DisplayName("TES 3 divide: Failed assertFalse")
    public void divideTestFailed1() {
        int result = Calculator.multiply(2,3);
        assertFalse(result == 2);
    }

    @Test
    @DisplayName("TES 4 divide: Failed assertFalse")
    public void divideTestFailed2() {
        int result = Calculator.multiply(2,3);
        assertFalse(result == 5);
    }

    @Test
    @DisplayName("TES 5 divide : Failed dibagi 0 ")
    public void divideTestFailed3() {
        assertThrows(ArithmeticException.class,
                ()->{
                    Calculator.divide(2,0);
                });
    }

    /**
     * TES MAIN METHOD
     */

    @Test
    @DisplayName("TES 1 main : Failed parameter string")
    public void mainTest1() {
        assertThrows(NumberFormatException.class,
                ()->{
                    String[] args = {"S", "D"};
                    Calculator.main(args);
                });
    }

    @Test
    @DisplayName("TES 2 main : Failed parameter string and numeric")
    public void mainTest2() {
        assertThrows(NumberFormatException.class,
                ()->{
                    String[] args = {"S", "1"};
                    Calculator.main(args);
                });
    }

    @Test
    @DisplayName("TES 3 main : Success parameter string numeric")
    public void mainTest3() {
        String[] args = {"2", "1"};
        Calculator.main(args);
    }

}
