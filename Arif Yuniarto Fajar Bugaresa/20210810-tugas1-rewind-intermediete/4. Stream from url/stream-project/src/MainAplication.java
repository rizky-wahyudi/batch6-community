import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainAplication {
    public static void main(String[] args) {

        for (int i = 1; i <= 5; i++) {

            try {
                URL url = new URL("http://jsonplaceholder.typicode.com/todos/" + i);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                if (httpURLConnection.getResponseCode() == 200) {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    StringBuffer sb = new StringBuffer();
                    BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                    String line = br.readLine();

                    // make a new file
                    File myFile = new File("File-" + i + ".json");
                    if(!myFile.exists()){
                        myFile.createNewFile();
                    }

                    PrintWriter printWriter = new PrintWriter(myFile);

                    while (line != null) {
                        try {

                            // add text to file
                            printWriter.println(line);

                            System.out.println(line);
                            line = br.readLine();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    // close file
                    printWriter.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
