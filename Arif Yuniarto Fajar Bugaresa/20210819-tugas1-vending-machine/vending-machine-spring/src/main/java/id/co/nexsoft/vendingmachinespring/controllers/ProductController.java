package id.co.nexsoft.vendingmachinespring.controllers;

import id.co.nexsoft.vendingmachinespring.entities.Product;
import id.co.nexsoft.vendingmachinespring.request.Request;
import id.co.nexsoft.vendingmachinespring.repositories.ProductRepository;
import id.co.nexsoft.vendingmachinespring.response.CommonResponse;
import id.co.nexsoft.vendingmachinespring.response.CommonResponseGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CommonResponseGenerator commonResponseGenerator;

    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable(value = "id") int id) {
        return productRepository.findById(id);
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Product addProduct(@RequestBody Product product) {
        return productRepository.save(product);
    }

    @PutMapping("/product/{id}")
    public String updateProduct(@RequestBody Product product, @PathVariable int id) {

        Optional<Product> productRepo = Optional.ofNullable(productRepository.findById(id));

        if (!productRepo.isPresent())
            return "Product not found";

        product.setId(id);
        productRepository.save(product);
        return "Sukses update stok";
    }

    @PostMapping("/product/buy")
    @ResponseStatus(HttpStatus.CREATED)
    public CommonResponse<Product> getProduct(@RequestBody Request request) {

        int requestId = request.getId();
        Product chooseProduct = productRepository.findById(requestId);
        CommonResponse<Product> response;

        if (
                request.getStatus().equalsIgnoreCase("paid") && (chooseProduct.getStok() > 0 )
        ) {
            String message = "Sukses, Silahkan ambil " + chooseProduct.getNama() + " anda";

            int updateStok = chooseProduct.getStok() - 1;
            chooseProduct.setStok(updateStok);
            productRepository.save(chooseProduct);
            response = commonResponseGenerator.successResponse(chooseProduct, message);
        } else {
            String message = "Failed";
            response = commonResponseGenerator.failedResponse(message);
        }

        return response;
    }
}
