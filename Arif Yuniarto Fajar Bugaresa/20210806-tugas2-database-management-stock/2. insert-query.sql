-- INSERT

INSERT INTO address(street, district, city, province, country, longitute, latitude)
	VALUES ("Jl. Mandala I No.42, RT.02/RW.01", "Mandalajati", "Bandung", "Jawa Barat", "Indonesia", "107.3201671", "-6.7749753"),
		("Jl. Semarang Indah C 17 No.1", "Tawangmas", "Semarang", "Jawa Tengah", "Indonesia", "107.320167", "-6.7749753"),
		("Jalan Sariwates ", "Antapani", "Pontianak", "Kalimantan", "Indonesia", "107.320986", "-6.77497534")
;

INSERT INTO distributor(name,address_id,address_type) 
	VALUES("GROSIR BANDUNG", 1, "Residential"),
		("GROSIR SEMARANG", 2, "Residential"),
        ("GROSIR KALIMANTAN", 1, "Residential")
;

INSERT INTO product_distributor(distributor_id, buy_price, stock)
	VALUES(1,20000,10),
		(1,30000,0),
        (1,25000,10),
        (1,40000,10),
        (1,26000,10),
        (2,50000,0),
        (2,40000,0),
        (2,23000,0),
        (3,25000,5),
        (3,68000,5)
;

INSERT INTO product_image(url, description) 
	VALUES("https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60", "img-silverqueen"),
		("https://images.unsplash.com/photo-1476718406336-bb5a9690ee2a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60", "img-roma-biskuit"),
        ("https://images.unsplash.com/photo-1512621776951-a57141f2eefd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80", "img-tissu-paseo"),
        ("https://images.unsplash.com/photo-1484723091739-30a097e8f929?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60", "img-dove"),
        ("https://images.unsplash.com/photo-1497034825429-c343d7c6a68f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTl8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60", "img-pepsodent"),
        ("https://images.unsplash.com/photo-1606787620651-3f8e15e00662?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80", "img-ciptandent"),
        ("https://images.unsplash.com/photo-1484980972926-edee96e0960d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjN8fGZvb2R8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60", "img-sunsilk"),
        ("https://images.unsplash.com/photo-1495147466023-ac5c588e2e94?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjV8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60", "img-coki-coki"),
        ("https://images.unsplash.com/photo-1478145046317-39f10e56b5e9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzZ8fGZvb2R8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60", "img-harmoni"),
        ("https://images.unsplash.com/photo-1475090169767-40ed8d18f67d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NDJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60", "img-makarizo")
;

INSERT INTO product(product_image_id, art_number, sell_price, expired_date)
	VALUES(1, "SQ001", 70000, "2022-08-01"),
		(2, "RM001", 740000, "2022-08-02"),
        (3, "PA001", 66000, "2022-08-03"),
        (4, "DO001", 780000, "2022-08-04"),
        (5, "PE001", 98000, "2022-08-05"),
        (6, "CI001", 65000, "2022-08-06"),
        (7, "SU001", 93000, "2022-08-07"),
        (8, "CO001", 79000, "2022-08-08"),
        (9, "HA001", 84000, "2022-08-09"),
        (10, "MA001", 69000, "2022-08-10")
;

INSERT INTO product_stock(product_id, product_distributor_id, purchase_date, delivery_date, stok)
	VALUES (1, 1, "2021-01-01", "2021-02-01", 0),
    (2, 2, "2021-01-02", "2021-02-02", 10),
    (3, 3, "2021-01-03", "2021-02-03", 10),
    (4, 1, "2021-01-04", "2021-02-04", 10),
    (5, 3, "2021-01-05", "2021-02-05", 0),
    (6, 3, "2021-01-06", "2021-02-06", 5),
    (7, 1, "2021-01-07", "2021-02-07", 5),
    (8, 2, "2021-01-08", "2021-02-08", 0),
    (9, 3, "2021-01-09", "2021-02-09", 5),
    (10, 2, "2021-01-10", "2021-02-10", 0)
;
    