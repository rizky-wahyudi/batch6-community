-- query tampilkan data 
-- product, productStock & distributor yang dimana stock dari product adalah 0.

SELECT
	ps.stok				as "Stok",
	p.art_number		as "Kode Produk",
    p.sell_price		as "Harga Jual",
    pd.buy_price		as "Harga Beli",
    pi.url				as "Gambar",
    d.name				as "Distributor",
    a.district			as "Alamat"    
FROM product as p
	JOIN product_stock as ps
    ON ps.product_id = p.id
		JOIN product_image as pi
		ON pi.id = p.product_image_id
			JOIN product_distributor as pd
            ON pd.id = ps.product_distributor_id
				JOIN distributor as d
                ON d.id = pd.distributor_id
					JOIN address as a
                    ON a.id = d.address_id
WHERE ps.stok = 0;

select * from product
	join product_stock
    on product.id = product_stock.product_id;