CREATE DATABASE management_stock;

use management_stock;

CREATE TABLE address(
	id INT NOT NULL AUTO_INCREMENT,
    street VARCHAR(100),
    longitute VARCHAR(100),
    latitude VARCHAR(100),
    city VARCHAR(50),
    district VARCHAR(50),
    province VARCHAR(50),
    country VARCHAR(50),
    PRIMARY KEY(id)
);

CREATE TABLE distributor(
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    address_id INT,
    address_type ENUM("Business","Residential"),
    PRIMARY KEY(id)
);

CREATE TABLE product_distributor(
	id INT NOT NULL AUTO_INCREMENT,
    distributor_id INT,
    buy_price INT,
    stock INT,
    PRIMARY KEY(id)
);

CREATE TABLE product_stock(
	id INT NOT NULL AUTO_INCREMENT,
	product_id INT,
    product_distributor_id INT,
    purchase_date DATETIME,
    delivery_date DATETIME,
    stok INT,
    PRIMARY KEY(id)
);

CREATE TABLE product(
	id INT NOT NULL AUTO_INCREMENT,
    product_image_id INT,
    art_number VARCHAR(20) UNIQUE,
    sell_price INT,
    expired_date DATETIME,
    PRIMARY KEY(id)
);

CREATE TABLE product_image(
	id INT NOT NULL AUTO_INCREMENT,
    url VARCHAR(200),
    description VARCHAR(50),
    PRIMARY KEY(id)
);

-- relasi
ALTER TABLE distributor
	ADD CONSTRAINT fk_distributor_address 
    FOREIGN KEY (address_id) REFERENCES address(id)
;

ALTER TABLE product_distributor
	ADD CONSTRAINT fk_product_distributor_distributor
    FOREIGN KEY (distributor_id) REFERENCES distributor(id)
;

ALTER TABLE product_stock
	ADD CONSTRAINT fk_product_stock_product_distributor
    FOREIGN KEY (product_distributor_id) REFERENCES product_distributor(id),
    ADD CONSTRAINT fk_product_stock_product
    FOREIGN KEY (product_id) REFERENCES product(id)
;


ALTER TABLE product
	ADD CONSTRAINT fk_product_image_id_product_image
    FOREIGN KEY (product_image_id) REFERENCES product_image(id)
;


    
	
    