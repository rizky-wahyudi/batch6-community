package Model;

public class Cashier {
    String name;

    public Cashier(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
