package Model;

import java.time.LocalDate;

public class Product implements Tax{
    String name;
    String articleNumber;
    String description;
    double purchasePrice;
    String Image;
    LocalDate expiredDate;
    ProductCategory productCategory;
    double tax;

    public Product(
            String name,
            String articleNumber,
            String description,
            double purchasePrice,
            String image,
            LocalDate expiredDate,
            ProductCategory productCategory
    ) {
        this.name = name;
        this.articleNumber = articleNumber;
        this.description = description;
        this.purchasePrice = purchasePrice;
        Image = image;
        this.expiredDate = expiredDate;
        this.productCategory = productCategory;
    }

    public String getName() {
        return name;
    }

    public String getArticleNumber() {
        return articleNumber;
    }

    public String getDescription() {
        return description;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public double getSellPrice() {
        return purchasePrice + (0.1 * purchasePrice);
    }

    public String getImage() {
        return Image;
    }

    public LocalDate getExpiredDate() {
        return expiredDate;
    }

    // Calculate Tax
    public double getTax(){
        switch (productCategory) {
            case FOOD:
                this.tax = TAX_FOOD * getSellPrice();
                break;
            case DRINK:
                this.tax = TAX_DRINK * getSellPrice();
                break;
            default:
                this.tax = TAX_SOAP * getSellPrice();
                break;
        }
        return tax;
    }
}
