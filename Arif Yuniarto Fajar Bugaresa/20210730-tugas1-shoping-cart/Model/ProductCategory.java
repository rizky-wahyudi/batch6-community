package Model;

public enum ProductCategory {
    FOOD, DRINK, SOAP;
}
