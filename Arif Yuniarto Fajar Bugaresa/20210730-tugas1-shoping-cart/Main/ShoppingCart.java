package Main;

import Model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class ShoppingCart {
    public static void main(String[] args) {

        boolean beliLagi = true;
        Product productPilihan = null;

        int orderNumber = 1;
        int orderId = 1;

        ProductCategory makanan = ProductCategory.FOOD;
        ProductCategory minuman = ProductCategory.DRINK;
        ProductCategory sabun = ProductCategory.SOAP;

        StatusOrder delivered = StatusOrder.DELIVERED;
        StatusOrder ordered = StatusOrder.ORDERED;

        Scanner masukan = new Scanner(System.in);
        Scanner masukanString = new Scanner(System.in);

        PaymentStatus paid = PaymentStatus.PAID;
        PaymentStatus unpaid = PaymentStatus.UNPAID;

        Order order = new Order();

        // save multiple orderItem
        ArrayList<OrderItem> orderItems = new ArrayList<OrderItem>();

        Cashier cashier1 = new Cashier("Arif");

        Product makanan1 = new Product(
                "SilverQueen",
                "SQ123",
                "Silverqueen adalah cokelat terenak",
                2000,
                "images/makanan/silverqueen.jpg",
                LocalDate.of(2022,12,30),
                makanan
        );

        Product makanan2 = new Product(
                "Chitato",
                "CH123",
                "Chitato adalah snack micin terlezat",
                1000,
                "images/makanan/chitato.jpg",
                LocalDate.of(2021,1,12),
                makanan
        );

        Product minuman1 = new Product(
                "Coca-Cola",
                "CC123",
                "Coca Cola adalah minuman soda termantap",
                500,
                "images/minuman/coca-cola.jpg",
                LocalDate.of(2020, 1, 6),
                minuman
        );

        Product sabun1 = new Product(
                "Detol",
                "DT123",
                "Detol adalah sabun paling higienis",
                300,
                "images/sabun/detol.jpg",
                LocalDate.of(2020,1,3),
                sabun
        );

        System.out.println("Selamat datang di Family Mart");
        while (beliLagi == true) {

            //display product
            System.out.println("\nDaftar Product");
            System.out.println("1. " + makanan1.getName());
            System.out.println("2. " + makanan2.getName());
            System.out.println("3. " + minuman1.getName());
            System.out.println("4. " + sabun1.getName());

            System.out.println("Mau beli yang mana? ");
            System.out.print("Masukan: ");

            int beli = masukan.nextInt();

            switch (beli) {
                case 1:
                    System.out.println("Oke, berhasil pilih barang");
                    System.out.println("\nPilihan kamu: " + makanan1.getName());
                    productPilihan = makanan1;
                    break;
                case 2:
                    System.out.println("Oke, berhasil pilih barang");
                    System.out.println("\nPilihan kamu: " + makanan2.getName());
                    productPilihan = makanan2;
                    break;
                case 3:
                    System.out.println("Oke, berhasil pilih barang");
                    System.out.println("\nPilihan kamu: " + minuman1.getName());
                    productPilihan = minuman1;
                    break;
                case 4:
                    System.out.println("Oke, berhasil pilih barang");
                    System.out.println("\nPilihan kamu: " + sabun1.getName());
                    productPilihan = sabun1;
                    break;
                default:
                    System.out.println("Masukan salah");
                    System.exit(0);
            }

            System.out.print("Berapa jumlahnya?: ");
            int jumlahMakanan = masukan.nextInt();

            OrderItem orderItem = new OrderItem();

            processOrderItem(
                    productPilihan,
                    jumlahMakanan,
                    orderId,
                    orderItem,
                    orderItems
            );


            System.out.println("\nKeranjang belanja kamu:");
            for(OrderItem ot: orderItems){
                System.out.println(ot.getAmount() + " pcs " + ot.getProduct().getName());
            }

            System.out.println("\nmau beli lagi? [y/n]");
            System.out.print("Masukan: ");
            String isBeliLagi = masukanString.nextLine();

            if (isBeliLagi.equals("y")) {
                beliLagi = true;
            } else {
                beliLagi = false;
            }
        }

        processOrder(
                order,
                cashier1,
                orderNumber,
                ordered,
                unpaid,
                orderItems
        );

        System.out.println("\nOke, selesai pilih barang");

        getLine();

        getTagihan(order);

        getLine();

        System.out.println("Mau lanjut bayar?");
        System.out.print("Masukan [y/n]: ");
        String bayarSekarang = masukanString.nextLine();

        if (bayarSekarang.equals("y")) {
            processBayar(order, paid, delivered);
        } else {
            getLine();
            System.out.println("Transaksi dibatalkan. \nTerimakasih");
            System.exit(0);
        }

        getLine();
        getNotaTransaksi(order);


    }

    public static void getTagihan(Order order) {
        System.out.println("Sekarang selesaikan pembayaran");
        System.out.println("\nTagihan kamu");
        System.out.println("Cashier: " + order.getCashier().getName());
        System.out.println("Order date: " + order.getOrderDate());
        System.out.println("\nOrder Items");
        for(OrderItem ot: order.getOrderItems()){
            System.out.println(ot.getAmount() + " pcs " + ot.getProduct().getName()
                    + "(Harga: " + ot.getProduct().getSellPrice() + " , "
                    + "Pajak: " + ot.getProduct().getTax()
            );
        }
        System.out.println("\nTotal Price: " + order.getTotalPrice());
        System.out.println("Status: " + order.getStatusOrder());
        System.out.println("Payment Status: " + order.getPaymentStatus());
    }

    public static void getLine(){
        System.out.println("\n====================================\n");
    }

    public static void getNotaTransaksi(Order order) {
        System.out.println("Nota Transaksi");
        System.out.println("Cashier: " + order.getCashier().getName());
        System.out.println("Order date: " + order.getOrderDate());
        System.out.println("\nOrder Items");
        for(OrderItem ot: order.getOrderItems()){
            System.out.println(ot.getAmount() + " pcs " + ot.getProduct().getName()
            + "(Harga: " + ot.getProduct().getSellPrice() + " , "
            + "Pajak: " + ot.getProduct().getTax()
            );
        }
        System.out.println("\nTotal Price: " + order.getTotalPrice());
        System.out.println("Status: " + order.getStatusOrder());
        System.out.println("Payment Status: " + order.getPaymentStatus());

        System.out.println("\n Transaksi selesai.");
        System.out.println("Silahkan ambil pesanan anda, Terimakasih");
    }

    public static void processBayar(Order order, PaymentStatus paid, StatusOrder delivered) {
        order.setStatusOrder(delivered);
        order.setPaymentStatus(paid);
    }

    public static void processOrderItem(
            Product product,
            int jumlahMakanan,
            int orderId,
            OrderItem orderItem,
            ArrayList<OrderItem> orderItems
    ) {

        orderItem.setProduct(product);
        orderItem.setOrderId(orderId);
        orderItem.setAmount(jumlahMakanan);

        orderItems.add(orderItem);

        System.out.println("Oke poduct berhasil dipilih");
    }


    public static void processOrder(
            Order order,
            Cashier cashier,
            int orderNumber,
            StatusOrder statusOrder,
            PaymentStatus paymentStatus,
            ArrayList<OrderItem> orderItem
    ){
        order.setOrderDate(LocalDate.now());
        order.setCashier(cashier);
        order.setOrderNumber(orderNumber);
        order.setStatusOrder(statusOrder);
        order.setPaymentStatus(paymentStatus);
        order.setOrderItem(orderItem);

        double totalPrice = 0;

        for(OrderItem oi: order.getOrderItems()){
            double sellPrice = oi.getProduct().getSellPrice();
            double tax = oi.getProduct().getTax();
            double totalProductPrice = sellPrice + tax;
            totalPrice += totalProductPrice;
        }

        order.setTotalPrice(totalPrice);
    }
}
