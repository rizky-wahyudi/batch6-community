public class Barang extends VendingMachine{
    private int baris;
    private int kolom;
    private String nama;
    private int harga;


    public Barang(int baris, int kolom, String nama, int harga) {
        this.baris = baris;
        this.kolom = kolom;
        this.nama = nama;
        this.harga = harga;
    }

    public int getBaris() {
        return baris;
    }

    public int getKolom() {
        return kolom;
    }

    public String getNama() {
        return nama;
    }

    public int getHarga() {
        return harga;
    }
}
