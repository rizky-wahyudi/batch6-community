import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ArrayList<Barang> barang = new ArrayList<Barang>();

        for (int i = 1; i <= 10; i++) {
            barang.add(new Barang(1, i, " CHIPS", 8000));
        }
        for (int i = 1; i <= 10; i++) {
            barang.add(new Barang(2, i, " CHOCO", 8000));
        }
        for (int i = 1; i <= 10; i++) {
            barang.add(new Barang(3, i, " CANDY", 8000));
        }
        for (int i = 1; i <= 10; i++) {
            barang.add(new Barang(4, i, " WATER", 8000));
        }
        for (int i = 1; i <= 10; i++) {
            barang.add(new Barang(5, i, " DRINK", 8000));
        }
        for (int i = 1; i <= 10; i++) {
            barang.add(new Barang(6, i, " TEA  ", 8000));
        }

        VendingMachine vm = new VendingMachine();
        vm.setBarang(barang);
        Scanner scanner = new Scanner(System.in);

        ArrayList<Barang> barangVm = vm.getBarang();

        //Display Product
        for (Barang data : barangVm){
            if (data.getKolom() == 10) {
                System.out.print(data.getBaris());
                System.out.print(data.getKolom());
                System.out.print(data.getNama() + "\t");
                System.out.println();
            } else {
                System.out.print(data.getBaris());
                System.out.print(data.getKolom());
                System.out.print(data.getNama() + "\t");
            }
        }

        //Seleksi Produk
        System.out.println("Mau beli yang mana? ");
        System.out.print("Baris: ");
        int pilihBaris = scanner.nextInt();
        System.out.print("Kolom: ");
        int pilihKolom = scanner.nextInt();

        for (Barang b : barangVm){

            //barang pilihan tp belum ada else
            if((pilihBaris == b.getBaris()) && (pilihKolom == b.getKolom())) {
                System.out.println("Pilihan Anda");
                System.out.println("\nBarang \t: "+ b.getNama() + b.getBaris() +  b.getKolom() );
                System.out.println("Harga \t: " + b.getHarga());

                boolean bisaBayar = false;
                do {
                    System.out.println("Masukkan uang anda: ");
                    int inputUang = scanner.nextInt();

                    System.out.println(inputUang);
                    System.out.println(b.getHarga());

                    //cek apakah uang memenuhi buat bayar
                    if (inputUang >= b.getHarga()) {
                        bisaBayar = true;
                    } else {
                        System.out.println("Uang yang anda masukkan kurang");
                        System.out.println("Mau cancel? [y/n]: ");

                        Scanner inputCancel = new Scanner(System.in);
                        String isCancel = inputCancel.nextLine();

                        if (isCancel.equals("y")) {
                            System.out.println("Oke, Anda memilih Cancel");
                            System.exit(0);
                        } else {
                            System.out.println("Silahkan ambil uang Anda dan ulangi");

                            
                        }
                    }
                }  while (bisaBayar = true);


            }
        }

    }


}
