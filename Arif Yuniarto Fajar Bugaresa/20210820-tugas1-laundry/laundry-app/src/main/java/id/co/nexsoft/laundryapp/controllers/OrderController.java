package id.co.nexsoft.laundryapp.controllers;

import id.co.nexsoft.laundryapp.entities.Orderan;
import id.co.nexsoft.laundryapp.entities.OrderItem;
import id.co.nexsoft.laundryapp.repositories.OrderItemRepository;
import id.co.nexsoft.laundryapp.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @GetMapping(value = "/orderan")
    public List<Orderan> getAllOrder() {
        return orderRepository.findAll();
    }

    @GetMapping(value = "orderan/{id}")
    public Orderan getOrderanById(@PathVariable int id) {
        return orderRepository.findById(id);
    }

    @PostMapping(value = "/orderan")
    public Orderan addOrderan(@RequestBody Orderan orderan) {
        Iterable<OrderItem> orderItemList = orderan.getOrderItems();
        orderItemRepository.saveAll(orderItemList);
        return orderRepository.save(orderan);
    }

    @PutMapping("/orderan/{id}")
    public ResponseEntity<Object> updateOrderan(@RequestBody Orderan order, @PathVariable int id) {

        Optional<Orderan> orderanOptional = Optional.ofNullable(orderRepository.findById(id));

        if (!orderanOptional.isPresent())
            return ResponseEntity.notFound().build();

        order.setId(id);
        orderRepository.save(order);
        return ResponseEntity.noContent().build();
    }
}
