package id.co.nexsoft.laundryapp.controllers;

import id.co.nexsoft.laundryapp.entities.Service;
import id.co.nexsoft.laundryapp.repositories.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServiceController {

    @Autowired
    ServiceRepository serviceRepository;

    @GetMapping(value = "/service")
    public List<Service> getAllService() {
        return serviceRepository.findAll();
    }

    @GetMapping(value = "/service/{id}")
    public Service getServiceById(@PathVariable int id) {
        return serviceRepository.findById(id);
    }

    @PostMapping(value = "/service")
    public Iterable<Service> addService(@RequestBody List<Service> service) {
        return serviceRepository.saveAll(service);
    }
}
