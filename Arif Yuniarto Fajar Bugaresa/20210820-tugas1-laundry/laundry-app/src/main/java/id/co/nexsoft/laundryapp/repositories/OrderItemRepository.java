package id.co.nexsoft.laundryapp.repositories;

import id.co.nexsoft.laundryapp.entities.OrderItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends CrudRepository<OrderItem, Integer> {
    OrderItem findById(int id);
    List<OrderItem> findAll();
    void deleteById(int id);
}
