package id.co.nexsoft.laundryapp.controllers;

import id.co.nexsoft.laundryapp.entities.OrderItem;
import id.co.nexsoft.laundryapp.repositories.OrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderItemController {

    @Autowired
    OrderItemRepository orderItemRepository;

    @GetMapping(value = "/orderItem/{id}")
    public OrderItem getOrderItemById(@PathVariable int id) {
        return orderItemRepository.findById(id);
    }

    @GetMapping(value = "/orderItem")
    public List<OrderItem> getAllOrderItem(){
        return orderItemRepository.findAll();
    }

    @PostMapping(value = "/orderItem")
    public OrderItem addOrderItem(@RequestBody OrderItem orderItem) {
        return orderItemRepository.save(orderItem);
    }
}
