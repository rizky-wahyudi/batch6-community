package id.co.nexsoft.laundryapp.repositories;

import id.co.nexsoft.laundryapp.entities.Costumer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CostumerRepository extends CrudRepository<Costumer, Integer> {
    Costumer findById(int id);
    List<Costumer> findAll();
    void deleteById(int id);
}
