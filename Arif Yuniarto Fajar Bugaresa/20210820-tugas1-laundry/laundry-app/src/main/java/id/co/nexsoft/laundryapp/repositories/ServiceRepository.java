package id.co.nexsoft.laundryapp.repositories;

import id.co.nexsoft.laundryapp.entities.Costumer;
import id.co.nexsoft.laundryapp.entities.Service;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends CrudRepository<Service, Integer> {
    Service findById(int id);
    List<Service> findAll();
    void deleteById(int id);
}
