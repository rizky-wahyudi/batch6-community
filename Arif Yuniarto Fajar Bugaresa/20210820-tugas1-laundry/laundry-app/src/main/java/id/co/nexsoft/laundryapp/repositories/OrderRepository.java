package id.co.nexsoft.laundryapp.repositories;

import id.co.nexsoft.laundryapp.entities.Orderan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Orderan, Integer> {
    Orderan findById(int id);
    List<Orderan> findAll();
}
