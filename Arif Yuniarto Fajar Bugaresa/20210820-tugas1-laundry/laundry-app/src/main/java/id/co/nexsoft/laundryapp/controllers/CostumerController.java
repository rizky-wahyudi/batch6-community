package id.co.nexsoft.laundryapp.controllers;

import id.co.nexsoft.laundryapp.entities.Address;
import id.co.nexsoft.laundryapp.entities.Costumer;
import id.co.nexsoft.laundryapp.repositories.AddressRepository;
import id.co.nexsoft.laundryapp.repositories.CostumerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CostumerController {

    @Autowired
    CostumerRepository costumerRepository;

    @Autowired
    AddressRepository addressRepository;

    @GetMapping(value = "/costumer")
    public List<Costumer> getAllCostumer() {
        return costumerRepository.findAll();
    }

    @GetMapping(value = "/costumer/{id}")
    public Costumer getCostumerById(@PathVariable int id) {
        return costumerRepository.findById(id);
    }

    @PostMapping(value = "/costumer")
    public Costumer addCustomerWithAddress(@RequestBody Costumer costumer) {
        Address address = costumer.getAddress();
        addressRepository.save(address);
        costumerRepository.save(costumer);

        return costumer;
    }
}
