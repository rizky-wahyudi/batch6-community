package id.co.nexsoft.laundryapp.repositories;

import id.co.nexsoft.laundryapp.entities.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {
}
