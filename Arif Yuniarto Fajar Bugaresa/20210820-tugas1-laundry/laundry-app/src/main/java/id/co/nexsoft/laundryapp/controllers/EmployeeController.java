package id.co.nexsoft.laundryapp.controllers;

import id.co.nexsoft.laundryapp.entities.Address;
import id.co.nexsoft.laundryapp.entities.Employee;
import id.co.nexsoft.laundryapp.repositories.AddressRepository;
import id.co.nexsoft.laundryapp.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    AddressRepository addressRepository;

    @GetMapping(value = "/employee")
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @GetMapping(value = "/employee/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeRepository.findById(id);
    }

    @PostMapping(value = "/employee")
    public Employee addEmployeeWithAddress(@RequestBody Employee employee) {
        Address address = employee.getAddress();
        addressRepository.save(address);
        employeeRepository.save(employee);

        return employee;
    }

}
