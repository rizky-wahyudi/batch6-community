public class FirstClass extends Airplane{

    public double getPriceAsiaEurope() {
        return super.getAsiaEurope()*3;
    }

    public double getPriceEuropeAsia() {
        return super.getEuropeAsia()*3;
    }

    public double getPriceEuropeUs() {
        return super.getEuropeUs()*3;
    }

    public double getMaxLaguage() {
        return super.getLaguage()*2.5;
    }
}
