public class BusinessClass extends Airplane{

    public double getPriceAsiaEurope() {
        return super.getAsiaEurope()*2.25;
    }

    public double getPriceEuropeAsia() {
        return super.getEuropeAsia()*2.25;
    }

    public double getPriceEuropeUs() {
        return super.getEuropeUs()*2.25;
    }

    public double getMaxLaguage() {
        return super.getLaguage()*2;
    }
}
