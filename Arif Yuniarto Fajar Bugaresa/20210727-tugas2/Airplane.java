public class Airplane{
    double europeAsia = 500;
    double asiaEurope = 600;
    double europeUs = 650;
    double laguage = 20;

    public double getEuropeAsia() {
        return europeAsia;
    }

    public double getAsiaEurope() {
        return asiaEurope;
    }

    public double getEuropeUs() {
        return europeUs;
    }

    public double getLaguage() {
        return laguage;
    }
}
