import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception{

        Scanner input = new Scanner(System.in);
        Scanner namaInput = new Scanner(System.in);

        System.out.println("Booking Tiket Pesawat");
        System.out.print("Nama Penumpang = ");
        String name = namaInput.nextLine();
        System.out.println("Processing...");
        Thread.sleep(1000);

        System.out.println("\nPerjalanan");
        System.out.println("1. Europe - Asia");
        System.out.println("2. Asia - Europe");
        System.out.println("3. Europe - Us");
        System.out.print("Silahkan Pilih Perjalanan = ");
        int trip = input.nextInt();

        if (trip < 1 || trip > 3) {
            System.out.println("Pilihan Diluar Jangkauan");
            System.exit(0);
        } else {
            System.out.println("Processing...");
        }

        Thread.sleep(1000);

        System.out.println("\nPilihan Kelas Penerbangan");
        System.out.println("1. Ekonomi");
        System.out.println("2. Bisnis");
        System.out.println("3. Fist Class");
        System.out.print("Pilih Kelas Penerbangan = ");
        int kelas = input.nextInt();

        if (kelas < 1 || kelas > 3) {
            System.out.println("Pilihan Diluar Jangkauan");
            System.exit(0);
        } else {
            System.out.println("Processing...");
        }

        Thread.sleep(1000);

        System.out.println("\nPilihan Tipe Pesawat");
        System.out.println("1. AirBus 380");
        System.out.println("2. Boeing 747");
        System.out.println("3. Boeing 787");
        System.out.print("Silahkan pilih tipe pesawat = ");
        int type = input.nextInt();

        if (type < 1 || type > 3) {
            System.out.println("Pilihan Diluar Jangkauan");
            System.exit(0);
        } else {
            System.out.println("Processing...");
        }

        Thread.sleep(1000);

        Calculate calculate = new Calculate(trip,kelas,type,name);
        calculate.Calculate();

        System.out.println("\nDone. Have A Nice Trip!\n");
    }

}
