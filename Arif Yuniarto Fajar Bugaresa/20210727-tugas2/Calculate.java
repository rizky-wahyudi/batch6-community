public class Calculate implements Seats{
    int trip, kelas, type;
    String name;

    public Calculate(int trip, int kelas, int type, String name) {
        this.trip = trip;
        this.kelas = kelas;
        this.type = type;
        this.name = name;
    }


    void Calculate() {
        System.out.println("\nDetail Perjalanan");
        System.out.println("Nama Penumpang = " + name);
        if ( trip == 1) {
            System.out.println("Trip: Europe - Asia");
            // initialized
            Airplane kelasPenerbangan = new Airplane();

            if (kelas == 1) {
                System.out.println("Kelas: Ekonomi");
                kelasPenerbangan = new EconomyClass();

                if (type == 1) {
                    double Seats = airbus380 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = airbus380 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = airbus380 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }

            } else if (kelas == 2) {
                System.out.println("Kelas: Bisnis");
                kelasPenerbangan = new BusinessClass();

                if (type == 1) {
                    double Seats = boeing747 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = boeing747 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = boeing747 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }

            } else if (kelas == 3) {
                System.out.println("Kelas: First Class");
                kelasPenerbangan = new FirstClass();

                if (type == 1) {
                    double Seats = boeing787 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = boeing787 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = boeing787 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }
            } else {
                System.out.println("Pilihan diluar jangkauan");
            }

            // convert sesuai pilihan
            if (kelasPenerbangan instanceof EconomyClass) {
                EconomyClass kelasPenerbanganConvert = (EconomyClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceEuropeAsia());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else if (kelasPenerbangan instanceof BusinessClass) {
                BusinessClass kelasPenerbanganConvert = (BusinessClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceEuropeAsia());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else if (kelasPenerbangan instanceof FirstClass) {
                FirstClass kelasPenerbanganConvert = (FirstClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceEuropeAsia());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else {
                System.out.println("ERROR CONVERT PILIHAN");
            }
        }
        else if ( trip == 2) {
            System.out.println("Trip: Asia - Europe");

            // initialized
            Airplane kelasPenerbangan = new Airplane();

            if (kelas == 1) {
                System.out.println("Kelas: Ekonomi");
                kelasPenerbangan = new EconomyClass();

                if (type == 1) {
                    double Seats = airbus380 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = airbus380 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = airbus380 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }

            } else if (kelas == 2) {
                System.out.println("Kelas: Bisnis");
                kelasPenerbangan = new BusinessClass();

                if (type == 1) {
                    double Seats = boeing747 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = boeing747 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = boeing747 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }

            } else if (kelas == 3) {
                System.out.println("Kelas: First Class");
                kelasPenerbangan = new FirstClass();

                if (type == 1) {
                    double Seats = boeing787 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = boeing787 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = boeing787 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }
            } else {
                System.out.println("Pilihan diluar jangkauan");
            }

            // convert sesuai pilihan
            if (kelasPenerbangan instanceof EconomyClass) {
                EconomyClass kelasPenerbanganConvert = (EconomyClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceAsiaEurope());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else if (kelasPenerbangan instanceof BusinessClass) {
                BusinessClass kelasPenerbanganConvert = (BusinessClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceAsiaEurope());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else if (kelasPenerbangan instanceof FirstClass) {
                FirstClass kelasPenerbanganConvert = (FirstClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceAsiaEurope());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else {
                System.out.println("ERROR CONVERT PILIHAN");
            }
        }
        else if ( trip == 3) {
            System.out.println("Trip: Europe - US");

            // initialized
            Airplane kelasPenerbangan = new Airplane();

            if (kelas == 1) {
                System.out.println("Kelas: Ekonomi");
                kelasPenerbangan = new EconomyClass();

                if (type == 1) {
                    double Seats = airbus380 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = airbus380 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = airbus380 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }

            } else if (kelas == 2) {
                System.out.println("Kelas: Bisnis");
                kelasPenerbangan = new BusinessClass();

                if (type == 1) {
                    double Seats = boeing747 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = boeing747 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = boeing747 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }

            } else if (kelas == 3) {
                System.out.println("Kelas: First Class");
                kelasPenerbangan = new FirstClass();

                if (type == 1) {
                    double Seats = boeing787 * 0.65;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 2) {
                    double Seats = boeing787 * 0.25;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else if (type == 3) {
                    double Seats = boeing787 * 0.1;
                    System.out.println("Jumlah Kursi Tersedia: " + Seats);
                } else {
                    System.out.println("ERROR PILIH KURSI");
                }
            } else {
                System.out.println("Pilihan diluar jangkauan");
            }

            // convert sesuai pilihan
            if (kelasPenerbangan instanceof EconomyClass) {
                EconomyClass kelasPenerbanganConvert = (EconomyClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceEuropeUs());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else if (kelasPenerbangan instanceof BusinessClass) {
                BusinessClass kelasPenerbanganConvert = (BusinessClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceEuropeUs());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else if (kelasPenerbangan instanceof FirstClass) {
                FirstClass kelasPenerbanganConvert = (FirstClass)kelasPenerbangan;
                System.out.println("Harga Tiket: " + kelasPenerbanganConvert.getPriceEuropeUs());
                System.out.println("Max Laguage: " + kelasPenerbanganConvert.getMaxLaguage());
            } else {
                System.out.println("ERROR CONVERT PILIHAN");
            }
        }
        else {
            System.out.println("Pilihan tidak tersedia");
        }
    }
}
