public class EconomyClass extends Airplane{

    public double getPriceAsiaEurope() {
        return super.getAsiaEurope();
    }

    public double getPriceEuropeAsia() {
        return super.getEuropeAsia();
    }

    public double getPriceEuropeUs() {
        return super.getEuropeUs();
    }

     public double getMaxLaguage() {
        return super.getLaguage();
     }
}
