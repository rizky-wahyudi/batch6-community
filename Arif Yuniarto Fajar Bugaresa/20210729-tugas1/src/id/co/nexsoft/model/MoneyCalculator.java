package id.co.nexsoft.model;

public class MoneyCalculator {
    MataUang dollar;
    MataUang rupiah;
    MataUang euro;

    public void setDollar(MataUang dollar) {
        this.dollar = dollar;
    }

    public void setRupiah(MataUang rupiah) {
        this.rupiah = rupiah;
    }

    public void setEuro(MataUang euro) {
        this.euro = euro;
    }

    public double getConversionIdrToDollar(double paramRupiah) {
        return paramRupiah / rupiah.getAmountToDollar();
    }

    public double getConversionEuroToDollar(double paramEuro) {
        return paramEuro * euro.getAmountToDollar();
    }

    public double getConversionDolarToEuro(double paramDollar) {
        return euro.getAmountToDollar() * paramDollar ;
    }

    public double getConversionDolarToIdr(double paramDollar) {
        return  rupiah.getAmountToDollar() /paramDollar ;
    }


}
