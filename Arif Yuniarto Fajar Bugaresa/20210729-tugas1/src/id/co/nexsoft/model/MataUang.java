package id.co.nexsoft.model;

public class MataUang {
    private String name;
    private double amountToDollar;

    public MataUang(String name, double amountToDollar) {
        this.name = name;
        this.amountToDollar = amountToDollar;
    }

    public String getName() {
        return name;
    }

    public double getAmountToDollar() {
        return amountToDollar;
    }
}
