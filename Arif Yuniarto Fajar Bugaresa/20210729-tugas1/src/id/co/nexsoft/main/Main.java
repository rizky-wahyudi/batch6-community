package id.co.nexsoft.main;

import id.co.nexsoft.model.MataUang;
import id.co.nexsoft.model.MoneyCalculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        MataUang indonesia = new MataUang("Rupiah", 14000);
        MataUang us = new MataUang("Dollar", 1);
        MataUang eropa = new MataUang("Euro", 0.84);

        MoneyCalculator mc = new MoneyCalculator();
        mc.setDollar(us);
        mc.setEuro(eropa);
        mc.setRupiah(indonesia);

        displayInfo();
        double hasil = calculate(mc);
        System.out.println(hasil);
    }

    public static void displayInfo() {

        System.out.println("Konversi Mata Uang");
        System.out.println("1. Rupiah -> Euro ");
        System.out.println("2. Rupiah -> Dollar ");
        System.out.println("3. Euro -> Rupiah  ");
        System.out.println("4. Euro -> Dollar ");
        System.out.println("5. Dollar -> Rupiah ");
        System.out.println("6. Dollar -> Euro ");
        System.out.print("Mau koveri yang mana: ");
    }

    public static double calculate(MoneyCalculator mc){
        Scanner scanner = new Scanner(System.in);
        double pilihan = scanner.nextDouble();

        System.out.print("Berapa duitnya?: ");

        double inputParameter = scanner.nextDouble();
        double hasilConvert = 0;

        if (pilihan == 1) {
            double dollar =  mc.getConversionIdrToDollar(inputParameter);
            double euro = mc.getConversionDolarToEuro(dollar);
            hasilConvert = euro;
        } else if (pilihan == 2) {
            double rupiah = mc.getConversionIdrToDollar(inputParameter);
            hasilConvert = rupiah;
        } else if (pilihan == 3) {
            double dollar = mc.getConversionEuroToDollar(inputParameter);
            double rupiah = mc.getConversionDolarToIdr(dollar);
            hasilConvert = rupiah;
        } else if (pilihan == 4) {
            double dollar = mc.getConversionEuroToDollar(inputParameter);
            hasilConvert = dollar;
        } else if (pilihan == 5) {
            double rupiah = mc.getConversionDolarToIdr(inputParameter);
            hasilConvert = rupiah;
        } else if (pilihan == 6) {
            double euro = mc.getConversionDolarToEuro(inputParameter);
            hasilConvert = euro;
        } else {
            System.out.println("Diluar Jangkauan");
        }

        return hasilConvert;

    }
}
