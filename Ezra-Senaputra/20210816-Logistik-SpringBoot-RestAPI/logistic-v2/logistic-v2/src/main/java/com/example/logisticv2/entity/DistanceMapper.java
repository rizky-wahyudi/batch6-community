package com.example.logisticv2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "distance_table")
public class DistanceMapper {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "fromWarehouseId")
    private int fromWarehouseId;

    @Column(name = "toWarehouseId")
    private int toWarehouseId;

    @Column(name = "distance")
    private int distance;

}
