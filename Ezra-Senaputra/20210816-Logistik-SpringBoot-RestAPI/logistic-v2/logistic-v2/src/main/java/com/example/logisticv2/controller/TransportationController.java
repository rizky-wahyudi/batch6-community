package com.example.logisticv2.controller;

import com.example.logisticv2.entity.Transportation;
import com.example.logisticv2.service.TransportationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TransportationController {
    @Autowired
    private TransportationService transportationService;

    @PostMapping("/addTransportation")
    public Transportation addTransportation(@RequestBody Transportation transportation){
        return transportationService.saveTransportation(transportation);
    }

    @GetMapping("/transportations")
    public List<Transportation> findAllTransportation(){
        return transportationService.getTransportation();
    }

    @GetMapping("/transportationsById/{id}")
    public Transportation findTransportationById(@PathVariable int id){
        return transportationService.getTransportationById(id);
    }


    @PutMapping("/updateTransportation")
    public Transportation updateTransportation(@RequestBody Transportation addres){
        return transportationService.updateTransportation(addres);
    }

    @DeleteMapping("/deleteTransportation/{id}")
    public String deleteTransportation(@PathVariable int id){
        return transportationService.deleteTransportation(id);
    }
}
