package com.example.logisticv2.service;

import com.example.logisticv2.entity.Product;
import com.example.logisticv2.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product saveProduct(Product product){
        return productRepository.save(product);
    }

    public List<Product> getProduct(){
        return productRepository.findAll();
    }

    public Product getProductById(int id){
        return productRepository.findById(id).orElse(null);
    }

    public String deleteProduct(int id){
        productRepository.deleteById(id);
        return "Product Removed";
    }

    public Product updateProduct(Product product){
        Product existingProduct = productRepository.findById(product.getId()).orElse(null);
        existingProduct.setName(product.getName());
        existingProduct.setDeliverAddress(product.getDeliverAddress());
        existingProduct.setHeight(product.getHeight());
        existingProduct.setDepth(product.getDepth());
        existingProduct.setWidth(product.getWidth());
        existingProduct.setWeight(product.getWeight());
        existingProduct.setAmount(product.getAmount());
        return productRepository.save(existingProduct);
    }
}
