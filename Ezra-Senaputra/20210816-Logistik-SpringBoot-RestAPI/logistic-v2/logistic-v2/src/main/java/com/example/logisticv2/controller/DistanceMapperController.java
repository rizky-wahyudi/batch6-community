package com.example.logisticv2.controller;

import com.example.logisticv2.entity.DistanceMapper;
import com.example.logisticv2.service.DistanceMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DistanceMapperController {

    @Autowired
    private DistanceMapperService distanceMapperService;

    @PostMapping("/addDistanceMapper")
    public DistanceMapper addDistanceMapper(@RequestBody DistanceMapper distanceMapper){
        return distanceMapperService.saveDistanceMapper(distanceMapper);
    }

    @GetMapping("/distanceMappers")
    public List<DistanceMapper> findAllDistanceMapper(){
        return distanceMapperService.getDistanceMapper();
    }

    @GetMapping("/distanceMappersById/{id}")
    public DistanceMapper findDistanceMapperById(@PathVariable int id){
        return distanceMapperService.getDistanceMapperById(id);
    }


    @PutMapping("/updateDistanceMapper")
    public DistanceMapper updateDistanceMapper(@RequestBody DistanceMapper addres){
        return distanceMapperService.updateDistanceMapper(addres);
    }

    @DeleteMapping("/deleteDistanceMapper/{id}")
    public String deleteDistanceMapper(@PathVariable int id){
        return distanceMapperService.deleteDistanceMapper(id);
    }
}
