package com.example.logisticv2.controller;

import com.example.logisticv2.entity.Address;
import com.example.logisticv2.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AddressController {

    @Autowired
    private AddressService addressService;

    @PostMapping("/addAddress")
    public Address addAddress(@RequestBody Address address){
        return addressService.saveAddress(address);
    }

    @GetMapping("/addresses")
    public List<Address> findAllAddress(){
        return addressService.getAddress();
    }

    @GetMapping("/addressesById/{id}")
    public Address findAddressById(@PathVariable int id){
        return addressService.getAddressById(id);
    }


    @PutMapping("/updateAddress")
    public Address updateAddress(@RequestBody Address addres){
        return addressService.updateAddress(addres);
    }

    @DeleteMapping("/deleteAddress/{id}")
    public String deleteAddress(@PathVariable int id){
        return addressService.deleteAddress(id);
    }

}
