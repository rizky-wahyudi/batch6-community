package com.example.logisticv2.controller;

import com.example.logisticv2.entity.Product;
import com.example.logisticv2.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    @Autowired

    private ProductService productService;

    @PostMapping("/addProduct")
    public Product addProduct(@RequestBody Product product){
        return productService.saveProduct(product);
    }

    @GetMapping("/products")
    public List<Product> findAllProduct(){
        return productService.getProduct();
    }

    @GetMapping("/productsById/{id}")
    public Product findProductById(@PathVariable int id){
        return productService.getProductById(id);
    }


    @PutMapping("/updateProduct")
    public Product updateProduct(@RequestBody Product addres){
        return productService.updateProduct(addres);
    }

    @DeleteMapping("/deleteProduct/{id}")
    public String deleteProduct(@PathVariable int id){
        return productService.deleteProduct(id);
    }
}
