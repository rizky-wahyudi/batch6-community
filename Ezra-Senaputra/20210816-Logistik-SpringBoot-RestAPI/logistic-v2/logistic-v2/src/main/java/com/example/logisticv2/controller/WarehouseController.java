package com.example.logisticv2.controller;

import com.example.logisticv2.entity.Warehouse;
import com.example.logisticv2.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WarehouseController {
    @Autowired
    private WarehouseService warehouseService;

    @PostMapping("/addWarehouse")
    public Warehouse addWarehouse(@RequestBody Warehouse warehouse){
        return warehouseService.saveWarehouse(warehouse);
    }

    @GetMapping("/warehouses")
    public List<Warehouse> findAllWarehouse(){
        return warehouseService.getWarehouse();
    }

    @GetMapping("/warehousesById/{id}")
    public Warehouse findWarehouseById(@PathVariable int id){
        return warehouseService.getWarehouseById(id);
    }


    @PutMapping("/updateWarehouse")
    public Warehouse updateWarehouse(@RequestBody Warehouse addres){
        return warehouseService.updateWarehouse(addres);
    }

    @DeleteMapping("/deleteWarehouse/{id}")
    public String deleteWarehouse(@PathVariable int id){
        return warehouseService.deleteWarehouse(id);
    }
}
