package com.example.logisticv2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transport_table")
public class Transportation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "driver")
    private String driver;

    @Column(name = "size")
    private int size;

    @Column(name = "speed")
    private int speed;

    @Column(name = "type_transport")
    private String typeTransport;

    @Column(name = "loadTransport")
    private int loadTransport;

    @Column(name = "maxItem")
    private int maxItem;
}
