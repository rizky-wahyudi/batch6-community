package com.example.logisticv2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "warehouse_table")
public class Warehouse {

    @Id
    @GeneratedValue
    private int id;

    private String name;

    // Belajar Lagi Relations
//    @OneToOne(fetch = FetchType.LAZY,
//            cascade =  CascadeType.ALL,
//            mappedBy = "warehouse")
//    private Address address;

    @Column(name = "address_id")
    private int addressId;

}
