package com.example.logisticv2.service;

import com.example.logisticv2.entity.Transportation;
import com.example.logisticv2.repository.TransportationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransportationService {

    @Autowired
    private TransportationRepository transportationRepository;

    public Transportation saveTransportation(Transportation transportation){
        return transportationRepository.save(transportation);
    }

    public List<Transportation> getTransportation(){
        return transportationRepository.findAll();
    }

    public Transportation getTransportationById(int id){
        return transportationRepository.findById(id).orElse(null);
    }

    public String deleteTransportation(int id){
        transportationRepository.deleteById(id);
        return "Transportation Removed";
    }

    public Transportation updateTransportation(Transportation transportation){
        Transportation existingTransportation = transportationRepository.findById(transportation.getId()).orElse(null);
        existingTransportation.setDriver(transportation.getDriver());
        existingTransportation.setSize(transportation.getSize());
        existingTransportation.setSpeed(transportation.getSpeed());
        existingTransportation.setTypeTransport(transportation.getTypeTransport());
        existingTransportation.setLoadTransport(transportation.getLoadTransport());
        existingTransportation.setMaxItem(transportation.getMaxItem());
        return transportationRepository.save(existingTransportation);
    }
}
