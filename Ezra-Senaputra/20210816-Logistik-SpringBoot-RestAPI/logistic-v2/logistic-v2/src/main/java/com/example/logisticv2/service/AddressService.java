package com.example.logisticv2.service;

import com.example.logisticv2.entity.Address;
import com.example.logisticv2.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    // Post
    public Address saveAddress(Address address){
        return addressRepository.save(address);
    }

    // Get
    public List<Address> getAddress(){
        return addressRepository.findAll();
    }

    public Address getAddressById(int id){
        return addressRepository.getById(id);
    }

    public String deleteAddress(int id){
        addressRepository.deleteById(id);
        return "Address Removed";
    }

    public Address updateAddress(Address address){
        Address existingAddress = addressRepository.findById(address.getId()).orElse(null);
        existingAddress.setCountry(address.getCountry());
        existingAddress.setDistrict(address.getDistrict());
        existingAddress.setProvince(address.getProvince());
        existingAddress.setStreet(address.getStreet());
        existingAddress.setLatitude(address.getLatitude());
        existingAddress.setLangitude(address.getLangitude());
        existingAddress.setWarehouseId(address.getWarehouseId());
        return addressRepository.save(address);
    }





}
