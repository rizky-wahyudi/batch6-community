package com.example.logisticv2.service;

import com.example.logisticv2.entity.Warehouse;
import com.example.logisticv2.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarehouseService {

    @Autowired
    private WarehouseRepository warehouseRepository;

    public Warehouse saveWarehouse(Warehouse warehouse){
        return warehouseRepository.save(warehouse);
    }

    public List<Warehouse> getWarehouse(){
        return warehouseRepository.findAll();
    }

    public Warehouse getWarehouseById(int id){
        return warehouseRepository.findById(id).orElse(null);
    }

    public String deleteWarehouse(int id){
        warehouseRepository.deleteById(id);
        return "Warehouse Removed";
    }

    public Warehouse updateWarehouse(Warehouse warehouse){
        Warehouse existingWarehouse = warehouseRepository.findById(warehouse.getId()).orElse(null);
        existingWarehouse.setAddressId(warehouse.getAddressId());
        existingWarehouse.setName(warehouse.getName());
        return warehouseRepository.save(existingWarehouse);
    }
}
