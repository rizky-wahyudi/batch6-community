package com.example.logisticv2.service;

import com.example.logisticv2.entity.DistanceMapper;
import com.example.logisticv2.repository.DistanceMapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistanceMapperService {
    @Autowired
    private DistanceMapperRepository distanceMapperRepository;

    public DistanceMapper saveDistanceMapper(DistanceMapper distanceMapper){
        return distanceMapperRepository.save(distanceMapper);
    }

    public List<DistanceMapper> getDistanceMapper(){
        return distanceMapperRepository.findAll();
    }

    public DistanceMapper getDistanceMapperById(int id){
        return distanceMapperRepository.findById(id).orElse(null);
    }

    public String deleteDistanceMapper(int id){
        distanceMapperRepository.deleteById(id);
        return "DistanceMapper Removed";
    }

    public DistanceMapper updateDistanceMapper(DistanceMapper distanceMapper){
        DistanceMapper existingDistanceMapper = distanceMapperRepository.findById(distanceMapper.getId()).orElse(null);
        existingDistanceMapper.setDistance(distanceMapper.getDistance());
        existingDistanceMapper.setFromWarehouseId(distanceMapper.getFromWarehouseId());
        existingDistanceMapper.setToWarehouseId(distanceMapper.getToWarehouseId());
        return distanceMapperRepository.save(existingDistanceMapper);
    }
}
