package com.example.logisticv2.repository;

import com.example.logisticv2.entity.DistanceMapper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistanceMapperRepository extends JpaRepository<DistanceMapper, Integer> {
}
