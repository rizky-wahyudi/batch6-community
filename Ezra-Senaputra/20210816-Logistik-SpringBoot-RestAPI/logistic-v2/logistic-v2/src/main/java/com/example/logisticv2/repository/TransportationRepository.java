package com.example.logisticv2.repository;

import com.example.logisticv2.entity.Transportation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransportationRepository extends JpaRepository<Transportation, Integer> {
}
