package com.example.logisticv2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "address_table")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "street")
    private String street;

    @Column(name = "district")
    private String district;

    @Column(name = "province")
    private String province;

    @Column(name = "country")
    private String country;

    @Column(name = "langitude")
    private double langitude;

    @Column(name = "latitude")
    private double latitude;

    // Belajar Lagi Relations
//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "warehouse_id")
//    private Warehouse warehouse;

    @Column(name = "warehouse_id")
    private int warehouseId;


}
