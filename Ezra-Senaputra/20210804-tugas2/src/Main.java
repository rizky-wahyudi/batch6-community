import model.Distance;
import model.Temp;
import service.Calculate;

import java.util.Scanner;

public class Main {

    Distance distance = new Distance();
    Temp temp = new Temp();


    public static void main(String[] args) {
        Scanner inputProcess = new Scanner(System.in);
        while(true){
            showOptions();
            System.out.println("Convert Again ? Y/X");
            String inputNext = inputProcess.nextLine();
            if(inputNext.equals("Y")){

            } else {
                break;
            }

        }
    }

    public static void showOptions(){
        Scanner input = new Scanner(System.in);
        System.out.println("Pick calculate : ");
        System.out.println("1. Distance");
        System.out.println("2. Temp");
        int inputOptions = input.nextInt();

        if(inputOptions == 1){
            System.out.println("Please choose the convertion : ");
            System.out.println("1. KM - Miles");
            System.out.println("2. Miles - KM");
            int inputConvertionDistance = input.nextInt();
            if(inputConvertionDistance == 1) {
                convertThis(1);
            } else {
                convertThis(2);
            }
        } else if(inputOptions == 2){
            System.out.println("Please choose the convertion : ");
            System.out.println("1. Celcius - Fahrenheit");
            System.out.println("2. Fahrenheit - Celcius");
            int inputConvertTemp = input.nextInt();
            if(inputConvertTemp == 1) {
                convertThis(3);
            } else {
                convertThis(4);
            }
        }
    }

    public static void convertThis(int id){
        Calculate calculate = new Calculate();
        Scanner scanner = new Scanner(System.in);
        switch (id) {
            case 1 :
                System.out.print("Please input KM amount : ");
                double km = scanner.nextDouble();
                double resultMiles = calculate.calculateDistance(1,km, 0);
                System.out.println("KM      : " + km);
                System.out.println("Miles   : " + resultMiles);
                break;
            case 2:
                System.out.print("Please input Miles amount : ");
                double miles = scanner.nextDouble();
                double resultKM = calculate.calculateDistance(2,0, miles);
                System.out.println("Miles   : " + miles);
                System.out.println("KM      : " + resultKM);
                break;
            case 3 :
                System.out.print("Please input Celcius amount : ");
                double celc = scanner.nextDouble();
                double resultFahr = calculate.calculateTemp(1,celc, 0);
                System.out.println("Celcius     : " + celc );
                System.out.println("Fahrenheit  : " + resultFahr);
                break;
            case 4 :
                System.out.print("Please input Fahrenheit amount : ");
                double fahr = scanner.nextDouble();
                double resultCelc = calculate.calculateTemp(1,0, fahr);
                System.out.println("Fahrenheit  : " + fahr);
                System.out.println("Celcius     : " + resultCelc);
                break;
            default:
                System.out.println("Not found");
        }
    }

}
