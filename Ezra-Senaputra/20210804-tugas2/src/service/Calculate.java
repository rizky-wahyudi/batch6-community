package service;

public class Calculate {

    public double calculateDistance(int id, double km, double miles){
        if(id == 1){
            return km / 1.609;
        } else if(id == 2){
            return miles * 1.609;
        } else {
            return 0;
        }
    }

    public double calculateTemp(int id, double celcius, double fahrenheit){
        if(id == 1){
            return (celcius * 1.8) + 32;
        } else if(id == 2){
            return (fahrenheit - 32) * (0.555555556);
        } else {
            return 0;
        }
    }
}
