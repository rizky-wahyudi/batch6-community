package id.co.nexsoft.handsonlaundry.entity;

import javax.persistence.*;

@Entity
public class OrderItems {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String itemName;
    private int weight;
    private int amount;

    @ManyToOne()
    @JoinColumn(name = "service_id")
    private Service service;

    public OrderItems() {
    }

    public OrderItems(String itemName, int weight, int amount, Service service) {
        this.itemName = itemName;
        this.weight = weight;
        this.amount = amount;
        this.service = service;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
