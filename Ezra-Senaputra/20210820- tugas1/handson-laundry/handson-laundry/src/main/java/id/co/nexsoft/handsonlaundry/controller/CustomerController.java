package id.co.nexsoft.handsonlaundry.controller;

import id.co.nexsoft.handsonlaundry.entity.Address;
import id.co.nexsoft.handsonlaundry.entity.Customer;
import id.co.nexsoft.handsonlaundry.repository.AddressRepository;
import id.co.nexsoft.handsonlaundry.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AddressRepository addressRepository;

    @GetMapping("/")
    public String welcome(){
        return  "<html><body>"
                + "<h1>Please Buy at your Desire</h1>"
                + "</body></html>";
    }

    @GetMapping("/customers")
    public List<Customer> getAllCustomer(){
        return customerRepository.findAll();
    }

    @GetMapping("/customers/{id}")
    public Customer getCustomerById(@PathVariable(value = "id") int id){
        return customerRepository.findById(id);
    }

    @PostMapping(value = "/customers", consumes = "application/json")
    public Address addCustomer(@RequestBody Customer customer){
        customerRepository.save(customer);
        Address address = customer.getAddress();
        address.setId(customer.getId());
        return addressRepository.save(address);
    }
}
