package id.co.nexsoft.handsonlaundry.controller;

import id.co.nexsoft.handsonlaundry.entity.OrderItems;
import id.co.nexsoft.handsonlaundry.entity.Orders;
import id.co.nexsoft.handsonlaundry.entity.Service;
import id.co.nexsoft.handsonlaundry.repository.OrderItemsRepository;
import id.co.nexsoft.handsonlaundry.repository.OrdersRepository;
import id.co.nexsoft.handsonlaundry.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrdersController {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrderItemsRepository orderItemsRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @GetMapping("/orders")
    public List<Orders> getAllOrders(){
        return ordersRepository.findAll();
    }

    @GetMapping("/orders/{id}")
    public Orders getOrdersById(@PathVariable(value = "id") int id){
        return ordersRepository.findById(id);
    }

    @PostMapping("/orders")
    public String addOrders(@RequestBody Orders orders){
        for (OrderItems orderItems : orders.getOrderItems()){
            orderItemsRepository.save(orderItems);
            Service service = orderItems.getService();
            serviceRepository.save(service);
        }
        ordersRepository.save(orders);
        return "Data Order Berhasil Disimpan";
    }
}
