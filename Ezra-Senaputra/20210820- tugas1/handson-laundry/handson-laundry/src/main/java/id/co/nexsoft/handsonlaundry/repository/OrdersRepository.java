package id.co.nexsoft.handsonlaundry.repository;

import id.co.nexsoft.handsonlaundry.entity.Orders;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrdersRepository extends CrudRepository<Orders, Integer> {
    List<Orders> findAll();
    Orders findById(int id);
}
