package id.co.nexsoft.handsonlaundry.controller;

import id.co.nexsoft.handsonlaundry.entity.Service;
import id.co.nexsoft.handsonlaundry.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServiceController {

    @Autowired
    private ServiceRepository serviceRepository;


    @GetMapping("/services")
    public List<Service> getAllService(){
        return serviceRepository.findAll();
    }

    @GetMapping("/services/{id}")
    public Service getServiceById(@PathVariable(value = "id") int id){
        return serviceRepository.findById(id);
    }

    @PostMapping("/services")
    public String addService(@RequestBody Service service){
        serviceRepository.save(service);;
        return "Data Karyawan Berhasil Disimpan";
    }
}
