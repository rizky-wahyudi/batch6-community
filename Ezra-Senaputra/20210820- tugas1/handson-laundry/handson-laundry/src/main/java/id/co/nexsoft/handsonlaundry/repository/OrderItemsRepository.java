package id.co.nexsoft.handsonlaundry.repository;

import id.co.nexsoft.handsonlaundry.entity.OrderItems;
import org.springframework.data.repository.CrudRepository;

public interface OrderItemsRepository extends CrudRepository<OrderItems, Integer> {
}
