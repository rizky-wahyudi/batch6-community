package id.co.nexsoft.handsonlaundry.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String street;
    private String district;
    private String city;
    private String province;
    private String country;
    private double longitude;
    private double latitude;

    @JsonIgnore
    @OneToOne(mappedBy = "address_customer")
    private Customer customer;

    @JsonIgnore
    @OneToOne(mappedBy = "address_employee")
    private Employee employee;

    public Address() {
    }

    public Address(String street, String district, String city, String province,
                   String country, double longitude, double latitude, Customer customer, Employee employee) {
        this.street = street;
        this.district = district;
        this.city = city;
        this.province = province;
        this.country = country;
        this.longitude = longitude;
        this.latitude = latitude;
        this.customer = customer;
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
