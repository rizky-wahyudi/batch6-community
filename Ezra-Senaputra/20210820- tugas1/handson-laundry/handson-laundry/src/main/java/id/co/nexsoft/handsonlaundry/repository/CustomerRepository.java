package id.co.nexsoft.handsonlaundry.repository;

import id.co.nexsoft.handsonlaundry.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    List<Customer> findAll();
    Customer findById(int id);
}
