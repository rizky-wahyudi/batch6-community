package id.co.nexsoft.handsonlaundry.repository;

import id.co.nexsoft.handsonlaundry.entity.Service;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServiceRepository extends CrudRepository<Service, Integer> {
    List<Service> findAll();
    Service findById(int id);
}
