package id.co.nexsoft.handsonlaundry.entity;


import javax.persistence.*;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String firstName;
    private String lastName;

    @OneToOne(cascade =  CascadeType.ALL)
    private Address address_customer;
    private String phone;
    private String email;
    private String avatar;

    public Customer() {
    }

    public Customer(String firstName, String lastName, Address address_customer,
                    String phone, String email, String avatar) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address_customer = address_customer;
        this.phone = phone;
        this.email = email;
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address_customer;
    }

    public void setAddress(Address address_customer) {
        this.address_customer = address_customer;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
