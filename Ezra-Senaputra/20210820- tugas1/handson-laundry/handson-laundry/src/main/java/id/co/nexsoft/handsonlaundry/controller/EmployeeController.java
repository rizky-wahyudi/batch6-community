package id.co.nexsoft.handsonlaundry.controller;

import id.co.nexsoft.handsonlaundry.entity.Address;
import id.co.nexsoft.handsonlaundry.entity.Employee;
import id.co.nexsoft.handsonlaundry.repository.AddressRepository;
import id.co.nexsoft.handsonlaundry.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AddressRepository addressRepository;

    @GetMapping("/employees")
    public List<Employee> getAllEmployee(){
        return employeeRepository.findAll();
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable(value = "id") int id){
        return employeeRepository.findById(id);
    }

    @PostMapping(value = "/employees", consumes = "application/json")
    public String addEmployee(@RequestBody Employee employee){
        employeeRepository.save(employee);
        Address address = employee.getAddress();
        addressRepository.save(address);
        return "Data Karyawan Berhasil Disimpan";
    }
}
