package id.co.nexsoft.handsonlaundry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandsonLaundryApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandsonLaundryApplication.class, args);
	}

}
