package id.co.nexsoft.handsonlaundry.repository;

import id.co.nexsoft.handsonlaundry.entity.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    List<Employee> findAll();
    Employee findById(int id);
}
