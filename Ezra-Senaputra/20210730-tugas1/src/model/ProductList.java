package model;

import java.time.LocalDate;

public interface ProductList {
    String[] productName = {"Lifebuoy Lemon", "Doritos BBQ", "Greenfields 1 L"};
    String[] productType = {"Soap", "Food", "Drink"};
    String[] productArticleNumber = {"SO001", "FO001", "DR001"};
    String descSoap = "Lifebuoy Lemon gives you lemon extract capable of enriching your body fragrant" ;
    String descFood = "Doritos BBQ made your entire eating environment be adventure";
    String descDrink = "GreenFields a milk that taken from the best quality cow in Australia";
    String[] productDesc = {descSoap, descFood, descDrink};
    int[] productPrice = {4550, 11000, 45000};
    String[] productImage = {"Lifebuoy Image", "Doritos Image", "Greenfields Image"};
    LocalDate[] productExpiredDates= {LocalDate.of(2022, 2, 20),
            LocalDate.of(2021, 7, 20),
            LocalDate.of(2021, 12, 12)
    };
    int[] productTax = {7, 5, 10};
    int sellPriceSoap = productPrice[0] + (productPrice[0] / 100);
    int sellPriceFood = productPrice[1] + (productPrice[1] / 100);
    int sellPriceDrink = productPrice[2] + (productPrice[2] / 100);
    int[] productSellPrice = {sellPriceSoap, sellPriceFood, sellPriceDrink};

}
