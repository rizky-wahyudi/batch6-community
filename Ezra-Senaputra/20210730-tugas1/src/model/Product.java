package model;

import java.time.LocalDate;

public class Product implements  ProductList{
    private String[] name = productName;
    private String[] type = productType;
    private String[] articleNumber = productArticleNumber;
    private String[] description = productDesc;
    private int[] purchasePrice = productPrice;
    private int[] sellPrice = productSellPrice;
    private String[] image = productImage;
    private LocalDate[] expiredDate = productExpiredDates;
    private int[] tax = productTax;

    public String[] getName() {
        return name;
    }

    public String[] getType() {
        return type;
    }

    public String[] getArticleNumber() {
        return articleNumber;
    }

    public String[] getDescription() {
        return description;
    }

    public int[] getPurchasePrice() {
        return purchasePrice;
    }

    public int[] getSellPrice() {
        return sellPrice;
    }

    public String[] getImage() {
        return image;
    }

    public LocalDate[] getExpiredDate() {
        return expiredDate;
    }

    public int[] getTax() {
        return tax;
    }
}
