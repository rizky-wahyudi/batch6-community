package model;

import java.util.ArrayList;

public class OrderItems extends Product {
    private ArrayList<Integer> orderId;
    private ArrayList<Integer> amount;

    public ArrayList<Integer> getOrderId() {
        return orderId;
    }

    public void setOrderId(ArrayList<Integer> orderId) {
        this.orderId = orderId;
    }

    public ArrayList<Integer> getAmount() {
        return amount;
    }

    public void setAmount(ArrayList<Integer> amount) {
        this.amount = amount;
    }
}
