package service;

import model.Order;
import model.OrderItems;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Payment {

    Order order = new Order();

    public int calculateTotalPrice(ArrayList<Integer> amount, ArrayList<Integer> price){
        int totalPrice = 0;
        for (int i = 0; i < amount.size(); i++) {
            totalPrice += amount.get(i) * price.get(i);
        }
        return totalPrice;

    }

    public int calculateTax(ArrayList<Integer> itemTax, ArrayList<Integer> itemAmount, ArrayList<Integer> itemPrice){
        int totalTax = 0;
        for(int i = 0; i < itemTax.size(); i++){
            int tax = itemTax.get(i);
            int amount = itemAmount.get(i);
            int price = itemPrice.get(i);
            int total = (tax * (amount * price)) / 100;
            totalTax += total;
        }
        return totalTax;
    }

    public int calculateToBePaid(int a, int b){
        return a + b;
    }

    public void orderPayment(int orderToBePaid){
        while(true){
            order.setOrderDate(LocalDate.now());
            String firstDigit =  Double.toString(Math.random());
            String secondDigit = Double.toString(Math.random());
            String thirdDigit = Double.toString(Math.random());
            String forthDigit = Double.toString(Math.random());

            order.setOrderNumber(firstDigit + secondDigit + thirdDigit + forthDigit);
            order.setBranchId(1);

            Scanner input = new Scanner(System.in);
            System.out.println("Input Uang Anda : ");
            try {

            int inputMoney = input.nextInt();
            if(inputMoney >= orderToBePaid) {
                int change = inputMoney - orderToBePaid;
                System.out.println("YOUR PAYMENT SUCCESS");
                System.out.println("YOUR CHANGE : " + change);
                break;
            } else if(inputMoney < orderToBePaid){
                System.out.println("MONEY NOT ENOUGH");
                System.out.println("PLEASE ENTER THE RIGHT AMOUNT");
            } else {
                System.out.println("PLEASE ENTER ONLY MONEY");

            }
            } catch (InputMismatchException e){
                e.printStackTrace();
                System.out.println("PLEASE ENTER ONLY MONEY");
            }

        }


    }

}
