package service;

import model.OrderItems;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class ShoppingCart {
    OrderItems orderItems = new OrderItems();
    ArrayList<Integer> itemId = new ArrayList<>();
    ArrayList<String> itemName = new ArrayList<>();
    ArrayList<Integer> itemAmount = new ArrayList<>();
    ArrayList<Integer> itemPrice = new ArrayList<>();
    ArrayList<Integer> itemTax = new ArrayList<>();
    ArrayList<LocalDate> itemDate = new ArrayList<>();



    public void showProduct(){
        int no = 0;
        for (int i = 0; i < orderItems.getName().length; i++) {
            no = i + 1;
            String name = orderItems.getName()[i];
            int price = orderItems.getSellPrice()[i];
            System.out.println(no + "." + name + " : " + ConvertToRupiah.convertToRupiah(price));
            System.out.println("========================================");
            System.out.println("Product Desc    : " + orderItems.getDescription()[i]);
            System.out.println("Product Image   : " + orderItems.getImage()[i]);
            System.out.println("Product Expired : " + orderItems.getExpiredDate()[i]);
            System.out.println("Product Type    : " + orderItems.getType()[i]);
            System.out.println(" ");
        }
    }

    public void pickProduct(int orderTime){
        while (true){

            try{
                Scanner input = new Scanner(System.in);
                System.out.println("PLEASE CHOOSE PRODUCT (1-3)");
                String inputProduct = input.nextLine();

                System.out.println("PLEASE INPUT AMOUNT");
                String inputAmountString = input.nextLine();
                int inputAmount = Integer.parseInt(inputAmountString);
                if(inputProduct.equals("1") && inputAmount > 0){
                    inputProduct(0, inputAmount, orderTime, orderItems.getSellPrice()[0]);
                    break;
                } else if(inputProduct.equals("2") && inputAmount > 0 ){
                    inputProduct(1, inputAmount, orderTime, orderItems.getSellPrice()[1]);
                    break;
                } else if(inputProduct.equals("3") && inputAmount > 0 ){
                    inputProduct(2, inputAmount, orderTime, orderItems.getSellPrice()[2]);
                    break;
                } else {
                    System.out.println("PLEASE INPUT PRODUCT 1-3");
                }
            } catch (NumberFormatException e){
                System.out.println("PLEASE INPUT ONLY NUMBERS");
            }



        }
    }

    public void inputProduct(int x, int amount, int orderTime, int price){
        itemId.add(orderTime);
        itemName.add(orderItems.getName()[x]);
        itemAmount.add(amount);
        itemPrice.add(price);
        itemTax.add(orderItems.getTax()[x]);
        itemDate.add(LocalDate.now());
    }

    public void setOrderItems(){
        orderItems.setOrderId(itemId);
        orderItems.setAmount(itemAmount);
    }

    public void showShoppingCart(){
        System.out.println("");
        int totalPrice = 0;
        for (int i = 0; i < orderItems.getOrderId().size(); i++) {


            System.out.println(itemId.get(i) + " " + itemName.get(i));
            if(itemName.get(i).equals(orderItems.getName()[0])){
                checkProductExpire(i, 0);
            } else if(itemName.get(i).equals(orderItems.getName()[1])){
                checkProductExpire(i, 1);
            } else if(itemName.get(i).equals(orderItems.getName()[2])){
                checkProductExpire(i, 2);
            }
            System.out.println("=========================");
            int price = itemPrice.get(i) * itemAmount.get(i);
            System.out.println("Price : " + ConvertToRupiah.convertToRupiah(price));
            totalPrice += price;
        }
            System.out.println("Total Price : "  + ConvertToRupiah.convertToRupiah(totalPrice));
            System.out.println("Not Include Tax");
    }

    public void checkProductExpire(int i, int x){
        if(itemDate.get(i).compareTo(orderItems.getExpiredDate()[x]) == 0){
            System.out.println("PRODUCT EXPIRE NOW");
        } else if(itemDate.get(i).compareTo(orderItems.getExpiredDate()[x]) < 0) {
            System.out.println("WARNING YOUR PRODUCT IS EXPIRED");
        }
    }

    public ArrayList<Integer> getAmount(){
        return itemAmount;
    }

    public ArrayList<Integer> getPrice(){
        return itemPrice;
    }

    public ArrayList<Integer> getItemTax() {
        return itemTax;
    }

    public void clearAllShoppingCart(){
        itemId.clear();
        itemName.clear();
        itemAmount.clear();
        itemPrice.clear();
        itemTax.clear();
    }

    public void removeItem(int removeId){
        itemId.remove(removeId);
        itemName.remove(removeId);
        itemAmount.remove(removeId);
        itemPrice.remove(removeId);
        itemTax.remove(removeId);
        System.out.println("REMOVE SUCCESSFULL");
    }
}
