import service.ConvertToRupiah;
import service.Payment;
import service.ShoppingCart;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ShoppingCart shoppingCart = new ShoppingCart();
        Payment payment = new Payment();

        int orderTime = 0;
        while (true){
            orderTime++;
            shoppingCart.showProduct();
            shoppingCart.pickProduct(orderTime);
            shoppingCart.setOrderItems();
            shoppingCart.showShoppingCart();

            Scanner input = new Scanner(System.in);
            System.out.println("PROCEED TO PAYMENT (Y)");
            System.out.println("PROCEED TO BUY AGAIN (X)");
            System.out.println("PROCEED REMOVE ITEM FROM SHOPPING CART (Z)");
            String inputProcess = input.nextLine();

            if(inputProcess.equals("Y")){
              int totalPrice  = payment.calculateTotalPrice(shoppingCart.getAmount(), shoppingCart.getPrice());
              int totalTax = payment.calculateTax(shoppingCart.getItemTax(), shoppingCart.getAmount(),
                      shoppingCart.getPrice());
              int totalPayment = payment.calculateToBePaid(totalPrice, totalTax);
              String totalPriceRupiah = ConvertToRupiah.convertToRupiah(totalPrice);
              String totalTaxRupiah = ConvertToRupiah.convertToRupiah(totalTax);
              String totalPaymentRupiah = ConvertToRupiah.convertToRupiah(totalPayment);
              showOrderPayment(totalPriceRupiah, totalTaxRupiah, totalPaymentRupiah);
              payment.orderPayment(totalPayment);

              System.out.println("PROCEED TO BUY AGAIN (Y)");
              System.out.println("PROCEED TO FINISHED (X)");
              String finishedBuy = input.nextLine();
              if(finishedBuy.equals("X")){
                  break;
              } else  {
                shoppingCart.clearAllShoppingCart();
                orderTime = 0;
              }

            } else if(inputProcess.equals("Z")) {
                shoppingCart.showShoppingCart();
                System.out.print("INPUT NO.ITEM TO REMOVE : ");
                int removeId = input.nextInt();
                shoppingCart.removeItem(removeId - 1);

            } else {
                System.out.println("BACK TO MARKET!");
            }
        }
    }

    public static void showOrderPayment(String totalPrice, String totalTax, String totalPayment){
        System.out.println("===== YOUR PAYMENT =====");
        System.out.println("Total Price     : " + totalPrice);
        System.out.println("Total Tax       : " + totalTax);
        System.out.println("Total Payment   : " + totalPayment);
        System.out.println("===================================");
    }


}
