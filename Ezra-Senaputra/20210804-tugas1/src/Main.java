import model.ElectricityVariabel;
import service.Calculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ElectricityVariabel electricityVariabel = new ElectricityVariabel();
        Calculator calculator = new Calculator();

        HashMap<String, Double> inputValue = new HashMap<>();
        ArrayList<String> inputCode = new ArrayList<>();

        Scanner input = new Scanner(System.in);

        while (true) {
            while(true) {

                System.out.println("Pick Variabels To Input : ");
                System.out.println("1. Resistance ");
                System.out.println("2. Current ");
                System.out.println("3. Voltage ");
                System.out.println("4. Power ");
                int inputVariabels = input.nextInt();

                switch(inputVariabels) {
                    case 1 -> {
                        System.out.print("Please input amount Resistance (R) : ");
                        double inputResistance = input.nextDouble();
                        electricityVariabel.setResistance(inputResistance);
                        inputValue.put("R", electricityVariabel.getResistance());
                        inputCode.add("R");

                    }
                    case 2 -> {
                        System.out.print("Please input amount Ampere (I) : ");
                        double inputAmpere = input.nextDouble();
                        electricityVariabel.setAmpere(inputAmpere);
                        inputValue.put("A", electricityVariabel.getAmpere());
                        inputCode.add("I");
                    }
                    case 3 -> {
                        System.out.println("Please input amount Voltage (V) : ");
                        double inputVolt = input.nextDouble();
                        electricityVariabel.setVolt(inputVolt);
                        inputValue.put("V", electricityVariabel.getVolt());
                        inputCode.add("V");
                    }
                    case 4 -> {
                        System.out.println("Please input amount Watt (P) : ");
                        double inputWatt = input.nextDouble();
                        electricityVariabel.setWatt(inputWatt);
                        inputValue.put("P", electricityVariabel.getWatt());
                        inputCode.add("W");
                    }
                    default -> System.out.println("Please input only 1-4");
                }


                if(inputValue.size() == 2) {
                    break;
                }
            }


            if(inputCode.get(0).equals("R") && inputCode.get(1).equals("I") ||
                    inputCode.get(1).equals("R") && inputCode.get(0).equals("I")){

                double volt =  calculator.calculateVolt(1, electricityVariabel.getAmpere(),
                        electricityVariabel.getResistance(), 0);
                double watt = calculator.calculateWatt(3, electricityVariabel.getAmpere(),
                        electricityVariabel.getResistance(), 0);

                System.out.println("Resistance  : " + electricityVariabel.getResistance());
                System.out.println("Current     : " + electricityVariabel.getAmpere());
                System.out.println("Voltage     : " + volt);
                System.out.println("Watt        : " + watt);

            } else if(inputCode.get(0).equals("R") && inputCode.get(1).equals("V") ||
                    inputCode.get(1).equals("R") && inputCode.get(0).equals("V")) {

                double ampere = calculator.calculateAmp(1, electricityVariabel.getVolt(),
                        electricityVariabel.getResistance(), 0);

                double watt = calculator.calculateWatt(2, 0,
                        electricityVariabel.getResistance(), electricityVariabel.getVolt());

                System.out.println("Resistance  : " + electricityVariabel.getResistance());
                System.out.println("Current     : " + ampere);
                System.out.println("Voltage     : " + electricityVariabel.getVolt());
                System.out.println("Watt        : " + watt);

            } else if(inputCode.get(0).equals("R") && inputCode.get(1).equals("W") ||
                    inputCode.get(1).equals("R") && inputCode.get(0).equals("W")){

                double ampere = calculator.calculateAmp(3, 0,
                        electricityVariabel.getResistance(), electricityVariabel.getWatt());

                double volt = calculator.calculateVolt(3, 0,
                        electricityVariabel.getResistance(), electricityVariabel.getWatt());

                System.out.println("Resistance  : " + electricityVariabel.getResistance());
                System.out.println("Current     : " + ampere);
                System.out.println("Voltage     : " + volt);
                System.out.println("Watt        : " + electricityVariabel.getWatt());

            } else if(inputCode.get(0).equals("I") && inputCode.get(1).equals("V") ||
                    inputCode.get(1).equals("I") && inputCode.get(0).equals("V")) {

                double resistance = calculator.calculateResistance(1,
                        electricityVariabel.getVolt(), electricityVariabel.getAmpere(), 0);

                double watt = calculator.calculateWatt(1, electricityVariabel.getAmpere(),
                        0, electricityVariabel.getVolt());

                System.out.println("Resistance  : " + resistance);
                System.out.println("Current     : " + electricityVariabel.getAmpere());
                System.out.println("Voltage     : " + electricityVariabel.getVolt());
                System.out.println("Watt        : " + watt);

            } else if(inputCode.get(0).equals("I") && inputCode.get(1).equals("W") ||
                    inputCode.get(1).equals("I") && inputCode.get(0).equals("W")){

                double resistance = calculator.calculateResistance(3,0,
                        electricityVariabel.getAmpere(), electricityVariabel.getWatt());

                double volt = calculator.calculateVolt(2, electricityVariabel.getAmpere(),
                        0, electricityVariabel.getWatt());

                System.out.println("Resistance  : " + resistance);
                System.out.println("Current     : " + electricityVariabel.getAmpere());
                System.out.println("Voltage     : " + volt);
                System.out.println("Watt        : " + electricityVariabel.getWatt());

            } else if(inputCode.get(0).equals("V") && inputCode.get(1).equals("W") ||
                    inputCode.get(1).equals("V") && inputCode.get(0).equals("W")) {

                double resistance = calculator.calculateResistance(2, electricityVariabel.getVolt(),
                        0, electricityVariabel.getWatt());

                double ampere = calculator.calculateAmp(2, electricityVariabel.getVolt(),
                        0, electricityVariabel.getWatt());

                System.out.println("Resistance  : " + resistance);
                System.out.println("Current     : " + ampere);
                System.out.println("Voltage     : " + electricityVariabel.getVolt());
                System.out.println("Watt        : " + electricityVariabel.getWatt());

            } else {
                System.out.println("Data not found");
            }

            Scanner inputProcess = new Scanner(System.in);
            System.out.println("Calculate Again? Y/N");
            String values = inputProcess.nextLine();
            if(values.equals("Y")) {
                inputValue.clear();
                inputCode.clear();
            } else {
                break;
            }
        }


    }


}
