package service;

import model.ElectricityVariabel;

public class Calculator {

    public double calculateResistance(int id, double volt, double ampere, double watt){
            if(id == 1){
                return volt / ampere;
            } else if(id == 2){
                return Math.pow(volt,2) / watt;
            } else if(id == 3){
                return watt / Math.pow(ampere, 2);
            } else {
                return 0;
            }
    }

    public double calculateAmp(int id, double volt, double resistance, double watt){
        if(id == 1){
            return volt / resistance;
        } else if(id == 2){
            return watt / volt;
        } else if(id == 3){
            return Math.sqrt(watt/resistance);
        } else {
            return 0;
        }
    }

    public double calculateVolt(int id, double ampere, double resistance, double watt){
        if(id == 1){
            return ampere * resistance;
        } else if(id == 2){
            return watt / ampere;
        } else if(id == 3){
            return Math.sqrt(watt * resistance);
        } else {
            return 0;
        }
    }

    public double calculateWatt(int id, double ampere, double resistance, double volt){
        if(id == 1){
            return volt * ampere;
        } else if(id == 2){
            return Math.pow(volt, 2) / resistance;
        } else if(id == 3){
            return Math.pow(ampere, 2) * resistance;
        } else {
            return 0;
        }
    }

}
