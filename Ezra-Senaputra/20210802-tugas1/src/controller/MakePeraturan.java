package controller;

import model.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class MakePeraturan {

    private int noTLN;
    private int noLN;
    private LocalDate tanggalDiundangkan;
    private LocalDate tanggalDiterapkan;
    private String tentangPeraturan;
    private String jenisPeraturan;
    private int noPeraturan;
    private LocalDate tahunPeraturan;
    private ArrayList<Menimbang> listMenimbang;
    private ArrayList<Mengingat> listMengingat;
    private ArrayList<Menetapkan> listMenetapkan;

    public int getNoTLN() {
        return noTLN;
    }

    public int getNoLN() {
        return noLN;
    }

    public LocalDate getTanggalDiundangkan() {
        return tanggalDiundangkan;
    }

    public LocalDate getTanggalDiterapkan() {
        return tanggalDiterapkan;
    }

    public String getTentangPeraturan() {
        return tentangPeraturan;
    }

    public String getJenisPeraturan() {
        return jenisPeraturan;
    }

    public int getNoPeraturan() {
        return noPeraturan;
    }

    public LocalDate getTahunPeraturan() {
        return tahunPeraturan;
    }

    public ArrayList<Menimbang> getListMenimbang() {
        return listMenimbang;
    }

    public ArrayList<Mengingat> getListMengingat() {
        return listMengingat;
    }

    public ArrayList<Menetapkan> getListMenetapkan() {
        return listMenetapkan;
    }

    public void buatPeraturan(){
        Peraturan peraturan = new Peraturan();
        peraturan.setNoLN(178);
        noLN = peraturan.getNoLN();

        peraturan.setNoTLN(5341);
        noTLN = peraturan.getNoTLN();

        peraturan.setTanggalDiundangkan(LocalDate.of(2012, 8, 22 ));
        tanggalDiundangkan = peraturan.getTanggalDiundangkan();

        peraturan.setTanggalDiterapkan(LocalDate.of(2012, 8, 22 ));
        tanggalDiterapkan = peraturan.getTanggalDiterapkan();

        peraturan.setTentangPeraturan("PERTANGGUNGJAWABAN ATAS PELAKSANAAN\n" +
                "ANGGARAN PENDAPATAN DAN BELANJA NEGARA\n" +
                "TAHUN ANGGARAN 2011");
        tentangPeraturan = peraturan.getTentangPeraturan();

        peraturan.setJenisPeraturan("KEUANGAN NEGARA");
        jenisPeraturan = peraturan.getJenisPeraturan();

        peraturan.setNoPeraturan(14);
        noPeraturan = peraturan.getNoPeraturan();

        peraturan.setTahunPeraturan(LocalDate.of(2012,8,22));
        tahunPeraturan = peraturan.getTahunPeraturan();

        peraturan.setListMenimbang(buatMenimbang());
        listMenimbang = peraturan.getListMenimbang();

        peraturan.setListMengingat(buatMengingat());
        listMengingat = peraturan.getListMengingat();

        peraturan.setListMenetapkan(buatMenetapkan());
        listMenetapkan = peraturan.getListMenetapkan();

    }

    public ArrayList<Menimbang> buatMenimbang(){
        Menimbang pointA = new Menimbang();
        pointA.setMenimbangId('a');
        pointA.setMenimbangDeskripsi("bahwa Anggaran Pendapatan dan Belanja Negara (APBN)\n" +
                "Tahun Anggaran 2011 yang diundangkan berdasarkan\n" +
                "Undang-Undang Nomor 10 Tahun 2010 sebagaimana\n" +
                "telah diubah dengan Undang-Undang Nomor 11 Tahun\n" +
                "2011 tentang Perubahan atas Undang-Undang Nomor 10\n" +
                "Tahun 2010, pelaksanaannya perlu dilakukan\n" +
                "pemeriksaan dan dipertanggungjawabkan sesuai UndangUndang Nomor 15 Tahun 2004 tentang Pemeriksaan\n" +
                "Pengelolaan dan Tanggung Jawab Keuangan Negara; ");

        Menimbang pointB = new Menimbang();
        pointB.setMenimbangId('b');
        pointB.setMenimbangDeskripsi("bahwa sesuai dengan ketentuan Pasal 30 ayat (1)\n" +
                "Undang-Undang Nomor 17 Tahun 2003 tentang\n" +
                "Keuangan Negara dan Pasal 4 ayat (2) Undang-Undang\n" +
                "Nomor 15 Tahun 2004 tentang Pemeriksaan Pengelolaan\n" +
                "dan Tanggung Jawab Keuangan Negara, terhadap\n" +
                "pelaksanaan APBN Tahun Anggaran 2011 telah dilakukan\n" +
                "pemeriksaan oleh Badan Pemeriksa Keuangan (BPK);");

        ArrayList<Menimbang> listMenimbang = new ArrayList<>();
        listMenimbang.add(pointA);
        listMenimbang.add(pointB);
        return listMenimbang;
    }

    public ArrayList<Mengingat> buatMengingat(){
        Mengingat point1 = new Mengingat();
        point1.setPointMengingat(1);
        point1.setIsiMengingat("Pasal 5 ayat (1), Pasal 20 ayat (1), ayat (2) dan ayat (5),\n" +
                "Pasal 23 ayat (1) dan Pasal 23E Undang-Undang Dasar\n" +
                "Negara Republik Indonesia Tahun 1945;");

        Mengingat point2 = new Mengingat();
        point2.setPointMengingat(2);
        point2.setIsiMengingat("Undang-Undang Nomor 17 Tahun 2003 tentang\n" +
                "Keuangan Negara (Lembaran Negara Republik Indonesia\n" +
                "Tahun 2003 Nomor 47, Tambahan Lembaran Negara\n" +
                "Republik Indonesia Nomor 4286); ");

        ArrayList<Mengingat> listMengingat = new ArrayList<>();
        listMengingat.add(point1);
        listMengingat.add(point2);
        return listMengingat;
    }

    public ArrayList<Menetapkan> buatMenetapkan(){
        Menetapkan menetapkan = new Menetapkan();
        menetapkan.setMenetapkanId(1);
        menetapkan.setMenetapkanDeskripsi("UNDANG-UNDANG TENTANG PERTANGGUNGJAWABAN\n" +
                "ATAS PELAKSANAAN ANGGARAN PENDAPATAN DAN\n" +
                "BELANJA NEGARA TAHUN ANGGARAN 2011.");
        menetapkan.setListPasal(buatPasal());
        ArrayList<Menetapkan> listMenetapkan = new ArrayList<>();
        listMenetapkan.add(menetapkan);
        return listMenetapkan;
    }

    public ArrayList<Pasal> buatPasal(){
        Pasal pasal1 = new Pasal();
        pasal1.setPasalId(1);
        pasal1.setListSubPasal(buatSubPasal());

        ArrayList<Pasal> listPasal = new ArrayList<>();
        listPasal.add(pasal1);
        return listPasal;
    }

    public ArrayList<SubPasal> buatSubPasal(){
        SubPasal subPasal1 = new SubPasal();
        subPasal1.setIsiSubPAsal("Pertanggungjawaban atas pelaksanaan APBN Tahun Anggaran 2011\n" +
                "tertuang dalam Laporan Keuangan Pemerintah Pusat Tahun 2011\n" +
                "sebagaimana ditetapkan dalam Lampiran Undang-Undang ini.");

        ArrayList<SubPasal> listSubPasal = new ArrayList<>();
        listSubPasal.add(subPasal1);
        return listSubPasal;
    }
}
