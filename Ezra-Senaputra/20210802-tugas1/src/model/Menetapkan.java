package model;

import java.util.ArrayList;

public class Menetapkan {
    private ArrayList<Pasal> listPasal;
    private int menetapkanId;
    private String menetapkanDeskripsi;

    public String getMenetapkanDeskripsi() {
        return menetapkanDeskripsi;
    }

    public void setMenetapkanDeskripsi(String menetapkanDeskripsi) {
        this.menetapkanDeskripsi = menetapkanDeskripsi;
    }

    public ArrayList<Pasal> getListPasal() {
        return listPasal;
    }

    public void setListPasal(ArrayList<Pasal> listPasal) {
        this.listPasal = listPasal;
    }

    public int getMenetapkanId() {
        return menetapkanId;
    }

    public void setMenetapkanId(int menetapkanId) {
        this.menetapkanId = menetapkanId;
    }
}
