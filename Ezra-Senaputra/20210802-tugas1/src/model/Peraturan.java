package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Peraturan {
    private int noTLN;
    private int noLN;
    private LocalDate tanggalDiundangkan;
    private LocalDate tanggalDiterapkan;
    private String tentangPeraturan;
    private String jenisPeraturan;
    private int noPeraturan;
    private LocalDate tahunPeraturan;
    private ArrayList<Menimbang> listMenimbang;
    private ArrayList<Mengingat> listMengingat;
    private ArrayList<Menetapkan> listMenetapkan;

    public int getNoTLN() {
        return noTLN;
    }

    public void setNoTLN(int noTLN) {
        this.noTLN = noTLN;
    }

    public int getNoLN() {
        return noLN;
    }

    public void setNoLN(int noLN) {
        this.noLN = noLN;
    }

    public LocalDate getTanggalDiundangkan() {
        return tanggalDiundangkan;
    }

    public void setTanggalDiundangkan(LocalDate tanggalDiundangkan) {
        this.tanggalDiundangkan = tanggalDiundangkan;
    }

    public LocalDate getTanggalDiterapkan() {
        return tanggalDiterapkan;
    }

    public void setTanggalDiterapkan(LocalDate tanggalDiterapkan) {
        this.tanggalDiterapkan = tanggalDiterapkan;
    }

    public String getTentangPeraturan() {
        return tentangPeraturan;
    }

    public void setTentangPeraturan(String tentangPeraturan) {
        this.tentangPeraturan = tentangPeraturan;
    }

    public String getJenisPeraturan() {
        return jenisPeraturan;
    }

    public void setJenisPeraturan(String jenisPeraturan) {
        this.jenisPeraturan = jenisPeraturan;
    }

    public int getNoPeraturan() {
        return noPeraturan;
    }

    public void setNoPeraturan(int noPeraturan) {
        this.noPeraturan = noPeraturan;
    }

    public LocalDate getTahunPeraturan() {
        return tahunPeraturan;
    }

    public void setTahunPeraturan(LocalDate tahunPeraturan) {
        this.tahunPeraturan = tahunPeraturan;
    }

    public ArrayList<Menimbang> getListMenimbang() {
        return listMenimbang;
    }

    public void setListMenimbang(ArrayList<Menimbang> listMenimbang) {
        this.listMenimbang = listMenimbang;
    }

    public ArrayList<Mengingat> getListMengingat() {
        return listMengingat;
    }

    public void setListMengingat(ArrayList<Mengingat> listMengingat) {
        this.listMengingat = listMengingat;
    }

    public ArrayList<Menetapkan> getListMenetapkan() {
        return listMenetapkan;
    }

    public void setListMenetapkan(ArrayList<Menetapkan> listMenetapkan) {
        this.listMenetapkan = listMenetapkan;
    }
}
