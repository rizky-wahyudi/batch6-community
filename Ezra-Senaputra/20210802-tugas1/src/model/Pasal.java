package model;

import java.util.ArrayList;

public class Pasal {
    private ArrayList<SubPasal> listSubPasal;
    private int pasalId;

    public ArrayList<SubPasal> getListSubPasal() {
        return listSubPasal;
    }

    public void setListSubPasal(ArrayList<SubPasal> listSubPasal) {
        this.listSubPasal = listSubPasal;
    }

    public int getPasalId() {
        return pasalId;
    }

    public void setPasalId(int pasalId) {
        this.pasalId = pasalId;
    }
}
