package model;

public class Menimbang {
    private char menimbangId;
    private String menimbangDeskripsi;

    public char getMenimbangId() {
        return menimbangId;
    }

    public void setMenimbangId(char menimbangId) {
        this.menimbangId = menimbangId;
    }

    public String getMenimbangDeskripsi() {
        return menimbangDeskripsi;
    }

    public void setMenimbangDeskripsi(String menimbangDeskripsi) {
        this.menimbangDeskripsi = menimbangDeskripsi;
    }
}
