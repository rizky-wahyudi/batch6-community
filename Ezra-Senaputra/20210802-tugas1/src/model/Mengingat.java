package model;

public class Mengingat {
    private int pointMengingat;
    private String isiMengingat;

    public int getPointMengingat() {
        return pointMengingat;
    }

    public void setPointMengingat(int pointMengingat) {
        this.pointMengingat = pointMengingat;
    }

    public String getIsiMengingat() {
        return isiMengingat;
    }

    public void setIsiMengingat(String isiMengingat) {
        this.isiMengingat = isiMengingat;
    }
}
