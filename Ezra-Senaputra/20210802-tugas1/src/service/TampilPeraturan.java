package service;

import controller.MakePeraturan;

import java.util.Scanner;

public class TampilPeraturan {
    public void tampil(){
        while(true) {
            Scanner input = new Scanner(System.in);
            MakePeraturan makePeraturan = new MakePeraturan();
            makePeraturan.buatPeraturan();

            System.out.println("==== PERATURAN INDONESIA ====");
            System.out.println("No LN           : " + makePeraturan.getNoLN());
            System.out.println("No TLN          : " + makePeraturan.getNoTLN());
            System.out.println("No Peraturan    : " + makePeraturan.getNoPeraturan());
            System.out.println("Jenis Peraturan : " + makePeraturan.getJenisPeraturan());
            System.out.println("Tentang         : " + makePeraturan.getTentangPeraturan());
            System.out.println("Tahun Peraturan : " + makePeraturan.getTahunPeraturan());

            System.out.println("=====================================================");
            System.out.println("1. Menimbang");
            System.out.println("2. Mengingat");
            System.out.println("3. Menetapkan");
            System.out.print("");
            System.out.print("Pilih Detail (1-3) : ");
            String inputDetail = input.nextLine();

            if(inputDetail.equals("1")) {
                for(int i = 0; i < makePeraturan.getListMengingat().size(); i++) {
                    int point = makePeraturan.getListMengingat().get(i).getPointMengingat();
                    String isi = makePeraturan.getListMengingat().get(i).getIsiMengingat();
                    System.out.println(point + ". " + isi);
                }

                System.out.println("Apakah ingin melihat lagi? Y/X");
                String inputProcess = input.nextLine();
                if(inputProcess.equals("X")){
                    break;
                } else {

                }

            } else if(inputDetail.equals("2")) {
                for(int i = 0; i < makePeraturan.getListMenimbang().size(); i++) {
                    int point = makePeraturan.getListMenimbang().get(i).getMenimbangId();
                    String isi = makePeraturan.getListMenimbang().get(i).getMenimbangDeskripsi();
                    System.out.println(point + ". " + isi);
                }

                System.out.println("Apakah ingin melihat lagi? Y/X");
                String inputProcess = input.nextLine();
                if(inputProcess.equals("X")){
                    break;
                } else {

                }

            } else if(inputDetail.equals("3")) {
                String desc = makePeraturan.getListMenetapkan().get(0).getMenetapkanDeskripsi();
                System.out.println(desc);
                for(int i = 0; i < makePeraturan.getListMenetapkan().size(); i++) {

                    for (int j = 0; j < makePeraturan.getListMenetapkan().get(i).getListPasal().size(); j++) {
                        int pasalId = makePeraturan.getListMenetapkan().get(i).getListPasal().get(j).getPasalId();

                        System.out.println("Pasal " + pasalId);

                        int subPasal = makePeraturan.getListMenetapkan().get(i).getListPasal().get(j).
                                getListSubPasal().size();
                        for (int k = 0; k < subPasal; k++) {
                            int z = 1;
                            String sub = makePeraturan.getListMenetapkan().get(i).getListPasal().get(j).
                                    getListSubPasal().get(k).getIsiSubPAsal();
                            System.out.println(z + k + ". " + sub );
                        }
                    }
                }

                System.out.println("Apakah ingin melihat lagi? Y/X");
                String inputProcess = input.nextLine();
                if(inputProcess.equals("X")){
                    break;
                } else {

                }

            }
        }

    }
}
