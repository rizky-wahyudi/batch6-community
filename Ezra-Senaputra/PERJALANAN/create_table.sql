/* CREATE DATABASE perjalanan; */
/* USE perjalanan; */

CREATE TABLE pelanggan(
	id INT NOT NULL AUTO_INCREMENT,
    nama VARCHAR(50),
    umur INT,
    alamat VARCHAR(200),
    document_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE transportasi(
	id INT NOT NULL AUTO_INCREMENT,
    tipe VARCHAR(25),
    name VARCHAR(20),
    brand_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE brand(
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(25),
    PRIMARY KEY (id)
);

CREATE TABLE document(
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20),
    number_id VARCHAR(20),
    pelanggan_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE bagasi(
	id INT NOT NULL AUTO_INCREMENT,
    transportasi_id INT,
    amount INT,
    PRIMARY KEY (id)
);

CREATE TABLE tiket (
	id INT NOT NULL AUTO_INCREMENT,
    artNumber VARCHAR(10),
    tujuan VARCHAR(50),
    dari VARCHAR(50),
    transportasi_id INT,
    pelanggan_id INT,
    bagasi_id INT,
    PRIMARY KEY (id)
);