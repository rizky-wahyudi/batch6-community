
INSERT INTO `perjalanan`.`transportasi` (`id`, `tipe`, `name`, `brand_id`) VALUES ('1', 'pesawat', 'LION-AIRBUS-2021', '1');
INSERT INTO `perjalanan`.`transportasi` (`id`, `tipe`, `name`, `brand_id`) VALUES ('2', 'kereta', 'Ciremai ', '2');
INSERT INTO `perjalanan`.`transportasi` (`id`, `tipe`, `name`, `brand_id`) VALUES ('3', 'kapal', 'Seas Harmony', '3');


INSERT INTO `perjalanan`.`brand` (`id`, `name`) VALUES ('1', 'LION-AIR');
INSERT INTO `perjalanan`.`brand` (`id`, `name`) VALUES ('2', 'CI-KA');
INSERT INTO `perjalanan`.`brand` (`id`, `name`) VALUES ('3', 'Pearl Seas');

INSERT INTO `perjalanan`.`pelanggan` (`id`, `nama`, `umur`, `alamat`, `document_id`) VALUES ('1', 'Rimuru', '20', 'Jakarta Timur', '1');
INSERT INTO `perjalanan`.`pelanggan` (`id`, `nama`, `umur`, `alamat`, `document_id`) VALUES ('2', 'Roland', '24', 'Tokyo', '2');
INSERT INTO `perjalanan`.`pelanggan` (`id`, `nama`, `umur`, `alamat`, `document_id`) VALUES ('3', 'Matthew', '30', 'Panama', '3');

INSERT INTO `perjalanan`.`document` (`id`, `name`, `number_id`, `pelanggan_id`) VALUES ('1', 'ID_CARD', '3908080898074561', '1');
INSERT INTO `perjalanan`.`document` (`id`, `name`, `number_id`, `pelanggan_id`) VALUES ('2', 'PASSPORT', 'TK908721', '2');
INSERT INTO `perjalanan`.`document` (`id`, `name`, `number_id`, `pelanggan_id`) VALUES ('3', 'ID_CARD', '4008080898074561', '3');

INSERT INTO `perjalanan`.`bagasi` (`id`, `transportasi_id`, `amount`) VALUES ('1', '1', '20');
INSERT INTO `perjalanan`.`bagasi` (`id`, `transportasi_id`, `amount`) VALUES ('2', '2', '10');
INSERT INTO `perjalanan`.`bagasi` (`id`, `transportasi_id`, `amount`) VALUES ('3', '3', '50');

INSERT INTO `perjalanan`.`tiket` (`id`, `artNumber`, `tujuan`, `dari`, `transportasi_id`, `pelanggan_id`, `bagasi_id`) VALUES ('1', 'KPLT001', 'Palembang', 'Jakarta', '3', '1', '3');
INSERT INTO `perjalanan`.`tiket` (`id`, `artNumber`, `tujuan`, `dari`, `transportasi_id`, `pelanggan_id`, `bagasi_id`) VALUES ('2', 'PSWT001', 'New York', 'Tokyo', '1', '2', '1');
INSERT INTO `perjalanan`.`tiket` (`id`, `artNumber`, `tujuan`, `dari`, `transportasi_id`, `pelanggan_id`, `bagasi_id`) VALUES ('3', 'KRTA001', 'Jakarta', 'Surabaya', '2', '3', '2');

SELECT * FROM pelanggan;
SELECT * FROM transportasi;
SELECT * FROM brand;
SELECT * FROM document;
SELECT * FROM bagasi;
SELECT * FROM tiket;
