import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Row row = new Row();

        System.out.println("SILAHKAN PILIH ITEM DI ROW");
        for(var i = 1; i <= 6; i++){
            System.out.print(i + ". ");
            row.getItemList(i);
        }

        System.out.println("Input Row : ");
        int inRow = input.nextInt();

        System.out.println("Input Slot : ");
        int inSlot = input.nextInt();

        row.setRowValue(inRow);

        Slots slots = new Slots();
        slots.setSlotsValue(inSlot);

        Price price = new Price();
        System.out.println("Price to be paid :");
        if(price.getPrice(row.getRowValue(), slots.getSlotsValue()) != 0) {
            System.out.println(price.getPrice(row.getRowValue(), slots.getSlotsValue()));
            System.out.print("Insert Money : ");
            int insertMoney = input.nextInt();
            if(insertMoney > price.getPrice(row.getRowValue(), slots.getSlotsValue())) {
                int result = insertMoney - price.getPrice(row.getRowValue(), slots.getSlotsValue());
                System.out.println("Your change : " + result);
                System.out.print("You already buy : ");
                row.getItemList(row.getRowValue());
                System.out.print("Slot : ");
                System.out.println(slots.getSlotsValue());
            } else if(insertMoney < price.getPrice(row.getRowValue(), slots.getSlotsValue())) {
                System.out.println("Not Enough Money");
            } else if(insertMoney == price.getPrice(row.getRowValue(), slots.getSlotsValue())) {
                System.out.println("Thanks");
                System.out.print("You already buy : ");
                row.getItemList(row.getRowValue());
                System.out.print("Slot : ");
                System.out.println(slots.getSlotsValue());
            } else {
                System.out.println("Money not detected");
            }
        } else {
            System.out.println("Row & Slot not found");
        }


    }
}
