import java.util.List;

public class Row implements ListItem {
    private int rowValue;

    public int getRowValue() {
        return rowValue;
    }

    public void setRowValue(int rowValue) {
        this.rowValue = rowValue;
    }

    public void getItemList(int value){
        switch (value) {
            case 1 :
                System.out.println(ListItem.row1);
                break;
            case 2 :
                System.out.println(ListItem.row2);
                break;
            case 3 :
                System.out.println(ListItem.row3);
                break;
            case 4 :
                System.out.println(ListItem.row4);
                break;
            case 5 :
                System.out.println(ListItem.row5);
                break;
            case 6 :
                System.out.println(ListItem.row6);
                break;
            default:
                System.out.println("Can't Found The Item");
        }
    }
}
