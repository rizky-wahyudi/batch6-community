public interface PriceList {
     int chips = 10000;
     int chocolates = 5000;
     int candies = 500;
     int water = 2000;
     int softDrinks = 4000;
     int iceTea = 3000;
}
