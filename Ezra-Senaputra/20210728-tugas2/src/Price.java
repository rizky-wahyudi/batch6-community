public class Price implements  PriceList{
    public int getPrice(int row, int slot){


        if(row == 1){
            return PriceList.chips + (5000 * slot);
        } else if(row == 2){
            return PriceList.chocolates + (1000 * slot);
        } else if(row == 3){
            return PriceList.candies + (500 * slot);
        } else if(row == 4){
            return PriceList.water + (1000 * slot);
        } else if(row == 5){
            return PriceList.softDrinks + (1500 * slot);
        } else if(row == 6){
            return PriceList.iceTea + (1200 * slot);
        } else {
            return 0;
        }


    }
}
