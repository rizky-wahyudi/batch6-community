public class Economy implements Price, Seats, Luggage {
    private double seats = 0.65;
    private double priceEA = 1 * Price.europeAsia;
    private double priceAE = 1 * Price.asiaEurope;
    private double priceEU = 1 * Price.europeUS;
    private double laguage = 1 * Luggage.laguage;

    public void setSeats(String nameAirPlane){
        if(nameAirPlane.equals("Airbus380")){
            this.seats = this.seats * Seats.airbus380;
        } else if(nameAirPlane.equals("Boeing747")){
            this.seats = this.seats * Seats.boeing747;
        } else if(nameAirPlane.equals("Boeing787")){
            this.seats = this.seats * Seats.boeing787;
        } else {
            this.seats = 0;
            System.out.println("Masukkan Kode Pesawat Yang Tepat");
        }
    }

    public double getSeats(){
        return this.seats;
    }


    public double getPriceEA(){
        return this.priceEA;
    }
    

    public double getPriceAE(){
        return this.priceAE;
    }


    public double getPriceEU(){
        return this.priceEU;
    }

    public double getProfitEA(){
        return this.seats * this.priceEA; 
    }
    public double getProfitAE(){
        return this.seats * this.priceAE;
    }
    public double getProfitEU(){
        return this.seats * this.priceEU;
    }

    public double getLaguage(){
        return this.laguage;
    } 
   
}