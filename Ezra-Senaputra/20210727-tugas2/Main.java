import java.util.Scanner; 

public class Main {
    public static void main(String args[]){
        Scanner input = new Scanner(System.in);
        System.out.println("Pilih Maskapai : ");
        System.out.println("1. Airbus 380");
        System.out.println("2. Boeing 747");
        System.out.println("3. Boeing 787");
        String in = input.nextLine();

        if(in.equals("1")){
            System.out.println("Masukkan jumlah penerbangan dalam 1 bulan : ");
            System.out.print("1. Europe - Asia : ");
            double inEA = input.nextDouble();
            System.out.print("2. Asia - Europe : ");
            double inAE = input.nextDouble();
            System.out.print("3. Europe - US : ");
            double inEU = input.nextDouble();
            getProfit("Airbus380", inEA, inAE, inEU);
        } else if(in.equals("2")){
            System.out.println("Masukkan jumlah penerbangan dalam 1 bulan : ");
            System.out.print("1. Europe - Asia : ");
            double inEA = input.nextDouble();
            System.out.print("2. Asia - Europe : ");
            double inAE = input.nextDouble();
            System.out.print("3. Europe - US : ");
            double inEU = input.nextDouble();
            getProfit("Boeing747", inEA, inAE, inEU);
        } else if(in.equals("3")){
            System.out.println("Masukkan jumlah penerbangan dalam 1 bulan : ");
            System.out.print("1. Europe - Asia : ");
            double inEA = input.nextDouble();
            System.out.print("2. Asia - Europe : ");
            double inAE = input.nextDouble();
            System.out.print("3. Europe - US : ");
            double inEU = input.nextDouble();
            getProfit("Boeing787", inEA, inAE, inEU);
        }

    }

    public static void getProfit(String airPlaneName, double totalEA, double totalAE, double totalEU){
        AirPlane airPlane = new AirPlane();
        Economy e = new Economy();
        Bussiness b = new Bussiness();
        FirstClass f = new FirstClass();
        Scanner input = new Scanner(System.in);

        airPlane.setName(airPlaneName);
        airPlane.setTotalFlightEuropeAsia(totalEA);
        airPlane.setTotalFlightAsiaEurope(totalAE);
        airPlane.setTotalFlightEuropeUS(totalEU);

        e.setSeats(airPlaneName);
        b.setSeats(airPlaneName);
        f.setSeats(airPlaneName);

        double totalSeats = e.getSeats() + b.getSeats() + f.getSeats();

        System.out.println("===== AIRPLANE ======");
        System.out.println("Plane Name  : " + airPlane.getName());
        System.out.println("Total Seats : " + totalSeats);

        System.out.println("Lihat Profit :");
        System.out.println("1. Europe - Asia");
        System.out.println("2. Asia - Europe");
        System.out.println("3. Europe - US");
        String in = input.nextLine();  

        if(in.equals("1")) {
          getProfitEuropeAsia(totalEA);
        } else if(in.equals("2")) {
          getProfitAsiaEurope(totalAE);
        } else if(in.equals("3")) {
          getProfitEuropeUS(totalEU);
        }
        
        
    } 

    public static void getProfitEuropeAsia(double totalEA){
        Economy e = new Economy();
        Bussiness b = new Bussiness();
        FirstClass f = new FirstClass();

        double total = (e.getProfitEA() + b.getProfitEA() + f.getProfitEA()) * totalEA;
        
        System.out.println("====== ECONOMY CLASS =====");
        System.out.println("Profit  : $" + (e.getProfitEA() * totalEA ));
        System.out.println("Laugage : " + e.getLaguage() + "kg");
        System.out.println("====== BUSSINESS CLASS =====");
        System.out.println("Profit : $" + (b.getProfitEA() * totalEA));
        System.out.println("Laugage : " + b.getLaguage() + "kg");
        System.out.println("====== FIRST CLASS =====");
        System.out.println("Profit : $" + (f.getProfitEA() * totalEA));
        System.out.println("Laugage : " + f.getLaguage() + "kg");
        System.out.println("==========================");
        System.out.println("TOTAL PROFIT : $" + total);
    }

    public static void getProfitAsiaEurope(double totalAE){
        Economy e = new Economy();
        Bussiness b = new Bussiness();
        FirstClass f = new FirstClass();

        double total = (e.getProfitAE() + b.getProfitAE() + f.getProfitAE()) * totalAE;
        
        System.out.println("====== ECONOMY CLASS =====");
        System.out.println("Profit : $" + (e.getProfitAE() * totalAE));
        System.out.println("Laugage : " + e.getLaguage() + "kg");
        System.out.println("====== BUSSINESS CLASS =====");
        System.out.println("Profit : $" + (b.getProfitAE() * totalAE));
        System.out.println("Laugage : " + b.getLaguage() + "kg");
        System.out.println("====== FIRST CLASS =====");
        System.out.println("Profit : $" + (f.getProfitAE() * totalAE));
        System.out.println("Laugage : " + f.getLaguage() + "kg");
        System.out.println("==========================");
        System.out.println("TOTAL PROFIT : $" + total);
    }

    public static void getProfitEuropeUS(double totalEU){
        Economy e = new Economy();
        Bussiness b = new Bussiness();
        FirstClass f = new FirstClass();

        double total = (e.getProfitEU() + b.getProfitEU() + f.getProfitEU()) * totalEU;
        
        System.out.println("====== ECONOMY CLASS =====");
        System.out.println("Profit : $" + (e.getProfitEU() * totalEU));
        System.out.println("Laugage : " + e.getLaguage() + "kg");
        System.out.println("====== BUSSINESS CLASS =====");
        System.out.println("Profit : $" + (b.getProfitEU() * totalEU));
        System.out.println("Laugage : " + b.getLaguage() + "kg");
        System.out.println("====== FIRST CLASS =====");
        System.out.println("Profit : $" + (f.getProfitEU() * totalEU));
        System.out.println("Laugage : " + f.getLaguage() + "kg");
        System.out.println("==========================");
        System.out.println("TOTAL PROFIT : $" + total);
    }
}