public class AirPlane {
    private String name;
    private double totalFlightEuropeAsia;
    private double totalFlightAsiaEurope;
    private double totalFlightEuropeUS;

    public void setName(String airPlaneName) {
        this.name = airPlaneName;
    }

    public String getName(){
        return this.name;
    }

    public void setTotalFlightEuropeAsia(double totalFlightPlaneEA){
        this.totalFlightEuropeAsia = totalFlightPlaneEA;
    }

    public double getTotalFlightEuropeAsia(){
        return this.totalFlightEuropeAsia;
    }

    public void setTotalFlightAsiaEurope(double totalFlightPlaneAE){
        this.totalFlightAsiaEurope = totalFlightPlaneAE;
    }

    public double getTotalFlightAsiaEurope(){
        return this.totalFlightAsiaEurope;
    }

    public void setTotalFlightEuropeUS(double totalFlightPlaneEUS){
        this.totalFlightEuropeUS = totalFlightPlaneEUS;
    }

    public double getTotalFlightEuropeUS(){
        return this.totalFlightEuropeUS;
    }

}