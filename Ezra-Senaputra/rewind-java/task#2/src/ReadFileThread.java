import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFileThread implements Runnable{
    public void run(){
        int min = 1;
        int max = 4;
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);
        readFile(random_int);
    }

    public static void readFile(int number){
        if(number == 1){
            try {
                File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                        "Bootcamp nexSOFT\\rewind-java\\task#2\\test1.txt");
                Scanner myReader = new Scanner(myObj);
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    System.out.println(data);
                }
                myReader.close();
            } catch (FileNotFoundException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        } else if (number == 2) {
           readTwoFiles();
        } else if(number == 3){
           readThreeFiles();
        } else if(number == 4){
            readFourFiles();
        } else {
            System.out.println("Numbering Error!");
        }
    }

    public static void readTwoFiles(){
        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test1.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test2.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void readThreeFiles(){
        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test1.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test2.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test3.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void readFourFiles(){
        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test1.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test2.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test3.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\rewind-java\\task#2\\test4.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
