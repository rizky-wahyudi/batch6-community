package command;

public class DroneControl {
    public static void main(String[] args) {

        Controller controller = new Controller();
        Drone drone = new Drone();

        Command moveUp = new DroneUpCommand(drone);
        Command moveDown = new DroneDownCommand(drone);

        controller.setCommand(moveDown);
        controller.executeCommand();

        controller.setCommand(moveUp);
        controller.executeCommand();

    }
}
