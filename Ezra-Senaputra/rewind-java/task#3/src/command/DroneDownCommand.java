package command;

public class DroneDownCommand implements Command {

    private Drone drone;

    public DroneDownCommand(Drone drone){
        this.drone = drone;
    }

    @Override
    public void execute() {
        drone.moveDown();
    }
}
