package command;

public class Drone {

    public void moveUp(){
        System.out.println("Drone Move Up");
    }

    public void moveDown(){
        System.out.println("Drone Move Down");
    }
}
