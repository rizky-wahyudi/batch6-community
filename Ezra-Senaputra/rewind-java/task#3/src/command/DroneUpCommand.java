package command;

public class DroneUpCommand implements Command{
    private Drone drone;

    public DroneUpCommand(Drone drone){
        this.drone = drone;
    }

    @Override
    public void execute() {
        drone.moveUp();
    }
}
