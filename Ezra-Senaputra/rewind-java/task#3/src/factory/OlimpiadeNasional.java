package factory;

import java.util.Scanner;

public class OlimpiadeNasional {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Cabang Olimpiade Sains : ");
        String cabang = input.nextLine();

        Olimpiade olim = OlimpiadeFactory.newInstance(cabang);

        System.out.print("Input Jumlah Peserta : ");
        int jumlah = input.nextInt();

        if(olim instanceof Physics){
            getPhyics(jumlah);
        } else if(olim instanceof Economics){
            getEconomics(jumlah);
        } else if(olim instanceof Math){
            getMath(jumlah);
        } else {
            System.out.println("Error!");
        }

    }

    public static void getEconomics(int jumlah){
        Economics economic = new Economics();
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan Nama Pemenang :");
        String pemenang = input.nextLine();

        economic.setPemenang(pemenang);
        economic.setJumlahPeserta(jumlah);
        System.out.println("Masukkan Score : ");
        int score = input.nextInt();
        economic.setScore(score);

        System.out.println("== REPORT == ");
        System.out.println("Jumlah Peserta Economics : " + economic.getJumlahPeserta());
        System.out.println("Pemenang Economics       : " + economic.getPemenang());
        System.out.println("Skor                     : " + economic.getScore() );

    }

    public static void getPhyics(int jumlah){
        Physics physics = new Physics();
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan Nama Pemenang :");
        String pemenang = input.nextLine();

        physics.setPemenang(pemenang);
        physics.setJumlahPeserta(jumlah);
        System.out.println("Masukkan Score : ");
        int score = input.nextInt();
        physics.setScore(score);

        System.out.println("== REPORT == ");
        System.out.println("Jumlah Peserta Physics : " + physics.getJumlahPeserta());
        System.out.println("Pemenang Physics       : " + physics.getPemenang());
        System.out.println("Skor                   : " + physics.getScore() );

    }

    public static void getMath(int jumlah){
        Math math = new Math();
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan Nama Pemenang :");
        String pemenang = input.nextLine();

        math.setPemenang(pemenang);
        math.setJumlahPeserta(jumlah);
        System.out.println("Masukkan Score : ");
        int score = input.nextInt();
        math.setScore(score);

        System.out.println("== REPORT == ");
        System.out.println("Jumlah Peserta Math : " + math.getJumlahPeserta());
        System.out.println("Pemenang Math       : " + math.getPemenang());
        System.out.println("Skor                : " + math.getScore() );

    }
}
