package factory;

public class OlimpiadeFactory {
    public static Olimpiade newInstance(String type){
        if(type.equals("Math")){
            return new Math();
        } else if(type.equals("Physics")){
            return new Physics();
        } else if(type.equals("Economics")){
            return new Economics();
        } else {
            return null;
        }
    }
}
