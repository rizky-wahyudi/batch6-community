package observer;

public class Student {
    private String name;
    private Physics physics = new Physics();

    public Student(String name) {
        this.name = name;
    }

    public void updateLesson(){
        System.out.println("There is new Task From Physics");
    }

    public void getLesson(Physics ph){
        physics = ph;
    }

}
