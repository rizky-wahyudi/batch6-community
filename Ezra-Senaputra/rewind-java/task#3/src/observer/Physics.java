package observer;

import java.util.ArrayList;
import java.util.List;

public class Physics {
    private List<Student> student = new ArrayList();
    private String title;

    public void addStudent(Student std){
        student.add(std);
    }

    public void removeStudent(Student std){
        student.remove(std);
    }

    public void notifyStudent(){
        for(Student stud : student){
            stud.updateLesson();
        }
    }
    
    public void uploadLesson(String title){
        this.title = title;
        notifyStudent();
    }
}
