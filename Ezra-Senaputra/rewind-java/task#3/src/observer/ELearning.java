package observer;

public class ELearning {
    public static void main(String[] args) {
        Physics physics = new Physics();

        Student student1  = new Student("Raymon");
        Student student2  = new Student("Christina");
        Student student3  = new Student("Ygreet");
        Student student4  = new Student("Ygdrassil");
        Student student5  = new Student("Evan");

        physics.addStudent(student1);
        physics.addStudent(student2);
        physics.addStudent(student3);
        physics.addStudent(student4);
        physics.addStudent(student5);

        student1.getLesson(physics);
        student2.getLesson(physics);
        student3.getLesson(physics);
        student4.getLesson(physics);
        student5.getLesson(physics);

        physics.uploadLesson("Make a summary from Elecromagnetic Force Min 120 Words");

    }
}
