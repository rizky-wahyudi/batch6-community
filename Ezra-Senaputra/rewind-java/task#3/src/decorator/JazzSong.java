package decorator;

public class JazzSong extends SongDecorator {
    public JazzSong(Song s){
        super(s);
    }

    public String addJazzy(){
        return "Saxophone";
    }

    @Override
    public String notes() {
        return super.notes() + " " + addJazzy();
    }
}
