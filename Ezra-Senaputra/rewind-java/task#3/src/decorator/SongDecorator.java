package decorator;

public class SongDecorator implements Song{
    private Song song;

    public SongDecorator(Song s){
        this.song = s;
    }

    @Override
    public String notes() {
        return song.notes();
    }
}
