package decorator;

public class RockSong extends SongDecorator {
    public RockSong(Song s){
        super(s);
    }

    public String addScream(){
        return "Scream";
    }

    @Override
    public String notes() {
        return super.notes() + " " + addScream();
    }
}
