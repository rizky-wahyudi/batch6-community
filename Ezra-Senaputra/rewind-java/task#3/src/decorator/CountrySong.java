package decorator;

public class CountrySong extends SongDecorator {
    public CountrySong(Song s){
        super(s);
    }

    public String addGuitar(){
        return "Guitaring";
    }

    @Override
    public String notes() {
        return super.notes() + " " + addGuitar();
    }
}
