package decorator;

public class BasicSong implements Song{
    @Override
    public String notes() {
        return "Structuring the notes";
    }
}
