package decorator;

public class Main {
    public static void main(String[] args) {
        Song songFusion = new RockSong(new JazzSong(new BasicSong()));
        System.out.println(songFusion.notes());
        Song songFusion2 = new JazzSong(new RockSong(new BasicSong()));
        System.out.println(songFusion2.notes());
    }
}
