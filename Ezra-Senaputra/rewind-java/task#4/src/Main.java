import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class Main {
    public static void main(String[] args)
    {
        for (int i = 1; i < 6; i++) {
            String output  = getUrlContents("https://jsonplaceholder.typicode.com/todos/" + i);
            System.out.println(output);
            generateFile(i, output);
        }

    }

    private static String getUrlContents(String theUrl)
    {
        StringBuilder content = new StringBuilder();

        try
        {
            URL url = new URL(theUrl); // Membuat Object URL
            URLConnection urlConnection = url.openConnection(); // Membuat Koneksi

            // Membungkus koneksi dengan BufferedRead
            BufferedReader bufferedReader = new BufferedReader
                    (new InputStreamReader(urlConnection.getInputStream()));
            String line;
            // Ketika terkoneksi BufferedReader akan membaca isi URL
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }

    public static void generateFile(int id, String input){
        try {

            // Membuat Files
            File myObj = new File("C:\\Users\\ezras\\Documents\\Workspace\\Bootcamp nexSOFT\\" +
                    "rewind-java\\task#4\\file-" + id + ".json");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
            // Input Ke Filenya
            FileWriter myWriter = new FileWriter("C:\\Users\\ezras\\Documents\\Workspace\\" +
                    "Bootcamp nexSOFT\\" +
                    "rewind-java\\task#4\\file-" + id + ".json");
            myWriter.write(input);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
