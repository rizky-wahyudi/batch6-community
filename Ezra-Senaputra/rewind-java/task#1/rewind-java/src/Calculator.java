public class Calculator {

    public static int add(int numOne, int numTwo) {
        int result = numOne + numTwo;
        return result;
    }

    public static int subtract(int numOne, int numTwo) {
        int result = numOne - numTwo;
        return result;


    }

    public static int multiply(int numOne, int numTwo) {
        int result = numOne * numTwo;
        return result;
    }

    public static int divide(int numOne, int numTwo) {
        try {
            int result = numOne / numTwo;
            System.out.println(numOne + " / " + numTwo + " = " + result);
            return result;
        } catch(ArithmeticException e){
            System.out.println("Mohon untuk tidak dibagi dengan 0");
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public static void main(String[] args) {
      try{
          int numOne = Integer.parseInt(args[0]);
          int numTwo = Integer.parseInt(args[1]);
          System.out.println(add(numOne, numTwo));
          System.out.println(subtract(numOne, numTwo));
          System.out.println(multiply(numOne, numTwo));
          System.out.println(divide(numOne, numTwo));

      } catch (ArrayIndexOutOfBoundsException e) {
          System.out.println("Argument Kosong");
          System.out.println(e.getMessage());
      } catch (NumberFormatException e){
          System.out.println("Hanya boleh input numeric");
          System.out.println(e.getMessage());
      }

    }
}

