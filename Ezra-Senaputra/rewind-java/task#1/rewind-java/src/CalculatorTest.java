import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @org.junit.jupiter.api.Test
    void add() {
        int numOne = 2;
        int numTwo = 3;
        int result = numOne + numTwo;
        //Positive
        assertTrue(result == 5);
        assertEquals(5, result);

        // Negate
        assertFalse(result == 8);
        assertFalse(result == 10);
        assertThrows(NumberFormatException.class, () -> {
            String[] args = {"1", "B"};
            int numOneArray = Integer.parseInt(args[0]);
            int numTwoArray = Integer.parseInt(args[1]);
            int resultArr = numOneArray + numTwoArray;
        });

    }

    @org.junit.jupiter.api.Test
    void subtract() {
        int numOne = 8;
        int numTwo = 3;
        int result = numOne - numTwo;
        //Positive
        assertTrue(result == 5);
        assertEquals(5, result);

        // Negate
        assertFalse(result == 8);
        assertFalse(result == 10);
        assertThrows(NumberFormatException.class, () -> {
            String[] args = {"1", "B"};
            int numOneArray = Integer.parseInt(args[0]);
            int numTwoArray = Integer.parseInt(args[1]);
            int resultArr = numOneArray - numTwoArray;
        });
    }

    @org.junit.jupiter.api.Test
    void multiply() {
        int numOne = 1;
        int numTwo = 5;
        int result = numOne * numTwo;
        //Positive
        assertTrue(result == 5);
        assertEquals(5, result);

        // Negate
        assertFalse(result == 8);
        assertFalse(result == 10);
        assertThrows(NumberFormatException.class, () -> {
            String[] args = {"1", "B"};
            int numOneArray = Integer.parseInt(args[0]);
            int numTwoArray = Integer.parseInt(args[1]);
            int resultArr = numOneArray * numTwoArray;
        });
    }

    @org.junit.jupiter.api.Test
    void divide() {
        int numOne = 5;
        int numTwo = 1;
        int result = numOne * numTwo;
        //Positive
        assertTrue(result == 5);
        assertEquals(5, result);

        // Negate
        assertFalse(result == 8);
        assertFalse(result == 10);
        assertThrows(ArithmeticException.class, () -> {
            int resultNull = 3/0;
        });
    }

    @org.junit.jupiter.api.Test
    void mainSuccess() {
        String[] args = {"2", "3"};
        int numOne = Integer.parseInt(args[0]);
        int numTwo = Integer.parseInt(args[1]);
        assertTrue(numOne == 2);
        assertEquals(2, numOne);
        assertTrue(numTwo == 3);
        assertEquals(3, numTwo);

        assertFalse(numOne == 10);
        assertFalse(numTwo == 123);

    }

    @org.junit.jupiter.api.Test
    void mainNoArray() {
      assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
        String[] argsAr = {};
        int numOneArray = Integer.parseInt(argsAr[0]);
        int numTwoArray = Integer.parseInt(argsAr[1]);
        System.out.println(numOneArray);
        System.out.println(numTwoArray);
        });
    }

    @org.junit.jupiter.api.Test
    void mainArrayString() {
        assertThrows(NumberFormatException.class, () -> {
            String[] argsAr = {"B","C"};
            int numOneArray = Integer.parseInt(argsAr[0]);
            int numTwoArray = Integer.parseInt(argsAr[1]);
            System.out.println(numOneArray);
            System.out.println(numTwoArray);
        });
    }


}