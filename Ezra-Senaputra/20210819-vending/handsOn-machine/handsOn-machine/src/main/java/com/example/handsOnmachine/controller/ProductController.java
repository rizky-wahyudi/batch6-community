package com.example.handsOnmachine.controller;

import com.example.handsOnmachine.entity.Product;
import com.example.handsOnmachine.entity.UserOrder;
import com.example.handsOnmachine.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/")
    public String welcome(){
        return  "<html><body>"
                + "<h1>Please Buy at your Desire</h1>"
                + "</body></html>";
    }

    @GetMapping("/products")
    public List<Product> getAllProduct(){
        return productRepository.findAll();
    }

    @GetMapping("/products/{id}")
    public Product getProduct(@PathVariable(value = "id") int id){
        return productRepository.findById(id);
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Product addProduct(@RequestBody Product product){
        return productRepository.save(product);
    }

    @PostMapping("/products")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Product> addProduct(@RequestBody List<Product> products){
        return (List<Product>) productRepository.saveAll(products);
    }

    @PutMapping("/updateAmount")
    public void updateAmount(UserOrder userOrder, Product product){
        productRepository.setAmount(userOrder.getProductId(), (product.getAmount() - userOrder.getAmountProduct() ));
    }

}
