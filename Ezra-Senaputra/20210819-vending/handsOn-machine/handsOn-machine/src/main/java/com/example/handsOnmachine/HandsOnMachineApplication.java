package com.example.handsOnmachine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandsOnMachineApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandsOnMachineApplication.class, args);
	}

}
