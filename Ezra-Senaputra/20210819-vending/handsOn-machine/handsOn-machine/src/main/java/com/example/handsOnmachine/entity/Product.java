package com.example.handsOnmachine.entity;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private int price;
    private String productColumn;
    private String productRow;
    private long amount;


    public Product() {
    }

    public Product(String name, int price, String productColumn, String productRow, long amount) {
        this.name = name;
        this.price = price;
        this.productColumn = productColumn;
        this.productRow = productRow;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProductColumn() {
        return productColumn;
    }

    public void setProductColumn(String productColumn) {
        this.productColumn = productColumn;
    }

    public String getProductRow() {
        return productRow;
    }

    public void setProductRow(String productRow) {
        this.productRow = productRow;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }


}
