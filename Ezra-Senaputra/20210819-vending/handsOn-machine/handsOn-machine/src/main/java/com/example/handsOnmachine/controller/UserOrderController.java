package com.example.handsOnmachine.controller;

import com.example.handsOnmachine.entity.UserOrder;
import com.example.handsOnmachine.repository.UserOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
public class UserOrderController {

    @Autowired
    private ProductController productController;

    @Autowired
    private UserOrderRepository userOrderRepository;

    @GetMapping("/userOrders")
    public List<UserOrder> getAllUserOrder(){
        return userOrderRepository.findAll();
    }

    @GetMapping("/userOrders/{id}")
    public UserOrder getUserOrder(@PathVariable(value = "id") int id){
        return userOrderRepository.findById(id);
    }

    @PostMapping("/userOrder")
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public UserOrder addUserOrder(@RequestBody UserOrder userOrder){
        productController.updateAmount(userOrder, productController.getProduct(userOrder.getProductId()));
        return userOrderRepository.save(userOrder);

    }

    @PostMapping("/userOrders")
    @ResponseStatus(HttpStatus.CREATED)
    public List<UserOrder> addUserOrder(@RequestBody List<UserOrder> userOrders){
        return (List<UserOrder>) userOrderRepository.saveAll(userOrders);

    }
}
