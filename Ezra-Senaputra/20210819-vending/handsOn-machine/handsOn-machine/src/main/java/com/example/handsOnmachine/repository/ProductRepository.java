package com.example.handsOnmachine.repository;

import com.example.handsOnmachine.entity.Product;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    List<Product> findAll();
    Product findById(int id);
    @Modifying
    @Query(value = "update Product p set p.amount = :amount where  p.id= :productId")
    void setAmount (@Param("productId") int productId, @Param("amount") long amount);

}
