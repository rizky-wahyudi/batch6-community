package com.example.handsOnmachine.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String userVending;
    private long amountProduct;
    private int userMoney;
    private int productId;

    public UserOrder() {
    }

    public UserOrder(String userVending, long amountProduct, int userMoney, int productId) {
        this.userVending = userVending;
        this.amountProduct = amountProduct;
        this.userMoney = userMoney;
        this.productId = productId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserVending() {
        return userVending;
    }

    public void setUserVending(String userVending) {
        this.userVending = userVending;
    }

    public long getAmountProduct() {
        return amountProduct;
    }

    public void setAmountProduct(int amountProduct) {
        this.amountProduct = amountProduct;
    }

    public int getUserMoney() {
        return userMoney;
    }

    public void setUserMoney(int userMoney) {
        this.userMoney = userMoney;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
