package com.example.handsOnmachine.repository;

import com.example.handsOnmachine.entity.UserOrder;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserOrderRepository extends CrudRepository<UserOrder, Integer> {
    List<UserOrder> findAll();
    UserOrder findById(int id);
}
