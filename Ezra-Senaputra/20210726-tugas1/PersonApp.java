import java.util.Scanner;

public class PersonApp{
    public static void main(String[] args){

        // Membuat scanner object untuk input
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan nilai A : ");
        int firstNumber = input.nextInt();
        System.out.print("Masukkan nilai B : ");
        int secondNumber = input.nextInt();
        System.out.print("Masukkan nilai C : ");
        int thirdNumber = input.nextInt();

        // Inisiasi object
        Person ezra = new Person(firstNumber,secondNumber,thirdNumber);
        ezra.numberCondition();
    }
}