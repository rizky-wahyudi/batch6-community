
public class Liburan {
    private int jumlahOrang;
    private int jumlahHari;
    private int hargaTicket;
    private int hargaHotel;
    private int biayaTransport;
    
    public void setJumlahOrang(int jumlah){
        this.jumlahOrang = jumlah;
    }
    public int getJumlahOrang(){
       return this.jumlahOrang;
    }

    public void setJumlahHari(int hari){
        this.jumlahHari = hari;
    }
    public int getJumlahHari(){
       return this.jumlahHari;
    }

    public void setHargaTicket(int harga){
        this.hargaTicket = harga;
    }

    public int getHargaTicket(){
        return this.hargaTicket;
    }

    public void setHargaHotel(int hargaHotelLiburan){
        this.hargaHotel = hargaHotelLiburan;
    }

    public int getHargaHotel(){
        return this.hargaHotel;
    }

    public void setBiayaTransport(int biayaTransportLiburan){
        this.biayaTransport = biayaTransportLiburan;
    }

    public int getBiayaTransport(){
        return this.biayaTransport;
    }
}