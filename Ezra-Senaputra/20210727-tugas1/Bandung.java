
public class Bandung extends Liburan {
    static int TangkubanPerahu = 100;
    static int JalanBraga = 120;
    static int GedungSate = 150;

    
    public static void getTangkubanPerahu(){
        System.out.println("Tangkuban Perahu $" + Bandung.TangkubanPerahu);
    }    

    public static void getJalanBraga(){
        System.out.println("Jalan Braga $" + Bandung.JalanBraga);
    }

    public static void getGedungSate(){
        System.out.println("Gedung Sate $" + Bandung.GedungSate);
    }
}