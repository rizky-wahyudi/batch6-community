import java.util.Scanner; 

public class LiburanApp {
    public static void main(String args[]){

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Destinasi : ");
        String destinasi = input.nextLine();
        
        Liburan destination  = LiburanFactory.newInstance(destinasi);
        
        System.out.print("Masukkan Jumlah Orang : ");
        int jumlahOrang = input.nextInt();
      
        System.out.print("Masukkan Jumlah Hari : ");
        int jumlahHari = input.nextInt();
      
        
        if(destination instanceof Tokyo) {

            Tokyo tokyo = new Tokyo();
            tokyo.setJumlahOrang(jumlahOrang);
            tokyo.setJumlahHari(jumlahHari);
            tokyo.setHargaTicket(1000);
            tokyo.setHargaHotel(50);
            tokyo.setBiayaTransport(10);

            int totalTicketPrice = tokyo.getHargaTicket() * tokyo.getJumlahOrang();
            int totalHargaHotel = tokyo.getHargaHotel() * tokyo.getJumlahHari();
            int totalBiayaTransport = tokyo.getBiayaTransport() * tokyo.getJumlahHari();
            int totalLiburan = totalTicketPrice + totalHargaHotel + totalBiayaTransport;

            System.out.println("Rincian : ");
            System.out.println("=========== TOKYO ===========");
            System.out.println("Jumlah Orang            : " + tokyo.getJumlahOrang());
            System.out.println("Jumah Hari              : " + tokyo.getJumlahHari());
            System.out.println("Total Harga Tiket       : $" + totalTicketPrice);
            System.out.println("Total Harga Hotel       : $" + totalHargaHotel);
            System.out.println("Total Harga Transport   : $" + totalBiayaTransport);
            System.out.println("TOTAL BIAYA LIBURAN     : $" + totalLiburan);

            System.out.println("======= Destinasi Wisata ======");
            Tokyo.getTokyoSkyTree(); 
            Tokyo.getMeijiJingu();
            Tokyo.getNationalGarden();

        } else if(destination instanceof NewYork) {

            NewYork newyork = new NewYork();
            newyork.setJumlahOrang(jumlahOrang);
            newyork.setJumlahHari(jumlahHari);
            newyork.setHargaTicket(2000);
            newyork.setHargaHotel(100);
            newyork.setBiayaTransport(20);

            int totalTicketPrice = newyork.getHargaTicket() * newyork.getJumlahOrang();
            int totalHargaHotel = newyork.getHargaHotel() * newyork.getJumlahHari();
            int totalBiayaTransport = newyork.getBiayaTransport() * newyork.getJumlahHari();
            int totalLiburan = totalTicketPrice + totalHargaHotel + totalBiayaTransport;

            System.out.println("Rincian : ");
            System.out.println("=========== New York ===========");
            System.out.println("Jumlah Orang            : " + newyork.getJumlahOrang());
            System.out.println("Jumah Hari              : " + newyork.getJumlahHari());
            System.out.println("Total Harga Tiket       : $" + totalTicketPrice);
            System.out.println("Total Harga Hotel       : $" + totalHargaHotel);
            System.out.println("Total Harga Transport   : $" + totalBiayaTransport);
            System.out.println("TOTAL BIAYA LIBURAN     : $" + totalLiburan);

            System.out.println("======= Destinasi Wisata ======");
            NewYork.getCentralPark();
            NewYork.getTheMetroPolitan();
            NewYork.getEmpireState();
        } else if(destination instanceof Bandung) {

            Bandung bandung = new Bandung();
            bandung.setJumlahOrang(jumlahOrang);
            bandung.setJumlahHari(jumlahHari);
            bandung.setHargaTicket(100);
            bandung.setHargaHotel(20);
            bandung.setBiayaTransport(10);

            int totalTicketPrice = bandung.getHargaTicket() * bandung.getJumlahOrang();
            int totalHargaHotel = bandung.getHargaHotel() * bandung.getJumlahHari();
            int totalBiayaTransport = bandung.getBiayaTransport() * bandung.getJumlahHari();
            int totalLiburan = totalTicketPrice + totalHargaHotel + totalBiayaTransport;

            System.out.println("Rincian : ");
            System.out.println("=========== Bandung ===========");
            System.out.println("Jumlah Orang            : " + bandung.getJumlahOrang());
            System.out.println("Jumah Hari              : " + bandung.getJumlahHari());
            System.out.println("Total Harga Tiket       : $" + totalTicketPrice);
            System.out.println("Total Harga Hotel       : $" + totalHargaHotel);
            System.out.println("Total Harga Transport   : $" + totalBiayaTransport);
            System.out.println("TOTAL BIAYA LIBURAN     : $" + totalLiburan);

            System.out.println("======= Destinasi Wisata ======");
            Bandung.getTangkubanPerahu();
            Bandung.getJalanBraga();
            Bandung.getGedungSate();

        } else {
            System.out.println("Destinasi Tidak Sesuai");
            System.out.println("Harap Pilih ");
            System.out.println("Bandung, Tokyo, NewYork");
        }


        
    }

    
}