
public class Tokyo extends Liburan{
    static int TokyoSkyTree = 100;
    static int MeijiJingu = 120;
    static int SGNationalGarden = 150;

    
    public static void getTokyoSkyTree(){
        System.out.println("Tokyo Sky Tree $" + Tokyo.TokyoSkyTree);
    }    

    public static void getMeijiJingu(){
        System.out.println("Meiji Jingu $" + Tokyo.MeijiJingu);
    }

    public static void getNationalGarden(){
        System.out.println("National Garden $" + Tokyo.SGNationalGarden);
    }
}