public class LiburanFactory{
    public static Liburan newInstance(String type){
        if(type.equals("Tokyo")){
            return new Tokyo();
        } else if(type.equals("NewYork")){
            return new NewYork();
        } else if(type.equals("Bandung")){
            return new Bandung();
        }
        return null;
    }
}