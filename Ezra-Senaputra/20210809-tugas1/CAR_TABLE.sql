CREATE TABLE car(
	id INT NOT NULL auto_increment,
    images_id INT,
    brand_id INT,
    category_id INT,
    engineSize_id INT,
    fuelType_id INT,
    currentLocation_id INT,
    gearType_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE images(
	id INT NOT NULL auto_increment,
	imageUrl varchar(2038),
    status_id char(1),
    car_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE brand (
	id INT NOT NULL auto_increment,
	name varchar(50),
    logoUrl varchar(2038),
    PRIMARY KEY (id)
);

CREATE TABLE category (
	id INT NOT NULL auto_increment,
    type VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE engineSize (
	id INT NOT NULL auto_increment,
	size varchar(10),
    PRIMARY KEY (id)
);

CREATE TABLE fuelType(
	id INT NOT NULL auto_increment,
    type varchar(20),
    PRIMARY KEY (id)
);

CREATE TABLE currentLocation(
	id INT NOT NULL auto_increment,
    street VARCHAR(200),
    district VARCHAR(30),
    city VARCHAR(30),
    province VARCHAR(30),
    country VARCHAR(50),
    longitude DECIMAL(11,8),
    latitude DECIMAL(10,8),
    PRIMARY KEY (id)
);

CREATE TABLE gearType(
	id INT NOT NULL auto_increment,
    type VARCHAR(20),
    PRIMARY KEY (id)
);