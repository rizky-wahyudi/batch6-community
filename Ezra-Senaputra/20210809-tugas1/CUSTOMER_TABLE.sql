CREATE TABLE customer(
	id INT NOT NULL auto_increment,
    firstName VARCHAR(100),
    lastName VARCHAR(100),
    phone VARCHAR(20),
    email VARCHAR(320),
    address_id INT,
    id_number VARCHAR(16),
    id_image VARCHAR(2038),
    PRIMARY KEY (id)
);

CREATE TABLE address(
	id INT NOT NULL auto_increment,
    street VARCHAR(50),
    district VARCHAR(50),
    city VARCHAR(30),
    province VARCHAR(30),
    country VARCHAR(50),
    PRIMARY KEY (id)
);