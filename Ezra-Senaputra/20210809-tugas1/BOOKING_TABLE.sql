CREATE TABLE booking(
	id INT NOT NULL auto_increment,
    customer_id INT,
    car_id INT,
    service_id INT,
    pickUpLocation_id INT, 
    dropOffLocation_id INT,
    pickUpDAte DATE,
    dropOffDate DATE,
    totalPrice BIGINT,
    PRIMARY KEY (id)
);