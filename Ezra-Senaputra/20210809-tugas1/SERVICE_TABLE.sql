CREATE TABLE service(
	id INT NOT NULL auto_increment,
    insurance CHAR(1),
    weekdayPrice_id INT,
    weekendPrice_id INT,
    holidayPrice_id INT,
    carCategory_id INT,
    carGearType_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE weekDayPrice(
	id INT NOT NULL auto_increment,
    amount BIGINT,
    PRIMARY KEY (id)
);

CREATE TABLE weekendPrice(
	id INT NOT NULL auto_increment,
    amount BIGINT,
    PRIMARY KEY (id)
);

CREATE TABLE holidayPrice(
	id INT NOT NULL auto_increment,
    amount BIGINT,
    PRIMARY KEY (id)
);