INSERT INTO address (street, longitude, latitude, city, district, province, country)
	VALUES ('305 - 14th Ave. S. Suite 3B', '-107.10923', '2.20321', 'ZURICH', 'A8', 'DANKE', 'SWITZERLAND'),
		('Jl. Yos Sudarso', '106.871483', '-6.132055', 'JAKARTA UTARA', 'B0', 'DKI JAKARTA', 'INDONESIA'),
		('','-18.7792678', '46.8344597', '', '', '', 'MADAGASKAR'); 

INSERT INTO distributor (name, address_id, addressType)
	VALUES ('SwitzCheese', 1, 'Regional'),
		('Yakults', 2, 'Regional'),
		('MadaHydroCoco', 3, 'Coordinates');

INSERT INTO productDistributor (distributor_id, buyPrice, stock)
	VALUES (1, 10000, 1000),
		(2, 5000, 2000),
		(3, 3500, 3000);

INSERT INTO productImages (url, description)
	VALUES ('https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.thejakartapost.com%2Flife%2F2020%2F03%2F02%2Fwhy-does-swiss-cheese-have-holes.html&psig=AOvVaw0yqsJhZxHNGG55cu80P1OX&ust=1628471361466000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCPifkNueoPICFQAAAAAdAAAAABAD', 'ORIGINAL CHEESE FROM OUR SWISS PARADISE'),
	('https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.99.co%2Fblog%2Findonesia%2Fmanfaat-yakult-kesehatan%2F&psig=AOvVaw20Xi8quy3L8FHnMjJBaG8s&ust=1628471441279000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCKjB4YCfoPICFQAAAAAdAAAAABAG', 'Sayangi UsusMu Minum yakult tiap hari'),
	('https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.genpi.co%2Fgaya-hidup%2F70473%2Fmanfaat-minuman-hydro-coco-ternyata-sangat-mencengangkan&psig=AOvVaw3RwgMnxwTu5qQmJOusBsY3&ust=1628471477287000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCKj3mZKfoPICFQAAAAAdAAAAABAE', 'Madagaskar Most Pride Coconuts Drink');


INSERT INTO product (artNumber, productImages_id, sellPrice, expiredDate)
	VALUES ('SZ001', 1, 15000, '20210920'),
		('SZ002', 1, 17000, '20210918'),
		('SZ003', 1, 19000, '20210917'),
		('SZ004', 1, 20000, '20210915'),
		('YK001', 2, 8000, '20220201'),
		('YK002', 2, 10000, '20220202'),
		('YK003', 2, 12000, '20220203'),
		('MHC001', 3, 5000, '20211001'),
		('MHC002', 3, 7500, '20211002'),
		('MHC003', 3, 10000, '20211002');  


INSERT INTO productStock (product_id, stock, productDistributor_id, purchaseDate, deliveryDate)
	VALUES (1, 100, 1, '20210225', '20210302'),
		(2, 200, 1, '20210301', '20210308'),
		(3, 400, 1, '20210302', '20210310'),
		(4, 300, 1, '20210304', '20210314'),
		(5, 0, 2, '20210425', '20210430'),
		(6, 0, 2, '20210505', '20210512'),
		(7, 500, 2, '20210510', '20210520'),
		(8, 1000, 3, '20210601', '20210608'),
		(9, 0, 3, '20210602', '20210609'),
		(10, 1000, 3, '20210603', '20210610');

SELECT product.artNumber, productImages.description, product.expiredDate, productStock.stock,
	productStock.purchaseDate, productStock.deliveryDate, distributor.name 
FROM productStock
LEFT JOIN product ON productStock.product_id = product.id
LEFT JOIN productImages ON product.productImages_id = productImages.id
LEFT JOIN productDistributor ON productStock.productDistributor_id = productDistributor.id
LEFT JOIN distributor ON productDistributor.distributor_id = distributor.id
WHERE productStock.stock = 0;
