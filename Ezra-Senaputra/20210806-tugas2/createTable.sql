CREATE TABLE productImages (
	id INT NOT NULL AUTO_INCREMENT, 
	url VARCHAR(2083) NOT NULL,
	description VARCHAR(300),
	PRIMARY KEY (id)
);

CREATE TABLE product (
	id INT NOT NULL AUTO_INCREMENT,
	artNumber VARCHAR(10),
	productImages_id INT,
	sellPrice BIGINT,
	expiredDate DATE,
	PRIMARY KEY (id),
	FOREIGN KEY (productImages_id) REFERENCES productImages
	(id)
);

CREATE TABLE address (
	id INT NOT NULL AUTO_INCREMENT,
	street VARCHAR(150),
	longitude DECIMAL(11,8),
	latitude DECIMAL(10,8),
	city VARCHAR(50),
	district VARCHAR(50),
	province VARCHAR(50),
	country VARCHAR(50),
	PRIMARY KEY (id)
);

CREATE TABLE distributor (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50),
	address_id INT,
	addressType VARCHAR(20),
	PRIMARY KEY(id),
	FOREIGN KEY (address_id) REFERENCES address 
	(id)
);

CREATE TABLE productDistributor (
	id INT NOT NULL AUTO_INCREMENT,
	distributor_id INT,
	buyPrice BIGINT,
	stock INT,
	PRIMARY KEY (id),
	FOREIGN KEY (distributor_id) REFERENCES distributor
	(id)
);

CREATE TABLE productStock (
	id INT NOT NULL AUTO_INCREMENT,
	product_id INT,
	stock INT,
	productDistributor_id INT,
	purchaseDate DATE,
	deliveryDate DATE,
	PRIMARY KEY (id),
	FOREIGN KEY (product_id) REFERENCES product (id),
	FOREIGN KEY (productDistributor_id) REFERENCES productDistributor 
	(id)
);