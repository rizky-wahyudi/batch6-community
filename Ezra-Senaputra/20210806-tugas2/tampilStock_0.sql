SELECT product.artNumber, productImages.description, product.expiredDate, productStock.stock,
	productStock.purchaseDate, productStock.deliveryDate, distributor.name 
FROM productStock
LEFT JOIN product ON productStock.product_id = product.id
LEFT JOIN productImages ON product.productImages_id = productImages.id
LEFT JOIN productDistributor ON productStock.productDistributor_id = productDistributor.id
LEFT JOIN distributor ON productDistributor.distributor_id = distributor.id
WHERE productStock.stock = 0;