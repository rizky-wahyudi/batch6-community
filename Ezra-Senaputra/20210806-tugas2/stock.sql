-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: handson_stock_management
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `street` varchar(150) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'305 - 14th Ave. S. Suite 3B',-107.10923000,2.20321000,'ZURICH','A8','DANKE','SWITZERLAND'),(2,'Jl. Yos Sudarso',106.87148300,-6.13205500,'JAKARTA UTARA','B0','DKI JAKARTA','INDONESIA'),(3,'',-18.77926780,46.83445970,'','','','MADAGASKAR');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distributor`
--

DROP TABLE IF EXISTS `distributor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distributor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `address_id` int DEFAULT NULL,
  `addressType` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  CONSTRAINT `distributor_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distributor`
--

LOCK TABLES `distributor` WRITE;
/*!40000 ALTER TABLE `distributor` DISABLE KEYS */;
INSERT INTO `distributor` VALUES (1,'SwitzCheese',1,'Regional'),(2,'Yakults',2,'Regional'),(3,'MadaHydroCoco',3,'Coordinates');
/*!40000 ALTER TABLE `distributor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `artNumber` varchar(10) DEFAULT NULL,
  `productImages_id` int DEFAULT NULL,
  `sellPrice` bigint DEFAULT NULL,
  `expiredDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productImages_id` (`productImages_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`productImages_id`) REFERENCES `productimages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'SZ001',1,15000,'2021-09-20'),(2,'SZ002',1,17000,'2021-09-18'),(3,'SZ003',1,19000,'2021-09-17'),(4,'SZ004',1,20000,'2021-09-15'),(5,'YK001',2,8000,'2022-02-01'),(6,'YK002',2,10000,'2022-02-02'),(7,'YK003',2,12000,'2022-02-03'),(8,'MHC001',3,5000,'2021-10-01'),(9,'MHC002',3,7500,'2021-10-02'),(10,'MHC003',3,10000,'2021-10-02');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productdistributor`
--

DROP TABLE IF EXISTS `productdistributor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productdistributor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `distributor_id` int DEFAULT NULL,
  `buyPrice` bigint DEFAULT NULL,
  `stock` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `distributor_id` (`distributor_id`),
  CONSTRAINT `productdistributor_ibfk_1` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productdistributor`
--

LOCK TABLES `productdistributor` WRITE;
/*!40000 ALTER TABLE `productdistributor` DISABLE KEYS */;
INSERT INTO `productdistributor` VALUES (1,1,10000,1000),(2,2,5000,2000),(3,3,3500,3000);
/*!40000 ALTER TABLE `productdistributor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productimages`
--

DROP TABLE IF EXISTS `productimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productimages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(2083) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productimages`
--

LOCK TABLES `productimages` WRITE;
/*!40000 ALTER TABLE `productimages` DISABLE KEYS */;
INSERT INTO `productimages` VALUES (1,'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.thejakartapost.com%2Flife%2F2020%2F03%2F02%2Fwhy-does-swiss-cheese-have-holes.html&psig=AOvVaw0yqsJhZxHNGG55cu80P1OX&ust=1628471361466000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCPifkNueoPICFQAAAAAdAAAAABAD','ORIGINAL CHEESE FROM OUR SWISS PARADISE'),(2,'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.99.co%2Fblog%2Findonesia%2Fmanfaat-yakult-kesehatan%2F&psig=AOvVaw20Xi8quy3L8FHnMjJBaG8s&ust=1628471441279000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCKjB4YCfoPICFQAAAAAdAAAAABAG','Sayangi UsusMu Minum yakult tiap hari'),(3,'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.genpi.co%2Fgaya-hidup%2F70473%2Fmanfaat-minuman-hydro-coco-ternyata-sangat-mencengangkan&psig=AOvVaw3RwgMnxwTu5qQmJOusBsY3&ust=1628471477287000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCKj3mZKfoPICFQAAAAAdAAAAABAE','Madagaskar Most Pride Coconuts Drink');
/*!40000 ALTER TABLE `productimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productstock`
--

DROP TABLE IF EXISTS `productstock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productstock` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `stock` int DEFAULT NULL,
  `productDistributor_id` int DEFAULT NULL,
  `purchaseDate` date DEFAULT NULL,
  `deliveryDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `productDistributor_id` (`productDistributor_id`),
  CONSTRAINT `productstock_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `productstock_ibfk_2` FOREIGN KEY (`productDistributor_id`) REFERENCES `productdistributor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productstock`
--

LOCK TABLES `productstock` WRITE;
/*!40000 ALTER TABLE `productstock` DISABLE KEYS */;
INSERT INTO `productstock` VALUES (1,1,100,1,'2021-02-25','2021-03-02'),(2,2,200,1,'2021-03-01','2021-03-08'),(3,3,400,1,'2021-03-02','2021-03-10'),(4,4,300,1,'2021-03-04','2021-03-14'),(5,5,0,2,'2021-04-25','2021-04-30'),(6,6,0,2,'2021-05-05','2021-05-12'),(7,7,500,2,'2021-05-10','2021-05-20'),(8,8,1000,3,'2021-06-01','2021-06-08'),(9,9,0,3,'2021-06-02','2021-06-09'),(10,10,1000,3,'2021-06-03','2021-06-10');
/*!40000 ALTER TABLE `productstock` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-08  8:38:57
