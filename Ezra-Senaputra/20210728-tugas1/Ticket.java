public interface Ticket {
    final String ticketCodeFirst = "TI";
    final String ticketCodeLast = "CKET";
    String getTicket();
}