public interface Passport {
    final String passportCode = "ID";
    String getPassport();
}