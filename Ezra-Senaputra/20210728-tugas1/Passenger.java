public class Passenger implements Ticket, Passport {
    private String name;
    private String ticket;
    private String passport;
    private String laguage;
    private int laguageWeight;
    private String handBag;

    Passenger(String name, String ticket, String passport, String laguage, int laguageWeight, String handBag){
        this.name = name;
        this.ticket = ticket;
        this.passport = passport;
        this.laguage = laguage;
        this.laguageWeight = laguageWeight;
        this.handBag = handBag;
    }
    
    public String getName(){
        return this.name;
    }

    public String getTicket(){
        return Ticket.ticketCodeFirst + this.ticket + Ticket.ticketCodeLast;
    }
    public String getPassport(){
        return Passport.passportCode + this.passport;
    }
    public String getLaguage(){
        return this.laguage;
    }
    public int getLaguageWeight(){
        return this.laguageWeight;
    }
    public String getHandBag(){
        return this.handBag;
    }



}