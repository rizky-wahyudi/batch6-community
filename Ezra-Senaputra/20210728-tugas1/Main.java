import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        System.out.println("=== AIRPLANE ===");
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan nama anda : ");
        String name = input.nextLine();
        
        System.out.print("Masukkan No Ticket anda (4 Char) : ");
        String ticket = input.nextLine();
        
        System.out.print("Masukkan No Passport anda (4 Char) : ");
        String passport = input.nextLine();
    
        System.out.println("Masukkan Barang Bawaan Anda");
        System.out.print("Laguage  : ");
        String laguage = input.nextLine();
        System.out.print("Berat    : ");
        int laguageWeight = input.nextInt();
        System.out.print("Hand Bag : ");
        
        Scanner in = new Scanner(System.in); 
        String handBag = in.nextLine();


        Passenger passengger = new Passenger(name, ticket, passport, laguage, laguageWeight, handBag);

        Security security = new Security();
        System.out.println("=== SECURITY CHECK ===");
        security.checkName(passengger.getName(), passengger.getTicket());
        
        if(security.checkLaguage(passengger.getLaguage())){
            System.out.println("SECURITY GUARD!");
            System.out.println("YOU ARE BRINGING " + passengger.getLaguage());
            System.exit(0);
        } else {
            System.out.println("X RAY OK!");
        }

        CounterAirlane counterAirlane = new CounterAirlane();
        System.out.println("=== COUNTER AIRLANE CHECK ===");
        counterAirlane.checkTicket(passengger.getTicket());
        counterAirlane.checkPassport(passengger.getPassport());
        counterAirlane.checkWeightLaguage(passengger.getLaguageWeight());

        Imigration imigration = new Imigration();
        System.out.println("=== IMIGRATION CHECK ===");
        imigration.checkTicket(passengger.getTicket());
        imigration.checkPassport(passengger.getPassport());
        imigration.checkLaguage(passengger.getLaguage());
        if(imigration.checkLaguage(passengger.getLaguage())){
            System.out.println("SECURITY GUARD!");
            System.exit(0);
        } else {
            System.out.println("X RAY OK!");
        }

        WaitingRoom waitingRoom = new WaitingRoom();
        System.out.println("=== PREAPRE TO ON-BOARD ===");
        waitingRoom.checkTicket(passengger.getTicket());
        waitingRoom.checkPassport(passengger.getPassport());
        if(waitingRoom.checkHandBag(passengger.getHandBag())) {
            System.out.println("SECURITY GUARD!");
            System.exit(0);
        } else {
            System.out.println("HAND BAG OK!");
        }

        System.out.println("");
        System.out.println("");
        System.out.println("========================");
        System.out.println("All Check");
        System.out.println("Enjoy Your Flight");
    }
}