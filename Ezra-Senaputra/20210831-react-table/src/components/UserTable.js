import './style.css'

function UserTable(props){

    return (
        <table className="randomUser" style={{width: "80%" , margin: "auto", textAlign: "center"}}>
            <thead>
                <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
                <th>Email</th>
                </tr>
            </thead>
            <tbody>
                {
                    props.data.map((item, index)=>{
                    return(                    
                        <tr key={index}>
                            <td>{index+1}</td>
                        <td>{item.name}</td>
                        <td>{item.age}</td>
                        <td>{item.email}</td>
                        </tr>
                    )
                    })
                }
            </tbody>
            </table>
    )

}

export default UserTable;