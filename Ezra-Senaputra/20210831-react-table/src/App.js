import './App.css';
import UserTable from './components/UserTable'
import Header from './components/Header'
function App() {
      const dataTable = [{
        name : "Wasif",
        age : 21,
        email : "wasif@gmail.com"
    },{
        name : "Morgan",
        age : 21,
        email : "morgan@gmail.com"
    },{
        name : "Edrick",
        age : 21,
        email : "edrick@gmail.com"
    }, {
        name : "Deanu",
        age : 21,
        email : "deanu@gmail.com"
    }]

  return (
    <div className="App">
        <Header />
        <UserTable data={dataTable} />
    </div>
  );
}

export default App;
