package com.example.handsonstudent.controller;

import com.example.handsonstudent.entity.Student;
import com.example.handsonstudent.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;


    @GetMapping("/")
    public String welcome(){
        return "<html><body>"
                + "<h1>Welcome New Student</h1>"
                + "</body></html>";
    }

    @GetMapping("/student")
    public List<Student> getAllStudent(){
        return studentRepository.findAll();
    }

    @GetMapping("/student/{id}")
    public Student getStudentById(@PathVariable(value = "id") int id){
        return studentRepository.findById(id);
    }

    @PostMapping("/student")
    @ResponseStatus(HttpStatus.CREATED)
    public Student addStudnet(@RequestBody Student student){
        return studentRepository.save(student);
    }

    @DeleteMapping("/deleteStudent/{id}")
    public String deleteStudent(@PathVariable(value = "id") int id){
        studentRepository.deleteById(id);
        return "Student Remove";
    }

    @PutMapping("/updateStudent/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Student student, @PathVariable int id){
        Optional<Student> studentRepo = Optional.ofNullable(studentRepository.findById(id));

        if(!studentRepo.isPresent()){
            return ResponseEntity.notFound().build();
        }

        student.setId(id);
        studentRepository.save(student);
        return ResponseEntity.noContent().build();
    }


}
