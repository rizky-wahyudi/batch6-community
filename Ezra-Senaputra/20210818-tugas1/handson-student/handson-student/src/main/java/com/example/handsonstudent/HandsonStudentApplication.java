package com.example.handsonstudent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandsonStudentApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandsonStudentApplication.class, args);
	}

}
