package com.example.handsonstudent.repository;

import com.example.handsonstudent.entity.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student, Integer> {
    Student findById(int id);
    List<Student> findAll();
    void deleteById(int id);
}
