import React from 'react';
import { Zoom } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'


const ImageSlider = () => {
  const images = [
    'image/1.jpg',
    'image/2.jpg',
    'image/3.jpg'
  ];
  
  const zoomInProperties = {
    indicators: true,
    scale: 1.4
  }
  return (
    <div>
    <Zoom {...zoomInProperties}>
      {images.map((each, index) => (
        <div key={index} style={{width: "100%"}}>
          <img style={{ objectFit: "cover", width: "100%" }} src={each} />
        </div>
      ))}
    </Zoom>
  </div>
  )
}

export default ImageSlider;