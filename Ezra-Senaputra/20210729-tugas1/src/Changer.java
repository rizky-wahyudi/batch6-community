public class Changer {

    public float changeIDR(String convertTo, float getPrice, float value) {
        if(convertTo.equals("usd")) {
            return value / getPrice;
        } else if(convertTo.equals("eur")){
            return value / getPrice;
        } else {
            return 0;
        }
    }

    public float changeUSD(String convertTo, float getPrice, float value) {
        if(convertTo.equals("idr")) {
            return value * getPrice;
        } else if(convertTo.equals("eur")){
            return value * getPrice;
        } else {
            return 0;
        }
    }

    public float changeEUR(String convertTo, float getPrice, float value) {
        if(convertTo.equals("idr")) {
            return value * getPrice;
        } else if(convertTo.equals("usd")){
            return value * getPrice;
        } else {
            return 0;
        }
    }
}
