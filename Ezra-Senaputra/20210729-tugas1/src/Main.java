import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("1. IDR");
        System.out.println("2. USD");
        System.out.println("3. EUR");
        System.out.print("Pick Your Currency : ");
        int inValuta = input.nextInt();

        System.out.println("Input Amount : ");
        float inAmount = input.nextFloat();

        Valuta valuta = new Valuta();
        Changer changer = new Changer();

        if(inValuta == 1) {
            System.out.println("1. USD");
            System.out.println("2. EUR");
            System.out.print("Convert To : ");
            int inConvert = input.nextInt();
            if(inConvert == 1) {
               float result =  changer.changeIDR("usd", valuta.getPrice("usd"), inAmount );
                System.out.println("Convert Success to $" + result);
            } else if(inConvert == 2) {
                float result = changer.changeIDR("eur", valuta.getPrice("eur"), inAmount);
                System.out.println("Convert Success to €" + result);
            } else {
                System.out.println("Your Choice is not found");
            }
        } else if(inValuta == 2) {
            System.out.println("1. IDR");
            System.out.println("2. EUR");
            System.out.print("Convert To : ");
            int inConvert = input.nextInt();
            if(inConvert == 1) {
                float result =  changer.changeUSD("idr", valuta.getPrice("usd"), inAmount );
                System.out.println("Convert Success to Rp." + result);
            } else if(inConvert == 2) {
                float result = changer.changeIDR("eur", valuta.getPrice("EURidr"), inAmount);
                System.out.println("Convert Success to €" + result);
            } else {
                System.out.println("Your Choice is not found");
            }
        } else if(inValuta == 3) {
            System.out.println("1. IDR");
            System.out.println("2. USD");
            System.out.print("Convert To : ");
            int inConvert = input.nextInt();
            if(inConvert == 1) {
                float result =  changer.changeEUR("idr", valuta.getPrice("eur"), inAmount );
                System.out.println("Convert Success to Rp." + result);
            } else if(inConvert == 2) {
                float result = changer.changeEUR("usd", valuta.getPrice("EURidr"), inAmount);
                System.out.println("Convert Success to $" + result);
            } else {
                System.out.println("Your Choice is not found");
            }
        }
    }
}
