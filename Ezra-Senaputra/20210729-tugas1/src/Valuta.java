public class Valuta implements PriceIDR {

    public float getPrice(String valuta){
        if(valuta.equals("usd")) {
            return PriceIDR.usd;
        } else if(valuta.equals("eur")) {
            return PriceIDR.eur;
        } else if(valuta.equals("USDidr")){
            return PriceIDR.usd / PriceIDR.eur;
        } else if(valuta.equals("EURidr")){
            return PriceIDR.eur / PriceIDR.usd;
        } else {
            return 0;
        }
    }
}
