package model.warehouse;

import java.util.ArrayList;

public class Warehouse {
    private int id;
    private Address address;
    private ArrayList<DistanceMapper> distanceMapper;

    public Warehouse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ArrayList<DistanceMapper> getDistanceMapper() {
        return distanceMapper;
    }

    public void setDistanceMapper(ArrayList<DistanceMapper> distanceMapper) {
        this.distanceMapper = distanceMapper;
    }
}
