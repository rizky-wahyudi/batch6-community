package model.warehouse;

public class DistanceMapper {
    private int id;
    private int fromWarehousId;
    private int toWarehousId;
    private int distance;

    public DistanceMapper() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromWarehousId() {
        return fromWarehousId;
    }

    public void setFromWarehousId(int fromWarehousId) {
        this.fromWarehousId = fromWarehousId;
    }

    public int getToWarehousId() {
        return toWarehousId;
    }

    public void setToWarehousId(int toWarehousId) {
        this.toWarehousId = toWarehousId;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}
