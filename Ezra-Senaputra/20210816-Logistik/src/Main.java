import controller.LogisticController;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        LogisticController logisticController = new LogisticController();
        while(true){
            logisticController.getLogistic();
            Scanner input = new Scanner(System.in);
            System.out.println("Apakah ingin lanjut ?");
            String inputProcess = input.next();
            if (inputProcess.equals("Y")){
                System.out.println("== Welcome ==");
            } else {
                break;
            }
        }

    }
}
