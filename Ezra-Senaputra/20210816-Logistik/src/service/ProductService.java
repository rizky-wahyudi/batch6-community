package service;

import model.product.Product;

public class ProductService {
    Product susuBeruang = new Product();
    Product tisuPaseo = new Product();
    Product botolMinum = new Product();

    public void inputProduct(){
        // Studi Kasus menggunakan Truck Kontainer
        susuBeruang.setId(1);
        susuBeruang.setName("Susu Beruang");
        susuBeruang.setHeight(15); // cm
        susuBeruang.setWidth(3); // cm
        susuBeruang.setDepth(3);
        susuBeruang.setWeight(0.6);
        susuBeruang.setAmount(200);
        susuBeruang.setDeliverAddress("Jl Yos Sudarso No 28, Jakarta Utara");

        // Studi Kasus menggunakan Mobil
        tisuPaseo.setId(2);
        tisuPaseo.setName("Tisu Paseo");
        tisuPaseo.setHeight(12); //cm
        tisuPaseo.setWidth(10); //cm
        tisuPaseo.setDepth(20); //cm
        tisuPaseo.setWeight(0.4);
        tisuPaseo.setAmount(100);
        tisuPaseo.setDeliverAddress("Jl Raya Bogor No 30, Depok");

        // Sttudi kasus menggunakan Motor
        botolMinum.setId(3);
        botolMinum.setName("Botol Minimum - Dark Orange");
        botolMinum.setHeight(30);
        botolMinum.setWidth(5);
        botolMinum.setDepth(5);
        botolMinum.setWeight(0.7);
        botolMinum.setAmount(10);
        botolMinum.setDeliverAddress("TJ Priok Pelabuhan");

    }

    public Product getSusuBeruang() {
        return susuBeruang;
    }

    public Product getTisuPaseo() {
        return tisuPaseo;
    }

    public Product getBotolMinum() {
        return botolMinum;
    }
}
