package service;

import model.warehouse.Address;
import model.warehouse.DistanceMapper;
import model.warehouse.Warehouse;

import java.util.ArrayList;
import java.util.Scanner;

public class WarehouseService {

    private ArrayList<Warehouse> listWarehouse = new ArrayList<>();
    private ArrayList<DistanceMapper> listDistance = new ArrayList<>();

    private Warehouse depokPool = new Warehouse();
    private Warehouse tjpriokPool = new Warehouse();

    // Skenario Depok - Manado
    public void inputWarehouse(){

        // Depok Pool
        depokPool.setId(1);

        Address depokAddress = new Address();
        depokAddress.setId(1);
        depokAddress.setStreet("Jl Raya Bogor Km 20");
        depokAddress.setDistrict("Tapos");
        depokAddress.setProvince("Jawa Barat");
        depokAddress.setCountry("Indonesia");
        depokAddress.setLatitude("180902809");
        depokAddress.setLangitude("920902809");

        depokPool.setAddress(depokAddress);

        // TJ Priok Pool
        tjpriokPool.setId(2);

        Address tjpriokAddress = new Address();
        tjpriokAddress.setId(2);
        tjpriokAddress.setStreet("Pelabuhan Tj Priok");
        tjpriokAddress.setDistrict("Jakarta Utara");
        tjpriokAddress.setProvince("DKI Jakarta");
        tjpriokAddress.setCountry("Indonesia");
        tjpriokAddress.setLatitude("1809099222");
        tjpriokAddress.setLangitude("909092223");

        tjpriokPool.setAddress(tjpriokAddress);


        // Depok - TJ Priok
        DistanceMapper depokTJPriok = new DistanceMapper();
        depokTJPriok.setId(1);
        depokTJPriok.setFromWarehousId(1);
        depokTJPriok.setToWarehousId(2);
        depokTJPriok.setDistance(60);

        DistanceMapper tjpriokDepok = new DistanceMapper();
        tjpriokDepok.setId(2);
        tjpriokDepok.setFromWarehousId(2);
        tjpriokDepok.setToWarehousId(1);
        tjpriokDepok.setDistance(60);


        listDistance.add(depokTJPriok);
        listDistance.add(tjpriokDepok);

        depokPool.setDistanceMapper(listDistance);
        tjpriokPool.setDistanceMapper(listDistance);

    }

    public Warehouse getDepokPool() {
        return depokPool;
    }

    public Warehouse getTjpriokPool() {
        return tjpriokPool;
    }
}
