package service;

import model.Logistic;

public class LogisticService {

    Logistic logisticSusu = new Logistic();
    Logistic logisticTisu = new Logistic();
    Logistic logisticBotol = new Logistic();

    public void inputLogistic(){
        logisticSusu.setId(1);
        logisticSusu.setProductId(1);
        logisticSusu.setTransportId(1);
        logisticSusu.setWarehouseId(1);
        logisticSusu.setToWarehouseId(2);

        logisticTisu.setId(2);
        logisticTisu.setProductId(2);
        logisticTisu.setTransportId(2);
        logisticTisu.setWarehouseId(2);
        logisticTisu.setToWarehouseId(1);

        logisticBotol.setId(3);
        logisticBotol.setProductId(3);
        logisticBotol.setTransportId(3);
        logisticBotol.setWarehouseId(3);
        logisticBotol.setToWarehouseId(3);
    }

    public Logistic getLogisticSusu() {
        return logisticSusu;
    }

    public Logistic getLogisticTisu() {
        return logisticTisu;
    }

    public Logistic getLogisticBotol() {
        return logisticBotol;
    }
}
