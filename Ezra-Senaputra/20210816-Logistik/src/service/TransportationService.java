package service;

import model.transport.Transport;

public class TransportationService {
    private Transport truck = new Transport();
    private Transport motor = new Transport();
    private Transport mobil = new Transport();

    public void inputTransport(){
        truck.setId(1);
        truck.setSize(19);
        truck.setSpeed(40);
        truck.setDriver("Hendra");
        truck.setType("Truck");
        truck.setLoad(10000);
        truck.setMaxItem(1000);

        motor.setId(2);
        motor.setSize(2);
        motor.setSpeed(50);
        motor.setDriver("Maman");
        motor.setType("Motor");
        motor.setLoad(50);
        motor.setMaxItem(60);

        mobil.setId(3);
        mobil.setSize(5);
        mobil.setSpeed(60);
        mobil.setDriver("Bambang");
        mobil.setType("Mobil");
        mobil.setLoad(2000);
        mobil.setMaxItem(500);

    }

    public Transport getTruck() {
        return truck;
    }

    public Transport getMotor() {
        return motor;
    }

    public Transport getMobil() {
        return mobil;
    }
}
