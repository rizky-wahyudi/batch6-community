package controller;

import service.LogisticService;
import service.ProductService;
import service.TransportationService;
import service.WarehouseService;

import java.util.Scanner;

public class LogisticController {

    LogisticService logisticService = new LogisticService();
    WarehouseService warehouseService = new WarehouseService();
    ProductService productService = new ProductService();
    TransportationService transportationService = new TransportationService();

    public void getLogistic(){

        // Input
        warehouseService.inputWarehouse();
        productService.inputProduct();
        transportationService.inputTransport();

        System.out.println("== LOGISTIK PANAMA ==");
        int idDepok = warehouseService.getDepokPool().getId();
        String warehouseDepok = warehouseService.getDepokPool().getAddress().getDistrict();
        System.out.println(idDepok + ". " + warehouseDepok);

        int idTJPriok = warehouseService.getTjpriokPool().getId();
        String warehouseTJPriok = warehouseService.getTjpriokPool().getAddress().getDistrict();
        System.out.println(idTJPriok + ". " + warehouseTJPriok);

        System.out.println("Pilih Warehouse anda");

        Scanner input = new Scanner(System.in);
        int inputProcess = input.nextInt();

        // Menampilkan semua pengiriman dari Depok Pool
        if(inputProcess == 1){
            String name = productService.getTisuPaseo().getName();
            System.out.println("1. " + name);
            int inputNext = input.nextInt();

            if(inputNext == 1){
              double height = productService.getTisuPaseo().getHeight();
              double width = productService.getTisuPaseo().getWidth();
              double depth = productService.getTisuPaseo().getDepth();
              double amount = productService.getTisuPaseo().getAmount();
              double weight = productService.getTisuPaseo().getWeight();
              int loadWeight = transportationService.getMotor().getLoad();
              String result = (amount * weight) <= loadWeight ? "Load OK" : "Overload";

                System.out.println("Name    : " + name);
                System.out.println("Height  : " + height);
                System.out.println("Width   : " + width);
                System.out.println("Depth   : " + depth);
                System.out.println("Amount  : " + amount);
                System.out.println("Volume  : " + height * width * depth * amount);
                System.out.println("Weight  : " + amount * weight + "(" + result + ")");
                System.out.println("===============================");

                System.out.println("Transport   : " + transportationService.getMobil().getType());
                System.out.println("Driver      : " + transportationService.getMobil().getDriver());
                System.out.println("Size        : " + transportationService.getMobil().getSize());
                System.out.println("Speed       : " + transportationService.getMobil().getSpeed());

                System.out.println("===============================");
                System.out.println("To Address  : " + productService.getTisuPaseo().getDeliverAddress());

                System.out.println("From Address: " + warehouseService.getTjpriokPool().
                        getAddress().getDistrict());
                System.out.println("Distance    : " + warehouseService.getTjpriokPool().
                        getDistanceMapper().get(1).getDistance() + "KM (Estimation)");

            }

        } else if(inputProcess == 2){
            String name1 = productService.getSusuBeruang().getName();
            System.out.println("1. "+ name1);

            String name2 = productService.getBotolMinum().getName();
            System.out.println("2. " + name2);

            int inputNext2 = input.nextInt();
            if(inputNext2 == 1){

                double height = productService.getSusuBeruang().getHeight();
                double width = productService.getSusuBeruang().getWidth();
                double depth = productService.getSusuBeruang().getDepth();
                double amount = productService.getSusuBeruang().getAmount();
                double weight = productService.getSusuBeruang().getWeight();

                int loadWeight = transportationService.getTruck().getLoad();
                String result = (amount * weight) <= loadWeight ? "Load OK" : "Overload";

                System.out.println("Name    : " + name1);
                System.out.println("Height  : " + height);
                System.out.println("Width   : " + width);
                System.out.println("Depth   : " + depth);
                System.out.println("Amount  : " + amount);
                System.out.println("Volume  : " + height * width * depth * amount);
                System.out.println("Weight  : " + amount * weight + "(" + result + ")");
                System.out.println("===============================");

                System.out.println("Transport   : " + transportationService.getTruck().getType());
                System.out.println("Driver      : " + transportationService.getTruck().getDriver());
                System.out.println("Size        : " + transportationService.getTruck().getSize());
                System.out.println("Speed       : " + transportationService.getTruck().getSpeed());

                System.out.println("===============================");
                System.out.println("To Address  : " + productService.getSusuBeruang().getDeliverAddress());

                System.out.println("From Address: " + warehouseService.getDepokPool().
                        getAddress().getDistrict());
                System.out.println("Distance    : " + warehouseService.getDepokPool().
                        getDistanceMapper().get(0).getDistance() + "KM (Estimation)");

            } else if(inputNext2 == 2){

                double height = productService.getBotolMinum().getHeight();
                double width = productService.getBotolMinum().getWidth();
                double depth = productService.getBotolMinum().getDepth();
                double amount = productService.getBotolMinum().getAmount();
                double weight = productService.getBotolMinum().getWeight();

                int loadWeight = transportationService.getMotor().getLoad();
                String result = (amount * weight) <= loadWeight ? "Load OK" : "Overload";

                System.out.println("Name    : " + name2);
                System.out.println("Height  : " + height);
                System.out.println("Width   : " + width);
                System.out.println("Depth   : " + depth);
                System.out.println("Amount  : " + amount);
                System.out.println("Volume  : " + height * width * depth * amount);
                System.out.println("Weight  : " + amount * weight + "(" + result + ")");
                System.out.println("===============================");

                System.out.println("Transport   : " + transportationService.getMotor().getType());
                System.out.println("Driver      : " + transportationService.getMotor().getDriver());
                System.out.println("Size        : " + transportationService.getMotor().getSize());
                System.out.println("Speed       : " + transportationService.getMotor().getSpeed());

                System.out.println("===============================");
                System.out.println("To Address  : " + productService.getBotolMinum().getDeliverAddress());

                System.out.println("From Address: " + warehouseService.getDepokPool().
                        getAddress().getDistrict());
                System.out.println("Distance    : " + warehouseService.getDepokPool().
                        getDistanceMapper().get(0).getDistance() + "KM (Estimation)");
            }

        }
    }



}

