package id.co.nexsoft;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan({ "id.co.nexsoft" })
public class MVCconfig extends WebMvcConfigurerAdapter {
}