public class Rates extends Valuta {  
	private Double rateIdrUsd = 0.000069;
	private Double rateIdrEuro = 0.000058;
	private Double rateUsdIdr = 14490d;
	private Double rateUsdEuro = 0.84;
	private Double rateEuroIdr = 17000d;
	private Double rateEuroUsd = 1.18;

	//Getter
	public Double getRateIdrUsd() {
		return this.rateIdrUsd;
	}

	public Double getRateIdrEuro() {
		return this.rateIdrEuro;
	}

	public Double getRateUsdIdr() {
		return this.rateUsdIdr;
	}

	public Double getRateUsdEuro() {
		return this.rateUsdEuro;
	}

	public Double getRateEuroIdr() {
		return this.rateEuroIdr;
	}

	public Double getRateEuroUsd() {
		return this.rateEuroUsd;
	}
	
} 