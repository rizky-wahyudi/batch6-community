public class Valuta {  
	private String valutaFrom;
	private String valutaTo;
	private Double exchangeRate;

	//Setter
	public void setValuta(String valutaFrom, String valutaTo, Double exchangeRate) {
		this.valutaFrom = valutaFrom;
		this.valutaTo = valutaTo;
		this.exchangeRate = exchangeRate;
	}

	//Getter
	public String getValutaFrom () {
		return this.valutaFrom;
	}

	public String getValutaTo () {
		return this.valutaTo;
	}

	public Double getExchangeRate () {
		return this.exchangeRate;
	}
} 