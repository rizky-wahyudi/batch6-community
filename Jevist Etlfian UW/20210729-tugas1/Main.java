import java.util.Scanner; 
public class Main {  
	public static void main(String args[])  {
		PrintLine line = new PrintLine();
		Scanner input = new Scanner(System.in);
		String currencyFrom = "";
		String currencyTo = "";
		String[] idCurrency = {"IDR","USD","EURO"};

		while ( currencyFrom.equals(currencyTo)) {
			line.printLineOne();
			boolean checkCurrencyFrom = false;
			boolean checkCurrencyTo = false;
			System.out.println(">>>>>Code Currency<<<<<");
			System.out.println("1=IDR , 2=USD , 3=EURO");
			line.printLineOne();

			// Checking CurrencyFrom Code Valid?, Bisa buat method baru dengan mengirim parameter, return boolean, dengan 3 input parameter
			while (!checkCurrencyFrom) {
				System.out.print("Your Code Currency : ");
				currencyFrom = input.next();
				for (String var : idCurrency) {
					if (currencyFrom.equals(var)) {
						checkCurrencyFrom = true;
						break;
					} else {
						checkCurrencyFrom = false;
						continue;
					}
				}
			}

			// Checking CurrencyTo Code Valid?, Bisa buat method baru dengan mengirim parameter,return boolean
			while (!checkCurrencyTo) {
				System.out.print("Choose The Currency Code You Want To Convert : ");
				currencyTo = input.next();
				for (String var : idCurrency) {
					if (currencyTo.equals(var)) {
						checkCurrencyTo = true;
						break;
					} else {
						checkCurrencyTo = false;
						continue;
					}
				}
			}
			line.printLineTwo();	
		}

		System.out.print("Your Amount Money : ");
		Double amountMoney = input.nextDouble();
		Code idCode = new Code();
		Rates rateValue = new Rates();
		ChangeMoney convert = new ChangeMoney();
		Valuta valuta = new Valuta();
		double resultConvert = 0d;

		// Bisa buat method baru
		if (currencyFrom.equals("IDR")) {

			if (currencyTo.equals("USD")) {
				resultConvert = convert.conversion(amountMoney,rateValue.getRateIdrUsd());
				valuta.setValuta(idCode.getIdIdr(), idCode.getIdUsd() , resultConvert);
			} else if (currencyTo.equals("EURO")) {
				resultConvert = convert.conversion(amountMoney,rateValue.getRateIdrEuro());
				valuta.setValuta(idCode.getIdIdr(), idCode.getIdEuro() , resultConvert);
			} else {
				System.out.println("Error input currency");
			}
			System.out.println(valuta.getValutaFrom() + " - " + valuta.getValutaTo());
			System.out.println("Conversion Currency = " + valuta.getExchangeRate() + " " + valuta.getValutaTo());

		} else if (currencyFrom.equals("USD")) {

			if (currencyTo.equals("IDR")) {
				resultConvert = convert.conversion(amountMoney,rateValue.getRateUsdIdr());
				valuta.setValuta(idCode.getIdUsd(), idCode.getIdIdr() , resultConvert);
			} else if (currencyTo.equals("EURO")) {
				resultConvert = convert.conversion(amountMoney,rateValue.getRateUsdEuro());
				valuta.setValuta(idCode.getIdUsd(), idCode.getIdEuro() , resultConvert);
			} else {
				System.out.println("Error input currency");
			}
			System.out.println(valuta.getValutaFrom() + " - " + valuta.getValutaTo());
			System.out.println("Conversion Currency = " + valuta.getExchangeRate() + " " + valuta.getValutaTo());

		} else if (currencyFrom.equals("EURO")) {

			if (currencyTo.equals("IDR")) {
				resultConvert = convert.conversion(amountMoney,rateValue.getRateEuroIdr());
				valuta.setValuta(idCode.getIdEuro(), idCode.getIdIdr() , resultConvert);
			} else if (currencyTo.equals("USD")) {
				resultConvert = convert.conversion(amountMoney,rateValue.getRateEuroUsd());
				valuta.setValuta(idCode.getIdEuro(), idCode.getIdUsd() , resultConvert);
			} else {
				System.out.println("Error input currency");
			}
			System.out.println(valuta.getValutaFrom() + " - " + valuta.getValutaTo());
			System.out.println("Conversion Currency = " + valuta.getExchangeRate() + " " + valuta.getValutaTo());

		} else {
			System.out.println("Error");
		} 

	}  
} 