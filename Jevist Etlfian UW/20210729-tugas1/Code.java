public class Code extends Valuta {  
	private String idUsd = "USD";
	private String idIdr = "IDR";
	private String idEuro = "EURO";

	//Getter
	public String getIdUsd() {
		return this.idUsd;
	}

	public String getIdIdr() {
		return this.idIdr;
	}

	public String getIdEuro() {
		return this.idEuro;
	}
} 