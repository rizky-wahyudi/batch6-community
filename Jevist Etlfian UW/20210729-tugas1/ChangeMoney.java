public class ChangeMoney { 
	private Double conversionMoney;
	
	public Double conversion(Double amountMoney, Double exchangeRate) {
		this.conversionMoney= amountMoney * exchangeRate;
		return this.conversionMoney;
	}
}