package com.data;

public class DataVendingMachine {
	private char[] row = new char[6];
	private int[] slot = new int[10];
	
	private String[] rowSlot = new String[60];
	private String[] nameData = new String [60];
	private int[] price = new int[60];
	
	public void setRow() {
		int startRow = 0;
		for (char ch='A'; ch <= 'F'; ++ch ) {
			this.row[startRow] = ch;
			startRow++;
		}
	}
	
	public char getRow(int index) {
		return this.row[index];
	}
	
	public void setSlot() {
		for (int i = 0; i < slot.length; i++) {
			this.slot[i] = i+1;
		}
	}
	
	public int getSlot(int index) {
		return this.slot[index];
	}
	
	public void setNamePriceData () {
		int start = 0;
		for (char ch='A'; ch <= 'F'; ++ch ) {
			for (int i = 0; i < 10; i++) {
				if (ch == 'A') {
					this.rowSlot[start] = String.valueOf(ch) + "" + String.valueOf(i+1);
					this.nameData[start] = "Chips" + String.valueOf(i+1);
					this.price[start] = 7000;
				} else if (ch == 'B') {
					this.rowSlot[start] = String.valueOf(ch) + "" + String.valueOf(i+1);
					this.nameData[start] = "Chocolates" + String.valueOf(i+1);
					this.price[start] = 4000;
				} else if (ch == 'C') {
					this.rowSlot[start] = String.valueOf(ch) + "" + String.valueOf(i+1);
					this.nameData[start] = "Candies" + String.valueOf(i+1);
					this.price[start] = 1000;
				} else if (ch == 'D') {
					this.rowSlot[start] = String.valueOf(ch) + "" + String.valueOf(i+1);
					this.nameData[start] = "Water" + String.valueOf(i+1);
					this.price[start] = 3000;
				} else if (ch == 'E') {
					this.rowSlot[start] = String.valueOf(ch) + "" + String.valueOf(i+1);
					this.nameData[start] = "Softdrinks" + String.valueOf(i+1);
					this.price[start] = 5000;
				} else if (ch == 'F') {
					this.rowSlot[start] = String.valueOf(ch) + "" + String.valueOf(i+1);
					this.nameData[start] = "IceTea" + String.valueOf(i+1);
					this.price[start] = 5000;
				} else {
					this.rowSlot[start] = String.valueOf(ch) + "" + String.valueOf(i+1);
					System.out.println("Out of Row");
				}
				start++;
			}
		}
	}
	
	public String getNameData(int i) {
		return this.nameData[i];
	}
	
	public String getRowSlot(int i) {
		return this.rowSlot[i];
	}
	
	public int getNameDataLength() {
		return this.nameData.length;
	}
	
	public int getPriceData(int i) {
		return this.price[i];
	}
	
	
}
