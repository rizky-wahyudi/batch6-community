package com.main;
import com.data.DataVendingMachine;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		HashMap <String,Integer> dataVendingMachine = new HashMap<>();
		DataVendingMachine data = new DataVendingMachine();
		data.setRow();
		data.setSlot();
		data.setNamePriceData();	
		int amountMoney = 0;
		
		System.out.println(">>>>>>Vending Machine<<<<<<");
		for (int i = 0; i < data.getNameDataLength(); i++) {
			dataVendingMachine.put(data.getRowSlot(i), data.getPriceData(i));
			System.out.println("Slot:" +data.getRowSlot(i) + " Nama : " + data.getNameData(i) + " harga:" +data.getPriceData(i));
		}
		System.out.println("=====================================================");
		
		System.out.println(dataVendingMachine.get("A1"));
		for (String i : dataVendingMachine.keySet()) {
			  System.out.println(i);
			}
		
		boolean vendingMachine = true;
		boolean useVendingMachine = false;
		while (!useVendingMachine) {
			System.out.println("Apakah mau menggunakan Vending Machine: Ya/Tidak");
			System.out.print("Jawaban Anda: ");
			String jawaban = input.next();
			if (jawaban.equalsIgnoreCase("Ya")) {
				vendingMachine = true;
				useVendingMachine = true;
			} else if (jawaban.equalsIgnoreCase("Tidak")) {
				vendingMachine = false;
				useVendingMachine = false;
			} else {
				useVendingMachine = false;
			}
		}
		
		int amountMoneyVendingMachine = 0;
		String slot = "";
		while (vendingMachine) {
			System.out.println("Uang pada Vending Machine : " + amountMoneyVendingMachine);
			System.out.print("Masukin Uang: ");
			amountMoney = input.nextInt();
			amountMoneyVendingMachine = amountMoneyVendingMachine + amountMoney;
			System.out.println("Uang pada Vending Machine : " + amountMoneyVendingMachine);			

			boolean pilihanSlotAda = false;
			while (!pilihanSlotAda) {
				System.out.print("Pilih Slot (A1 - F10) : ");
				slot = input.next();
				for (String cekSlot : dataVendingMachine.keySet()) {
					if (cekSlot.equals(slot)) {
						pilihanSlotAda = true;
						break;
					} else {
						pilihanSlotAda = false;
						continue;
					}
				}
			}
			if (dataVendingMachine.get(slot) <= amountMoneyVendingMachine) {
				amountMoneyVendingMachine = amountMoneyVendingMachine - dataVendingMachine.get(slot);
				System.out.println("Barang diKeluarkan");
				break;
			} else if (dataVendingMachine.get(slot) > amountMoneyVendingMachine) {
				int uangKurang = dataVendingMachine.get(slot) - amountMoneyVendingMachine;
				System.out.println("Uang Tidak Cukup, Kekurangan " + uangKurang);
				System.out.println("Harga barang = " + dataVendingMachine.get(slot));
			} else {
				System.out.println("Error Vending machine");
			}
		}
		
		System.out.println("Uang dikembalikan sebanyak " + amountMoneyVendingMachine);
		System.out.println("Selesai Pembelajaan");
	}

}
