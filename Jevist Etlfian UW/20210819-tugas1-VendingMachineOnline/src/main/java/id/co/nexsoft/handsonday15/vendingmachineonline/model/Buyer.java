package id.co.nexsoft.handsonday15.vendingmachineonline.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Buyer {
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int slotId;
	private double amountMoney;
	private int amountProduct;
	
	public Buyer() {
	}
	
	public Buyer(int slotId, double amountMoney, int amountProduct) {
		this.slotId = slotId;
		this.amountMoney = amountMoney;
		this.amountProduct = amountProduct;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSlotId() {
		return slotId;
	}
	public void setSlotId(int slotId) {
		this.slotId = slotId;
	}
	public double getAmountMoney() {
		return amountMoney;
	}
	public void setAmountMoney(double amountMoney) {
		this.amountMoney = amountMoney;
	}
	public int getAmountProduct() {
		return amountProduct;
	}
	public void setAmountProduct(int amountProduct) {
		this.amountProduct = amountProduct;
	}
}
