package id.co.nexsoft.handsonday15.vendingmachineonline.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.handsonday15.vendingmachineonline.model.Slot;

@Repository
public interface SlotRepository extends CrudRepository<Slot, Integer> {
    Slot findById(int id);
    List<Slot> findAll();
    void deleteById(int id);
}
