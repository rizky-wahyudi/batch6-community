package id.co.nexsoft.handsonday15.vendingmachineonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendingmachineonlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(VendingmachineonlineApplication.class, args);
	}

}
