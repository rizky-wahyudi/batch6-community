package id.co.nexsoft.handsonday15.vendingmachineonline.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.handsonday15.vendingmachineonline.model.Buyer;
import id.co.nexsoft.handsonday15.vendingmachineonline.model.Product;
import id.co.nexsoft.handsonday15.vendingmachineonline.model.Slot;
import id.co.nexsoft.handsonday15.vendingmachineonline.repository.BuyerRepository;
import id.co.nexsoft.handsonday15.vendingmachineonline.repository.ProductRepository;
import id.co.nexsoft.handsonday15.vendingmachineonline.repository.SlotRepository;

@RestController 
public class VendingMachineOnlineController {
	@Autowired
	private SlotRepository slotRepo;
	@Autowired
	private ProductRepository productRepo;
	@Autowired
	private BuyerRepository buyerRepo;
	
    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
            + "<h1>VENDING MACHINE ONLINE</h1>"
            + "</body></html>";
    }

    @GetMapping("/slots")
    public List<Slot> getAllSlot() {
    	return slotRepo.findAll();
    }
    
    @GetMapping("/products")
    public List<Product> getAllProduct() {
    	return productRepo.findAll();
    }
    
    @GetMapping("/buyers")
    public List<Buyer> getAllBuyer() {
    	return buyerRepo.findAll();
    }
    
    @GetMapping("/slots/{id}")
    public Slot getSlotByID(@PathVariable(value = "id") int id) {
    	return slotRepo.findById(id);
    }
    
    @GetMapping("/products/{id}")
    public Product getProductByID(@PathVariable(value = "id") int id) {
    	return productRepo.findById(id);
    }
    
    @GetMapping("/buyer/{id}")
    public Buyer getBuyerByID(@PathVariable(value = "id") int id) {
    	return buyerRepo.findById(id);
    }
    
    @PostMapping(value = "/slots", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Slot> addSlot(@RequestBody List<Slot> slot) {
        return (List<Slot>) slotRepo.saveAll(slot);
    }
    
    @PostMapping(value = "/products", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Product> addProduct(@RequestBody List<Product> product) {
    	return (List<Product>) productRepo.saveAll(product);
    }
    
    @PostMapping(value = "/buyers", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Buyer> addBuyer(@RequestBody List<Buyer> buyer) {
    	return (List<Buyer>) buyerRepo.saveAll(buyer);
    }
    
//    @PostMapping(value = "/slots/buy", consumes = "application/json")
//    @ResponseStatus(HttpStatus.CREATED)
//    public Slot buySlotProduct(@RequestBody Slot slot) {
//    	Slot slotObj = new Slot();
//    	Product productObj = new Product();
//        return slotRepo.save(slot);
//    }
}
