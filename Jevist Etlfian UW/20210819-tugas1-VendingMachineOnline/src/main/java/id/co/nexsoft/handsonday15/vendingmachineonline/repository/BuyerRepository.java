package id.co.nexsoft.handsonday15.vendingmachineonline.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.handsonday15.vendingmachineonline.model.Buyer;

@Repository
public interface BuyerRepository extends CrudRepository<Buyer, Integer> {
	Buyer findById(int id);
    List<Buyer> findAll();
    void deleteById(int id);
}
