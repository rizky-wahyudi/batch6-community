package id.co.nexsoft.handsonday15.vendingmachineonline.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Slot {
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String slotPosition;
	private int slotStock;
	private int productId;

    public Slot() {
    }
    
    public Slot(String slotPosition, int slotStock, int productId) {
    	this.slotPosition = slotPosition;
    	this.slotStock = slotStock;
    	this.productId = productId;
    }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSlotPosition() {
		return slotPosition;
	}
	public void setSlotPosition(String slotPosition) {
		this.slotPosition = slotPosition;
	}
	public int getSlotStock() {
		return slotStock;
	}
	public void setSlotStock(int slotStock) {
		this.slotStock = slotStock;
	}

	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
}