package id.co.nexsoft.handsonday15.vendingmachineonline.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.handsonday15.vendingmachineonline.model.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
    Product findById(int id);
    List<Product> findAll();
    void deleteById(int id);
}
