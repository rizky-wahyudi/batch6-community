select
	pdi.description,
	pd.sellPrice,
	pdist.buyPrice,
	pd.expiredDate,
	pds.stock 		AS 'Product Stock',
	pdist.stock 		AS 'Distributor Stock',
	dist.name,
	ad.city
FROM	
	productStock pds	
	left join product 		pd 	ON pds.productId = pd.id
	left join productImages 	pdi 	ON pd.productImagesid = pdi.id
	left join productDistributor 	pdist 	ON pds.productdistributorId = pdist.id
	left join distributor 		dist 	ON pdist.distributorId = dist.id
	left join address 		ad 	ON dist.addressId = ad.id
WHERE
	pds.stock = 0;
