CREATE TABLE handsonday10_tugas2.address (
	id INT NOT NULL AUTO_INCREMENT,
	street VARCHAR(225) NOT NULL,
	longtitude DECIMAL(11,8) NOT NULL,
	latititude DECIMAL(10,8) NOT NULL,
	city VARCHAR(25)  NOT NULL,
	district VARCHAR(25) NOT NULL,
	province VARCHAR(25) NOT NULL,
	country VARCHAR(25) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday10_tugas2.distributor (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	addressId INT NOT NULL,
	addressType VARCHAR(50) NOT NULL,
	FOREIGN KEY (addressId) REFERENCES address(id),
	PRIMARY KEY (id));

CREATE TABLE handsonday10_tugas2.productDistributor (
	id INT NOT NULL AUTO_INCREMENT,
	distributorId INT NOT NULL,
	buyPrice DECIMAL(20,2) NOT NULL,
	stock INT NOT NULL,
	FOREIGN KEY (distributorId) REFERENCES distributor(id),
	PRIMARY KEY (id));

CREATE TABLE handsonday10_tugas2.productImages (
	id INT NOT NULL AUTO_INCREMENT,
	url VARCHAR(2083) NOT NULL,
	description TEXT,
	PRIMARY KEY (id));

CREATE TABLE handsonday10_tugas2.product (
	id INT NOT NULL AUTO_INCREMENT,
	artNumber VARCHAR(50) NOT NULL,
	productImagesId INT NOT NULL,
	sellPrice DECIMAL(20,2) NOT NULL,
	expiredDate TEXT,
	FOREIGN KEY (productImagesId) REFERENCES productImages(id),
	PRIMARY KEY (id));

CREATE TABLE handsonday10_tugas2.productStock (
	id INT NOT NULL AUTO_INCREMENT,
	productId INT NOT NULL,
	stock INT NOT NULL,
	productDistributorId INT NOT NULL,
	purchaseDate DATE,
	deliveryDate DATE,
	FOREIGN KEY (productId) REFERENCES product(id),
	FOREIGN KEY (productDistributorId) REFERENCES productDistributor(id),
	PRIMARY KEY (id));