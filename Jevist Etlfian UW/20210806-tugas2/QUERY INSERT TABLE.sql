INSERT INTO productImages (url,description) VALUES
	('https://cdn.popbela.com/content-images/post/20210309/httpsdeboravictorya2016wordpresscomlifebuoy-191d106eb088b9c815f3056d3a9d7adc.jpg','Lifebuoy Sabun Mandi'),
	('https://cdn.popbela.com/content-images/post/20210309/sunlightcoidvarian-sunlight-f0dbb5f872d178277502573fd73aa25b.jpg','Sunlight Sabun Cuci Piring'),
	('https://cdn.popbela.com/content-images/post/20210309/ipricecompepsodent-6c3f1b614e22c008b9bda84c141b5c18.png','Pepsodent Odor Gigi'),
	('https://cdn.popbela.com/content-images/post/20210309/1-3d7639273a8255b4f98600e3f5c1d0f6.jpg','Rexona'),
	('https://cdn.popbela.com/content-images/post/20210309/alfacartcomrinso-3335d866e9fe791eebd77e1bbc4836c9.png','Rinso Sabun Cuci Baju'),
	('https://cdn.popbela.com/content-images/post/20210309/sariwangi-youtube-0d088c45ab716cb3e3bfa68ac16ca3f3.jpg','Sariwangi Teh Celup'),
	('https://cdn.popbela.com/content-images/post/20210309/alfacart-royco-bf08c76dad25e154d1d571354268f435.jpg','Royco'),
	('https://cdn.popbela.com/content-images/post/20210309/hargaoricombango-33739d6872351d80d6aaa76dd3d6fa11.png','Bango Kecap Manis'),
	('https://cdn.popbela.com/content-images/post/20210309/2-2d5eb61338f0c0a9d87e2edc9bb98a2e.jpg','Walls Ice Cream'),
	('https://cdn.popbela.com/content-images/post/20210309/behancenetbuavita-f70352a7bb8a509f680ab1c4b98278b9.jpg','Buavita Minuman Jus Buah');

INSERT INTO product (artNumber,productImagesId,sellPrice,expiredDate) VALUES
	('LFBY01',1,7000,'2025-08-19'),
	('SNLT02',2,25000,'2025-05-04'),
	('PSDT03',3,10000,'2024-01-15'),
	('RXNA04',4,45000,'2026-04-14'),
	('RNSO05',5,20000,'2023-09-11'),
	('SRWG06',6,9000,'2022-03-08'),
	('RYCO07',7,3000,'2022-04-21'),
	('BNGO08',8,7000,'2023-07-27'),
	('WLIC09',9,13000,'2022-03-24'),
	('BAVT10',10,17000,'2022-06-01');

INSERT INTO address (street, longtitude, latitude, city, district,province,country) VALUES
	('Jalan Jendral Sudirman',106.81398632910272,-6.217836526376508,'Jakarta Pusat','Semanggi','Jakarta','Indonesia'),
	('Jalan BSD Bouelevard Utara',106.64171983964142,-6.286984170038634,'Tangerang','Lengkong Kulon','Banten','Indonesia'),
	('Jalan Alam Sutera Boulevard',106.8148017206387,-6.2428505845843,'Tangerang Selatan','Alam Sutera','Banten','Indonesia');

INSERT INTO distributor (name, addressId, addressType) VALUES
	('Alex William',1,'email'),
	('Kevin Budianta',2,'email'),
	('Christiana Jessica',3,'email');

INSERT INTO productDistributor (distributorId, buyPrice, stock) VALUES
	(1,10000,10),
	(1,30000,5),
	(1,15000,10),
	(1,50000,0),
	(1,23000,10),
	(2,55000,10),
	(2,30000,10),
	(2,19000,0),
	(2,13000,5),
	(2,17000,0),
	(3,10000,5),
	(3,16000,0),
	(3,20000,10),
	(3,48000,5),
	(3,28000,10);

INSERT INTO productStock (productId, stock, productdistributorId, purchaseDate, DeliveryDate) VALUES
	(4,10,1,'2021-08-07','2021-08-09'),
	(3,0,2,'2021-08-09','2021-08-16'),
	(1,10,3,'2021-08-10','2021-08-18'),
	(2,5,4,'2021-08-10','2021-08-18'),
	(9,0,5,'2021-08-15','2021-08-25'),
	(4,0,6,'2021-08-08','2021-08-15'),
	(5,0,7,'2021-08-09','2021-08-16'),
	(6,5,8,'2021-08-10','2021-08-17'),
	(7,10,9,'2021-08-10','2021-08-17'),
	(8,10,10,'2021-08-10','2021-08-17'),
	(8,5,11,'2021-08-13','2021-08-21'),
	(9,0,12,'2021-08-13','2021-08-22'),
	(10,10,13,'2021-08-16','2021-08-27'),
	(4,0,14,'2021-08-18','2021-08-24'),
	(2,10,15,'2021-08-20','2021-08-24');