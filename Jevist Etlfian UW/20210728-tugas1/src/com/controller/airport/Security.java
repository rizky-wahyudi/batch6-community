package com.controller.airport;

public class Security {
	private String[] forbiddenStuff = {"Pisau","Pistol","Bom"};

	public int getForbiddenStuffLength() {
		return forbiddenStuff.length;
	}
	
	public String getForbiddenStuff(int i) {
		return forbiddenStuff[i];
	}
	
	
}
