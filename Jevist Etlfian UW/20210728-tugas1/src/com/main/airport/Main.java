package com.main.airport;

import java.util.ArrayList;
import java.util.Scanner;

import com.controller.airport.Security;
import com.data.airport.DatabaseAirport;
import com.data.person.Luggage;
import com.data.person.PersonData;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		PersonData dataOrangSatu = new PersonData();
		Luggage isiLuggageOrangSatu = new Luggage();
		Luggage isiHandLuggageOrangSatu = new Luggage();
		
		dataOrangSatu.setCodeTicket("TIKETCODESATU");
		dataOrangSatu.setDataPassport("Jevist Etlfian", 123456789);
		ArrayList<String> isiLuggage = new ArrayList<String>();
		isiLuggage.add("Baju");
		isiLuggage.add("Celana");
		isiLuggage.add("Pakaian Dalam");
		isiLuggageOrangSatu.setWeight(17);
		isiLuggageOrangSatu.setStuff(isiLuggage);
		
		ArrayList<String> isiHandLuggage = new ArrayList<String>();
		isiHandLuggage.add("Baju");
		isiHandLuggage.add("Laptop");
		isiHandLuggage.add("Pakaian");
		isiHandLuggage.add("Celana");
		isiHandLuggageOrangSatu.setWeight(5);
		isiHandLuggageOrangSatu.setStuff(isiHandLuggage);
		
		Security barangLarangan = new Security();		
		boolean barangAman = true;
		System.out.println(">>>>>Security Xray Scan<<<<<");
		for (int i = 0; i < barangLarangan.getForbiddenStuffLength(); i++) {
			for (int j = 0; j < isiLuggageOrangSatu.getStuff().size(); j++) {
				if (isiLuggageOrangSatu.getStuff().get(j).equalsIgnoreCase(barangLarangan.getForbiddenStuff(i))) {
					System.out.println("Terdapat barang larangan : " + isiLuggageOrangSatu.getStuff().get(j));
					barangAman = false;
				} else {
					continue;
				}
			}
		}
		
		while (!barangAman) {
			System.out.println("Ada barang larangan");
			System.out.println("Apakah mau keluarin barang larangan : Ya/Tidak;");
			System.out.print("Masukin Jawaban Anda : ");
			String jawaban = input.next();
			if (jawaban.equalsIgnoreCase("ya")) {
				System.out.println("Barang Larangan sudah dikeluarin");
				barangAman = true;
			} else {
				System.out.println("Silahkan setuju keluarin barang dulu baru boleh melanjutkan ke imigrasi");
			}
		}
		System.out.println("Semua Barang sudah aman");
		System.out.println("Anda boleh melanjutkan ke counter");
		
		boolean dataSesuai = false;
		DatabaseAirport dataAirport = new DatabaseAirport();
		while (!dataSesuai) {
			System.out.println(">>>>>Counter<<<<<");
			if (dataOrangSatu.getCodeTicket().equalsIgnoreCase(dataAirport.getCodeTiketOrang()) && 
					dataOrangSatu.getNama().equalsIgnoreCase(dataAirport.getNamaOrang()) && 
					dataOrangSatu.getNik() == dataAirport.getNik() &&
					isiLuggageOrangSatu.getWeight() <= dataAirport.getBatasBerat())  {
				System.out.println("Data Sesuai silahkan lanjut ke imigrasi");
				dataSesuai = true;
			}	
			else {
			 System.out.println("Data tidak sesuai mohon dikoreksi data anda");
			 System.out.print("masukan kode tiket anda : ");
			 String kodeTiket = input.nextLine();
			 System.out.print("masukan nama anda : ");
			 String namaOrang = input.nextLine();
			 System.out.print("masukan nik anda : ");
			 int nikOrang = input.nextInt();
			 System.out.print("masukan berat luggage anda : ");
			 int beratluggage = input.nextInt();
			 dataOrangSatu.setCodeTicket(kodeTiket);
			 dataOrangSatu.setDataPassport(namaOrang, nikOrang);
			 isiLuggageOrangSatu.setWeight(beratluggage);
			 dataSesuai = false;
			}
		}
		
		dataSesuai = false;
		while (!dataSesuai) {
			System.out.println(">>>>>Imigrasi<<<<<");
			if (dataOrangSatu.getCodeTicket().equalsIgnoreCase(dataAirport.getCodeTiketOrang()) && 
					dataOrangSatu.getNama().equalsIgnoreCase(dataAirport.getNamaOrang()) && 
					dataOrangSatu.getNik() == dataAirport.getNik())  {
				System.out.println("Data Sesuai silahkan lanjut ke Waiting Room");
				dataSesuai = true;
			}	
			else {
			 System.out.println("Data tidak sesuai mohon dikoreksi data anda");
			 System.out.print("masukan kode tiket anda : ");
			 String kodeTiket = input.nextLine();
			 System.out.print("masukan nama anda : ");
			 String namaOrang = input.nextLine();
			 System.out.print("masukan nik anda : ");
			 int nikOrang = input.nextInt();
			 dataOrangSatu.setCodeTicket(kodeTiket);
			 dataOrangSatu.setDataPassport(namaOrang, nikOrang);
			 dataSesuai = false;
			}
		}
		
		barangAman = true;
		System.out.println(">>>>>Xray Scan 2<<<<<");
		for (int i = 0; i < barangLarangan.getForbiddenStuffLength(); i++) {
			for (int j = 0; j < isiHandLuggageOrangSatu.getStuff().size(); j++) {
				if (isiHandLuggageOrangSatu.getStuff().get(j).equalsIgnoreCase(barangLarangan.getForbiddenStuff(i))) {
					System.out.println("Terdapat barang larangan : " + isiHandLuggageOrangSatu.getStuff().get(j));
					barangAman = false;
				} else {
					continue;
				}
			}
		}
		
		while (!barangAman) {
			System.out.println("Ada barang larangan di hand lugagge anda");
			System.out.println("Apakah mau keluarin barang larangan : Ya/Tidak;");
			System.out.print("Masukin Jawaban Anda : ");
			String jawaban = input.next();
			if (jawaban.equalsIgnoreCase("ya")) {
				System.out.println("Barang Larangan sudah dikeluarin");
				barangAman = true;
			} else {
				System.out.println("Silahkan setuju keluarin barang dulu baru boleh melanjutkan ke Waiting Room");
			}
		}
		System.out.println("Semua Barang sudah aman");
		System.out.println("Anda boleh melanjutkan ke Waiting Room");
		
		dataSesuai = false;
		while (!dataSesuai) {
			System.out.println(">>>>>Waiting Room<<<<<");
			if (dataOrangSatu.getCodeTicket().equalsIgnoreCase(dataAirport.getCodeTiketOrang()) && 
					dataOrangSatu.getNama().equalsIgnoreCase(dataAirport.getNamaOrang()) && 
					dataOrangSatu.getNik() == dataAirport.getNik())  {
				System.out.println("Data Sesuai silahkan Naik ke Pesawat");
				dataSesuai = true;
			}	
			else {
			 System.out.println("Data tidak sesuai mohon dikoreksi data anda");
			 System.out.print("masukan kode tiket anda : ");
			 String kodeTiket = input.nextLine();
			 System.out.print("masukan nama anda : ");
			 String namaOrang = input.nextLine();
			 System.out.print("masukan nik anda : ");
			 int nikOrang = input.nextInt();
			 dataOrangSatu.setCodeTicket(kodeTiket);
			 dataOrangSatu.setDataPassport(namaOrang, nikOrang);
			 dataSesuai = false;
			}
		}
		System.out.println("Sudah naik ke pesawat menuju liburan");
	
	}
	
}
