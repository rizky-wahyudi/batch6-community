package com.data.person;

public class PersonData {
	private String codeTicket;
	private String nama;
	private int nik;
	
	public void setDataPassport(String nama, int nik) {
		this.nama = nama;
		this.nik = nik;
	}
	
	public String getNama() {
		return nama;
	}
	public int getNik() {
		return nik;
	}

	public String getCodeTicket() {
		return codeTicket;
	}

	public void setCodeTicket(String codeTicket) {
		this.codeTicket = codeTicket;
	}
}
