package com.data.person;
import java.util.ArrayList;

public class Luggage extends PersonData {
	private int weight;
	private ArrayList<String> stuff = new ArrayList<String>();
	
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public ArrayList<String> getStuff() {
		return stuff;
	}
	public void setStuff(ArrayList<String> stuff) {
		this.stuff = stuff;
	}
}
