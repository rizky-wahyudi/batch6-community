package com.data.airport;

public class DatabaseAirport {
	private String namaOrang = "Jevist Etlfian";
	private String codeTiketOrang = "TIKETCODESATU";
	private int nik = 123456789;
	private int batasBerat = 20;
	
	public String getNamaOrang() {
		return namaOrang;
	}

	public int getNik() {
		return nik;
	}

	public int getBatasBerat() {
		return batasBerat;
	}

	public String getCodeTiketOrang() {
		return codeTiketOrang;
	}
}
