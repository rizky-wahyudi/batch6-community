package id.co.nexsoft.handsonday16.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.handsonday16.laundry.model.Orders;
import id.co.nexsoft.handsonday16.laundry.repository.OrdersRepository;

@RestController 
public class OrdersController {
	@Autowired
	private OrdersRepository ordersRepo;
	
    @GetMapping("/orders")
    public List<Orders> getAllOrders() {
    	return ordersRepo.findAll();
    }
    
    @GetMapping("/orders/{id}")
    public Orders getOrdersByID(@PathVariable(value = "id") int id) {
    	return ordersRepo.findById(id);
    }
    
    //Orderitems dan employee Id harus manual input dari database, postmapping lewat json masih gagal
    @PostMapping(value = "/orders", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Orders addOrders(@RequestBody Orders order) {
        return ordersRepo.save(order);
    }
}
