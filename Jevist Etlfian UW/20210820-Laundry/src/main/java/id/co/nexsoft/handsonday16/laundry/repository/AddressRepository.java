package id.co.nexsoft.handsonday16.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.handsonday16.laundry.model.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer>{
	Address findById(int id);
	List<Address> findAll();
	void deleteById(int id);
}
