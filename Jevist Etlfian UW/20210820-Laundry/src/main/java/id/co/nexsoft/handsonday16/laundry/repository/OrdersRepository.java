package id.co.nexsoft.handsonday16.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.handsonday16.laundry.model.Orders;

@Repository
public interface OrdersRepository extends CrudRepository<Orders, Integer>{
	Orders findById(int id);
	List<Orders> findAll();
	void deleteById(int id);
}
