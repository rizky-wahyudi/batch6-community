package id.co.nexsoft.handsonday16.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.handsonday16.laundry.model.Service;
import id.co.nexsoft.handsonday16.laundry.repository.ServiceRepository;

@RestController 
public class ServiceController {
	@Autowired
	private ServiceRepository serviceRepo;
	
    @GetMapping("/services")
    public List<Service> getAllService() {
    	return serviceRepo.findAll();
    }
    
    @GetMapping("/services/{id}")
    public Service getServiceByID(@PathVariable(value = "id") int id) {
    	return serviceRepo.findById(id);
    }
    
    @PostMapping(value = "/services", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Service> addService(@RequestBody List<Service> service) {
        return (List<Service>) serviceRepo.saveAll(service);
    }

}
