package id.co.nexsoft.handsonday16.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.handsonday16.laundry.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer>{
	Employee findById(int id);
	List<Employee> findAll();
	void deleteById(int id);
}
