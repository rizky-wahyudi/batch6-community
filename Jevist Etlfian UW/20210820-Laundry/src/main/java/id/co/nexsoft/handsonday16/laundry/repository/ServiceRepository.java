package id.co.nexsoft.handsonday16.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.handsonday16.laundry.model.Service;

@Repository
public interface ServiceRepository extends CrudRepository<Service, Integer>{
	Service findById(int id);
	List<Service> findAll();
	void deleteById(int id);
}
