package id.co.nexsoft.handsonday16.laundry.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class OrderItems {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String itemName;
	private double weight;
	private double amount;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "serviceId", referencedColumnName = "id")
	private Service service;
	
	public OrderItems()
	{
	}
	
	public OrderItems(String itemName, double weight, double amount,
			Service service) {
		this.itemName = itemName;
		this.weight = weight;
		this.amount = amount;
		this.service = service;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
}
