package id.co.nexsoft.handsonday16.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.handsonday16.laundry.model.Employee;
import id.co.nexsoft.handsonday16.laundry.repository.EmployeeRepository;

@RestController 
public class EmployeeController {
	@Autowired
	private EmployeeRepository employeeRepo;
	
    @GetMapping("/employees")
    public List<Employee> getAllEmployee() {
    	return employeeRepo.findAll();
    }
    
    @GetMapping("/employees/{id}")
    public Employee getEmployeeByID(@PathVariable(value = "id") int id) {
    	return employeeRepo.findById(id);
    }
    
    //Post Employee untuk address dalam bentuk int masih belum bisa,
    //Harus manual lewat database dalam bentuk int
    @PostMapping(value = "/employees", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee addEmployee(@RequestBody Employee employee) {
        return employeeRepo.save(employee);
    }
}
