package id.co.nexsoft.handsonday16.laundry.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Orders {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "orderItemsId", referencedColumnName = "id")
	private OrderItems orderitem;
	private Date orderDate;
	private String status;
	private String paymentStatus;
	private Date lastUpdateDate;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "employeeId", referencedColumnName = "id")
	private Employee updateBy;
	
	public Orders() {
	}
	
	public Orders(OrderItems orderitem, Date orderDate, String status,
			String paymentStatus, Date lastUpdateDate, Employee updateBy) {
		this.orderitem = orderitem;
		this.orderDate = orderDate;
		this.status = status;
		this.paymentStatus = paymentStatus;
		this.lastUpdateDate = lastUpdateDate;
		this.updateBy = updateBy;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public OrderItems getOrderitem() {
		return orderitem;
	}
	public void setOrderitem(OrderItems orderitem) {
		this.orderitem = orderitem;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public Employee getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(Employee updateBy) {
		this.updateBy = updateBy;
	}
}
