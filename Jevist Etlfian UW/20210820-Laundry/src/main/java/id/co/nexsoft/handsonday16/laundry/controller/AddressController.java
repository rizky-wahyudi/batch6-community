package id.co.nexsoft.handsonday16.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.handsonday16.laundry.model.Address;
import id.co.nexsoft.handsonday16.laundry.repository.AddressRepository;

@RestController 
public class AddressController {
	@Autowired
	private AddressRepository addressRepo;
	
    @GetMapping("/addresses")
    public List<Address> getAllAddress() {
    	return addressRepo.findAll();
    }
    
    @GetMapping("/addresses/{id}")
    public Address getAddressByID(@PathVariable(value = "id") int id) {
    	return addressRepo.findById(id);
    }
    
    @PostMapping(value = "/addresses", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Address> addAddress(@RequestBody List<Address> address) {
        return (List<Address>) addressRepo.saveAll(address);
    }

}
