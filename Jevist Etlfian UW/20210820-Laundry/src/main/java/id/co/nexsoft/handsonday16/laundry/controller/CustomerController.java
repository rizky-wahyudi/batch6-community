package id.co.nexsoft.handsonday16.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.handsonday16.laundry.model.Customer;
import id.co.nexsoft.handsonday16.laundry.repository.CustomerRepository;

@RestController 
public class CustomerController {
	@Autowired
	private CustomerRepository customerRepo;

	
    @GetMapping("/customers")
    public List<Customer> getAllCustomer() {
    	return customerRepo.findAll();
    }
    
    @GetMapping("/customers/{id}")
    public Customer getCustomerByID(@PathVariable(value = "id") int id) {
    	return customerRepo.findById(id);
    }
    
    //Post Customer untuk address dalam bentuk int masih belum bisa,
    //Harus manual lewat database dalam bentuk int
    @PostMapping(value = "/customers", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerRepo.save(customer);
    }
}
