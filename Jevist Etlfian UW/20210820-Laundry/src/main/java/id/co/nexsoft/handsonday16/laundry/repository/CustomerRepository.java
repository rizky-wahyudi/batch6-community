package id.co.nexsoft.handsonday16.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.handsonday16.laundry.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer>{
	Customer findById(int id);
	List<Customer> findAll();
	void deleteById(int id);
}
