package id.co.nexsoft.handsonday16.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.handsonday16.laundry.model.OrderItems;
import id.co.nexsoft.handsonday16.laundry.repository.OrderItemsRepository;

@RestController 
public class OrderItemsController {
	@Autowired
	private OrderItemsRepository orderItemsRepo;
	
    @GetMapping("/orderitems")
    public List<OrderItems> getAllOrderItems() {
    	return orderItemsRepo.findAll();
    }
    
    @GetMapping("/orderitems/{id}")
    public OrderItems getOrderItemsByID(@PathVariable(value = "id") int id) {
    	return orderItemsRepo.findById(id);
    }
    
    //Service Id harus manual input dari database, postmapping lewat json masih gagal
    @PostMapping(value = "/orderitems", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public OrderItems addOrderItems(@RequestBody OrderItems orderItem) {
        return orderItemsRepo.save(orderItem);
    }
}
