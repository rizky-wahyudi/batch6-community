package model;

public class Data {
	private double resistance = 0;
	private double current = 0;
	private double voltage = 0;
	private double power = 0;
	
	public double getResistance() {
		return resistance;
	}
	public void setResistance(double resistance) {
		this.resistance = resistance;
	}
	public double getCurrent() {
		return current;
	}
	public void setCurrent (double current) {
		this.current = current;
	}
	public double getVoltage() {
		return voltage;
	}
	public void setVoltage(double voltage) {
		this.voltage = voltage;
	}
	public double getPower() {
		return power;
	}
	public void setPower(double power) {
		this.power = power;
	}
	
	

}
