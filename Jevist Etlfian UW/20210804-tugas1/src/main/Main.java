package main;

import model.Data;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Data data = new Data();
		
		data.setResistance(1000);
		data.setCurrent(4);
		data.setVoltage(24);
		data.setPower(600);
		
		double ohm = data.getResistance();
		double ampere = data.getCurrent();
		double volt = data.getVoltage();
		double watt = data.getPower();
		

		// get ohm if ohm zero
		if (ohm == 0 && ampere > 0 && volt > 0 && watt > 0) {
			ohm = volt / ampere;
			watt = volt * ampere;
		} else if (ohm == 0 && ampere > 0 && volt > 0 && watt == 0) {
			ohm = volt / ampere;
			watt = volt * ampere;
		} else if (ohm == 0 && ampere > 0 && volt == 0 && watt > 0) {
			ohm = watt / Math.pow(ampere, 2);
			volt = watt / ampere;
		} else if (ohm == 0 && ampere == 0 && volt > 0 && watt > 0) {
			ohm = Math.pow(volt,2) / watt;
			ampere = watt / volt;
		// get ampere if ampere zero
		} else if (ohm > 0 && ampere == 0 && volt > 0 && watt > 0) {
			ampere = volt / ohm;
			watt = Math.pow(volt,2) / ohm;
		} else if (ohm > 0 && ampere == 0 && volt > 0 && watt == 0) {
			ampere = volt / ohm;
			watt = Math.pow(volt,2) / ohm;
		} else if (ohm > 0 && ampere == 0 && volt == 0 && watt > 0) {
			volt = Math.sqrt(watt*ohm);
			ampere = Math.sqrt(watt/ohm);
		// get volt if volt zero
		} else if (ohm > 0 && ampere > 0 && volt == 0 && watt > 0) {
			volt = ampere * ohm;
			watt = Math.pow(ampere, 2) * ohm;
		} else if (ohm > 0 && ampere > 0 && volt == 0 && watt == 0) {
			volt = ampere * ohm;
			watt = Math.pow(ampere, 2) * ohm;
		// get watt if watt zero
		} else if (ohm > 0 && ampere > 0 && volt > 0 && watt == 0) {
			ampere = volt / ohm;
			watt = Math.pow(volt,2) / ohm;
		// if all input
		} else if (ohm > 0 && ampere > 0 && volt > 0 && watt > 0){
			ampere = volt / ohm;
			watt = Math.pow(volt,2) / ohm;
		} else {
			System.out.println("Hasil error");
		}
		
		System.out.println("Resistance: " + ohm);
		System.out.println("Ampere:" + ampere);
		System.out.println("Voltage: " + volt);
		System.out.println("Power: " + watt);
		
	}

}
