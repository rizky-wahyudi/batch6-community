public class Tokyo extends TujuanWisata {  
	private int totalDanaLiburan;
	private int biayaKendaraan = 300_000;
	private int biayaNginap = 1_000_000;
	private int biayaBelanja = 1_500_000;
	private String[] tempatWisata = {"Tokyo Sky Tree","Meiji Jingu","Shinjuku Gyoen National Garden"};
	
	public int getTotalDanaLiburan(int jumlahOrang, int jumlahHari) {
		this.totalDanaLiburan = biayaKendaraan + (biayaNginap + biayaBelanja) * jumlahHari;
		this.totalDanaLiburan = this.totalDanaLiburan * jumlahOrang;
		return this.totalDanaLiburan;
	}

	public void showTempatWisata() {
		for (int i=0; i < tempatWisata.length; i++) {
			System.out.println("Tempat Wisata " + (i+1) + " = " + tempatWisata[i]);
		}
	}
} 