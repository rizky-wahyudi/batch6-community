public class Bandung extends TujuanWisata {  
	private int totalDanaLiburan;
	private int biayaKendaraan = 800_000;
	private int biayaNginap = 700_000;
	private int biayaBelanja = 500_000;
	private String[] tempatWisata = {"Tangkuban Perahu","Jalan Braga","Gedung Sate"};
	
	public int getTotalDanaLiburan(int jumlahOrang, int jumlahHari) {
		this.totalDanaLiburan = biayaKendaraan + (biayaNginap + biayaBelanja) * jumlahHari;
		this.totalDanaLiburan = this.totalDanaLiburan * jumlahOrang;
		return this.totalDanaLiburan;
	}

	public void showTempatWisata() {
		for (int i=0; i < tempatWisata.length; i++) {
			System.out.println("Tempat Wisata " + (i+1) + " = " + tempatWisata[i]);
		}
	}
} 