public class TujuanWisata {  
	private String kota;
	private int jumlahOrang;
	private int jumlahHari;

	//Setter
	public void setKota(String kota) {
		this.kota = kota;
	}

	public void setJumlahOrang(int jumlahOrang) {
		this.jumlahOrang = jumlahOrang;
	}	

	public void setJumlahHari(int jumlahHari) {
		this.jumlahHari = jumlahHari;
	}

	//Getter
	public String getKota() {
		return this.kota;
	}

	public int getJumlahOrang() {
		return this.jumlahOrang;
	}

	public int getJumlahHari() {
		return this.jumlahHari;
	}
} 