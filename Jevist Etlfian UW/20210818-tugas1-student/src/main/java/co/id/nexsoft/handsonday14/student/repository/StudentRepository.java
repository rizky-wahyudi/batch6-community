package co.id.nexsoft.handsonday14.student.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.id.nexsoft.handsonday14.student.model.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {
    Student findById(int id);
    List<Student> findAll();
    void deleteById(int id);
}