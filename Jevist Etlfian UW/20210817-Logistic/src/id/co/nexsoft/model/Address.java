package id.co.nexsoft.model;

public class Address {
	private int id;
	private String street;
	private String district;
	private String province;
	private String country;
	private String longitude;
	private String latitude;
	
	public Address() {
	}
	
	public Address(int id, String street, String province, String district,
			String country, String longitude, String latitude) {
		this.id = id;
		this.street = street;
		this.district = district;
		this.province = province;
		this.country = country;
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
}
