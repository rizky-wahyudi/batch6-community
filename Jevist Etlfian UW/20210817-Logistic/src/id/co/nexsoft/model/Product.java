package id.co.nexsoft.model;

public class Product {
	private int id;
	private String name;
	private Address fromAddress;
	private Address deliverAddress;
	private double size;
	private double width;
	private double height;
	private double depth;
	private double weight;
	
	public Product() {
	}
	
	public Product(int id, String name, Address fromAddress, Address deliverAddress,
			double width, double height, double depth, double weight) {
		this.id = id;
		this.name = name;
		this.fromAddress = fromAddress;
		this.deliverAddress = deliverAddress;
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.size = this.width * this.height * this.depth;
		this.weight = weight;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(Address fromAddress) {
		this.fromAddress = fromAddress;
	}

	public Address getDeliverAddress() {
		return deliverAddress;
	}
	public void setDeliverAddress(Address deliverAddress) {
		this.deliverAddress = deliverAddress;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double width, double height, double weight) {
		this.size = this.width * this.height * this.depth;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getDepth() {
		return depth;
	}
	public void setDepth(double depth) {
		this.depth = depth;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}	
}
