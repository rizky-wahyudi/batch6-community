package id.co.nexsoft.model;

public class Transport {
	private int id;
	private String driver;
	private double size;
	private double speed;
	private String type;
	private double load;
	
	public Transport() {
	}
	
	public Transport(int id, String driver, double size, double speed,
			String type, double load) {
		this.id = id;
		this.driver = driver;
		this.size = size;
		this.speed = speed;
		this.type = type;
		this.load = load;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getLoad() {
		return load;
	}
	public void setLoad(double load) {
		this.load = load;
	}
}
