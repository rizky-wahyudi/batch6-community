package id.co.nexsoft.model;

public class DistanceMapper {
	private int id;
	private Warehouse fromWarehouseId;
	private Warehouse toWarehouseId;
	private double distance;
	
	public DistanceMapper() {
	}
	
	public DistanceMapper(int id, Warehouse fromWarehouseId, Warehouse toWarehouseId,
			double distance) {
		this.id = id;
		this.fromWarehouseId = fromWarehouseId;
		this.toWarehouseId = toWarehouseId;
		this.distance = distance;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Warehouse getFromWarehouseId() {
		return fromWarehouseId;
	}
	public void setFromWarehouseId(Warehouse fromWarehouseId) {
		this.fromWarehouseId = fromWarehouseId;
	}
	public Warehouse getToWarehouseId() {
		return toWarehouseId;
	}
	public void setToWarehouseId(Warehouse toWarehouseId) {
		this.toWarehouseId = toWarehouseId;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
}
