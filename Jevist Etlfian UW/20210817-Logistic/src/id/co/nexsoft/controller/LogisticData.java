package id.co.nexsoft.controller;

import java.util.ArrayList;
import java.util.List;

import id.co.nexsoft.model.Address;
import id.co.nexsoft.model.DistanceMapper;
import id.co.nexsoft.model.Product;
import id.co.nexsoft.model.Transport;
import id.co.nexsoft.model.Warehouse;

public class LogisticData {
	private List<Address> address = new ArrayList<>();
	private List<Warehouse> warehouse = new ArrayList<>();
	private List<DistanceMapper> distanceMapper = new ArrayList<>();
	private List<Transport> transport = new ArrayList<>();
	private List<Product> product = new ArrayList<>();
	
	public List<Address> getAddressData() {
		address.add(new Address(1,"Jalan Tangerang","Provinsi Tangerang",
				"Daerah Tangerang","Indonesia","100,1234324","8,543434"));
		address.add(new Address(2,"Jalan Jakarta","Provinsi Jakarta",
				"Daerah Jakarta","Indonesia","12,454525","7,55432523"));
		address.add(new Address(3,"Jalan Bandung","Provinsi Bandung",
				"Daerah Bandung","Indonesia","67,765487","34,35324525"));
		return address;
	}
	
	public List<Warehouse> getWarehouseData() {
		warehouse.add(new Warehouse(1, address.get(0)));
		warehouse.add(new Warehouse(2, address.get(1)));
		warehouse.add(new Warehouse(3, address.get(2)));
		return warehouse;
	}
	
	public List<DistanceMapper> getDistanceMapper() {
		distanceMapper.add(new DistanceMapper(1,warehouse.get(0),warehouse.get(1),5));
		distanceMapper.add(new DistanceMapper(2,warehouse.get(0),warehouse.get(2),10));
		distanceMapper.add(new DistanceMapper(3,warehouse.get(1),warehouse.get(0),5));
		distanceMapper.add(new DistanceMapper(4,warehouse.get(1),warehouse.get(2),15));
		distanceMapper.add(new DistanceMapper(5,warehouse.get(2),warehouse.get(0),10));
		distanceMapper.add(new DistanceMapper(6,warehouse.get(2),warehouse.get(1),15));
		return distanceMapper;
	}
	
	public List<Transport> getTransportData() {
		transport.add(new Transport(1,"Driver 1",31_878_000,60,"Truk",10000));
		transport.add(new Transport(2,"Driver 2",1_000_000,70,"Mobil",2000));
		transport.add(new Transport(3,"Driver 3",160_000,80,"Motor",50));
		return transport;
	}
	
	public void setProductData(int id, String name, int indexFromAddress, 
			int indexDeliverAddress, double width, double height, 
			double depth, double weight) {
		product.add(new Product(id, name, address.get(indexFromAddress), 
				address.get(indexDeliverAddress),
				width, height, depth, weight));
	}
	
	public List<Product> getProductData() {
		return product;
	}
}
