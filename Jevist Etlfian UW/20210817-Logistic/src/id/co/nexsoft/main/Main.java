package id.co.nexsoft.main;

//import java.util.Scanner;

import id.co.nexsoft.controller.LogisticData;

public class Main {

	public static void main(String[] args) {
		LogisticData logisticSystem = new LogisticData();
		logisticSystem.getAddressData();
		logisticSystem.getWarehouseData();
		logisticSystem.getDistanceMapper();
		logisticSystem.getDistanceMapper();
		
		// input pakai scanner
//		Scanner input = new Scanner(System.in);
//		System.out.println(">>>>Input data barang yang mau dikirim<<<<");
//		System.out.print("Masukin id produk = ");
//		int idProduk = input.nextInt();
//		System.out.print("Masukin nama produk = ");
//		String namaProduk = input.next();
//		System.out.println("==================================");
//		System.out.println("0=Tangerang , 1=Jakarta, 2=Bandung");
//		System.out.println("==================================");
//		System.out.println("Input id alamat asal = ");
//		int idAlamatAsal = input.nextInt();
//		System.out.println("Input id alamat tujuan = ");
//		int idAlamatTujuan = input.nextInt();
//		System.out.println("Masukin lebar produk (cm) = ");
//		int lebarProduk = input.nextInt();
//		System.out.println("Masukin tinggi produk (cm) = ");
//		int tinggiProduk = input.nextInt();
//		System.out.println("Masukin panjang produk (cm) = ");
//		int panjangProduk = input.nextInt();
//		System.out.println("Masukin berat produk (kg) = ");
//		int beratProduk = input.nextInt();
		
		// input manual produk 1
		int idProduk = 1;
		String namaProduk = "Produk 1";
		int idAlamatAsal = 2;
		int idAlamatTujuan = 1;
		double lebarProduk = 20;
		double tinggiProduk = 20;
		double panjangProduk = 20;
		double beratProduk = 40;
		logisticSystem.setProductData(idProduk, namaProduk, idAlamatAsal, idAlamatTujuan, 
				lebarProduk, tinggiProduk, panjangProduk, beratProduk);
		
		// input manual produk 2
		idProduk = 2;
		namaProduk = "Produk 2";
		idAlamatAsal = 0;
		idAlamatTujuan = 2;
		lebarProduk = 100;
		tinggiProduk = 60;
		panjangProduk = 40;
		beratProduk = 40;
		logisticSystem.setProductData(idProduk, namaProduk, idAlamatAsal, idAlamatTujuan, 
				lebarProduk, tinggiProduk, panjangProduk, beratProduk);
		
		// Pemilhan produk yang telah diinput manual
		int indexObjekProduct = 0;
		
		// Pemilihan DistanceMapper
		int indexObjekDistanceMapper = 0;
		if (logisticSystem.getProductData().get(indexObjekProduct).getFromAddress().getId() == 1 &&
				logisticSystem.getProductData().get(indexObjekProduct).getDeliverAddress().getId() == 2) {
			indexObjekDistanceMapper = 0;
		} else if(logisticSystem.getProductData().get(indexObjekProduct).getFromAddress().getId() == 1 &&
				logisticSystem.getProductData().get(indexObjekProduct).getDeliverAddress().getId() == 3) {
			indexObjekDistanceMapper = 1;
		} else if(logisticSystem.getProductData().get(indexObjekProduct).getFromAddress().getId() == 2 &&
				logisticSystem.getProductData().get(indexObjekProduct).getDeliverAddress().getId() == 1) {
			indexObjekDistanceMapper = 2;
		} else if(logisticSystem.getProductData().get(indexObjekProduct).getFromAddress().getId() == 2 &&
				logisticSystem.getProductData().get(indexObjekProduct).getDeliverAddress().getId() == 3) {
			indexObjekDistanceMapper = 3;
		} else if(logisticSystem.getProductData().get(indexObjekProduct).getFromAddress().getId() == 3 &&
				logisticSystem.getProductData().get(indexObjekProduct).getDeliverAddress().getId() == 1) {
			indexObjekDistanceMapper = 4;
		} else if(logisticSystem.getProductData().get(indexObjekProduct).getFromAddress().getId() == 3 &&
				logisticSystem.getProductData().get(indexObjekProduct).getDeliverAddress().getId() == 2) {
			indexObjekDistanceMapper = 5;
		} else {
			System.out.println("Alamat asal dan alamat tujuan salah");
		}
		
		// Pemilihan Transport
		int indexObjekTransport = 0;
		if (logisticSystem.getProductData().get(indexObjekProduct).getSize() <= 160000 && 
				logisticSystem.getProductData().get(indexObjekProduct).getWeight() <= 50) {
			indexObjekTransport = 2;
		} else if (logisticSystem.getProductData().get(indexObjekProduct).getSize() > 160_000 && 
				logisticSystem.getProductData().get(indexObjekProduct).getSize() <= 1_000_000 || 
				logisticSystem.getProductData().get(indexObjekProduct).getWeight() > 50 && 
				logisticSystem.getProductData().get(indexObjekProduct).getWeight() <= 2000) {
			indexObjekTransport = 1;
		} else if (logisticSystem.getProductData().get(indexObjekProduct).getSize() > 1_000_000 && 
				logisticSystem.getProductData().get(indexObjekProduct).getSize() <= 31_878_000 || 
				logisticSystem.getProductData().get(indexObjekProduct).getWeight() > 2000 && 
				logisticSystem.getProductData().get(indexObjekProduct).getWeight() <= 10000) {
			indexObjekTransport = 0;
		} else {
			System.out.println("Berat dan Size Produk melebihi batas");
		}
		
		// Hasil Result
		System.out.println("==============================================");
		System.out.println(">>>>Data Produk<<<<");
		System.out.println("Nama Produk : " + logisticSystem.getProductData().get(indexObjekProduct).getName());
		System.out.println("Alamat Asal : " + logisticSystem.getProductData().get(indexObjekProduct).getFromAddress().getStreet());	
		System.out.println("Alamat Tujuan : " + logisticSystem.getProductData().get(indexObjekProduct).getDeliverAddress().getStreet());	
		System.out.println("Size Produk (cm3) : " + logisticSystem.getProductData().get(indexObjekProduct).getSize());	
		System.out.println("Berak Produk (kg) : " + logisticSystem.getProductData().get(indexObjekProduct).getWeight());	

		System.out.println("==============================================");
		System.out.println(">>>>Data Gudang dan Jarak Pengiriman<<<<");
		System.out.println("Id Gudang Asal : " + logisticSystem.getDistanceMapper().get(indexObjekDistanceMapper).getFromWarehouseId().getId());
		System.out.println("Id Gudang Tujuan : " + logisticSystem.getDistanceMapper().get(indexObjekDistanceMapper).getToWarehouseId().getId());
		System.out.println("Jarak (km) : " + logisticSystem.getDistanceMapper().get(indexObjekDistanceMapper).getDistance());
		
		System.out.println("==============================================");
		System.out.println(">>>>Data Tranportasi yang digunakan<<<<");
		System.out.println("Supir : " + logisticSystem.getTransportData().get(indexObjekTransport).getDriver());
		System.out.println("Batas Ukuran (cm3): " + logisticSystem.getTransportData().get(indexObjekTransport).getSize());
		System.out.println("Kecepatan (km/jam) : " + logisticSystem.getTransportData().get(indexObjekTransport).getSpeed());
		System.out.println("Tipe : " + logisticSystem.getTransportData().get(indexObjekTransport).getType());
		System.out.println("Batas berat (kg): " + logisticSystem.getTransportData().get(indexObjekTransport).getLoad());
	}
}
