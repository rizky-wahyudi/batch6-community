package com.product;

import java.util.ArrayList;

public class OrderItems extends Order{
	private ArrayList <Product> product = new ArrayList<>(); // Belum tau Cara masukin data produk ke ArrayList dari Objek Produk
	private String orderID;
	private int amount;
	int ID = 0;
	
	public void setOrderItems(String orderID, int amount) {
		this.orderID = orderID +" OrderID"+ID++;
		this.amount = amount;
	}
	
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getOrderID() {
		return this.orderID;
	}
	public int getAmount() {
		return this.amount;
	}
}
