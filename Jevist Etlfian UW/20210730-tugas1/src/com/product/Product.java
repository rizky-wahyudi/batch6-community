package com.product;

public class Product extends OrderItems {
	private String productName ;
	private String productArticleNumber;
	private String productDescription;
	private double productPurchasePrice;
	private double productSellPrice;
	private String productImage;
	private String productExpiredDate;
	private double productTax;
	private String productCategory;
	private double productPriceAfterTax;
	
	public void showProduct() {
		System.out.println("Nama Produk : " + this.productName);
		System.out.println("Nomor Artikel Produk : " + this.productArticleNumber);
		System.out.println("Deskripsi Produk : " + this.productDescription);
		System.out.println("Harga Produk sebelum tax : " + this.productSellPrice);
		System.out.println("Harga Produk setelah tax : " + this.productPriceAfterTax);
		System.out.println("Gambar Produk : " + this.productImage);
		System.out.println("Expired Date Produk : " + this.productExpiredDate);
		System.out.println("Kategori Produk : " + this.productCategory);
		System.out.println("==========================================================================");
	}
	
	public void setProduct (String productName, String productArticleNumber, String productDescription, 
			double productPurchasePrice, String productImage, String productExpiredDate, String productCategory) {
		this.productName = productName;
		this.productArticleNumber = productArticleNumber;
		this.productDescription = productDescription;
		this.productPurchasePrice = productPurchasePrice;
		this.productImage = productImage;
		this.productExpiredDate = productExpiredDate;
		this.productCategory = productCategory;
		this.productSellPrice = this.productPurchasePrice + (0.1 * this.productPurchasePrice);
		if (this.productCategory.equalsIgnoreCase("Makanan")) {
			this.productTax = 0.05;
			this.productPriceAfterTax = this.productSellPrice + (this.productTax * this.productPurchasePrice);
		} else if (this.productCategory.equalsIgnoreCase("Produk Sabun")) {
			this.productTax = 0.07;
			this.productPriceAfterTax = this.productSellPrice + (this.productTax * this.productPurchasePrice);
		} else if (this.productCategory.equalsIgnoreCase("Minuman")) {
			this.productTax = 0.1;
			this.productPriceAfterTax = this.productSellPrice + (this.productTax * this.productPurchasePrice);
		}
		
	}
	
	public String getProductName() {
		return this.productName;
	}
	
	public String getProductArticleNumber() {
		return this.productArticleNumber;
	}
	
	public String getProductDescription() {
		return this.productDescription;
	}
	
	public double getProductPurchasePrice() {
		return this.productPurchasePrice;
	}
	
	public double getProductSellPrice() {
		this.productSellPrice = this.productPurchasePrice + (0.1 * this.productPurchasePrice);
		return this.productSellPrice;
	}
	
	public String getProductImage() {
		return this.productImage;
	}
	
	public String getProductExpiredDate() {
		return this.productExpiredDate;
	}
	
	public double getProductTax() {
		if (this.productCategory.equalsIgnoreCase("Makanan")) {
			this.productTax = 0.05;
		} else if (this.productCategory.equalsIgnoreCase("Produk Sabun")) {
			this.productTax = 0.07;
		} else if (this.productCategory.equalsIgnoreCase("Minuman")) {
			this.productTax = 0.1;
		} 
		return this.productTax;
	}
	
	public double getProductPriceAfterTax() {
		this.productPriceAfterTax = this.productSellPrice + (this.productTax * this.productPurchasePrice); 
		return this.productPriceAfterTax;
	}
	
	public String getProductCategory() {
		return this.productCategory;
	}
	
	
}
