package com.product;

import java.util.ArrayList;
import java.util.UUID;

public class Order {
	private ArrayList <OrderItems> totalOrderItems = new ArrayList<>(); // Belum tau Cara masukin data OrderItems ke ArrayList dari Objek Produk
	private String orderDate;
	private String cashier;
	private String orderNumber;
	private String branchID;
	private double totalPrice;
	private double tax;
	private boolean status;
	private String statusCondition;
	private boolean paymentStatus;
	private String paymentStatusCondition;
	
	public void setOrder (String orderDate, String cashier, String branchID) {
		this.orderDate = orderDate;
		this.cashier = cashier;
		this.branchID = branchID;
		this.orderNumber = UUID.randomUUID().toString();
		this.status = false;
		this.paymentStatus = false;		
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public void setPaymentStatus(boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
	public String getStatusCondition() {
		if (this.status) {
			this.statusCondition = "Delivered";
		} else {
			this.statusCondition = "Ordered";
		}
		return this.statusCondition;
	}
	
	public String getPaymentStatusCondition() {
		if (this.paymentStatus){
			this.paymentStatusCondition = "Paid";
		} else {
			this.paymentStatusCondition = "Unpaid";
		}
		return this.paymentStatusCondition;
	}

	public String getOrderDate() {
		return this.orderDate;
	}
	public String getCashier() {
		return this.cashier;
	}
	public String getOrderNumber() {
		return this.orderNumber;
	}
	public String getBranchID() {
		return this.branchID;
	}
	public double getTotalPrice() {
		return this.totalPrice;
	}
	public double getTax() {
		return this.tax;
	}
	public boolean isStatus() {
		return this.status;
	}
	public boolean isPaymentStatus() {
		return paymentStatus;
	}

	
}
