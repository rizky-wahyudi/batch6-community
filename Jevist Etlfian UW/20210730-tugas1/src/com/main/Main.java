package com.main;

import java.util.Scanner;

import com.product.Product;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		Product p1 = new Product();
		Product p2 = new Product();
		Product p3 = new Product();
		p1.setProduct("Makanan Tipe 1", "MKNTIPE1", "Makanan Lezat", 4000, "Image Makanan Tipe 1", "2022-04-05", "Makanan");
		p2.setProduct("Sabun Tipe 1", "SBNTIPE1", "Sabun Lembut", 7000, "Image Sabun Tipe 1", "2025-08-20", "Produk Sabun");
		p3.setProduct("Minuman Tipe 1", "MNMTIPE1", "Minuman Segar", 3000, "Image Minuman Tipe 1", "2023-03-12", "Minuman");
		
		
		String jawaban = "";
		boolean orderProduk = true;
		while (orderProduk) {
			System.out.print("Apakah mau order produk ya/tidak : ");
			jawaban = input.nextLine();
			if (jawaban.equalsIgnoreCase("ya")) {
				orderProduk = false;
				break;
			} else if (jawaban.equalsIgnoreCase("tidak") ) {
				System.exit(0);
			} else {
				orderProduk = true;
				continue;
			}
		}
		
		System.out.println("==============================Daftar Produk==============================");
		System.out.println("Tombol : 1");
		p1.showProduct();
		System.out.println("Tombol : 2");
		p2.showProduct();
		System.out.println("Tombol : 3");
		p3.showProduct();
		
		int jawabanAngka = 0;
		int banyakProduk = 0;
		boolean statusPilihan = true;
		while (statusPilihan) {
			System.out.print("Silahkan masukin angka tombol produk yang ingin dibeli: ");
			jawabanAngka = input.nextInt();
			if (jawabanAngka == 1) {
				System.out.print("Mau beli berapa banyak: ");
				banyakProduk = input.nextInt();
				p1.setOrderItems(p1.getProductName(), banyakProduk);
			} else if (jawabanAngka == 2) {
				System.out.print("Mau beli berapa banyak: ");
				banyakProduk = input.nextInt();
				p2.setOrderItems(p2.getProductName(), banyakProduk);
			} else if (jawabanAngka == 3) {
				System.out.print("Mau beli berapa banyak: ");
				banyakProduk = input.nextInt();
				p3.setOrderItems(p3.getProductName(), banyakProduk);
			} else {
				System.out.println("Tombol Masukin Salah silahkan masukin tombol yang benar");
			}
			
			orderProduk = true;
			while (orderProduk) {
				System.out.print("Apakah mau order produk lagi (ya/tidak) : ");
				jawaban = input.nextLine();
				if (jawaban.equalsIgnoreCase("ya")) {
					statusPilihan = true;
					orderProduk = false;
					break;
				} else if (jawaban.equalsIgnoreCase("tidak") ) {
					statusPilihan = false;
					orderProduk = false;
					break;
				} else {
					orderProduk = true;
					continue;
				}
			}
		}
		

	}

}
