CREATE TABLE handsonday11_tugas2.dokumen (
	id INT NOT NULL AUTO_INCREMENT,
	ktp VARCHAR(25) NOT NULL,
	passport VARCHAR(25) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas2.bagasi (
	id INT NOT NULL AUTO_INCREMENT,
	berat DECIMAL(4,2) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas2.transportasi (
	id INT NOT NULL AUTO_INCREMENT,
	jenis VARCHAR(20) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas2.pelanggan (
	id INT NOT NULL AUTO_INCREMENT,
	nama VARCHAR(50) NOT NULL,
	dokumenId INT NOT NULL,
	bagasiId INT NOT NULL,
	FOREIGN KEY (dokumenId) REFERENCES dokumen(id),
	FOREIGN KEY (bagasiId) REFERENCES bagasi(id),
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas2.tiket (
	id INT NOT NULL AUTO_INCREMENT,
	kodeTiket VARCHAR(50) NOT NULL,
	keberangkatan VARCHAR(20) NOT NULL,
	tujuan VARCHAR(20) NOT NULL,
	transportasiId INT NOT NULL,
	FOREIGN KEY (transportasiId) REFERENCES transportasi(id),
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas2.perjalanan (
	id INT NOT NULL AUTO_INCREMENT,
	tiketId INT NOT NULL,
	pelangganId INT NOT NULL,
	FOREIGN KEY (tiketId) REFERENCES tiket(id),
	FOREIGN KEY (pelangganId) REFERENCES pelanggan(id),
	PRIMARY KEY (id));