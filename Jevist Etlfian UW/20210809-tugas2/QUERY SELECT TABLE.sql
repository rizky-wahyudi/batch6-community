SELECT
	pl.nama,
	d.ktp,
	d.passport,
	tk.kodeTiket,
	tk.keberangkatan,
	tk.tujuan,
	tp.jenis
FROM	
	perjalanan pj
	LEFT JOIN 	tiket 		tk 	ON pj.tiketId = tk.id
	LEFT JOIN 	pelanggan 	pl 	ON pj.pelangganId = pl.id
	LEFT JOIN 	dokumen 	d 	ON pl.dokumenId = d.id
	LEFT JOIN 	bagasi 		b 	ON pl.bagasiId = b.id
	LEFT JOIN 	transportasi 	tp 	ON tk.transportasiId = tp.id
WHERE 
	tk.kodeTiket = "A123456789";
	
