public class Airplane {  
	private String jenisPesawat;
	private String poinKeberangkatan;
	private String poinDestinasi;
	private int hargaTotal;
	private int beratKoper;
	private int jumlahKursi;

	//Setter
	public void setJenisPesawat(String jenisPesawat) {
		this.jenisPesawat = jenisPesawat;
	}

	public void setPoinKeberangkatan(String poinKeberangkatan) {
		this.poinKeberangkatan = poinKeberangkatan;
	}

	public void setPoinDestinasi(String poinDestinasi) {
		this.poinDestinasi = poinDestinasi;
	}

	public void setHargaTotal(int hargaTotal) {
		this.hargaTotal = hargaTotal;
	}

	public void setBeratKoper(int beratKoper) {
		this.beratKoper = beratKoper;
	}

	public void setJumlahKursi(int jumlahKursi) {
		this.jumlahKursi = jumlahKursi;
	}	


	//Getter
	public String getJenisPesawat() {
		return this.jenisPesawat;
	}

	public String getPoinKeberangkatan() {
		return this.poinKeberangkatan;
	}

	public String getPoinDestinasi() {
		return this.poinDestinasi;
	}

	public int getHargaTotal() {
		return this.hargaTotal;
	}

	public int getBeratKoper() {
		return this.beratKoper;
	}

	public int getJumlahKursi() {
		return this.jumlahKursi;
	}
} 