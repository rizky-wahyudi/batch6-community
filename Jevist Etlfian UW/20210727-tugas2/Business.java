public class Business extends Airplane {  
	private int totalKursiDariPesawat;
	private int hargaDariPesawat;
	private int totalBeratDariPesawat;
	
	public int getTotalKursiDariPesawat(int totalKursiDariPesawat) {
		this.totalKursiDariPesawat = totalKursiDariPesawat * 25 / 100;
		return this.totalKursiDariPesawat;
	}

	public int getHargaDariPesawat(int hargaDariPesawat) {
		this.hargaDariPesawat = hargaDariPesawat * 225 / 100;
		return this.hargaDariPesawat;
	}

	public int getTotalBeratDariPesawat(int totalBeratDariPesawat) {
		this.totalBeratDariPesawat = totalBeratDariPesawat * 200 / 100;
		return this.totalBeratDariPesawat;
	}
}