public class FirstClass extends Airplane {  
	private int totalKursiDariPesawat;
	private int hargaDariPesawat;
	private int totalBeratDariPesawat;
	
	public int getTotalKursiDariPesawat(int totalKursiDariPesawat) {
		this.totalKursiDariPesawat = totalKursiDariPesawat * 10 / 100;
		return this.totalKursiDariPesawat;
	}

	public int getHargaDariPesawat(int hargaDariPesawat) {
		this.hargaDariPesawat = hargaDariPesawat * 300 / 100;
		return this.hargaDariPesawat;
	}

	public int getTotalBeratDariPesawat(int totalBeratDariPesawat) {
		this.totalBeratDariPesawat = totalBeratDariPesawat * 250 / 100;
		return this.totalBeratDariPesawat;
	}
}