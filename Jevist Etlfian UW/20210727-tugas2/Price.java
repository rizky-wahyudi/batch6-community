public class Price { 
	private int harga;

	public void setHarga(String poinBerangkat, String poinDestinasi) {
		if (poinBerangkat == "europa" &&  poinDestinasi == "asia") {
			this.harga = 500;
		} else if (poinBerangkat == "asia" &&  poinDestinasi == "europa") {
			this.harga = 600;
		} else if (poinBerangkat == "europa" &&  poinDestinasi == "US") {
			this.harga = 650;
		} else if (poinBerangkat == "US" &&  poinDestinasi == "europa") {
			this.harga = 650;
		} else {
			this.harga = 0;
		}
	}
	
	public int getHarga() {
		if (this.harga != 0) {
			return this.harga;
		} else {
			System.out.println("Masukin poin berangkat dan destinasi dengan benar");
			System.out.println("europa - asia");
			System.out.println("asia - europa");
			System.out.println("europa - US");
			System.out.println("US- europa");
			return this.harga = 0;
		}
	}
}