public class AirplaneClassFactory {  
	public static Airplane newInstance(String type)  { 
		if (type.equals("Economy")) {
			return new Economy();
		} else if (type.equals("Business")) {
			return new Business();
		} else if (type.equals("FirstClass")) {
			return new FirstClass();
		}
		return null; 
	}  
} 