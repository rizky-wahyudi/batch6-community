import java.util.Scanner; 
public class Main {  
	public static void main(String args[])  {
		PrintGaris garis = new PrintGaris();
		Scanner input = new Scanner(System.in);
		Price hargaPesawat = new Price();

		String[] negara = {"europa","asia","US"};
		String[] kelasIdPesawat = {"Economy","Business","FirstClass"};
		String[] jenisPesawat = {"Airbus 380","Boeing 747","Boeing 787"};
		int[] jumlahKursiPesawat = {750,529,248};
		int beratKoper = 20;

		// Menampilkan Menu pilihan
		garis.printGarisSatu();
		System.out.println(">>>>>ID Negara<<<<<");
		System.out.println("0=europa , 1=asia , 2=US");
		garis.printGarisDua();
		System.out.println(">>>>>ID Kelas Pesawat<<<<<");
		System.out.println("0=Ekonomi , 1=Business , 2=FirstClass");
		garis.printGarisDua();
		System.out.println(">>>>>ID Jenis Pesawat<<<<<");
		System.out.println("0=Airbus 380 , 1=Boeing 747 , 2=Boeing 787");
		garis.printGarisSatu();

		// Pemilihan input dengan angka
		System.out.print("Masukin ID Negara Keberangkatan : ");
		int idNegaraBerangkat = input.nextInt();
		System.out.print("Masukin ID Negara Destinasi : ");
		int idNegaraDestinasi = input.nextInt();
		System.out.print("Masukin ID Kelas Pesawat : ");
		int idKelasPesawat = input.nextInt();
		System.out.print("Masukin ID Jenis Pesawat : ");
		int idJenisPesawat = input.nextInt();
		garis.printGarisSatu();

		//Set harga Pesawat
		hargaPesawat.setHarga(negara[idNegaraBerangkat],negara[idNegaraDestinasi]);

		Airplane kelasPesawat = AirplaneClassFactory.newInstance(kelasIdPesawat[idKelasPesawat]);
		if (kelasPesawat instanceof Economy) {
			Economy tipe = new Economy();

			//Set pesawat tipe ekonomi
			tipe.setPoinKeberangkatan(negara[idNegaraBerangkat]);
			tipe.setPoinDestinasi(negara[idNegaraDestinasi]);
			tipe.setJenisPesawat(jenisPesawat[idJenisPesawat]);
			tipe.setHargaTotal(tipe.getHargaDariPesawat(hargaPesawat.getHarga()));
			tipe.setBeratKoper(tipe.getTotalBeratDariPesawat(beratKoper));
			tipe.setJumlahKursi(tipe.getTotalKursiDariPesawat(jumlahKursiPesawat[idJenisPesawat]));

			// Menampilkan Data pemilihan
			garis.printGarisSatu();
			System.out.print("Keberangkatan - Destinasi : ");
			System.out.println(tipe.getPoinKeberangkatan() + " - " + tipe.getPoinDestinasi());
			System.out.print("Kelas Pesawat yang di pilih : ");
			System.out.println(kelasIdPesawat[idKelasPesawat]);
			System.out.print("Jenis Pesawat yang di pilih : ");
			System.out.println(tipe.getJenisPesawat());
			System.out.print("Harga Pesawat : ");
			System.out.println(tipe.getHargaTotal());
			System.out.print("Berat Koper : ");
			System.out.println(tipe.getBeratKoper());
			System.out.print("Jumlah Kursi : ");
			System.out.println(tipe.getJumlahKursi());		

		} else if (kelasPesawat instanceof Business) {
			Business tipe = new Business();

			//Set pesawat tipe bisnis
			tipe.setPoinKeberangkatan(negara[idNegaraBerangkat]);
			tipe.setPoinDestinasi(negara[idNegaraDestinasi]);
			tipe.setJenisPesawat(jenisPesawat[idJenisPesawat]);
			tipe.setHargaTotal(tipe.getHargaDariPesawat(hargaPesawat.getHarga()));
			tipe.setBeratKoper(tipe.getTotalBeratDariPesawat(beratKoper));
			tipe.setJumlahKursi(tipe.getTotalKursiDariPesawat(jumlahKursiPesawat[idJenisPesawat]));

			// Menampilkan Data pemilihan
			garis.printGarisSatu();
			System.out.print("Keberangkatan - Destinasi : ");
			System.out.println(tipe.getPoinKeberangkatan() + " - " + tipe.getPoinDestinasi());
			System.out.print("Kelas Pesawat yang di pilih : ");
			System.out.println(kelasIdPesawat[idKelasPesawat]);
			System.out.print("Jenis Pesawat yang di pilih : ");
			System.out.println(tipe.getJenisPesawat());
			System.out.print("Harga Pesawat : ");
			System.out.println(tipe.getHargaTotal());
			System.out.print("Berat Koper : ");
			System.out.println(tipe.getBeratKoper());
			System.out.print("Jumlah Kursi : ");
			System.out.println(tipe.getJumlahKursi());

		} else if (kelasPesawat instanceof FirstClass) {
			FirstClass tipe = new FirstClass();

			//Set pesawat tipe kelas pertama
			tipe.setPoinKeberangkatan(negara[idNegaraBerangkat]);
			tipe.setPoinDestinasi(negara[idNegaraDestinasi]);
			tipe.setJenisPesawat(jenisPesawat[idJenisPesawat]);
			tipe.setHargaTotal(tipe.getHargaDariPesawat(hargaPesawat.getHarga()));
			tipe.setBeratKoper(tipe.getTotalBeratDariPesawat(beratKoper));
			tipe.setJumlahKursi(tipe.getTotalKursiDariPesawat(jumlahKursiPesawat[idJenisPesawat]));

			// Menampilkan Data pemilihan
			garis.printGarisSatu();
			System.out.print("Keberangkatan - Destinasi : ");
			System.out.println(tipe.getPoinKeberangkatan() + " - " + tipe.getPoinDestinasi());
			System.out.print("Kelas Pesawat yang di pilih : ");
			System.out.println(kelasIdPesawat[idKelasPesawat]);
			System.out.print("Jenis Pesawat yang di pilih : ");
			System.out.println(tipe.getJenisPesawat());
			System.out.print("Harga Pesawat : ");
			System.out.println(tipe.getHargaTotal());
			System.out.print("Berat Koper : ");
			System.out.println(tipe.getBeratKoper());
			System.out.print("Jumlah Kursi : ");
			System.out.println(tipe.getJumlahKursi());

		} else {
			System.out.println("Salah memilih kelas pesawat");
		} 

	}  
} 