package rewindintermediate.task3.factory.main;

public class Drawer extends ShapeFactory{
    @Override
    Shape createShape(String type) {
        if (type.equalsIgnoreCase("round")) {
            return new ShapeRound("Round Shape");
        } else if (type.equalsIgnoreCase("rectangle")) {
            return new ShapeRectangle("Rectangle Shape");
        } else return null;
    }
}
