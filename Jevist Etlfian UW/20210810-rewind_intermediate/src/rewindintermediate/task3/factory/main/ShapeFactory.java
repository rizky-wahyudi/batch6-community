package rewindintermediate.task3.factory.main;

abstract class ShapeFactory {

    abstract Shape createShape(String type);

    Shape chooseShape(String type) {
        Shape shape = createShape(type);
        if (shape == null) {
            System.out.println("Drawer can't draw it \n");
            return null;
        }
        System.out.println("Making " + shape.getName());
        shape.drawing();
        shape.coloring();
        System.out.println("Created " + shape.getName() + "\n");
        return shape;
    }
}
