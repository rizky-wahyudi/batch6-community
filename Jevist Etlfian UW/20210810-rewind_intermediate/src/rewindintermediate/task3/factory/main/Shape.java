package rewindintermediate.task3.factory.main;

abstract class Shape {
	private String name;
	
	Shape(String name) {
		this.name = name;
	}
	
	String getName() {
		return this.name;
	}
	
    void drawing() {
        System.out.println("Drawing");
    }
    
    void coloring() {
        System.out.println("Coloring");
    }
}
