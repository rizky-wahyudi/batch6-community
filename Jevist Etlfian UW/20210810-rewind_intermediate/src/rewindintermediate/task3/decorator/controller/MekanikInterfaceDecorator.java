package rewindintermediate.task3.decorator.controller;

public class MekanikInterfaceDecorator implements MekanikInterface {
	protected MekanikInterface mekanikInterface;
	
	public MekanikInterfaceDecorator (MekanikInterface mekanikInterface) {
		this.mekanikInterface = mekanikInterface;
	}

	@Override
	public void job() {
		this.mekanikInterface.job();
	}
}
