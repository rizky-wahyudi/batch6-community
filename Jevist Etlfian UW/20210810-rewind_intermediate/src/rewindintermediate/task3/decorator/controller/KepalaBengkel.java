package rewindintermediate.task3.decorator.controller;

public class KepalaBengkel extends MekanikInterfaceDecorator {
	
	public KepalaBengkel(MekanikInterface mekanikInterface) {
		super(mekanikInterface);
	}
	
	@Override
	public void job(){
		super.job();
		System.out.print("Mengkoordinasi mekanik dalam pembuatan tugas servis");
	}
	
}
