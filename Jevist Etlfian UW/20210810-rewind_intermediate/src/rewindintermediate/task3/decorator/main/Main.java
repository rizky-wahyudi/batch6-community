package rewindintermediate.task3.decorator.main;

import rewindintermediate.task3.decorator.controller.KepalaBengkel;
import rewindintermediate.task3.decorator.controller.Mekanik;
import rewindintermediate.task3.decorator.controller.MekanikInterface;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MekanikInterface mekanik = new Mekanik();
		MekanikInterface kepalaBengkel = new KepalaBengkel(new Mekanik());
		
		System.out.println("==========================================");
		mekanik.job();

		System.out.println("==========================================");
		kepalaBengkel.job();
	}

}
