package rewindintermediate.task3.command.main;

import rewindintermediate.task3.command.controller.Command;
import rewindintermediate.task3.command.controller.Controller;
import rewindintermediate.task3.command.controller.LeftClickCommand;
import rewindintermediate.task3.command.controller.Mouse;
import rewindintermediate.task3.command.controller.RightClickCommand;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        Controller controller = new Controller();
        Mouse mouse = new Mouse();

        Command rightClick = new RightClickCommand(mouse);
        Command leftClick = new LeftClickCommand(mouse);

        controller.setCommand(rightClick);
        controller.clickCommand();

        controller.setCommand(leftClick);
        controller.clickCommand();
	}
}
