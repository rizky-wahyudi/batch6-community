package rewindintermediate.task3.command.controller;

public class LeftClickCommand implements Command {

	    private Mouse mouse;

	    public LeftClickCommand(Mouse mouse) {
	        this.mouse = mouse;
	    }

	    @Override
	    public void click() {
	        mouse.leftClick();
	    }

}
