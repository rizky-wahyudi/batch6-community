package rewindintermediate.task3.command.controller;

public class Mouse {
    public void rightClick() {
        System.out.println("Mouse Right Click");
    }

    public void leftClick() {
        System.out.println("Mouse Left Click");
    }
    
}
