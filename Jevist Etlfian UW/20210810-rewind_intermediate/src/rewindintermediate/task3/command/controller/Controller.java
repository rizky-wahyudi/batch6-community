package rewindintermediate.task3.command.controller;

public class Controller {
	    private Command command;

	    public void setCommand(Command command) {
	        this.command = command;
	    }

	    public void clickCommand() {
	        command.click();
	    }
	
}
