package rewindintermediate.task3.command.controller;

public class RightClickCommand implements Command {

	    private Mouse mouse;

	    public RightClickCommand(Mouse mouse) {
	        this.mouse = mouse;
	    }

	    @Override
	    public void click() {
	        mouse.rightClick();
	    }

}
