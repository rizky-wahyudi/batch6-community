package rewindintermediate.task3.observer.controller;

public class Student {
	private String name;
	private UniversityEmail email = new UniversityEmail();
	
	public Student(String name) {
		this.name = name;
	}
	
	public void receiveEmail () {
		System.out.println(this.name + " receive an email | info = " + email.getInfo());
	}
	
	public void activeUniversityEmail(UniversityEmail mail) {
		email = mail;
	}
}
