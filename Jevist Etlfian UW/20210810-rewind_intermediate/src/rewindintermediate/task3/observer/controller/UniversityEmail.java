package rewindintermediate.task3.observer.controller;

import java.util.ArrayList;
import java.util.List;

public class UniversityEmail {
	private List<Student> persons = new ArrayList<>();
	private String info;
	
	public void registerStudent(Student student) {
		persons.add(student);
	}
	
	public void unregisterStudent(Student student) {
		persons.remove(student);
	}
	
	public void sendEmail() {
		for (Student person : persons) {
			person.receiveEmail();
		}
	}
		
	public void announcement(String info) {
		this.info = info;
		sendEmail();
	}
	
	public String getInfo() {
		return this.info;
	}
}
