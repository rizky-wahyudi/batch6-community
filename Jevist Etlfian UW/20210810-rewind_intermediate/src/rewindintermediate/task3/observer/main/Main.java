package rewindintermediate.task3.observer.main;

import rewindintermediate.task3.observer.controller.Student;
import rewindintermediate.task3.observer.controller.UniversityEmail;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UniversityEmail atmajaya = new UniversityEmail();
		Student stud1 = new Student("Jevist Etlfian");
		Student stud2 = new Student("Jendy Lim");
		Student stud3 = new Student("Jaelani Sidik");
		Student stud4 = new Student("Michael Agustinus");
		
		atmajaya.registerStudent(stud1);
		atmajaya.registerStudent(stud2);
		atmajaya.registerStudent(stud3);
		atmajaya.registerStudent(stud4);
		
		stud1.activeUniversityEmail(atmajaya);
		stud2.activeUniversityEmail(atmajaya);
		stud3.activeUniversityEmail(atmajaya);
		stud4.activeUniversityEmail(atmajaya);
		
		atmajaya.announcement("Pengumuman besok Libur");
	}

}
