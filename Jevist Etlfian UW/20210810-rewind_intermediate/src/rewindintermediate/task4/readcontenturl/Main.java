package rewindintermediate.task4.readcontenturl;
import java.net.*;
import java.io.*;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub 
		try {
			for (int i = 0; i < 5; i++) {
				String urlName = "https://jsonplaceholder.typicode.com/todos/" + (i+1);
				URL url = new URL(urlName);
		        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
		        
				String NamaFile =  "src\\rewindintermediate\\task4\\"+"file-"+ (i+1) +".json";
				File file = new File(NamaFile);
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
		        
		        String inputLine;
		        while ((inputLine = br.readLine()) != null) {
		            bw.write(inputLine);
		        }
		        
		        br.close();
		        bw.close();
				System.out.println("file-"+(i+1)+".json selesai dibuat");
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
}
