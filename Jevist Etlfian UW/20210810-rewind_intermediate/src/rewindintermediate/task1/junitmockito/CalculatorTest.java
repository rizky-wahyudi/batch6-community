package rewindintermediate.task1.junitmockito;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAddAssertEquals() {
        Calculator calculator = new Calculator();
        int result = calculator.add(2, 2);
        Assert.assertEquals(4, result);
    }
    
    @Test
    public void testAddAssertTrue() {
        Calculator calculator = new Calculator();
        int result = calculator.add(2, 2);
        Assert.assertTrue(result == 4);
    }
    
    @Test
    public void testAddAssertFalse() {
        Calculator calculator = new Calculator();
        int result = calculator.add(2, 2);
        Assert.assertFalse(result == 5);
    }
    
    @Test
    public void testAddAssertFalseTwo() {
        Calculator calculator = new Calculator();
        int result = calculator.add(2, 2);
        Assert.assertFalse(result != 4 );
    }

	@Test
    public void testSubtractAssertEquals() {
        Calculator calculator = new Calculator();
        int result = calculator.subtract(4, 2);
        Assert.assertEquals(2, result);
    }
    
    @Test
    public void testSubtractAssertTrue() {
        Calculator calculator = new Calculator();
        int result = calculator.subtract(4, 2);
        Assert.assertTrue(result == 2);
    }
    
    @Test
    public void testSubtractAssertFalse() {
        Calculator calculator = new Calculator();
        int result = calculator.subtract(4, 2);
        Assert.assertFalse(result == 1);
    }
    
    @Test
    public void testSubtractAssertFalseTwo() {
        Calculator calculator = new Calculator();
        int result = calculator.subtract(4, 2);
        Assert.assertFalse(result != 2 );
    }
    
    @Test
    public void testMultiplyAssertEquals() {
        Calculator calculator = new Calculator();
        int result = calculator.multiply(4, 2);
        Assert.assertEquals(8, result);
    }
    
    @Test
    public void testMultiplyAssertTrue() {
        Calculator calculator = new Calculator();
        int result = calculator.multiply(4, 2);
        Assert.assertTrue(result == 8);
    }
    
    @Test
    public void testMultiplyAssertFalse() {
        Calculator calculator = new Calculator();
        int result = calculator.multiply(4, 2);
        Assert.assertFalse(result == 1);
    }
    
    @Test
    public void testMultiplyAssertFalseTwo() {
        Calculator calculator = new Calculator();
        int result = calculator.multiply(4, 2);
        Assert.assertFalse(result != 8 );
    }
       
    @Test
    public void testDivideAssertEquals() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(4, 2);
        Assert.assertEquals(2, result);
    }
    
    @Test
    public void testDivideAssertTrue() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(4, 2);
        Assert.assertTrue(result == 2);
    }
    
    @Test
    public void testDivideAssertFalse() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(4, 2);
        Assert.assertFalse(result == 1);
    }
    
    @Test
    public void testDivideAssertFalseTwo() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(4, 2);
        Assert.assertFalse(result != 2 );
    }
}
