package rewindintermediate.task2.threadexecutor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile implements Runnable  {
	@Override
	public void run() {
	    try {
	    	String[] namaFile = {"test1","test2","test3","test4"};
	    	for (String nama : namaFile) {
				String pathFile = "src\\rewindintermediate\\task2\\threadexecutor\\" + nama + ".txt";
		        File myObj = new File(pathFile);
		        Scanner myReader = new Scanner(myObj);
		        while (myReader.hasNextLine()) {
		          String data = myReader.nextLine();
		          System.out.println(data);
		        }
		        myReader.close();   
	    	}
	      } catch (FileNotFoundException e) {
	        System.out.println("An error occurred.");
	        e.printStackTrace();
	      }
	}
}
