package rewindintermediate.task2.threadexecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ExecutorService executor = Executors.newFixedThreadPool(2);
		for (int i = 0; i < 1000; i++) {
			ReadFile bacaFile = new ReadFile();
			executor.execute(bacaFile);
		}
		executor.shutdown();
	}
}
