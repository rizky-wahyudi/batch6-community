package main;

import java.util.Scanner;
import controller.Translator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Translator translator = new Translator();
		Scanner input = new Scanner(System.in);
		
		int temperatureType=0;
		int distanceType=0;
		int translatorType=0;
		double distanceNumber = 0;
		double temperatureNumber = 0;
		double result = 0;
		boolean checkTranslatorType = false;
		boolean checkTemperaturetype = false;
		boolean checkDistancetype = false;
		
		while (!checkTranslatorType) {
			translator.showOptionTranslatorType();
			System.out.println("======================================");
			System.out.print("Input Translator Type (1/2) : ");
			translatorType = input.nextInt();
			checkTranslatorType = translator.checkTranslatortype(translatorType);
		}
		
		if (translatorType == 1) {
			while(!checkTemperaturetype) {
				translator.showOptionTemperatureType();
				System.out.println("======================================");
				System.out.print("Input Temperature Type (1/2) : ");
				temperatureType = input.nextInt();
				checkTemperaturetype = translator.checkTemperatureType(temperatureType);
			}
			System.out.print("Input your temperature : ");
			temperatureNumber = input.nextDouble();
			result = translator.calculateTemperature(temperatureType, temperatureNumber);
			System.out.println("Converted Temperature = " + result);
			System.out.println("======================================");
		} else if (translatorType == 2) {
			while(!checkDistancetype) {
				translator.showOptionDistanceType();
				System.out.println("======================================");
				System.out.print("Input Distance Type (1/2) : ");
				distanceType = input.nextInt();
				checkDistancetype = translator.checkDistanceType(distanceType);
			}
			System.out.print("Input your Distance : ");
			distanceNumber = input.nextDouble();
			result = translator.calculateDistance(distanceType, distanceNumber);
			System.out.println("Converted Distance = " + result);
			System.out.println("======================================");
		} else {
			System.out.println("Error input Translator Type");
		}
		System.out.println("Finish Calculate");
		input.close();
	}

}
