package controller;

public class Translator {
	private double result;
	
	public void showOptionTranslatorType () {
		System.out.println(">>>>>>>>>>Choose Translator Type You Want To Translate<<<<<<<<<<");
		System.out.println("1. Temperature");
		System.out.println("2. Distance");
	}
	
	public void showOptionTemperatureType () {
		System.out.println(">>>>>>>>>>Choose Temperature Type You Want Convert To<<<<<<<<<<");
		System.out.println("1. Celcius");
		System.out.println("2. Farenheit");
	}
	
	public void showOptionDistanceType () {
		System.out.println(">>>>>>>>>>Choose Distance Type You Want Convert To<<<<<<<<<<");
		System.out.println("1. Kilometer");
		System.out.println("2. Miles");
	}
	
	public boolean checkTranslatortype (int typeId) {
		if (typeId==1) {
			System.out.println("You choose Temperature");	
			return true;
		} else if (typeId==2){
			System.out.println("You choose Distance");
			return true;
		} else {
			System.out.println("Warning ----- Error Input Translator");
			return false;
		}
	}
	
	public boolean checkTemperatureType (int typeId) {
		if (typeId==1) {
			System.out.println("You choose convert to Celcius");	
			return true;
		} else if (typeId==2){
			System.out.println("You choose convert to Farenheit");
			return true;
		} else {
			System.out.println("Warning ----- Error Input Temperature type");
			return false;
		}
	}
	
	public boolean checkDistanceType (int typeId) {
		if (typeId==1) {
			System.out.println("You choose convert to Kilometer");
			return true;
		} else if (typeId==2){
			System.out.println("You choose convert to Miles");
			return true;
		} else {
			System.out.println("Warning ----- Error Input Distance type");
			return false;
		}
	}
	
	public double calculateTemperature(int typeId, double temperature) {
		if (typeId==1) {
			result = (temperature - 32) * 5 / 9 ;
		} else if (typeId==2){
			result = (10.0 * 9 / 5) + 32 ;	
		}
		return result;
	}
	
	public double calculateDistance(int typeId, double distance) {
		if (typeId==1) {
			result = distance / 0.621 ;
		} else if (typeId==2){
			result = distance * 0.621 ;
		}
		return result;
	}
}
