CREATE TABLE handsonday11_tugas1.address (
	id INT NOT NULL AUTO_INCREMENT,
	street VARCHAR(225) NOT NULL,
	district VARCHAR(25) NOT NULL,
	city VARCHAR(25)  NOT NULL,
	province VARCHAR(25) NOT NULL,
	country VARCHAR(25) NOT NULL,
	longtitude DECIMAL(11,8) NOT NULL,
	latititude DECIMAL(10,8) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.image (
	id INT NOT NULL AUTO_INCREMENT,
	imageUrl VARCHAR(2083) NOT NULL,
	status VARCHAR (25) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.brand (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(25) NOT NULL,
	logoUrl VARCHAR(2083) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.category (
	id INT NOT NULL AUTO_INCREMENT,
	type VARCHAR(25) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.gearType (
	id INT NOT NULL AUTO_INCREMENT,
	type VARCHAR(25) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.dataIdNumber (
	id INT NOT NULL AUTO_INCREMENT,
	ktpNumber INT NOT NULL,
	passportNumber INT NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.dataIdImage (
	id INT NOT NULL AUTO_INCREMENT,
	ktpUrl VARCHAR(2083) NOT NULL,
	passportUrl VARCHAR(2083) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.car (
	id INT NOT NULL AUTO_INCREMENT,
	imageId INT NOT NULL,
	brandId INT NOT NULL,
	categoryId INT NOT NULL,
	engineSize VARCHAR(25) NOT NULL,
	fuelType  VARCHAR(25) NOT NULL,
	addressId INT NOT NULL,
	gearType INT NOT NULL,
	FOREIGN KEY (imageId) REFERENCES image(id),
	FOREIGN KEY (brandId) REFERENCES brand(id),
	FOREIGN KEY (categoryId) REFERENCES category(id),
	FOREIGN KEY (addressId) REFERENCES address(id),
	FOREIGN KEY (gearType) REFERENCES gearType(id),
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.customer (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	phone VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	addressId INT NOT NULL,
	idNumber INT NOT NULL,
	idImage INT NOT NULL,
	FOREIGN KEY (addressId) REFERENCES address(id),
	FOREIGN KEY (idNumber) REFERENCES dataIdNumber(id),
	FOREIGN KEY (idImage) REFERENCES dataIdImage(id),
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.service (
	id INT NOT NULL AUTO_INCREMENT,
	insurance BOOLEAN,
	weekdayPrice DECIMAL(20,2) NOT NULL,
	weekendPrice DECIMAL(20,2) NOT NULL,
	holidayPrice DECIMAL(20,2) NOT NULL,
	carCategoryId INT NOT NULL,
	carGearType INT NOT NULL,
	FOREIGN KEY (carCategoryId) REFERENCES category(id),
	FOREIGN KEY (carGearType) REFERENCES gearType(id),
	PRIMARY KEY (id));

CREATE TABLE handsonday11_tugas1.booking (
	id INT NOT NULL AUTO_INCREMENT,
	customerId INT NOT NULL,
	carId INT NOT NULL,
	serviceId INT NOT NULL,
	pickupLocation INT NOT NULL,
	dropOffLocation INT NOT NULL,
	pickupDate DATE,
	dropoffDate DATE,
	totalPrice DECIMAL(20,2) NOT NULL,
	FOREIGN KEY (customerId) REFERENCES customer(id),
	FOREIGN KEY (carId) REFERENCES car(id),
	FOREIGN KEY (serviceId) REFERENCES service(id),
	FOREIGN KEY (pickupLocation) REFERENCES address(id),
	FOREIGN KEY (dropOffLocation) REFERENCES address(id),
	PRIMARY KEY (id));

