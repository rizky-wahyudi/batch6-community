public class Laguage{
    private int berat;
    private String handLanguage;

    public int getBerat() {
        return berat;
    }

    public void setBerat(int berat) {
        this.berat = berat;
    }

    public String getHandLanguage() {
        return handLanguage;
    }

    public void setHandLanguage(String handLanguage) {
        this.handLanguage = handLanguage;
    }
    
}