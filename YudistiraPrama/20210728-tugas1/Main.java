import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("----- SELAMAT DATANG DI AIRPORT -----");
        System.out.print("Masukkan Nama                             : ");
        String nama = sc.next();
        System.out.print("Masukkan NO Tiket                         : ");
        String noTiket = sc.next();
        System.out.print("Masukkan Tanggal Keberangkatan (dd/mm/yyy): ");
        String tanggal = sc.next();
        System.out.print("Masukkan No Passport (8 digit)            : ");
        String pass = sc.next();
        System.out.print("Masukkan Jumlah Bagasi                    : ");
        int bagasi = sc.nextInt();
        System.out.print("Apakah ada barang bawaan? (ada/tidak)     : ");
        String hand = sc.next();
        System.out.println("");

        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Keberangkatan Hari : " + format.format(now));

        Laguage laguage = new Laguage();
        laguage.setBerat(bagasi);
        laguage.setHandLanguage(hand);

        Passport passport = new Passport();
        passport.setNama(nama);
        passport.setNo(pass);

        Ticket tiket = new Ticket();
        tiket.setNama(nama);
        tiket.setTanggal(tanggal);
        tiket.setNo(noTiket);
        tiket.setLanguage(laguage);

        Airport a = new Airport();
        a.setTicket(tiket);
        a.setPassport(passport);
        a.setLaguage(laguage);
        a.setNama("--- BANDARA SUSU MURNI INTERNASIONAL ---");

        System.out.println("");
        a.periksa();

    }
}