import java.text.SimpleDateFormat;
import java.util.Date;

public class Airport {

    private String nama;
    private Ticket ticket;
    private Passport passport;
    private Laguage laguage;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public Laguage getLaguage() {
        return laguage;
    }

    public void setLaguage(Laguage laguage) {
        this.laguage = laguage;
    }

    public boolean checkTicket() {
        System.out.println("Pemeriksaan Ticket ...");
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String nowDate = format.format(now);
        String ticketDate = getTicket().getTanggal();
        if (ticketDate.equals(nowDate)) {
            System.out.println("Tiket Valid");
            return true;
        } else {
            System.out.println("Ticket tidak valid");
            return false;
        }
    }

    public boolean checkPassport() {
        System.out.println("Pemeriksaan passport ...");
        if (getPassport().getNama().equalsIgnoreCase(getTicket().getNama())) {
            if (getPassport().getNo().length() == 8) {
                System.out.println("Nama dan no pada Passport valid");
                return true;
            } else {
                System.out.println("No pada Passport valid");
                return false;
            }
        } else {
            System.out.println("Nama dan no pada Passport tidak valid");
            return false;
        }
    }

    public boolean checkLaguage() {
        System.out.println("Pemeriksaan barang bagasi ...");
        if (getLaguage().getBerat() <= 40) {
            System.out.println("Jumlah Bagasi valid");
            return true;
        } else {
            System.out.println("Jumlah Bagasi melebihi batas ");
            return false;
        }
    }

    public boolean checkHandLaguage() {
        System.out.println("Pemeriksaan barang bawaan ...");
        if (getLaguage().getHandLanguage().equalsIgnoreCase("ada")) {
            System.out.println("Terdapat Barang Bawaan");
            return true;
        } else {
            System.out.println("Tidak Terdapat Barang Bawaan");
            return false;
        }
    }

    public void periksa() {
        System.out.println("-- SECURITY CHECK ---");
        if (checkTicket() == true && checkPassport() == true && checkLaguage() == true) {
            System.out.println("Seluruh Data valid ... ");
            System.out.println("");
            System.out.println("-- IMIGRATION CHECK ---");
            if (checkTicket() == true && checkPassport() == true) {
                System.out.println("Seluruh Data valid ... ");
                System.out.println("");
                System.out.println("-- X-RAY CHECK ---");
                if (checkTicket() == true && checkPassport() == true && checkHandLaguage() == true || checkHandLaguage() == false) {
                    System.out.println("");
                    System.out.println("Silahkan Masuk ke pesawat");
                    System.out.println("Pemeriksaan selesai");
                }
            }
        } else {
            System.out.println("");
            System.out.println("Maaf anda tidak bisa berangkat");
        }
    }
}