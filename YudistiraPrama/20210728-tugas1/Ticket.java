public class Ticket {

    private String no;
    private String nama;
    private String tanggal;
    private Laguage language;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public Laguage getLanguage() {
        return language;
    }

    public void setLanguage(Laguage language) {
        this.language = language;
    }

}