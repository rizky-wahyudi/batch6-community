import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        VendingMachine machine = new VendingMachine();
        machine.setIsiUang(5000);
        Item[][] isi = machine.getIsiMesin();

        System.out.println("----- MENU -----");
        for (int i = 0; i < 6; i++) {
            System.out.println("KODE " + machine.getKodeString(i));
            for (int j = 0; j < 10; j++) {
                System.out.print((j + 1) + " " + isi[i][j].getName() + ", ");
            }
            System.out.println("");
        }
        
        System.out.println("");
        System.out.println("----- PILIH ------");
        System.out.println("KODE    : A, B, C, D, E, F");
        System.out.println("POSISI  : 1, 2, 3, 4, 5, 6, 7, 8, 9, 10");

        System.out.println("");
        System.out.print("Pilih KODE    : ");
        String kode = sc.next();
        System.out.print("Pilih POSISI  : ");
        String posisi = sc.next();

        Item terpilih = isi[machine.getKode(kode)][Integer.valueOf(posisi) - 1];

        System.out.println("");
        System.out.println("Kode    : " + kode.toUpperCase() + terpilih.getCode());
        System.out.println("Jenis   : " + terpilih.getJenis());
        System.out.println("Nama    : " + terpilih.getName());
        System.out.println("Harga   : Rp " + terpilih.getPrice());

        System.out.println("");
        System.out.print("Silahkan Masukkan Uang Anda : Rp ");
        int uang = sc.nextInt();

        machine.setUangBayar(uang);

        System.out.println("");

        System.out.println("--- PESANAN DIPROSES ---");

        while (!machine.isSelesai()) {
            int kembali = machine.getUangBayar() - terpilih.getPrice();
            if (kembali == 0) {
                System.out.println("Uang diterima...");
                System.out.println("Uang diterima sesuai dengan harga");
                System.out.println("Silahkan ambil makanan/minuman anda ..");
                System.out.println("");
                System.out.println("Selamat Menikmati. Terima kasih");
                machine.setSelesai(true);
            } else if (kembali > 0) {
                if (kembali <= machine.getIsiUang()) {
                    System.out.println("Uang diterima...");
                    System.out.println("Uang diterima lebih Rp " + kembali + " dari harga");
                    System.out.println("Silahkan ambil makanan/minuman anda ..");
                    System.out.println("Silahkan ambil uang kembalian : Rp " + kembali);
                    System.out.println("");
                    System.out.println("Selamat Menikmati. Terima kasih");
                    machine.setSelesai(true);
                } else {
                    System.out.println("Uang diterima...");
                    System.out.println("Uang diterima lebih Rp " + kembali + " dari harga");
                    System.out.println("Uang pada mesih tidak cukup memberikan kembalian...");
                    System.out.println("Pesanan dibatalkan...");
                    System.out.println("Silahkan ambil kembali uang anda : Rp " + machine.getUangBayar());
                    System.out.println("");
                    System.out.println("Mohon maaf. Terima kasih");
                    machine.setSelesai(true);
                }
            } else {
                System.out.println("Uang diterima...");
                while (machine.getUangBayar() < terpilih.getPrice()) {
                    System.out.println("");
                    System.out.println("Uang yang anda masukkan kurang...");
                    System.out.println("Uang diterima kurang Rp " + Math.abs(terpilih.getPrice() - machine.getUangBayar()));
                    System.out.println("*Ketik -1 untuk membatalkan ");
                    System.out.print("Silahkan Masukkan Uang Anda : Rp ");
                    int tambahan = sc.nextInt();
                    if (tambahan != -1) {
                        machine.setUangBayar(machine.getUangBayar() + tambahan);
                    } else {
                        System.out.println("");
                        System.out.println("Pesanan dibatalkan...");
                        System.out.println("");
                        System.out.println("Mohon maaf. Terima kasih");
                        machine.setSelesai(true);
                        break;
                    }
                }
                System.out.println("");
            }
        }
    }
}