public class VendingMachine {

    private int isiUang;
    private int uangBayar;
    private boolean selesai;

    public VendingMachine() {
        this.setSelesai(false);
    }

    public int getIsiUang() {
        return isiUang;
    }

    public void setIsiUang(int isiUang) {
        this.isiUang = isiUang;
    }

    public int getUangBayar() {
        return uangBayar;
    }

    public void setUangBayar(int uangBayar) {
        this.uangBayar = uangBayar;
    }

    public boolean isSelesai() {
        return selesai;
    }

    public void setSelesai(boolean selesai) {
        this.selesai = selesai;
    }

    public Item[][] isiMesin() {
        String[] jenis = {"Chips", "Chocolate", "Candies", "Water", "Soft Drink", "Ice Tea"};
        String[] merek = {"Lays", "Chitato", "Beng-beng", "Silverqueen", "Yupi", "Aphelible",
            "Aqua", "Le Minerale", "Coca-cola", "Pepsi", "Teh Botol", "Teh pucuk"};
        int[] harga = {5000, 7000, 2000, 10000, 5000, 4000, 5000, 4000, 8000, 10000, 7000, 6000};

        int baris = 6;
        int kolom = 10;
        Item[][] isi = new Item[baris][kolom];
        int jen = 0;
        int mer = 0;
        int har = 0;

        for (int i = 0; i < baris; i++) {
            for (int j = 0; j < kolom; j++) {
                if (j == 5) {
                    jen++;
                    mer++;
                    har++;
                }
                isi[i][j] = new Item((j + 1), jenis[i], merek[mer], harga[har]);
            }
            jen++;
            mer++;
            har++;
        }

        return isi;
    }

    public Item[][] getIsiMesin() {
        return isiMesin();
    }

    public int getKode(String a) {
        if (a.equalsIgnoreCase("A")) {
            return 0;
        } else if (a.equalsIgnoreCase("B")) {
            return 1;
        } else if (a.equalsIgnoreCase("C")) {
            return 2;
        } else if (a.equalsIgnoreCase("D")) {
            return 3;
        } else if (a.equalsIgnoreCase("E")) {
            return 4;
        } else if (a.equalsIgnoreCase("F")) {
            return 5;
        } else {
            System.out.println("Inputan salah");
            return -1;
        }
    }

    public String getKodeString(int a) {
        switch (a) {
            case 0:
                return "A";
            case 1:
                return "B";
            case 2:
                return "C";
            case 3:
                return "D";
            case 4:
                return "E";
            case 5:
                return "F";
            default:
                System.out.println("Inputan salah");
                return null;
        }
    }

}