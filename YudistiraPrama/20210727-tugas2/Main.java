import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Pilih Penerbangan : ");
        System.out.println("1. Europe - Asia"
                + "\n2. Asia - Europe"
                + "\n3. Europe - US");

        System.out.print("\nPilih : ");
        int pilih = sc.nextInt();

        String[] plane = {"Airbus 380", "Boeing 747", "Boeing 787"};
        String awal;
        String Tujuan;

        Economy e = new Economy();
        Business b = new Business();
        FirstClass f = new FirstClass();

        switch (pilih) {
            case 1:
                awal = "europe";
                Tujuan = "asia";

                System.out.println("-----ECONOMY CLASS-----");
                e.setAwal(awal);
                e.setTujuan(Tujuan);
                e.setPlane(plane[0]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");

                e.setPlane(plane[1]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");

                e.setPlane(plane[2]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + e.totalLaguage() + " kg");
                System.out.println("Price       : $" + e.totalPrice());
                System.out.println("");

                System.out.println("-----BUSINESS CLASS-----");
                b.setAwal(awal);
                b.setTujuan(Tujuan);
                b.setPlane(plane[0]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");

                b.setPlane(plane[1]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");

                b.setPlane(plane[2]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + b.totalLaguage() + " kg");
                System.out.println("Price       : $" + b.totalPrice());
                System.out.println("");

                System.out.println("-----FIRST CLASS-----");
                f.setAwal(awal);
                f.setTujuan(Tujuan);
                f.setPlane(plane[0]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");

                f.setPlane(plane[1]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");

                f.setPlane(plane[2]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + f.totalLaguage() + " kg");
                System.out.println("Price       : $" + f.totalPrice());
                System.out.println("");
                break;
            case 2:
                awal = "Asia";
                Tujuan = "Europe";

                System.out.println("-----ECONOMY CLASS-----");
                e.setAwal(awal);
                e.setTujuan(Tujuan);
                e.setPlane(plane[0]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");

                e.setPlane(plane[1]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");

                e.setPlane(plane[2]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + e.totalLaguage() + " kg");
                System.out.println("Price       : $" + e.totalPrice());
                System.out.println("");

                System.out.println("-----BUSINESS CLASS-----");
                b.setAwal(awal);
                b.setTujuan(Tujuan);
                b.setPlane(plane[0]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");

                b.setPlane(plane[1]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");

                b.setPlane(plane[2]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + b.totalLaguage() + " kg");
                System.out.println("Price       : $" + b.totalPrice());
                System.out.println("");

                System.out.println("-----FIRST CLASS-----");
                f.setAwal(awal);
                f.setTujuan(Tujuan);
                f.setPlane(plane[0]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");

                f.setPlane(plane[1]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");

                f.setPlane(plane[2]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + f.totalLaguage() + " kg");
                System.out.println("Price       : $" + f.totalPrice());
                System.out.println("");
                break;
            case 3:
                awal = "Europe";
                Tujuan = "US";

                System.out.println("-----ECONOMY CLASS-----");
                e.setAwal(awal);
                e.setTujuan(Tujuan);
                e.setPlane(plane[0]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");

                e.setPlane(plane[1]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");

                e.setPlane(plane[2]);
                System.out.println("Plane       : " + e.getPlane());
                System.out.println("Seats       : " + e.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + e.totalLaguage() + " kg");
                System.out.println("Price       : $" + e.totalPrice());
                System.out.println("");

                System.out.println("-----BUSINESS CLASS-----");
                b.setAwal(awal);
                b.setTujuan(Tujuan);
                b.setPlane(plane[0]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");

                b.setPlane(plane[1]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");

                b.setPlane(plane[2]);
                System.out.println("Plane       : " + b.getPlane());
                System.out.println("Seats       : " + b.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + b.totalLaguage() + " kg");
                System.out.println("Price       : $" + b.totalPrice());
                System.out.println("");

                System.out.println("-----FIRST CLASS-----");
                f.setAwal(awal);
                f.setTujuan(Tujuan);
                f.setPlane(plane[0]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");

                f.setPlane(plane[1]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");

                f.setPlane(plane[2]);
                System.out.println("Plane       : " + f.getPlane());
                System.out.println("Seats       : " + f.totalSeats());
                System.out.println("");
                System.out.println("Laguage     : " + f.totalLaguage() + " kg");
                System.out.println("Price       : $" + f.totalPrice());
                System.out.println("");
                break;

            default:
                System.out.println("Inputan Salah");
                break;
        }

    }
}