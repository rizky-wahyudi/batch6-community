public class Economy extends AirPlane {

    @Override
    public int totalSeats() {
        return (int) (super.getSeats() * 0.65);
    }

    @Override
    public int totalPrice() {
        return (int) (super.getPrice() * 1);
    }

    @Override
    public int totalLaguage() {
        return (int) (super.getLaguage() * 1);
    }

}