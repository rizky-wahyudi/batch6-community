public class FirstClass extends AirPlane {

    @Override
    public int totalSeats() {
        return (int) (super.getSeats() * 0.1);
    }

    @Override
    public int totalPrice() {
        return (int) (super.getPrice() * 3);
    }

    @Override
    public int totalLaguage() {
        return (int) (super.getLaguage() * 2.5);
    }
}