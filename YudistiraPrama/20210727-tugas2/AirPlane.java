public abstract class AirPlane {

    protected String awal;
    protected String tujuan;
    protected String plane;

    public String getAwal() {
        return awal;
    }

    public void setAwal(String awal) {
        this.awal = awal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String getPlane() {
        return plane;
    }

    public void setPlane(String plane) {
        this.plane = plane;
    }

    public double getSeats() {
        if (plane.equalsIgnoreCase("Airbus 380")) {
            return 750;
        } else if (plane.equalsIgnoreCase("Boeing 747")) {
            return 529;
        } else if (plane.equalsIgnoreCase("Boeing 787")) {
            return 248;
        } else {
            System.out.println("Pesawat tidak ada");
            return -1;
        }

    }

    public double getPrice() {
        if (awal.equalsIgnoreCase("europe") && tujuan.equalsIgnoreCase("asia")) {
            return 500;
        } else if (awal.equalsIgnoreCase("asia") && tujuan.equalsIgnoreCase("europe")) {
            return 600;
        } else if (awal.equalsIgnoreCase("europe") && tujuan.equalsIgnoreCase("US")) {
            return 650;
        } else {
            System.out.println("Penerbangan tidak ada");
            return -1;
        }
    }

    public double getLaguage() {
        return 20;
    }

    public abstract int totalSeats();

    public abstract int totalPrice();

    public abstract int totalLaguage();
}