public class Business extends AirPlane {

    @Override
    public int totalSeats() {
        return (int) (super.getSeats() * 0.25);
    }

    @Override
    public int totalPrice() {
        return (int) (super.getPrice() * 2.25);
    }

    @Override
    public int totalLaguage() {
        return (int) (super.getLaguage() * 2);
    }

}