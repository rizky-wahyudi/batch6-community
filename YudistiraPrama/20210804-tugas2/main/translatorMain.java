import java.util.Scanner;
import model.jarak;
import model.suhu;

public class translatorMain {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean koversi = true;
        while (koversi == true) {
            System.out.println("PILIH KONVERSI : ");
            System.out.println("1. Jarak"
                    + "\n2. Suhu");
            System.out.println("");
            System.out.print("Pilihan : ");
            int pilih = sc.nextInt();

            System.out.println("");
            switch (pilih) {
                case 1:
                    System.out.println("PILIH KONVERSI JARAK : ");
                    System.out.println("1. Kilometer -  Mil"
                            + "\n2. Mil - Kilometer");
                    System.out.println("");
                    System.out.print("Pilihan : ");
                    int pilihJarak = sc.nextInt();
                    System.out.print("Masukkan Jarak : ");
                    double besarJarak = sc.nextDouble();
                    jarak jarak = new jarak();
                    jarak.setJarak(besarJarak);
                    System.out.println("");
                    switch (pilihJarak) {
                        case 1:
                            System.out.println("Konversi Kilometer - Mil");
                            System.out.println("Kilometer   : " + besarJarak);
                            System.out.println("Mil         : " + jarak.getKMtoMiles());
                            break;
                        case 2:
                            System.out.println("Konversi Mil - Kilometer");
                            System.out.println("Mil         : " + besarJarak);
                            System.out.println("Kilometer   : " + jarak.getMilestoKM());
                            break;
                        default:
                            System.out.println("Inputan Salah!");
                            break;
                    }
                    break;
                case 2:
                    System.out.println("PILIH KONVERSI SUHU : ");
                    System.out.println("1. Celcius -  Fahrenheit"
                            + "\n2. Fahrenheit - Celcius");
                    System.out.println("");
                    System.out.print("Pilihan : ");
                    int pilihSuhu = sc.nextInt();
                    System.out.print("Masukkan Suhu : ");
                    double besarSuhu = sc.nextDouble();
                    suhu suhu = new suhu();
                    suhu.setSuhu(besarSuhu);
                    System.out.println("");
                    switch (pilihSuhu) {
                        case 1:
                            System.out.println("Konversi Celcius -  Fahrenheit");
                            System.out.println("Celcius     : " + besarSuhu);
                            System.out.println("Fahrenheit  : " + suhu.getCelsiusToFahrenheit());
                            break;
                        case 2:
                            System.out.println("Konversi Fahrenheit - Celcius");
                            System.out.println("Fahrenheit  : " + besarSuhu);
                            System.out.println("Celcius     : " + suhu.getFahrenheitToCelsius());
                            break;
                        default:
                            System.out.println("Inputan Salah!");
                            break;
                    }
                    break;
                default:
                    System.out.println("Inputan Salah!");
                    break;
            }

            System.out.println("");
            System.out.print("Konversi Lagi? (Y/N) : ");
            String lagi = sc.next();

            koversi = !lagi.equalsIgnoreCase("n");
            System.out.println("=====================================");
            System.out.println("");
        }

    }
}
