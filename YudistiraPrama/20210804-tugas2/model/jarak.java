public class jarak {

    private double jarak;

    public double getJarak() {
        return jarak;
    }

    public void setJarak(double jarak) {
        this.jarak = jarak;
    }

    public double getKMtoMiles() {
        return (getJarak() * 0.621371);
    }

    public double getMilestoKM() {
        return (getJarak() * 1.60934);
    }

}
