public class suhu {

    private double suhu;

    public double getSuhu() {
        return suhu;
    }

    public void setSuhu(double suhu) {
        this.suhu = suhu;
    }

    public double getCelsiusToFahrenheit() {
        return (((1.8) * this.getSuhu()) + 32);
    }

    public double getFahrenheitToCelsius() {
        return ((getSuhu() - 32) * 0.55);
    }
}
