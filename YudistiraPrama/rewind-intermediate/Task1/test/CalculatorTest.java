import Task1.Calculator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {
    
    @Test
    public void testAdd() {
        int result = Calculator.add(2, 2);
        int expect = 4;
        assertTrue(expect == result);
        assertFalse(result != expect);
        assertFalse(result+1 == expect);
        assertThrows(NumberFormatException.class, ()->{
           Calculator.add(2, Integer.parseInt("dua"));
        });
        assertEquals(expect, result);
    }
    
    @Test
    public void testSubtract() {
        int result = Calculator.subtract(2, 2);
        int expect = 0;
        assertTrue(expect == result);
        assertFalse(result != expect);
        assertFalse(result+1 == expect);
        assertThrows(NumberFormatException.class, ()->{
           Calculator.subtract(3, Integer.parseInt("dua"));
        });
        assertEquals(expect, result);
    }
    
    @Test
    public void testMultiply() {
        int result = Calculator.multiply(2, 2);
        int expect = 4;
        assertTrue(expect == result);
        assertFalse(result != expect);
        assertFalse(result+1 == expect);
        assertThrows(NumberFormatException.class, ()->{
           Calculator.multiply(2, Integer.parseInt("dua"));
        });
        assertEquals(expect, result);
    }
    
    @Test
    public void testDivide() {
        int result = Calculator.divide(2, 2);
        int expect = 1;
        assertTrue(expect == result);
        assertFalse(result != expect);
        assertFalse(result+1 == expect);
        assertThrows(NumberFormatException.class, ()->{
           Calculator.divide(2, Integer.parseInt("dua"));
        });
        assertEquals(expect, result);
    }
}
