import model.ThreadFile
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    
    public static void main(String[] args) {
        FileClass file = new FileClass();
        file.buatFileTxt("file1.txt", "Ini adalah isi File 1");
        file.buatFileTxt("file2.txt", "Ini adalah isi File 2");
        file.buatFileTxt("file3.txt", "Ini adalah isi File 3");
        file.buatFileTxt("file4.txt", "Ini adalah isi File 4");
        
        String[] namaFile = {"file1.txt", "file2.txt", "file3.txt", "file4.txt"};
        
        ExecutorService executor = Executors.newFixedThreadPool(2);
        
        System.out.println("");
        for (String namaFile1 : namaFile) {
            executor.execute(new ThreadFile(namaFile1));
        }
        executor.shutdown();
    }
}
