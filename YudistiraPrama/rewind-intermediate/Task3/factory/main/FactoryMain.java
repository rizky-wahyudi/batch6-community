import model.Transport;
import model.TransportGarage;

public class FactoryMain {

    public static void main(String[] args) {
        TransportGarage transport = new TransportGarage();
        Transport t1 = transport.getTransport("Bus");
        Transport t2 = transport.getTransport("Pesawat");
        Transport t3 = transport.getTransport("Kereta");
    }
}
