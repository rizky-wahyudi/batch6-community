abstract class TransportFactory {

    abstract Transport createTransport(String type);

    public Transport getTransport(String type) {
        Transport transport = createTransport(type);
        if (transport == null) {
            System.out.println("Maaf Transpot tidak tersedia\n");
            return null;
        }
        System.out.println("Memesan " + transport.getNama());
        transport.pesanTransport();
        transport.siapTransport();
        System.out.println("Menyiapkan " + transport.getNama() + "\n");
        return transport;
    }
}
