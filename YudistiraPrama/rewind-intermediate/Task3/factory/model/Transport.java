public class Transport {

    private String nama;
    private int kapasitas;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(int kapasitas) {
        this.kapasitas = kapasitas;
    }
    
    public void pesanTransport(){
        System.out.println("Memesan transport pilihan");
    }
    
    public void siapTransport(){
        System.out.println("Mempersiapkan transport pilihan");
    }

}
