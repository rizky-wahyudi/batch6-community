public class TransportGarage extends TransportFactory {

    @Override
    public Transport createTransport(String type) {
        if (type.equalsIgnoreCase("Pesawat")) {
            return new Pesawat("Garuda Indonesia", 250);
        } else if (type.equalsIgnoreCase("Kereta")) {
            return new Kereta("KAI Laksana", 500);
        } else {
            return null;
        }
    }

}
