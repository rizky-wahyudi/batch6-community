import model.Instagram;
import model.InstagramFollowers;

public class observerMain {
    public static void main(String[] args) {
        Instagram instagram = new Instagram();
        
        InstagramFollowers followers1 = new InstagramFollowers(instagram);
        InstagramFollowers followers2 = new InstagramFollowers(instagram);
        InstagramFollowers followers3 = new InstagramFollowers(instagram);

        instagram.addObserver(followers1);
        instagram.addObserver(followers2);
        instagram.addObserver(followers3);
        
        instagram.newPost("Happy Sunday");
        
        System.out.println("");
        instagram.newPost("Family Time");
        
        System.out.println("");
        instagram.newPost("Pizza timeeeee");
        
    }
}
