import java.util.ArrayList;

public class Instagram implements Observable {

    private ArrayList<Observer> observers = new ArrayList<Observer>();

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    public void newPost(String post) {
        System.out.println("Posting new photo " + post);
        notifyObserver();
    }

}
