public class InstagramFollowers implements Observer {

    private Observable observeble;

    public InstagramFollowers(Observable observable) {
        this.observeble = observable;
    }

    @Override
    public void update() {
        System.out.println("Segera lihat postingan terbaru");
    }

}
