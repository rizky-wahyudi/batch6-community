import model.Anjing;
import model.Hewan;
import model.Kucing;
import model.makananDecorator;

public class decoratorMain {

    public static void main(String[] args) {

        Hewan kucing = new makananDecorator(new Kucing());
        Hewan Anjing = new makananDecorator(new Anjing());
        
        System.out.println("Kucing");
        kucing.suara();
        
        System.out.println("");
        System.out.println("Anjing");
        Anjing.suara();      

    }
}
