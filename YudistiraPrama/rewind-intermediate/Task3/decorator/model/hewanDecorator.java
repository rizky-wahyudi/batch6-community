public class hewanDecorator implements Hewan {

    protected Hewan decoratorHewan;

    public hewanDecorator(Hewan decoratorHewan) {
        this.decoratorHewan = decoratorHewan;
    }

    @Override
    public void suara() {
        decoratorHewan.suara();
    }

}
