public class makananDecorator extends hewanDecorator{

    public makananDecorator(Hewan decoratorHewan) {
        super(decoratorHewan);
    }
    
    @Override
    public void suara(){
        decoratorHewan.suara();
        setMakanan(decoratorHewan);
    }
    
    private void setMakanan(Hewan decoratorHewan){
        System.out.println("Makan nasi");
    }
}
