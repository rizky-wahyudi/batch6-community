public class komporMatiCommand implements Command {

    private Kompor kompor;

    public komporMatiCommand(Kompor kompor) {
        this.kompor = kompor;
    }

    @Override
    public void execute() {
        kompor.komporMati();
    }

}
