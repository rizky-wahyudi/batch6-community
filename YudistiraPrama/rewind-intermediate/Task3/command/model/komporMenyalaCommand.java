public class komporMenyalaCommand implements Command {

    private Kompor kompor;

    public komporMenyalaCommand(Kompor kompor) {
        this.kompor = kompor;
    }

    @Override
    public void execute() {
        kompor.komporMenyala();
    }
}
