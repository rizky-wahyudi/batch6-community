import model.Command;
import model.Controller;
import model.Kompor;
import model.komporMatiCommand;
import model.komporMenyalaCommand;

public class CommandMain {

    public static void main(String[] args) {
        Controller controller = new Controller();
        Kompor kompor = new Kompor();

        Command komporMenyala = new komporMenyalaCommand(kompor);
        Command komporMati = new komporMatiCommand(kompor);
        controller.setCommand(komporMenyala);
        controller.executeCommand();

        controller.setCommand(komporMati);
        controller.executeCommand();
    }
}
