import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileClass {

    public void buatFileTxt(String nama, String isi) {
        try {
            FileWriter file = new FileWriter(nama);
            file.write(isi);
            file.close();

            System.out.println("File " + nama + " berhasil dibuat");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public String bacaFileTxt(String file) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            System.out.println("Isi File " + file);

            String barisData;
            while ((barisData = br.readLine()) != null) {
                System.out.println(barisData);
            }
            br.close();

        } catch (FileNotFoundException e1) {
            System.out.println("File tidak ditemukan " + file);
        } catch (Exception e2) {
            System.out.println("File tidak dapat dibaca " + file);
        }
        return null;
    }

}
