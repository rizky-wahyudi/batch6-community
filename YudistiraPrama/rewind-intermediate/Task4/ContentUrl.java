import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ContentUrl {
    
    public static void main(String[] args) {
        String output = null;
        FileClass file = new FileClass();
        
        for (int i = 1; i <= 5; i++) {
            output = getUrlContents("https://jsonplaceholder.typicode.com/todos/#" + i);
            file.buatFileTxt("#" + i + ".json", output);
        }
        
    }
    
    private static String getUrlContents(String theUrl) {
        StringBuilder content = new StringBuilder();
        
        try {
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}
