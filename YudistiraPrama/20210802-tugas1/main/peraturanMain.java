import java.util.ArrayList;
import java.util.Scanner;
import model.peraturan;

public class peraturanMain {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        peraturan peraturan = new peraturan();
        ArrayList<peraturan> daftar = peraturan.getDaftarPeraturan();

        System.out.println("--- DAFTAR PERATURAN ---");
        for (int i = 0; i < daftar.size(); i++) {
            System.out.println("PERATURAN " + (i + 1) + " : ");
            System.out.println(daftar.get(i).getJenis() + " No." + daftar.get(i).getNoPeraturan() + " Tahun " + daftar.get(i).getTahunPeraturan());
        }
        System.out.println("");
        System.out.print("Pilih No Urutan Peraturan : ");
        int pilih = sc.nextInt();
        
        System.out.println("");
        switch (pilih) {
            case 1:
                daftar.get(pilih-1).cetakPeraturan();
                break;
            case 2:
                daftar.get(pilih-1).cetakPeraturan();
                break;
            case 3:
                daftar.get(pilih-1).cetakPeraturan();
                break;
            default:
                System.out.println("Inputan Salah!");
                break;
        }

    }

}
