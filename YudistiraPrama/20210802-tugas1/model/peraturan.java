import java.util.ArrayList;

public class peraturan {

    private String tanggalDiundangkan;
    private String jenis;
    private String noPeraturan;
    private String tahunPeraturan;
    private String tentang;
    private String tanggalDitetapkan;
    private String noLN;
    private String noTLN;
    private isiDokumen isi;

    public String getTanggalDiundangkan() {
        return tanggalDiundangkan;
    }

    public void setTanggalDiundangkan(String tanggalDiundangkan) {
        this.tanggalDiundangkan = tanggalDiundangkan;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getNoPeraturan() {
        return noPeraturan;
    }

    public void setNoPeraturan(String noPeraturan) {
        this.noPeraturan = noPeraturan;
    }

    public String getTahunPeraturan() {
        return tahunPeraturan;
    }

    public void setTahunPeraturan(String tahunPeraturan) {
        this.tahunPeraturan = tahunPeraturan;
    }

    public String getTentang() {
        return tentang;
    }

    public void setTentang(String tentang) {
        this.tentang = tentang;
    }

    public String getTanggalDitetapkan() {
        return tanggalDitetapkan;
    }

    public void setTanggalDitetapkan(String tanggalDitetapkan) {
        this.tanggalDitetapkan = tanggalDitetapkan;
    }

    public String getNoLN() {
        return noLN;
    }

    public void setNoLN(String noLN) {
        this.noLN = noLN;
    }

    public String getNoTLN() {
        return noTLN;
    }

    public void setNoTLN(String noTLN) {
        this.noTLN = noTLN;
    }

    public isiDokumen getIsi() {
        return isi;
    }

    public void setIsi(isiDokumen isi) {
        this.isi = isi;
    }

    public void cetakPeraturan() {
        System.out.println("DETAIL PERATURAN : ");
        System.out.println("Jenis               : " + getJenis());
        System.out.println("No Peraturan        : " + getNoPeraturan());
        System.out.println("Tahun Peraturan     : " + getTahunPeraturan());
        System.out.println("Tentang             : " + getTentang());
        System.out.println("Tanggal Ditetapkan  : " + getTanggalDitetapkan());
        System.out.println("No LN               : " + getNoLN());
        System.out.println("No TLN              : " + getNoTLN());
        System.out.println("Tanggal Diundangkan : " + getTanggalDiundangkan());
        System.out.println("");
        System.out.println("ISI PERATURAN :");

        System.out.println("MENIMBANG : ");
        ArrayList<String> menimbang = getIsi().getMenimbang().getPoin();
        for (int i = 0; i < menimbang.size(); i++) {
            System.out.println((i + 1) + ". " + menimbang.get(i));
        }
        System.out.println("");
        System.out.println("MENGINGAT : ");
        ArrayList<String> mengingat = getIsi().getMengingat().getPoin();
        for (int i = 0; i < mengingat.size(); i++) {
            System.out.println((i + 1) + ". " + mengingat.get(i));
        }

        System.out.println("");
        System.out.println("MENETAPKAN : ");
        ArrayList<pasal> menetapkan = getIsi().getMenetapkan().getPasal();
        for (int i = 0; i < menetapkan.size(); i++) {
            System.out.println(menetapkan.get(i).getNama());
            ArrayList<String> sub = menetapkan.get(i).getSubPasal();
            for (int j = 0; j < sub.size(); j++) {
                System.out.println((j + 1) + ". " + sub.get(j));
            }
            System.out.println("");
        }

    }

    public ArrayList<peraturan> getDaftarPeraturan() {
        ArrayList<peraturan> daftarPeraturan = new ArrayList<>();

        //peraturan1
        peraturan peraturan1 = new peraturan();
        peraturan1.setJenis("Undang-Undang");
        peraturan1.setNoPeraturan("1");
        peraturan1.setTahunPeraturan("2020");
        peraturan1.setTentang("PENGESAHAN PERSETUJUAN KEMITRAAN EKONOMI KOMPREHENSIF INDONESIA–AUSTRALIA (INDONESIA–AUSTRALIA COMPREHENSIVE ECONOMIC PARTNERSHIP AGREEMENT)");
        peraturan1.setTanggalDitetapkan("2020-02-28");
        peraturan1.setNoLN("67");
        peraturan1.setNoTLN("6476");
        peraturan1.setTanggalDiundangkan("2020-02-28");

        isiDokumen isi1 = new isiDokumen();

        ArrayList<String> subPasal1 = new ArrayList<>();
        String sub1 = "Mengesahkan Persetujuan Kemitraan Ekonomi Komprehensif Indonesia–Australia";
        String sub2 = "Salinan naskah asli Persetujuan Kemitraan Ekonomi Komprehensif Indonesia–Australia";
        subPasal1.add(sub1);
        subPasal1.add(sub2);

        ArrayList<String> subPasal2 = new ArrayList<>();
        sub1 = "Undang-Undang ini mulai berlaku pada tanggal diundangkan.";
        subPasal2.add(sub1);

        ArrayList<pasal> daftarPasal = new ArrayList<>();
        pasal pasal1 = new pasal();
        pasal1.setNama("Pasal 1");
        pasal1.setSubPasal(subPasal1);
        daftarPasal.add(pasal1);

        pasal pasal2 = new pasal();
        pasal2.setNama("Pasal 2");
        pasal2.setSubPasal(subPasal2);
        daftarPasal.add(pasal2);

        menetapkan menetapkan1 = new menetapkan();
        menetapkan1.setPasal(daftarPasal);

        isi1.setMenetapkan(menetapkan1);

        ArrayList<String> subPasalMengingat = new ArrayList<>();
        sub1 = "Pasal 5 ayat (1), Pasal 11";
        sub2 = "Undang-Undang Nomor 24 Tahun 2000";
        String sub3 = "Undang-Undang Nomor 7 Tahun 2014";
        subPasalMengingat.add(sub1);
        subPasalMengingat.add(sub2);
        subPasalMengingat.add(sub3);

        mengingat mengingat1 = new mengingat();
        mengingat1.setPoin(subPasalMengingat);

        isi1.setMengingat(mengingat1);

        ArrayList<String> subPasalMenimbang = new ArrayList<>();
        sub1 = "bahwa kegiatan perdagangan merupakan salah satu sektor utama penggerak perekonomian nasional";
        sub2 = "bahwa untuk meningkatkan kerja sama ekonomi secara komprehensif antara Indonesia dan Australia";
        sub3 = "bahwa untuk melaksanakan Persetujuan Kemitraan Ekonomi Komprehensif Indonesia–Australia";
        String sub4 = "bahwa berdasarkan pertimbangan sebagaimana dimaksud dalam huruf a, huruf b, dan huruf c";
        subPasalMenimbang.add(sub1);
        subPasalMenimbang.add(sub2);
        subPasalMenimbang.add(sub3);
        subPasalMenimbang.add(sub4);

        menimbang menimbang1 = new menimbang();
        menimbang1.setPoin(subPasalMenimbang);

        isi1.setMenimbang(menimbang1);

        peraturan1.setIsi(isi1);
        daftarPeraturan.add(peraturan1);

        //peraturan 2
        peraturan peraturan2 = new peraturan();
        peraturan2.setJenis("Undang-Undang");
        peraturan2.setNoPeraturan("3");
        peraturan2.setTahunPeraturan("2020");
        peraturan2.setTentang("PERUBAHAN ATAS UNDANG-UNDANG NOMOR 4 TAHUN 2009 TENTANG PERTAMBANGAN MINERAL DAN BATUBARA");
        peraturan2.setTanggalDitetapkan("2020-06-10");
        peraturan2.setNoLN("147");
        peraturan2.setNoTLN("6525");
        peraturan2.setTanggalDiundangkan("2020-06-10");

        isiDokumen isi2 = new isiDokumen();

        ArrayList<String> subPasal21 = new ArrayList<>();
        sub1 = "Pertambangan adalah sebagian atau seluruh tahapan kegiatan dalam rangka";
        sub2 = "Mineral adalah senyawa anorganik yang terbentuk di alam";
        subPasal21.add(sub1);
        subPasal21.add(sub2);

        ArrayList<pasal> daftarPasal2 = new ArrayList<>();
        pasal pasal21 = new pasal();
        pasal21.setNama("Pasal 1");
        pasal21.setSubPasal(subPasal21);
        daftarPasal2.add(pasal21);

        menetapkan menetapkan2 = new menetapkan();
        menetapkan2.setPasal(daftarPasal2);

        isi2.setMenetapkan(menetapkan2);

        ArrayList<String> subPasalMengingat2 = new ArrayList<>();
        sub1 = "Pasal 20, Pasal 21, dan Pasal 33 ayat (2) dan ayat (3) ";
        sub2 = "Undang-Undang Nomor 4 Tahun 2009 tentang Pertambangan Mineral dan Batubara";
        subPasalMengingat2.add(sub1);
        subPasalMengingat2.add(sub2);

        mengingat mengingat2 = new mengingat();
        mengingat2.setPoin(subPasalMengingat2);
        isi2.setMengingat(mengingat2);

        ArrayList<String> subPasalMenimbang2 = new ArrayList<>();
        sub1 = "bahwa mineral dan batubara yang berada di dalam wilayah Negara Kesatuan Republik Indonesia";
        sub2 = "bahwa kegiatan usaha pertambangan mineral dan batubara mempunyai peranan penting";
        sub3 = "bahwa pengaturan mengenai pertambangan mineral dan batubara yang saat ini diatur dalam UndangUndang Nomor 4 Tahun 2009 tentang Pertambangan Mineral dan Batubara";
        sub4 = "bahwa berdasarkan pertimbangan sebagaimana dimaksud dalam huruf a, huruf b, dan huruf c";
        subPasalMenimbang2.add(sub1);
        subPasalMenimbang2.add(sub2);
        subPasalMenimbang2.add(sub3);
        subPasalMenimbang2.add(sub4);

        menimbang menimbang2 = new menimbang();
        menimbang2.setPoin(subPasalMenimbang2);

        isi2.setMenimbang(menimbang2);

        peraturan2.setIsi(isi2);

        daftarPeraturan.add(peraturan2);

        return daftarPeraturan;
    }

}
