import java.util.ArrayList;

public class pasal {

    private String nama;
    private ArrayList<String> subPasal;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public ArrayList<String> getSubPasal() {
        return subPasal;
    }

    public void setSubPasal(ArrayList<String> subPasal) {
        this.subPasal = subPasal;
    }

}
