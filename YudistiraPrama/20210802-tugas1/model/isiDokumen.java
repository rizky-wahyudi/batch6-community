public class isiDokumen {

    private menimbang menimbang;
    private mengingat mengingat;
    private menetapkan menetapkan;

    public menimbang getMenimbang() {
        return menimbang;
    }

    public void setMenimbang(menimbang menimbang) {
        this.menimbang = menimbang;
    }

    public mengingat getMengingat() {
        return mengingat;
    }

    public void setMengingat(mengingat mengingat) {
        this.mengingat = mengingat;
    }

    public menetapkan getMenetapkan() {
        return menetapkan;
    }

    public void setMenetapkan(menetapkan menetapkan) {
        this.menetapkan = menetapkan;
    }
    
    
}
