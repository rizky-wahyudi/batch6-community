import model.electricity;
import java.util.Scanner;

public class ElectricityCalculator {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("----- ELECTRICITY CALCULATOR -----");
        System.out.println("*Masukkan 0 jika tidak ada");
        System.out.print("Resstance(R)    = ");
        double res = sc.nextDouble();
        System.out.print("Current(I)      = ");
        double cur = sc.nextDouble();
        System.out.print("Voltage(V)      = ");
        double vol = sc.nextDouble();
        System.out.print("Power(P)        = ");
        double pow = sc.nextDouble();

        System.out.println("");
        electricity cal = new electricity();
        cal.setResistance(res);
        cal.setCurrent(cur);
        cal.setVoltage(vol);
        cal.setPower(pow);

        cal.cetakHasil();
    }

}