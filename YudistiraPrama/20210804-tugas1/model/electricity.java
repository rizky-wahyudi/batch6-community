public class electricity {

    private double resistance;
    private double current;
    private double voltage;
    private double power;

    public double getCurrent() {
        return current;
    }

    public void setCurrent(double current) {
        this.current = current;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public double getResistance() {
        return resistance;
    }

    public void setResistance(double resistance) {
        this.resistance = resistance;
    }

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public double resistanceValue() {
        double res = 0;
        if (getVoltage() != 0 && getCurrent() != 0) {
            res = (getVoltage() / getCurrent());
        } else if (getVoltage() != 0 && getPower() != 0) {
            res = (Math.pow(getVoltage(), 2) / getPower());
        } else if (getPower() != 0 && getCurrent() != 0) {
            res = (getPower() / Math.pow(getCurrent(), 2));
        } else {
            res = 0;
        }
        return res;
    }

    public double currentValue() {
        double cur = 0;
        if (getVoltage() != 0 && getResistance() != 0) {
            cur = (getVoltage() / getResistance());
        } else if (getPower() != 0 && getVoltage() != 0) {
            cur = (getPower() / getVoltage());
        } else if (getPower() != 0 && getResistance() != 0) {
            cur = (Math.sqrt((getPower() / getResistance())));
        } else {
            cur = 0;
        }
        return cur;
    }

    public double voltagetValue() {
        double vol = 0;
        if (getCurrent() != 0 && getResistance() != 0) {
            vol = (getCurrent() * getResistance());
        } else if (getPower() != 0 && getCurrent() != 0) {
            vol = (getPower() / getCurrent());
        } else if (getPower() != 0 && getResistance() != 0) {
            vol = (Math.sqrt((getPower() * getResistance())));
        } else {
            vol = 0;
        }

        return vol;
    }

    public double powerValue() {
        double po = 0;
        if (getVoltage() != 0 && getCurrent() != 0) {
            po = (getVoltage() * getCurrent());
        } else if (getVoltage() != 0 && getResistance() != 0) {
            po = (Math.pow(getVoltage(), 2) / getResistance());
        } else if (getCurrent() != 0 && getResistance() != 0) {
            po = (Math.pow(getCurrent(), 2) * getResistance());
        } else {
            po = 0;
        }
        return po;
    }

    public void cetakHasil() {

        for (int i = 0; i < 4; i++) {
            if (getResistance() == 0) {
               this.setResistance(resistanceValue());
            } else if (getCurrent() == 0) {
                this.setCurrent(currentValue());
            } else if (getVoltage() == 0) {
                this.setVoltage(voltagetValue());
            } else if (getPower() == 0) {
                this.setPower(powerValue());
            }
        }
        
        if (getCurrent() != currentValue()) {
            this.setCurrent(currentValue());
        }
        if (getPower()!= powerValue()) {
            this.setPower(powerValue());
        }

        System.out.println("----- HASIL PERHITUNGAN -----");
        System.out.println("Resistance(R)   = " + getResistance() + " ohms");
        System.out.println("Current(I)      = " + getCurrent() + " amps");
        System.out.println("Voltage(60)     = " + getVoltage() + " volts");
        System.out.println("Power(P)        = " + getPower() + " watts");
    }

}
