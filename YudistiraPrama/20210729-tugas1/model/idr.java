public class idr extends Valuta {

    public idr(String code) {
        super.setCode(code);
    }

    public double idrToEur(double idr) {
        return (idr * 0.000058);
    }

    public double idrToUsd(double idr) {
        return (idr * 0.000069);
    }
}