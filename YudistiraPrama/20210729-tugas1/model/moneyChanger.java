public class moneyChanger {

    private Valuta valuta1;
    private Valuta valuta2;
    private double jumlahUang;
    private Date date;

    public moneyChanger() {
        Date d = new Date();
        this.setDate(d);
    }

    public Valuta getValuta1() {
        return valuta1;
    }

    public void setValuta1(Valuta valuta1) {
        this.valuta1 = valuta1;
    }

    public Valuta getValuta2() {
        return valuta2;
    }

    public void setValuta2(Valuta valuta2) {
        this.valuta2 = valuta2;
    }

    public double getJumlahUang() {
        return jumlahUang;
    }

    public void setJumlahUang(double jumlahUang) {
        this.jumlahUang = jumlahUang;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double hasilTukar() {
        if (getValuta1().getCode().equalsIgnoreCase("IDR")) {
            idr a = (idr) getValuta1();
            if (getValuta2().getCode().equalsIgnoreCase("EUR")) {
                return a.idrToEur(getJumlahUang());
            } else {
                return a.idrToUsd(getJumlahUang());
            }
        } else if (getValuta1().getCode().equalsIgnoreCase("USD")) {
            usd a = (usd) getValuta1();
            if (getValuta2().getCode().equalsIgnoreCase("IDR")) {
                return a.usdToIdr(getJumlahUang());
            } else {
                return a.usdToEur(getJumlahUang());
            }
        } else {
            eur a = (eur) getValuta1();
            if (getValuta2().getCode().equalsIgnoreCase("IDR")) {
                return a.eurToIdr(getJumlahUang());
            } else {
                return a.EurToUsd(getJumlahUang());
            }
        }

    }
}