public class usd extends Valuta {

    public usd(String code) {
        super.setCode(code);
    }

    public double usdToIdr(double usd) {
        return (usd * 14490);
    }

    public double usdToEur(double usd) {
        return (usd * 0.84);
    }

}