public class valutaFactory {
    
    public static Valuta newInstance(int pilih) {
        switch (pilih) {
            case 1:
                return new idr("IDR");
            case 2:
                return new usd("USD");
            case 3:
                return new eur("EUR");
            default:
                System.out.println("Inputan salah");
                return null;
        }
    }
}