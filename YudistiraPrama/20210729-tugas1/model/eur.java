public class eur extends Valuta {

    public eur(String code) {
        super.setCode(code);
    }

    public double eurToIdr(double eur) {
        return (eur * 17000);
    }

    public double EurToUsd(double eur) {
        return (eur * 1.19);
    }

}