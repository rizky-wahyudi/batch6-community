import model.Valuta;
import model.ValutaFactory;
import model.moneyChanger;
import java.util.Scanner;

public class moneyChangerMain {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        moneyChanger mc = new moneyChanger();
        System.out.println("-----SELAMAT DATANG-----");

        System.out.println("");
        System.out.println("Silahkan pilih valuta masukan : "
                + "\n1. IDR"
                + "\n2. USD"
                + "\n3. EUR");
        System.out.print("Masukkan Pilihan : ");
        int valuta1 = sc.nextInt();

        int valuta2 = valuta1;
        while (valuta2 == valuta1) {
            System.out.println("");
            System.out.println("Silahkan pilih valuta keluaran : "
                    + "\n1. IDR"
                    + "\n2. USD"
                    + "\n3. EUR");
            System.out.print("Masukkan Pilihan : ");
            valuta2 = sc.nextInt();

            if (valuta1 == valuta2) {
                System.out.println("Valuta sama. Silahkan pilih kembali : ");
            }
        }
        System.out.println("");
        System.out.print("Silahkan Masukkan Nominal sesuai valuta masukan : ");
        double amount = sc.nextDouble();

        Valuta val1 = ValutaFactory.newInstance(valuta1);
        Valuta val2 = ValutaFactory.newInstance(valuta2);

        mc.setValuta1(val1);
        mc.setValuta2(val2);
        mc.setJumlahUang(amount);

        System.out.println("");
        System.out.println("Uang yang diterima dalam " + mc.getValuta1().getCode());
        System.out.println("Jumlah : " + amount);

        System.out.println("Memproses ...");
        System.out.println("");
        System.out.println("Uang telah diubah dari " + mc.getValuta1().getCode() + " mnjadi " + mc.getValuta2().getCode());
        System.out.println("Jumlah : " + mc.hasilTukar());
        System.out.println("");
        System.out.println("Waktu Transaksi : " + mc.getDate());
        System.out.println("Terima Kasih");

    }
}