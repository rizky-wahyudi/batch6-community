package nexsoft.laundry.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nexsoft.laundry.Entitiy.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

	Employee findById(int id);

	List<Employee> findAll();

	void deleteById(int id);
}
