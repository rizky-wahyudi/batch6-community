package nexsoft.laundry.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nexsoft.laundry.Entitiy.Service;

@Repository
public interface ServiceRepository extends CrudRepository<Service, Integer> {
	Service findById(int id);

	List<Service> findAll();

	void deleteById(int id);
}
