package nexsoft.laundry.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nexsoft.laundry.Entitiy.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

	Address findById(int id);

	List<Address> findAll();

	void deleteById(int id);
}
