package nexsoft.laundry.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import nexsoft.laundry.Entitiy.Address;
import nexsoft.laundry.Entitiy.Customer;
import nexsoft.laundry.Entitiy.Employee;
import nexsoft.laundry.Entitiy.OrderItems;
import nexsoft.laundry.Entitiy.OrderLaudry;
import nexsoft.laundry.Entitiy.Service;
import nexsoft.laundry.Repository.AddressRepository;
import nexsoft.laundry.Repository.CustomerRepository;
import nexsoft.laundry.Repository.EmployeeRepository;
import nexsoft.laundry.Repository.OrderItemsRepository;
import nexsoft.laundry.Repository.OrderRepository;
import nexsoft.laundry.Repository.ServiceRepository;

@RestController
public class LaundryController {

	@Autowired
	private AddressRepository addressRepo;
	@Autowired
	private CustomerRepository customerRepo;
	@Autowired
	private EmployeeRepository employeeRepo;
	@Autowired
	private OrderItemsRepository orderItemRepo;
	@Autowired
	private OrderRepository orderRepo;
	@Autowired
	private ServiceRepository serviceRepo;

	@GetMapping("/")
	public String welcome() {
		return "<html><body>" + "<h1>WELCOME</h1>" + "</body></html>";
	}

	@PostMapping("/address")
	@ResponseStatus
	public Iterable<Address> addAddress(@RequestBody List<Address> listAddress) {
		return addressRepo.saveAll(listAddress);
	}

	@GetMapping("/address")
	public List<Address> getAllAddress() {
		return addressRepo.findAll();
	}

	@GetMapping("/address/{id}")
	public Address getAddressById(@PathVariable(value = "id") int id) {
		return addressRepo.findById(id);
	}

	@PostMapping("/customer/address:{id}")
	@ResponseStatus
	public Customer addCustomer(@RequestBody Customer listCustomer, @PathVariable(value = "id") int id) {
		Address address = addressRepo.findById(id);
		Customer customer = listCustomer;
		customer.setAddressId(address);

		return customerRepo.save(customer);
	}

	@GetMapping("/customer")
	public List<Customer> getAllCustomer() {
		return customerRepo.findAll();
	}

	@GetMapping("/customer/{id}")
	public Customer getCustomerById(@PathVariable(value = "id") int id) {
		return customerRepo.findById(id);
	}

	@PostMapping("/employee/address:{id}")
	@ResponseStatus
	public Employee addCustomer(@RequestBody Employee listEmloyee, @PathVariable(value = "id") int id) {
		Address address = addressRepo.findById(id);
		Employee employee = listEmloyee;
		employee.setAddressId(address);

		return employeeRepo.save(employee);
	}

	@GetMapping("/employee")
	public List<Employee> getAllEmployee() {
		return employeeRepo.findAll();
	}

	@GetMapping("/employee/{id}")
	public Employee getemployeeById(@PathVariable(value = "id") int id) {
		return employeeRepo.findById(id);
	}

	@PostMapping("/service")
	@ResponseStatus
	public Iterable<Service> addServices(@RequestBody List<Service> listServices) {
		return serviceRepo.saveAll(listServices);
	}

	@GetMapping("/service")
	public List<Service> getAllServices() {
		return serviceRepo.findAll();
	}

	@GetMapping("/service/{id}")
	public Service getServiceById(@PathVariable(value = "id") int id) {
		return serviceRepo.findById(id);
	}

	@PostMapping(value = "/order/service:{idService}/employee:{idEmployee}", consumes = "application/json")
	@ResponseStatus
	public String addOrder(@RequestBody OrderLaudry orders, @PathVariable(value = "idService") int idService,
			@PathVariable(value = "idEmployee") int idEmployee) {

		List<OrderItems> order = orders.getOrderItem();
		
		for (OrderItems orderLaudry : order) {
			orderLaudry.setServices(serviceRepo.findById(idService));
			orderItemRepo.save(orderLaudry);
		}
		orders.setUpdateBy(employeeRepo.findById(idEmployee));
		orderRepo.save(orders);
		
		return "Order berhasil ditambahkan";
	}

	@GetMapping("/order")
	public List<OrderLaudry> getAllOrder() {
		return orderRepo.findAll();
	}

	@GetMapping("/order/{id}")
	public OrderLaudry getOrderById(@PathVariable(value = "id") int id) {
		return orderRepo.findById(id);
	}
}
