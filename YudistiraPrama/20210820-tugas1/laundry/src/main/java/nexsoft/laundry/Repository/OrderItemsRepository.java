package nexsoft.laundry.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nexsoft.laundry.Entitiy.OrderItems;

@Repository
public interface OrderItemsRepository extends CrudRepository<OrderItems, Integer> {

	OrderItems findById(int id);

	List<OrderItems> findAll();

	void deleteById(int id);
}
