package nexsoft.laundry.Entitiy;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class OrderLaudry {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@OneToMany(targetEntity = OrderItems.class)
	private List<OrderItems> orderItem;

	private LocalDate orderDate;
	private String status;
	private String paymentStatus;
	private LocalDate lastUpdateDate;

	@ManyToOne(targetEntity = Employee.class)
	private Employee updateBy;

	public OrderLaudry() {

	}

	public OrderLaudry(List<OrderItems> orderItem, LocalDate orderDate, String status, String paymentStatus,
			LocalDate lastUpdateDate) {
		this.orderItem = orderItem;
		this.orderDate = orderDate;
		this.status = status;
		this.paymentStatus = paymentStatus;
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<OrderItems> getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(List<OrderItems> orderItem) {
		this.orderItem = orderItem;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public LocalDate getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDate lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public Employee getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Employee updateBy) {
		this.updateBy = updateBy;
	}

}
