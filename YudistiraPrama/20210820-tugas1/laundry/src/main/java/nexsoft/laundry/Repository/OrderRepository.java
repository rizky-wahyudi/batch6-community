package nexsoft.laundry.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nexsoft.laundry.Entitiy.OrderLaudry;

@Repository
public interface OrderRepository extends CrudRepository<OrderLaudry, Integer> {
	OrderLaudry findById(int id);

	List<OrderLaudry> findAll();

	void deleteById(int id);

}
