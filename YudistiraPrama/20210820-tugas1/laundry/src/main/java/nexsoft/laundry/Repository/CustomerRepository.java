package nexsoft.laundry.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nexsoft.laundry.Entitiy.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {

	Customer findById(int id);

	List<Customer> findAll();

	void deleteById(int id);
}
