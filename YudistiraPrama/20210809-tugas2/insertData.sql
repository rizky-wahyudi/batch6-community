insert into pelanggan (name, phone, birthdate) values
('Adi Saputra', '081999222333', '1980-06-11'),
('Ririn Putri', '082555444990', '1990-12-05');

insert into transportasi (nama, kapasitas) values
('Garuda Indonesia', 200), ('Kereta Kencana', 150), ('Bus Safari', 30);

insert into bagasi (jumlah_muatan)  values
(20), (40), (15);

insert into dokumen (nama_dokumen, nomor_dokumen) values
('KTP', '56569897877877'), ('KTP', '03242344434322');

insert into tiket (nomor_tiket, penumpangId, transportasiId, dokumenId, bagasiId, asal, tujuan, waktu_berangkat, waktu_sampai) values
('GI001', 1, 1, 1, 1, 'Yogyakarta', 'Jakarta', '2021-08-09 15:00:00',  '2021-08-09 17:20:00'),
('KAK50', 2, 2, 2, 2, 'Surabaya', 'Bandunf', '2021-08-10 08:30:00',  '2021-08-10 21:45:00');

select t.nomor_tiket, p.name, tr.nama, d.nama_dokumen, d.nomor_dokumen, b.jumlah_muatan as 'Bagasi (kg)', t.asal, t.tujuan,
t.waktu_berangkat, t.waktu_sampai
from tiket t 
left join pelanggan p on t.penumpangId = p.id
left join transportasi tr on t.transportasiId = tr.id
left join dokumen d on t.dokumenId = d.id
left join bagasi b on t.bagasiId = b.id;
