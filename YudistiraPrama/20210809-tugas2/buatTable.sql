show databases;

USE perjalanan;

create table perjalanan.pelanggan (
id int not null auto_increment,
name varchar(50),
phone varchar(20),
birthDate date,
primary key (id));

create table perjalanan.transportasi (
id int not null auto_increment,
nama varchar(50),
kapasitas int(3),
primary key (id));

create table perjalanan.bagasi (
id int not null auto_increment,
jumlah_muatan int(2),
primary key(id));

create table perjalanan.dokumen (
id int not null auto_increment,
nama_dokumen varchar(20),
nomor_dokumen varchar(20),
primary key(id));

create table perjalanan.tiket (
id int not null auto_increment,
penumpangId int(3),
transportasiId int(3),
dokumenId int(3),
bagasiId int(3),
asal varchar(20),
tujuan varchar(20),
waktu_berangkat datetime,
waktu_sampai datetime,
primary key(id));

alter table tiket add foreign key (penumpangId) references pelanggan(id);
alter table tiket add foreign key (transportasiId) references transportasi(id);
alter table tiket add foreign key (dokumenId) references dokumen(id);
alter table tiket add foreign key (bagasiId) references bagasi(id);

alter table tiket add column nomor_tiket varchar(5);