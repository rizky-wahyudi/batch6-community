import java.util.Date;

public class Order {

    private Date orderDate;
    private String cashier;
    private int orderNumber;
    private String branchId;
    private orderItems[] orderItem;
    private String status;
    private String paymentStatus;

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public orderItems[] getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(orderItems[] orderItem) {
        this.orderItem = orderItem;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public int totalPrice() {
        int total = 0;
        for (int i = 0; i < getOrderItem().length; i++) {
            total = total + getOrderItem()[i].productPice();
        }
        return total;
    }

    public int totalTax() {
        double total = 0;
        double a;
        for (int i = 0; i < getOrderItem().length; i++) {
            a = getOrderItem()[i].getProduct().tax() * getOrderItem()[i].productPice();
            total = total + a;
        }
        return (int) total;
    }

    public int uangBayar() {
        return (totalTax() + totalPrice());
    }

    public void cetakOrder() {
        System.out.println("=====================================");
        System.out.println("------------ DETAIL ORDER -----------");
        System.out.println("Order Date          : " + getOrderDate());
        System.out.println("Order Number        : ORDER - " + getOrderNumber());
        System.out.println("Cashier             : " + getCashier());
        System.out.println("Branch Id           : " + getBranchId());
        System.out.println("Jumlah Order Item   : " + getOrderItem().length);
        System.out.println("Order Item          : ");
        for (int i = 0; i < getOrderItem().length; i++) {
            System.out.println("\tITEM " + (i + 1));
            getOrderItem()[i].cetak();
            System.out.println("");
        }
        System.out.println("Price               : Rp " + totalPrice());
        System.out.println("Total Tax           : Rp " + totalTax());
        System.out.println("Total Price         : Rp " + uangBayar());
        System.out.println("Status              : " + getStatus());
        System.out.println("Payment Status      : " + getPaymentStatus());
        System.out.println("=====================================");
    }

}