public class orderItems {

    private Product product;
    private String orderId;
    private int amount;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int productPice() {
        return (int) (getProduct().sellPrice() * getAmount());

    }

    public void cetak() {
        System.out.println("\tOrder Id        : " + getOrderId());
        System.out.println("\tArticle Number  : " + getProduct().getArticleNumber());
        System.out.println("\tDescription     : " + getProduct().getDescription());
        System.out.println("\tAmount          : " + getAmount());
        System.out.println("\tImage           : " + getProduct().getImage());
        System.out.println("\tExpired Date    : " + getProduct().getExpiredDate());
        System.out.println("\tPrice           : Rp " + productPice());
    }

}