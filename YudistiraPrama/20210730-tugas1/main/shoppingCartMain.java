import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import model.Order;
import model.Product;
import model.orderItems;

public class shoppingCartMain {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //Daftar produk
        Product makanan1 = new Product();
        makanan1.setArticleNumber("1010203040");
        makanan1.setDescription("Sari Roti : Roti Tawar");
        makanan1.setJenis("makanan");
        makanan1.setPurchasePrice(12000);
        makanan1.setImage("sari_roti.jpg");
        makanan1.setExpiredDate("05/08/2021");

        Product makanan2 = new Product();
        makanan2.setArticleNumber("3058795083");
        makanan2.setDescription("Chitato : BBQ");
        makanan2.setJenis("makanan");
        makanan2.setPurchasePrice(15000);
        makanan2.setImage("chitato.jpg");
        makanan2.setExpiredDate("22/01/2023");

        Product sabun1 = new Product();
        sabun1.setArticleNumber("7654009890");
        sabun1.setDescription("NUVO : Full Protection");
        sabun1.setJenis("sabun");
        sabun1.setPurchasePrice(3000);
        sabun1.setImage("nuvo.jpg");
        sabun1.setExpiredDate("20/01/2025");

        Product sabun2 = new Product();
        sabun2.setArticleNumber("1876898098");
        sabun2.setDescription("Lifebuoy : Antibacterial Wash");
        sabun2.setJenis("sabun");
        sabun2.setPurchasePrice(3500);
        sabun2.setImage("lifebuoy.jpg");
        sabun2.setExpiredDate("09/019/2025");

        Product minuman1 = new Product();
        minuman1.setArticleNumber("0098765679");
        minuman1.setDescription("Teh Pucuk : Less Sugar");
        minuman1.setJenis("minuman");
        minuman1.setPurchasePrice(5000);
        minuman1.setImage("tehpucuk.jpg");
        minuman1.setExpiredDate("15/11/2023");

        Product minuman2 = new Product();
        minuman2.setArticleNumber("3090002566");
        minuman2.setDescription("Le Minerale");
        minuman2.setJenis("minuman");
        minuman2.setPurchasePrice(4000);
        minuman2.setImage("leminerale.jpg");
        minuman2.setExpiredDate("02/08/2022");

        //Set nama-nama kasir
        String[] kasir = {"Nadia Saraswati", "Budi Nugraha", "Michael Saja"};
        Random ran = new Random();

        //Mulai Proses order
        ArrayList<Order> listOrder = new ArrayList<>();

        boolean orderProcess = true;
        int orderNumber = 1;
        while (orderProcess == true) {
            System.out.println("----- SELAMAT DATANG -----");
            Order order = new Order();
            int pilihanKasir = ran.nextInt(3);
            order.setCashier(kasir[pilihanKasir]);
            System.out.println("Kasir : " + order.getCashier());
            System.out.println("");

            System.out.println("----- DAFTAR PRODUK -----");
            System.out.println("1. " + makanan1.getDescription() + ""
                    + "\n2. " + makanan2.getDescription() + ""
                    + "\n3. " + sabun1.getDescription() + ""
                    + "\n4. " + sabun2.getDescription() + ""
                    + "\n5. " + minuman1.getDescription() + ""
                    + "\n6. " + minuman2.getDescription());
            System.out.println("");

            ArrayList<Integer> pilihan = new ArrayList<>();
            ArrayList<Integer> jumlah = new ArrayList<>();
            int pilihProduk = 0;
            while (pilihProduk != -1) {
                System.out.print("Masukkan Pilihan Produk ke-" + (pilihProduk + 1) + " yang mau dibeli : ");
                int pilih = sc.nextInt();

                while (pilih > 6 || pilih < 1) {
                    if (pilih == -1) {
                        pilihProduk = -1;
                        break;
                    } else {
                        System.out.println("");
                        System.out.println("Nomor Pilihan salah!");
                        System.out.print("Masukkan Pilihan Produk ke-" + (pilihProduk + 1) + " yang mau dibeli : ");
                        pilih = sc.nextInt();
                    }
                }
                if (pilih == -1) {
                    pilihProduk = -1;
                    break;
                }
                System.out.print("Masukkan Jumlah Produk ke-" + (pilihProduk + 1) + " yang mau dibeli  : ");
                int jum = sc.nextInt();

                pilihan.add(pilih);
                jumlah.add(jum);
                pilihProduk++;
                System.out.println("");
                System.out.println("*Ketik -1 untuk melanjutkan proses");
            }

            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyMMdd");

            orderItems[] listOrderItem = new orderItems[pilihan.size()];
            for (int i = 0; i < pilihan.size(); i++) {
                orderItems orderItem = new orderItems();
                if (pilihan.get(i) == 1) {
                    orderItem.setProduct(makanan1);
                } else if (pilihan.get(i) == 2) {
                    orderItem.setProduct(makanan2);
                } else if (pilihan.get(i) == 3) {
                    orderItem.setProduct(sabun1);
                } else if (pilihan.get(i) == 4) {
                    orderItem.setProduct(sabun2);
                } else if (pilihan.get(i) == 5) {
                    orderItem.setProduct(minuman1);
                } else {
                    orderItem.setProduct(minuman2);
                }
                orderItem.setAmount(jumlah.get(i));
                orderItem.setOrderId("ORDER" + (i + 1) + "-" + format.format(date));
                listOrderItem[i] = orderItem;
            }
            System.out.println("");
            order.setOrderItem(listOrderItem);
            order.setOrderDate(date);
            order.setOrderNumber(orderNumber);

            String branch = "";
            branch = branch + (Math.abs(ran.nextInt()));
            order.setBranchId(branch);
            order.setStatus("Ordered");
            order.setPaymentStatus("Unpaid");

            order.cetakOrder();
            System.out.println("");

            System.out.print("Apakah Akan melakukan pembayaran ? (Y/N) : ");
            String lanjut = sc.next();
            System.out.println("");
            if (lanjut.equalsIgnoreCase("Y")) {
                while (order.getPaymentStatus().equalsIgnoreCase("unpaid")) {
                    System.out.println("*ketik -1 untuk membatalkan proses pembayaran");
                    System.out.print("Silahkan masukkan nominal sesuai dengan total price : Rp ");
                    double bayar = sc.nextDouble();
                    if (bayar == -1) {
                        System.out.println("");
                        System.out.println("Pembayaran dibatalkan...");
                        System.out.println("");
                        System.out.println("Silahkan melakukan pembayaran segera");
                        System.out.println("TERIMA KASIH");
                        break;
                    } else if (bayar == order.uangBayar()) {
                        System.out.println("");
                        System.out.println("Pembayaran berhasil dilakukan!");
                        order.setStatus("Delivered");
                        order.setPaymentStatus("Paid");
                        order.cetakOrder();
                        System.out.println("");
                        Date now = new Date();
                        System.out.println("Tanggal Pembayaran : " + date);
                        System.out.println("TERIMA KASIH");
                        System.out.println("");
                        break;
                    } else {
                        System.out.println("");
                        System.out.println("Nominal yang anda lakukan tidak sesuai!");
                    }
                }

            } else {
                System.out.println("Silahkan melakukan pembayaran segera");
                System.out.println("TERIMA KASIH");
            }

            listOrder.add(order);
            System.out.println("");
            System.out.println("=====================================");
            System.out.println("------------ DAFTAR ORDER -----------");
            for (int i = 0; i < listOrder.size(); i++) {
                System.out.println((i + 1) + ". ORDER " + listOrder.get(i).getOrderNumber());
                System.out.println("Total Item   : " + listOrder.get(i).getOrderItem().length
                        + "\nTotal Price  : Rp " + listOrder.get(i).uangBayar()
                        + "\nStatus       : " + listOrder.get(i).getStatus() + " / " + listOrder.get(i).getPaymentStatus());
                System.out.println("");
            }

            System.out.print("Apakah ingin order lagi? (Y/N) : ");
            String orderLagi = sc.next();

            System.out.println("");
            if (orderLagi.equalsIgnoreCase("y")) {
                System.out.println("Silahkan Melakukan Orderan kembali");
                orderNumber++;
                orderProcess = true;
            } else {
                System.out.println("Orderan Telah Selesai");
                System.out.println("TERIMA KASIH");
                orderProcess = false;
            }

        }

    }
}