public class Person {

    private int umur;

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public static void banding(Person a, Person b, Person c) {
        if (a.getUmur() > b.getUmur()) {
            System.out.println("Umur a lebih besar dari b");
        }
	if (b.getUmur() > c.getUmur()) {
            System.out.println("Umur b lebih besar dari c");
        }
	if (c.getUmur() > 0) {
            System.out.println("Umur c lebih besar dari 0");
        } 

    }
}