/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logistic.main;

import Logistic.controller.Controller;
import Logistic.model.Address;
import Logistic.model.DistanceMapper;
import Logistic.model.Product;
import Logistic.model.Transport;
import Logistic.model.Warehouse;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class LogisticMain {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ArrayList<Product> listProduct = new ArrayList<>();
        listProduct.add(new Product(1, "Meja", 1, 1, 1, 3, 10));
        listProduct.add(new Product(2, "Kursi", 1, 1, 1, 2, 10));
        listProduct.add(new Product(3, "Lemari", 1, 2, 1, 10, 10));
        listProduct.add(new Product(4, "Pintu", 1, 2, 1, 5, 10));
        listProduct.add(new Product(5, "Jendela", 1, 1, 1, 2, 10));

        ArrayList<Address> listAddress = new ArrayList<>();
        listAddress.add(new Address(1, "Jalan Raya Janti", "Bantul", "Yogyakarta", "Indonesia", "-7.798654", "110.409384"));
        listAddress.add(new Address(2, "Ring Road Utara", "Sleman", "Yogyakarta", "Indonesia", "-7.761051", "110.410457"));
        listAddress.add(new Address(3, "Laksda Adisucipto", "Sleman", "Yogyakarta", "Indonesia", "-7.783118", "110.4056474"));
        listAddress.add(new Address(4, "Jalan Nasional III", "Kulon Progo", "Yogyakarta", "Indonesia", "-7.866810", ", 110.175902"));
        listAddress.add(new Address(5, "Jalan Veteran", "Gunung Kidul", "Yogyakarta", "Indonesia", "-7.968060", "110.597653"));

        ArrayList<DistanceMapper> listDistanceMapper = new ArrayList<>();
        listDistanceMapper.add(new DistanceMapper(1, 1, 2, 40.4));
        listDistanceMapper.add(new DistanceMapper(2, 2, 3, 4.4));
        listDistanceMapper.add(new DistanceMapper(3, 3, 4, 9.3));
        listDistanceMapper.add(new DistanceMapper(4, 4, 5, 30.1));
        listDistanceMapper.add(new DistanceMapper(5, 5, 1, 50.2));

        ArrayList<Warehouse> listWarehouse = new ArrayList<>();
        listWarehouse.add(new Warehouse(1, listAddress.get(0), listDistanceMapper.get(0)));
        listWarehouse.add(new Warehouse(2, listAddress.get(1), listDistanceMapper.get(1)));
        listWarehouse.add(new Warehouse(3, listAddress.get(2), listDistanceMapper.get(2)));
        listWarehouse.add(new Warehouse(4, listAddress.get(3), listDistanceMapper.get(3)));
        listWarehouse.add(new Warehouse(5, listAddress.get(4), listDistanceMapper.get(4)));

        ArrayList<Transport> listTransPort = new ArrayList<>();
        listTransPort.add(new Transport(1, "Adi Jaya", 10, 100, "Truck", 10000));
        listTransPort.add(new Transport(2, "Maman Man", 5, 80, "Car", 2000));
        listTransPort.add(new Transport(3, "Samsul Yup", 2, 60, "Motor", 50));

        for (int j = 0; j < listProduct.size(); j++) {
            System.out.println("id : " + listProduct.get(j).getId() + " | Nama : " + listProduct.get(j).getName());
        }
        System.out.println("");
        ArrayList<Product> listKirim = new ArrayList<>();
        System.out.print("Masukkan Jumlah Jenis Product Yang Akan Dikirim: ");
        int jumlahProductKirim = sc.nextInt();
        System.out.println("");
        for (int i = 0; i < jumlahProductKirim; i++) {
            for (int j = 0; j < listProduct.size(); j++) {
                System.out.println("id : " + listProduct.get(j).getId() + " | Nama : " + listProduct.get(j).getName());
            }
            System.out.println("");
            System.out.print("Masukkan Id Product    : ");
            int productId = sc.nextInt();
            Product prod = null;
            for (int k = 0; k < listProduct.size(); k++) {
                if (listProduct.get(k).getId() == productId) {
                    prod = listProduct.get(k);
                    listKirim.add(prod);
                    System.out.println("Nama    : " + prod.getName());
                    System.out.println("Berat   : " + prod.getWeight() + " kg");
                    System.out.println("Jumlah  : " + prod.getAmount());
                    break;
                }
            }
            System.out.print("Masukkan Jumlah Product " + prod.getName() + " yang akan dikirim : ");
            int jumlahProduct = sc.nextInt();
            if (jumlahProduct == prod.getAmount()) {
                listProduct.remove(prod);
            } else {
                listProduct.get(prod.getId() - 1).setAmount(prod.getAmount() - jumlahProduct);
            }
            System.out.println("");
        }
        System.out.println("");
        System.out.println("Lokasi Awal Pengiriman  : ");
        for (int i = 0; i < listAddress.size(); i++) {
            System.out.println("id : " + listAddress.get(i).getId() + " | Lokasi : " + listAddress.get(i).getStreet() + ", " + listAddress.get(i).getDistrict());
        }
        System.out.println("");
        System.out.print("Masukkan Warehouse Pengiriman : ");
        int idLokasiAwal = sc.nextInt();
        System.out.println("");
        System.out.println("Tujuan Pengiriman       : ");
        for (int i = 0; i < listAddress.size(); i++) {
            System.out.println("id : " + listAddress.get(i).getId() + " | Lokasi : " + listAddress.get(i).getStreet() + ", " + listAddress.get(i).getDistrict());
        }
        System.out.println("");
        System.out.print("Masukkan Warehouse Tujuan : ");
        int idLokasiTujuan = sc.nextInt();
        for (int i = 0; i < listKirim.size(); i++) {
            listKirim.get(i).setDeliverAddress(idLokasiTujuan);
        }
        System.out.println("");

        Controller con = new Controller();
        con.kirimBarang(listKirim, listWarehouse, listTransPort, idLokasiAwal, idLokasiTujuan);

    }
}
