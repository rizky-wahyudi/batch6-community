/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logistic.model;

/**
 *
 * @author Asus
 */
public class Warehouse {

    private int id;
    private Address address;
    private DistanceMapper distanceMapper;

    public Warehouse() {
    }

    public Warehouse(int id, Address address, DistanceMapper distanceMapper) {
        this.id = id;
        this.address = address;
        this.distanceMapper = distanceMapper;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public DistanceMapper getDistanceMapper() {
        return distanceMapper;
    }

    public void setDistanceMapper(DistanceMapper distanceMapper) {
        this.distanceMapper = distanceMapper;
    }

}
