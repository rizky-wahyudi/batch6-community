/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logistic.controller;

import Logistic.model.Product;
import Logistic.model.Transport;
import Logistic.model.Warehouse;
import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class Controller {

    public void transportUsing(ArrayList<Transport> listTransport, int jumlah, int berat, double jarak) {
        if (jumlah <= 60 && berat <= 50 && jarak <= 10.0) {
            System.out.println("Diantar Oleh : ");
            System.out.println("Driver      : " + listTransport.get(2).getDriver());
            System.out.println("Kendaraan   : " + listTransport.get(2).getType());
        } else if (berat <= 2000) {
            System.out.println("Diantar Oleh : ");
            System.out.println("Driver      : " + listTransport.get(1).getDriver());
            System.out.println("Kendaraan   : " + listTransport.get(1).getType());
        } else if (berat <= 10000) {
            System.out.println("Diantar Oleh : ");
            System.out.println("Driver      : " + listTransport.get(0).getDriver());
            System.out.println("Kendaraan   : " + listTransport.get(0).getType());
        }
    }

    public void kirimBarang(ArrayList<Product> listKirim, ArrayList<Warehouse> listWarehouse, ArrayList<Transport> listTransport, int awal, int tujuan) {
        System.out.println("========== MENGIRIM PRODUCT ===========");
        System.out.println("Dari Warehouse "+awal);
        System.out.println("Menuju Warehouse "+tujuan);
        int totalProduct = 0;
        int totalWeight = 0;
        for (int i = 0; i < listKirim.size(); i++) {
            totalProduct = totalProduct + listKirim.get(i).getAmount();
            totalWeight = totalWeight + (listKirim.get(i).getAmount() * listKirim.get(i).getWeight());
        }

        System.out.println("Total Product   : " + totalProduct + " product");
        System.out.println("Total Berat     : " + totalWeight + " kg");
        System.out.println("");

        awal = awal - 1;
        tujuan = tujuan - 1;
        double jarak = 0;
        while (awal != tujuan) {
            jarak = listWarehouse.get(awal).getDistanceMapper().getDistance();
            if (awal == 4) {
                System.out.println("Barang dikirim dari Warehouse " + (awal + 1) + " Menuju Warehouse 1");
            }else{
                System.out.println("Barang dikirim dari Warehouse " + (awal + 1) + " Menuju Warehouse " + (awal + 2));
            }
            
            System.out.println("Jarak       : " + jarak + " km");
            System.out.println("");
            this.transportUsing(listTransport, totalProduct, totalWeight, jarak);
            if (awal == 4) {
                awal = 0;
            } else {
                awal = awal + 1;
            }
            System.out.println("");
        }
        System.out.println("========== PRODUCT SAMPAI ===========");
    }
}
