package nexsoft.VendingMachine2.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nexsoft.VendingMachine2.Entity.Item;

@Repository
public interface ItemRepository extends CrudRepository<Item, Integer> {
	Item findById(int id);
	List<Item> findAll();
	void deleteById(int id);
}
