package nexsoft.VendingMachine2.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String code;
	private String jenis;
	private String name;
	private int harga;

	public Item() {

	}

	public Item(String kode, String jenis, String nama, int harga) {
		this.code = kode;
		this.jenis = jenis;
		this.name = nama;
		this.harga = harga;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKode() {
		return code;
	}

	public void setKode(String kode) {
		this.code = kode;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public String getNama() {
		return name;
	}

	public void setNama(String nama) {
		this.name = nama;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public String toString() {
		return "\nKode	: " + getKode() + "" + "\nJenis	: " + getJenis() + "" + "\nNama	: " + getNama() + "" + "\nHarga	: "
				+ getHarga();
	}

}
