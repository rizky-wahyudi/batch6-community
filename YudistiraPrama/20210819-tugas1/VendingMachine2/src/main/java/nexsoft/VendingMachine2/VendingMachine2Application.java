package nexsoft.VendingMachine2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendingMachine2Application {

	public static void main(String[] args) {
		SpringApplication.run(VendingMachine2Application.class, args);
	}

}
