package nexsoft.VendingMachine2.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import nexsoft.VendingMachine2.Entity.Item;
import nexsoft.VendingMachine2.Repository.ItemRepository;

@RestController
public class ItemController {
	@Autowired
	private ItemRepository repo;

	// Home Page
	@GetMapping("/")
	public String welcome() {
		return "<html><body>" + "<h1>WELCOME</h1>" + "</body></html>";
	}

	// Get All Notes
	@GetMapping("/item")
	public List<Item> getAllItem() {
		return repo.findAll();
	}

	// Get the company details by ID
	@GetMapping("/item/id/{id}")
	public Item getItemById(@PathVariable(value = "id") int id) {
		return repo.findById(id);
	}

	@PostMapping("/item")
	@ResponseStatus
	public Iterable<Item> addItem(@RequestBody List<Item> listItem) {
		return repo.saveAll(listItem);
	}

	@DeleteMapping("/item/delete/{id}")
	public void deleteItem(@PathVariable(value = "id") int id) {
		repo.deleteById(id);
	}

	@PutMapping("/item/beli/{id}")
	public String updateItem(@PathVariable int id) {
		Item item = getItemById(id);
		Optional<Item> itemRepo = Optional.ofNullable(repo.findById(id));

		if (!itemRepo.isPresent())
			return "Item Tidak Ada";

		item.setId(id);
		item.setHarga(itemRepo.get().getHarga());
		item.setJenis(itemRepo.get().getJenis());
		item.setKode(itemRepo.get().getKode());
		item.setNama(itemRepo.get().getNama());
		repo.save(item);

		return "Item yang Dibeli " + item.toString();
	}
}
