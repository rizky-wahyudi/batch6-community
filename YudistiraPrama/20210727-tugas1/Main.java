import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("-----LIBURAN-----");
        System.out.print("Msukkan Tujuan    : ");
        String tujuan = sc.nextLine();

        System.out.print("Jumlah Orang      : ");
        int orang = sc.nextInt();

        System.out.print("Jumlah Hari       : ");
        int hari = sc.nextInt();

        Liburan lib = liburanFactory.newInstance(tujuan);

        if (lib instanceof Tokyo) {
            Tokyo t = new Tokyo();
            t.setJumlahOrang(orang);
            t.setJumlahHari(hari);
            t.setBiayaHotel(500000);
            t.setBiayaMakan(200000);
            t.setBiayaTransportasi(2500000);
            System.out.print("Tempat Wisata     : ");
            t.tempatWisataTokyo();
            System.out.println("Biaya Total       : Rp " + t.danaLiburan());
        } else if (lib instanceof Bandung) {
            Bandung t = new Bandung();
            t.setJumlahOrang(orang);
            t.setJumlahHari(hari);
            t.setBiayaHotel(300000);
            t.setBiayaMakan(150000);
            t.setBiayaTransportasi(1000000);
            System.out.print("Tempat Wisata     : ");
            t.tempatWisataBandung();
            System.out.println("Biaya Total       : Rp " + t.danaLiburan());
        } else if (lib instanceof newYork) {
            newYork t = new newYork();
            t.setJumlahOrang(orang);
            t.setJumlahHari(hari);
            t.setBiayaHotel(2000000);
            t.setBiayaMakan(1500000);
            t.setBiayaTransportasi(5000000);
            System.out.print("Tempat Wisata     : ");
            t.tempatWisataNY();
            System.out.println("Biaya Total       : Rp " + t.danaLiburan());
        } else {
            System.out.println("INputan salah");
        }
    }
}