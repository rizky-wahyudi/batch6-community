public class liburanFactory {

    public static Liburan newInstance(String tujuan) {
        if (tujuan.equalsIgnoreCase("Tokyo")) {
            return new Tokyo();
        } else if (tujuan.equalsIgnoreCase("New York")) {
            return new newYork();
        } else if (tujuan.equalsIgnoreCase("Bandung")) {
            return new Bandung();
        } else {
            System.out.println("Inputan salah");
            return null;
        }
    }
}