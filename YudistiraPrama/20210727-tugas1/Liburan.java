public class Liburan {

    private int jumlahOrang;
    private int jumlahHari;
    private int biayaHotel;
    private int biayaTransportasi;
    private int biayaMakan;
    private int biayaJalan;

    public int getJumlahOrang() {
        return jumlahOrang;
    }

    public void setJumlahOrang(int jumlahOrang) {
        this.jumlahOrang = jumlahOrang;
    }

    public int getJumlahHari() {
        return jumlahHari;
    }

    public void setJumlahHari(int jumlahHari) {
        this.jumlahHari = jumlahHari;
    }

    public int getBiayaHotel() {
        return biayaHotel;
    }

    public void setBiayaHotel(int biayaHotel) {
        this.biayaHotel = biayaHotel;
    }

    public int getBiayaTransportasi() {
        return biayaTransportasi;
    }

    public void setBiayaTransportasi(int biayaTransportasi) {
        this.biayaTransportasi = biayaTransportasi;
    }

    public int getBiayaMakan() {
        return biayaMakan;
    }

    public void setBiayaMakan(int biayaMakan) {
        this.biayaMakan = biayaMakan;
    }

    public int getBiayaJalan() {
        return biayaJalan;
    }

    public void setBiayaJalan(int biayaJalan) {
        this.biayaJalan = biayaJalan;
    }

    public int danaLiburan() {
        return ((getBiayaHotel() + getBiayaJalan() + getBiayaMakan() + getBiayaTransportasi()) * getJumlahOrang()) * getJumlahHari();
    }
}