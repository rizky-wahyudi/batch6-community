import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Machine m = new Machine(1000000000);
        MoneyChanger mC = new MoneyChanger();
        int toMoney, fromMoney;
        int moneyChange, moneyPay;
        double money;

        System.out.println("\n:::::::: MONEY CHANGER ::::::::");

        System.out.println("\n===== CONVERT =====");
        System.out.println("Pilih mata uang yang mau dibeli: ");
        System.out.println("1. USD \n2. EUR \n3. IDR");
        System.out.print("Masukkan Angka (1 - 3)  : ");
        toMoney = scan.nextInt();

        do {
            System.out.println("\nPilih mata uang anda: ");
            System.out.println("1. USD \n2. EUR \n3. IDR");
            System.out.print("Masukkan Angka (1 - 3)  : ");
            fromMoney = scan.nextInt();
            if(toMoney == fromMoney){
                System.out.println("Maaf! Mata uang anda dan" 
                + "dan Mata uang yang ingin ditukar sama.");
                System.out.println("Ulangi lagi");
            }
        } while (fromMoney == toMoney);
        m.setFromMoney(fromMoney);
        m.setToMoney(toMoney);

        System.out.print("\nMasukkan Jumlah Uang : " + m.getFromMoney() + " ");
        money = scan.nextInt();
        m.setMoneyPay(money);

        
        mC.setMoneyChange(m.getFromMoney(), m.getToMoney(), money);
        moneyChange = (int)Math.round(mC.getMoneyChange());
        moneyPay = (int)Math.round(m.getMoneyPay());

        System.out.println("\n===== CONFIRMATION =====");
        System.out.println("Jumlah uang     : " + m.getToMoney()+ " " + moneyChange);
        System.out.println("Apakah anda yakin ingin menukarnya?");
        System.out.println("1.Ya \n2.Tidak");
        System.out.print("Masukkan Angka    : ");
        int choice = scan.nextInt();

        if (choice == 1) {
            System.out.println("Masukkan uang!");
            System.out.println("-> Uang Ditukar...");
        } else {
                System.out.println("Sampai Jumpa Lagi!");
                System.exit(2);
        }

        System.out.println("\n===== STRUCK =====");
        System.out.println("Konversi            : " + m.getFromMoney() + " -> " + m.getToMoney());
        System.out.println("Jumlah pembayaran   : " + m.getFromMoney()+ " " + moneyPay);
        System.out.println("Jumlah pengembalian : " + m.getToMoney()+ " " + moneyChange);
        System.out.println("Tanggal Transaksi   : " + m.getDate());

        scan.close();
    }
}