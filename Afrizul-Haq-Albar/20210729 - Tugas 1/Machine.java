import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Machine {
    private double moneyMachine;
    private double moneyPay;
    private String fromMoney;
    private String toMoney;
    private String date;

    Machine() {}

    Machine(int moneyMachine) {
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        this.date = dt.format(dateFormat);
        this.moneyMachine = moneyMachine;
    }

    public String getDate() {
        return date;
    }

    double getMoneyMachine() {
       return this.moneyMachine;
    }

    double getMoneyPay() {
        return this.moneyPay;
    }

    String getFromMoney() {
        return this.fromMoney;
    }

    String getToMoney() {
        return this.toMoney;
    }
    
    void setMoneyPay(double pay) {
        this.moneyPay = pay;
    }

    void setFromMoney(int current) {
        switch (current) {
            case 1:
                this.fromMoney = "USD";
                break;
            case 2:
                this.fromMoney = "EUR";
                break;
            case 3:
                this.fromMoney = "IDR";
                break;
            default:
                System.out.println("Pilihan tidak tersedia.");
                System.exit(2);
        }
    }

    void setToMoney(int convertTo) {
        switch (convertTo) {
            case 1:
                this.toMoney = "USD";
                break;
            case 2:
                this.toMoney= "EUR";
                break;
            case 3:
                this.toMoney = "IDR";
                break;
            default:
                System.out.println("Pilihan tidak tersedia.");
                System.exit(2);
        }
    }
}
