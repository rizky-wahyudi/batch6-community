public class MoneyChanger extends Machine {
    private double moneyChange;

    void setMoneyChange(String from, String to, double money) {
        if ("USD".equalsIgnoreCase(to)) {
            this.moneyChange = convertToUSD(from, money);
        } else if ("IDR".equalsIgnoreCase(to)) {
            this.moneyChange = convertToIDR(from, money);
        } else if ("EUR".equalsIgnoreCase(to)) {
            this.moneyChange = convertToEUR(from, money);
        } else {
            System.out.println("Pilihan tidak tersedia.");
            System.out.println("Proses dibatalkan.");
            System.out.println("Mohon Maaf!");
            System.exit(2);
        }
    }

    double getMoneyChange() {
        return this.moneyChange;
    }

    int convertToIDR(String from, double money) {
        double afterMoney = 0;
        if ("USD".equalsIgnoreCase(from)) {  //USD to IDR
            afterMoney = money * 14490.0;
        } else if ("EUR".equalsIgnoreCase(from)) {   //EUR to IDR
            afterMoney = money * 17000.0;
        }
        return (int)afterMoney;
    }

    double convertToUSD(String from, double money) {
        double afterMoney = 0;
        if ("IDR".equalsIgnoreCase(from)) {  //IDR to USD
            afterMoney =  money * 0.000069;
        } else if ("EUR".equalsIgnoreCase(from)) {   //EUR to USD
            afterMoney = money * 1.19;
        }
        return afterMoney;
    }

    double convertToEUR(String from, double money) {
        double afterMoney = 0;
        if ("IDR".equalsIgnoreCase(from)) {  //IDR to EUR
            afterMoney =  money * 0.000058;
        } else if ("USD".equalsIgnoreCase(from)) {   //USD to EUR
            afterMoney = money * 0.84;
        }
        return afterMoney;
    }
}