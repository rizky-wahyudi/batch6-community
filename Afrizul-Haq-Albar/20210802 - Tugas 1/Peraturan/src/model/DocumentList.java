package model;
import java.util.ArrayList;

public class DocumentList {
    private ArrayList<Document> doc = new ArrayList<Document>();

    public ArrayList<Document> getDoc() {
        fillDoc();
        return this.doc;
    }

    public void fillDoc() {
        //Doc 1
        Menetapkan me = new Menetapkan();
        ArrayList<SubPasal> subPasals = new ArrayList<SubPasal>();
        ArrayList<Pasal> pasal = new ArrayList<Pasal>();
        Pasal p = new Pasal();
        SubPasal sp = new SubPasal();
        sp.setId(1);
        sp.setMessage("Ini Sub Pasal A Pasal 1 Peraturan 1");
        subPasals.add(sp);

        sp = new SubPasal();
        sp.setId(2);
        sp.setMessage("Ini Sub Pasal B Pasal 1 Peraturan 1");
        subPasals.add(sp);
        p.setSubPasals(subPasals);
        pasal.add(p);

        subPasals = new ArrayList<SubPasal>();
        p = new Pasal();
        sp = new SubPasal();
        sp.setId(2);
        sp.setMessage("Ini Sub Pasal A Pasal 2 Peraturan 1");
        subPasals.add(sp);

        sp = new SubPasal();
        sp.setId(2);
        sp.setMessage("Ini Sub Pasal B Pasal 2 Peraturan 1");
        p.setSubPasals(subPasals); 
        pasal.add(p);
        me.setPasal(pasal);

        Mengingat mg = new Mengingat();
        mg.setId(1);
        mg.setMessage("Ini adalah Pengingat Peraturan 1");

        Menimbang mn = new Menimbang();
        ArrayList<MenimbangPoint> menimbangPoints = new ArrayList<MenimbangPoint>();
        MenimbangPoint mp = new MenimbangPoint();
        mp.setId(1);
        mp.setMessage("Ini adalah menimbang point A Peraturan 1");
        menimbangPoints.add(mp);
        
        mp = new MenimbangPoint();
        mp.setId(2);
        mp.setMessage("Ini adalah menimbang point B Peraturan 1");
        menimbangPoints.add(mp);
        mn.setMp(menimbangPoints);

        this.doc.add(new Document(1, mn, mg, me));

        //Doc 2
        me = new Menetapkan();
        subPasals = new ArrayList<SubPasal>();
        pasal = new ArrayList<Pasal>();
        p = new Pasal();
        sp = new SubPasal();
        sp.setId(1);
        sp.setMessage("Bahwa presiden boleh naik masa jabatannya");
        subPasals.add(sp);

        sp = new SubPasal();
        sp.setId(2);
        sp.setMessage("Maksimal masa jabatan diperbolehkan sampai 30 Tahun");
        subPasals.add(sp);
        p.setSubPasals(subPasals);
        pasal.add(p);

        subPasals = new ArrayList<SubPasal>();
        p = new Pasal();
        sp = new SubPasal();
        sp.setId(2);
        sp.setMessage("Ini Sub Pasal A Pasal 2 Peraturan 1");
        subPasals.add(sp);

        sp = new SubPasal();
        sp.setId(2);
        sp.setMessage("PerPres No.212387 Tahun 1969");
        p.setSubPasals(subPasals); 
        pasal.add(p);
        me.setPasal(pasal);

        mg = new Mengingat();
        mg.setId(1);
        mg.setMessage("Pasal 20000A Tahun 2021");

        mn = new Menimbang();
        menimbangPoints = new ArrayList<MenimbangPoint>();
        mp = new MenimbangPoint();
        mp.setId(1);
        mp.setMessage("Naik masa jabatan merupakan hak Presiden");
        menimbangPoints.add(mp);
        
        mp = new MenimbangPoint();
        mp.setId(2);
        mp.setMessage("Naik masa jabatan merupakan kewajiban Presiden");
        menimbangPoints.add(mp);
        mn.setMp(menimbangPoints);

        this.doc.add(new Document(2, mn, mg, me));
    }
}
