package model;

import java.util.ArrayList;

public class Menimbang {
    private ArrayList<MenimbangPoint> mp;

    public void setMp(ArrayList<MenimbangPoint> mp) {
        this.mp = mp;
    }

    public ArrayList<MenimbangPoint> getMp() {
        return mp;
    }
}
