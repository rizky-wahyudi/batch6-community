package model;

import java.util.ArrayList;

public class PeraturanList {
    private ArrayList<Peraturan> peraturans = new ArrayList<Peraturan>();

    public ArrayList<Peraturan> getPeraturans() {
        fillPeraturan();
        return peraturans;
    }

    public void fillPeraturan() {
        Peraturan rule = new Peraturan();
        rule.setIdPeraturan(1);
        rule.setJenis("Undang - Undang");
        rule.setLn(809);
        rule.setTln(1290);
        rule.setNoPeraturan("09/PK/XY");
        rule.setTahunPeraturan("2008");
        rule.setTanggalDiterapkan("2008-11-26");
        rule.setTanggalDiundangkan("2008-11-20");
        rule.setTentang("Hak dan Kepemilikan Warga Negara Indonesia");
        this.peraturans.add(rule);

        rule = new Peraturan();
        rule.setIdPeraturan(2);
        rule.setJenis("Peraturan Presiden");
        rule.setLn(69);
        rule.setTln(390);
        rule.setNoPeraturan("10/PKM");
        rule.setTahunPeraturan("2009");
        rule.setTanggalDiterapkan("2009-05-11");
        rule.setTanggalDiundangkan("2009-05-05");
        rule.setTentang("Kenaikan Masa Jabatan Presiden");
        this.peraturans.add(rule);
    }
}
