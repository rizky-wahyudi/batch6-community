package model;

public class MenimbangPoint {
    private int id;
    private String Message;

    public void setId(int id) {
        this.id = id;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return Message;
    }
}
