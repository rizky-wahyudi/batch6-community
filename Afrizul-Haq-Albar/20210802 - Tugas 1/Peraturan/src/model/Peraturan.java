package model;

public class Peraturan {
    private int idPeraturan;
    private String tanggalDiundangkan;
    private String tanggalDiterapkan;
    private int ln;
    private int tln;
    private String tentang;
    private String jenis;
    private String noPeraturan;
    private String tahunPeraturan;

    public void setIdPeraturan(int idPeraturan) {
        this.idPeraturan = idPeraturan;
    }

    public void setTanggalDiterapkan(String tanggalDiterapkan) {
        this.tanggalDiterapkan = tanggalDiterapkan;
    }

    public void setTanggalDiundangkan(String tanggalDiundangkan) {
        this.tanggalDiundangkan = tanggalDiundangkan;
    }

    public void setTahunPeraturan(String tahunPeraturan) {
        this.tahunPeraturan = tahunPeraturan;
    }

    public void setNoPeraturan(String noPeraturan) {
        this.noPeraturan = noPeraturan;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public void setLn(int ln) {
        this.ln = ln;
    }

    public void setTentang(String tentang) {
        this.tentang = tentang;
    }

    public void setTln(int tln) {
        this.tln = tln;
    }

    public int getIdPeraturan() {
        return idPeraturan;
    }

    public String getJenis() {
        return jenis;
    }

    public int getLn() {
        return ln;
    }

    public String getNoPeraturan() {
        return noPeraturan;
    }

    public String getTahunPeraturan() {
        return tahunPeraturan;
    }

    public String getTanggalDiterapkan() {
        return tanggalDiterapkan;
    }

    public String getTanggalDiundangkan() {
        return tanggalDiundangkan;
    }

    public String getTentang() {
        return tentang;
    }

    public int getTln() {
        return tln;
    }
}
