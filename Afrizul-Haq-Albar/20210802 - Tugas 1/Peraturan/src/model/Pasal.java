package model;

import java.util.ArrayList;

public class Pasal {
    private ArrayList<SubPasal> subPasals;
    
    public void setSubPasals(ArrayList<SubPasal> subPasals) {
        this.subPasals = subPasals;
    }

    public ArrayList<SubPasal> getSubPasals() {
        return subPasals;
    }
}
