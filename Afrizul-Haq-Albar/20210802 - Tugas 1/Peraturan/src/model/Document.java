package model;

public class Document {
    private int idPeraturan;
    private Menetapkan menetapkan;
    private Mengingat mengingat;
    private Menimbang menimbang;

    public Document() {}

    public Document(int idPeraturan, Menimbang mn, Mengingat mg, Menetapkan me) {
        this.idPeraturan = idPeraturan;
        this.menimbang = mn;
        this.mengingat = mg;
        this.menetapkan = me;
    }

    public int getIdPeraturan() {
        return idPeraturan;
    }

    public Menetapkan getMenetapkan() {
        return menetapkan;
    }

    public Mengingat getMengingat() {
        return mengingat;
    }

    public Menimbang getMenimbang() {
        return menimbang;
    }
}
