import java.util.ArrayList;
import java.util.Scanner;

import controller.Display;
import controller.FilterRule;
import model.*;

public class App {
    static ArrayList<Peraturan> ruleList = new PeraturanList().getPeraturans();
    static Scanner scan = new Scanner(System.in);
    static int choice = 0;
    public static void main(String[] args) throws Exception {
        start();
    }

    public static void start() {
        System.out.println("==== SELAMAT DATANG DI Peraturan.com ====");
        do {
            System.out.println("\nMau melakukan apa?");
            System.out.println("1. Lihat Peraturan");
            System.out.println("2. Filter Peraturan");
            System.out.println("0. Keluar");
            
            System.out.print("Masukkan Angka: ");
            choice = scan.nextInt();

            if (choice == 1) {
                displayRule();
                detailPeraturan();
            } else if (choice == 2) {
                setFilter();
            } else if (choice == 0) {
                System.out.println("Terima Kasih! Sampai Jumpa Lagi!");
                System.exit(2);
            } else {
                System.out.println("Salah input!");
            } 
        } while (choice != 0 || choice != 3);

        scan.close();
    }

    public static void displayRule() {
        for(Peraturan list: ruleList) {
            System.out.println("\nID Peraturan        : " + list.getIdPeraturan());
            System.out.println("Jenis Peraturan     : " + list.getJenis());
            System.out.println("Tentang             : " + list.getTentang());
        }
    }

    public static void setFilter() {
        FilterRule f = new FilterRule();
        scan = new Scanner(System.in);
        String r;

        System.out.println("Masukkan Tentang/Jenis/Tahun Peraturan Untuk Memfilter Peraturan!");
        System.out.print("Masukkan Input: ");
        r = scan.nextLine();

        f.addToFilter(r);
        if (f.getFilter().size() > 0) {
            f.filterDisplay();
        } else {
            System.out.println("==============================");
            System.out.println("Data Peraturan Tidak Tersedia!");
            System.out.println("==============================");
        }
    }

    public static void detailPeraturan() {
        Display display = new Display();
        int pId;

        System.out.print("Masukkan ID Peraturan: ");
        pId = scan.nextInt();
        display.setAll(pId);

        if (display.getPeraturan() != null) {
            display.displayRule();
        }
    }
}
