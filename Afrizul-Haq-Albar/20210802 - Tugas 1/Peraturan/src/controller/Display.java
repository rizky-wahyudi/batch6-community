package controller;

import java.util.ArrayList;
import model.*;

public class Display {
    private Peraturan peraturan;
    private Document document;

    public void setAll(int pId) {
        ArrayList<Peraturan> ruleList = new PeraturanList().getPeraturans();
        ArrayList<Document> doc = new DocumentList().getDoc();

        for(Peraturan rule: ruleList) {
            if (rule.getIdPeraturan() == pId) {
                this.peraturan = rule;
                break;
            }
        }

        if (this.peraturan != null) {
            for(Document list: doc) {
                if(list.getIdPeraturan() == this.peraturan.getIdPeraturan()) {
                    this.document = list;
                }
            }
        }
    }

    public Peraturan getPeraturan() {
        return peraturan;
    }

    public Document getDocument() {
        return document;
    }

    public void displayRule() {
        if (getDocument() == null) {
            System.out.println("Data Detail tidak tersedia");
            System.out.println("Jenis Peraturan     : " + getPeraturan().getJenis());
            System.out.println("Nomor Peraturan     : " + getPeraturan().getNoPeraturan());
            System.out.println("Tentang             : " + getPeraturan().getTentang());
            System.out.println("Tgl Diterapkan      : " + getPeraturan().getTanggalDiterapkan());
            System.out.println("Nomor LN            : " + getPeraturan().getLn());
            System.out.println("Nomor TLN           : " + getPeraturan().getTln());
            System.out.println("Tanggal Diundangkan : " + getPeraturan().getTanggalDiundangkan());
        } else {
            System.out.println("\n==================== PERATURAN ====================");
            System.out.println("Jenis Peraturan     : " + getPeraturan().getJenis());
            System.out.println("Nomor Peraturan     : " + getPeraturan().getNoPeraturan());
            System.out.println("Tentang             : " + getPeraturan().getTentang());
            System.out.println("Tgl Diterapkan      : " + getPeraturan().getTanggalDiterapkan());
            System.out.println("Nomor LN            : " + getPeraturan().getLn());
            System.out.println("Nomor TLN           : " + getPeraturan().getTln());
            System.out.println("Tanggal Diundangkan : " + getPeraturan().getTanggalDiundangkan());
            System.out.println("================= DETAIL PERATURAN =================");
            System.out.println("Menimbang           :");
            for(MenimbangPoint mp: document.getMenimbang().getMp()) {
                System.out.println("- " + mp.getMessage());
            }
            System.out.println("Mengingat           :");
            System.out.println("- " + document.getMengingat().getMessage());
            System.out.println("Menetapkan          :");
            for(Pasal p: document.getMenetapkan().getPasal()) {
                int i = 1;
                System.out.println("- Pasal " + i + " : ");
                for(SubPasal sp: p.getSubPasals()) {
                    System.out.println("    - " + sp.getMessage());
                }
                i++;
            }
            System.out.println("================= ================= =================");
        }
    }
}
