package controller;

import java.util.ArrayList;

import model.*;

public class FilterRule {
    private ArrayList<Peraturan> filter = new ArrayList<Peraturan>();

    public void addToFilter(String input) {
        ArrayList<Peraturan> ruleList = new PeraturanList().getPeraturans();

        for(Peraturan rule: ruleList) {
            if (rule.getJenis().toUpperCase().contains(input.toUpperCase())) {
                this.filter.add(rule);
            } else if (rule.getTentang().toUpperCase().contains(input.toUpperCase())) {
                this.filter.add(rule);
            } else if (rule.getTahunPeraturan().equals(input)) {
                this.filter.add(rule);
            }
        }
    }

    public ArrayList<Peraturan> getFilter() {
        return filter;
    }

    public void filterDisplay() {
        System.out.println("\n==================== FILTER ====================");
        for(Peraturan list: getFilter()) {
            System.out.println("\nID Peraturan        : " + list.getIdPeraturan());
            System.out.println("Jenis Peraturan     : " + list.getJenis());
            System.out.println("Tentang             : " + list.getTentang());
            System.out.println("Tahun               : " + list.getTahunPeraturan());
        }
    }
}
