import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("=== Destinasi Wisata ===");
        System.out.println("1.Tokyo \n2.New York \n3.Bandung");
        System.out.print("Masukkan angka: ");
        int choice = scan.nextInt();
        System.out.print("Berapa Orang: ");
        int amountPeople = scan.nextInt();
        System.out.print("Berapa Hari: ");
        int amountDays = scan.nextInt();

        if(choice == 1){
            Tokyo tokyo = new Tokyo(7000);
            tokyo.setAmountPeople(amountPeople);
            tokyo.setAmountDays(amountDays);
            tokyo.showAll();
        }else if(choice == 2){
            NewYork nyc = new NewYork(50);
            nyc.setAmountPeople(amountPeople);
            nyc.setAmountDays(amountDays);
            nyc.showAll();
        }else if(choice == 3){
            Bandung bandung = new Bandung(200000);
            bandung.setAmountPeople(amountPeople);
            bandung.setAmountDays(amountDays);
            bandung.showAll();
        }else{
            System.out.println("Destinasi tidak tersedia");
        }
    }
}