public class Tokyo extends Liburan {
    String[] destination = {"Tokyo Skytree", "Meiji Jingu", "Shinjuku Gyoen National Garden"};
    private int prices;

    Tokyo(int prices){
        this.prices = prices;
    }

    void destinasi(){
        System.out.println("Destinasi: ");
        for(int i = 0; i < destination.length; i++){
            System.out.println("-" + destination[i]);
        }
    }

    int danaLiburan(){
        return this.prices * this.getAmountPeople() * this.getAmountDays();  
    }

    @Override
    void showAll(){
        System.out.println("====== TOKYO ======");
        destinasi();
        System.out.println("\nDana yang dibutuhkan: " + danaLiburan() + " Yen");
        System.out.println("===================");
    }
}
