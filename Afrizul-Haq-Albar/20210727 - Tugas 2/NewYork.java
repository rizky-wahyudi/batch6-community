public class NewYork extends Liburan {
    String[] destination = {"Central Park", "The Metropolitan Museum of Art", "Empire State Building"};
    private int prices;

    NewYork(int prices){
        this.prices = prices;
    }

    void destinasi(){
        System.out.println("Destinasi: ");
        for(int i = 0; i < destination.length; i++){
            System.out.println("-" + destination[i]);
        }
    }

    int danaLiburan(){
        return this.prices * this.getAmountPeople() * this.getAmountDays();  
    }

    @Override
    void showAll(){
        System.out.println("====== New York ======");
        destinasi();
        System.out.println("\nDana yang dibutuhkan: " + danaLiburan() + " $US");
        System.out.println("======================");
    }
}
