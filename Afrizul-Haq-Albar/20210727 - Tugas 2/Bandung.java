public class Bandung extends Liburan {
    String[] destination = {"Tangkuban Perahu", "Jalan Braga", "Gedung Sate"};
    private int prices;

    Bandung(int prices){
        this.prices = prices;
    }

    void destinasi(){
        System.out.println("Destinasi: ");
        for(int i = 0; i < destination.length; i++){
            System.out.println("-" + destination[i]);
        }
    }

    int danaLiburan(){
        return this.prices * this.getAmountPeople() * this.getAmountDays();  
    }

    @Override
    void showAll(){
        System.out.println("====== Bandung ======");
        destinasi();
        System.out.println("\nDana yang dibutuhkan: " + danaLiburan() + " Rupiah");
        System.out.println("=====================");
    }
}
