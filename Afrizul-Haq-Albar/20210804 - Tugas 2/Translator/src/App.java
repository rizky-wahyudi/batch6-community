import java.util.Scanner;

import controller.Distance;
import controller.Temperature;

public class App {
    static Scanner scan = new Scanner(System.in);
    static Temperature temperature = new Temperature();
    static Distance distance = new Distance();
    static int choice = 0;
    public static void main(String[] args) {
        start();
        scan.close();
    }

    public static void start() {
        System.out.println("==== SELAMAT DATANG DI TRANSLATOR ====");
        do {
            System.out.println("\nMau konversi apa?");
            System.out.println("1. Temperature");
            System.out.println("2. Distance");
            System.out.println("0. Keluar");
            
            System.out.print("Masukkan Angka: ");
            choice = scan.nextInt();

            if (choice == 1) {
                convTemperature();
            } else if (choice == 2) {
                convDistance();
            } else if (choice == 0) {
                System.out.println("Sampai Jumpa Lagi!");
                System.exit(2);
            } else {
                System.out.println("Salah input!");
            } 
        } while (true);
    }

    public static void convTemperature() {
        int temp;
        System.out.println("\n========== TEMPERATURE ==========");
        System.out.println("Pilih Konversi apa: ");
        System.out.println("1.Fahrenheit -> Celcius \n2.Celcius -> Fahrenheit ");
        System.out.print("Masukkan Angka: ");
        temp = scan.nextInt();
        if (temp == 1) {
            toCelcius();
        } else if (temp ==2) {
            toFahrenheit();
        } else {
            System.out.println("Inputan salah!");
        }
    }

    public static void convDistance() {
        int dist;
        System.out.println("\n========== DISTANCE ==========");
        System.out.println("Pilih Konversi apa: ");
        System.out.println("1.Km -> Miles \n2.Miles -> Km ");
        System.out.print("Masukkan Angka: ");
        dist = scan.nextInt();
        if (dist== 1) {
            toMiles();
        } else if (dist ==2) {
            toKM();
        } else {
            System.out.println("Inputan salah!");
        }
    }

    public static void toCelcius() {
        temperature = new Temperature();
        double fahrenheit;
        System.out.print("\nMasukkan Derajat Fahrenheit (°F)  : ");
        fahrenheit = scan.nextDouble();
        temperature.setFahrenheit(fahrenheit);
        temperature.translateTemp();

        System.out.println("\n========== CONVERT TEMPERATURE ==========");
        System.out.println("Konversi    : (°F) -> (°C) ");
        System.out.println("Fahrenheit  : " + temperature.getFahrenheit() + " °F");
        System.out.println("Celcius     : " + temperature.getCelcius() + " °C");
    }

    public static void toFahrenheit() {
        temperature = new Temperature();
        double celcius;
        System.out.print("\nMasukkan Derajat Celcius (°C)  : ");
        celcius = scan.nextDouble();
        temperature.setCelcius(celcius);;
        temperature.translateTemp();

        System.out.println("\n========== CONVERT TEMPERATURE ==========");
        System.out.println("Konversi    : (°C) -> (°F) ");
        System.out.println("Celcius     : " + temperature.getCelcius() + " °C");
        System.out.println("Fahrenheit  : " + temperature.getFahrenheit() + " °F");
    }

    public static void toKM() {
        distance = new Distance();
        double miles;
        System.out.print("\nMasukkan Jarak Dalam Miles  : ");
        miles = scan.nextDouble();
        distance.setMiles(miles);
        distance.translateDist();

        System.out.println("========== CONVERT DISTANCE ==========");
        System.out.println("Konversi    : miles -> Km ");
        System.out.println("Miles       : " + distance.getMiles() + " miles");
        System.out.println("Kilometer   : " + distance.getkM() + " Km");
    }

    public static void toMiles() {
        distance = new Distance();
        double kM;
        System.out.print("\nMasukkan Jarak Dalam Kilometer : ");
        kM = scan.nextDouble();
        distance.setkM(kM);
        distance.translateDist();

        System.out.println("========== CONVERT DISTANCE ==========");
        System.out.println("Konversi    : Km -> miles ");
        System.out.println("Kilometer   : " + distance.getkM() + " Km");
        System.out.println("Miles       : " + distance.getMiles() + " miles");
    }
}
