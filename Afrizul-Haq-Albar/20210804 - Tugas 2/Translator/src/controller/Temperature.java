package controller;

public class Temperature {
    private double celcius;
    private double fahrenheit;

    public void setCelcius(double celcius) {
        this.celcius = celcius;
    }

    public void setFahrenheit(double fahrenheit) {
        this.fahrenheit = fahrenheit;
    }

    public double getCelcius() {
        return celcius;
    }

    public double getFahrenheit() {
        return fahrenheit;
    }

    public void translateTemp() {
        if (getCelcius() == 0) {
            this.celcius = (getFahrenheit() - 32) * 5/9;
        } else {
            this.fahrenheit = (getCelcius() * 9/5) + 32;
        }
    }
}
