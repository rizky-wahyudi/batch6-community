package controller;

public class Distance {
    private double kM;
    private double miles;

    public void setMiles(double miles) {
        this.miles = miles;
    }

    public void setkM(double kM) {
        this.kM = kM;
    }

    public double getMiles() {
        return miles;
    }

    public double getkM() {
        return kM;
    }

    public void translateDist() {
        if (getMiles() == 0) {
            this.miles = getkM() * 0.62137;
        } else {
            this.kM = getMiles() * 1.609;
        }
    }
}