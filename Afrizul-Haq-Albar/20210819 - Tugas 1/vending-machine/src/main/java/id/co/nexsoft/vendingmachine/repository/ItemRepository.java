package id.co.nexsoft.vendingmachine.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.vendingmachine.entity.Items;

@Repository
public interface ItemRepository extends CrudRepository<Items, Integer> {
    Items findById(int id);
    List<Items> findAll();
    List<Items> findByLine(String line);

    @Query(value = "SELECT * FROM items i where i.line = :line AND i.slot = :slot", nativeQuery = true)
    Items findByCode(String line, int slot);

    void deleteById(int id);
}
