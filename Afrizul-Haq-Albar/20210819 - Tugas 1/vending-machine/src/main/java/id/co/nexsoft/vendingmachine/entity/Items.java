package id.co.nexsoft.vendingmachine.entity;

import javax.persistence.*;

@Entity
public class Items {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    
    private String line;

    @Column(unique = true)
    private int slot;

    private String name;
    private int price;
    private int quantity;

    public Items() {}

    public Items(String row, int slot, String name, int price, int quantity) {
        this.line = row;
        this.slot = slot;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public String getLine() {
        return line;
    }

    public int getslot() {
        return slot;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
}
