package id.co.nexsoft.vendingmachine.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.co.nexsoft.vendingmachine.entity.Items;
import id.co.nexsoft.vendingmachine.repository.ItemRepository;

@RestController
public class ItemController {
    @Autowired
    private ItemRepository repo;

    @GetMapping("/itemList")
    public List<Items> getAllItem() {
        return repo.findAll();
    }

    @GetMapping("/item/{line}")
    public List<Items> getItemByLine(@PathVariable String line) {
        return repo.findByLine(line);
    }

    @GetMapping("/item/{line}/{slot}")
    public Items getItem(@PathVariable String line, @PathVariable int slot) {
        return repo.findByCode(line, slot);
    } 

    @PostMapping("/addItem")
    @ResponseStatus(HttpStatus.CREATED)
    public Items addItem(@RequestBody Items item) {
        return repo.save(item);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteItem(@PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }

    @PutMapping("/item/{id}")
    public ResponseEntity<Object> updateItem(@RequestBody Items item, @PathVariable int id) {
        Optional<Items> itemRepo = Optional.ofNullable(repo.findById(id));

        if(!itemRepo.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        item.setId(id);
        repo.save(item);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/buyItem/{line}/{slot}")
    public ResponseEntity<Object> BuyItem(@PathVariable String line, @PathVariable int slot) {
        Optional<Items> itemRepo = Optional.ofNullable(repo.findByCode(line, slot));
        Items items = repo.findByCode(line, slot);

        if(!itemRepo.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        if(items.getQuantity() == 0) {
            return ResponseEntity.badRequest().build();
        }

        items.setId(items.getId());
        items.setQuantity(items.getQuantity() - 1);
        repo.save(items);
        return ResponseEntity.noContent().build();
    }
}
