create database perjalanan;
use perjalanan;

create table perjalanan.lugage(
id int not null auto_increment,
weight decimal (6,2),
primary key (id)
);

create table perjalanan.transportation(
id int not null auto_increment,
vehicle varchar(30),
capacity int,
primary key (id)
);

create table perjalanan.passenger(
id int not null auto_increment,
name varchar(50),
phoneNumber varchar(20),
gender enum('male','female'),
birthday date,
lugageId int,
primary key (id),
foreign key (lugageId) references lugage(id)
);

create table perjalanan.document(
id int not null auto_increment,
passengerId int,
name varchar(40),
number varchar(30),
primary key (id),
foreign key(passengerId) references passenger(id)
);

create table perjalanan.ticket(
id int not null auto_increment,
ticketNumber varchar(20),
passengerId int not null,
transportId int not null,
startPoint varchar(30),
destination varchar(30),
departure datetime,
arrival datetime,
primary key (id),
foreign key(passengerId) references passenger(id),
foreign key(transportId) references transportation(id)
);

SELECT
	p.name 						   AS Nama,
    concat(d.name, ': ', d.number) AS Dokumen,
    l.weight					   AS 'Berat Barang (Kg)',
    t.ticketNumber				   AS 'Nomor Tiket',
    tr.vehicle					   AS Kendaraan,
    t.startPoint				   AS Keberangkatan,
    t.destination				   AS Tujuan,
    t.departure					   AS 'Waktu Berangkat',
    t.arrival					   AS 'Waktu Sampai'
FROM
	ticket t
LEFT JOIN
	passenger p ON t.passengerId = p.id
Left JOIN
	document d ON p.id = d.passengerId
Left JOIN
	lugage l ON p.lugageId = l.id
LEFT JOIN    
	transportation tr ON t.transportId = tr.id;