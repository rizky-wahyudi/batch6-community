import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import controller.*;
import model.ProductList;
import model.Product;

public class App {
    static ArrayList<Product> products = new ProductList().getProductList();
    static List<OrderItem> cart = new ArrayList<>();
    static LocalDateTime dt = LocalDateTime.now();
    static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-mm-yyyy");
    static int choice = 0;
    static Scanner scan = new Scanner(System.in); 
    public static void main(String[] args) {
            start();
    }

    public static void start() {
        System.out.println("==== SELAMAT DATANG DI ALPHAMART ====");
        do {
            System.out.println("Mau melakukan apa?");
            System.out.println("1. Belanja");
            System.out.println("2. Lihat Keranjang");
            System.out.println("3. Bayar");
            System.out.println("0. Keluar");
            
            System.out.print("Masukkan Angka: ");
            choice = scan.nextInt();

            if (choice == 1) {
                displayProductList();
                 goShopping();
            } else if (choice == 2) {
                displayCart();
            } else if(choice == 3) {
                if(cart.size() <= 0) {
                    System.out.println("=======================");
                    System.out.println("Keranjang Masih Kosong!");
                    System.out.println("=======================");
                } else {
                    break;
                }
            } else if (choice == 0) {
                System.out.println("Sampai Jumpa Lagi!");
                System.exit(2);
            } else {
                System.out.println("Salah input!");
            } 
        } while (choice != 0 || choice != 3);

        payItem();
        scan.close();
    }

    public static void displayProductList() {
        for(Product list: products) {
            System.out.println("\nID          : " + list.getPid());
            System.out.println("Deskripsi   : " + list.getDescription());
            System.out.println("Harga       : " + (int)list.getSellPrice());
        }
    }

    public static void goShopping() {
        OrderItem saveItem = new OrderItem();

        int item, amount;
        System.out.println("\nBelanja apa?");
        System.out.print("Masukkan ID Barang      : ");
        item = scan.nextInt();
        System.out.print("Masukkan Jumlah Barang  : ");
        amount = scan.nextInt();
        saveItem.setProduct(item, amount);
        if (saveItem.getProduct() != null) {
            cart.add(saveItem);
            System.out.println("============================");
            System.out.println("Barang Sudah Masuk Keranjang");
            System.out.println("============================");
        } else {
            System.out.println("Produk tidak tersedia!");
        }
    }

    public static void displayCart() {
        if (cart.size() <= 0) {
            System.out.println("=======================");
            System.out.println("Keranjang Masih Kosong!");
            System.out.println("=======================");
        } else {
            int totalAll = 0;
            for (OrderItem list: cart) {
                totalAll += list.totalItemPrice();
                System.out.println("\nNama    :" + list.getProduct().getDescription());
                System.out.println("Jumlah  : " + list.getAmount());
                System.out.println("Total   :" + list.totalItemPrice());
            }
            System.out.println("\nTotal Semua: " + totalAll);
            System.out.println("");
        }
    }

    public static void payItem() {
        Order order = new Order();
        int pay, money;

        order.setBranchId("A02");
        order.setCashier("Sari Santi");
        order.setOrderNumber(1);
        order.setCart(cart);
        order.setPaymentStatus("Unpaid");
        order.setStatus("Ordered");
        order.checkout();

        do {
            System.out.println("Masukkan uang Anda Sesuai Total Harga di Akhir!");
            System.out.print("Uang Anda: ");
            pay = scan.nextInt();

            money = pay - order.totalPay();
            if (money == 0) {
                order.setPaymentStatus("Paid");
                order.setStatus("Delivered");
                order.checkout();
                System.out.println("Terima Kasih! Sampai Jumpa Lagi!");
            } else if (money >= 0) {
                order.setPaymentStatus("Paid");
                order.setStatus("Delivered");
                order.checkout();
                System.out.println("Uang Kembali    : " + money);
                System.out.println("Terima Kasih! Sampai Jumpa Lagi!");
            } else {
                System.out.println("Uang Anda Kurang!");
            }
        } while (money < 0);
    }
}