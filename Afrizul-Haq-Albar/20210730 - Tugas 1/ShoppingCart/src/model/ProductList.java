package model;

import java.util.ArrayList;

public class ProductList {
    private ArrayList<Product> productList = new ArrayList<Product>();

    public ArrayList<Product> getProductList() {
        fillProductList();
        return this.productList;
    }

    public void fillProductList() {
        String[] articleNumber = {"F001", "S001", "D001"};
        String[] desc = {"Lays: keripik kentang", "Lervia: Sabun cair dengan campuran susu", "Coca-Cola: Minuman Soda"};
        String[] image = {"Lays.jpg", "Lervia.jpg", "Coca_cola.jpg"};
        int[] price = {10000, 18000, 7500};
        String[] ed = {"21-12-2021", "19-03-2022", "17-8-2021"};

        for(int i = 0; i < articleNumber.length; i++) {
            this.productList.add(new Product((i+1), articleNumber[i], desc[i], image[i], price[i], ed[i]));
        }
    }
}