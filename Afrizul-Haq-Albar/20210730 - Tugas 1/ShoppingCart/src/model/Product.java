package model;

public class Product {
    private int pid;
    private String articleNumber;
    private String description;
    private String image;
    private String expiredDate;
    private int price;

    public Product() {}

    public Product(int id, String aN, String d, String image, int price, String eD){
        this.pid = id;
        this.articleNumber = aN;
        this.description = d;
        this.image = image;
        this.price = price;
        this.expiredDate = eD;
    }

    public double getSellPrice() {
        return getPrice() + (getPrice() * 0.1);
    }

    public double getTax() {
        double tax = 0;
        if ("F001".equalsIgnoreCase(getArticleNumber())) {
            tax = 0.05;
        } else if ("S001".equalsIgnoreCase(getArticleNumber())) {
            tax = 0.07;
        } else if ("D001".equalsIgnoreCase(getArticleNumber())) {
            tax = 0.1;
        }
        return tax;
    }

    public int getPid() {
        return pid;
    }

    public String getArticleNumber() {
        return articleNumber;
    }

    public String getDescription() {
        return description;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public String getImage() {
        return image;
    }

    public int getPrice() {
        return price;
    }
}
