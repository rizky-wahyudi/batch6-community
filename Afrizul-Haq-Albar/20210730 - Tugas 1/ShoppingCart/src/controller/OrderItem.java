package controller;

import java.util.ArrayList;

import model.*;

public class OrderItem {
    private Product product;
    private int orderId;
    private int amount;

    public void setProduct(int pId, int amount) {
        ArrayList<Product> productList = new ProductList().getProductList();
        for(Product prod: productList) {
            if (prod.getPid() == pId) {
                this.product = prod;
                break;
            }
        }
        if (this.product == null) {
            System.out.println("Produk tidak tersedia.");
        } else {
            setAmount(amount);
        }
    }

    public int totalItemPrice() {
        return (int) getProduct().getSellPrice() * getAmount();
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Product getProduct() {
        return product;
    }

    public int getAmount() {
        return amount;
    }

    public int getOrderId() {
        return orderId;
    }
}
