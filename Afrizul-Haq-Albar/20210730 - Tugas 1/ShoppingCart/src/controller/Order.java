package controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Order {
    private String cashier;
    private OrderItem[] cart;
    private String branchId;
    private String paymentStatus;
    private String status;
    private int orderNumber;
    private String date;

    public Order() {
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-mm-yyyy");
        this.date = dt.format(dateFormat);
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public void setCart(List<OrderItem> cart) {
        this.cart = cart.toArray(new OrderItem[cart.size()]);
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCashier() {
        return cashier;
    }

    public OrderItem[] getCart() {
        return cart;
    }

    public String getBranchId() {
        return branchId;
    }

    public String getDate() {
        return date;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public String getStatus() {
        return status;
    }

    public int totalPrice() {
        int total = 0;
        for(OrderItem oi: getCart()) {
            total +=  oi.totalItemPrice();
        }
        return total;
    }

    public int totalTax() {
        double total = 0;
        for(OrderItem oi: getCart()) {
            total += oi.getProduct().getTax() * oi.getProduct().getPrice();
        }
        return (int) total;
    }

    public int totalPay() {
        return (totalPrice() + totalTax());
    }

    public void checkout() {
        System.out.println("==================================================");
        System.out.println("-------------------  ALPHAMART  ------------------");
        System.out.println("==================================================");
        System.out.println("ID Cabang       : " + getBranchId());
        System.out.println("Kasir           : " + getCashier());
        System.out.println("Tanggal         : " + getDate());
        System.out.println("Jumlah Barang   : " + getCart().length);
        System.out.println("Barang          : ");
        for (OrderItem c: getCart()) {
            System.out.println("\nNama Barang : " + c.getProduct().getDescription());
            System.out.println("Nomor seri  : " + c.getProduct().getArticleNumber());
            System.out.println("Harga       : " + (int)c.getProduct().getSellPrice());
            System.out.println("Jumlah      : " + c.getAmount());
            System.out.println("Tanggal Kadaluwarsa :" + c.getProduct().getExpiredDate());
            System.out.println("Total Harga : " + c.totalItemPrice());
        }
        System.out.println("\nHarga           : " + totalPrice());
        System.out.println("Pajak           : " + totalTax());
        System.out.println("Total Harga     : " + totalPay());
        System.out.println("Status Pembayaran : " + getPaymentStatus());
        System.out.println("Status          : " + getStatus());
    }
}
