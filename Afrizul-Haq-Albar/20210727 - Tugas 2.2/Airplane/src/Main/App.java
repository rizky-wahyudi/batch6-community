package Main;

import java.util.Scanner;

import Airplane.Airplane;
import Airplane.Business;
import Airplane.Economy;
import Airplane.FirstClass;


public class App {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);

        String[] route = {"asia - europe", "europe - asia", "europe - US"};
        int plane;
        double total;

        System.out.println("====== PROFIT SITILINK ======");
        System.out.println("Lihat Profit Pesawat: ");
        System.out.println("1. airbus 380 \n2.Boeing 747 \n3.Boeing 787");
        System.out.print("Masukkan Angka: ");
        plane = scan.nextInt();

        for(int i = 0; i < route.length; i++){
            int routePrice = getPriceRoute(i);

            if(plane == 1){
                Airplane airbus = new Airplane("airbus 380", 750, route[i], routePrice);
                airbus.showPlane();
                
                //Economy
                Economy eco = new Economy();
                eco.sumPrice(airbus.getRoutePrice());
                eco.sumPassenger(airbus.getSeat());
                eco.setLaguage(airbus.getLaguage());
                eco.showPlane();

                //Business
                Business bisnis = new Business();
                bisnis.sumPrice(airbus.getRoutePrice());
                bisnis.sumPassenger(airbus.getSeat());
                bisnis.setLaguage(airbus.getLaguage());
                bisnis.showPlane();

                //First Class
                FirstClass first = new FirstClass();
                first.sumPrice(airbus.getRoutePrice());
                first.sumPassenger(airbus.getSeat());
                first.setLaguage(airbus.getLaguage());
                first.showPlane();

                total = eco.sumTotal() + bisnis.sumTotal() + first.sumTotal();
                System.out.println("Total Keuntungan: " + total);
            }else if(plane == 2){
                Airplane airbus = new Airplane("Boeing 747", 529, route[i], routePrice);
                airbus.showPlane();
                
                //Economy
                Economy eco = new Economy();
                eco.sumPrice(airbus.getRoutePrice());
                eco.sumPassenger(airbus.getSeat());
                eco.setLaguage(airbus.getLaguage());
                eco.showPlane();

                //Business
                Business bisnis = new Business();
                bisnis.sumPrice(airbus.getRoutePrice());
                bisnis.sumPassenger(airbus.getSeat());
                bisnis.setLaguage(airbus.getLaguage());
                bisnis.showPlane();

                //First Class
                FirstClass first = new FirstClass();
                first.sumPrice(airbus.getRoutePrice());
                first.sumPassenger(airbus.getSeat());
                first.setLaguage(airbus.getLaguage());
                first.showPlane();

                total = eco.sumTotal() + bisnis.sumTotal() + first.sumTotal();
                System.out.println("Total Keuntungan: " + total);
            }else if(plane == 3){
                Airplane airbus = new Airplane("Boeing 787", 248, route[i], routePrice);
                airbus.showPlane();
                
                //Economy
                Economy eco = new Economy();
                eco.sumPrice(airbus.getRoutePrice());
                eco.sumPassenger(airbus.getSeat());
                eco.setLaguage(airbus.getLaguage());
                eco.showPlane();

                //Business
                Business bisnis = new Business();
                bisnis.sumPrice(airbus.getRoutePrice());
                bisnis.sumPassenger(airbus.getSeat());
                bisnis.setLaguage(airbus.getLaguage());
                bisnis.showPlane();

                //First Class
                FirstClass first = new FirstClass();
                first.sumPrice(airbus.getRoutePrice());
                first.sumPassenger(airbus.getSeat());
                first.setLaguage(airbus.getLaguage());
                first.showPlane();

                total = eco.sumTotal() + bisnis.sumTotal() + first.sumTotal();
                System.out.println("Total Keuntungan: " + total);
            }else{
                System.out.println("Pilihan tidak tersedia");
            }
        }
    }

    static int getPriceRoute(int i){
        int price = 0;
        if(i == 0){
            price = 600;
        }else if(i == 1){
            price = 500;
        }else if(i == 2){
            price = 650;
        }

        return price;
    }
}
