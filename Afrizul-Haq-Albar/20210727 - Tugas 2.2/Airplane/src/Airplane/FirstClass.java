package Airplane;

public class FirstClass extends Airplane {
    private double price;
    private double laguage;
    private double totalSeats;
    
    public double sumPrice(double routePrice){
        return this.price = routePrice * 300/100;
    }

    public double sumPassenger(int seat){
        return this.totalSeats = seat * 10/100;
    }

    public double setLaguage(double laguage){
        return this.laguage = laguage * 250/100;
    }

    public double sumTotal(){
        return getTotalSeat() * getPrice();
    }

    public double getTotalSeat(){
        return this.totalSeats;
    }

    public double getPrice(){
        return this.price;
    }

    public double getLaguage(){
        return this.laguage;
    }

    @Override
    public void showPlane(){
        System.out.println("=== FIRST CLASS ===");
        System.out.println("Kelas           : First Class");
        System.out.println("Harga           : $" + getPrice());
        System.out.println("Maksimal Barang : " + getLaguage());
        System.out.println("Penumpang       : " + getTotalSeat());
        System.out.println("Keuntungan      : $" + sumTotal());    
    }
}