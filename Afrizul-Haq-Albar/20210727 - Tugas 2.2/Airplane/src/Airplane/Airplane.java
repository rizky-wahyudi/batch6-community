package Airplane;

public class Airplane {
    private String name;
    private int seats;
    private String route;
    private int routePrices;
    private double laguages;

    Airplane(){}

    public Airplane(String name, int seat, String route, int routePrice){
        this.name = name;
        this.seats = seat;
        this.route = route;
        this.routePrices = routePrice;
    }
    
    public int getSeat(){
        return this.seats;
    }

    public int getRoutePrice(){
        return this.routePrices;
    }

    public double getLaguage(){
        this.laguages = 20d;
        return this.laguages;
    }

    public void showPlane(){
        System.out.println("\n=== Pesawat ===");
        System.out.println("Nama Pesawat    : " + this.name);
        System.out.println("Total Kursi     : " + getSeat());
        System.out.println("Rute            : " + this.route);
        System.out.println("Harga rute      : $" + getRoutePrice());
    }
}
