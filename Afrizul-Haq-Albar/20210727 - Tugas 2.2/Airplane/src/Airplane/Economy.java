package Airplane;

public class Economy extends Airplane {
    private double price;
    private double laguage;
    private double totalSeats;
    
    public double sumPrice(double routePrice){
        return this.price = routePrice * 100/100;
    }

    public double sumPassenger(int seat){
        return this.totalSeats = seat * 65/100;
    }

    public double setLaguage(double laguage){
        return this.laguage = laguage * 100/100;
    }

    public double sumTotal(){
        return getTotalSeat() * getPrice();
    }

    public double getTotalSeat(){
        return this.totalSeats;
    }

    public double getPrice(){
        return this.price;
    }

    public double getLaguage(){
        return this.laguage;
    }

    @Override
    public void showPlane(){
        System.out.println("=== EKONOMI ===");
        System.out.println("Kelas           : Ekonomi");
        System.out.println("Harga           : $" + getPrice());
        System.out.println("Maksimal Barang : " + getLaguage());
        System.out.println("Penumpang       : " + getTotalSeat());
        System.out.println("Keuntungan      : $" + sumTotal());    
    }
}