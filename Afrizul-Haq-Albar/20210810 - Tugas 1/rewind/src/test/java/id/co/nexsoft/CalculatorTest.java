package id.co.nexsoft;

import static org.junit.Assert.*;

import org.junit.Test;

import id.co.nexsoft.task1.Calculator;

/**
 * Unit test for simple App.
 */
public class CalculatorTest {
    @Test
    public void testAssertTrue1(){
        int result = Calculator.add(5, 6);
        assertTrue((result == 11));
        System.out.println("testAssertTrue 1 Passed");
    }

    @Test
    public void testAssertEqual1() {
        int result = Calculator.add(5, 6);
        assertEquals(11, result);
        System.out.println("testAssertEqual 1 Passed");
    }

    @Test
    public void testAssertFalse1() {
        int result = Calculator.add(5, 6);
        assertFalse((result == 12));;
        System.out.println("testAssertFalse 1 Passed");
    }

    @Test
    public void testAssertFalse1_2() {
        int result = Calculator.add(5, 6);
        assertFalse((result > 12));
        System.out.println("testAssertFalse 1.2 Passed");
    }

    @Test(expected = NumberFormatException.class)
    public void testAssertThrown1() {
        String[] args = {"2", "A"};
        int numOne = Integer.parseInt(args[0]);
        int numTwo = Integer.parseInt(args[1]);
        Calculator.add(numOne, numTwo);
        System.out.println("testAssertThrown1 Passed");
    }


    @Test
    public void testAssertTrue2(){
        int result = Calculator.subtract(10, 6);
        assertTrue((result == 4));
        System.out.println("testAssertTrue 2 Passed");
    }

    @Test
    public void testAssertEqual2() {
        int result = Calculator.subtract(10, 6);
        assertEquals(4, result);
        System.out.println("testAssertEqual 2 Passed");
    }

    @Test
    public void testAssertFalse2() {
        int result = Calculator.subtract(10, 6);
        assertFalse((result == 12));;
        System.out.println("testAssertFalse 2 Passed");
    }

    @Test
    public void testAssertFalse2_2() {
        int result = Calculator.subtract(5, 10);
        assertFalse((result > 0));
        System.out.println("testAssertFalse 2.2 Passed");
    }

    @Test(expected = NumberFormatException.class)
    public void testAssertThrown2() {
        String[] args = {"10", null};
        int numOne = Integer.parseInt(args[0]);
        int numTwo = Integer.parseInt(args[1]);
        Calculator.subtract(numOne, numTwo);
        System.out.println("testAssertThrown 1 Passed");
    }

    @Test
    public void testAssertTrue3(){
        int result = Calculator.multiply(5, 6);
        assertTrue((result == 30));
        System.out.println("testAssertTrue 3 Passed");
    }

    @Test
    public void testAssertEqual3() {
        int result = Calculator.multiply(5, 6);
        assertEquals(30, result);
        System.out.println("testAssertEqual 3 Passed");
    }

    @Test
    public void testAssertFalse3() {
        int result = Calculator.multiply(5, 6);
        assertFalse((result == 36));;
        System.out.println("testAssertFalse 3 Passed");
    }

    @Test
    public void testAssertFalse3_2() {
        int result = Calculator.multiply(5, 6);
        assertFalse((result < 30));
        System.out.println("testAssertFalse 3.2 Passed");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testAssertThrown3() {
        String[] args = {"10"};
        int numOne = Integer.parseInt(args[0]);
        int numTwo = Integer.parseInt(args[1]);
        Calculator.multiply(numOne, numTwo);
        System.out.println("testAssertThrown3 Passed");
    }


    @Test
    public void testAssertTrue4(){
        double result = Calculator.divide(60, 6);
        assertTrue((result == 10));
        System.out.println("testAssertTrue 4 Passed");
    }

    @Test
    public void testAssertEqual4() {
        double result = Calculator.divide(60, 6);
        assertEquals(10, result, 0);
        System.out.println("testAssertEqual 4 Passed");
    }

    @Test
    public void testAssertFalse4() {
        double result = Calculator.divide(60, 6);
        assertFalse((result == 36));;
        System.out.println("testAssertFalse 4 Passed");
    }

    @Test
    public void testAssertFalse4_2() {
        double result = Calculator.divide(60, 6);
        assertFalse((result < 10));
        System.out.println("testAssertFalse 4.2 Passed");
    }

    @Test(expected = ArithmeticException.class)
    public void testAssertThrown4() {
        String[] args = {"10", "0"};
        int numOne = Integer.parseInt(args[0]);
        int numTwo = Integer.parseInt(args[1]);
        Calculator.divide(numOne, numTwo);
        System.out.println("testAssertThrown4 Passed");
    }
}
