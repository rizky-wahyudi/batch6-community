package id.co.nexsoft.task4;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;

public class Main {
    public static void main(String[] args) {
        for(int i = 1; i <= 5; i++) {
            try {
                URL json = new URL("https://jsonplaceholder.typicode.com/todos/" + i);
                BufferedReader in = new BufferedReader(new InputStreamReader(json.openStream()));
                FileWriter file = new FileWriter("file-" + i + ".json");
                StringBuilder str = new StringBuilder();

                String st;
                while ((st = in.readLine()) != null) {
                    System.out.println(st);
                    str.append(st + "\n");
                }
                file.write(str.toString());

                in.close();
                file.close();
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }
}
