package id.co.nexsoft.task3.decorator;

public class ToppingFoodDecorator extends FoodDecorator {
    private String topping;

    public ToppingFoodDecorator(String top, Food f) {
        super(f);
        this.topping = top;
    }

    @Override
    public void serve() {
        decorateFood.serve();
        setTopping(decorateFood);
    }

    public void setTopping(Food f) {
        System.out.println("Add Topping: " + topping);
    }
}
