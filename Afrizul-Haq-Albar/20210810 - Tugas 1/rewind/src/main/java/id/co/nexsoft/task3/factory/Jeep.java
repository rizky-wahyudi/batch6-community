package id.co.nexsoft.task3.factory;

public class Jeep implements Car {
    @Override
    public void getCharacteristic() {
        System.out.println("\n== JEEP CAR ==");
        System.out.println("- Strong");
        System.out.println("- Durable");
        System.out.println("- Dependable");
    }
}
