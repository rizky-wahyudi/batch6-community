package id.co.nexsoft.task3.command;

public class Stock {
    private String name;
    private int quantity;

    public Stock(String n, int q) {
        this.name = n;
        this.quantity = q;
    }

    public void buy() {
        System.out.println("Stock [ Product: " + name + ", Quantity: "
         + quantity + " ] bought");
    }

    public void sell() {
        System.out.println("Stock [ Product: " + name + ", Quantity: "
         + quantity + " ] sold");
    }
}
