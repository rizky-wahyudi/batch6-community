package id.co.nexsoft.task3.observer;

import java.util.ArrayList;
import java.util.List;

public class InstagramAccount implements Observable {
    private List<Observer> observers = new ArrayList<>();
    private String account = "afrizul_albar";

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserver() {
        for(Observer obs: observers) {
            obs.update();
        }
    }

    public void post(String type, String post) {
        System.out.println("New " + type + " From " + account);
        System.out.println("Post: " + post);
    }
}
