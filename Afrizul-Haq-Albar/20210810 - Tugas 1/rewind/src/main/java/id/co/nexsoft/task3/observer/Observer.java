package id.co.nexsoft.task3.observer;

public interface Observer {
    public void update();
}
