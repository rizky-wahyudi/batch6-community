package id.co.nexsoft.task3.observer;

public class InstagramFollower implements Observer {
    private Observable observable;

    public InstagramFollower(Observable obs) {
        this.observable = obs;
    }

    public Observable getObservable() {
        return observable;
    }

    @Override
    public void update() {
        System.out.println("New Post!");
    }
}
