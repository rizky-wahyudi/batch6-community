package id.co.nexsoft.task3.factory;

public class CarFactory {
    public Car getCar(String carType) {
        if (carType.equalsIgnoreCase("SPORT")) {
            return new Sport();
        } else if (carType.equalsIgnoreCase("Jeep")) {
            return new Jeep();
        } else if (carType.equalsIgnoreCase("MPV")) {
            return new MPV();
        } else {
            return null;
        }
    }
}
