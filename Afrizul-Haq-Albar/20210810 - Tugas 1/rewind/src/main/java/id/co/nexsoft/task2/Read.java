package id.co.nexsoft.task2;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Read extends Thread {
    
    @Override
    public void run(){
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        String fileName = s + "/src/main/java/id/co/nexsoft/task2/file";
        try {
            for(int i = 1; i <= 4; i++){
                File file = new File(fileName + "/test" + i + ".txt");
                BufferedReader read = new BufferedReader(new FileReader(file));

                String st;
                while((st = read.readLine()) != null) {
                    System.out.println(st);
                }
                read.close();
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
