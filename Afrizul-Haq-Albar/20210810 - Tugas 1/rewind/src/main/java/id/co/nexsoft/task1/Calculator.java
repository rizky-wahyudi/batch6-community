package id.co.nexsoft.task1;

public class Calculator {
    public static int add(int numOne, int numTwo) {
        int result = numOne + numTwo;
        return result;
    }

    public static int subtract(int numOne, int numTwo) {
        int result = numOne - numTwo;
        return result;
    }

    public static int multiply(int numOne, int numTwo) {
        int result = numOne * numTwo;
        return result;
    }

    public static double divide(int numOne, int numTwo) {
        int result = numOne / numTwo;
        return result;
    }

    public static void main(String[] args) {
        int numOne = Integer.parseInt(args[0]);
        int numTwo = Integer.parseInt(args[1]);
        System.out.println(numOne);
        System.out.println(numTwo);
        add(numOne, numTwo);
        subtract(numOne, numTwo);
        multiply(numOne, numTwo);
        divide(numOne, numTwo);
    }
}