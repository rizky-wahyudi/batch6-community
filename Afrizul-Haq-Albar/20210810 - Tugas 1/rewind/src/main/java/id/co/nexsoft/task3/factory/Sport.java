package id.co.nexsoft.task3.factory;

public class Sport implements Car {
    @Override
    public void getCharacteristic() {
        System.out.println("\n== SPORT CAR ==");
        System.out.println("- Fast");
        System.out.println("- Luxury");
        System.out.println("- Manly");
    }
}
