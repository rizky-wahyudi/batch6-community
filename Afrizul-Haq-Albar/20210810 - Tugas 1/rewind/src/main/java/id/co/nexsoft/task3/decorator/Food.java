package id.co.nexsoft.task3.decorator;

public interface Food {
    void serve();
}
