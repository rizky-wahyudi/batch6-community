package id.co.nexsoft.task3.command;

public class Main {
    public static void main(String[] args) {
        Stock product = new Stock("Chitato", 30);
        Agent agent = new Agent();

        BuyStock buy = new BuyStock(product);
        SellStock sell = new SellStock(product);

        agent.takeOrder(buy);
        agent.takeOrder(sell);

        agent.placeOrder();
    }
}
