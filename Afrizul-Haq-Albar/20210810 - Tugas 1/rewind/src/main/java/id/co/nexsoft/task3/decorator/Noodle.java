package id.co.nexsoft.task3.decorator;

public class Noodle implements Food {
    
    @Override
    public void serve() {
        System.out.println("Food: Noodle");
    }
}
