package id.co.nexsoft.task3.factory;

public class MPV implements Car {
    @Override
    public void getCharacteristic() {
        System.out.println("\n== MPV CAR ==");
        System.out.println("- Flexible");
        System.out.println("- Favorite Car");
        System.out.println("- Affordable Price");
    }
}
