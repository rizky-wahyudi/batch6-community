package id.co.nexsoft.task3.command;

public class BuyStock implements Order {
    private Stock product;

    public BuyStock(Stock prod) {
        this.product = prod;
    }

    public void execute() {
        product.buy();
    }
}
