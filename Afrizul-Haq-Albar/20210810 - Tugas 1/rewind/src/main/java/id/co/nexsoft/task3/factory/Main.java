package id.co.nexsoft.task3.factory;

public class Main {
    public static void main(String[] args) {
        CarFactory factory = new CarFactory();

        Car car = factory.getCar("Sport");
        car.getCharacteristic();

        car = factory.getCar("Jeep");
        car.getCharacteristic();

        car = factory.getCar("MPV");
        car.getCharacteristic();
    }
}
