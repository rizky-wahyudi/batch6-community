package id.co.nexsoft.task3.command;

public class SellStock implements Order {
    private Stock product;

    public SellStock(Stock prod) {
        this.product = prod;
    }

    public void execute() {
        product.sell();
    }
}
