package id.co.nexsoft.task3.observer;

public class Main {
    public static void main(String[] args) {
        InstagramAccount iAccount = new InstagramAccount();
        InstagramFollower ilham = new InstagramFollower(iAccount);
        InstagramFollower dani = new InstagramFollower(iAccount);

        iAccount.addObserver(ilham);
        iAccount.addObserver(dani);

        iAccount.post("Reels", "Dance TokTik");
        iAccount.post("Story", "Photo");
    }
}
