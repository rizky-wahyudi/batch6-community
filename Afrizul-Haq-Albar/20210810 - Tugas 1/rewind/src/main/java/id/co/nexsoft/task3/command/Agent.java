package id.co.nexsoft.task3.command;

import java.util.ArrayList;
import java.util.List;

public class Agent {
    private List<Order> orderList = new ArrayList<>();

    public void takeOrder(Order order) {
        orderList.add(order);
    }

    public void placeOrder() {
        for(Order o: orderList) {
            o.execute();
        }
        orderList.clear();
    }
}
