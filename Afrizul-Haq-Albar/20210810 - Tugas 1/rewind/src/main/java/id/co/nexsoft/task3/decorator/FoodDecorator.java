package id.co.nexsoft.task3.decorator;

public class FoodDecorator implements Food {
    protected Food decorateFood;

    public FoodDecorator(Food food) {
        this.decorateFood = food;
    }

    @Override
    public void serve() {
        decorateFood.serve();
    }
}
