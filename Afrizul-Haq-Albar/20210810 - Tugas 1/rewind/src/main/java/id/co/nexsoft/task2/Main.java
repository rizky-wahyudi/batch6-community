package id.co.nexsoft.task2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        ExecutorService exe = Executors.newFixedThreadPool(2);
        for(int i = 0; i < 1000; i++) {
            Read r = new Read();
            exe.execute(r);
        }
        exe.shutdown();
    }
}
