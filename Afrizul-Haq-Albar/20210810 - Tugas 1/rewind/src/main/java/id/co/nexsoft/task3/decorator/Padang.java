package id.co.nexsoft.task3.decorator;

public class Padang implements Food {
    
    @Override
    public void serve() {
        System.out.println("Food: Padang Rice");
    }
}
