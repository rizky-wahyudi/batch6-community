package id.co.nexsoft.task3.decorator;

public class Main {
    public static void main(String[] args) {
        Food noodle = new Noodle();
        Food toppingNoodle = new ToppingFoodDecorator("Egg", new Noodle());
        Food toppingPadang = new ToppingFoodDecorator("Fish", new Padang());

        System.out.println("Regular Noodle");
        noodle.serve();

        System.out.println("\nTopping Noodle");
        toppingNoodle.serve();

        System.out.println("\nTopping Padang Rice");
        toppingPadang.serve();
    }
}
