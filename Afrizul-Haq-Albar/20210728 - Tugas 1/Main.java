import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] laguage = {"Baju", "Celana", "Laptop"};
        String name, nik, code, destination;

        Airport airport = new Airport("Juanda");
        Pasport paspor = new Pasport("Supri", "1232132132312323", "Surabaya");
        Ticket tck = new Ticket("Supri", "A512l", "Jakarta");
        Laguage lg = new Laguage(laguage, 30);

        System.out.println("==== SELAMAT DATANG DI Bandara" + airport.getAirport() + " ====");

        //X RAY
        System.out.println("\n## X Ray Check Laguage");
        System.out.println("-> Check barang terlebih dahulu");
        System.out.println(".....");
        
        if(lg.checkLaguage() == true) {
            System.out.println("-> Barang Aman. Bisa melanjutkan");
        } else {
            System.out.println("-> Terdapat barang berbahaya. STOP!");
            System.exit(2);
        }

        //Counter check
        System.out.println("\n## Counter Airline");
        System.out.println("- Selamat datang di Counter Airline");
        System.out.print("Masukkan Nama Lengkap : ");
        name = scan.nextLine();
        System.out.print("Masukkan NIK          : ");
        nik = scan.nextLine();

        System.out.println("...");
        if(nik.equalsIgnoreCase(paspor.getNIK()) && name.equalsIgnoreCase(tck.getName())) {
            int pay = 0;
            if(lg.getWeight() > 20){
                pay = (lg.getWeight() - 20) * 1;
            }

            System.out.println("-> Tambahan pembayaran: $" + pay);
            System.out.println("-> Data sudah sesuai. Silahkan lanjut ke Imigrasi");
        } else {
            System.out.println("-> Data tidak sesuai. STOP!");
            System.exit(2);
        }

        //Imigrasi Check
        System.out.println("\n## Imigrasi");
        System.out.println("- Selamat datang di Imigrasi");
        System.out.print("Sebutkan tujuan penerbangan: ");
        destination = scan.nextLine();

        if(destination.equalsIgnoreCase(tck.getDestination()) && nik.equalsIgnoreCase(paspor.getNIK())) {
            System.out.println("-> Data dan Tiket sudah sesuai.");
            System.out.println("-> Silahkan melanjutkan ke waiting room.");
        } else {
            System.out.println("-> Tiket tidak sesuai. STOP!");
            System.exit(2);
        }

        //Last Check
        System.out.println("\n## Waiting Room");
        System.out.println("-> Menunggu di waiting room");
        System.out.println("-> Check terakhir");
        System.out.print("Sebutkan Kode Tiket: ");
        code = scan.nextLine();

        if(code.equalsIgnoreCase(tck.getCode()) && nik.equalsIgnoreCase(paspor.getNIK())) {
            System.out.println("-> Tiket sudah sesuai");
            System.out.println("-> Data sudah sesuai");
            System.out.println("-> Bagasi oke");
        } else {
            System.out.println("-> Tiket tidak sesuai");
            System.exit(2);
        }

        System.out.println("-> Masuk pesawat & Siap liburan!");
        scan.close();
    }
}