public class Pasport extends Airport {
    private String name;
    private String nik;
    private String domicile;

    Pasport(String name, String nik, String domicile) {
        this.name = name;
        this.nik = nik;
        this.domicile = domicile;
    }

    String getName() {
        return this.name;
    }

    String getNIK() {
        return this.nik;
    }

    String getDomicile() {
        return this.domicile;
    }
}
