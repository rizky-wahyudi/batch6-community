public class Airport {
    private String airport;

    Airport() {}

    Airport(String name) {
        airport = name;
    }

    String getAirport() {
        return this.airport;
    }
}
