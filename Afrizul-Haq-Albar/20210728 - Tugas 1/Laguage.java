import java.util.Arrays;

public class Laguage extends Airport {
    private String[] laguage;
    private int weight;
    private String[] prohibited = {"Pisau", "Pistol", "Bom"};

    Laguage(String[] laguage, int weight) {
        this.laguage = laguage;
        this.weight = weight;
    }

    String[] getLaguage() {
        return this.laguage;
    }

    int getWeight() {
        return this.weight;
    }

    boolean checkLaguage() {
        boolean bool = true;
        for(int i = 0; i < prohibited.length; i++){
            if (Arrays.asList(this.laguage).contains(prohibited[i])) {
                bool = false;
                break;
            } else {
                bool = true;
            }
        }
        return bool;
    }
}
