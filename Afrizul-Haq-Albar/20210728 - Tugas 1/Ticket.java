public class Ticket extends Airport {
    private String name;
    private String code;
    private String destination;

    Ticket(String name, String code, String destination) {
        this.name = name;
        this.code = code;
        this.destination = destination;
    }

    String getName() {
        return this.name;
    }

    String getCode() {
        return this.code;
    }

    String getDestination() {
        return this.destination;
    }
}
