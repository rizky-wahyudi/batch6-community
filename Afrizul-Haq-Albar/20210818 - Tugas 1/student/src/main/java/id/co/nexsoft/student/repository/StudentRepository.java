package id.co.nexsoft.student.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.student.entity.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {
    Student findById(int id);
    List<Student> findAll();
    void deleteById(int id);
}
