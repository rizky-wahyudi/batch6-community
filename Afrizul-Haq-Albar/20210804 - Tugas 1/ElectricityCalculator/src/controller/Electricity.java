package controller;

public class Electricity {
    private double resistance;
    private double current;
    private double voltage;
    private double power;

    public void setResistance(String resistance) {
        try {
            if (resistance != null) {
                this.resistance = Double.parseDouble(resistance);
            }
        } catch (NumberFormatException e) {
            this.resistance = 0;
        }
    }

    public void setCurrent(String current) {
        try {
            if (current != null) {
                this.current = Double.parseDouble(current);
            }
        } catch (NumberFormatException e) {
            this.current = 0;
        }
    }

    public void setVoltage(String voltage) {
        try {
            if (voltage != null) {
                this.voltage = Double.parseDouble(voltage);
            }
        } catch (NumberFormatException e) {
            this.voltage = 0;
        }
    }

    public void setPower(String power) {
        try {
            if (power != null) {
                this.power= Double.parseDouble(power);
            }
        } catch (NumberFormatException e) {
            this.power = 0;
        }
    }

    public double getResistance() {
        double r = this.resistance;

        if (r == 0) {
            if (this.voltage > 0 && this.current > 0) {
                r = this.voltage / this.current;
            } else if (this.voltage > 0 && this.power > 0) {
                r = Math.pow(this.voltage, 2) / this.power;
            } else if (this.power > 0 && this.current > 0) {
                r = this.power / Math.pow(this.power, 2);
            }
        }
        return r;
    }

    public double getCurrent() {
        double c = this.current;

        if (c == 0) {
            if (this.voltage > 0 && this.resistance > 0) {
                c = this.voltage / this.resistance;
            } else if (this.voltage > 0 && this.power > 0) {
                c = this.power / this.voltage;
            } else if (this.power > 0 && this.resistance> 0) {
                c = Math.sqrt(this.power / this.resistance);
            }
        }
        return c;
    }

    public double getPower() {
        double p = this.power;

        if (p == 0) {
            if (this.voltage > 0 && this.current > 0) {
                p = this.voltage * this.current;
            } else if (this.voltage > 0 && this.resistance> 0) {
                p = Math.pow(this.voltage, 2) / this.resistance;
            } else if (this.resistance> 0 && this.current > 0) {
                p = Math.pow(this.current, 2) * this.resistance;
            }
        }
        return p;
    }

    public double getVoltage() {
        double v = this.voltage;

        if (v == 0) {
            if (this.resistance > 0 && this.current > 0) {
                v = this.resistance * this.current;
            } else if (this.current > 0 && this.power > 0) {
                v = this.power / this.current;
            } else if (this.power > 0 && this.resistance > 0) {
                v = Math.sqrt(this.power * this.resistance);
            }
        }
        return v;
    }

    public void calculate() {
        System.out.println("\n======= RESULT =======");
        System.out.println("Resistance (R)  : " + getResistance());
        System.out.println("Current (I)     : " + getCurrent());
        System.out.println("Voltage (V)     : " + getVoltage());
        System.out.println("Power (P)       : " + getPower());
    }
}