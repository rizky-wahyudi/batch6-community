import java.util.Scanner;

import controller.Electricity;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        Electricity cal = new Electricity();
        String resistance, current, voltage, power;

        System.out.println("======= ELECTRICITY CALCULATOR =======");
        System.out.println("\n:::(INSTRUCTION):::");
        System.out.println("Anda akan disuruh mengisi beberapa hal:");
        System.out.println("- Resistance (R)");
        System.out.println("- Current (I)");
        System.out.println("- Voltage (V)");
        System.out.println("- Power (P)");
        System.out.println("Isi 2 Nilai Saja Dari Hal Diatas");
        System.out.println("Tekan Enter Untuk Yang Tidak Diisi");

        System.out.println("\n======= CALCULATOR =======");
        System.out.print("Resistance (R): ");
        resistance = scan.nextLine();
        cal.setResistance(resistance);
        System.out.print("Current (I)   : ");
        current = scan.nextLine();
        cal.setCurrent(current);
        System.out.print("Voltage (V)   : ");
        voltage = scan.nextLine();
        cal.setVoltage(voltage);
        System.out.print("Power (P)     : ");
        power = scan.nextLine();
        cal.setPower(power);

        cal.calculate();
        scan.close();
    }
}
