package model;

public class Warehouse {
    private int id;
    private Address address;

    public Warehouse(int id, Address ad) {
        this.id = id;
        this.address = ad;
    }

    public int getId() {
        return id;
    }

    public Address getAddress() {
        return address;
    }
}
