package model;

public class Address {
    private int id;
    private String street;
    private String district;
    private String city;
    private String province;
    private String country;
    private double langitude;
    private double latitude;
    
    public Address(int id, String str, String dtr, String ct, 
    String pr, String ctr, double lg, double lt) {
        this.id = id;
        this.street = str;
        this.district = dtr;
        this.city = ct;
        this.province = pr;
        this.country = ctr;
        this.langitude = lg;
        this.latitude = lt;
    }

    public int getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public String getDistrict() {
        return district;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getCountry() {
        return country;
    }

    public double getLangitude() {
        return langitude;
    }

    public double getLatitude() {
        return latitude;
    }
}