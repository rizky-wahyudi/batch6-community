package model;

public class Product {
    private int id;
    private String name;
    private Address deliverAddress;
    private int width;
    private int height;
    private int depth;
    private int weight;

    public Product(int id, String n, Address dA, 
    int w, int h, int d, int we) {
        this.id = id;
        this.name = n;
        this.deliverAddress = dA;
        this.width = w;
        this.height = h;
        this.depth = d;
        this.weight = we;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Address getDeliverAddress() {
        return deliverAddress;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public int getWidth() {
        return width;
    }
}
