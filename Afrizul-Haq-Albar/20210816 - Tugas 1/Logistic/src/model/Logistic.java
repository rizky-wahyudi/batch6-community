package model;

import java.util.ArrayList;
import java.util.List;

public class Logistic {
    private String resiNumber;
    private List<Warehouse> warehouses = new ArrayList<Warehouse>();
    private List<DistanceMapper> dMappers = new ArrayList<>();
    private Transport transport;
    private Product product;

    public Logistic(String rN, List<Warehouse> wh, List<DistanceMapper> dM, Transport tr, Product pr) {
        this.resiNumber = rN;
        this.warehouses = wh;
        this.dMappers = dM;
        this.transport = tr;
        this.product = pr;
    }

    public String getResiNumber() {
        return resiNumber;
    }

    public List<Warehouse> getWarehouses() {
        return warehouses;
    }

    public List<DistanceMapper> getdMappers() {
        return dMappers;
    }

    public Transport getTransport() {
        return transport;
    }

    public Product getProduct() {
        return product;
    }
}
