package model;

public class Transport {
    private int id;
    private String driver;
    private int size;
    private int speed;
    private String type;
    private int load;

    public Transport(int id, String name, int size, 
    int speed, int load) {
        setType(load);
        this.id = id;
        this.driver = name;
        this.size = size;
        this.speed = speed;
        this.load = load;
    }

    public void setType(int load) {
        if (load <= 50) {
            this.type = "Motor";
        } else if (load <= 2000) {
            this.type = "Mobil";
        } else if (load <= 10000) {
            this.type = "Truk";
        } else {
            System.out.println("Opsi Tidak Tersedia");
        }
    }

    public int getId() {
        return id;
    }

    public String getDriver() {
        return driver;
    }

    public int getSize() {
        return size;
    }

    public int getSpeed() {
        return speed;
    }

    public String getType() {
        return type;
    }

    public int getLoad() {
        return load;
    }
}
