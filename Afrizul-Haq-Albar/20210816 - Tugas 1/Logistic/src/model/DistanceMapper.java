package model;

public class DistanceMapper {
    private int id;
    private Warehouse fromWarehouse;
    private Warehouse toWarehouse;
    private int distance;

    public DistanceMapper (int id, Warehouse fwh, Warehouse twh, int distance) {
        this.id = id;
        this.fromWarehouse = fwh;
        this.toWarehouse = twh;
        this.distance = distance;
    }
    
    public int getId() {
        return id;
    }

    public Warehouse getFromWarehouse() {
        return fromWarehouse;
    }

    public Warehouse getToWarehouse() {
        return toWarehouse;
    }

    public int getDistance() {
        return distance;
    }
}
