package model.db;

import java.util.ArrayList;
import java.util.List;

import model.Address;
import model.DistanceMapper;
import model.Logistic;
import model.Product;
import model.Transport;
import model.Warehouse;

public class DB {
    List<Logistic> logistics = new ArrayList<Logistic>();

    public List<Logistic> getLogistics() {
        initLogistic();
        return logistics;
    }

    public void initLogistic() {
        // Logistic 1
        List<Warehouse> warehouseList = new ArrayList<>();
        List<DistanceMapper> dMappers = new ArrayList<>();
        Address adr = new Address(1, "Jl Pemuda Keren VI", "Waru", "Sidoarjo", 
                        "Jawa Timur", "Indonesia", -30.67890, 15.23456);
        Warehouse wr = new Warehouse(1, adr);
        warehouseList.add(wr);
        adr = new Address(2, "Jl Sangar V", "Singaraja", "Jakarta Utara", 
                "Jakarta", "Indonesia", -31.45385, 16.87465);
        wr = new Warehouse(2, adr);
        warehouseList.add(wr);
        DistanceMapper dm = new DistanceMapper(1, warehouseList.get(0), warehouseList.get(1), 780);
        dMappers.add(dm);
        dm = new DistanceMapper(1, warehouseList.get(1), warehouseList.get(1), 0);
        dMappers.add(dm);
        Product pd = new Product(1, "Stella Smart", adr, 60, 75, 69, 2);
        Transport tr = new Transport(1, "Joko", (pd.getWidth()*pd.getDepth()*pd.getHeight()), 
                        80, pd.getWeight());
        logistics.add(new Logistic("034526", warehouseList, dMappers, tr, pd));
    }
}
