package controller;

import java.util.List;

import model.DistanceMapper;
import model.Logistic;
import model.db.DB;

public class Controller {
    private Logistic logistic;

    public void checkLogistic(String resiNumber) {
        List<Logistic> logistics = new DB().getLogistics();

        for(Logistic log: logistics) {
            if(log.getResiNumber().equals(resiNumber)) {
                this.logistic = log;
                break;
            }
        }
    }

    public Logistic getLogistic() {
        return logistic;
    }

    public void traceLogistic() {
        System.out.println("\n======== SNI LOGISTIC ========");
        System.out.println("Nama Barang     : " + getLogistic().getProduct().getName());
        System.out.println("Berat Barang    : " + getLogistic().getProduct().getWeight() + " Kg");
        System.out.println("Kendaraan       : " + getLogistic().getTransport().getType());
        System.out.println("Driver          : " + getLogistic().getTransport().getDriver());
        System.out.println("Gudang Pengirim : " + getLogistic().getWarehouses().get(0).getAddress().getCity());
        System.out.println("Gudang Penerima : " + getLogistic().getWarehouses().get(getLogistic().getWarehouses().size() - 1)
                            .getAddress().getCity());
        System.out.println("Track           : ");
        for(DistanceMapper dm: getLogistic().getdMappers()) {
            System.out.println("\n======== TRACK ========");
            System.out.println("Posisi Barang       : " + dm.getFromWarehouse().getAddress().getCity());
            System.out.println("Tujuan Selanjutnya  : " + dm.getToWarehouse().getAddress().getCity());
            System.out.println("Jarak   : " + dm.getDistance());
            if (dm.getDistance() == 0) {
                System.out.println("Barang Sudah Sampai!");
            }
        }
    }
}
