import java.util.Scanner;

import controller.Controller;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        int choice;
        String resiNumber;

        System.out.println("==== SNI TRACKING & TRACE ====");
        do {
            System.out.println("\nMau melakukan apa?");
            System.out.println("1. Track Barang");
            System.out.println("0. Keluar");
            
            System.out.print("Masukkan Angka: ");
            choice = scan.nextInt();

            if (choice == 1) {
                scan = new Scanner(System.in);
                Controller controller = new Controller();

                System.out.print("\nMasukkan Nomor Resi: ");
                resiNumber = scan.nextLine();
                controller.checkLogistic(resiNumber);
                if (controller.getLogistic() == null) {
                    System.out.println("==========================");
                    System.out.println("Pengiriman tidak tersedia!");
                    System.out.println("==========================");
                } else {
                    controller.traceLogistic();
                }

            } else if (choice == 0) {
                System.out.println("Terima Kasih! Sampai Jumpa Lagi!");
                System.exit(2);
            } else {
                System.out.println("Salah input!");
            } 
        } while (choice != 0);
        scan.close();
    }    
}
