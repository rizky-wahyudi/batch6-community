-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: stock_management
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `street` varchar(75) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `district` varchar(25) DEFAULT NULL,
  `province` varchar(15) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'Jl Kebagusan Pol',106.82181000,-6.19312500,'Tangerang','Periuk','Banten','Indonesia'),(2,'Jl Krysan',106.55124600,-6.15746900,'Tangerang','Kemis','Banten','Indonesia'),(3,'Jl Majapahit',106.28772300,-6.04293500,'Jakarta Barat','Kesongsong','Jakarta','Indonesia');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distributor`
--

DROP TABLE IF EXISTS `distributor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distributor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `addressId` int NOT NULL,
  `addressType` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addressId` (`addressId`),
  CONSTRAINT `distributor_ibfk_1` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distributor`
--

LOCK TABLES `distributor` WRITE;
/*!40000 ALTER TABLE `distributor` DISABLE KEYS */;
INSERT INTO `distributor` VALUES (1,'PT Makmur Jaya',1,'Rumah'),(2,'PT Intan Indah',2,'Kantor'),(3,'PT JayaBaya',3,'Kantor');
/*!40000 ALTER TABLE `distributor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `artNumber` varchar(15) DEFAULT NULL,
  `imageId` int DEFAULT NULL,
  `sellPrice` int DEFAULT NULL,
  `expiredDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `imageId` (`imageId`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`imageId`) REFERENCES `productimages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'A0928',1,20000,'2022-03-12'),(2,'B8967',2,10000,'2022-01-02'),(3,'B6786',3,15000,'2022-06-19'),(4,'09765',4,17500,'2023-11-21'),(5,'78642',5,7500,'2021-09-09'),(6,'32810',6,27500,'2021-08-11'),(7,'90768',7,6500,'2022-08-19'),(8,'87845',8,5500,'2022-03-19'),(9,'C5631',9,12500,'2022-12-19'),(10,'X4578',10,50000,'2021-06-12');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productdistributor`
--

DROP TABLE IF EXISTS `productdistributor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productdistributor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `distributorId` int NOT NULL,
  `buyPrice` int DEFAULT NULL,
  `stock` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `distributorId` (`distributorId`),
  CONSTRAINT `productdistributor_ibfk_1` FOREIGN KEY (`distributorId`) REFERENCES `distributor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productdistributor`
--

LOCK TABLES `productdistributor` WRITE;
/*!40000 ALTER TABLE `productdistributor` DISABLE KEYS */;
INSERT INTO `productdistributor` VALUES (1,1,20000,15),(2,1,10000,5),(3,2,5000,0),(4,2,7500,0),(5,3,17500,0),(6,3,40000,18),(7,1,10000,5),(8,1,7000,10),(9,2,15000,4),(10,3,60000,4);
/*!40000 ALTER TABLE `productdistributor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productimages`
--

DROP TABLE IF EXISTS `productimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productimages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productimages`
--

LOCK TABLES `productimages` WRITE;
/*!40000 ALTER TABLE `productimages` DISABLE KEYS */;
INSERT INTO `productimages` VALUES (1,'https://cdn.pixabay.com/photo/2018/10/04/14/22/reading-3723751__480.jpg','O\'donut Rasa Coklat'),(2,'https://cdn.pixabay.com/photo/2016/03/23/15/00/ice-cream-1274894__340.jpg','Walls Paddlepop Vanilla'),(3,'https://cdn.pixabay.com/photo/2015/07/12/14/26/coffee-842020__340.jpg','Nescafe Latte'),(4,'https://cdn.pixabay.com/photo/2013/07/13/01/21/popcorn-155602__340.png','Popcorn Oyen Rasa Karamel'),(5,'https://cdn.pixabay.com/photo/2016/03/05/20/09/bake-1238681__340.jpg','Paroti rasa Coklat'),(6,'https://cdn.pixabay.com/photo/2014/10/19/20/59/hamburger-494706__340.jpg','Chitato Rasa Burger'),(7,'https://cdn.pixabay.com/photo/2012/06/27/15/02/candy-50838__480.jpg','Chacha Coklat'),(8,'https://cdn.pixabay.com/photo/2018/03/11/09/08/cookie-3216243__480.jpg','Good Time Cookies'),(9,'https://cdn.pixabay.com/photo/2013/07/12/19/20/popsicle-154587__480.png','Walls Magnum Almond'),(10,'https://cdn.pixabay.com/photo/2013/07/13/09/36/pizza-155771__480.png','Taro rasa Pizza');
/*!40000 ALTER TABLE `productimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productstock`
--

DROP TABLE IF EXISTS `productstock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productstock` (
  `id` int NOT NULL AUTO_INCREMENT,
  `productId` int NOT NULL,
  `stock` int DEFAULT NULL,
  `producDistributorId` int DEFAULT NULL,
  `purchaseDate` date DEFAULT NULL,
  `deliveryDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productId` (`productId`),
  KEY `producDistributorId` (`producDistributorId`),
  CONSTRAINT `productstock_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `productstock_ibfk_2` FOREIGN KEY (`producDistributorId`) REFERENCES `productdistributor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productstock`
--

LOCK TABLES `productstock` WRITE;
/*!40000 ALTER TABLE `productstock` DISABLE KEYS */;
INSERT INTO `productstock` VALUES (1,1,0,1,'2020-01-14','2020-01-20'),(2,2,15,5,'2020-01-17','2020-01-20'),(3,3,15,1,'2020-01-21','2020-01-25'),(4,4,0,1,'2020-01-25','2020-01-28'),(5,5,5,2,'2020-01-25','2020-01-28'),(6,6,10,6,'2020-02-01','2020-02-03'),(7,7,12,4,'2020-02-01','2020-02-03'),(8,8,0,4,'2020-02-04','2020-02-07'),(9,9,0,9,'2020-02-07','2020-02-10'),(10,10,0,10,'2020-02-11','2020-02-13');
/*!40000 ALTER TABLE `productstock` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-07 10:53:47
