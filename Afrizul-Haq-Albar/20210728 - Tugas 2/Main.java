import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        VendingMachine vm = new VendingMachine(50000);
        Item item = new Item();

        String[][] product = item.getProduct();
        String[] row = {"A", "B", "C", "D", "E", "F"};
        String[] slot = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};


        System.out.println("::::::::::: VENDING MACHINE :::::::::::");

        System.out.println("\n======...... PRODUK ......======");
        for(int i = 0; i < 6; i++){
            System.out.println("ROW " + row[i] + ": ");
            for(int j = 0; j < 10; j++){
                System.out.print(" ("+ row[i] + slot[j] + ")");
                System.out.print(product[i][j]);
            }
            System.out.println(" ");
        }

        System.out.println("\n======...... PILIH ......======");
        System.out.print("Masukkan Baris (A - F) : ");
        String rowChoice = scan.next();
        System.out.print("Masukkan Kolom (0 - 9) : ");
        int colChoice = scan.nextInt();

        //Set
        vm.setRow(rowChoice);
        vm.setSlot(colChoice);
        item.setItem(vm.getRow(), vm.getSlot());

        System.out.println("\n======...... BARANG ......======");
        System.out.println("Nama    : " + item.getItemName());
        System.out.println("Harga   : " + item.getPrice());

        System.out.println("\n======...... Pilihan ......======");
        System.out.println("Ingin melanjutkan transaksi ?");
        System.out.println("1. Ya \n2. Tidak");
        System.out.print("Masukkan angka: ");
        int nextChoice = scan.nextInt();
        if (nextChoice == 1) {
            System.out.println("\nOke! Melanjutkan proses.");
        } else {
            System.out.println("Oke! Sampai Jumpa!");
            System.exit(2);
        }

        System.out.println("\n======...... Pembayaran ......======");
        System.out.print("Masukkan uangmu: ");
        int pay = scan.nextInt();
        vm.setMoneyPay(pay);

        int sumMoney = vm.getMoneyPay() - item.getPrice();

        System.out.println("\n======...... CHECKOUT ......======");
        if(vm.getMoneyPay() == item.getPrice()) {
            System.out.println("-> Uang sudah pas");
            System.out.println("-> Pesanan diproses");
            System.out.println("-> " + item.getItemName() + " Telah keluar");
            System.out.println("Terima kasih. Sampai jumpa lagi!");
        } else if (vm.getMoneyPay() > item.getPrice()) {
            if(sumMoney > vm.getMoneyMachine()) {
                System.out.println("-> Mesin tidak punya kembalian");
                System.out.println("-> Pesanan dibatalkan.");
                System.out.println("-> Uang dikembalikan: " + vm.getMoneyPay());
                System.out.println("Mohon Maaf!");
            } else {
                System.out.println("-> Uang lebih besar");
                System.out.println("-> Pesanan diproses");
                System.out.println("-> " + item.getItemName() + " Telah keluar");
                System.out.println("-> Mengembalikan uang: " + (vm.getMoneyPay() - item.getPrice()));
                System.out.println("Terima kasih. Sampai jumpa lagi!");
            }
        } else {
            System.out.println("-> Uang kurang");
            System.out.println("-> Pesanan dibatalkan");
            System.out.println("-> Mengembalikan uang: " + vm.getMoneyPay());
            System.out.println("Mohon Maaf!");
        }

        scan.close();
    }
}