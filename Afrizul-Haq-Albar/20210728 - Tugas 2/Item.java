public class Item extends VendingMachine{
    private String itemName;

    private String[] chips = {"Lays", "Lays", "Lays", "Lays", "Lays", "Chitato", "Chitato", "Chitato", "Chitato",
    "Chitato"};
    private String[] choco = {"Snicker", "Snicker", "Snicker", "Snicker", "Snicker", "KitKat", "KitKat", "KitKat",
    "KitKat", "KitKat"};
    private String[] candies = {"m&m", "m&m", "m&m", "m&m", "m&m", "chacha", "chacha", "chacha", "chacha", 
    "chacha"};
    private String[] water = {"Cleo", "Cleo", "Cleo", "Cleo", "Cleo", "Aqua", "Aqua", "Aqua", "Aqua", "Aqua"};
    private String[] soft = {"Fanta", "Fanta", "Fanta", "Fanta", "Fanta", "Sprite", "Sprite", "Sprite", "Sprite", 
    "Sprite"};
    private String[] tea = {"Nu", "Nu", "Nu", "Nu", "Nu", "frestea", "frestea", "frestea", "frestea", "frestea",
    "frestea"};  
    private String[][] items = {chips, choco, candies, water, soft, tea};

    String[][] getProduct() {
        return this.items;
    }

    String getItemName() {
        return this.itemName;
    }

    String setItem(int row, int slot) {
        return this.itemName = items[row][slot];
    }

    int getPrice() {
        if (getItemName().equals("Lays")) {
            return 9000;
        } else if (getItemName().equals("Chitato")) {
            return 8000;
        } else if (getItemName().equals("Snicker") || getItemName().equals("KitKat")) {
            return 7000;
        } else if (getItemName().equals("m&m")) {
            return 6500;
        } else if (getItemName().equals("chacha")) {
            return 4000;
        } else if (getItemName().equals("Aqua") || getItemName().equals("Cleo")) {
            return 4500;
        } else if (getItemName().equals("Fanta") || getItemName().equals("Sprite")) {
            return 8500;
        } else if (getItemName().equals("Nu")) {
            return 6000;
        } else {
            return 5000;
        }
    }
}
