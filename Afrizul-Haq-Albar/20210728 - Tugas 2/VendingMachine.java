class VendingMachine {
    private int moneyMachine;
    private int moneyPay;
    private int row;
    private int slot;

    VendingMachine() { }

    VendingMachine(int money) {
        this.moneyMachine = money;
    }

    int getMoneyMachine() {
        return this.moneyMachine;
    }

    int getMoneyPay() {
        return this.moneyPay;
    }

    int getRow() {
        return this.row;
    }

    int getSlot() {
        return this.slot;
    }

    void setMoneyPay(int money) {
        this.moneyPay = money;
    }

    void setRow(String row) {
        if (row.equalsIgnoreCase("A")) {
            this.row = 0;
        } else if (row.equalsIgnoreCase("B")) {
            this.row = 1;
        } else if (row.equalsIgnoreCase("C")) {
            this.row = 2;
        }  else if (row.equalsIgnoreCase("D")) {
            this.row = 3;
        }  else if (row.equalsIgnoreCase("E")) {
            this.row = 4;
        } else if (row.equalsIgnoreCase("F")) {
            this.row = 5;
        } else {
            System.out.println("Pilihan tidak tersedia");
            System.exit(2);
        }
    }

    void setSlot(int slot) {
        if (slot > 9 || slot < 0) {
            System.out.println("Pilihan tidak tersedia.");
            System.exit(2);
        } else {
            this.slot = slot;
        }
    }
}