package id.co.nexsoft.laundry.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import id.co.nexsoft.laundry.entity.*;
import id.co.nexsoft.laundry.repository.*;

@RestController
public class Controller {
    @Autowired
    AddressRepository addressRepo;

    @Autowired
    CustomerRepository customerRepo;

    @Autowired
    EmployeeRepository employeeRepo;

    @Autowired
    OrderItemsRepository orderItemRepo;

    @Autowired
    OrderLaundryRepository orderRepo;

    @Autowired
    ServiceRepository serviceRepo;

    @GetMapping("/address")
    public List<Address> getAllAddress() {
        return addressRepo.findAll();
    }

    @GetMapping("/service")
    public List<Service> getAllService() {
        return serviceRepo.findAll();
    }

    @GetMapping("/order")
    public List<OrderLaundry> getAllOrder() {
        return orderRepo.findAll();
    }

    @GetMapping("/order/user={userID}")
    public List<OrderLaundry> getOrderByUserId(@PathVariable int userID) {
        List<OrderLaundry> order = new ArrayList<>();
        for(OrderLaundry o: orderRepo.findAll()) {
            int id = o.getOrderBy().getId();
            if(id == userID) {
                order.add(o);
            }
        }
        return order;
    }

    @GetMapping("/order/{id}")
    public OrderLaundry getOrder(@PathVariable int id) {
        return orderRepo.findById(id);
    }

    @GetMapping("/employee")
    public List<Employee> getAllEmployee() {
        return employeeRepo.findAll();
    }

    @GetMapping("/employee/{id}")
    public Employee getEmployee(@PathVariable int id) {
        return employeeRepo.findById(id);
    }

    @GetMapping("/customer")
    public List<Customer> getAllCustomer() {
        return customerRepo.findAll();
    }

    @GetMapping("/customer/{id}")
    public Customer getCustomer(@PathVariable int id) {
        return customerRepo.findById(id);
    }

    @PostMapping("/service")
    public String addService(@RequestBody Service service) {
        serviceRepo.save(service);
        return "Success added to database!";
    }

    @PostMapping("/employee")
    public String addEmployee(@RequestBody Employee employee) {
        employeeRepo.save(employee);
        return "Success added to database!";
    }

    @PostMapping("/customer")
    public String addCustomer(@RequestBody Customer customer) {
        customerRepo.save(customer);
        return "Success added to database!";
    }

    @PostMapping("/order")
    public String addOrder(@RequestBody OrderLaundry orderLaundry) {
        orderRepo.save(orderLaundry);
        return "Success added to database!";
    } 

    @PutMapping("/order/id={id}/status={status}")
    public String updateOrderStatus(@PathVariable int id, @PathVariable String status) {
        Optional<OrderLaundry> order = Optional.ofNullable(orderRepo.findById(id));
        OrderLaundry o = orderRepo.findById(id);

        if(!order.isPresent()) {
            return "Order Not Found!";
        }

        o.setId(o.getId());
        o.setStatus(status);
        orderRepo.save(o);
        return "Succes Update Status!";
    }
}
