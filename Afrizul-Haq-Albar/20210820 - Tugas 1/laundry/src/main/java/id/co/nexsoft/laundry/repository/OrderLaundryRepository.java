package id.co.nexsoft.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.laundry.entity.OrderLaundry;

@Repository
public interface OrderLaundryRepository extends CrudRepository<OrderLaundry,Integer> {
    List<OrderLaundry> findAll();
    OrderLaundry findById(int id);
}
