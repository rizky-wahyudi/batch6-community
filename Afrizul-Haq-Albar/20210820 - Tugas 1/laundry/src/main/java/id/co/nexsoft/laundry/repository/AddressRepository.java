package id.co.nexsoft.laundry.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.laundry.entity.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {
    List<Address> findAll();
    Address findById(int id);
}
