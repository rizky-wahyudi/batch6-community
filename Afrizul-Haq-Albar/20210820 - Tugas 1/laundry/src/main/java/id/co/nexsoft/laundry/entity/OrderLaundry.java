package id.co.nexsoft.laundry.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class OrderLaundry {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @OneToMany(targetEntity = OrderItems.class, cascade = CascadeType.ALL)
    private List<OrderItems> orderItems;

    @JsonFormat(locale =  "yyyy-mm-dd")
    private LocalDate orderDate;
    private String status;
    private String paymentStatus;

    @JsonFormat(locale =  "yyyy-mm-dd")
    private LocalDate lastUpdateDate;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer orderBy;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee updateBy;

    public OrderLaundry () {}

    public void setId(int id) {
        this.id = id;
    }

    public void setOrderItems(List<OrderItems> orderItems) {
        this.orderItems = orderItems;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public void setLastUpdateDate(LocalDate lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public void setOrderBy(Customer orderBy) {
        this.orderBy = orderBy;
    }

    public void setUpdateBy(Employee updateBy) {
        this.updateBy = updateBy;
    }

    public int getId() {
        return id;
    }

    public List<OrderItems> getOrderItems() {
        return orderItems;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public String getStatus() {
        return status;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public LocalDate getLastUpdateDate() {
        return lastUpdateDate;
    }

    public Customer getOrderBy() {
        return orderBy;
    }

    public Employee getUpdateBy() {
        return updateBy;
    }
}
