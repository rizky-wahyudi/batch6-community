import java.util.*;

class Person {

    public static void main(String[] args) {
        PersonController pc = new PersonController();
        Scanner scan = new Scanner(System.in);

        System.out.print("Masukkan Nilai a: ");
        int a = scan.nextInt();
        System.out.print("Masukkan Nilai b: ");
        int b = scan.nextInt();
        System.out.print("Masukkan Nilai c: ");
        int c = scan.nextInt();

        pc.calculate(a, b, c);
    }
}