import React, {Component} from 'react';

class TableData extends Component {
    constructor(props) {
        super(props)
        this.state = {
            students: [
                { id: 1, name: 'Wasif', age: 21, email: 'wasif@email.com' },
                { id: 2, name: 'Ali', age: 19, email: 'ali@email.com' },
                { id: 3, name: 'Saad', age: 16, email: 'saad@email.com' },
                { id: 4, name: 'Asad', age: 25, email: 'asad@email.com' }
            ]
        }
    }

    render() {
        return (
            <table className="table">
                <thead className="table-dark">
                    <tr>
                    {
                        Object.keys(this.state.students[0]).map((key, index) => {
                            return(
                                <th key={index}>{key.toUpperCase()}</th>
                            )
                        })
                    }
                    </tr>
                </thead>
                <tbody>
                    {
                        this.state.students.map((item, index) => {
                            return (
                                <tr>
                                    <td>{item.id}</td>
                                    <td>{item.name}</td>
                                    <td>{item.age}</td>
                                    <td>{item.email}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        )
    }
}

export default TableData;