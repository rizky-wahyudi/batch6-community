import logo from './logo.svg';
import './App.css';
import TableData from './component/TableData'

function App() {
  return (
    <div className="App">
      <div className="container d-flex flex-column">
        <h2 className="my-4">React Dynamic Table</h2>
        <TableData />
      </div>
    </div>
  );
}

export default App;
