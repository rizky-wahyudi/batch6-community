public class Main {
    public static void main(String[] args){
        //Security Check
        Security1 sc = new Security1();
        System.out.println("===Security Check===");
        sc.checkTicket("A12320210728");
        sc.returnTicket();
        sc.returnPasport();
        sc.checkPasport("TA12345");
        sc.checkLaguage(15);

        //Imigration Check
        Imigration im = new Imigration();
        System.out.println("\n===Imigration Check===");
        im.checkTicket("A12320210728", sc.returnTicket());
        im.checkPasport("TA12345", sc.returnPasport());

        //Waiting room check
        WaitingRoom wr = new WaitingRoom();
        System.out.println("\n===Waiting Room Check===");
        wr.checkTicket("A12320210728", sc.returnTicket());
        wr.checkPasport("TA12345", sc.returnPasport());
        wr.checkLaguage(3);
        System.out.println("\nYou can enter the plane");

        //Another input
        //Security Check
        System.out.println("\n===ANOTHER INPUT===");
        Security1 sc1 = new Security1();
        System.out.println("===Security Check===");
        sc1.checkTicket("A12345678928");
        sc1.returnTicket();
        sc1.returnPasport();
        sc1.checkPasport("TA54321");
        sc1.checkLaguage(25);

        //Imigration Check
        Imigration im1 = new Imigration();
        System.out.println("\n===Imigration Check===");
        im1.checkTicket("A12345678928", sc1.returnTicket());
        im1.checkPasport("TA54321", sc1.returnPasport());

        //Waiting room check
        WaitingRoom wr1 = new WaitingRoom();
        System.out.println("\n===Waiting Room Check===");
        wr1.checkTicket("A12345678928", sc1.returnTicket());
        wr1.checkPasport("TA54321", sc1.returnPasport());
        wr1.checkLaguage(6);
        System.out.println("\nYou can enter the plane");
    }
}