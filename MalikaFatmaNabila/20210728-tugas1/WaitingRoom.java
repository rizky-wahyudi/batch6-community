public class WaitingRoom{
    public void checkTicket(String ticket, String securityTicket){
        if(ticket == securityTicket && ticket.charAt(0) == 'A'){
            System.out.println("Your ticket is verified in waiting room");
        } else{
            System.out.println("Ticket not found or you enter the wrong waiting room");
            System.exit(0);
        }
    }
    public void checkPasport(String pasport, String securityPasport){
        if(pasport == securityPasport){
            System.out.println("Your pasport is verified in waiting room");
        } else{
            System.out.println("Incorrect Pasport");
            System.exit(0);
        }
    }
    public void checkLaguage(int laguage){
        if(laguage <= 5){
            System.out.println("Hand luggage according to the hand luggage criteria");
        } else{
            System.out.println("Your luggage is over capacity. You have to reduce it");
        }
    }
}