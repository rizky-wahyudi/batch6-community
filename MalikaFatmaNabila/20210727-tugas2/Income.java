public class Income{
    protected Double income;
    public void setFirstClassIncome(Double planeSeat, Double price){
        this.income = ((10.0/100.0) * planeSeat) * (300/100 * price);
    }
    public Double getFirstClassIncome(){
        return income;
    }
    public void setBusinessIncome(Double planeSeat, Double price){
        this.income = ((25.0 / 100.0) * planeSeat) * (225/100 *price);
    }
    public Double getBusinessIncome(){
        return income;
    }
    public void setEconomyIncome(Double planeSeat, Double price){
        this.income = ((65.0 / 100.0) * planeSeat) * price;
    }
    public Double getEconomyIncome(){
        return income;
    }
}