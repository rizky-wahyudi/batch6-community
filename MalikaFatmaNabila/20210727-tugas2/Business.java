public class Business {
    public Double getBusiness(){
        Price price = new Price();
        PlaneSeat ps = new PlaneSeat();
        Income in = new Income();
        //Income from airbus
        in.setBusinessIncome(ps.getAirbus380(), price.getEuropeAsia());
        Double total = in.getBusinessIncome();
        in.setBusinessIncome(ps.getAirbus380(), price.getAsiaEurope());
        Double total1 = in.getBusinessIncome();
        in.setBusinessIncome(ps.getAirbus380(), price.getEuropeUS());
        Double total2 = in.getBusinessIncome();
        //Income from boeing747
        in.setBusinessIncome(ps.getBoeing747(), price.getEuropeAsia());
        Double total3 = in.getBusinessIncome();
        in.setBusinessIncome(ps.getBoeing747(), price.getAsiaEurope());
        Double total4 = in.getBusinessIncome();
        in.setBusinessIncome(ps.getBoeing747(), price.getEuropeUS());
        Double total5 = in.getBusinessIncome();
        //income from boeing787
        in.setBusinessIncome(ps.getBoeing787(), price.getEuropeAsia());
        Double total6 = in.getBusinessIncome();
        in.setBusinessIncome(ps.getBoeing787(), price.getAsiaEurope());
        Double total7 = in.getBusinessIncome();
        in.setBusinessIncome(ps.getBoeing787(), price.getEuropeUS());
        Double total8 = in.getBusinessIncome();
        Double totalBusiness = total+total1+total2+total3+total4+total5+total6+total7+total8;
        System.out.println("\n===PENDAPATAN DARI BUSINESS CLASS===");
        System.out.println("- Dari pesawat airbus 380 semua tujuan : $" + (total+total1+total2));
        System.out.println("- Dari pesawat boeing 747 semua tujuan : $" + (total3+total4+total5));
        System.out.println("- Dari pesawat boeing 787 semua tujuan : $" + (total6+total7+total8));
        System.out.println("Total Pendapatan : $" + totalBusiness);
        return totalBusiness;
    }
} 