public class Price{
    protected Double europeAsia = 500.0;
    protected Double asiaEurope = 600.0;
    protected Double europeUS = 650.0;
    public Double getEuropeAsia(){
        return this.europeAsia;
    }
    public Double getAsiaEurope(){
        return this.asiaEurope;
    }
    public Double getEuropeUS(){
        return this.europeUS;
    }
}