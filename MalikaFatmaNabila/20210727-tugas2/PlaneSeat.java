public class PlaneSeat{
    protected Double airbus380 = 750.0;
    protected Double boeing747 = 529.0;
    protected Double boeing787 = 248.0;
    public Double getAirbus380(){
        return this.airbus380;
    }
    public Double getBoeing747(){
        return this.boeing747;
    }
    public Double getBoeing787(){
        return this.boeing787;
    }
}