public class Economy {
    public Double getEconomy(){
        Price price = new Price();
        PlaneSeat ps = new PlaneSeat();
        Income in = new Income();
        //Income from airbus
        in.setEconomyIncome(ps.getAirbus380(), price.getEuropeAsia());
        Double total = in.getEconomyIncome();
        in.setEconomyIncome(ps.getAirbus380(), price.getAsiaEurope());
        Double total1 = in.getEconomyIncome();
        in.setEconomyIncome(ps.getAirbus380(), price.getEuropeUS());
        Double total2 = in.getEconomyIncome();
        //Income from boeing747
        in.setEconomyIncome(ps.getBoeing747(), price.getEuropeAsia());
        Double total3 = in.getEconomyIncome();
        in.setEconomyIncome(ps.getBoeing747(), price.getAsiaEurope());
        Double total4 = in.getEconomyIncome();
        in.setEconomyIncome(ps.getBoeing747(), price.getEuropeUS());
        Double total5 = in.getEconomyIncome();
        //income from boeing787
        in.setEconomyIncome(ps.getBoeing787(), price.getEuropeAsia());
        Double total6 = in.getEconomyIncome();
        in.setEconomyIncome(ps.getBoeing787(), price.getAsiaEurope());
        Double total7 = in.getEconomyIncome();
        in.setEconomyIncome(ps.getBoeing787(), price.getEuropeUS());
        Double total8 = in.getEconomyIncome();
        Double totalEconomy = total+total1+total2+total3+total4+total5+total6+total7+total8;
        System.out.println("===PENDAPATAN DARI ECONOMY CLASS===");
        System.out.println("- Dari pesawat airbus 380 semua tujuan : $" + (total+total1+total2));
        System.out.println("- Dari pesawat boeing 747 semua tujuan : $" + (total3+total4+total5));
        System.out.println("- Dari pesawat boeing 787 semua tujuan : $" + (total6+total7+total8));
        System.out.println("Total Pendapatan : $" + totalEconomy);
        return totalEconomy;
    }
} 