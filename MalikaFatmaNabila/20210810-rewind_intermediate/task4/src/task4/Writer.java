package task4;

import java.io.FileWriter;
import java.io.IOException;

public class Writer {
	public static void writer(String url, int id) {
		String content = Reader.getUrlContents(url);
		try {
		      FileWriter myWriter = new FileWriter("file-" +id+ ".json");
		      myWriter.write(content);
		      myWriter.close();
		      System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
}