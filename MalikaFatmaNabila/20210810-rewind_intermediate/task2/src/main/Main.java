package main;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import reader.Reader;

public class Main {

	public static void main(String[] args) {
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
		Reader read = new Reader();
		for (int i= 0; i<1000; i++) {
			executor.execute(read);
		}
    }
}