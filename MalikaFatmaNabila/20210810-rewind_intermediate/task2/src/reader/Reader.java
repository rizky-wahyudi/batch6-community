package reader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Reader implements Runnable{
	File source;
	File destination;
	static File dir = new File("D:\\NexSOFT\\Bootcamp\\Code\\batch6-community\\MalikaFatmaNabila\\20210810-rewind_intermediate\\task2\\testFile");
    static File[] files = dir.listFiles();
	
	@Override
    public void run() {
        for (File file : files) {
        	readFromFile(file.getAbsolutePath());
        }
        
	}
	
	static String readFromFile(String filename){
        StringBuffer content = new StringBuffer();
        try {
            String text;
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            while((text = reader.readLine())!=null){
                 content.append(text);
                 content.append("\n");
                   
             }
             System.out.println(filename + " => " + content);
             reader.close();
             
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    return content.toString(); 
    }
}
