package decorator;

public abstract class ShapeColor implements Shape{
	protected Shape coloredShape;
	
	public ShapeColor(Shape coloredShape) {
		this.coloredShape = coloredShape;
	}
	
	public void draw() {
		coloredShape.draw();
	}
}
