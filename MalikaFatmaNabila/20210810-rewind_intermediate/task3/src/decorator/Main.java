package decorator;

public class Main {

	public static void main(String[] args) {
		Shape circle = new Circle();
		Shape triangle = new Triangle();
		Shape redCircle = new RedShapeColor(new Circle());
		Shape redTriangle = new RedShapeColor(new Triangle());
		System.out.println("Shape Without Color");
		circle.draw();
		triangle.draw();
		System.out.println("\nShape With Color");
		redCircle.draw();
		System.out.println("");
		redTriangle.draw();
	}

}
