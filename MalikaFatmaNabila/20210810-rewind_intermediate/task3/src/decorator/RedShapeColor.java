package decorator;

public class RedShapeColor extends ShapeColor {
	public RedShapeColor(Shape coloredShape) {
		super(coloredShape);
	}
	
	@Override
	public void draw() {
		coloredShape.draw();
		setRedColor(coloredShape);
	}
	
	private void setRedColor(Shape coloredShape) {
		System.out.println("Color : Red");
	}
}
