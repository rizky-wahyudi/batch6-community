package tests;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
	
	@Test
	public void testAdd1() {
		int result = Calculator.add(1, 2);
		assertEquals(3, result);
	}
	
	@Test
	public void tesAdd2() {
		assertTrue(Calculator.add(1,  1) == 2);
	}
	
	@Test
	public void testAdd3() {
		assertFalse(Calculator.add(5,2) == 8);
	}
	
	@Test
	public void testAdd4() {
		assertFalse(Calculator.add(5,2) == 7);
	}
	
	@Test
	public void testAdd5() {
		String[] a = {"1","m"};
        assertThrows(NumberFormatException.class, ()-> Calculator.main(a));
	}
	
	@Test
	public void testSubtract1() {
		int result = Calculator.subtract(5, 2);
		assertEquals(3, result);
	}
	
	@Test
	public void testSubtract2() {
		assertTrue(Calculator.subtract(1,  1) == 0);
	}
	
	@Test
	public void testSubtract3() {
		assertFalse(Calculator.subtract(10,2) == 8);
	}
	
	@Test
	public void testSubtract4() {
		assertFalse(Calculator.subtract(5,2) == 7);
	}
	
	@Test
	public void testSubtract5() {
		String[] a = {"1","m"};
        assertThrows(NumberFormatException.class, ()-> Calculator.main(a));
	}
	
	@Test
	public void testDivide1() {
		int result = Calculator.divide(6, 2);
		assertEquals(3, result);
	}
	
	@Test
	public void testDivide2() {
		assertTrue(Calculator.divide(1,  1) == 1);
	}
	
	@Test
	public void testDivide3() {
		assertFalse(Calculator.divide(10,2) == 5);
	}
	
	@Test
	public void testDivide4() {
		assertFalse(Calculator.divide(10,2) == 7);
	}
	
	@Test
	public void testDividet5() {
		String[] a = {"1","m"};
        assertThrows(NumberFormatException.class, ()-> Calculator.main(a));
	}
	
	@Test
	public void testDividet6() {
        assertThrows(ArithmeticException.class, ()-> Calculator.divide(2, 0));
	}
	
	@Test
	public void testMultiply1() {
		int result = Calculator.multiply(6, 2);
		assertEquals(12, result);
	}
	
	@Test
	public void testMultiply2() {
		assertTrue(Calculator.multiply(5,  4) == 20);
	}
	
	@Test
	public void testMultiply3() {
		assertFalse(Calculator.multiply(10, 2) == 20);
	}
	
	@Test
	public void testMultiply4() {
		assertFalse(Calculator.multiply(10, 2) == 7);
	}
	
	@Test
	public void testMultiply5() {
		String[] a = {"1","m"};
        assertThrows(NumberFormatException.class, ()-> Calculator.main(a));
	}
}
