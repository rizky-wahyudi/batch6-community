import java.util.Scanner;
public class Liburan{
    public static void main(String[] args){
        Tokyo tok = new Tokyo();
        NewYork ny = new NewYork();
        Bandung bd = new Bandung();
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan tujuan anda : ");
        String tujuan = input.nextLine();
        System.out.print("Masukkan jumlah wisatawan : ");
        int jumlah = input.nextInt();
        System.out.print("Masukkan lama wisata (dalam hari) : ");
        int hari = input.nextInt();

        if(tujuan.equalsIgnoreCase("Tokyo")){
            tok.getDestination();
            tok.setTiketPesawat(5000000, jumlah);
            tok.setPenginapan(400000, jumlah, hari);
            tok.setTransportasi(600000, jumlah, hari);
            tok.setOther(500000, jumlah, hari);
            tok.setTotal();
            System.out.println("Perkiraan Biaya Total : Rp" + tok.getTotal() + ",-");
        }else if(tujuan.equalsIgnoreCase("New York")){
            ny.getDestination();
            ny.setTiketPesawat(10000000, jumlah);
            ny.setPenginapan(1000000, jumlah, hari);
            ny.setTransportasi(1500000, jumlah, hari);
            ny.setOther(2000000, jumlah, hari);
            ny.setTotal();
            System.out.println("Perkiraan Biaya Total : Rp" + ny.getTotal() + ",-");
        }else if(tujuan.equalsIgnoreCase("Bandung")){
            bd.getDestination();
            bd.setTiketPesawat(500000, jumlah);
            bd.setPenginapan(100000, jumlah, hari);
            bd.setTransportasi(200000, jumlah, hari);
            bd.setOther(100000, jumlah, hari);
            bd.setTotal();
            System.out.println("Perkiraan Biaya Total : Rp" + bd.getTotal() + ",-");
        }else{
            System.out.println("Mohon Maaf Tujuan Belum Tersedia");
        }
    }
}