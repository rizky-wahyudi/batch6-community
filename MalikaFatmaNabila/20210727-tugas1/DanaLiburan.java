public class DanaLiburan{
    protected int tiketPesawat;
    protected int penginapan;
    protected int transportasi;
    protected int other;
    protected int total;
    public void setTiketPesawat(int tiketPesawat, int jumlahWisatawan){
        this.tiketPesawat = tiketPesawat * jumlahWisatawan ;
    }
    public int getTiketPesawat(){
        return tiketPesawat;
    }
    public void setPenginapan(int penginapan, int jumlahWisatawan, int day){
        this.penginapan = penginapan * jumlahWisatawan * day;
    }
    public int getPenginapan(){
        return penginapan;
    }
    public void setTransportasi(int transportasi, int jumlahWisatawan, int day){
        this.transportasi = transportasi * jumlahWisatawan * day;
    }
    public int getTranspoertasi(){
        return transportasi;
    }
    public void setOther(int other, int jumlahWisatawan, int day){
        this.other = other * jumlahWisatawan * day;
    }
    public int getOther(){
        return other;
    }
    public void setTotal(){
        this.total = this.tiketPesawat + this.penginapan + this.transportasi + this.other;
    }
    public int getTotal(){
        return total;
    }
}