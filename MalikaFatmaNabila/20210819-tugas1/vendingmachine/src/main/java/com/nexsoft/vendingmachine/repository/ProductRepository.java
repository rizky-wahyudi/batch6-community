package com.nexsoft.vendingmachine.repository;

import com.nexsoft.vendingmachine.entity.Product;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends CrudRepository <Product, Integer> {
    Product findById(int id);
    List<Product> findAll();
    void deleteById(int id);
    Product findByPosition(String position);
    @Modifying
    @Query("update Product p set p.stock = p.stock-1 where p.position = :position")
    void updateStock(@Param(value = "position") String position);
}
