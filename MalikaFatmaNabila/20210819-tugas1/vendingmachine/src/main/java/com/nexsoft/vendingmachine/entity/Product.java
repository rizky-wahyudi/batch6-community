package com.nexsoft.vendingmachine.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String name;
    int price;
    String position;
    int stock;

    public Product(){}
    public Product(String name, int price, String position, int stock){
        this.name = name;
        this.price = price;
        this.position = position;
        this.stock = stock;
    }

    public int getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    public int getPrice(){
        return price;
    }
    public String getPosition(){
        return position;
    }
    public int getStock(){
        return stock;
    }
    public void setId(){
        this.id = id;
    }
}