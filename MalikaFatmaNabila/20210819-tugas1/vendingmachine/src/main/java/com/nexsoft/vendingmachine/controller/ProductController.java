package com.nexsoft.vendingmachine.controller;

import com.nexsoft.vendingmachine.entity.Product;
import com.nexsoft.vendingmachine.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    @Autowired
    private ProductRepository repo;

    @GetMapping("/product")
    public List<Product> getAllNotes() {
        return repo.findAll();
    }

    @GetMapping("/buyproduct/{position}")
    public Product getProductByPosition(@PathVariable(value = "position") String position){
        repo.updateStock(position);
        return repo.findByPosition(position);
    }

    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Product addProduct(@RequestBody  Product product){
        return repo.save(product);
    }


}