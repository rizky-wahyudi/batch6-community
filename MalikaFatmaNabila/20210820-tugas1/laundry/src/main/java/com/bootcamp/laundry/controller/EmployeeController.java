package com.bootcamp.laundry.controller;

import com.bootcamp.laundry.model.Customer;
import com.bootcamp.laundry.model.Employee;
import com.bootcamp.laundry.repository.AddressRepository;
import com.bootcamp.laundry.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeRepository repo;
    @Autowired
    private AddressRepository repository;

    @GetMapping("/employee")
    public String Employee(){
        return "Employee Controller";
    }
    @GetMapping("/getAllEmployee")
    public List<Employee> getAllEmployee(){
        return repo.findAll();
    }
    @GetMapping("/getEmployeeById/{id}")
    public Employee getEmployeeById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
    @PostMapping("/addEmployee/address:{id}")
    public Employee addEmployee(@RequestBody Employee employee, @PathVariable(value = "id") int id){
        employee.setAddressId(repository.findById(id));
        return repo.save(employee);
    }
}
