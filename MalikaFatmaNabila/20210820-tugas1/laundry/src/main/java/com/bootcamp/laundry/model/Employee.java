package com.bootcamp.laundry.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private Date birthDate;
    @OneToOne(targetEntity = Address.class)
    private Address addressId;
    private String phone;
    private String email;

    public Employee() {
    }

    public Employee(String name, Date birthDate, String phone, String email) {
        this.name = name;
        this.birthDate = birthDate;
        this.phone = phone;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId() {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Address getAddress() {
        return addressId;
    }

    public void setAddressId(Address address) {
        this.addressId = addressId;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail(){
        return email;
    }
}

