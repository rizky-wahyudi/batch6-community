package com.bootcamp.laundry.model;

import javax.persistence.*;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String street;
    String district;
    String city;
    String province;
    String country;

    public Address(){}
    public Address(String street, String district, String city, String province, String country){
        this.street = street;
        this.district = district;
        this.city = city;
        this.province = province;
        this.country = country;
    }
    public int getId(){return id;}
    public String getStreet(){return street;}
    public String getDistrict(){return district;}
    public String getCity(){return  city;}
    public String getProvince(){return province;}
    public String getCountry(){return country;}
    public void setId(){this.id = id;}
}