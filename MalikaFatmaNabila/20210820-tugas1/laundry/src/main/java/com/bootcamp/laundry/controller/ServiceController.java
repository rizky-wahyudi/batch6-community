package com.bootcamp.laundry.controller;

import com.bootcamp.laundry.model.Service;
import com.bootcamp.laundry.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServiceController {
    @Autowired
    private ServiceRepository repo;

    @GetMapping("/service")
    public String service(){
        return "Service Controller";
    }
    @GetMapping("/getAllService")
    public List<Service> getAllService(){
        return repo.findAll();
    }
    @GetMapping("/getServiceById/{id}")
    public Service getServiceById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
    @PostMapping("/addService")
    public Service addService(@RequestBody Service service){
        return repo.save(service);
    }
}
