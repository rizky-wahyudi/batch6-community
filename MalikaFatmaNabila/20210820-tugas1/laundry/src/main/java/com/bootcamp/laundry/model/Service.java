package com.bootcamp.laundry.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String name;
    double price;
    String unit;
    String type;
    public Service(){}
    public Service (String name, double price, String unit, String type){
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.type = type;
    }
    public int getId(){ return id; }
    public String getName(){ return name; }
    public double getPrice(){ return price; }
    public String getUnit(){ return unit; }
    public String type(){ return type; }
    public void setId(){ this.id = id; }
}