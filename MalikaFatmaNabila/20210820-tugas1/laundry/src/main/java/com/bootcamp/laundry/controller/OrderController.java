package com.bootcamp.laundry.controller;

import com.bootcamp.laundry.model.OrderItem;
import com.bootcamp.laundry.model.Orders;
import com.bootcamp.laundry.repository.OrderItemRepository;
import com.bootcamp.laundry.repository.OrdersRepository;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {
    @Autowired
    private OrdersRepository repo;
    @Autowired
    private OrderItemRepository repository;

    @GetMapping("/Order")
    public String order(){
        return "Order Controller";
    }
    @GetMapping("/getAllOrder")
    public List<Orders> getAllOrder(){
        return repo.findAll();
    }
    @GetMapping("/getOrderById/{id}")
    public Orders getOrderById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
    @PostMapping("/addOrder")
    public Orders addOrder(@RequestBody Orders order){
        return repo.save(order);
    }
    @PostMapping(value = "/addOrderBrute/employee:{id}", consumes = "application/json")
    public String addCountry (@RequestBody Orders order, @PathVariable(value = "id") int id){
        for(OrderItem oi : order.getOrderItem()){
            repository.save(oi);
        }
        order.setStatus((String.valueOf(id)));
        repo.save(order);
        return "Data berhasil disimpan";
    }
}
