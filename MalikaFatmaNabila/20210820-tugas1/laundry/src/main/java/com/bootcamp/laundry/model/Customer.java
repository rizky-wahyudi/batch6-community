package com.bootcamp.laundry.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String firstName;
    String lastName;
    @OneToOne(targetEntity = Address.class)
    private Address address;
    String phone;
    String email;
    String avatar;

    public Customer(){}
    public Customer(String firstName, String lastName, String phone, String email, String avatar){
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.avatar = avatar;
    }
    public int getId(){return id;}
    public String getFirstName(){return firstName;}
    public String getLastName(){return lastName;}
    public String getPhone(){return phone;}
    public String getEmail(){return email;}
    public String getAvatar(){return avatar;}
    public Address getAddress() {return address;}
    public void setAddress(Address address) {this.address = address;}
    public void setId(){this.id = id;}
}
