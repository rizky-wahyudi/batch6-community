package com.bootcamp.laundry.repository;

import com.bootcamp.laundry.model.OrderItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends CrudRepository <OrderItem, Integer> {
    OrderItem findById(int id);
    List<OrderItem> findAll();
    void deleteById(int id);
    OrderItem save(OrderItem orderItem);
}
