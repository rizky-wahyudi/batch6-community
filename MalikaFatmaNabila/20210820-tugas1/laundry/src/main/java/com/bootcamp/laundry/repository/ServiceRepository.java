package com.bootcamp.laundry.repository;

import com.bootcamp.laundry.model.Service;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends CrudRepository <Service, Integer> {
    Service findById(int id);
    List<Service> findAll();
    void deleteById(int id);
    Service save(Service service);
}
