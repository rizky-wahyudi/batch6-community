package com.bootcamp.laundry.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @OneToMany(targetEntity = OrderItem.class)
    private List<OrderItem> orderItem = new ArrayList<>();
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate orderDate;
    private String status;
    private String statusOrders;
    private boolean paymentStatus;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate lastUpdateDate;
    public Orders(){}
    public Orders(List<OrderItem> orderItem, LocalDate orderDate, boolean paymentStatus, LocalDate lastUpdateDate){
        this.orderItem = orderItem;
        this.orderDate = orderDate;
        this.paymentStatus = paymentStatus;
        this.lastUpdateDate = lastUpdateDate;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<OrderItem> getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(List<OrderItem> orderItem) {
        this.orderItem = orderItem;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public LocalDate getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDate lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
}
