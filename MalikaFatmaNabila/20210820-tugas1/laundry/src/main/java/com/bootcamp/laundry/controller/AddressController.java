package com.bootcamp.laundry.controller;

import com.bootcamp.laundry.model.Address;
import com.bootcamp.laundry.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AddressController {
    @Autowired
    private AddressRepository repo;

    @GetMapping("/address")
    public String Address(){
        return "Controller Address";
    }
    @GetMapping("/getAllAddress")
    public List<Address> getAllDress(){
        return repo.findAll();
    }
    @GetMapping("/getAddressById/{id}")
    public Address getAddressById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
    @PostMapping("/addAddress")
    public Address addAddress(@RequestBody Address address){
        return repo.save(address);
    }
}
