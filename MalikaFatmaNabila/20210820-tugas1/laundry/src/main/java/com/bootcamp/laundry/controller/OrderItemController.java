package com.bootcamp.laundry.controller;

import com.bootcamp.laundry.model.OrderItem;
import com.bootcamp.laundry.model.Service;
import com.bootcamp.laundry.repository.OrderItemRepository;
import com.bootcamp.laundry.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderItemController {
    @Autowired
    private OrderItemRepository repo;
    @Autowired
    private ServiceRepository serviceRepo;

    @GetMapping("/orderItem")
    public String orderItem(){
        return "OrderItem Controller";
    }

    @GetMapping("/getAllOrderItem")
    public List<OrderItem> getAllOrderItem(){
        return repo.findAll();
    }

    @GetMapping("/getOrderItemById/{id}")
    public OrderItem getOrderItemById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }

    @PostMapping("/addOrderItem/service:{id}")
    public OrderItem addOrderItem(@RequestBody OrderItem orderItem, @PathVariable(value = "id") int id){
        orderItem.setService((Service) serviceRepo.findById(id));
        return repo.save(orderItem);
    }
}
