package com.bootcamp.laundry.controller;

import com.bootcamp.laundry.model.Address;
import com.bootcamp.laundry.model.Customer;
import com.bootcamp.laundry.repository.AddressRepository;
import com.bootcamp.laundry.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    private CustomerRepository repo;
    @Autowired
    private AddressRepository repository;

    @GetMapping("/customer")
    public String Customer(){
        return "Customer Controller";
    }
    @GetMapping("/getAllCustomer")
    public List<Customer> getAllDress(){
        return repo.findAll();
    }
    @GetMapping("/getCustomerById/{id}")
    public Customer getCustomerById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
    @PostMapping("/addCustomer/address:{id}")
    public Customer addCustomer(@RequestBody Customer customer, @PathVariable(value = "id") int id){
        customer.setAddress(repository.findById(id));
        return repo.save(customer);
    }
}
