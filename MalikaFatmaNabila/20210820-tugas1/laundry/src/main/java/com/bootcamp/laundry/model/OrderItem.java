package com.bootcamp.laundry.model;

import javax.persistence.*;

@Entity
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String itemName;
    double weight;
    int amount;
    @OneToOne(targetEntity = Service.class)
    private Service service;

    public OrderItem(){}
    public OrderItem(String itemName, double weight, int amount){
        this.itemName = itemName;
        this.weight = weight;
        this.amount = amount;
    }
    public int getId(){return id;}
    public void setId(int id) {
        this.id = id;
    }
    public String getItemName() {
        return itemName;
    }
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public Service getService() {
        return service;
    }
    public void setService(Service service) {
        this.service = service;
    }
}