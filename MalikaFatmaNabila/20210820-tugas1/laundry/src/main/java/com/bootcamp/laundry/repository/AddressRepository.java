package com.bootcamp.laundry.repository;

import com.bootcamp.laundry.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends CrudRepository <Address, Integer> {
    Address findById(int id);
    List<Address> findAll();
    void deleteById(int id);
    Address save(Address address);
}
