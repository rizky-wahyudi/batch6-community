package com.example.demostudent.repository;

import com.example.demostudent.entity.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository <Student, Integer> {
    Student findById(int id);
    List<Student> findAll();
    void deleteById(int id);
}

