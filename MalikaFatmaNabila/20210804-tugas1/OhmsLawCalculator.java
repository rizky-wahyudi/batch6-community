import java.awt.Color;
import java.awt.Font;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import static javax.swing.GroupLayout.Alignment.*;

public class OhmsLawCalculator extends JFrame {
	public OhmsLawCalculator() {
        this.setResizable(false);
        
		JLabel voltsLabel = new JLabel("Volts (E)");
        final JTextField voltsTextField = new JTextField();
        
		JLabel resistanceLabel = new JLabel("Resistance (R)");
		final JTextField resistanceTextField = new JTextField(10);
        
		JLabel currentLabel = new JLabel("Current (I)");
		final JTextField currentTextField = new JTextField(10);

		JLabel wattsLabel = new JLabel("Power (P)"); 
		final JTextField resultsTextField = new JTextField();
       
        JLabel titleLabel = new JLabel("Electricity Calculator");
		titleLabel.setFont(new Font("Serif", Font.BOLD, 18));
		titleLabel.setForeground(Color.blue);

        JButton calcButton = new JButton("Calculate");
        JButton clearButton = new JButton("Clear");
        String sFileName = "picture.png";
        BufferedImage image = null;
		try {
			image = ImageIO.read(new File(sFileName));
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, ex.toString() + " " + sFileName);
			System.exit(0);
		}

		JLabel labelPic1 = new JLabel(new ImageIcon(image));
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
    		.addGroup(layout.createParallelGroup(LEADING)
	    		.addGroup(layout.createSequentialGroup()
	    			.addGap(120)
        			.addComponent(titleLabel)
	    		)
    			.addGroup(layout.createSequentialGroup()
		        	.addGroup(layout.createParallelGroup(LEADING)
		    			.addComponent(labelPic1)
					)
		    		.addGroup(layout.createParallelGroup(LEADING)
		    			.addComponent(resistanceLabel)
		    			.addComponent(currentLabel)
                        .addComponent(voltsLabel)
                        .addComponent(wattsLabel)
		    			.addComponent(calcButton)
					)
					.addGroup(layout.createParallelGroup(LEADING)
						.addComponent(resistanceTextField)
						.addComponent(currentTextField)
                        .addComponent(voltsTextField)
                        .addComponent(resultsTextField)
		    			.addComponent(clearButton)
					)
				)
			)
		);
        
        layout.setVerticalGroup(layout.createSequentialGroup()	
    		.addGroup(layout.createParallelGroup(BASELINE)
				.addComponent(titleLabel)
			)
			.addGap(25)
        	.addGroup(layout.createParallelGroup(BASELINE)
    			.addComponent(labelPic1)
    			.addGroup(layout.createSequentialGroup()
					.addGap(5)
					.addGroup(layout.createParallelGroup(BASELINE)
                        .addComponent(resistanceLabel)
                        .addComponent(resistanceTextField)
					)
					.addGroup(layout.createParallelGroup(BASELINE)
                        .addComponent(currentLabel)
                        .addComponent(currentTextField)
					)
					.addGroup(layout.createParallelGroup(BASELINE)
                    .addComponent(voltsLabel)
                    .addComponent(voltsTextField)
					)
					.addGroup(layout.createParallelGroup(BASELINE)
                        .addComponent(wattsLabel)
                        .addComponent(resultsTextField)
					)
                    .addGap(20)
			        .addGroup(layout.createParallelGroup(BASELINE)
                        .addComponent(calcButton)
                        .addComponent(clearButton)
			        )
				)
			)
		);

        clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentTextField.setText("");
				resistanceTextField.setText("");
				voltsTextField.setText("");
				resultsTextField.setText("");
			}
		});
        
        calcButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
                if (!currentTextField.getText().equals("") && !resistanceTextField.getText().equals("") && voltsTextField.getText().equals("")) {
					if (isNumeric(currentTextField.getText()) && isNumeric(resistanceTextField.getText())) {
						double volts = Double.parseDouble(currentTextField.getText()) * Double.parseDouble(resistanceTextField.getText());
						double p = Double.parseDouble(currentTextField.getText()) * volts;
						resultsTextField.setText(Double.toString(round(p, 2)));
						currentTextField.setText(currentTextField.getText());
						resistanceTextField.setText(resistanceTextField.getText());
						voltsTextField.setText(Double.toString(round(volts, 2)));	
					} else {
						JOptionPane.showMessageDialog(null, "Please enter valid mueric values for resistance and current");
					}
				} else if (currentTextField.getText().equals("") && !resistanceTextField.getText().equals("") && !voltsTextField.getText().equals("")) {
					if (isNumeric(voltsTextField.getText()) && isNumeric(resistanceTextField.getText())) {
						double current = Double.parseDouble(voltsTextField.getText()) / Double.parseDouble(resistanceTextField.getText());
						double p = Double.parseDouble(voltsTextField.getText()) * current;
						resultsTextField.setText(Double.toString(round(p, 2)));
						currentTextField.setText(Double.toString(round(current, 2)));
						resistanceTextField.setText(resistanceTextField.getText());
						voltsTextField.setText(voltsTextField.getText());	
					} else {
						JOptionPane.showMessageDialog(null, "Please enter valid mueric values for resistance and volts");
					}
				} else if (!currentTextField.getText().equals("") && resistanceTextField.getText().equals("") && !voltsTextField.getText().equals("")) {
					if (isNumeric(currentTextField.getText()) && isNumeric(currentTextField.getText())) {
						double resistance = Double.parseDouble(voltsTextField.getText()) / Double.parseDouble(currentTextField.getText());
						resistanceTextField.setText(Double.toString(round(resistance, 2)));
						double p = Double.parseDouble(voltsTextField.getText()) * Double.parseDouble(currentTextField.getText());
						resultsTextField.setText(Double.toString(round(p, 2)));						
						currentTextField.setText(currentTextField.getText());
						voltsTextField.setText(voltsTextField.getText());
					} else {
						JOptionPane.showMessageDialog(null, "Please enter valid mueric values for current and volts");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please enter 2 vaules to calcualte the 3rd");
				}
			}
		});
        
        setTitle("Electricity Calculator");
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	public boolean isNumeric(String str) {
		try {
			Double.parseDouble(str);
		} catch(Exception e) {
		    return(false);
		}
		return(true);
	}
	
	public double round(double val, int plc) {
		double pwr = Math.pow(10,plc);  
		val = val * pwr;
		double tmp = (int) val;     
		if (((int)(val + .5)) == (int) val)
			return (tmp/pwr);
		else
			return((tmp + 1.0)/pwr);
    }

	public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                new OhmsLawCalculator().setVisible(true);
            }
        });
    }
}