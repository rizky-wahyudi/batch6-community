package logistic;

public enum OrderStatus {
	DELIVERED,
	PROCESSING,
	CANCELLED;
}
