package logistic;

public enum PaymentStatus {
	PAID,
    UNPAID;
}
