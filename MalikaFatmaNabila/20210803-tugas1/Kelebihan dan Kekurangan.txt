- List Kekurangan :
1. Memiliki daya tahan tubuh yang kurang baik => istirahat cukup, minum vitamin, menjaga pola hidup yang sehat.
2. Terlalu ambisius yang menyebabkan semuanya kacau => melakukan sesuatu sesuai porsinya, melihat kemampuan diri sendiri
3. Malas => mengingatkan diri sendiri tentang tujuan yang ingin dicapai, memberi reward pada diri sendiri jika goals berhasil tercapai, motto untuk diri sendiri "mungkin kamu bisa menunda, tapi waktu tidak bisa menunggu"
4. Ceroboh
5. Slow like a snail
6. Fokus yang berlebihan
7. Mudah panik
8. Pelupa
9. Mudah overthingking
10. Tidak terlalu mahir frontend dan segala sesuatu yang membutuhkan pemikiran mengenai keindahan, proporsi, dan lain sebagainya

- List Kelebihan:
1. Mudah bersosialisasi secara offline => mungkin 
2. Suka belajar => akan lebih ditingkatkan untuk belajar diluar kesukaan dan kebutuhan
3. Pantang menyerah => akan lebih ditambah
4. Suka mencoba hal baru
5. Pembicara yang baik
6. Orang yang aktif
7. Pendengar yang baik
8. Sabar
9. Mudah mendapat ide
10. Tidak mudah bosan