SELECT
	p.nama				as "nama",
    d.nomor_KTP			as "KTP",
    tr.jenis_kendaraan	as "kendaraan",
    b.berat				as "maks. bagasi / org",
    ti.lokasiAwal		as "asal",
    ti.tujuan			as "tujuan",
    ti.harga_tiket		as "Harga",
    ti.tanggal_berangkat	as "tanggal"
FROM tiket as ti
JOIN pelanggan as p
	ON ti.pelanggan_id = p.id
JOIN dokumen as d 
	ON p.dokumen_id = d.id
JOIN transportasi as tr
	ON ti.transportasi_id = tr.id
JOIN bagasi as b
	ON tr.bagasi_id = b.id
;