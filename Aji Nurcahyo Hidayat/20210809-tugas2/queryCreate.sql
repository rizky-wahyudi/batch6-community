CREATE DATABASE handson_perjalanan;

use handson_perjalanan;

CREATE TABLE pelanggan(
	id INT NOT NULL AUTO_INCREMENT,
    nama VARCHAR(50),
    no_telp VARCHAR(20),
    dokumen_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE dokumen(
	id INT NOT NULL AUTO_INCREMENT,
    nomor_KTP VARCHAR(16),
    nomor_passport VARCHAR(16),
    surat_vaksin VARCHAR(20),
    PRIMARY KEY (id)
);

CREATE TABLE bagasi(
	id INT NOT NULL AUTO_INCREMENT,
    berat INT,
    PRIMARY KEY (id)
);

CREATE TABLE transportasi(
	id INT NOT NULL AUTO_INCREMENT,
    jenis_kendaraan VARCHAR(50),
    bagasi_id INT,
    PRIMARY KEY (id)
);

CREATE TABLE tiket(
	id INT NOT NULL AUTO_INCREMENT,
    pelanggan_id INT,
    transportasi_id INT,
    tanggal_berangkat DATE,
    lokasiAwal VARCHAR(50),
    tujuan VARCHAR(50),
    harga_tiket INT,
    PRIMARY KEY (id)
);

ALTER
TABLE pelanggan
ADD CONSTRAINT fk_dokumen_pelanggan
FOREIGN KEY (dokumen_id)
REFERENCES dokumen(id);

ALTER
TABLE transportasi
ADD CONSTRAINT fk_bagasi_transportasi
FOREIGN KEY (bagasi_id)
REFERENCES bagasi(id);

ALTER
TABLE tiket
ADD CONSTRAINT fk_pelanggan_tiket
FOREIGN KEY (pelanggan_id)
REFERENCES pelanggan(id);

ALTER
TABLE tiket
ADD CONSTRAINT fk_transportasi_tiket
FOREIGN KEY (transportasi_id)
REFERENCES transportasi(id);
