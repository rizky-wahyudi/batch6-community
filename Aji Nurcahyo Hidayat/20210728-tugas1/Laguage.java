public class Laguage {
    private int weight;
    private int handlaguage;
    private String isi;

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHandlaguage() {
        return handlaguage;
    }

    public void setHandlaguage(int handlaguage) {
        this.handlaguage = handlaguage;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}