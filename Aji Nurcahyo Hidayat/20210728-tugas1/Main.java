public class Main {

    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        Pasport pasport = new Pasport();
        Laguage laguage = new Laguage();

        ticket.setJenisTicket("ekonomi");
        pasport.setStatus("aktif");
        laguage.setWeight(15);
        laguage.setHandlaguage(5);
        laguage.setIsi("makanan");

        Airport airport1 = new Airport(ticket, pasport, laguage);
        airport1.checkBySecurity1();
        airport1.checkByXray();
        airport1.checkByCounterAirlane();
        airport1.checkByImigration();
        airport1.checkByXray();
        airport1.enterWaitingRoom();
        System.out.println("Masuk pesawat dan liburan");
    }
}