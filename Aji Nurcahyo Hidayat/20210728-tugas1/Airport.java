public class Airport {
    private Ticket ticket;
    private Pasport pasport;
    private Laguage laguage;

    Airport(Ticket ticket, Pasport pasport, Laguage laguage){
        this.ticket = ticket;
        this.pasport = pasport;
        this.laguage = laguage;
    }

    public void checkTicket(){
            if (ticket.getJenisTicket().equals("ekonomi") || ticket.getJenisTicket().equals("bisnis")) {
                System.out.println("Ticket sudah sesuai");
            } else {
                System.out.println("ticket tidak sesuai");
                System.exit(0);
            }
    }
    public void checkPasport(){
            if (pasport.getStatus().equals("aktif")) {
                System.out.println("Pasport masih aktif");
            } else {
                System.out.println("pasport tidak aktif");
                System.exit(0);
            }
    }
    public void checkBeratLaguage(){
            if (laguage.getWeight() <= 20) {
                System.out.println("Berat barang bagasi sesuai");
            } else {
                System.out.println("kelebihan beban");
                System.exit(0);
            }
    }
    public void checkIsiLaguage(){
            if (!laguage.getIsi().equals("barang terlarang")) {
                System.out.println("Isi barang aman");
            } else {
                System.out.println("Tidak lolos X Ray, barang tidak aman");
                System.exit(0);
            }
    }
    public void checkHandLaguage(){
            if (laguage.getHandlaguage() <= 10) {
                System.out.println("Berat barang bawaan sesuai");
            } else {
                System.out.println("kelebihan beban hand laguage");
                System.exit(0);
            }
    }

    public void checkBySecurity1(){
        System.out.println("Pengecekan oleh Security 1 : ");
        checkTicket();
        checkPasport();
        checkBeratLaguage();
        System.out.println("======================================\n");
    }
    public void checkByXray(){
        System.out.println("Pengecekan oleh X Ray : ");
        checkIsiLaguage();
        System.out.println("======================================\n");
    }
    public void checkByCounterAirlane(){
        System.out.println("Pengecekan oleh Counter Airlane : ");
        checkTicket();
        checkPasport();
        checkBeratLaguage();
        System.out.println("======================================\n");
    }

    public void checkByImigration(){
        System.out.println("Pengecekan oleh Imigrasi : ");
        checkPasport();
        checkTicket();
        System.out.println("======================================\n");
    }

    public void enterWaitingRoom(){
        System.out.println("Masuk Waiting room :");
        checkPasport();
        checkTicket();
        checkHandLaguage();
        System.out.println("======================================\n");
    }
}