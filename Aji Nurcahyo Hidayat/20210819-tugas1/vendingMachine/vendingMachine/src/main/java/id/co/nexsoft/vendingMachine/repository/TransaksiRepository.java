package id.co.nexsoft.vendingMachine.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.vendingMachine.entity.Transaksi;

public interface TransaksiRepository extends CrudRepository<Transaksi, Integer>{
    Transaksi findById(int Id);
    List<Transaksi> findAll();    
}
