package id.co.nexsoft.vendingMachine.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.vendingMachine.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Integer>{
    Product findById(int id);
    List<Product> findAll();
    void deleteById(int id);
}
