package id.co.nexsoft.vendingMachine.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.vendingMachine.entity.Product;
import id.co.nexsoft.vendingMachine.entity.Transaksi;
import id.co.nexsoft.vendingMachine.repository.ProductRepository;
import id.co.nexsoft.vendingMachine.repository.TransaksiRepository;

@RestController
public class VendingMachineController {
    @Autowired
    private ProductRepository repoProduct;
    @Autowired
    private TransaksiRepository repoTransaksi;

    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
            + "<h1>WELCOME to Vending Machine</h1>"
            + "</body></html>";
    }

    @PostMapping("/buy")
    public Transaksi buyProduct(@RequestBody Transaksi transaksi ){
        List<Product> listProduct = repoProduct.findAll();
        Transaksi barangBeli = null;
        int productId = 0;
        Product productBuy = null;
        // for(int i=0; i<listProduct.size(); i++){
        //     if(listProduct.get(i).getCode().equals(transaksi.getProductCode())){  
        //         productId = listProduct.get(i).getId(); 
        //         break;
        //     } else {
        //         productId = 0; 
        //     }
        // }


        // productBuy = repoProduct.findById(productId);
        if(transaksi.isPaymentStatus()){
            for(int i=0; i<listProduct.size(); i++){
                if(listProduct.get(i).getCode().equals(transaksi.getProductCode())){  
                    productId = listProduct.get(i).getId(); 
                    break;
                } else {
                    productId = 0; 
                }
            }
            productBuy = repoProduct.findById(productId);

            if(productBuy != null){
                int jmlProduct = productBuy.getStock();
                productBuy.setStock(jmlProduct - transaksi.getTotalBuy());  
                repoProduct.save(productBuy);
                barangBeli = transaksi;
            }else{
                return barangBeli;
            }
        } else {
            barangBeli = null;
            return barangBeli;
        }
        // barangBeli = transaksi;
        return repoTransaksi.save(barangBeli);
        // return "lolos else";
    }

    @GetMapping("/showTransaction")
    public List<Transaksi> getAllTransaksi(){
        return repoTransaksi.findAll();
    }
}
