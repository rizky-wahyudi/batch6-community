package id.co.nexsoft.vendingMachine.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.vendingMachine.entity.Product;
import id.co.nexsoft.vendingMachine.repository.ProductRepository;

@RestController
public class ProductController {
    @Autowired
    private ProductRepository repoProduct;

    @GetMapping("/product")
    public List<Product> getAllProduct(){
        return repoProduct.findAll();
    }

    @PostMapping("/product")
    public Iterable<Product> addProduct(@RequestBody List<Product> product){
        return repoProduct.saveAll(product);
    }
}
