import java.util.HashMap;
import java.util.Scanner;
import model.Food;
import model.VendingMachine;

public class Main {

    public static void main(String[] args) {
        Scanner masukan = new Scanner(System.in);
        HashMap<Integer, Food> menu = new HashMap<Integer, Food>();
        for (int i = 10; i <= 19; i++) {
            menu.put(i, new Food("piatos" + i, 10000));
        }
        for (int i = 20; i <= 29; i++) {
            menu.put(i, new Food("Chocolate" + i, 15000));
        }
        for (int i = 30; i <= 39; i++) {
            menu.put(i, new Food("Permen mint" + i, 5000));
        }
        for (int i = 40; i <= 49; i++) {
            menu.put(i, new Food("air mineral" + i, 7000));
        }
        for (int i = 50; i <= 59; i++) {
            menu.put(i, new Food("fanta" + i, 6000));
        }
        for (int i = 60; i <= 69; i++) {
            menu.put(i, new Food("es teh" + i, 5000));
        }

        VendingMachine vendingMachine = new VendingMachine(menu);
        System.out.println("\nPilih Nomor makanan: ");
        int kode = masukan.nextInt();

        System.out.println("\nmenu pilihan anda: "+ menu.get(kode).getName());
        System.out.println("harga: "+ menu.get(kode).getHarga());
        String nama = menu.get(kode).getName();
        int harga = menu.get(kode).getHarga();
        bayar(harga, nama);
    }


    public static void bayar(int harga, String nama){
        int kembalian = 0;
        int uang = 0;

        System.out.println("Masukan uang anda: ");
        Scanner input = new Scanner(System.in);
        uang = input.nextInt();

        if (uang< harga) {
            while (uang < harga) {
                System.out.println("Uang anda kurang, Lanjut/Tidak(1/2): ");
                int next = input.nextInt();
                if (next == 1){
                    System.out.println("masukan uang anda: ");
                    int uang2= input.nextInt();
                    uang = uang + uang2;
                    if (uang > harga){
                        kembalian = uang - harga;
                        System.out.println("silahkan menikmati "+nama);
                        System.out.println("\nkembalian anda: "+kembalian);
                    }
                }else {
                    System.out.println("Terima Kasih");
                    System.out.println("\nuang anda kembali sebesar "+uang);
                    break;
                }
            }
        }else{
            kembalian = uang - harga;
            System.out.println("silahkan menikmati "+nama);
            System.out.println("\nkembalian anda: "+kembalian);
        }
    }

}
