package model;

import java.util.HashMap;

public class VendingMachine{
    public VendingMachine(){

    }
    public VendingMachine(HashMap<Integer, Food> menu){
        for (HashMap.Entry<Integer, Food> entry : menu.entrySet()){
            Integer key = entry.getKey();
            Food food = entry.getValue();

            System.out.println(key+". " +food.getName()+" "+food.getHarga());
        }
    }
}
