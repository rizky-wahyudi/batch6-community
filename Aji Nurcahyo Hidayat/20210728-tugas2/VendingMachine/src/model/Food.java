package model;

public class Food extends VendingMachine{
    private String name;
    private int harga;

    Food(){

    }
    public Food(String name, int harga){
        this.name = name;
        this.harga = harga;
    }

    public String getName() {
        return name;
    }

    public int getHarga() {
        return harga;
    }
}
