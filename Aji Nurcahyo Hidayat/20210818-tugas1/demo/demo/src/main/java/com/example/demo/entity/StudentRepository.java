package com.example.demo.entity;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer>{
    Student findById(int id);
    List<Student> findAll();
    void deleteById(int id);
}
