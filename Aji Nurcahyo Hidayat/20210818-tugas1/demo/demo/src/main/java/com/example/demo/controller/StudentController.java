package com.example.demo.controller;

import java.util.List;

import com.example.demo.entity.Student;
import com.example.demo.entity.StudentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class StudentController{
    @Autowired
    private StudentRepository repo;

    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
            + "<h1>WELCOME</h1>"
            + "</body></html>";
    }

    @GetMapping("/student")
    public List<Student> getAllStudent(){
        return repo.findAll();
    }

    @GetMapping("/student/{id}")
    public Student getStudentById(@PathVariable(value = "id") int id) {
        return repo.findById(id);
    }

    @PostMapping("/student")
    @ResponseStatus(HttpStatus.CREATED)
    public Student addStudent(@RequestBody Student student) {
        return repo.save(student);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteStudent(@PathVariable(value = "id") int id){
        repo.deleteById(id);
    }

}