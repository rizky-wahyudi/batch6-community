public abstract class Liburan {
    private int jmlHari;
    private String tujuan;
    private int jmlOrang;

    public Liburan(String tujuan, int jmlHari, int jmlOrang){
        this.tujuan = tujuan;
		this.jmlHari = jmlHari;
        this.jmlOrang = jmlOrang;
    }

    public int getJmlHari(){
        return jmlHari;
    }
    public int getJmlOrang(){
        return jmlOrang;
    }

    public abstract void hitungDana();
}