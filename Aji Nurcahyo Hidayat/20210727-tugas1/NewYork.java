public class NewYork extends Liburan{
    public NewYork(String tujuan, int jmlHari, int jmlOrang){
        super(tujuan, jmlHari, jmlOrang);
    }
    @Override
    public void hitungDana(){
        System.out.println("Liburan ke New York\n");
        System.out.println("(Central park)\n");
        System.out.println("(The Metropolitan Museum of Art)\n");
        System.out.println("(Empire State Building)\n");

        int dana = getJmlOrang() * getJmlHari() *100;
        System.out.println("Dana liburan yang diperlukan: "+dana+ "dollar");
    }
}