public class Bandung extends Liburan{
    public Bandung(String tujuan, int jmlHari, int jmlOrang){
        super(tujuan, jmlHari, jmlOrang);
    }
    @Override
    public void hitungDana(){
        System.out.println("Liburan ke Bandung\n");
        System.out.println("(Tangkuban Perahu)\n");
        System.out.println("(Jalan Braga)\n");
        System.out.println("(Gedung Sate)\n");

        int dana = getJmlOrang() * getJmlHari() *30;
        System.out.println("Dana liburan yang diperlukan: "+dana+ "dollar");
    }
}
