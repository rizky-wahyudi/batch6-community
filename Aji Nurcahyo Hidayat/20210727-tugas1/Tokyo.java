public class Tokyo extends Liburan {
    public Tokyo(String tujuan, int jmlHari, int jmlOrang){
        super(tujuan, jmlHari, jmlOrang);
    }
    @Override
    public void hitungDana(){
        System.out.println("Liburan ke tokyo\n");
        System.out.println("(Tokyo Skytree)\n");
        System.out.println("(Meiji Jingu)\n");
        System.out.println("(Shinjuku Gyoen national Garden)\n");

        int dana = getJmlOrang() * getJmlHari() *50;
        System.out.println("Dana liburan yang diperlukan: "+dana+ "dollar");
    }
}