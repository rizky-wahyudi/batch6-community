package Model;

public interface Tax {
    final static double taxMakanan = 0.05;
    final static double taxSabun =  0.07;
    final static double taxMinuman = 0.1;
}
