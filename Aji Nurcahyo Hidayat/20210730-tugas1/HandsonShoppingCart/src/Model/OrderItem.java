package Model;

import java.util.ArrayList;
import java.util.List;

public class OrderItem {
    private Product product;
    private int amount;
    private List<Product> orderItems = new ArrayList<Product>();

    public OrderItem(){

    }

    public OrderItem(Product product, int jumlah){
        this.product = product;
        this.amount = jumlah;
    }

    public List<Product> getOrderItems() {
        return orderItems;
    }

    public Product getProduct() {
        return product;
    }

    public int getAmount() {
        return amount;
    }

}
