package Model;

import java.util.Date;

public class Product implements Tax{
    private int productId;
    private String articleNum ;
    private String description;
    private double purchasePrice;
    private double sellPrice = 0;
    private String kategori;
    private double tax = 0;

    Product(){

    }
    public Product(int productId, String articleNum, String description, double purchasePrice, String kategori){
        this.productId = productId;
        this.articleNum = articleNum;
        this.description = description;
        this.purchasePrice = purchasePrice;
        this.kategori = kategori;
        this.sellPrice = 0.1 * purchasePrice + purchasePrice;
        if (kategori.equalsIgnoreCase("makanan")){
            this.tax = sellPrice * taxMakanan;
        }else if(kategori.equalsIgnoreCase("sabun")){
            this.tax = sellPrice * taxSabun;
        }else if(kategori.equalsIgnoreCase("minuman")){
            this.tax = sellPrice * taxMinuman;
        }else {
            this.tax = tax;
        }
    }

    public int getProductId() {
        return productId;
    }

    public String getArticleNum() {
        return articleNum;
    }

    public String getDescription() {
        return description;
    }

    public double getSellPrice() {
        return sellPrice;
    }

    public double getTax() {
        return tax;
    }

}
