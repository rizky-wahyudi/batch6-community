import Model.Order;
import Model.OrderItem;
import Model.Product;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Product> dataProduk = new ArrayList<Product>();
        dataProduk.add(new Product(1,"makanan1","pisang goreng", 2000,"makanan"));
        dataProduk.add(new Product(2,"sabun1","lifeboi", 3000, "sabun"));
        dataProduk.add(new Product(3,"minuman1","teh pucuk", 4000, "minuman"));
        halamanUtama(dataProduk);
    }

    public  static  void halamanUtama(ArrayList<Product> dataProduk){
        Scanner input = new Scanner(System.in);
        boolean beliLagi = true;
        Order order = null;

        while (beliLagi) {
            tampilProduk(dataProduk);
            System.out.print("\nMau beli apa? (pilih angka): ");
            int idPilihan = input.nextInt();
            if (idPilihan  > 0 && idPilihan <= dataProduk.size()) {
                System.out.print("Beli berapa: ");
                int jmlBeli = input.nextInt();
                order = new Order();
                List<OrderItem> listBelanjaan = order.getOrderItems();
                listBelanjaan.add(orderBarang(dataProduk, idPilihan, jmlBeli));
            }else {
                System.out.println("barang tidak ada\n");
            }
            System.out.print("\nMau beli lagi? (Ya = 1 / No = 0): ");
            int beli = input.nextInt();
            if (beli == 0){
                beliLagi = false;
            }
        }

        System.out.println("\n=================================");
        System.out.println("Daftar Belanjaan anda");
        if(order != null) {
            tampilOrder(order);
        }else {
            System.out.println("tidak ada order");
        }
        System.out.println("=================================\n");

        bayar(order);
    }
    public static void tampilProduk(ArrayList<Product> data){
        System.out.println("=================================");
        for (int i = 0; i< data.size(); i++){
            Product dP= data.get(i);
            System.out.println(dP.getProductId()+". "+dP.getArticleNum()+ "  "+dP.getDescription()+"  Rp "+dP.getSellPrice());
        }
    }
    public static OrderItem orderBarang(ArrayList<Product> data, int id, int jumlah){
        Product product =null;
        for(Product pd : data){
            if (pd.getProductId() == id){
                product = pd;
                break;
            }
        }
        OrderItem orderItem = new OrderItem(product, jumlah);
        return orderItem;
    }
    public static void tampilOrder(Order order){
        order.setOrderDate(LocalDate.now());
        System.out.println("tanggal: \t"+order.getOrderDate());
        int banyakBelanja =order.getOrderItems().size();
        double totalBiaya = 0;
        for (int x =0; x<banyakBelanja; x++){
            OrderItem belanjaan = order.getOrderItems().get(x);
            double pajak = belanjaan.getProduct().getTax();
            double harga = (belanjaan.getProduct().getSellPrice() + pajak) * belanjaan.getAmount();
            totalBiaya =Math.ceil(totalBiaya +harga);

            System.out.println("\n"+belanjaan.getProduct().getArticleNum()+"\t "+
                    belanjaan.getProduct().getDescription()+"\t"+
                    belanjaan.getAmount()+"\t"+
                    belanjaan.getProduct().getSellPrice()+"\t"+
                    Math.ceil(belanjaan.getProduct().getTax())+"\t"+
                    harga);
        }
        order.setTotalPrice(totalBiaya);
        System.out.println("\n=================================");
        String totalPrice= String.format("%.2f %n",totalBiaya);
        System.out.println("branch id : "+order.getBranchId());
        System.out.println("Status : "+order.getStatus());
        System.out.println("Status pembayaran : "+order.getPaymentStatus());
        System.out.println("\nTotal: "+totalPrice);
    }
    public static void bayar(Order order){
        System.out.print("\nmasukan uang: ");
        Scanner inp = new Scanner(System.in);
        double uang = inp.nextDouble();
        double bayar = uang;
        double kembalian = 0;
        while (bayar < order.getTotalPrice()){
            System.out.print("Uang kurang, masukan lagi :");
            uang = inp.nextDouble();
            bayar = bayar + uang;
        }
        if(bayar > order.getTotalPrice()){
            kembalian = Math.floor(bayar - order.getTotalPrice());
        }
        order.setPaymentStatus("Paid");
        order.setStatus("Delivered");
        System.out.println("\n=================================");
        System.out.println("Struk belanjaan");
        tampilOrder(order);
        System.out.println("Bayar: "+bayar);
        System.out.println("Kembalian: "+kembalian);
        System.out.println("\n=================================");
    }
}
