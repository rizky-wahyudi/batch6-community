public interface Seats {
    final double airbus380 = 750;
    final double boeing747 = 529;
    final double boeing787 = 248;
}