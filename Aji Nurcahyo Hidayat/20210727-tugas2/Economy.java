public class Economy extends AirPlane implements Seats, Price{

    public Economy(String asal, String tujuan){
        super(asal, tujuan);
    }

    public double getProfitEconomy(){
        if(super.getName().equalsIgnoreCase("airbus380")){
            if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("asia")){
                double profitAirBus380 = 0.65 * Seats.airbus380 * Price.europeAsia * 1;
                return profitAirBus380;
            }else if (super.getAsal().equalsIgnoreCase("Asia") && super.getTujuan().equalsIgnoreCase("Europe")){
                double profitAirBus380 = 0.65 * Seats.airbus380 * Price.asiaEurope * 1;
                return profitAirBus380;
            }else if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("US")){
                double profitAirBus380 = 0.65 * Seats.airbus380 * Price.europeUS * 1;
                return profitAirBus380;
            }else {
                return 0;
            }
        }else if(super.getName().equalsIgnoreCase("boeing747")){
            if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("asia")){
                double profitBoeing747 = 0.65 * Seats.boeing747 * Price.europeAsia * 1;
                return profitBoeing747;
            }else if (super.getAsal().equalsIgnoreCase("Asia") && super.getTujuan().equalsIgnoreCase("Europe")){
                double profitBoeing747 = 0.65 * Seats.boeing747 * Price.asiaEurope * 1;
                return profitBoeing747;
            }else if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("US")){
                double profitBoeing747 = 0.65 * Seats.boeing747 * Price.europeUS * 1;
                return profitBoeing747;
            }else {
                return 0;
            }
        }else if(super.getName().equalsIgnoreCase("boeing787")){
            if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("asia")){
                double profitBoeing787 = 0.65 * Seats.boeing787 * Price.europeAsia * 1;
                return profitBoeing787;
            }else if (super.getAsal().equalsIgnoreCase("Asia") && super.getTujuan().equalsIgnoreCase("Europe")){
                double profitBoeing787 = 0.65 * Seats.boeing787 * Price.asiaEurope * 1;
                return profitBoeing787;
            }else if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("US")){
                double profitBoeing787 = 0.65 * Seats.boeing787 * Price.europeUS * 1;
                return profitBoeing787;
            }else {
                return 0;
            }
        }
            return 0;
    }

    public double totalLaugageEconomy(){
        if(super.getName().equalsIgnoreCase("airbus380")){
            return super.getLaugage() * 1 * 0.65 * Seats.airbus380;
        }else if(super.getName().equalsIgnoreCase("boeing747")){
            return super.getLaugage() * 1 * 0.65 * Seats.boeing747;
        }else if(super.getName().equalsIgnoreCase("boeing787")){
            return super.getLaugage() * 1 * 0.65 * Seats.boeing787;
        }else {
            return 0;
        }
    }
}