import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	
        Scanner input = new Scanner(System.in);
        System.out.println("masukan asal: ");
        String masukan = input.nextLine();
        System.out.println("masukan tujuan: ");
        String masukan2 = input.nextLine();
        System.out.println("masukan pesawat: ");
        String masukan3 = input.nextLine();
        
        Economy ekonomi = new Economy(masukan, masukan2);
        ekonomi.setName(masukan3);
        Business bisnis = new Business(masukan, masukan2);
        bisnis.setName(masukan3);
        FirstClass firstClass = new FirstClass(masukan, masukan2);
        firstClass.setName(masukan3);

        System.out.println("\n=====================");
        System.out.println("Profit tiap perjalanan pesawat");
        System.out.println("=====================\n");
        System.out.println("Nama Pesawat: "+ekonomi.getName());
        System.out.println("Asal keberangkatan: "+ekonomi.getAsal());
        System.out.println("Tujuan penerbangan: "+ekonomi.getTujuan());

        double totalProfit = ekonomi.getProfitEconomy() + bisnis.getProfitBusiness() + firstClass.getProfitFirstClass();
        double totalLaugage = ekonomi.totalLaugageEconomy() + bisnis.totalLaugageBusiness()         +firstClass.totalLaugageFirstClass();
        System.out.println("Total omset: "+ totalProfit);
        System.out.println("Total muatan: "+ totalLaugage);
        System.out.println("=====================\n");
    }
}