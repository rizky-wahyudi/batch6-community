public class AirPlane {
    private String name;
    private String asal;
    private String tujuan;
    private int laugage=20;

    AirPlane(String asal, String tujuan){
        this.asal = asal;
        this.tujuan = tujuan;
    }

    public int getLaugage() {
        return laugage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }
}