public interface Price {
    final double europeAsia = 500;
    final double asiaEurope = 600;
    final double europeUS = 650;
}
