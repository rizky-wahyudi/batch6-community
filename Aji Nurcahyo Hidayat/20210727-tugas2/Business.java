public class Business extends AirPlane implements Seats, Price{
    public Business(String asal, String tujuan){
        super(asal, tujuan);
    }

    public double getProfitBusiness(){
        if(super.getName().equalsIgnoreCase("airbus380")){
            if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("asia")){
                double profitAirBus380 = 0.25 * Seats.airbus380 * Price.europeAsia * 2.25;
                return profitAirBus380;
            }else if (super.getAsal().equalsIgnoreCase("Asia") && super.getTujuan().equalsIgnoreCase("Europe")){
                double profitAirBus380 = 0.25 * Seats.airbus380 * Price.asiaEurope * 2.25;
                return profitAirBus380;
            }else if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("US")){
                double profitAirBus380 = 0.25 * Seats.airbus380 * Price.europeUS * 2.25;
                return profitAirBus380;
            }else {
                return 0;
            }
        }else if(super.getName().equalsIgnoreCase("boeing747")){
            if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("asia")){
                double profitBoeing747 = 0.25 * Seats.boeing747 * Price.europeAsia * 2.25;
                return profitBoeing747;
            }else if (super.getAsal().equalsIgnoreCase("Asia") && super.getTujuan().equalsIgnoreCase("Europe")){
                double profitBoeing747 = 0.25 * Seats.boeing747 * Price.asiaEurope * 2.25;
                return profitBoeing747;
            }else if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("US")){
                double profitBoeing747 = 0.25 * Seats.boeing747 * Price.europeUS * 2.25;
                return profitBoeing747;
            }else {
                return 0;
            }
        }else if(super.getName().equalsIgnoreCase("boeing787")){
            if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("asia")){
                double profitBoeing787 = 0.25 * Seats.boeing787 * Price.europeAsia * 2.25;
                return profitBoeing787;
            }else if (super.getAsal().equalsIgnoreCase("Asia") && super.getTujuan().equalsIgnoreCase("Europe")){
                double profitBoeing787 = 0.25 * Seats.boeing787 * Price.asiaEurope * 2.25;
                return profitBoeing787;
            }else if (super.getAsal().equalsIgnoreCase("europe") && super.getTujuan().equalsIgnoreCase("US")){
                double profitBoeing787 = 0.25 * Seats.boeing787 * Price.europeUS * 2.25;
                return profitBoeing787;
            }else {
                return 0;
            }
        }
        return 0;
    }

    public double totalLaugageBusiness(){
        if(super.getName().equalsIgnoreCase("airbus380")){
            return super.getLaugage() * 2 * 0.25 * Seats.airbus380;
        }else if(super.getName().equalsIgnoreCase("boeing747")){
            return super.getLaugage() * 2 * 0.25 * Seats.boeing747;
        }else if(super.getName().equalsIgnoreCase("boeing787")){
            return super.getLaugage() * 2 * 0.25 * Seats.boeing787;
        }else {
            return 0;
        }
    }
}