package id.co.nexsoft;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

public class CalcularorTest {
    private Calculator calculatorTest;

    @BeforeEach
    public void init(){
        calculatorTest = new Calculator();
    }

    @Test
    public void addTest1(){
        int result = calculatorTest.add(10, 20);
        Assert.assertEquals(30,result);
    }

    @Test
    public void addTest2(){
        int result = calculatorTest.add(10, 20);
        Assert.assertTrue( "condition true",result == 30);
    }

    @Test
    public void addTest3(){
        int result = calculatorTest.add(10, 20);
        Assert.assertFalse("codition false", result == 10);
    }

    @Test
    public void addTest4(){
        int result = calculatorTest.add(10, 20);
        Assert.assertFalse("codition false", result < 10);
    }

    @Test
    public void addTest5(){
        Assert.assertThrows( "throw exeption",NumberFormatException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                calculatorTest.add(Integer.parseInt("a"), 10);
            }
        });
    }

    @Test
    public void subtractTest1(){
        int result = calculatorTest.subtract(10, 20);
        Assert.assertEquals(-10,result);
    }

    @Test
    public void subtractTest2(){
        int result = calculatorTest.subtract(15, 7);
        Assert.assertTrue( "condition true",result == 8);
    }

    @Test
    public void subtractTest3(){
        int result = calculatorTest.subtract(10, 9);
        Assert.assertFalse("codition false", result > 2);
    }

    @Test
    public void subtractTest4(){
        int result = calculatorTest.subtract(16, 8);
        Assert.assertFalse("codition false", result < 10);
    }

    @Test
    public void subtractTest5(){
        Assert.assertThrows( "throw exeption",NumberFormatException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                calculatorTest.subtract(Integer.parseInt("a"), 10);
            }
        });
    }

    @Test
    public void multiplyTest1(){
        int result = calculatorTest.multiply(2, 5);
        Assert.assertEquals(10,result);
    }

    @Test
    public void multiplyTest2(){
        int result = calculatorTest.multiply(4, 3);
        Assert.assertTrue( "condition true",result == 10);
    }

    @Test
    public void multiplyTest3(){
        int result = calculatorTest.multiply(3,6 );
        Assert.assertFalse("codition false", result == 18);
    }

    @Test
    public void multiplyTest4(){
        int result = calculatorTest.multiply(2, 8);
        Assert.assertFalse("codition false", result > 10 );
    }

    @Test
    public void multiplyTest5(){
        Assert.assertThrows( "throw exeption",NumberFormatException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                calculatorTest.multiply(Integer.parseInt("a"), 10);
            }
        });
    }

    @Test
    public void divideTest1(){
        int result = calculatorTest.divide(10, 2);
        Assert.assertEquals(5,result);
    }

    @Test
    public void divideTest2(){
        int result = calculatorTest.divide(8, 4);
        Assert.assertTrue( "condition true",result == 2);
    }

    @Test
    public void divideTest3(){
        int result = calculatorTest.divide(9,3 );
        Assert.assertFalse("codition false", result == 0);
    }

    @Test
    public void divideTest4(){
        int result = calculatorTest.divide(6, 3);
        Assert.assertFalse("codition false", result == 3 );
    }

    @Test
    public void divideTest5(){
        Assert.assertThrows( "throw exeption", ArithmeticException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                calculatorTest.divide(10, 0);
            }
        });
    }

    @Test
    @DisplayName("tes main: success parameter")
    public void mainTest1(){
        String[] args = {"4","6"};
        Calculator.main(args);
    }

    @Test
    @DisplayName("tes main: failed parameter string")
    public void mainTest2(){
        Assert.assertThrows(NumberFormatException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                String[] args = {"A","i"};
                Calculator.main(args);
            }
        });
    }

    @Test
    @DisplayName("tes main: failed parameter string & numeric")
    public void mainTest3(){
        Assert.assertThrows(NumberFormatException.class, ()->{
           String[] args = {"5","O"};
           Calculator.main(args);
        });
    }

}
