import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
	    for(int i=0; i<1000; i++){
	        ExecutorService executors = Executors.newFixedThreadPool(2);
            executors.submit(()->{
                try {
                    for (int j =1; j<=4; j++) {
                        File file = new File("test" + j+".txt");
                        Scanner scan = new Scanner(file);
                        while (scan.hasNext()){
                            String data= scan.nextLine();
                            System.out.println(data);
                        }

                        scan.close();
                    }
                }catch (FileNotFoundException e){
                    System.out.println("file tidak ditemukan");
                }
            });
            executors.shutdown();
        }
    }
}
