package model;

import java.net.URL;
import java.net.URLConnection;

public class UrlConnectionRead {
    public static Object getText(String urlname) throws Exception{
        URL url = new URL(urlname);
        URLConnection urlConnection = url.openConnection();
        return SerializationUtil.deserialize(urlConnection.getInputStream());

    }

}
