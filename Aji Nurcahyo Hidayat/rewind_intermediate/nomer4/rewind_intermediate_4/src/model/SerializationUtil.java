package model;

import java.io.*;
import java.net.URL;

public class SerializationUtil {

    public static Object deserialize(InputStream filename) throws IOException, ClassNotFoundException {
//        FileInputStream fis = new FileInputStream(filename));
        BufferedInputStream bis = new BufferedInputStream(filename);
        ObjectInputStream ois = new ObjectInputStream(bis);
        Object obj = ois.readObject();
        ois.close();
        return obj;
    }

    public static void serialize(Object obj, String filename) throws IOException{
        FileOutputStream fos = new FileOutputStream(filename);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(obj);
        oos.close();
    }
}
