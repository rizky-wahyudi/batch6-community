import model.SerializationUtil;
import model.UrlConnectionRead;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	for (int i=1; i<=5; i++){
	    try {
	        URL url = new URL("https://jsonplaceholder.typicode.com/todos/" + i);
            URLConnection urlConnection = url.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            urlConnection.getInputStream()));

            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();

            System.out.println(response.toString());


//            save ke file
            String filename = "file-"+i+".json";
            SerializationUtil.serialize(response, filename);


//	        Object[] obj = (Object[]) UrlConnectionRead.getText("https://jsonplaceholder.typicode.com/todos/" + i);
//            System.out.println(obj.toString());
//            System.out.println(Arrays.toString(obj));



//            String filename = ("https://jsonplaceholder.typicode.com/todos/" + i);
//        System.out.println(filename);
//
//            Object obj = SerializationUtil.deserialize(urlConnection.getInputStream());
//            System.out.println(obj);


        }catch (Exception e){
	        e.printStackTrace();
        }
    }
    }
}
