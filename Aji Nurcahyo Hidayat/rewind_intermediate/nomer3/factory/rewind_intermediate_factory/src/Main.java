import model.Mobil;
import model.MobilFactory;
import model.Sedan;
import model.Sport;

public class Main {

    public static void main(String[] args) { 
        Mobil mobil1 = MobilFactory.newInstance("sport", "lamborgini", "3000cc");
        Sport mobilSport = (Sport)mobil1;
        System.out.println("nama mobil :" +mobilSport.getName());
        System.out.println("kapasitas mesin : "+ mobilSport.getKapasitasMesin());
        System.out.println("tempat duduk : "+mobilSport.getJmlTempatDuduk());
        mobilSport.testDrive();
        System.out.println("\n");

        Mobil mobil2 = MobilFactory.newInstance("sedan", "civic", "1500cc");
        Sedan mobilSedan = (Sedan) mobil2;
        System.out.println("nama mobil :" +mobilSedan.getName());
        System.out.println("kapasitas mesin : "+ mobilSedan.getKapasitasMesin());
        System.out.println("tempat duduk : "+mobilSedan.getJmlTempatDuduk());
        mobilSedan.testDrive();
    }
}
