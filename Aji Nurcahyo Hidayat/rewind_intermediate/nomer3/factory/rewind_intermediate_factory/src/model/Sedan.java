package model;

public class Sedan extends Mobil{
    private String name;

    public Sedan (String name, String kapasitasMesin){
        this.name = name;
        setKapasitasMesin(kapasitasMesin);
        setJmlTempatDuduk(4);
    }

    public String getName() {
        return name;
    }

    public void testDrive(){
        System.out.println("mobil sedan "+ name +" mampu mencapai kecepatan maksimal 100km/jam");
    }
}
