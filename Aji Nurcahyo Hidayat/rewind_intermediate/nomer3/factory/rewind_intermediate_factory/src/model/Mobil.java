package model;

public class Mobil {
    private String kapasitasMesin;
    private int jmlTempatDuduk;

    public String getKapasitasMesin() {
        return kapasitasMesin;
    }

    public void setKapasitasMesin(String kapasitasMesin) {
        this.kapasitasMesin = kapasitasMesin;
    }

    public int getJmlTempatDuduk() {
        return jmlTempatDuduk;
    }

    public void setJmlTempatDuduk(int jmlTempatDuduk) {
        this.jmlTempatDuduk = jmlTempatDuduk;
    }
}
