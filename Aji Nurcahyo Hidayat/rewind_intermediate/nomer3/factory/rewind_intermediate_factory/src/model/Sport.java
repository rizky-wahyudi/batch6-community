package model;

public class Sport extends Mobil{
    private String name;

    public Sport(String name, String kapasitasMesin){
        this.name = name;
        setKapasitasMesin(kapasitasMesin);
        setJmlTempatDuduk(2);
    }

    public String getName() {
        return name;
    }

    public void testDrive(){
        System.out.println("mobil sport "+ name +" mampu mencapai kecepatan maksimal 250km/jam");
    }
}
