package model;

public class MobilFactory {
    public static Mobil newInstance(String type, String nama, String kapasitasMesin){
        switch (type){
            case "sedan":
                return new Sedan(nama, kapasitasMesin);
            case "sport":
                return new Sport(nama, kapasitasMesin);
            default:
                System.out.println("tidak ada");
                return null;
        }

    }
}
