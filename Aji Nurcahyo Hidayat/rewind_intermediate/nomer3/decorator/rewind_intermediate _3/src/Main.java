import model.BasicMotor;
import model.Motor;
import model.MotorBebek;
import model.MotorSport;

public class Main {

    public static void main(String[] args) {
        Motor motorBebek = new MotorBebek(new BasicMotor());
        motorBebek.buatMotor();
        System.out.println();

        Motor motorSport = new MotorSport(new BasicMotor());
        motorSport.buatMotor();
        System.out.println();

        Motor motorBebekSport = new MotorBebek(new MotorSport(new BasicMotor()));
        motorBebekSport.buatMotor();
    }
}
