package model;

public class MotorBebek extends MotorDecorator{
    public MotorBebek(Motor motor){
        super(motor);
    }

    @Override
    public void buatMotor(){
        super.buatMotor();
        System.out.println("tambah fitur motor bebek");
    }
}
