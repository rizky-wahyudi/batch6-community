package model;

public class MotorDecorator implements Motor{
    private Motor motor;

    public MotorDecorator(Motor motor){
        this.motor = motor;
    }

    @Override
    public void buatMotor(){
        this.motor.buatMotor();
    }
}
