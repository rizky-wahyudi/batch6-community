package model;

import java.util.ArrayList;

public class Indihome implements Observerable{
    private ArrayList<Customer> observerList = new ArrayList<Customer>();
    private int bulanan = 400000;

    public void tagihanBulanan(){
        notifyObserver();

    }

    public int getBulanan() {
        return bulanan;
    }

    @Override
    public void registerObserver(Customer customer){
        observerList.add(customer);
    }

    @Override
    public void removeObserver(Customer customer){
        observerList.remove(customer);
    }

    @Override
    public void notifyObserver(){
        for (int i = 0; i < observerList.size(); i++ ){
            observerList.get(i).update();
        }
    }

}
