package model;

public class Customer implements Observer{
    private String name;
    private Indihome indihome = new Indihome();

    public Customer(String name){
        this.name= name;
    }

    @Override
    public void update(){
        System.out.println("Hai "+name+ ", tagihan kamu sudah keluar yaa.");
        System.out.println("tagihan sebesar : "+ indihome.getBulanan());
    }

    public void berlangganan(Indihome indihome){
        this.indihome= indihome;
    }
}
