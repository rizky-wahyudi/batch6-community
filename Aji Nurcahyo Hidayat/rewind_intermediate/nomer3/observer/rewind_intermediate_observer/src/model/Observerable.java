package model;

public interface Observerable {
    public void registerObserver(Customer customer);
    public void removeObserver(Customer customer);
    public void notifyObserver();
}
