import model.Customer;
import model.Indihome;

public class Main {

    public static void main(String[] args) {
        Indihome indihome = new Indihome();

        Customer customer1 = new Customer("aji");
        Customer customer2 = new Customer("firda");
        customer1.berlangganan(indihome);
        customer1.berlangganan(indihome);

        indihome.registerObserver(customer1);
        indihome.registerObserver(customer2);

        indihome.tagihanBulanan();
    }
}
