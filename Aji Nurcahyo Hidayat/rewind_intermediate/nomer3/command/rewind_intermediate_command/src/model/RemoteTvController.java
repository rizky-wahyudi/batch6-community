package model;

public class RemoteTvController {
    private Command command;

    public void setCommand(Command command){
        this.command=command;
    }

    public void execute(){
        command.execute();
    }
}
