package model;

public class TvNyala implements Command{

    private TV tv;

    public TvNyala(TV tv){
        this.tv = tv;
    }

    public void execute(){
        tv.nyalakanTv();
    }
}
