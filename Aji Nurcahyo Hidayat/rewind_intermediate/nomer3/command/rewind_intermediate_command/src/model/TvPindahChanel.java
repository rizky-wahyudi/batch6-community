package model;

public class TvPindahChanel implements Command{
    private TV tv;

    public TvPindahChanel(TV tv){
        this.tv = tv;
    }

    public void execute(){
        tv.pindahChanel();
    }
}
