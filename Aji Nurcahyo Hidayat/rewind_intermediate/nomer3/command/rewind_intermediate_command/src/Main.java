import model.*;

public class Main {

    public static void main(String[] args) {
        RemoteTvController remote = new RemoteTvController();
        TV tv = new TV();

        Command TvNyala = new TvNyala(tv);
        Command TvPindahChanel = new TvPindahChanel(tv);

        remote.setCommand(TvNyala);
        remote.execute();
        System.out.println("\n");

        remote.setCommand(TvPindahChanel);
        remote.execute();
    }
}
