package model;

public class Calculate {
    private Resitance r = null;
    private Current c = null;
    private Voltage v = null;
    private Power p = null;
//    private ElectricityVariabel elVar;

//    private double r = 0;
//    private double c = 0;
//    private double v = 0;
//    private double p = 0;

    public Calculate(Resitance resitance, Current current, Voltage voltage, Power power) {
        this.r = resitance;
        this.c = current;
        this.v = voltage;
        this.p = power;
    }

    public void calculateElectricity(){
        double hasilR = 0;
        double hasilC = 0;
        double hasilV = 0;
        double hasilP = 0;

        double nilaiR = r.getNilaiResistance();
        double nilaiC = c.getNilaiCurrent();
        double nilaiV = v.getNilaiVoltage();
        double nilaiP = p.getNilaiPower();

        if(nilaiP==0 && nilaiC == 0 && nilaiR==0 && nilaiV == 0){
            System.out.println("semua nilai 0");
        }else if(nilaiP == 0 && nilaiV == 0){
            System.out.println("Resistance : "+nilaiR);
            System.out.println("Current : "+nilaiC);
            hasilP = Math.pow(nilaiC,2) * nilaiR;
            hasilV = nilaiC * nilaiR;
            System.out.println("Voltage : "+hasilV);
            System.out.println("Power : "+hasilP);
        }else if(nilaiP == 0 && nilaiC == 0){
            System.out.println("Resistance : "+nilaiP);
            System.out.println("Voltage : "+ nilaiV);
            hasilP = Math.pow(nilaiV,2)/ nilaiR;
            hasilC = nilaiV / nilaiR;
            System.out.println("Power : "+ hasilP);
            System.out.println("Current : "+ hasilC);
        } else if(nilaiP == 0 && nilaiR == 0){
            System.out.println("Voltage : "+nilaiV);
            System.out.println("Current : "+nilaiC);
            hasilP = nilaiV * nilaiC;
            hasilR = nilaiV / nilaiC;
            System.out.println("Power : "+ hasilP);
            System.out.println("Resistance : "+hasilR);
        } else if(nilaiV == 0 && nilaiC == 0){
            System.out.println("Resistance : "+ nilaiR);
            System.out.println("Power : "+nilaiP);
            hasilV = Math.sqrt(nilaiP*nilaiR);
            hasilC = Math.sqrt(nilaiP / nilaiR);
            System.out.println("Voltage : "+hasilV);
            System.out.println("Current : "+ hasilC);
        } else if (nilaiV == 0 && nilaiR==0 ){
            System.out.println("Current : "+nilaiC);
            System.out.println("Power : "+nilaiP);
            hasilV = nilaiP / nilaiC;
            hasilR = nilaiP / Math.pow(nilaiC,2);
            System.out.println("Voltage : "+hasilV);
            System.out.println("Resistance : "+hasilR);
        }else if(nilaiC == 0 && nilaiR == 0){
            System.out.println("Voltage : "+nilaiV);
            System.out.println("Power : "+nilaiP);
            hasilC = nilaiP / nilaiV;
            hasilR = Math.pow(nilaiV,2)/ nilaiP;
            System.out.println("Current : "+hasilC);
            System.out.println("Resistance : "+hasilR);
        }else{
            System.out.println("inputan tidak sesuai");
        }

//        if(nilaiP == 0){
//            if (nilaiV == 0) {
//                hasilP = Math.pow(nilaiC,2) * nilaiR;
//                hasilV = nilaiC * nilaiR;
//                System.out.println("");
//                System.out.println("Voltage : "+hasilV);
//                System.out.println("Power : "+hasilP);
//            }else if(nilaiC == 0){
//                //diket r v
//                hasilP = Math.pow(nilaiV,2)/ nilaiV;
//                hasilC = nilaiV / nilaiR;
//                System.out.println("Power : "+ hasilP);
//                System.out.println("Current : "+ hasilC);
//            }else {
//                // diket c v
//                hasilP = nilaiV * nilaiC;
//                hasilR = nilaiV / nilaiC;
//                System.out.println("Power : "+ hasilP);
//                System.out.println("Resistance : "+hasilR);
//            }
//        }else if(nilaiV == 0){
//            if (nilaiC == 0){
//                //diket r p
//                hasilV = Math.sqrt(nilaiP*nilaiR);
//                hasilC = Math.sqrt(nilaiP / nilaiR);
//                System.out.println("Voltage : "+hasilV);
//                System.out.println("Current : "+ hasilC);
//            }else {
//                //diket c p
//                hasilV = nilaiP / nilaiC;
//                hasilR = nilaiP / Math.pow(nilaiC,2);
//                System.out.println("Voltage : "+hasilV);
//                System.out.println("Power : "+hasilP);
//            }
//        }else{
//            hasilC = nilaiP / nilaiV;
//            hasilR = Math.pow(nilaiV,2)/ nilaiP;
//            System.out.println("Current : "+hasilC);
//            System.out.println("Resistance : "+hasilR);
//        }

//        if (r == null && c== null && v == null && p== null){
//            System.out.println("Tidak ada nilai yang dimasukan");
//            return 0;
//        } else if(){
////            if(){
////
////            }
//        }else{
//
//        }
//        return 0;
    }

}
