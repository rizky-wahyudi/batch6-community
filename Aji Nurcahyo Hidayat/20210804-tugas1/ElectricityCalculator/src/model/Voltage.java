package model;

public class Voltage {
    private double nilaiVoltage = 0;

    public double getNilaiVoltage() {
        return nilaiVoltage;
    }

    public void setNilaiVoltage(double nilaiVoltage) {
        this.nilaiVoltage = nilaiVoltage;
    }
}
