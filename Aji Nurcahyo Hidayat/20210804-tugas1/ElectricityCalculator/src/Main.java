import model.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Resitance resitance = new Resitance();
        Current current = new Current();
        Voltage voltage = new Voltage();
        Power power = new Power();
        System.out.println("==== Electricity Calculator ====");
        boolean pilihLagi = true;
        while(pilihLagi) {
            System.out.println("\n1. Resistance : " + resitance.getNilaiResistance());
            System.out.println("2. Current : "+current.getNilaiCurrent());
            System.out.println("3. Voltage : "+ voltage.getNilaiVoltage());
            System.out.println("4. power : " + power.getNilaiPower());
            Scanner input = new Scanner(System.in);
            System.out.print("\nPilih yang mau anda atur nilainya: ");
            int pilihAtur = input.nextInt();
            switch (pilihAtur){
                case 1:
                    System.out.println("nilai resistace : ");
                    Scanner inpR = new Scanner(System.in);
                    double nR = inpR.nextDouble();
                    resitance.setNilaiResistance(nR);
                    break;
                case 2:
                    System.out.println("nilai current : ");
                    Scanner inpC = new Scanner(System.in);
                    double nC = inpC.nextDouble();
                    current.setNilaiCurrent(nC);
                    break;
                case 3:
                    System.out.println("nilai voltage : ");
                    Scanner inpV = new Scanner(System.in);
                    double nV = inpV.nextDouble();
                    voltage.setNilaiVoltage(nV);
                    break;
                case 4:
                    System.out.println("nilai power : ");
                    Scanner inpP = new Scanner(System.in);
                    double nP = inpP.nextDouble();
                    power.setNilaiPower(nP);
                    break;
                default:
                    System.out.println("Tidak ada");
                    break;
            }
            System.out.print("pilih nilai yang lain? (1: ya / 0: tidak)");
            Scanner input1 = new Scanner(System.in);
            int pilihLagi1 = input1.nextInt();
            if (pilihLagi1 == 0){
                pilihLagi = false;
            }else {
                pilihLagi = true;
            }
        }
        Calculate hasil = new Calculate(resitance,current,voltage,power);
        hasil.calculateElectricity();

    }
}
