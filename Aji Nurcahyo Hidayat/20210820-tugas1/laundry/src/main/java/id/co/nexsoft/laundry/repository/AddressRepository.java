package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.entity.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AddressRepository extends CrudRepository<Address, Integer> {
    Address findById(int id);
    List<Address> findAll();
    void deleteById(int id);
}
