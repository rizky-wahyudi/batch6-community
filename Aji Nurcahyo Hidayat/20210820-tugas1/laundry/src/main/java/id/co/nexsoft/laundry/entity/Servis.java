package id.co.nexsoft.laundry.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Servis {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String price;
    private int unit;
    private String type;

//    @OneToMany(mappedBy = "servis", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JsonBackReference
//    private List<OrderItems> orderItemsList;

    public Servis() {
    }

    public Servis(int id, String name, String price, int unit, String type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.type = type;
//        this.orderItemsList = orderItemsList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

//    public List<OrderItems> getOrderItemsList() {
//        return orderItemsList;
//    }
//
//    public void setOrderItemsList(List<OrderItems> orderItemsList) {
//        this.orderItemsList = orderItemsList;
//    }
}
