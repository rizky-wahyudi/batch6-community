package id.co.nexsoft.laundry.controller;

import id.co.nexsoft.laundry.entity.OrderItems;
import id.co.nexsoft.laundry.entity.Orderan;
import id.co.nexsoft.laundry.repository.OrderItemsRepository;
import id.co.nexsoft.laundry.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemsRepository orderItemRepository;

    @GetMapping(value = "/orderan")
    public List<Orderan> getAllOrder() {
        return orderRepository.findAll();
    }

    @GetMapping(value = "orderan/{id}")
    public Orderan getOrderanById(@PathVariable int id) {
        return orderRepository.findById(id);
    }

    @PostMapping(value = "/orderan")
    public Orderan addOrderan(@RequestBody Orderan orderan) {
        Iterable<OrderItems> orderItems = orderan.getOrderItemsList();
        orderItemRepository.saveAll(orderItems);
        return orderRepository.save(orderan);
    }

    @PutMapping("/orderan/{id}")
    public ResponseEntity<Object> updateOrderan(@RequestBody Orderan order, @PathVariable int id) {

        Optional<Orderan> orderanOptional = Optional.ofNullable(orderRepository.findById(id));

        if (!orderanOptional.isPresent())
            return ResponseEntity.notFound().build();

        order.setId(id);
        orderRepository.save(order);
        return ResponseEntity.noContent().build();
    }
}