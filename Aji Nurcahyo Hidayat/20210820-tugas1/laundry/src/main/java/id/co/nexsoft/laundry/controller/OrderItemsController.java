package id.co.nexsoft.laundry.controller;

import id.co.nexsoft.laundry.entity.OrderItems;
import id.co.nexsoft.laundry.repository.OrderItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderItemsController {
    @Autowired
    private OrderItemsRepository orderItemsRepository;

    @PostMapping("orderItems")
    private OrderItems addOrderItems(@RequestBody OrderItems orderItems){
        return orderItemsRepository.save(orderItems);
    }

    @GetMapping("/orderItems")
    private List<OrderItems> getAllOrderItems(){
        return orderItemsRepository.findAll();
    }

    @GetMapping("/orderItems/{id}")
    private OrderItems getOrderItemsById(@PathVariable int id){

        return orderItemsRepository.findById(id);
    }
}
