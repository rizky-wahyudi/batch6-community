package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.entity.OrderItems;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface OrderItemsRepository extends CrudRepository<OrderItems, Integer> {
    OrderItems findById(int id);
    List<OrderItems> findAll();
    void deleteById(int id);
}
