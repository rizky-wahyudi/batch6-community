package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.entity.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    Employee findById(int id);
    List<Employee> findAll();
    void deleteById(int id);
}
