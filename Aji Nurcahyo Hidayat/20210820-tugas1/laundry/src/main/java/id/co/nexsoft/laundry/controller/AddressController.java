package id.co.nexsoft.laundry.controller;

import id.co.nexsoft.laundry.entity.Address;
import id.co.nexsoft.laundry.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AddressController {
    @Autowired
    private AddressRepository addressRepository;

    @GetMapping("/address")
    public List<Address> getAllAddress(){
        return addressRepository.findAll();
    }
}
