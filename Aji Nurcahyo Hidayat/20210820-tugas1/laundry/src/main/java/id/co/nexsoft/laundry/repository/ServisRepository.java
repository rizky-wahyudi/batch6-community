package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.entity.Servis;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServisRepository extends CrudRepository<Servis, Integer> {
    Servis findById(int id);
    List<Servis> findAll();
    void deleteById(int id);
}
