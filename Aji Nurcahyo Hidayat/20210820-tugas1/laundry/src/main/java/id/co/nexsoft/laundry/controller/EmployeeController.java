package id.co.nexsoft.laundry.controller;

import id.co.nexsoft.laundry.entity.Address;
import id.co.nexsoft.laundry.entity.Customer;
import id.co.nexsoft.laundry.entity.Employee;
import id.co.nexsoft.laundry.repository.AddressRepository;
import id.co.nexsoft.laundry.repository.CustomerRepository;
import id.co.nexsoft.laundry.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AddressRepository addressRepository;

    @GetMapping("/employee")
    public List<Employee> getAllEmployee(){
        return employeeRepository.findAll();
    }

    @GetMapping("/employee/{id}")
    public Employee getEmployeeById(@PathVariable int id){
        return employeeRepository.findById(id);
    }

    @PostMapping("/employee")
    public Employee addEmployee(@RequestBody Employee employee){
        Address address = employee.getAddress();
        addressRepository.save(address);
        employeeRepository.save(employee);
        return employee;
    }
}
