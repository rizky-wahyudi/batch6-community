package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    Customer findById(int id);
    List<Customer> findAll();
    void deleteCustomerById(int id);
}
