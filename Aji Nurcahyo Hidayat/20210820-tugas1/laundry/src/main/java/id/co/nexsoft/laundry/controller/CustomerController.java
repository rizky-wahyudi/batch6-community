package id.co.nexsoft.laundry.controller;

import id.co.nexsoft.laundry.entity.Address;
import id.co.nexsoft.laundry.entity.Customer;
import id.co.nexsoft.laundry.repository.AddressRepository;
import id.co.nexsoft.laundry.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private AddressRepository addressRepository;

    @GetMapping("/customer")
    public List<Customer> getAllCustomer(){
        return customerRepository.findAll();
    }

    @GetMapping("/customer/{id}")
    public Customer getCusstomerById(@PathVariable int id){
        return customerRepository.findById(id);
    }

    @PostMapping("/customer")
    public Customer addCustomer(@RequestBody Customer customer){
        Address address = customer.getAddress();
        addressRepository.save(address);
        customerRepository.save(customer);
        return customer;
    }
}
