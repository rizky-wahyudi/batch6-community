package id.co.nexsoft.laundry.controller;

import id.co.nexsoft.laundry.entity.Address;
import id.co.nexsoft.laundry.entity.Employee;
import id.co.nexsoft.laundry.entity.OrderItems;
import id.co.nexsoft.laundry.entity.Servis;
import id.co.nexsoft.laundry.repository.ServisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServisController {
    @Autowired
    private ServisRepository servisRepository;

    @GetMapping("/servis")
    public List<Servis> getAllServis(){
        return servisRepository.findAll();
    }

    @GetMapping("/servis/{id}")
    public Servis getServisById(@PathVariable int id){
        return servisRepository.findById(id);
    }

    @PostMapping("/servis")
    public Servis addServis(@RequestBody Servis servis){
        servisRepository.save(servis);
        return servis;
    }

//    @GetMapping("/servis/{id}/list")
//    public List<OrderItems> getListServisItem(@PathVariable int id){
//        return servisRepository.findById(id).getOrderItemsList();
//    }
}
