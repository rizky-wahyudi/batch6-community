package id.co.nexsoft.laundry.repository;

import id.co.nexsoft.laundry.entity.Orderan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Orderan, Integer> {
    Orderan findById(int id);
    List<Orderan> findAll();
}
