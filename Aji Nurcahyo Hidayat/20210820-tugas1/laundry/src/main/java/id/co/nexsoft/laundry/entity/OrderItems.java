package id.co.nexsoft.laundry.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
public class OrderItems {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    private String itemName;
    private int weight;
    private int amount;

    @ManyToOne
    @JoinColumn(name = "ServisId")
    private Servis servis;

    public OrderItems() {
    }

    public OrderItems(int id, String itemName, int weight, int amount, Servis servis) {
        this.id = id;
        this.itemName = itemName;
        this.weight = weight;
        this.amount = amount;
        this.servis = servis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Servis getServis() {
        return servis;
    }

    public void setServis(Servis servis) {
        this.servis = servis;
    }
}
