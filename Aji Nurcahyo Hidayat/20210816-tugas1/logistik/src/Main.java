import controller.*;
import model.Address;
import model.DistanceMapper;
import model.Transportasi;
import model.Warehouse;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Main {
    public static void main(String[] args){
        AddressController addressController = new AddressController();
        initAddress(addressController);

        DistanceMapperController distanceMapperController = new DistanceMapperController();
        initDistanceMapper(distanceMapperController);

        WarehouseController warehouseController = new WarehouseController();
        initWarehouse(warehouseController, addressController, distanceMapperController);

        TrasnportasiController trasnportasiController = new TrasnportasiController();
        initTransportasi(trasnportasiController);

        ProductControler productControler = new ProductControler();
        initProduct(productControler);

        System.out.println("Daftar barang yang akan diantar : ");
        System.out.println("===================================");
        productControler.show();
        System.out.println("Total size : "+productControler.getTotalSize());
        System.out.println("Total weight : "+productControler.getTotalWeight());
        Transportasi vehicle = setDelivery(productControler, trasnportasiController);
        System.out.println("\nPengiriman menggunakan : "+ vehicle.getType());
        System.out.println("Supir : "+vehicle.getDriver());
        System.out.println("Beban Max : "+vehicle.getLoad());
        System.out.println("Volume Max : "+vehicle.getSize()+" cm kubik");
        System.out.println("Kecepatan : "+vehicle.getSpeed());

        int idFrom = 1;
        int idTo = 3;
        getRoute(idFrom, idTo, warehouseController, vehicle);
    }

    public static void initAddress(AddressController addressController){
        addressController.createAddress(
                1,
                "jl.perwira",
                "cilacap",
                "jawa tengah",
                "indonesia",
                "018081839218",
                "0971781671"
        );
        addressController.createAddress(
                2,
                "jl.madrani",
                "purwokerto",
                "jawa tengah",
                "indonesia",
                "01928186751",
                "56147168"
        );
        addressController.createAddress(
                3,
                "jl.blater",
                "purbalingga",
                "jawa tengah",
                "indonesia",
                "018781265",
                "091751786"
        );
    }

    public static void initDistanceMapper(DistanceMapperController distanceMapperController){
        distanceMapperController.createDistanceMapper(1,1,2,60);
        distanceMapperController.createDistanceMapper(2,2,3,10);
        distanceMapperController.createDistanceMapper(3,3,1,80);
    }

    public static void initWarehouse(WarehouseController warehouseController,
                                     AddressController addressController,
                                     DistanceMapperController distanceMapperController){
        warehouseController.createWarehouse(
                1,
                "Warehouse Cilacap",
                addressController.getAddressById(1),
                distanceMapperController.getDistanceMapperById(1)
        );
        warehouseController.createWarehouse(
                2,
                "Warehouse Purwokerto",
                addressController.getAddressById(2),
                distanceMapperController.getDistanceMapperById(2)
        );
        warehouseController.createWarehouse(
                3,
                "Warehouse Purbalingga",
                addressController.getAddressById(3),
                distanceMapperController.getDistanceMapperById(3)
        );
    }

    public static void initTransportasi(TrasnportasiController trasnportasiController){
        trasnportasiController.createTransportasi(1,"dadang",1000000,40,"Truk",10000);
        trasnportasiController.createTransportasi(2,"wisnu",100000,60,"mobil",2000);
        trasnportasiController.createTransportasi(3,"anto",10000,70,"motoGP",50);
    }

    public static void initProduct(ProductControler productControler){
        productControler.createProduct(
                1,
                "laptop",
                "kalimanah",
                50,
                2,
                30,
                2
        );
        productControler.createProduct(
                2,
                "monitor",
                "blater utara",
                50,
                30,
                10,
                5
        );
//        productControler.createProduct(
//                3,
//                "kulkas",
//                "bojong",
//                40,
//                150,
//                30,
//                10
//        );
    }

    public static Transportasi setDelivery(ProductControler pC, TrasnportasiController tC){
        int totalSizeProduct = pC.getTotalSize();
        int totalWeightProduct = pC.getTotalWeight();
        int totalItem = pC.getListProduct().size();
        Transportasi transportasi = null;

        Transportasi truk = tC.getTransportasiById(1);
        Transportasi mobil = tC.getTransportasiById(2);
        Transportasi motor = tC.getTransportasiById(3);

        if ((totalSizeProduct <= motor.getSize()) &&
                (totalWeightProduct <= mobil.getLoad()) &&
                (totalItem <= 60)){
            transportasi = motor;
        } else if((totalSizeProduct <= mobil.getSize()) && (totalWeightProduct <= mobil.getLoad())){
            transportasi = mobil;
        }else if((totalSizeProduct <= truk.getSize()) && (totalWeightProduct <= truk.getLoad())){
            transportasi = truk;
        } else {
            transportasi = null;
        }
        return transportasi;
    }

    public static void getRoute(int from, int to, WarehouseController wC, Transportasi vehicle){
        Warehouse fromWarehouse = wC.getWarehouseById(from);
        Warehouse toWarehaouse = wC.getWarehouseById(to);
        int speed = vehicle.getSpeed();

        int indexFrom = wC.getListWarehouse().indexOf(fromWarehouse);
        int indexTo = wC.getListWarehouse().indexOf(toWarehaouse);

        System.out.println("\n\npengiriman "+fromWarehouse.getAddress().getDistrict()+" ke "
                +toWarehaouse.getAddress().getDistrict());
        System.out.println("=======================================");
        for (int i=indexFrom; i<=indexTo; i++){
            int noWarehouse = i + 1;
            System.out.println("\nWarehouse ke "+noWarehouse+" : "+wC.getListWarehouse().get(i).getNama());
            System.out.println("alamat : "+wC.getListWarehouse().get(i).getAddress().getStreet());
            if(i+1 <= indexTo){
                int jarak = wC.getListWarehouse().get(i).getDistanceMapper().getDistance();
                float estimasiWaktu;
                estimasiWaktu = jarak * 60 / speed;
                System.out.println("jarak ke warehouse berikutnya: "+jarak+" Km");
                System.out.println("estimasi sampai warehouse berikutnya : "+estimasiWaktu+" menit");
            } else {
                System.out.println("Sampai tujuan..");
            }
        }
    }

}
