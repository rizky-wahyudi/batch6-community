package model;

public class Product {
    private int id;
    private String name;
    private String deliverAddress;
    private int width;
    private int height;
    private int depth;
    private int weight;

    public Product() {
    }

    public Product(int id, String name, String deliverAddress, int width, int height, int depth, int weight) {
        this.id = id;
        this.name = name;
        this.deliverAddress = deliverAddress;
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeliverAddress() {
        return deliverAddress;
    }

    public void setDeliverAddress(String deliverAddress) {
        this.deliverAddress = deliverAddress;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
