package model;

public class Warehouse {
    private int id;
    private String nama;
    private Address address;
    private DistanceMapper distanceMapper;

    public Warehouse() {
    }

    public Warehouse(int id, String nama, Address address, DistanceMapper distanceMapper) {
        this.id = id;
        this.nama = nama;
        this.address = address;
        this.distanceMapper = distanceMapper;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public DistanceMapper getDistanceMapper() {
        return distanceMapper;
    }

    public void setDistanceMapper(DistanceMapper distanceMapper) {
        this.distanceMapper = distanceMapper;
    }
}
