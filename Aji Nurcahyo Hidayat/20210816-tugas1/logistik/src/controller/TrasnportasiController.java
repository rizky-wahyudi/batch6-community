package controller;

import model.Transportasi;
import model.Warehouse;

import java.util.HashMap;
import java.util.Map;

public class TrasnportasiController {
    HashMap<Integer, Transportasi> listTransportasi = new HashMap<>();

    public void createTransportasi(
            int id,
            String driver,
            int size,
            int speed,
            String type,
            int load
    ){
        Transportasi transportasi = new Transportasi();
        transportasi.setId(id);
        transportasi.setDriver(driver);
        transportasi.setSize(size);
        transportasi.setSpeed(speed);
        transportasi.setType(type);
        transportasi.setLoad(load);
        this.listTransportasi.put(id, transportasi);
    }

    public HashMap<Integer, Transportasi> getListTransportasi() {
        return listTransportasi;
    }

    public void setListTransportasi(HashMap<Integer, Transportasi> listTransportasi) {
        this.listTransportasi = listTransportasi;
    }

    public Transportasi getTransportasiById(int id){
        Transportasi transportasi = null;

        for (Map.Entry<Integer, Transportasi> transportasiEntry : listTransportasi.entrySet()){
            if (transportasiEntry.getKey().equals(id)){
                transportasi = transportasiEntry.getValue();
            }
        }
        return transportasi;
    }

}
