package controller;

import model.Product;

import java.util.HashMap;
import java.util.Map;

public class ProductControler {
    HashMap<Integer,Product> listProduct = new HashMap<>();

    public void createProduct(
            int id,
            String name,
            String deliverAddress,
            int width,
            int height,
            int depth,
            int weight
    ){
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setDeliverAddress(deliverAddress);
        product.setWidth(width);
        product.setHeight(height);
        product.setDepth(depth);
        product.setWeight(weight);
        this.listProduct.put(id, product);
    }

    public HashMap<Integer, Product> getListProduct() {
        return listProduct;
    }

    public void setListProduct(HashMap<Integer, Product> listProduct) {
        this.listProduct = listProduct;
    }

    public void show(){
        for (Map.Entry<Integer, Product> listOfProduct : listProduct.entrySet()){
            int id = listOfProduct.getKey();
            String name = listOfProduct.getValue().getName();
            String to = listOfProduct.getValue().getDeliverAddress();
            int width = listOfProduct.getValue().getWidth();
            int height = listOfProduct.getValue().getHeight();
            int depth = listOfProduct.getValue().getDepth();
            int weight = listOfProduct.getValue().getWeight();
            System.out.println(id+". "+name+", lebar "+width+" cm, tinggi "+height+" cm, depth "+depth+" cm");
            System.out.println("berat "+weight+" kg");
            System.out.println("pengiriman : "+to+"\n");
        }
    }

    public int getTotalSize(){
        int totalSize = 0;
        for (Map.Entry<Integer, Product> listOfProduct : listProduct.entrySet()){
            int width = listOfProduct.getValue().getWidth();
            int height = listOfProduct.getValue().getHeight();
            int depth = listOfProduct.getValue().getDepth();
            int size = width * depth * height;
            totalSize = totalSize + size;
        }
        return totalSize;
    }

    public int getTotalWeight(){
        int totalWeight = 0;
        for (Map.Entry<Integer, Product> listOfProduct : listProduct.entrySet()){
            int weight = listOfProduct.getValue().getWeight();
            totalWeight = totalWeight + weight;
        }
        return totalWeight;
    }
}
