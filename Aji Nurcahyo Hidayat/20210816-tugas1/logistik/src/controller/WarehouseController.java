package controller;

import model.Address;
import model.DistanceMapper;
import model.Warehouse;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class WarehouseController {
//    HashMap<Integer, Warehouse> listWarehouse = new HashMap<>();
    LinkedList<Warehouse> listWarehouse = new LinkedList<>();

    public void createWarehouse(
            int id,
            String nama,
            Address address,
            DistanceMapper distanceMapper
    ){
        Warehouse warehouse = new Warehouse();
        warehouse.setId(id);
        warehouse.setNama(nama);
        warehouse.setAddress(address);
        warehouse.setDistanceMapper(distanceMapper);
        this.listWarehouse.add(warehouse);
    }

    public LinkedList<Warehouse> getListWarehouse() {
        return listWarehouse;
    }

    public void setListWarehouse(LinkedList<Warehouse> listWarehouse) {
        this.listWarehouse = listWarehouse;
    }

    public Warehouse getWarehouseById(int id){
        Warehouse warehouse = null;

        for (int i = 0; i<listWarehouse.size(); i++){
            if (listWarehouse.get(i).getId() == id){
                warehouse = listWarehouse.get(i);
            }
        }
        return warehouse;
    }
}
