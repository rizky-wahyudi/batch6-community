package controller;

import model.DistanceMapper;
import model.Warehouse;

import java.util.HashMap;
import java.util.Map;

public class DistanceMapperController {
    HashMap<Integer, DistanceMapper> listDistanceMapper = new HashMap<>();

    public void createDistanceMapper(
            int id,
            int fromWarehouseId,
            int toWarehouseId,
            int distance
    ){
        DistanceMapper distanceMapper = new DistanceMapper();
        distanceMapper.setId(id);
        distanceMapper.setFromWarehouseId(fromWarehouseId);
        distanceMapper.setToWarehouseId(toWarehouseId);
        distanceMapper.setDistance(distance);
        this.listDistanceMapper.put(id,distanceMapper);
    }

    public DistanceMapper getDistanceMapperById(int id){
        DistanceMapper distanceMapper = null;

        for (Map.Entry<Integer, DistanceMapper> distanceMapperEntry : listDistanceMapper.entrySet()){
            if (distanceMapperEntry.getKey().equals(id)){
                distanceMapper = distanceMapperEntry.getValue();
            }
        }
        return distanceMapper;
    }
}
