package controller;

import model.Address;

import java.util.HashMap;
import java.util.Map;

public class AddressController {
    HashMap<Integer, Address> listAddress = new HashMap<>();

    public HashMap<Integer, Address> getListAddress() {
        return listAddress;
    }

    public void setListAddress(HashMap<Integer, Address> listAddress) {
        this.listAddress = listAddress;
    }

    public void createAddress(
            int id,
            String street,
            String district,
            String province,
            String country,
            String longtitude,
            String latitude
    ){
        Address address = new Address();
        address.setId(id);
        address.setStreet(street);
        address.setDistrict(district);
        address.setProvince(province);
        address.setCountry(country);
        address.setLongtitude(longtitude);
        address.setLatitude(latitude);
        this.listAddress.put(id, address);
    }

    public Address getAddressById(int id){
        Address address = null;

        for (Map.Entry<Integer, Address> addressEntry : listAddress.entrySet()){
            if (addressEntry.getKey().equals(id)){
                address = addressEntry.getValue();
            }
        }
        return address;
    }

}
