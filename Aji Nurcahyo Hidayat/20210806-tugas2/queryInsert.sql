INSERT
    INTO address(street, longtitude, latitude, city, district, province, country)
    VALUES
    ("jl. manggis", "-7.722505", "109.006666", "cilacap", "cilacap selatan", "jawa tengah", "indonesia"),
    ("jl. madrani", "-7.404132", "109.249896", "purwokerto", "purwokerto utara", "jawa tengah", "indonesia"),
    ("jl. blater", "-7.4306470", "109.335855", "purbalingga", "purbalingga", "jawa tengah", "indonesia")
    ;


INSERT
    INTO distributor(name, addressId, addressType)
    VALUES
    ("Dika","1","perumahan"),
    ("Wahyu","2","kantor"),
    ("Adi","3","perumhan")
    ;


INSERT
    INTO product(artNumber, sellPrice, expiredDate)
    VALUES
    ("A001","10000","2022-08-01"),
    ("A002","8000","2022-08-01"),
    ("A003","5000","2022-08-01"),
    ("A004","7000","2022-08-01"),
    ("A005","12000","2022-08-01"),
    ("A006","11000","2022-08-01"),
    ("A007","9000","2022-08-01"),
    ("A009","7500","2022-08-01"),
    ("A010","6000","2022-08-01");
INSERT
    INTO product(artNumber, sellPrice, expiredDate)
    VALUES
    ("B012","15000","2022-08-01");


INSERT
    INTO productImage(url, description, productId)
    VALUES
    ("https://www.mayora.com/media/1986/2017-bengbeng.png","jajan beng beng","1"),
    ("https://ds393qgzrxwzn.cloudfront.net/resize/m400x400/cat1/img/images/0/ouoJC9dMoX.jpg","jajan sari gandum","2"),
    ("https://ds393qgzrxwzn.cloudfront.net/resize/m400x400/cat1/img/images/0/qjj9RvyBe6.jpg","jajan astor","3"),
    ("https://singapore-s3.123ish.com/images/7376/amp_standard_webp?1566468733","jajan tanggo","4"),
    ("https://singapore-s3.123ish.com/images/7507/amp_standard_webp?1568036350","jajan silverqueen","5"),
    ("https://assets.klikindomaret.com/products/20052977/20052977_1.jpg","jajan cheetos","6"),
    ("https://cf.shopee.co.id/file/5e7158c128297762a0aef79abda176e2","pocari","7"),
    ("https://assets.klikindomaret.com/share/20055205/20055205_1.jpg","jajan taro","8"),
    ("https://assets.indozone.news/local/5f7ffbfb2103e.jpg","jajan biskuat","9"),
    ("https://www.nabatisnack.co.id/lib/product/3457911938wafer.png","jajan nabati","10");


INSERT
    -> INTO productStock(productId, productDistributorId, purchaseDate, deliveryDate, stock)
    -> VALUES
    -> (1, 1, "2021-08-01","2021-09-01", 10),
    -> (2, 2, "2021-08-01","2021-09-01", 10),
    -> (3, 3, "2021-08-01","2021-09-01", 0),
    -> (4, 4, "2021-08-01","2021-09-01", 5),
    -> (5, 5, "2021-08-01","2021-09-01", 0),
    -> (6, 8, "2021-08-01","2021-09-01", 10),
    -> (7, 9, "2021-08-01","2021-09-01", 5),
    -> (8, 6, "2021-08-01","2021-09-01", 0),
    -> (9, 10, "2021-08-01","2021-09-01", 10),
    -> (10, 7, "2021-08-01","2021-09-01", 15);