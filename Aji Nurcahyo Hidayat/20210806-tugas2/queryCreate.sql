CREATE TABLE address(
    id INT NOT NULL AUTO_INCREMENT,
    street VARCHAR(255) NOT NULL,
    city VARCHAR(50) NOT NULL,
    district VARCHAR(50) NOT NULL,
    province VARCHAR(50) NOT NULL,
    country VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
    );


CREATE TABLE distributor (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    addressId INT NOT NULL,
    addressType VARCHAR(50) NOT NULL,
    FOREIGN KEY (addressId) REFERENCES address(id),
    PRIMARY KEY (id)
    );


CREATE TABLE productDistributor(
    id INT NOT NULL AUTO_INCREMENT,
    distributorId INT NOT NULL,
    buyPrice INT NOT NULL,
    stock INT NOT NULL,
    FOREIGN KEY (distributorId) REFERENCES distributor(id),
    PRIMARY KEY (id)
    );


CREATE TABLE productImage(
    id INT NOT NULL AUTO_INCREMENT,
    url VARCHAR(2083) NOT NULL,
    description VARCHAR(255),
    PRIMARY KEY (id)
    );

CREATE TABLE product(
    id INT NOT NULL AUTO_INCREMENT,
    artNumber VARCHAR(10) NOT NULL,
    sellPrice INT NOT NULL,
    expiredDate DATE NOT NULL,
    FOREIGN KEY (productImageId) REFERENCES productImage(id),
    PRIMARY KEY (id)
    );


 CREATE TABLE productStock(
    id INT NOT NULL AUTO_INCREMENT,
    productId INT NOT NULL,
    stock INT NOT NULL,
    productDistributorId INT NOT NULL,
    purchaseDate DATE,
    deliveryDate DATE,
    FOREIGN KEY (productId) REFERENCES product(id),
    FOREIGN KEY (productDistributorId) REFERENCES productDistributor(id),
    PRIMARY KEY (id)
    );


ALTER
    TABLE address
    ADD longtitude VARCHAR(25) NOT NULL
    AFTER street;
 ALTER
    TABLE address
    ADD latitude VARCHAR(25) NOT NULL
    AFTER longtitude;

ALTER
    TABLE productImage
    ADD productId INT NOT NULL;
ALseleTER
    TABLE productImage
    ADD CONSTRAINT fk_productImage_product
    FOREIGN KEY (productId)
    REFERENCES product(id);
