import model.Pasal;
import model.Peraturan;
import model.SubPasal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {


        ArrayList<Peraturan> peraturanArrays = new ArrayList<Peraturan>();
        ArrayList<String> menimbang = new ArrayList<String>(Arrays.asList("point 1. blablabla","Point 2. blablabla"));
        ArrayList<Pasal> menetapkan = new ArrayList<Pasal>();

        Pasal pasal1 = new Pasal();
        Pasal pasal2 = new Pasal();
        SubPasal sub1 = new SubPasal("subpasal 1 .....");
        SubPasal sub2 = new SubPasal("subpasal 2 .....");
        ArrayList<SubPasal> listSubPasal = new ArrayList<SubPasal>(Arrays.asList(sub1,sub2));
        pasal1.setNamapasal("pasal 1");
        pasal1.setSubpasal(listSubPasal);
        pasal2.setNamapasal("pasal 2");
        pasal2.setSubpasal(listSubPasal);
        menetapkan.add(pasal1);
        menetapkan.add(pasal2);

        Peraturan peraturan1 = new Peraturan("2021-07-30",
                0001,
                01,
                "2021-07-31",
                "Peraturan 1",
                1,
                "2021",
                menimbang,
                "Ini mengingan peraturan 1",
                menetapkan);

        Peraturan peraturan2 = new Peraturan("2010-07-30",
                0002,
                02,
                "2010-07-31",
                "Peraturan 2",
                2,
                "2010",
                menimbang,
                "Ini Mengingat yang ada di peraturan 2",
                menetapkan);

        peraturanArrays.add(peraturan1);
        peraturanArrays.add(peraturan2);
        System.out.println("======== Daftar Peraturan ========");
        for (int i=0; i<peraturanArrays.size(); i++){
            System.out.println();
            System.out.println();
            System.out.println("Nomor Peraturan: "+peraturanArrays.get(i).getNomorPeraturan());
            System.out.println("Nama Peraturan: "+peraturanArrays.get(i).getTentang());
            System.out.println("Tahun Peraturan: "+peraturanArrays.get(i).getTahunPeraturan());
            System.out.println("Nomor TLN Peraturan: "+peraturanArrays.get(i).getNomorTLN());
            System.out.println("Nomor LN Peraturan: "+peraturanArrays.get(i).getNomorLN());

        }

        System.out.println("Mau Cek detail?(no = 0 / ya = 1) :");
        Scanner input = new Scanner(System.in);
        int cek = input.nextInt();
        while (cek == 1){
            System.out.println("pilih yang mau di cek : ");
            int idCek = input.nextInt();
            Peraturan peraturanCek = null;
            for (int i=0;i<peraturanArrays.size();i++) {
                if (peraturanArrays.get(i).getNomorPeraturan() == idCek ){
                    peraturanCek = peraturanArrays.get(i);
                    break;
                }else {
                    System.out.println("peraturan tidak ada");
                }
            }

            System.out.println("\nNomor Peraturan: "+peraturanCek.getNomorPeraturan());
            System.out.println("Peraturan: "+peraturanCek.getTentang());
            System.out.println("\nMenimbang: ");
            for (int i=0;i<peraturanCek.getMenimbang().size();i++) {
                System.out.println(peraturanCek.getMenimbang().get(i));
            }
            System.out.println("\nMengingat: "+peraturanCek.getMengingat());
            System.out.println("\nMenetapkan: " );
            for (int i=0;i<peraturanCek.getMenetapkan().size();i++) {
                System.out.println();
                System.out.println(peraturanCek.getMenetapkan().get(i).getNamapasal());
                Pasal pasalCek = peraturanCek.getMenetapkan().get(i);
                for (int j=0; j<pasalCek.getSubpasal().size(); j++){
                    System.out.println(pasalCek.getSubpasal().get(j).getSubpasal());
                }
            }
            System.out.println("\ncek yang lain?(no = 0 / ya = 1): ");
            cek = input.nextInt();
        }

    }
}
