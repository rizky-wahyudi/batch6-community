package model;

import java.util.ArrayList;

public class Peraturan extends DetailPeraturan{
    private String tglDiundangkan;
    private int nomorTLN;
    private int nomorLN;
    private String tglDiterapkan;
    private String tentang;
    private int nomorPeraturan;
    private String tahunPeraturan;

    public Peraturan(String tglDiundangkan,
                     int nomorTLN,
                     int nomorLN,
                     String tglDiterapkan,
                     String tentang,
                     int nomorPeraturan,
                     String tahunPeraturan,
                     ArrayList<String> menimbang,
                     String mengingat,
                     ArrayList<Pasal> menetapkan) {
        this.tglDiundangkan = tglDiundangkan;
        this.nomorTLN = nomorTLN;
        this.nomorLN = nomorLN;
        this.tglDiterapkan = tglDiterapkan;
        this.tentang = tentang;
        this.nomorPeraturan = nomorPeraturan;
        this.tahunPeraturan = tahunPeraturan;
        super.setMenimbang(menimbang);
        super.setMengingat(mengingat);
        super.setMenetapkan(menetapkan);
    }

    public String getTglDiundangkan() {
        return tglDiundangkan;
    }

    public int getNomorTLN() {
        return nomorTLN;
    }

    public int getNomorLN() {
        return nomorLN;
    }

    public String getTglDiterapkan() {
        return tglDiterapkan;
    }

    public String getTentang() {
        return tentang;
    }

    public int getNomorPeraturan() {
        return nomorPeraturan;
    }

    public String getTahunPeraturan() {
        return tahunPeraturan;
    }
}
