package model;

public class SubPasal {
    private String subpasal;

    public SubPasal(String subpasal) {
        this.subpasal = subpasal;
    }

    public String getSubpasal() {
        return subpasal;
    }

    public void setSubpasal(String subpasal) {
        this.subpasal = subpasal;
    }
}
