package model;

import java.util.ArrayList;

public class Pasal {
    private String namapasal;
    private ArrayList<SubPasal> subpasal;

    public String getNamapasal() {
        return namapasal;
    }

    public void setNamapasal(String namapasal) {
        this.namapasal = namapasal;
    }

    public ArrayList<SubPasal> getSubpasal() {
        return subpasal;
    }

    public void setSubpasal(ArrayList<SubPasal> subpasal) {
        this.subpasal = subpasal;
    }
}
