package model;

import java.util.ArrayList;

public class DetailPeraturan {
    private ArrayList<String> menimbang;
    private String mengingat;
    private ArrayList<Pasal> menetapkan;

//    public DetailPeraturan(ArrayList<String> menimbang, String mengingat, ArrayList<String> menetapkan) {
//        this.menimbang = menimbang;
//        this.mengingat = mengingat;
//        this.menetapkan = menetapkan;
//    }

    public ArrayList<String> getMenimbang() {
        return menimbang;
    }

    public void setMenimbang(ArrayList<String> menimbang) {
        this.menimbang = menimbang;
    }

    public String getMengingat() {
        return mengingat;
    }

    public void setMengingat(String mengingat) {
        this.mengingat = mengingat;
    }

    public ArrayList<Pasal> getMenetapkan() {
        return menetapkan;
    }

    public void setMenetapkan(ArrayList<Pasal> menetapkan) {
        this.menetapkan = menetapkan;
    }
}
