package model;

public class TranslatorSuhu {
    private double suhu;

    public TranslatorSuhu(){
        this.suhu = 0;
    }

    public double getSuhu() {
        return suhu;
    }

    public void setSuhu(double suhu) {
        this.suhu = suhu;
    }
}
