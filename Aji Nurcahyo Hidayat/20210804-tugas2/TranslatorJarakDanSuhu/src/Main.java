import model.TranslatorJarak;
import model.TranslatorSuhu;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("==== Translator ====");
        System.out.println("\n1. Jarak");
        System.out.println("2. Suhu");
        System.out.print("\nMau hitung apa? (masukan angka): ");
        Scanner input = new Scanner(System.in);
        int pilih = input.nextInt();
        switch (pilih) {
            case 1:
                System.out.println("\n1. KM - Mil");
                System.out.println("2. Mil - KM");
                System.out.print("\nPilih yang mana : ");
                int pilihJarak = new Scanner(System.in).nextInt();
                System.out.print("masukan jaraknya: ");
                double nilaiJarak = new Scanner(System.in).nextDouble();
                if (pilihJarak == 1) {
                    TranslatorJarak jarak = new TranslatorJarak();
                    jarak.setNilaiJarak(nilaiJarak);
                    System.out.println("\n"+jarak.getNilaiJarak()+" Kilometer sama dengan ");
                    System.out.println(hitungKmToMil(jarak)+" Mil");
                } else if (pilihJarak == 2) {
                    TranslatorJarak jarak = new TranslatorJarak();
                    jarak.setNilaiJarak(nilaiJarak);
                    System.out.println("\n"+jarak.getNilaiJarak()+" Mil sama dengan");
                    System.out.println(hitumgMilToKm(jarak)+" Kilometer");
                } else {
                    System.out.println("tidak ada");
                }
                break;
            case 2:
                System.out.println("\n1. Celcius - Fahrenheit");
                System.out.println("2. Fahrenheit - Celcius");
                System.out.print("\nPilih yang mana : ");
                int pilihSuhu = new Scanner(System.in).nextInt();
                System.out.print("masukan suhunya: ");
                double nilaiSuhu = new Scanner(System.in).nextDouble();
                if (pilihSuhu == 1) {
                    TranslatorSuhu suhu = new TranslatorSuhu();
                    suhu.setSuhu(nilaiSuhu);
                    System.out.println("\n"+suhu.getSuhu()+" Celcius sama dengan ");
                    System.out.println(hitungCelciusToFahrenheit(suhu)+" Fahrenheit");
                } else if (pilihSuhu == 2) {
                    TranslatorSuhu suhu = new TranslatorSuhu();
                    suhu.setSuhu(nilaiSuhu);
                    System.out.println("\n"+suhu.getSuhu()+" Fahrenheit sama dengan ");
                    System.out.println(hitungFahrenheitToCelcius(suhu)+" Celcius");
                } else {
                    System.out.println("tidak ada");
                }
                break;
            default:
                System.out.println("Tidak ada..");
                break;
        }
    }

    public static double hitungKmToMil(TranslatorJarak jarak){
        double hasil = jarak.getNilaiJarak()*0.6214;
        return hasil;
    }
    public static double hitumgMilToKm(TranslatorJarak jarak){
        double hasil = jarak.getNilaiJarak() * 1.606;
        return hasil;
    }
    public static double hitungCelciusToFahrenheit(TranslatorSuhu suhu){
        double hasil = (suhu.getSuhu() * 9 / 5) +32;
        return hasil;
    }
    public static double hitungFahrenheitToCelcius(TranslatorSuhu suhu){
        double hasil = (suhu.getSuhu() - 32) * 5 / 9;
        return hasil;
    }
}
